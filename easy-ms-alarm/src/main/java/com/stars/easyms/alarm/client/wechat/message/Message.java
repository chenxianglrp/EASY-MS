package com.stars.easyms.alarm.client.wechat.message;

import java.util.Map;

/**
 * <p>className: Message</p>
 * <p>description: 企业微信机器人发送消息</p>
 *
 * @author caotieshuan
 * @version 1.5.0
 * @date 2019-12-18 16:21
 */
public interface Message {

    /**
     * 返回消息的Json格式字符串
     *
     * @return 消息的Json格式字符串
     */
    public String toJsonString();
    /**
     * 返回消息的Map格式字符串
     *
     * @return 消息的Json格式字符串
     */
    public Map<String, Object> toMessageMap();

}
