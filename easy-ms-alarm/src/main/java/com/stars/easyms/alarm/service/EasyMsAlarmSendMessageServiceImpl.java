package com.stars.easyms.alarm.service;

import com.stars.easyms.alarm.EasyMsAlarmService;
import com.stars.easyms.base.alarm.SendAlarmMessageService;

/**
 * <p>className: EasyMsAlarmSendMessageServiceImpl</p>
 * <p>description: EasyMs的实现发送告警信息的实现类</p>
 *
 * @author guoguifang
 * @version 1.7.1
 * @date 2020/12/9 2:08 下午
 */
public class EasyMsAlarmSendMessageServiceImpl implements SendAlarmMessageService {

    private EasyMsAlarmService alarmService;

    public EasyMsAlarmSendMessageServiceImpl(EasyMsAlarmService alarmService) {
        this.alarmService = alarmService;
    }

    @Override
    public void sendExceptionAlarmMessage(Throwable throwable) {
        alarmService.sendExceptionAlarmMessage(throwable);
    }

    @Override
    public void sendNormalAlarmMessage(String message) {
        alarmService.sendNormalAlarmMessage(message);
    }

}
