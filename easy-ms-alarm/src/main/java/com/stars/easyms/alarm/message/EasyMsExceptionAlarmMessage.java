package com.stars.easyms.alarm.message;

import com.stars.easyms.alarm.bean.EasyMsExceptionAlarmTraceInfo;

/**
 * <p>className: EasyMsExceptionAlarmMessage</p>
 * <p>description: 异常情况发生的告警消息</p>
 *
 * @author guoguifang
 * @version 1.6.1
 * @date 2020/8/31 9:54 上午
 */
public class EasyMsExceptionAlarmMessage extends EasyMsAlarmMessage {

    private EasyMsExceptionAlarmTraceInfo exceptionTraceInfo;

    private static final String TITLE = "异常告警";

    public EasyMsExceptionAlarmMessage() {
        this.setTitle(TITLE);
    }

    @Override
    protected String handleMessageContext() {
        if (exceptionTraceInfo != null) {
            return ExceptionAlarmMessageMarkdownHandler
                    .getInstance()
                    .handleContext(this, exceptionTraceInfo);
        }
        return null;
    }

    public void setExceptionTraceInfo(EasyMsExceptionAlarmTraceInfo exceptionTraceInfo) {
        this.exceptionTraceInfo = exceptionTraceInfo;
    }

    public EasyMsExceptionAlarmTraceInfo getExceptionTraceInfo() {
        return exceptionTraceInfo;
    }
}
