package com.stars.easyms.alarm.client.sms;

import com.stars.easyms.alarm.client.BaseEasyMsAlarmChannel;
import com.stars.easyms.alarm.constant.EasyMsAlarmConstant;
import com.stars.easyms.alarm.properties.EasyMsAlarmProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

/**
 * <p>className: EasyMsSmsAlarmChannel</p>
 * <p>description: 短信告警客户端</p>
 *
 * @author caotieshuan
 * @version 1.5.0
 * @date 2019-12-18 16:21
 */
@Slf4j
public final class EasyMsSmsAlarmChannel extends BaseEasyMsAlarmChannel {

    public EasyMsSmsAlarmChannel(EasyMsAlarmProperties alarmProperties) {
        super(alarmProperties);
    }

    @Override
    public String getChannelType() {
        return EasyMsAlarmConstant.SMS;
    }

    @Override
    protected void init(EasyMsAlarmProperties alarmProperties) {
        EasyMsAlarmProperties.Sms smsAlarmProperties = alarmProperties.getSms();
        if (smsAlarmProperties.isEnabled()) {
            String defaultPostUrl = smsAlarmProperties.getPostUrl();
            String defaultKey = smsAlarmProperties.getKey();
            String defaultToUser = smsAlarmProperties.getToUser();
            if (StringUtils.isNotBlank(defaultPostUrl) && StringUtils.isNotBlank(defaultKey) && StringUtils.isNotBlank(defaultToUser)) {
                addDefaultAlarmClient(new EasyMsSmsAlarmClient(this, defaultPostUrl, defaultKey, defaultToUser));
            }
            smsAlarmProperties.getClients().forEach(d -> {
                String id = d.getId();
                String postUrl = d.getPostUrl();
                String key = d.getKey();
                String toUser = d.getToUser();
                if (!StringUtils.isEmpty(id) && !StringUtils.isEmpty(postUrl) && !StringUtils.isEmpty(key) && !StringUtils.isEmpty(toUser)) {
                    addAlarmClient(id, new EasyMsSmsAlarmClient(this, id, postUrl, key, toUser));
                }
            });
        }
    }
}