package com.stars.easyms.alarm.client.dingtalk;

import com.stars.easyms.alarm.client.BaseEasyMsAlarmChannel;
import com.stars.easyms.alarm.constant.EasyMsAlarmConstant;
import com.stars.easyms.alarm.properties.EasyMsAlarmProperties;
import org.apache.commons.lang3.StringUtils;

/**
 * <p>className: EasyMsDingTalkAlarmClient</p>
 * <p>description: 钉钉告警客户端</p>
 *
 * @author guoguifang
 * @version 1.6.1
 * @date 2020/8/22 3:53 下午
 */
public final class EasyMsDingTalkAlarmChannel extends BaseEasyMsAlarmChannel {

    public EasyMsDingTalkAlarmChannel(EasyMsAlarmProperties alarmProperties) {
        super(alarmProperties);
    }

    @Override
    public String getChannelType() {
        return EasyMsAlarmConstant.DING_TALK;
    }

    @Override
    protected void init(EasyMsAlarmProperties alarmProperties) {
        EasyMsAlarmProperties.DingTalk dingTalkAlarmProperties = alarmProperties.getDingTalk();
        if (dingTalkAlarmProperties.isEnabled()) {
            String defaultRobotUrl = dingTalkAlarmProperties.getRobotUrl();
            String defaultRobotSecret = dingTalkAlarmProperties.getRobotSecret();
            if (!StringUtils.isEmpty(defaultRobotUrl) && !StringUtils.isEmpty(defaultRobotSecret)) {
                addDefaultAlarmClient(new EasyMsDingTalkAlarmClient(this, defaultRobotUrl, defaultRobotSecret));
            }
            dingTalkAlarmProperties.getClients().forEach(d -> {
                String id = d.getId();
                String robotUrl = d.getRobotUrl();
                String robotSecret = d.getRobotSecret();
                if (!StringUtils.isEmpty(id) && !StringUtils.isEmpty(robotUrl) && !StringUtils.isEmpty(robotSecret)) {
                    addAlarmClient(id, new EasyMsDingTalkAlarmClient(this, id, robotUrl, robotSecret));
                }
            });
        }
    }

}
