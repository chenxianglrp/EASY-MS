package com.stars.easyms.alarm.message;

import com.stars.easyms.alarm.bean.EasyMsExceptionAlarmTraceInfo;

/**
 * <p>className: ExceptionAlarmMessageMarkdownHandler</p>
 * <p>description: 异常告警服务类</p>
 *
 * @author guoguifang
 * @version 1.6.1
 * @date 2020/8/31 10:40 上午
 */
final class ExceptionAlarmMessageMarkdownHandler extends BaseAlarmMessageMarkdownHandler<EasyMsExceptionAlarmTraceInfo> {

    @Override
    String getDetailMessage(EasyMsExceptionAlarmTraceInfo alarmTraceInfo) {
        return "\n\n**异常信息:** " +
                "\n> - 异常类型:     \n" + alarmTraceInfo.getExceptionClassName() +
                "\n> - 异常信息:     \n" + transferMarkdownCharacter(alarmTraceInfo.getExceptionMessage()) +
                "\n> - 异常位置:     \n" +
                transferMarkdownCharacter(alarmTraceInfo.getFirstExceptionStack());
    }

    static ExceptionAlarmMessageMarkdownHandler getInstance() {
        return ExceptionAlarmMessageMarkdownHandlerHolder.INSTANCE;
    }

    private ExceptionAlarmMessageMarkdownHandler() {}

    private static class ExceptionAlarmMessageMarkdownHandlerHolder {

        private static final ExceptionAlarmMessageMarkdownHandler INSTANCE = new ExceptionAlarmMessageMarkdownHandler();

    }
}
