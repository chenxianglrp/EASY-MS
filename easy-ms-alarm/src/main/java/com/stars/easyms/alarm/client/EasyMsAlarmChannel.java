package com.stars.easyms.alarm.client;

import java.util.LinkedHashMap;

/**
 * <p>className: EasyMsAlarmChannel</p>
 * <p>description: 告警渠道接口</p>
 *
 * @author guoguifang
 * @version 1.6.1
 * @date 2020/8/22 2:22 下午
 */
public interface EasyMsAlarmChannel {

    /**
     * 获取渠道类型
     *
     * @return 渠道类型
     */
    String getChannelType();

    /**
     * 获取渠道客户端信息
     *
     * @return 渠道客户端信息
     */
    LinkedHashMap<String, EasyMsAlarmClient> getChannelClientInfo();

    /**
     * 初始化客户端并返回客户端是否有效
     *
     * @return 是否有效
     */
    boolean isValid();
}
