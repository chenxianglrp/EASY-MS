package com.stars.easyms.alarm.pointcut;

import com.stars.easyms.alarm.aspect.EasyMsAlarmAspect;
import com.stars.easyms.base.annotation.EasyMsAlarm;
import com.stars.easyms.base.pointcut.EasyMsAnnotationMatchingPointcut;
import org.aopalliance.aop.Advice;
import org.springframework.aop.Pointcut;
import org.springframework.aop.support.AbstractPointcutAdvisor;
import org.springframework.core.Ordered;
import org.springframework.util.ObjectUtils;

/**
 * <p>className: EasyMsAlarmMethodPointcutAdvisor</p>
 * <p>description: EasyMsAlarm的方法上注解适配器</p>
 *
 * @author guoguifang
 * @version 1.7.2
 * @date 2021/1/19 3:20 下午
 */
public class EasyMsAlarmMethodPointcutAdvisor extends AbstractPointcutAdvisor {

    private transient EasyMsAlarmAspect advice;

    private transient EasyMsAnnotationMatchingPointcut pointcut;

    public EasyMsAlarmMethodPointcutAdvisor() {
        this.advice = new EasyMsAlarmAspect();
        this.pointcut = new EasyMsAnnotationMatchingPointcut();
        this.pointcut.setMethodMatcher(EasyMsAlarm.class, true);
        this.setOrder(Ordered.HIGHEST_PRECEDENCE);
    }

    @Override
    public Pointcut getPointcut() {
        return this.pointcut;
    }

    @Override
    public Advice getAdvice() {
        return this.advice;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || this.getClass() != other.getClass()) {
            return false;
        }
        EasyMsAlarmMethodPointcutAdvisor otherAdvisor = (EasyMsAlarmMethodPointcutAdvisor) other;
        return (ObjectUtils.nullSafeEquals(this.advice, otherAdvisor.advice) &&
                ObjectUtils.nullSafeEquals(this.pointcut, otherAdvisor.pointcut));
    }

    @Override
    public int hashCode() {
        return 41 + this.advice.hashCode() * 41 + this.pointcut.hashCode() * 41;
    }

}
