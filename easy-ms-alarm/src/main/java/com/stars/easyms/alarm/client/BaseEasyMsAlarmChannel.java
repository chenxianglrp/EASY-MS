package com.stars.easyms.alarm.client;

import com.stars.easyms.alarm.properties.EasyMsAlarmProperties;
import com.stars.easyms.base.constant.EasyMsCommonConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedHashMap;

/**
 * <p>className: BaseEasyMsAlarmChannel</p>
 * <p>description: 告警渠道基础类</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/11/26 2:09 下午
 */
public abstract class BaseEasyMsAlarmChannel implements EasyMsAlarmChannel {
    
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    
    private final LinkedHashMap<String, EasyMsAlarmClient> alarmClientMap = new LinkedHashMap<>(8);
    
    protected BaseEasyMsAlarmChannel(EasyMsAlarmProperties alarmProperties) {
        this.init(alarmProperties);
    }
    
    @Override
    public LinkedHashMap<String, EasyMsAlarmClient> getChannelClientInfo() {
        return alarmClientMap;
    }
    
    @Override
    public boolean isValid() {
        return !alarmClientMap.isEmpty();
    }
    
    protected void addDefaultAlarmClient(EasyMsAlarmClient alarmClient) {
        addAlarmClient(EasyMsCommonConstants.DEFAULT, alarmClient);
    }
    
    protected void addAlarmClient(String id, EasyMsAlarmClient alarmClient) {
        alarmClientMap.put(id, alarmClient);
        logger.info("Easy-ms alarm channel '{}' add client with id '{}'.", getChannelType(), id);
    }
    
    protected abstract void init(EasyMsAlarmProperties alarmProperties);
}
