package com.stars.easyms.alarm.client;

import com.stars.easyms.alarm.message.EasyMsAlarmMessage;

import java.util.LinkedHashMap;

/**
 * <p>className: EasyMsAlarmClient</p>
 * <p>description: EasyMs告警客户端</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/11/26 2:21 下午
 */
public interface EasyMsAlarmClient {

    /**
     * 获取告警渠道
     *
     * @return 告警渠道
     */
    EasyMsAlarmChannel getAlarmChannel();

    /**
     * 获取告警客户端ID
     *
     * @return 客户端ID
     */
    String getClientId();

    /**
     * 获取告警客户端信息
     *
     * @return 客户端信息
     */
    LinkedHashMap<String, String> getClientInfo();

    /**
     * 发送告警信息，使用markdown格式
     *
     * @param alarmMessage 告警信息
     * @throws Exception 发送异常
     */
    void sendAlarmMessage(EasyMsAlarmMessage alarmMessage) throws Exception;
}
