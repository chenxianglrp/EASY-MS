package com.stars.easyms.alarm.listener;

import com.stars.easyms.alarm.client.EasyMsAlarmClientFactory;
import com.stars.easyms.alarm.properties.EasyMsAlarmProperties;
import com.stars.easyms.base.event.EasyMsConfigChangeEvent;
import com.stars.easyms.base.listener.EasyMsConfigChangeListener;

/**
 * <p>className: EasyMsAlarmConfigChangeListener</p>
 * <p>description: EasyMs的告警配置修改监听类</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/11/25 3:26 下午
 */
public class EasyMsAlarmConfigChangeListener implements EasyMsConfigChangeListener {

    @Override
    public void listen(EasyMsConfigChangeEvent configChangeEvent) {
        EasyMsAlarmClientFactory.init(getProperties(EasyMsAlarmProperties.class));
    }

    @Override
    public String listenConfigPrefix() {
        return EasyMsAlarmProperties.PREFIX;
    }

}
