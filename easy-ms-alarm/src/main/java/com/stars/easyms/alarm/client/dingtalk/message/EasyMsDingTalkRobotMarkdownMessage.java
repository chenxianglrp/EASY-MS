package com.stars.easyms.alarm.client.dingtalk.message;

import lombok.Getter;

/**
 * <p>className: EasyMsDingTalkRobotMarkdownMessage</p>
 * <p>description: EasyMs钉钉告警信息类，使用markdown模式</p>
 *
 * @author guoguifang
 * @version 1.6.1
 * @date 2020/8/24 10:56 上午
 */
@Getter
public final class EasyMsDingTalkRobotMarkdownMessage extends EasyMsDingTalkRobotMessage {

    /**
     * 固定使用markdown
     */
    private final String msgtype = "markdown";

    private final Markdown markdown = new Markdown();

    public void setTitle(String title) {
        this.markdown.title = title;
    }

    public void setText(String text) {
        this.markdown.text = text;
    }

    @Getter
    private static class Markdown {

        /**
         * 首屏会话透出的展示内容
         */
        private String title;

        /**
         * markdown格式的消息
         */
        private String text;

    }

}
