package com.stars.easyms.alarm.exception;

import com.stars.easyms.base.util.ExceptionUtil;
import com.stars.easyms.base.util.MessageFormatUtil;

/**
 * <p>className: EasyMsAlarmRuntimeException</p>
 * <p>description: EasyMs告警运行时异常</p>
 *
 * @author guoguifang
 * @version 1.6.1
 * @date 2020/8/24 11:53 上午
 */
public final class EasyMsAlarmRuntimeException extends RuntimeException {

    private static final long serialVersionUID = -1034892240245468631L;

    public EasyMsAlarmRuntimeException(String message, Object... args) {
        super(MessageFormatUtil.format(message, ExceptionUtil.trimmedCopy(args)), ExceptionUtil.extractThrowable(args));
    }
}
