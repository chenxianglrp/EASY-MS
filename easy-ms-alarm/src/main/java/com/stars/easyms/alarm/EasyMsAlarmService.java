package com.stars.easyms.alarm;

import com.stars.easyms.alarm.client.EasyMsAlarmClientFactory;
import com.stars.easyms.alarm.message.EasyMsExceptionAlarmMessage;
import com.stars.easyms.alarm.message.EasyMsNormalAlarmMessage;
import com.stars.easyms.alarm.properties.EasyMsAlarmProperties;
import com.stars.easyms.alarm.service.EasyMsAsynAlarmService;
import com.stars.easyms.alarm.util.EasyMsAlarmUtil;
import com.stars.easyms.base.util.ExceptionUtil;
import com.stars.easyms.base.util.JsonUtil;
import org.apache.commons.lang3.StringUtils;

/**
 * <p>className: EasyMsAlarmService</p>
 * <p>description: 告警发送类</p>
 *
 * @author caotieshuan
 * @version 1.5.0
 * @date 2019-12-18 16:37
 */
public class EasyMsAlarmService {

    private final AlarmInterceptor alarmInterceptor;

    private final EasyMsAsynAlarmService easyMsAsynAlarmService;

    private final EasyMsAlarmProperties alarmProperties;

    public EasyMsAlarmService(AlarmInterceptor alarmInterceptor, EasyMsAlarmProperties alarmProperties) {
        this.alarmInterceptor = alarmInterceptor;
        this.alarmProperties = alarmProperties;
        this.easyMsAsynAlarmService = new EasyMsAsynAlarmService();
    }

    public boolean canAlarm() {
        return alarmInterceptor != null || EasyMsAlarmClientFactory.hasValidChannel();
    }

    public void sendExceptionAlarmMessage(EasyMsExceptionAlarmMessage exceptionAlarmMessage) {
        // 为了防止重复发送，当本地可以告警的情况下不再发送到拦截器中
        if (EasyMsAlarmClientFactory.hasValidChannel()) {
            easyMsAsynAlarmService.add(exceptionAlarmMessage);
        } else if (alarmInterceptor != null) {
            alarmInterceptor.sendExceptionAlarmMessage(exceptionAlarmMessage);
        }
    }

    public void sendNormalAlarmMessage(EasyMsNormalAlarmMessage normalAlarmMessage) {
        // 为了防止重复发送，当本地可以告警的情况下不再发送到拦截器中
        if (EasyMsAlarmClientFactory.hasValidChannel()) {
            easyMsAsynAlarmService.add(normalAlarmMessage);
        } else if (alarmInterceptor != null) {
            alarmInterceptor.sendNormalAlarmMessage(normalAlarmMessage);
        }
    }

    public void sendExceptionAlarmMessage(String clientId, Throwable throwable) {
        boolean isEnableLocalAlarm = EasyMsAlarmClientFactory.hasValidChannel();
        if (alarmInterceptor != null || isEnableLocalAlarm) {
            Throwable exactThrowable = ExceptionUtil.exactThrowable(throwable);
            if (!EasyMsAlarmUtil.needAlarm(exactThrowable)) {
                return;
            }
            EasyMsExceptionAlarmMessage exceptionAlarmMessage = EasyMsAlarmUtil.getExceptionAlarmMessage(exactThrowable);
            if (StringUtils.isNotBlank(clientId)) {
                exceptionAlarmMessage.setClientId(clientId);
            }
            // 为了防止重复发送，当本地可以告警的情况下不再发送到拦截器中
            if (isEnableLocalAlarm) {
                easyMsAsynAlarmService.add(exceptionAlarmMessage);
            } else {
                alarmInterceptor.sendExceptionAlarmMessage(exceptionAlarmMessage);
            }
        }
    }

    public void sendExceptionAlarmMessage(Throwable throwable) {
        this.sendExceptionAlarmMessage(alarmProperties.getDefaultClientId(), throwable);
    }

    public void sendNormalAlarmMessage(String clientId, String message) {
        boolean isHasAlarmInterceptor = alarmInterceptor != null;
        boolean isEnableLocalAlarm = EasyMsAlarmClientFactory.hasValidChannel();
        if (isHasAlarmInterceptor || isEnableLocalAlarm) {
            EasyMsNormalAlarmMessage normalAlarmMessage = EasyMsAlarmUtil.getNormalAlarmMessage(message);
            if (StringUtils.isNotBlank(clientId)) {
                normalAlarmMessage.setClientId(clientId);
            }
            // 为了防止重复发送，当本地可以告警的情况下不再发送到拦截器中
            if (isEnableLocalAlarm) {
                easyMsAsynAlarmService.add(normalAlarmMessage);
            } else {
                alarmInterceptor.sendNormalAlarmMessage(normalAlarmMessage);
            }
        }
    }

    public void sendNormalAlarmMessage(String message) {
        this.sendNormalAlarmMessage(alarmProperties.getDefaultClientId(), message);
    }

    public void sendAlarmMessage(String clientId, Object alarmObj) {
        if (alarmObj instanceof Throwable) {
            this.sendExceptionAlarmMessage(clientId, (Throwable) alarmObj);
        } else if (alarmObj instanceof String) {
            this.sendNormalAlarmMessage(clientId, (String) alarmObj);
        } else if (alarmObj instanceof EasyMsExceptionAlarmMessage) {
            EasyMsExceptionAlarmMessage exceptionAlarmMessage = (EasyMsExceptionAlarmMessage) alarmObj;
            if (StringUtils.isNotBlank(clientId)) {
                exceptionAlarmMessage.setClientId(clientId);
            }
            this.sendExceptionAlarmMessage(exceptionAlarmMessage);
        } else if (alarmObj instanceof EasyMsNormalAlarmMessage) {
            EasyMsNormalAlarmMessage normalAlarmMessage = (EasyMsNormalAlarmMessage) alarmObj;
            if (StringUtils.isNotBlank(clientId)) {
                normalAlarmMessage.setClientId(clientId);
            }
            this.sendNormalAlarmMessage(normalAlarmMessage);
        } else {
            this.sendNormalAlarmMessage(clientId, JsonUtil.toJSONString(alarmObj));
        }
    }

    public void sendAlarmMessage(Object alarmObj) {
        if (alarmObj instanceof EasyMsExceptionAlarmMessage) {
            this.sendExceptionAlarmMessage((EasyMsExceptionAlarmMessage) alarmObj);
        } else if (alarmObj instanceof EasyMsNormalAlarmMessage) {
            this.sendNormalAlarmMessage((EasyMsNormalAlarmMessage) alarmObj);
        } else if (alarmObj instanceof Throwable) {
            this.sendExceptionAlarmMessage(alarmProperties.getDefaultClientId(), (Throwable) alarmObj);
        } else if (alarmObj instanceof String) {
            this.sendNormalAlarmMessage(alarmProperties.getDefaultClientId(), (String) alarmObj);
        } else {
            this.sendNormalAlarmMessage(alarmProperties.getDefaultClientId(), JsonUtil.toJSONString(alarmObj));
        }
    }
}