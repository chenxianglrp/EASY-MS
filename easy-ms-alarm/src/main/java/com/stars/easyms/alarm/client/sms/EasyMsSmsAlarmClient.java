package com.stars.easyms.alarm.client.sms;

import com.stars.easyms.alarm.client.BaseEasyMsAlarmClient;
import com.stars.easyms.alarm.client.EasyMsAlarmChannel;
import com.stars.easyms.alarm.message.EasyMsAlarmMessage;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>className: EasyMsSmsAlarmClient</p>
 * <p>description: EasyMsSms告警客户端</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/11/26 4:29 下午
 */
@Slf4j
public class EasyMsSmsAlarmClient extends BaseEasyMsAlarmClient {

    private String postUrl;

    private String key;

    private String toUser;

    EasyMsSmsAlarmClient(EasyMsAlarmChannel easyMsAlarmChannel, String id, String postUrl, String key, String toUser) {
        super(easyMsAlarmChannel, id);
        this.postUrl = postUrl;
        this.key = key;
        this.toUser = toUser;
    }

    EasyMsSmsAlarmClient(EasyMsAlarmChannel easyMsAlarmChannel, String postUrl, String key, String toUser) {
        this(easyMsAlarmChannel, null, postUrl, key, toUser);
    }

    @Override
    public void sendAlarmMessage(EasyMsAlarmMessage easyMsAlarmMessage) {
        log.info("Sms sendMsg success");
        // todo: 待补充
    }

}
