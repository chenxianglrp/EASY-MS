package com.stars.easyms.alarm.client;

import com.stars.easyms.alarm.client.dingtalk.EasyMsDingTalkAlarmChannel;
import com.stars.easyms.alarm.client.email.EasyMsEmailAlarmChannel;
import com.stars.easyms.alarm.client.sms.EasyMsSmsAlarmChannel;
import com.stars.easyms.alarm.client.wechat.EasyMsWeChatAlarmChannel;
import com.stars.easyms.alarm.properties.EasyMsAlarmProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.util.MultiValueMap;

import java.util.*;

/**
 * <p>className: EasyMsAlarmChannelFactory</p>
 * <p>description: EasyMs的告警渠道工厂类</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/11/25 5:26 下午
 */
@Slf4j
public final class EasyMsAlarmClientFactory {

    /**
     * 从clientId分组
     */
    private static MultiValueMap<String, EasyMsAlarmClient> alarmClients = CollectionUtils.toMultiValueMap(Collections.emptyMap());

    /**
     * 从渠道分组
     */
    private static List<EasyMsAlarmChannel> alarmChannels = Collections.emptyList();

    @NonNull
    public static List<EasyMsAlarmChannel> getAlarmChannelList() {
        return Collections.unmodifiableList(alarmChannels);
    }

    @NonNull
    public static List<EasyMsAlarmClient> getAlarmClientList(String clientId) {
        List<EasyMsAlarmClient> localAlarmClients = alarmClients.get(clientId);
        return localAlarmClients != null ? Collections.unmodifiableList(localAlarmClients) : Collections.emptyList();
    }

    @NonNull
    public static List<String> getAlarmClientIds() {
        return new ArrayList<>(alarmClients.keySet());
    }

    public static boolean hasValidChannel() {
        return !alarmChannels.isEmpty();
    }

    public static void init(EasyMsAlarmProperties alarmProperties) {
        if (alarmProperties == null || !alarmProperties.isEnabled()) {
            log.warn("Easy-ms alarm is not enabled!");
            alarmChannels = Collections.emptyList();
            alarmClients = CollectionUtils.toMultiValueMap(Collections.emptyMap());
            return;
        }

        log.info("Easy-ms alarm initialization begin...");
        
        // 为了防止初始化或者更新时数据不统一，使用copyOnWrite的思想防止更新时出现空list的情况
        List<EasyMsAlarmChannel> localAlarmChannels = new ArrayList<>();
        MultiValueMap<String, EasyMsAlarmClient> localAlarmClients =
                CollectionUtils.toMultiValueMap(new LinkedCaseInsensitiveMap<>(8, Locale.ENGLISH));

        // SMS客户端创建及初始化
        initChannel(localAlarmChannels, localAlarmClients, new EasyMsSmsAlarmChannel(alarmProperties));

        // EMAIL客户端创建及初始化
        initChannel(localAlarmChannels, localAlarmClients, new EasyMsEmailAlarmChannel(alarmProperties));

        // 钉钉客户端创建及初始化
        initChannel(localAlarmChannels, localAlarmClients, new EasyMsDingTalkAlarmChannel(alarmProperties));

        // 企业微信客户端创建及初始化
        initChannel(localAlarmChannels, localAlarmClients, new EasyMsWeChatAlarmChannel(alarmProperties));

        // 替换
        alarmChannels = localAlarmChannels;
        alarmClients = localAlarmClients;
        log.info("Easy-ms alarm initialization end.");
    }

    private static void initChannel(List<EasyMsAlarmChannel> localAlarmChannels,
                                    MultiValueMap<String, EasyMsAlarmClient> localAlarmClients,
                                    EasyMsAlarmChannel alarmChannel) {
        if (alarmChannel.isValid()) {
            localAlarmChannels.add(alarmChannel);
            alarmChannel.getChannelClientInfo().forEach(localAlarmClients::add);
        }
    }

    private EasyMsAlarmClientFactory() {
    }
}
