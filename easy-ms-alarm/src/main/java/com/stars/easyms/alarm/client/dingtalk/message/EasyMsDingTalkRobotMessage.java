package com.stars.easyms.alarm.client.dingtalk.message;

import lombok.Getter;

import java.util.List;

/**
 * <p>className: EasyMsDingTalkRobotMessage</p>
 * <p>description: EasyMs钉钉机器人消息类</p>
 *
 * @author guoguifang
 * @version 1.6.1
 * @date 2020/8/24 2:57 下午
 */
@Getter
public class EasyMsDingTalkRobotMessage {

    private final At at = new At();

    public void setAtMobiles(List<String> atMobiles) {
        this.at.atMobiles = atMobiles;
    }

    public void setAtAll(boolean atAll) {
        this.at.isAtAll = atAll;
    }

    @Getter
    private static class At {

        /**
         * 被@人的手机号（在text内容里要有@手机号）
         */
        private List<String> atMobiles;

        /**
         * 是否@所有人
         */
        private Boolean isAtAll = Boolean.TRUE;
    }
}
