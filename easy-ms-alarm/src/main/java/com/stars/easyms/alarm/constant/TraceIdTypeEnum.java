package com.stars.easyms.alarm.constant;

/**
 * <p>className: TraceIdTypeEnum</p>
 * <p>description: traceId类型枚举</p>
 *
 * @author guoguifang
 * @date 2020/12/19 5:58 下午
 */
public enum TraceIdTypeEnum {

    TRACE_ID("链路ID"),

    REQUEST_ID("请求ID"),

    ASYNC_ID("异步ID"),

    SKYWALKING_TRACE_ID("SW链路ID");

    private String name;

    TraceIdTypeEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
