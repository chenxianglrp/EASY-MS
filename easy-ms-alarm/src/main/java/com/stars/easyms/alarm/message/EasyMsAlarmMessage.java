package com.stars.easyms.alarm.message;

import lombok.Data;
import org.springframework.lang.NonNull;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * <p>className: EasyMsAlarmMessage</p>
 * <p>description: 告警消息体</p>
 *
 * @author guoguifang
 * @version 1.6.1
 * @date 2020/8/22 2:09 下午
 */
@Data
public class EasyMsAlarmMessage {

    /**
     * 指定发送到哪个客户端
     */
    private String clientId;

    /**
     * 消息类型，默认使用markdown
     */
    private EasyMsAlarmMessageType type = EasyMsAlarmMessageType.MARKDOWN;

    /**
     * 首屏会话透出的展示内容
     */
    private String title;

    /**
     * 消息内容
     */
    private String context;

    /**
     * 被@人的手机号
     */
    private final Set<String> atMobiles = new HashSet<>();

    /**
     * 是否@所有人，默认false
     */
    private Boolean atAll = Boolean.TRUE;

    public void addAtMobile(String mobile) {
        this.atMobiles.add(mobile);
    }

    public void setAtMobiles(List<String> atMobiles) {
        this.atMobiles.addAll(atMobiles);
    }

    @NonNull
    public List<String> getAtMobiles() {
        return new ArrayList<>(this.atMobiles);
    }

    public String getContext() {
        if (context == null) {
            context = handleMessageContext();
        }
        return context;
    }

    protected String handleMessageContext() {
        return null;
    }
}
