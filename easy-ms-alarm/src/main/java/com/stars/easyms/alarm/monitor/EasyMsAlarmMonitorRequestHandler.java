package com.stars.easyms.alarm.monitor;

import com.stars.easyms.alarm.client.EasyMsAlarmClientFactory;
import com.stars.easyms.alarm.constant.EasyMsAlarmConstant;
import com.stars.easyms.alarm.properties.EasyMsAlarmProperties;
import com.stars.easyms.base.bean.LazyLoadBean;
import com.stars.easyms.monitor.MonitorRequestHandler;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * <p>className: EasyMsAlarmMonitorRequestHandler</p>
 * <p>description: EasyMs告警模块监控类</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/11/26 11:13 上午
 */
public final class EasyMsAlarmMonitorRequestHandler implements MonitorRequestHandler {
    
    private final LazyLoadBean<EasyMsAlarmProperties> alarmPropertiesLazyLoadBean = new LazyLoadBean<>(
            EasyMsAlarmProperties.class);
    
    @Override
    public Map<String, Object> getMonitorInfo() {
        Map<String, Object> result = getBriefMonitorInfo();
        Map<String, Object> channelMap = new LinkedHashMap<>();
        EasyMsAlarmClientFactory.getAlarmChannelList().forEach(easyMsAlarmChannel -> channelMap
                .put(easyMsAlarmChannel.getChannelType(),
                        new ArrayList<>(easyMsAlarmChannel.getChannelClientInfo().keySet())));
        result.put("channel", channelMap);
        EasyMsAlarmProperties.Trace trace = alarmPropertiesLazyLoadBean.getNonNullBean().getTrace();
        Map<String, Object> traceMap = new LinkedHashMap<>();
        if (trace.isEnabled()) {
            traceMap.put("enabled", true);
            traceMap.put("selector", trace.getSelector());
            traceMap.put("plumeLog", trace.getPlumeLog().getUrl());
            traceMap.put("elk", trace.getElk().getKibanaUrl());
            traceMap.put("skywalking", trace.getSkywalking().getUrl());
        } else {
            traceMap.put("enabled", false);
        }
        result.put("trace", traceMap);
        return result;
    }
    
    @Override
    public Map<String, Object> getBriefMonitorInfo() {
        Map<String, Object> result = new LinkedHashMap<>();
        result.put("enabled", alarmPropertiesLazyLoadBean.getNonNullBean().isEnabled());
        result.put("defaultClientId", alarmPropertiesLazyLoadBean.getNonNullBean().getDefaultClientId());
        return result;
    }
    
    @Override
    public String getModuleName() {
        return EasyMsAlarmConstant.MODULE_NAME;
    }
    
    @Override
    public int getOrder() {
        return 1;
    }
    
}