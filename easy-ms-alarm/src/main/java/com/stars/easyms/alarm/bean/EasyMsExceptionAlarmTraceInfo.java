package com.stars.easyms.alarm.bean;

/**
 * <p>className: EasyMsExceptionTraceInfo</p>
 * <p>description: 异常相关告警信息类</p>
 *
 * @author guoguifang
 * @version 1.6.1
 * @date 2020/8/31 9:34 上午
 */
public class EasyMsExceptionAlarmTraceInfo extends EasyMsAlarmTraceInfo {

    /**
     * 异常类名称
     */
    private String exceptionClassName;

    /**
     * 异常消息
     */
    private String exceptionMessage;

    /**
     * 抛出异常的位置信息
     */
    private String firstExceptionStack;

    public String getExceptionClassName() {
        return exceptionClassName;
    }

    public void setExceptionClassName(String exceptionClassName) {
        this.exceptionClassName = exceptionClassName;
    }

    public String getExceptionMessage() {
        return exceptionMessage;
    }

    public void setExceptionMessage(String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
    }

    public String getFirstExceptionStack() {
        return firstExceptionStack;
    }

    public void setFirstExceptionStack(String firstExceptionStack) {
        this.firstExceptionStack = firstExceptionStack;
    }
}
