package com.stars.easyms.alarm.client.dingtalk.message;

import lombok.Data;

/**
 * <p>className: DingTalkResponse</p>
 * <p>description: 钉钉返回的响应信息</p>
 *
 * @author guoguifang
 * @version 1.6.1
 * @date 2020/8/24 1:39 下午
 */
@Data
public final class DingTalkRobotResponse {

    /**
     * 错误码
     */
    private Integer errcode;

    /**
     * 错误信息
     */
    private String errmsg;
}