package com.stars.easyms.alarm;

import com.stars.easyms.alarm.message.EasyMsExceptionAlarmMessage;
import com.stars.easyms.alarm.message.EasyMsNormalAlarmMessage;

/**
 * <p>className: AlarmInterceptor</p>
 * <p>description: 告警拦截器</p>
 *
 * @author guoguifang
 * @version 1.6.3
 * @date 2020/9/17 6:18 下午
 */
public interface AlarmInterceptor {

    void sendExceptionAlarmMessage(EasyMsExceptionAlarmMessage exceptionAlarmMessage);

    void sendNormalAlarmMessage(EasyMsNormalAlarmMessage normalAlarmMessage);
}
