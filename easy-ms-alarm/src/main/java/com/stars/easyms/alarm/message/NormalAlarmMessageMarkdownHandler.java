package com.stars.easyms.alarm.message;

import com.stars.easyms.alarm.bean.EasyMsNormalAlarmTraceInfo;

/**
 * <p>className: NormalAlarmMessageMarkdownHandler</p>
 * <p>description: 一般告警信息服务类</p>
 *
 * @author guoguifang
 * @version 1.6.3
 * @date 2020/9/18 3:05 下午
 */
final class NormalAlarmMessageMarkdownHandler extends BaseAlarmMessageMarkdownHandler<EasyMsNormalAlarmTraceInfo> {

    @Override
    String getDetailMessage(EasyMsNormalAlarmTraceInfo alarmTraceInfo) {
        return "\n\n**告警信息:** " + transferMarkdownCharacter(alarmTraceInfo.getAlarmMessage());
    }

    static NormalAlarmMessageMarkdownHandler getInstance() {
        return NormalAlarmMessageMarkdownHandlerHolder.INSTANCE;
    }

    private NormalAlarmMessageMarkdownHandler() {}

    private static class NormalAlarmMessageMarkdownHandlerHolder {

        private static final NormalAlarmMessageMarkdownHandler INSTANCE = new NormalAlarmMessageMarkdownHandler();

    }

}
