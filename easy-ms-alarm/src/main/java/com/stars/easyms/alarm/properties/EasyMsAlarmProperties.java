package com.stars.easyms.alarm.properties;

import com.stars.easyms.base.constant.EasyMsCommonConstants;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>className: EasyMsAlarmProperties</p>
 * <p>description: 告警属性配置类</p>
 *
 * @author guoguifang
 * @version 1.6.1
 * @date 2020/8/22 10:35 上午
 */
@Data
@ConfigurationProperties(prefix = EasyMsAlarmProperties.PREFIX)
public class EasyMsAlarmProperties {

    public static final String PREFIX = EasyMsCommonConstants.EASY_MS_PROPERTIES_PREFIX + "alarm";

    /**
     * 是否启用告警，默认为false
     */
    private boolean enabled;

    /**
     * 当不指定clientId时使用的默认的clientId,如果不配置则默认使用"default"
     */
    private String defaultClientId = EasyMsCommonConstants.DEFAULT;

    private Email email = new Email();

    private Sms sms = new Sms();

    private WeChat weChat = new WeChat();

    private DingTalk dingTalk = new DingTalk();

    private Trace trace = new Trace();

    /**
     * <p>className: Email</p>
     * <p>description: 邮件参数配置</p>
     *
     * @author caotieshuan
     * @version 1.5.0
     * @date 2019-12-18 16:37
     */
    @Data
    public static class Email {

        /**
         * 是否激活
         */
        private boolean enabled;

        /**
         * 邮件服务器地址，如：smtp.163.com
         */
        private String host;

        /**
         * 端口，如：25
         */
        private String port;

        /**
         * ssl 端口
         */
        private String sslPort;

        /**
         * 发送邮件的帐号
         */
        private String fromUser;

        /**
         * 发送邮件的token
         */
        private String token;

        /**
         * 发给谁,多个以逗号隔开，如：cts@136.com,7552456@qq.com
         */
        private String toUser;

        /**
         * 若有多个机器人时使用
         */
        private List<EmailClientDefinition> clients = new ArrayList<>();

        @Data
        public static class EmailClientDefinition {

            /**
             * 机器人的ID，用来区分不同的机器人，可以向指定相应ID的机器人发送消息
             */
            private String id;

            /**
             * 邮件服务器地址，如：smtp.163.com
             */
            private String host;

            /**
             * 端口，如：25
             */
            private String port;

            /**
             * ssl 端口
             */
            private String sslPort;

            /**
             * 发送邮件的帐号
             */
            private String fromUser;

            /**
             * 发送邮件的token
             */
            private String token;

            /**
             * 发给谁,多个以逗号隔开，如：cts@136.com,7552456@qq.com
             */
            private String toUser;
        }
    }

    /**
     * <p>className: Sms</p>
     * <p>description:短信参数配置</p>
     *
     * @author caotieshuan
     * @version 1.5.0
     * @date 2019-12-18 16:37
     */
    @Data
    public static class Sms {

        /**
         * 是否激活
         */
        private boolean enabled;

        /**
         * 请求发送地址
         */
        private String postUrl;

        /**
         * 密钥
         */
        private String key;

        /**
         * 发给谁,多个以逗号隔开，如：13811121212
         */
        private String toUser;

        /**
         * 若有多个机器人时使用
         */
        private List<SmsClientDefinition> clients = new ArrayList<>();

        @Data
        public static class SmsClientDefinition {

            /**
             * 机器人的ID，用来区分不同的机器人，可以向指定相应ID的机器人发送消息
             */
            private String id;

            /**
             * 请求发送地址
             */
            private String postUrl;

            /**
             * 密钥
             */
            private String key;

            /**
             * 发给谁,多个以逗号隔开，如：13811121212
             */
            private String toUser;
        }
    }

    /**
     * <p>className: WeChat</p>
     * <p>description: 企业微信参数配置</p>
     *
     * @author caotieshuan
     * @version 1.5.0
     * @date 2019-12-18 16:37
     */
    @Data
    public static class WeChat {

        /**
         * 是否激活
         */
        private boolean enabled;

        /**
         * 企业微信发送地址,多个以逗号隔开，如：https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=b3fd73ea-d226-4866-84f2-756bac9512bb
         */
        private String robotUrl;

        /**
         * 若有多个机器人时使用
         */
        private List<WeChatClientDefinition> clients = new ArrayList<>();

        @Data
        public static class WeChatClientDefinition {

            /**
             * 机器人的ID，用来区分不同的机器人，可以向指定相应ID的机器人发送消息
             */
            private String id;

            /**
             * 企业微信发送地址,多个以逗号隔开，如：https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=b3fd73ea-d226-4866-84f2-756bac9512bb
             */
            private String robotUrl;
        }
    }

    /**
     * <p>className: DingTalk</p>
     * <p>description: 钉钉属性参数配置类</p>
     *
     * @author guoguifang
     * @version 1.6.1
     * @date 2020/8/22 1:44 下午
     */
    @Data
    public static class DingTalk {

        /**
         * 是否激活
         */
        private boolean enabled;

        /**
         * 默认机器人的地址
         */
        private String robotUrl;

        /**
         * 秘钥
         */
        private String robotSecret;

        /**
         * 若有多个机器人时使用
         */
        private List<DingTalkClientDefinition> clients = new ArrayList<>();

        @Data
        public static class DingTalkClientDefinition {

            /**
             * 机器人的ID，用来区分不同的机器人，可以向指定相应ID的机器人发送消息
             */
            private String id;

            /**
             * 机器人的地址，需要完整路径，包括access_token
             */
            private String robotUrl;

            /**
             * 秘钥
             */
            private String robotSecret;
        }
    }

    @Data
    public static class Trace {

        /**
         * 是否启用,默认为true
         */
        private boolean enabled = true;

        /**
         * 选择器：elk、plumeLog, 如果为空则默认按照plumeLog、elk的顺序读取
         */
        private String selector;

        private PlumeLog plumeLog = new PlumeLog();

        private Elk elk = new Elk();

        private Skywalking skywalking = new Skywalking();

        /**
         * <p>className: PlumeLog</p>
         * <p>description: PlumeLog相关配置，优先级高于ELK</p>
         *
         * @author guoguifang
         * @version 1.7.0
         * @date 2020/11/25 5:59 下午
         */
        @Data
        public static class PlumeLog {

            /**
             * plume-log的地址，根据plume-log地址自动链接到elk查询
             */
            private String url;

        }

        /**
         * <p>className: Elk</p>
         * <p>description: Elk相关配置类</p>
         *
         * @author guoguifang
         * @version 1.6.1
         * @date 2020/8/24 5:57 下午
         */
        @Data
        public static class Elk {

            /**
             * elk-kibana地址，根据elk-kibana地址自动链接到elk查询
             */
            private String kibanaUrl;

            /**
             * 展示哪些列
             */
            private String columns;

            /**
             * 全局index，用于自动查询全链路日志
             */
            private String globalIndex;

            /**
             * 本系统的index，用于自动查询本系统日志
             */
            private String index;

            /**
             * 当使用告警微服务时，统一的所有需要告警的serviceId对应的index
             */
            private Map<String, String> indexs = new HashMap<>(32);

        }

        /**
         * <p>className: Skywalking</p>
         * <p>description: Skywalking相关配置</p>
         *
         * @author guoguifang
         * @version 1.8.0
         * @date 2021/4/10 10:56 上午
         */
        @Data
        public static class Skywalking {

            /**
             * skywalking的地址
             */
            private String url;
        }

    }

}
