package com.stars.easyms.alarm.message;

import com.stars.easyms.alarm.bean.EasyMsNormalAlarmTraceInfo;

/**
 * <p>className: EasyMsExceptionAlarmMessage</p>
 * <p>description: 异常情况发生的告警消息</p>
 *
 * @author guoguifang
 * @version 1.6.1
 * @date 2020/8/31 9:54 上午
 */
public class EasyMsNormalAlarmMessage extends EasyMsAlarmMessage {

    private EasyMsNormalAlarmTraceInfo normalAlarmTraceInfo;

    private static final String TITLE = "消息告警";

    public EasyMsNormalAlarmMessage() {
        this.setTitle(TITLE);
    }

    @Override
    protected String handleMessageContext() {
        if (normalAlarmTraceInfo != null) {
            return NormalAlarmMessageMarkdownHandler
                    .getInstance()
                    .handleContext(this, normalAlarmTraceInfo);
        }
        return null;
    }

    public void setNormalAlarmTraceInfo(EasyMsNormalAlarmTraceInfo normalAlarmTraceInfo) {
        this.normalAlarmTraceInfo = normalAlarmTraceInfo;
    }

    public EasyMsNormalAlarmTraceInfo getNormalAlarmTraceInfo() {
        return normalAlarmTraceInfo;
    }

}
