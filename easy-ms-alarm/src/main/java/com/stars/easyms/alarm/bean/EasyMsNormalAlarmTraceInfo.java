package com.stars.easyms.alarm.bean;

/**
 * <p>className: EasyMsNormalAlarmTraceInfo</p>
 * <p>description: EasyMs普通告警类trace信息</p>
 *
 * @author guoguifang
 * @version 1.6.3
 * @date 2020/9/18 4:22 下午
 */
public class EasyMsNormalAlarmTraceInfo extends EasyMsAlarmTraceInfo {

    private String alarmMessage;

    public String getAlarmMessage() {
        return alarmMessage;
    }

    public void setAlarmMessage(String alarmMessage) {
        this.alarmMessage = alarmMessage;
    }
}
