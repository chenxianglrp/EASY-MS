package com.stars.easyms.alarm.client.dingtalk.message;

import lombok.Getter;

/**
 * <p>className: EasyMsDingTalkRobotTextMessage</p>
 * <p>description: EasyMs钉钉告警信息类，使用text模式</p>
 *
 * @author guoguifang
 * @version 1.6.1
 * @date 2020/8/24 10:56 上午
 */
@Getter
public final class EasyMsDingTalkRobotTextMessage extends EasyMsDingTalkRobotMessage {

    /**
     * 固定使用markdown
     */
    private final String msgtype = "text";

    private final Text text = new Text();

    public void setContent(String content) {
        this.text.content = content;
    }

    @Getter
    private static class Text {

        /**
         * 消息内容
         */
        private String content;

    }

}
