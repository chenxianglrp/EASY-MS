package com.stars.easyms.alarm.message;

/**
 * <p>className: EasyMsAlarmMessageType</p>
 * <p>description: 发送告警信息类型枚举类</p>
 *
 * @author guoguifang
 * @version 1.6.1
 * @date 2020/8/24 3:21 下午
 */
public enum EasyMsAlarmMessageType {

    /**
     * 字符串格式
     */
    TEXT(1),

    /**
     * markdown格式
     */
    MARKDOWN(2);

    private int code;

    EasyMsAlarmMessageType(int code) {
        this.code = code;
    }

    public static EasyMsAlarmMessageType of(int code) {
        if (code == 1) {
            return TEXT;
        }
        return MARKDOWN;
    }

}
