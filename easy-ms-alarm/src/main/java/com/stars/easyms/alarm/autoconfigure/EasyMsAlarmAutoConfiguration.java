package com.stars.easyms.alarm.autoconfigure;

import com.stars.easyms.alarm.AlarmInterceptor;
import com.stars.easyms.alarm.EasyMsAlarmService;
import com.stars.easyms.alarm.client.EasyMsAlarmClientFactory;
import com.stars.easyms.alarm.pointcut.EasyMsAlarmMethodPointcutAdvisor;
import com.stars.easyms.alarm.pointcut.EasyMsAlarmTypePointcutAdvisor;
import com.stars.easyms.alarm.properties.EasyMsAlarmProperties;
import com.stars.easyms.alarm.service.EasyMsAlarmSendMessageServiceImpl;
import com.stars.easyms.base.alarm.EasyMsAlarmAssistor;
import com.stars.easyms.base.alarm.SendAlarmMessageService;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * <p>className: EasyMsAlarmAutoConfiguration</p>
 * <p>description: 告警自动配置类</p>
 *
 * @author caotieshuan
 * @version 1.5.0
 * @date 2019-12-19 16:22
 */
@Configuration
@EnableConfigurationProperties(EasyMsAlarmProperties.class)
public class EasyMsAlarmAutoConfiguration {
    
    private final EasyMsAlarmProperties alarmProperties;
    
    private final AlarmInterceptor alarmInterceptor;
    
    public EasyMsAlarmAutoConfiguration(EasyMsAlarmProperties alarmProperties,
            ObjectProvider<AlarmInterceptor> alarmInterceptors) {
        this.alarmProperties = alarmProperties;
        this.alarmInterceptor = alarmInterceptors.getIfAvailable();
    }
    
    @Bean
    public EasyMsAlarmService easyMsAlarmService() {
        return new EasyMsAlarmService(alarmInterceptor, alarmProperties);
    }
    
    @Bean
    @ConditionalOnMissingBean
    public SendAlarmMessageService easyMsAlarmSendMessageServiceImpl(EasyMsAlarmService easyMsAlarmService) {
        return new EasyMsAlarmSendMessageServiceImpl(easyMsAlarmService);
    }
    
    @Bean
    public EasyMsAlarmTypePointcutAdvisor easyMsAlarmTypePointcutAdvisor(
            SendAlarmMessageService sendAlarmMessageService) {
        EasyMsAlarmAssistor.setSendAlarmMessageService(sendAlarmMessageService);
        return new EasyMsAlarmTypePointcutAdvisor();
    }
    
    @Bean
    public EasyMsAlarmMethodPointcutAdvisor easyMsAlarmMethodPointcutAdvisor() {
        return new EasyMsAlarmMethodPointcutAdvisor();
    }
    
    @PostConstruct
    private void init() {
        EasyMsAlarmClientFactory.init(alarmProperties);
    }
    
}