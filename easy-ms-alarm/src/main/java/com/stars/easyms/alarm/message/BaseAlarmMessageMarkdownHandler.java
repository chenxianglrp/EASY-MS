package com.stars.easyms.alarm.message;

import com.stars.easyms.alarm.bean.EasyMsAlarmTraceInfo;
import com.stars.easyms.alarm.constant.TraceIdTypeEnum;
import com.stars.easyms.alarm.properties.EasyMsAlarmProperties;
import com.stars.easyms.base.util.ApplicationContextHolder;
import com.stars.easyms.base.util.DateTimeUtil;
import org.apache.commons.lang3.StringUtils;

/**
 * <p>className: BaseAlarmMessageMarkdownHandler</p>
 * <p>description: 告警信息markdown处理基础类</p>
 *
 * @author guoguifang
 * @version 1.6.3
 * @date 2020/9/18 3:07 下午
 */
abstract class BaseAlarmMessageMarkdownHandler<T extends EasyMsAlarmTraceInfo> {

    private EasyMsAlarmProperties easyMsAlarmProperties;

    BaseAlarmMessageMarkdownHandler() {
        this.easyMsAlarmProperties = ApplicationContextHolder.getApplicationContext().getBean(EasyMsAlarmProperties.class);
    }

    String handleContext(EasyMsAlarmMessage easyMsAlarmMessage, T traceInfo) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("## ").append(easyMsAlarmMessage.getTitle());
        appendRequestInfo(stringBuilder, traceInfo);
        appendAlarmTime(stringBuilder, traceInfo);
        stringBuilder.append(getDetailMessage(traceInfo));

        // 添加traceId、requestId、asyncId、skywalkingId
        boolean isPlumeLog = !"elk".equalsIgnoreCase(easyMsAlarmProperties.getTrace().getSelector());
        appendTraceIds(stringBuilder, traceInfo, isPlumeLog, TraceIdTypeEnum.TRACE_ID);
        appendTraceIds(stringBuilder, traceInfo, isPlumeLog, TraceIdTypeEnum.REQUEST_ID);
        appendTraceIds(stringBuilder, traceInfo, isPlumeLog, TraceIdTypeEnum.ASYNC_ID);
        appendTraceIds(stringBuilder, traceInfo, isPlumeLog, TraceIdTypeEnum.SKYWALKING_TRACE_ID);
        return stringBuilder.toString();
    }

    String transferMarkdownCharacter(String sourceStr) {
        if (sourceStr == null) {
            return "";
        }
        return sourceStr.replace("#", "\\#")
                .replace("\\r\\n", "  \\r\\n");
    }

    private void appendRequestInfo(StringBuilder stringBuilder, EasyMsAlarmTraceInfo traceInfo) {
        stringBuilder.append("\n\n**告警系统:** ").append(traceInfo.getApplicationName());
        if (traceInfo.getEnv() != null) {
            stringBuilder.append("\n\n**告警环境:** ").append(traceInfo.getEnv());
        }
        if (StringUtils.isNotBlank(traceInfo.getRequestSys())) {
            stringBuilder.append("\n\n**请求系统:** ").append(traceInfo.getRequestSys());
        }
        if (StringUtils.isNotBlank(traceInfo.getRequestPath())) {
            stringBuilder.append("\n\n**请求地址:** ").append(traceInfo.getRequestPath());
        }
    }

    private void appendAlarmTime(StringBuilder stringBuilder, EasyMsAlarmTraceInfo traceInfo) {
        long alarmTime = traceInfo.getAlarmTime();
        String alarmLocalDateTimeStr = DateTimeUtil.getDatetimeNormalStrWithMills(alarmTime);
        stringBuilder.append("\n\n**告警时间:** ").append(alarmLocalDateTimeStr);
        String alarmBeijingDataTimeStr = DateTimeUtil.getBeijingDateTimeNormalStrWithMills(alarmTime);
        if (!alarmBeijingDataTimeStr.equals(alarmLocalDateTimeStr)) {
            stringBuilder.append("\n\n**北京时间:** ").append(alarmBeijingDataTimeStr);
        }
    }

    abstract String getDetailMessage(T alarmTraceInfo);

    private void appendTraceIds(StringBuilder stringBuilder, EasyMsAlarmTraceInfo traceInfo, boolean isPlumeLog, TraceIdTypeEnum traceIdTypeEnum) {
        EasyMsAlarmProperties.Trace trace = easyMsAlarmProperties.getTrace();
        boolean traceEnabled = trace.isEnabled()
                && StringUtils.isNotBlank(TraceIdTypeEnum.SKYWALKING_TRACE_ID == traceIdTypeEnum ? trace.getSkywalking().getUrl()
                : isPlumeLog ? trace.getPlumeLog().getUrl() : trace.getElk().getKibanaUrl());
        appendTraceId(stringBuilder, traceInfo, traceIdTypeEnum, traceEnabled, isPlumeLog);
    }

    private void appendTraceId(StringBuilder stringBuilder, EasyMsAlarmTraceInfo traceInfo, TraceIdTypeEnum traceIdTypeEnum,
                               boolean traceEnabled, boolean isPlumeLog) {
        String oneId = getOneId(traceInfo, traceIdTypeEnum);
        if (StringUtils.isNotBlank(oneId)) {
            if (traceEnabled) {
                stringBuilder.append("\n\n**")
                        .append(traceIdTypeEnum.getName())
                        .append(":** [")
                        .append(oneId)
                        .append("](")
                        .append(getQueryUrl(traceInfo, oneId, isPlumeLog, traceIdTypeEnum))
                        .append(")");
            } else {
                stringBuilder.append("\n\n**")
                        .append(traceIdTypeEnum.getName())
                        .append(":** ")
                        .append(oneId);
            }
        }
    }

    private String getOneId(EasyMsAlarmTraceInfo traceInfo, TraceIdTypeEnum traceIdTypeEnum) {
        switch (traceIdTypeEnum) {
            case TRACE_ID:
                return traceInfo.getTraceId();
            case REQUEST_ID:
                return traceInfo.getRequestId();
            case ASYNC_ID:
                return traceInfo.getAsyncId();
            case SKYWALKING_TRACE_ID:
                return traceInfo.getSkywalkingTraceId();
            default:
        }
        return null;
    }

    private String getQueryUrl(EasyMsAlarmTraceInfo traceInfo, String oneId, boolean isPlumeLog, TraceIdTypeEnum traceIdType) {
        if (traceIdType == TraceIdTypeEnum.SKYWALKING_TRACE_ID) {
            return getSkywalkingQueryUrl(oneId);
        }
        if (isPlumeLog) {
            return getPlumeLogQueryUrl(traceInfo, oneId, traceIdType);
        }
        return getFullKibanaQueryUrl(traceInfo, oneId, traceIdType);
    }

    private String getSkywalkingQueryUrl(String skywalkingId) {
        return getFormatUrl(easyMsAlarmProperties.getTrace().getSkywalking().getUrl()) + "trace?traceid=" + skywalkingId;
    }

    private String getPlumeLogQueryUrl(EasyMsAlarmTraceInfo traceInfo, String oneId, TraceIdTypeEnum traceIdType) {
        String plumeLogUrl = easyMsAlarmProperties.getTrace().getPlumeLog().getUrl();
        if (TraceIdTypeEnum.TRACE_ID != traceIdType) {
            return getFormatUrl(plumeLogUrl)
                    + "#/?traceId=" + traceInfo.getTraceId() + "&searchKey=" + oneId
                    + "&time=" + (System.currentTimeMillis() - 86400000) + "," + (System.currentTimeMillis() + 3600000);
        }
        return getFormatUrl(plumeLogUrl)
                + "#/?traceId=" + oneId
                + "&time=" + (System.currentTimeMillis() - 86400000) + "," + (System.currentTimeMillis() + 3600000);
    }

    private String getFullKibanaQueryUrl(EasyMsAlarmTraceInfo traceInfo, String oneId, TraceIdTypeEnum traceIdType) {
        StringBuilder stringBuilder = new StringBuilder();
        EasyMsAlarmProperties.Trace trace = easyMsAlarmProperties.getTrace();
        String kibanaUrl = trace.getElk().getKibanaUrl();
        if (StringUtils.isNotBlank(kibanaUrl)) {
            long exceptionTime = traceInfo.getAlarmTime();
            String columns = trace.getElk().getColumns();
            stringBuilder.append(getFormatUrl(kibanaUrl))
                    .append("#/discover?_g=(filters:!(),refreshInterval:(pause:!t,value:0),time:(from:'")
                    .append(getKibanaQueryStartTime(exceptionTime))
                    .append("',to:'")
                    .append(getKibanaQueryEndTime(exceptionTime))
                    .append("'))&_a=(columns:!(")
                    .append(StringUtils.isNotBlank(columns) ? columns : "_source")
                    .append("),index:'")
                    .append(getKibanaQueryIndex(traceInfo, traceIdType == TraceIdTypeEnum.TRACE_ID))
                    .append("',interval:auto,query:(language:kuery,query:'")
                    .append(oneId)
                    .append("'),sort:!('@timestamp',asc))");
        }
        return stringBuilder.toString();
    }

    /**
     * 获取kibana查询起始时间，异常发生时间前五分钟开始
     */
    private String getKibanaQueryStartTime(long exceptionTime) {
        return DateTimeUtil.getUTCDateTime(exceptionTime - 300000);
    }

    /**
     * 获取kibana查询结束时间，为保证全链路显示完整，结束时间为异常发生时间后五分钟结束
     */
    private String getKibanaQueryEndTime(long exceptionTime) {
        return DateTimeUtil.getUTCDateTime(exceptionTime + 300000);
    }

    private String getKibanaQueryIndex(EasyMsAlarmTraceInfo traceInfo, boolean isTraceId) {
        EasyMsAlarmProperties.Trace.Elk elk = easyMsAlarmProperties.getTrace().getElk();
        String index = null;
        if (isTraceId) {
            index = elk.getGlobalIndex();
            if (StringUtils.isBlank(index)) {
                index = elk.getIndex();
            }
        } else {
            if (!traceInfo.isUserDefaultElkIndex()) {
                index = elk.getIndexs().get(traceInfo.getApplicationName());
            }
            if (StringUtils.isBlank(index)) {
                index = elk.getIndex();
                if (StringUtils.isBlank(index)) {
                    index = elk.getGlobalIndex();
                }
            }
        }
        return index == null ? "" : index;
    }

    private String getFormatUrl(String url) {
        return url.endsWith("/") ? url : url + "/";
    }
}
