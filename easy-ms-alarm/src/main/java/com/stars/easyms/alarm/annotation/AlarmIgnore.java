package com.stars.easyms.alarm.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 异常类上增加该注解后表示忽略该异常的告警.
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/7/23 2:39 下午
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AlarmIgnore {

}
