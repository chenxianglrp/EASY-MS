package com.stars.easyms.alarm.client.wechat.message;

import com.alibaba.fastjson.JSON;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>className: TextMessage</p>
 * <p>description: 文本类型消息</p>
 *
 * @author caotieshuan
 * @version 1.5.0
 * @date 2019-12-20 14:25
 */
public class TextMessage implements Message {

    /**
     * 文本内容，最长不超过2048个字节，必须是utf8编码
     */
    private String text;
    /**
     * 手机号列表，提醒手机号对应的群成员(@某个成员)，@all表示提醒所有人
     */
    private List<String> mentionedMobileList;
    /**
     * 是否@all
     */
    private boolean isAtAll;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<String> getMentionedMobileList() {
        return mentionedMobileList;
    }

    public void setMentionedMobileList(List<String> mentionedMobileList) {
        this.mentionedMobileList = mentionedMobileList;
    }

    public boolean isAtAll() {
        return isAtAll;
    }

    public void setAtAll(boolean atAll) {
        isAtAll = atAll;
    }

    @Override
    public String toJsonString() {
        Map<String, Object> items = new HashMap<>(8);
        //消息类型，此时固定为text
        items.put("msgtype", "text");

        Map<String, Object> textContent = new HashMap<>(8);
        if (StringUtils.isBlank(text)) {
            throw new IllegalArgumentException("text should not be blank");
        }
        //文本内容，最长不超过2048个字节，必须是utf8编码
        textContent.put("content", text);
        if (isAtAll) {
            if (mentionedMobileList == null) {
                mentionedMobileList = new ArrayList<>();
                mentionedMobileList.add("@all");
            }
        }
        if (mentionedMobileList != null && !mentionedMobileList.isEmpty()) {
            textContent.put("mentioned_mobile_list", mentionedMobileList);
        }
        items.put("text", textContent);
        return JSON.toJSONString(items);
    }

    @Override
    public Map<String, Object> toMessageMap() {
        Map<String, Object> items = new HashMap<>(8);
        //消息类型，此时固定为text
        items.put("msgtype", "text");

        Map<String, Object> textContent = new HashMap<>(8);
        if (StringUtils.isBlank(text)) {
            throw new IllegalArgumentException("text should not be blank");
        }
        //文本内容，最长不超过2048个字节，必须是utf8编码
        textContent.put("content", text);
        if (isAtAll) {
            if (mentionedMobileList == null) {
                mentionedMobileList = new ArrayList<>();
                mentionedMobileList.add("@all");
            }
        }
        if (mentionedMobileList != null && !mentionedMobileList.isEmpty()) {
            textContent.put("mentioned_mobile_list", mentionedMobileList);
        }
        items.put("text", textContent);
        return items;
    }
}