package com.stars.easyms.alarm.client.email;

import com.stars.easyms.alarm.client.BaseEasyMsAlarmChannel;
import com.stars.easyms.alarm.constant.EasyMsAlarmConstant;
import com.stars.easyms.alarm.properties.EasyMsAlarmProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

/**
 * <p>className: EasyMsEmailAlarmChannel</p>
 * <p>description: 邮箱告警</p>
 *
 * @author caotieshuan
 * @version 1.5.0
 * @date 2019-12-18 16:21
 */
@Slf4j
public final class EasyMsEmailAlarmChannel extends BaseEasyMsAlarmChannel {

    public EasyMsEmailAlarmChannel(EasyMsAlarmProperties alarmProperties) {
        super(alarmProperties);
    }

    @Override
    public String getChannelType() {
        return EasyMsAlarmConstant.EMAIL;
    }

    @Override
    protected void init(EasyMsAlarmProperties alarmProperties) {
        EasyMsAlarmProperties.Email emailAlarmProperties = alarmProperties.getEmail();
        if (emailAlarmProperties.isEnabled()) {
            String defaultToUser = emailAlarmProperties.getToUser();
            String defaultHost = emailAlarmProperties.getHost();
            String defaultPort = emailAlarmProperties.getPort();
            String defaultSslPort = emailAlarmProperties.getSslPort();
            String defaultFromUser = emailAlarmProperties.getFromUser();
            String defaultToken = emailAlarmProperties.getToken();
            if (StringUtils.isNotBlank(defaultToUser) && StringUtils.isNotBlank(defaultHost) && StringUtils.isNotBlank(defaultPort)
                    && StringUtils.isNotBlank(defaultFromUser) && StringUtils.isNotBlank(defaultToken)) {
                addDefaultAlarmClient(new EasyMsEmailAlarmClient(this, defaultHost, defaultPort, defaultSslPort,
                        defaultFromUser, defaultToken, defaultToUser));
            }
            emailAlarmProperties.getClients().forEach(d -> {
                String id = d.getId();
                String toUser = d.getToUser();
                String host = d.getHost();
                String port = d.getPort();
                String sslPort = d.getSslPort();
                String fromUser = d.getFromUser();
                String token = d.getToken();
                if (StringUtils.isNotBlank(toUser) && StringUtils.isNotBlank(host) && StringUtils.isNotBlank(port)
                        && StringUtils.isNotBlank(fromUser) && StringUtils.isNotBlank(token)) {
                    addAlarmClient(id, new EasyMsEmailAlarmClient(this, id, host, port, sslPort, fromUser, token, toUser));
                }
            });
        }
    }


}