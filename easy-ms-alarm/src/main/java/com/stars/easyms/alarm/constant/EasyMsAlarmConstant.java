package com.stars.easyms.alarm.constant;

/**
 * <p>className: AlarmConstant</p>
 * <p>description: 告警常量</p>
 *
 * @author caotieshuan
 * @version 1.5.0
 * @date 2019-12-19 10:44
 */
public class EasyMsAlarmConstant {

    /**
     * 模块名称
     */
    public static final String MODULE_NAME = "alarm";

    /**
     * 短信
     */
    public static final String SMS = "Sms";

    /**
     * 邮件
     */
    public static final String EMAIL = "Email";

    /**
     * 企业微信
     */
    public static final String WECHAT = "WeChat";

    /**
     * 钉钉
     */
    public static final String DING_TALK = "DingTalk";

    /**
     * 默认的告警信息title
     */
    public static final String DEFAULT_TITLE = "Easy-ms Alarm Message";

    /**
     * 最大异常信息长度
     */
    public static final int MAX_EXCEPTION_MSG_LENGTH = 500;

    private EasyMsAlarmConstant() {}
}