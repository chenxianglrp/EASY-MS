package com.stars.easyms.alarm.bean;

/**
 * <p>className: EasyMsAlarmTraceInfo</p>
 * <p>description: EasyMs告警trace信息类</p>
 *
 * @author guoguifang
 * @version 1.6.3
 * @date 2020/9/18 4:19 下午
 */
public class EasyMsAlarmTraceInfo {

    /**
     * 系统名称
     */
    private String applicationName;

    /**
     * 环境名称
     */
    private String env;

    /**
     * 上游请求系统
     */
    private String requestSys;

    /**
     * 请求地址
     */
    private String requestPath;

    /**
     * 告警时间
     */
    private long alarmTime;

    /**
     * 请求ID
     */
    private String requestId;

    /**
     * 全链路ID
     */
    private String traceId;

    /**
     * 异步链路ID
     */
    private String asyncId;

    /**
     * skywalking的traceId
     */
    private String skywalkingTraceId;

    /**
     * 是否使用默认elk的index，默认是true
     */
    private boolean userDefaultElkIndex = true;

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }

    public String getRequestSys() {
        return requestSys;
    }

    public void setRequestSys(String requestSys) {
        this.requestSys = requestSys;
    }

    public String getRequestPath() {
        return requestPath;
    }

    public void setRequestPath(String requestPath) {
        this.requestPath = requestPath;
    }

    public long getAlarmTime() {
        return alarmTime;
    }

    public void setAlarmTime(long alarmTime) {
        this.alarmTime = alarmTime;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public String getAsyncId() {
        return asyncId;
    }

    public void setAsyncId(String asyncId) {
        this.asyncId = asyncId;
    }

    public String getSkywalkingTraceId() {
        return skywalkingTraceId;
    }

    public void setSkywalkingTraceId(String skywalkingTraceId) {
        this.skywalkingTraceId = skywalkingTraceId;
    }

    public boolean isUserDefaultElkIndex() {
        return userDefaultElkIndex;
    }

    public void setUserDefaultElkIndex(boolean userDefaultElkIndex) {
        this.userDefaultElkIndex = userDefaultElkIndex;
    }

}
