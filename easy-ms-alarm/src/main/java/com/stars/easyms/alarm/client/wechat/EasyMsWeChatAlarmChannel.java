package com.stars.easyms.alarm.client.wechat;

import com.stars.easyms.alarm.client.BaseEasyMsAlarmChannel;
import com.stars.easyms.alarm.constant.EasyMsAlarmConstant;
import com.stars.easyms.alarm.properties.EasyMsAlarmProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

/**
 * <p>className: EasyMsWeChatAlarmChannel</p>
 * <p>description: 企业微信告警渠道</p>
 *
 * @author caotieshuan
 * @version 1.5.0
 * @date 2019-12-18 16:21
 */
@Slf4j
public class EasyMsWeChatAlarmChannel extends BaseEasyMsAlarmChannel {

    public EasyMsWeChatAlarmChannel(EasyMsAlarmProperties alarmProperties) {
        super(alarmProperties);
    }

    @Override
    public String getChannelType() {
        return EasyMsAlarmConstant.WECHAT;
    }

    @Override
    protected void init(EasyMsAlarmProperties alarmProperties) {
        EasyMsAlarmProperties.WeChat weChatAlarmProperties = alarmProperties.getWeChat();
        if (weChatAlarmProperties.isEnabled()) {
            String defaultRobotUrl = weChatAlarmProperties.getRobotUrl();
            if (StringUtils.isNotBlank(defaultRobotUrl)) {
                addDefaultAlarmClient(new EasyMsWeChatAlarmClient(this, defaultRobotUrl));
            }
            weChatAlarmProperties.getClients().forEach(d -> {
                String id = d.getId();
                String robotUrl = d.getRobotUrl();
                if (!StringUtils.isEmpty(id) && !StringUtils.isEmpty(robotUrl)) {
                    addAlarmClient(id, new EasyMsWeChatAlarmClient(this, id, robotUrl));
                }
            });
        }
    }


}
