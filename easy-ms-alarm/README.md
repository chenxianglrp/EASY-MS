# Easy-Ms-Alarm

　　easy-ms-alarm是一个基于spring的、异步告警信息提示的功能增强型框架，是Easy-Ms的一个组件框架。

## 一、功能特点

1. 支持企业微信、邮件，短信等告警方式；
2. 异步告警，发送失败，多次尝试重发

## 二、依赖注入
#### 1. 使用parent的方式

　　好处：easy-ms-parent里包含了许多的依赖版本管理，可以在依赖管理`<dependencies>`中，有较多常用的引入直接使用默认的版本而无需指定版本号

```xml
    <!-- 可以直接把easy-ms-parent当做parent，或者当引入多个easy-ms组件时使用parent可以只需设置一次版本号 -->
    <parent>
        <groupId>com.stars.easyms</groupId>
        <artifactId>easy-ms-parent</artifactId>
        <version>${easy-ms.version}</version>
    </parent>
		
    <!-- 如果是直接引入easy-ms，则不需要引入easy-ms-alarm，easy-ms已经默认依赖了easy-ms-alarm -->
    <dependencies>
        <dependency>
            <groupId>com.stars.easyms</groupId>
            <artifactId>easy-ms-alarm</artifactId>
        </dependency>
    </dependencies>
```
#### 2. 直接引用

```xml
    <dependencies>
        <dependency>
            <groupId>com.stars.easyms</groupId>
            <artifactId>easy-ms-alarm</artifactId>
            <version>${easy-ms.version}</version>
        </dependency>
    </dependencies>
```

## 三、参数配置

#### 1.邮箱配置
| 参数               | 默认值                        | 说明                                                         |
| ------------------ | ---------------------------- | ------------------------------------------------------------ |
| enabled            | false                        |  是否启用                                                     |
| host               |                              |  邮箱服务器地址，smtp/pop                                      |
| port               |                              |  邮箱服务器端口                                                |
| sslPort            |                              |  邮箱服务器ssl端口                                             |
| fromUser           |                             |   邮箱账户                                                     |
| password           |                              |  邮箱账户密码                                                  |
| toUser             |                             |   接收者，支持多个，以逗号隔开                                    |

#### 2.企业微信配置
| 参数               | 默认值                        | 说明                                                         |
| ------------------ | ---------------------------- | ------------------------------------------------------------ |
| enabled            | false                        |  是否启用                                                     |
| robotUrl            |                             |告警企业微信接口地址                                     |

#### 3.短信配置
| 参数               | 默认值                        | 说明                                                         |
| ------------------ | ---------------------------- | ------------------------------------------------------------ |
| enabled            | false                        |  是否启用                                                     |
| postUrl             |                              |  短信请求地址                                      |
| key                 |                              |  密钥                                                |
| toUser             |                              |   接收者，支持多个，以逗号隔开                                    |

## 四、使用方法

　　使用demo如下：

```java
public class EasyMsAlarmService {

    @Autowired
    private SendAlarmService sendAlarmService;
    @Override
    public BaseOutput execute(AlarmInput input) throws Exception {
        sendAlarmService.sendMsg(input.getTitle(),input.getContent());
        return null;
    }
    
}
```


> 　　我们提供框架的维护和技术支持，提供给感兴趣的开发人员学习和交流的平台，同时希望感兴趣的同学一起加入我们让框架更完善，让我们一起为了给大家提供更好更优秀的框架而努力。
>
> 　　若该框架及本文档中有什么错误及问题的，可采用创建Issue、加群@管理、私聊作者或管理员的方式进行联系，谢谢！


