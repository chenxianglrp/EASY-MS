package com.stars.easyms.jwt.autoconfigure;

import com.stars.easyms.jwt.provider.EasyMsJwtProvider;
import com.stars.easyms.jwt.properties.EasyMsJwtProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <p>className: EasyMsJwtAutoConfiguration</p>
 * <p>description: EasyMs的jwt自定配置类</p>
 *
 * @author guoguifang
 * @version 1.6.1
 * @date 2020/8/26 2:21 下午
 */
@Configuration
@EnableConfigurationProperties(EasyMsJwtProperties.class)
public class EasyMsJwtAutoConfiguration {

    private EasyMsJwtProperties easyMsJwtProperties;

    public EasyMsJwtAutoConfiguration(EasyMsJwtProperties easyMsJwtProperties) {
        this.easyMsJwtProperties = easyMsJwtProperties;
    }

    @Bean
    public EasyMsJwtProvider easyMsJwtProvider() {
        return new EasyMsJwtProvider(easyMsJwtProperties);
    }

}
