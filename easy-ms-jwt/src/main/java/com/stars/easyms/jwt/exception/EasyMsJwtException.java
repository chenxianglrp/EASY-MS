package com.stars.easyms.jwt.exception;

import com.stars.easyms.base.util.ExceptionUtil;
import com.stars.easyms.base.util.MessageFormatUtil;

/**
 * <p>className: EasyMsJwtException</p>
 * <p>description: Jwt相关异常</p>
 *
 * @author guoguifang
 * @version 1.6.1
 * @date 2020/8/26 3:05 下午
 */
public final class EasyMsJwtException extends RuntimeException {

    private static final long serialVersionUID = -2134892190245566631L;

    public EasyMsJwtException(String message, Object... args) {
        super(MessageFormatUtil.format(message, ExceptionUtil.trimmedCopy(args)), ExceptionUtil.extractThrowable(args));
    }
}
