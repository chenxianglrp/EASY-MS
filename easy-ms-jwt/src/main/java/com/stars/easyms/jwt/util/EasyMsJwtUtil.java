package com.stars.easyms.jwt.util;

import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

import javax.crypto.SecretKey;
import java.util.Base64;

/**
 * <p>className: EasyMsJwtUtil</p>
 * <p>description: EasyMsJwt工具类</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/22 6:08 下午
 */
public final class EasyMsJwtUtil {

    /**
     * 生成一个随机的秘钥
     */
    public static SecretKey getRandomSecretKey(SignatureAlgorithm alg) {
        return Keys.secretKeyFor(alg);
    }

    /**
     * 生成一个随机的秘钥
     */
    public static String getRandomSecret(SignatureAlgorithm alg) {
        return Base64.getEncoder().encodeToString(getRandomSecretKey(alg).getEncoded());
    }

    /**
     * 生成一个随机的秘钥
     */
    public static SecretKey getRandomSecretKey() {
        return getRandomSecretKey(SignatureAlgorithm.HS512);
    }

    /**
     * 生成一个随机的秘钥
     */
    public static String getRandomSecret() {
        return Base64.getEncoder().encodeToString(getRandomSecretKey().getEncoded());
    }

    private EasyMsJwtUtil() {}
}
