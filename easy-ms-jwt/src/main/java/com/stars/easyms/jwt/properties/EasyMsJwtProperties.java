package com.stars.easyms.jwt.properties;

import com.stars.easyms.base.constant.EasyMsCommonConstants;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * <p>className: EasyMsJwtProperties</p>
 * <p>description: jwt的属性配置类</p>
 *
 * @author guoguifang
 * @version 1.6.1
 * @date 2020/8/12 15:30
 */
@Data
@ConfigurationProperties(prefix = EasyMsJwtProperties.PREFIX)
public class EasyMsJwtProperties {

    public static final String PREFIX = EasyMsCommonConstants.EASY_MS_PROPERTIES_PREFIX + "jwt";

    /**
     * 是否激活jwt
     */
    private boolean enabled;

    /**
     * 不需要jwt权限控制的url
     */
    private String permit;

    /**
     * 存放在header头的key值，如不填写默认"Authorization"
     */
    private String headerKey = "Authorization";

    /**
     * jwt的发行方
     */
    private String issuer;

    /**
     * jwt的秘钥(明文)
     */
    private String secret;

    /**
     * jwt的秘钥(base64)
     */
    private String base64Secret;

    /**
     * token的有效时间(单位：秒)：默认30分钟
     */
    private long tokenValidityInSeconds = 1800L;

    /**
     * session过期时间(单位：秒)：默认30分钟
     */
    private long sessionValidityInSeconds = 1800L;

    /**
     * token在选了记住我的情况下的会话有效时间(单位：秒)：默认72小时
     */
    private long sessionValidityInSecondsForRememberMe = 2592000L;
}
