package com.stars.easyms.proxy.server.rest;

import com.stars.easyms.rest.scan.EasyMsRestControllerScan;

/**
 * <p>className: ProxyServerRestControllerScan</p>
 * <p>description: 代理RestController扫描实现类</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/31 3:35 下午
 */
public class ProxyServerRestControllerScan implements EasyMsRestControllerScan {

    @Override
    public String[] scanPackages() {
        return new String[]{this.getClass().getPackage().getName()};
    }

}
