package com.stars.easyms.proxy.server.mq;

import com.stars.easyms.mq.annotation.MQManager;
import com.stars.easyms.mq.annotation.MQReceiver;
import com.stars.easyms.proxy.client.local.LocalProxyClient;
import com.stars.easyms.proxy.constant.ProxyConstants;
import com.stars.easyms.proxy.dto.ProxyDTO;

/**
 * <p>className: ProxyServerMqManager</p>
 * <p>description: 代理MQ服务端管理类</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/31 3:18 下午
 */
@MQManager
public class ProxyServerMqManager extends LocalProxyClient {

    @MQReceiver(key = ProxyConstants.PROXY_MQ_KEY, batch = false, maxRetryCount = 0)
    public void receiveProxy(ProxyDTO proxyDTO) {
        proxy(proxyDTO);
    }

}
