package com.stars.easyms.proxy.server.rest.controller;

import com.stars.easyms.proxy.constant.ProxyConstants;
import com.stars.easyms.proxy.dto.ProxyDTO;
import com.stars.easyms.proxy.server.rest.service.ProxyServerRestService;
import com.stars.easyms.rest.annotation.EasyMsRestController;
import com.stars.easyms.rest.annotation.EasyMsRestMapping;

/**
 * <p>className: ProxyServerRestController</p>
 * <p>description: 代理服务端rest controller类</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/31 5:30 下午
 */
@EasyMsRestController(name = "代理服务端接口")
public interface ProxyServerRestController {

    @EasyMsRestMapping(code = ProxyConstants.PROXY_REST_URL, service = ProxyServerRestService.class, name = "代理服务")
    String proxy(ProxyDTO proxyDTO);

}
