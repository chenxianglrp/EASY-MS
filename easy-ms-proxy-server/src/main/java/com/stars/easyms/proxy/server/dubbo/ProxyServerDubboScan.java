package com.stars.easyms.proxy.server.dubbo;

import com.stars.easyms.dubbo.scan.EasyMsDubboScan;

/**
 * <p>className: ProxyServerDubboScan</p>
 * <p>description: 代理Dubbo扫描实现类</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/31 3:35 下午
 */
public class ProxyServerDubboScan implements EasyMsDubboScan {

    @Override
    public String[] scanPackages() {
        return new String[]{this.getClass().getPackage().getName()};
    }

}
