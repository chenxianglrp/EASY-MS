package com.stars.easyms.proxy.server.rest.service;

import com.stars.easyms.proxy.client.local.LocalProxyClient;
import com.stars.easyms.proxy.dto.ProxyDTO;
import com.stars.easyms.rest.RestService;
import com.stars.easyms.rest.exception.BusinessRestException;

/**
 * <p>className: ProxyServerRestService</p>
 * <p>description: 代理服务端rest服务实现类</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/31 5:32 下午
 */
public class ProxyServerRestService extends LocalProxyClient implements RestService<ProxyDTO, String> {

    @Override
    public String execute(ProxyDTO proxyDTO) throws BusinessRestException {
        return proxy(proxyDTO);
    }

}
