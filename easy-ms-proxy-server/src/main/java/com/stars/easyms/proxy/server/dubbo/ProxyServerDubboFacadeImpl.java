package com.stars.easyms.proxy.server.dubbo;

import com.stars.easyms.proxy.client.local.LocalProxyClient;
import com.stars.easyms.proxy.dto.ProxyDTO;
import com.stars.easyms.proxy.dubbo.ProxyClientDubboFacade;
import org.apache.dubbo.config.annotation.DubboService;

/**
 * <p>className: ProxyServerDubboFacadeImpl</p>
 * <p>description: Dubbo方式代理客户端实现类</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/31 5:09 下午
 */
@DubboService
public class ProxyServerDubboFacadeImpl extends LocalProxyClient implements ProxyClientDubboFacade {

    @Override
    public String proxy(ProxyDTO proxyDTO) {
        return super.proxy(proxyDTO);
    }

}
