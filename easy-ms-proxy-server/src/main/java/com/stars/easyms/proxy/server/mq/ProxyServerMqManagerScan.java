package com.stars.easyms.proxy.server.mq;

import com.stars.easyms.mq.scan.EasyMsMQManagerScan;

/**
 * <p>className: ProxyServerMqManagerScan</p>
 * <p>description: 代理MQ管理扫描实现类</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/31 3:35 下午
 */
public class ProxyServerMqManagerScan implements EasyMsMQManagerScan {

    @Override
    public String[] scanPackages() {
        return new String[]{this.getClass().getPackage().getName()};
    }

}
