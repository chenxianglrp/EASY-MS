package com.stars.easyms.gateway.filter;

import com.stars.easyms.base.constant.HttpHeaderConstants;
import com.stars.easyms.gateway.properties.EasyMsGatewayProperties;
import com.stars.easyms.jwt.properties.EasyMsJwtProperties;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.cors.reactive.CorsUtils;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

/**
 * EasyMs网关自定义cors过滤器.
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/7/6 11:40 上午
 */
public class EasyMsCorsWebFilter implements WebFilter {
    
    private static final Set<String> ACCESS_CONTROL_ALLOW_HEADER_SET = new HashSet<>();
    
    @Autowired
    private EasyMsGatewayProperties gatewayProperties;
    
    @Autowired
    private EasyMsJwtProperties jwtProperties;
    
    static {
        ACCESS_CONTROL_ALLOW_HEADER_SET.add("origin");
        ACCESS_CONTROL_ALLOW_HEADER_SET.add("no-cache");
        ACCESS_CONTROL_ALLOW_HEADER_SET.add("x-requested-with");
        ACCESS_CONTROL_ALLOW_HEADER_SET.add("accept");
        ACCESS_CONTROL_ALLOW_HEADER_SET.add("if-modified-since");
        ACCESS_CONTROL_ALLOW_HEADER_SET.add("pragma");
        ACCESS_CONTROL_ALLOW_HEADER_SET.add("last-modified");
        ACCESS_CONTROL_ALLOW_HEADER_SET.add("cache-control");
        ACCESS_CONTROL_ALLOW_HEADER_SET.add("expires");
        ACCESS_CONTROL_ALLOW_HEADER_SET.add("x-e4m-with");
        ACCESS_CONTROL_ALLOW_HEADER_SET.add("content-type");
        ACCESS_CONTROL_ALLOW_HEADER_SET.add("x-session-id");
        ACCESS_CONTROL_ALLOW_HEADER_SET.add("token");
        ACCESS_CONTROL_ALLOW_HEADER_SET.add(HttpHeaderConstants.HEADER_KEY_USER_INFO);
        ACCESS_CONTROL_ALLOW_HEADER_SET.add(HttpHeaderConstants.HEADER_KEY_REMEMBER_ME);
    }
    
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        if (CorsUtils.isCorsRequest(request)) {
            HttpHeaders requestHeaders = request.getHeaders();
            ServerHttpResponse response = exchange.getResponse();
            HttpHeaders headers = response.getHeaders();
            EasyMsGatewayProperties.Cors cors = gatewayProperties.getCors();
            headers.add(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, requestHeaders.getOrigin());
            headers.add(HttpHeaders.ACCESS_CONTROL_ALLOW_HEADERS, getAllowHeaders());
            headers.add(HttpHeaders.ACCESS_CONTROL_ALLOW_METHODS, cors.getAllowMethods());
            headers.add(HttpHeaders.ACCESS_CONTROL_ALLOW_CREDENTIALS, cors.getAllowCredentials());
            headers.add(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, cors.getExposeHeaders());
            headers.add(HttpHeaders.ACCESS_CONTROL_MAX_AGE, gatewayProperties.getCors().getMaxAge());
            if (request.getMethod() == HttpMethod.OPTIONS) {
                response.setStatusCode(HttpStatus.OK);
                return Mono.empty();
            }
        }
        return chain.filter(exchange);
    }
    
    private String getAllowHeaders() {
        Set<String> allowHeaderSet = new HashSet<>(ACCESS_CONTROL_ALLOW_HEADER_SET);
        EasyMsGatewayProperties.Cors cors = gatewayProperties.getCors();
        if (cors.getAllowHeaders() != null && !cors.getAllowHeaders().isEmpty()) {
            cors.getAllowHeaders().stream().map(s -> s.toLowerCase(Locale.ENGLISH)).forEach(allowHeaderSet::add);
        }
        if (StringUtils.isNotBlank(jwtProperties.getHeaderKey())) {
            allowHeaderSet.add(jwtProperties.getHeaderKey().toLowerCase(Locale.ENGLISH));
        }
        return StringUtils.join(allowHeaderSet, ",");
    }
    
}
