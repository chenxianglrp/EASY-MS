package com.stars.easyms.gateway.autoconfigure;

import com.stars.easyms.base.enums.EncryptTypeEnum;
import com.stars.easyms.base.exception.IllegalPropertiesException;
import com.stars.easyms.base.util.PatternUtil;
import com.stars.easyms.gateway.filter.EasyMsAuthorizationFilter;
import com.stars.easyms.gateway.filter.EasyMsClearTraceFilter;
import com.stars.easyms.gateway.filter.EasyMsCorsWebFilter;
import com.stars.easyms.gateway.filter.EasyMsRequestFilter;
import com.stars.easyms.gateway.filter.EasyMsResponseFilter;
import com.stars.easyms.gateway.properties.EasyMsGatewayProperties;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.reactive.HiddenHttpMethodFilter;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.Set;

/**
 * <p>className: EasyMsGatewayAutoConfiguration</p>
 * <p>description: EasyMs的网关自动配置类</p>
 *
 * @author guoguifang
 * @version 1.6.1
 * @date 2020/8/26 3:20 下午
 */
@Configuration
@EnableConfigurationProperties(EasyMsGatewayProperties.class)
public class EasyMsGatewayAutoConfiguration {
    
    private EasyMsGatewayProperties gatewayProperties;
    
    public EasyMsGatewayAutoConfiguration(EasyMsGatewayProperties easyMsGatewayProperties) {
        this.gatewayProperties = easyMsGatewayProperties;
    }
    
    @Bean
    public EasyMsAuthorizationFilter easyMsAuthorizationFilter() {
        return new EasyMsAuthorizationFilter();
    }
    
    @Bean
    public EasyMsRequestFilter easyMsRequestFilter() {
        return new EasyMsRequestFilter();
    }
    
    @Bean
    public EasyMsResponseFilter easyMsResponseTraceFilter() {
        return new EasyMsResponseFilter(gatewayProperties);
    }
    
    @Bean
    public EasyMsClearTraceFilter easyMsClearRequestTraceFilter() {
        return new EasyMsClearTraceFilter();
    }
    
    @Bean
    public WebFilter corsFilter() {
        return new EasyMsCorsWebFilter();
    }
    
    @Bean
    public HiddenHttpMethodFilter hiddenHttpMethodFilter() {
        return new HiddenHttpMethodFilter() {
            @Override
            public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
                return chain.filter(exchange);
            }
        };
    }
    
    @PostConstruct
    private void init() {
        // 对EasyMsGatewayProperties进行验证，如果需要加解密则必须要有秘钥和秘钥偏移量
        StringBuilder stringBuilder = new StringBuilder();
        checkSourceConfig(gatewayProperties.getSource(), null, stringBuilder);
        gatewayProperties.getSources().forEach((key, source) -> checkSourceConfig(source, key, stringBuilder));
        if (stringBuilder.length() > 0) {
            throw new IllegalPropertiesException(stringBuilder.toString());
        }
        Set<String> logRequestPermitUrlSet = gatewayProperties.getLog().getLogRequestPermitUrlSet();
        logRequestPermitUrlSet.clear();
        if (StringUtils.isNotBlank(gatewayProperties.getLog().getRequestPermitUrl())) {
            Collections.addAll(logRequestPermitUrlSet,
                    PatternUtil.splitWithComma(gatewayProperties.getLog().getRequestPermitUrl()));
        }
        Set<String> logResponsePermitUrlSet = gatewayProperties.getLog().getLogResponsePermitUrlSet();
        logResponsePermitUrlSet.clear();
        if (StringUtils.isNotBlank(gatewayProperties.getLog().getResponsePermitUrl())) {
            Collections.addAll(logResponsePermitUrlSet,
                    PatternUtil.splitWithComma(gatewayProperties.getLog().getResponsePermitUrl()));
        }
    }
    
    private void checkSourceConfig(EasyMsGatewayProperties.Source source, String key, StringBuilder stringBuilder) {
        EasyMsGatewayProperties.Encrypt encrypt = source.getEncrypt();
        if (encrypt.isEnabled()) {
            EncryptTypeEnum encryptType = encrypt.getType();
            if (EncryptTypeEnum.SYMMETRIC == encryptType) {
                EasyMsGatewayProperties.Symmetric symmetric = encrypt.getSymmetric();
                if (StringUtils.isBlank(symmetric.getSecret())) {
                    stringBuilder.append("来源系统(").append(key == null ? "默认/未知" : key).append(")的对称加密秘钥为空，请使用")
                            .append(EasyMsGatewayProperties.PREFIX).append(key == null ? ".source" : ".sources." + key)
                            .append(".encrypt.symmetric.secret进行配置!\r\n");
                }
                if (StringUtils.isBlank(symmetric.getIv())) {
                    stringBuilder.append("来源系统(").append(key == null ? "默认/未知" : key).append(")的对称加密秘钥为空，请使用")
                            .append(EasyMsGatewayProperties.PREFIX).append(key == null ? ".source" : ".sources." + key)
                            .append(".encrypt.symmetric.iv进行配置!\r\n");
                }
            }
        }
        Set<String> encryptRequestPermitUrlSet = encrypt.getEncryptRequestPermitUrlSet();
        Set<String> encryptResponsePermitUrlSet = encrypt.getEncryptResponsePermitUrlSet();
        encryptRequestPermitUrlSet.clear();
        encryptResponsePermitUrlSet.clear();
        if (StringUtils.isNotBlank(encrypt.getRequestPermitUrl())) {
            String[] requestPermitUrls = PatternUtil.splitWithComma(encrypt.getRequestPermitUrl());
            Collections.addAll(encryptRequestPermitUrlSet, requestPermitUrls);
            Collections.addAll(encryptResponsePermitUrlSet, requestPermitUrls);
        }
        if (StringUtils.isNotBlank(encrypt.getResponsePermitUrl())) {
            Collections.addAll(encryptResponsePermitUrlSet, PatternUtil.splitWithComma(encrypt.getResponsePermitUrl()));
        }
    }
    
}
