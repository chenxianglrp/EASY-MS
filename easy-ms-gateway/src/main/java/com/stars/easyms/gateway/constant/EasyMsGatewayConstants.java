package com.stars.easyms.gateway.constant;

/**
 * <p>className: EasyMsGatewayConstants</p>
 * <p>description: EasyMs网关模块常量类</p>
 *
 * @author guoguifang
 * @version 1.6.3
 * @date 2020/9/11 2:41 下午
 */
public final class EasyMsGatewayConstants {

    public static final String EASY_MS_URL = "easy-ms-url";

    public static final String SWAGGER_DEBUG = "swaggerDebug";

    public static final String SWAGGER_DEBUG_REQUEST_HEADER_KEY = "swagger-debug";

    public static final String SWAGGER_DEBUG_REQUEST_HEADER_VALUE = "true";

    private EasyMsGatewayConstants() {}
}
