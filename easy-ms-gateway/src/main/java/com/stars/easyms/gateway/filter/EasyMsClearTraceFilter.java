package com.stars.easyms.gateway.filter;

import com.stars.easyms.base.trace.EasyMsTraceSynchronizationManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * <p>className: EasyMsClearRequestTraceFilter</p>
 * <p>description: 清除缓存</p>
 *
 * @author guoguifang
 * @version 1.6.1
 * @date 2020/8/20 18:32
 */
@Slf4j
public class EasyMsClearTraceFilter implements GlobalFilter, Ordered {

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        EasyMsTraceSynchronizationManager.clearTraceInfo();
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return Ordered.LOWEST_PRECEDENCE;
    }

}
