package com.stars.easyms.gateway.properties;

import com.stars.easyms.base.constant.EasyMsCommonConstants;
import com.stars.easyms.base.enums.EncryptTypeEnum;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <p>className: EasyMsGatewayProperties</p>
 * <p>description: EasyMs网关属性配置类</p>
 *
 * @author guoguifang
 * @version 1.6.1
 * @date 2020/8/27 6:02 下午
 */
@Data
@ConfigurationProperties(prefix = EasyMsGatewayProperties.PREFIX)
public class EasyMsGatewayProperties {

    public static final String PREFIX = EasyMsCommonConstants.EASY_MS_PROPERTIES_PREFIX + "gateway";

    private final Cors cors = new Cors();
    
    /**
     * 默认的源系统配置,只要不在sources中配置的默认都是用该配置
     */
    private final Source source = new Source();

    /**
     * 多个源系统时的配置
     */
    private final Map<String, Source> sources = new HashMap<>(8);

    /**
     * 是否记录日志的配置
     */
    private final Log log = new Log();

    /**
     * 是否激活debug模式，可在开发、测试环境激活：默认不激活
     */
    private boolean debugEnabled;
    
    /**
     * cors配置
     */
    @Data
    public static class Cors {
    
        /**
         * 允许的header头
         */
        private List<String> allowHeaders;
    
        /**
         * 允许的RequestMethod
         */
        private String allowMethods = "POST,GET,OPTIONS,PUT,DELETE,PATCH,HEAD";
    
        /**
         * 是否允许凭证
         */
        private String allowCredentials = "true";
    
        /**
         * 暴露的header头
         */
        private String exposeHeaders = "*";
    
        /**
         * 最大存活时间，单位：秒
         */
        private String maxAge = "18000L";
        
    }
    
    @Data
    public static class Source {

        /**
         * 加密信息
         */
        private Encrypt encrypt = new Encrypt();
    }

    /**
     * 加解密相关属性类
     */
    @Data
    public static class Encrypt {

        /**
         * 是否激活，默认激活
         */
        private boolean enabled = true;

        /**
         * 加密方式: symmetric/asymmetric，默认对称加密
         */
        private EncryptTypeEnum type = EncryptTypeEnum.SYMMETRIC;

        /**
         * 对称加密
         */
        private Symmetric symmetric = new Symmetric();

        /**
         * 非对称加密
         */
        private Asymmetric asymmetric = new Asymmetric();

        /**
         * 不做加解密的相关url
         */
        private String requestPermitUrl;

        /**
         * 不做加解密的相关url
         */
        private String responsePermitUrl;

        private final Set<String> encryptRequestPermitUrlSet = new HashSet<>();

        private final Set<String> encryptResponsePermitUrlSet = new HashSet<>();
    }

    @Data
    public static class Symmetric {

        /**
         * 对称加密的秘钥
         */
        private String secret;

        /**
         * 对称加密的秘钥偏移量
         */
        private String iv;

        /**
         * 加密json中已加密的请求值
         */
        private String key = EasyMsCommonConstants.DEFAULT_ENCRYPT_KEY;

    }

    @Data
    public static class Asymmetric {

        private String publicKey;

        private String privateKey;
    }

    @Data
    public static class Log {

        /**
         * 不需要记录日志的url
         */
        private String requestPermitUrl;

        /**
         * 不需要记录日志的url
         */
        private String responsePermitUrl;

        private final Set<String> logRequestPermitUrlSet = new HashSet<>();

        private final Set<String> logResponsePermitUrlSet = new HashSet<>();
    }

    public Source getSource(String code) {
        if (StringUtils.isNotBlank(code)) {
            EasyMsGatewayProperties.Source source = sources.get(code);
            if (source != null) {
                return source;
            }
        }
        return source;
    }

}
