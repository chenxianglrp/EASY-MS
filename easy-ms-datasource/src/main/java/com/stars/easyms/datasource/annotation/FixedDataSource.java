package com.stars.easyms.datasource.annotation;

import java.lang.annotation.*;

/**
 * <p>className: FixedDataSource</p>
 * <p>description: 用于固定数据源，由于easy-ms在不使用事务时，数据源是负载均衡的，因此在多主多从情况下可能同一个方法内会使用不同的主或不同的从，</p>
 *              <p>如果想要在多主多从情况下同一方法使用相同的主或相同的从请在该方法或该类上增加该注解</p>
 *
 * @author guoguifang
 * @date 2019/12/10 9:39
 * @since 1.4.2
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
@Documented
public @interface FixedDataSource {

}
