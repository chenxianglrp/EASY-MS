package com.stars.easyms.datasource.support;

import com.stars.easyms.datasource.enums.DatabaseType;
import com.stars.easyms.datasource.properties.EasyMsDataSourcePropertiesProcessor;

import javax.sql.DataSource;

/**
 * <p>interfaceName: DataSourceSupport</p>
 * <p>description: 数据源支持类接口</p>
 *
 * @author guoguifang
 * @version 1.2.1
 * @date 2019-05-14 17:06
 */
public interface DataSourceSupport<T extends DataSource> {

    /**
     * 创建数据源
     *
     * @param propertiesProcessor 配置处理者
     * @param databaseType        数据库类型
     * @param driverClassName     数据库驱动类名称
     * @param isMaster            是否是主数据源
     * @return
     */
    DataSource createDataSource(EasyMsDataSourcePropertiesProcessor propertiesProcessor, DatabaseType databaseType, String driverClassName, boolean isMaster);

    /**
     * 根据源数据源克隆数据源
     *
     * @param dataSource 源数据源
     * @return
     */
    DataSource cloneDataSource(T dataSource);

    /**
     * 设置连接参数
     *
     * @param dataSource 数据源
     * @param url 连接URL
     * @param username 连接用户名
     * @param password 连接密码
     */
    void setConnectParams(T dataSource, String url, String username, String password);

    /**
     * 判断数据源是否有效
     *
     * @param dataSource 数据源
     * @return
     */
    boolean isValid(T dataSource);
}
