package com.stars.easyms.datasource.scan;

/**
 * <p>className: EasyMsMQManagerScan</p>
 * <p>description: EasyMsMQ扫描包注册类</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/4/12 8:35 下午
 */
public interface EasyMsDataSourceMapperScan {

    /**
     * mybatis的mapper文件的扫描路径，不同路径之间用','分割，每个路径以'classpath*:'开头
     */
    default String mapperLocations() {
        return null;
    }

    /**
     * mybatis仓库的扫描路径
     */
    String typeAliasesPackage();

}