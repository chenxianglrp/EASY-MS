package com.stars.easyms.datasource.pointcut;

import com.stars.easyms.base.pointcut.EasyMsAnnotationMatchingPointcut;
import com.stars.easyms.datasource.annotation.FixedDataSource;
import com.stars.easyms.datasource.interceptor.FixedDataSourceInterceptor;
import org.aopalliance.aop.Advice;
import org.springframework.aop.Pointcut;
import org.springframework.aop.support.AbstractPointcutAdvisor;
import org.springframework.lang.NonNull;
import org.springframework.util.ObjectUtils;

import java.util.Collections;

/**
 * <p>className: FixedDataSourceTypePointcutAdvisor</p>
 * <p>description: 注解FixedDataSource在类上的顾问类</p>
 *
 * @author guoguifang
 * @date 2019/12/10 10:07
 * @since 1.4.2
 */
public final class FixedDataSourceTypePointcutAdvisor extends AbstractPointcutAdvisor {

    private transient Advice advice;

    private transient EasyMsAnnotationMatchingPointcut pointcut;

    public FixedDataSourceTypePointcutAdvisor() {
        this.advice = new FixedDataSourceInterceptor();
        this.pointcut = new EasyMsAnnotationMatchingPointcut();
        this.pointcut.setClassFilter(new EasyMsDataSourceAnnotationClassFilter(Collections.singletonList(FixedDataSource.class)));
        super.setOrder(10);
    }

    @NonNull
    @Override
    public Pointcut getPointcut() {
        return this.pointcut;
    }

    @NonNull
    @Override
    public Advice getAdvice() {
        return this.advice;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || this.getClass() != other.getClass()) {
            return false;
        }
        FixedDataSourceTypePointcutAdvisor otherAdvisor = (FixedDataSourceTypePointcutAdvisor) other;
        return (ObjectUtils.nullSafeEquals(this.advice, otherAdvisor.advice) &&
                ObjectUtils.nullSafeEquals(this.pointcut, otherAdvisor.pointcut));
    }

    @Override
    public int hashCode() {
        return 41 + this.advice.hashCode() * 41 + this.pointcut.hashCode() * 41;
    }
}