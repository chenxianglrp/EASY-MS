package com.stars.easyms.datasource.enums;

/**
 * <p>enumName: DataSourceType</p>
 * <p>description: 数据源类型:主从数据源</p>
 *
 * @author guoguifang
 * @version 1.1.0
 * @date 2019-03-04 10:00
 */
public enum DataSourceType {

    /**
     * 主数据源
     */
    MASTER,

    /**
     * 从数据源
     */
    SLAVE;
}
