package com.stars.easyms.datasource.properties;

import lombok.Getter;

import java.util.Properties;

/**
 * EasyMs的Mybatis属性类型
 *
 * @author guoguifang
 * @date 2018-10-19 10:07
 * @since 1.0.0
 */
@Getter
public final class EasyMsPageHelperProperties {

    public static final String PAGE_HELPER_PREFIX = "pagehelper";

    private Properties properties = new Properties();

    private Boolean offsetAsPageNum;

    private Boolean rowBoundsWithCount;

    private Boolean pageSizeZero;

    private Boolean reasonable;

    private Boolean supportMethodsArguments;

    private Boolean autoRuntimeDialect;

    private Boolean autoDialect;

    private Boolean closeConn;

    private String params;

    private Boolean defaultCount;

    private String dialectAlias;

    void setAutoDialect(Boolean autoDialect) {
        if (autoDialect != null) {
            this.autoDialect = autoDialect;
            this.properties.setProperty("autoDialect", String.valueOf(autoDialect));
        }
    }

    void setAutoRuntimeDialect(Boolean autoRuntimeDialect) {
        if (autoRuntimeDialect != null) {
            this.autoRuntimeDialect = autoRuntimeDialect;
            this.properties.setProperty("autoRuntimeDialect", String.valueOf(autoRuntimeDialect));
        }
    }

    void setCloseConn(Boolean closeConn) {
        if (closeConn != null) {
            this.closeConn = closeConn;
            this.properties.setProperty("closeConn", String.valueOf(closeConn));
        }
    }

    void setDefaultCount(Boolean defaultCount) {
        if (defaultCount != null) {
            this.defaultCount = defaultCount;
            this.properties.setProperty("defaultCount", String.valueOf(defaultCount));
        }
    }

    void setParams(String params) {
        if (params != null) {
            this.params = params;
            this.properties.setProperty("params", params);
        }
    }

    void setOffsetAsPageNum(Boolean offsetAsPageNum) {
        if (offsetAsPageNum != null) {
            this.offsetAsPageNum = offsetAsPageNum;
            this.properties.setProperty("offsetAsPageNum", String.valueOf(offsetAsPageNum));
        }
    }

    void setRowBoundsWithCount(Boolean rowBoundsWithCount) {
        if (rowBoundsWithCount != null) {
            this.rowBoundsWithCount = rowBoundsWithCount;
            this.properties.setProperty("rowBoundsWithCount", String.valueOf(rowBoundsWithCount));
        }
    }

    void setReasonable(Boolean reasonable) {
        if (reasonable != null) {
            this.reasonable = reasonable;
            this.properties.setProperty("reasonable", String.valueOf(reasonable));
        }
    }

    void setPageSizeZero(Boolean pageSizeZero) {
        if (pageSizeZero != null) {
            this.pageSizeZero = pageSizeZero;
            this.properties.setProperty("pageSizeZero", String.valueOf(pageSizeZero));
        }
    }

    void setSupportMethodsArguments(Boolean supportMethodsArguments) {
        if (supportMethodsArguments != null) {
            this.supportMethodsArguments = supportMethodsArguments;
            this.properties.setProperty("supportMethodsArguments", String.valueOf(supportMethodsArguments));
        }
    }

    void setDialectAlias(String dialectAlias) {
        if (dialectAlias != null) {
            this.dialectAlias = dialectAlias;
            this.properties.setProperty("dialectAlias", dialectAlias);
        }
    }

    EasyMsPageHelperProperties() {}
}
