package com.stars.easyms.datasource.mybatis;

import org.springframework.aop.framework.ReflectiveMethodInvocation;

import java.lang.reflect.Method;
import java.util.List;

/**
 * <p>className: EasyMsMybatisMethodInvocation</p>
 * <p>description: Mybatis的Mapper方法反射执行类</p>
 *
 * @author guoguifang
 * @version 1.2.2
 * @date 2019-08-12 15:22
 */
public final class EasyMsMybatisMethodInvocation<T> extends ReflectiveMethodInvocation {

    private EasyMsMapperProxy<T> easyMsMapperProxy;

    public EasyMsMybatisMethodInvocation(Object proxy, EasyMsMapperProxy<T> target, Method method, Object[] arguments, List<Object> interceptorsAndDynamicMethodMatchers) {
        super(proxy, target, method, arguments, null, interceptorsAndDynamicMethodMatchers);
        this.easyMsMapperProxy = target;
    }

    @Override
    protected Object invokeJoinpoint() throws Throwable {
        return easyMsMapperProxy.proceed(proxy, method, arguments);
    }

    public Class<T> getMapperInterface() {
        return easyMsMapperProxy.getMapperInterface();
    }
}