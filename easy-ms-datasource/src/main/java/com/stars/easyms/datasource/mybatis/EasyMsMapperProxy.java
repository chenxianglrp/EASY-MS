package com.stars.easyms.datasource.mybatis;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * <p>className: EasyMsMapperProxy</p>
 * <p>description: Mapper代理类接口</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/11/5 4:10 下午
 */
public interface EasyMsMapperProxy<T> extends InvocationHandler {

    Object proceed(Object proxy, Method method, Object[] args) throws Throwable;

    Class<T> getMapperInterface();
}
