package com.stars.easyms.datasource.mybatis;

import org.apache.ibatis.binding.BindingException;
import org.apache.ibatis.builder.annotation.MapperAnnotationBuilder;
import org.apache.ibatis.session.Configuration;

/**
 * <p>className: EasyMsMapperRegistry</p>
 * <p>description: EasyMs自定义mapper注册类</p>
 *
 * @author guoguifang
 * @version 1.2.2
 * @date 2019-08-12 11:57
 */
final class EasyMsMybatisMapperRegistry extends EasyMsMapperRegistry {

    private Configuration configuration;

    EasyMsMybatisMapperRegistry(Configuration config) {
        super(config);
        this.configuration = config;
    }

    @Override
    public <T> void addMapper(Class<T> type) {
        if (type.isInterface()) {
            if (hasMapper(type)) {
                throw new BindingException("Type " + type + " is already known to the MapperRegistry.");
            }
            boolean loadCompleted = false;
            try {
                knownMappers.put(type, new EasyMsMybatisMapperProxyFactory<>(type));
                MapperAnnotationBuilder parser = new MapperAnnotationBuilder(configuration, type);
                parser.parse();
                loadCompleted = true;
            } finally {
                if (!loadCompleted) {
                    knownMappers.remove(type);
                }
            }
        }
    }
}