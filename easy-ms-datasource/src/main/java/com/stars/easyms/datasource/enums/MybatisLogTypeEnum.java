package com.stars.easyms.datasource.enums;

import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.logging.Log;
import org.apache.ibatis.logging.commons.JakartaCommonsLoggingImpl;
import org.apache.ibatis.logging.jdk14.Jdk14LoggingImpl;
import org.apache.ibatis.logging.log4j.Log4jImpl;
import org.apache.ibatis.logging.log4j2.Log4j2Impl;
import org.apache.ibatis.logging.nologging.NoLoggingImpl;
import org.apache.ibatis.logging.slf4j.Slf4jImpl;
import org.apache.ibatis.logging.stdout.StdOutImpl;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>className: MybatisLogTypeEnum</p>
 * <p>description: mybatis支持的日志类型枚举</p>
 *
 * @author guoguifang
 * @version 1.7.2
 * @date 2020/12/23 2:37 下午
 */
public enum MybatisLogTypeEnum {

    /**
     * SLF4J (Ignore case)
     */
    SLF4J(Slf4jImpl.class),

    /**
     * COMMONS_LOGGING (Ignore case)
     */
    COMMONS_LOGGING(JakartaCommonsLoggingImpl.class),

    /**
     * COMMONS (Ignore case)
     */
    COMMONS(JakartaCommonsLoggingImpl.class),

    /**
     * LOG4J (Ignore case)
     */
    LOG4J(Log4jImpl.class),

    /**
     * LOG4J2 (Ignore case)
     */
    LOG4J2(Log4j2Impl.class),

    /**
     * JDK_LOGGING (Ignore case)
     */
    JDK_LOGGING(Jdk14LoggingImpl.class),

    /**
     * JDK (Ignore case)
     */
    JDK(Jdk14LoggingImpl.class),

    /**
     * STDOUT_LOGGING (Ignore case)
     */
    STDOUT_LOGGING(StdOutImpl.class),

    /**
     * STDOUT (Ignore case)
     */
    STDOUT(StdOutImpl.class),

    /**
     * NO_LOGGING (Ignore case)
     */
    NO_LOGGING(NoLoggingImpl.class),

    /**
     * NO (Ignore case)
     */
    NO(NoLoggingImpl.class);

    private Class<? extends Log> loggingImplClass;

    MybatisLogTypeEnum(Class<? extends Log> loggingImplClass) {
        this.loggingImplClass = loggingImplClass;
    }

    public Class<? extends Log> getLoggingImplClass() {
        return loggingImplClass;
    }

    private static Map<String, Class<? extends Log>> mybatisLogCache;

    static {
        mybatisLogCache = new HashMap<>(16);
        for (MybatisLogTypeEnum mybatisLogTypeEnum : values()) {
            mybatisLogCache.put(mybatisLogTypeEnum.name(), mybatisLogTypeEnum.loggingImplClass);
        }
    }

    public static Class<? extends Log> getMybatisLogImplClass(String name) {
        Class<? extends Log> localLoggingImplClass = null;
        if (StringUtils.isNotBlank(name)) {
            for (Map.Entry<String, Class<? extends Log>> entry : mybatisLogCache.entrySet()) {
                if (entry.getKey().equalsIgnoreCase(name)
                        || entry.getValue().getName().equalsIgnoreCase(name)) {
                    localLoggingImplClass = entry.getValue();
                    break;
                }
            }
        }
        return localLoggingImplClass;
    }

}
