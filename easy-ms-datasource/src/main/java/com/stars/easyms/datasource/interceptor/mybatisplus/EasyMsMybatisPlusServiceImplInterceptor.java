package com.stars.easyms.datasource.interceptor.mybatisplus;

import com.stars.easyms.datasource.holder.EasyMsMybatisPlusHolder;
import com.stars.easyms.datasource.interceptor.BaseEasyMsDatasourceMethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

/**
 * <p>className: EasyMsMybatisPlusServiceImplInterceptor</p>
 * <p>description: EasyMs的mybatis-plus的serviceImpl实现类拦截器</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/11/21 12:27 下午
 */
public final class EasyMsMybatisPlusServiceImplInterceptor extends BaseEasyMsDatasourceMethodInterceptor {

    @Override
    protected Object interceptMethod(MethodInvocation methodInvocation) throws Throwable {
        EasyMsMybatisPlusHolder.setCurrIsMybatisPlusMethod();
        try {
            return proceed(methodInvocation);
        } finally {
            EasyMsMybatisPlusHolder.clear();
        }
    }
}
