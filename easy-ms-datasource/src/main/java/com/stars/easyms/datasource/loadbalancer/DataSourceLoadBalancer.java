package com.stars.easyms.datasource.loadbalancer;

import com.stars.easyms.datasource.EasyMsDataSource;
import com.stars.easyms.datasource.EasyMsDataSourceSet;

/**
 * <p>interfaceName: DataSourceLoadBalancer</p>
 * <p>description: 数据源负载均衡器接口类</p>
 *
 * @author guoguifang
 * @version 1.2.1
 * @date 2019-05-13 13:31
 */
public interface DataSourceLoadBalancer {

    /**
     * 通过负载均衡策略获取相应的数据源
     *
     * @param easyMsDataSourceSet EasyMs数据源集合
     * @return 经过负载均衡计算后的数据源
     */
    EasyMsDataSource getDataSource(EasyMsDataSourceSet easyMsDataSourceSet);

}
