package com.stars.easyms.datasource.exception;

import com.stars.easyms.base.util.ExceptionUtil;
import com.stars.easyms.base.util.MessageFormatUtil;

/**
 * <p>className: DataSourceInterceptorException</p>
 * <p>description: 执行拦截器时抛出的异常</p>
 *
 * @author guoguifang
 * @date 2019/5/29 9:48
 * @version 1.2.1
 */
public class DataSourceInterceptorException extends Exception {

    private static final long serialVersionUID = -1034894190345465631L;

    public DataSourceInterceptorException(Throwable throwable) {
        super(throwable);
    }

    public DataSourceInterceptorException(String message, Object... args) {
        super(MessageFormatUtil.format(message, ExceptionUtil.trimmedCopy(args)), ExceptionUtil.extractThrowable(args));
    }
}
