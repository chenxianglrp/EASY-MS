package com.stars.easyms.datasource.mybatisplus;

import com.stars.easyms.datasource.mybatis.EasyMsMapperProxy;
import com.stars.easyms.datasource.mybatis.EasyMsMybatisMapperProxyFactory;
import org.apache.ibatis.session.SqlSession;

import java.lang.reflect.Proxy;

/**
 * <p>className: EasyMsMybatisPlusMapperProxyFactory</p>
 * <p>description: EasyMs自定义Mybatis-Plus-Mapper代理的工厂类</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/11/5 4:01 下午
 */
@SuppressWarnings("unchecked")
final class EasyMsMybatisPlusMapperProxyFactory<T> extends EasyMsMybatisMapperProxyFactory<T> {

    EasyMsMybatisPlusMapperProxyFactory(Class<T> mapperInterface) {
        super(mapperInterface);
    }

    @Override
    public T newInstance(SqlSession sqlSession) {
        final EasyMsMapperProxy<T> mapperProxy = new EasyMsMybatisPlusMapperProxy<>(sqlSession, mapperInterface, methodCache);
        return (T) Proxy.newProxyInstance(mapperInterface.getClassLoader(), new Class[] { mapperInterface }, mapperProxy);
    }
}