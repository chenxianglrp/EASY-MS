package com.stars.easyms.datasource.dynamic;

import com.stars.easyms.datasource.EasyMsDataSourceFactory;
import com.stars.easyms.datasource.common.EasyMsDataSourceConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.util.ClassUtils;

/**
 * <p>className: DynamicLoadDataSourceConfig</p>
 * <p>description: 动态加载数据源配置类</p>
 *
 * @author guoguifang
 * @version 1.2.1
 * @date 2019-05-30 13:47
 */
@Slf4j
public final class DynamicLoadDataSourceConfig {

    private EasyMsDataSourceFactory easyMsDataSourceFactory;

    public void start(ApplicationContext applicationContext) {
        // 判断是否有apollo
        if (ClassUtils.isPresent(EasyMsDataSourceConstant.JUDGE_APOLLO_IS_EXIST_CLASS, DynamicLoadDataSourceConfig.class.getClassLoader())) {
            DynamicLoadApolloConfig.startListen(easyMsDataSourceFactory, applicationContext);
        }
    }

    public DynamicLoadDataSourceConfig(EasyMsDataSourceFactory easyMsDataSourceFactory) {
        this.easyMsDataSourceFactory = easyMsDataSourceFactory;
    }
}