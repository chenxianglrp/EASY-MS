package com.stars.easyms.datasource.mybatis;

import org.apache.ibatis.binding.MapperProxyFactory;
import org.apache.ibatis.session.SqlSession;

import java.lang.reflect.Proxy;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * <p>className: EasyMsMapperProxyFactory</p>
 * <p>description: EasyMs自定义Mapper代理的工厂类</p>
 *
 * @author guoguifang
 * @version 1.2.2
 * @date 2019-08-12 12:02
 */
@SuppressWarnings("unchecked")
public class EasyMsMybatisMapperProxyFactory<T> extends MapperProxyFactory<T> {

    protected final Class<T> mapperInterface;

    protected final Map methodCache = new ConcurrentHashMap();

    public EasyMsMybatisMapperProxyFactory(Class<T> mapperInterface) {
        super(mapperInterface);
        this.mapperInterface = mapperInterface;
    }

    @Override
    public Map getMethodCache() {
        return methodCache;
    }

    @Override
    public T newInstance(SqlSession sqlSession) {
        final EasyMsMapperProxy<T> mapperProxy = new EasyMsMybatisMapperProxy<>(sqlSession, mapperInterface, methodCache);
        return (T) Proxy.newProxyInstance(mapperInterface.getClassLoader(), new Class[] { mapperInterface }, mapperProxy);
    }
}