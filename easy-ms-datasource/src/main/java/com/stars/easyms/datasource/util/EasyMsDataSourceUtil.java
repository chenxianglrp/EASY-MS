package com.stars.easyms.datasource.util;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * <p>className: EasyMsDataSourceUtil</p>
 * <p>description: 数据源工具类</p>
 *
 * @author guoguifang
 * @date 2019-12-09 13:14
 * @since 1.4.2
 */
@Slf4j
public final class EasyMsDataSourceUtil {

    @NonNull
    public static Connection getConnection(@NonNull DataSource dataSource) throws SQLException {
        if (log.isDebugEnabled()) {
            log.debug("Fetching JDBC Connection from DataSource: {}", dataSource);
        }
        return dataSource.getConnection();
    }

    private EasyMsDataSourceUtil() {}
}