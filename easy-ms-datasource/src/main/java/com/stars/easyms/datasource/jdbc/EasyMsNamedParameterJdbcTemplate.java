package com.stars.easyms.datasource.jdbc;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

/**
 * <p>className: EasyMsNamedParameterJdbcTemplate</p>
 * <p>description: EasyMs的NamedParameterJdbcTemplate类</p>
 *
 * @author guoguifang
 * @date 2019-11-22 19:15
 * @since 1.4.1
 */
public final class EasyMsNamedParameterJdbcTemplate extends NamedParameterJdbcTemplate {

    public EasyMsNamedParameterJdbcTemplate(EasyMsJdbcTemplate easyMsJdbcTemplate) {
        super(easyMsJdbcTemplate);
    }

}