package com.stars.easyms.datasource.mybatis;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * <p>className: EasyMsSqlSessionInterceptor</p>
 * <p>description: SqlSession拦截器</p>
 * 位于EasyMsRepositoryInterceptor、SpecifyDataSourceInterceptor之后，EasyMsQueryInterceptor、EasyMsUpdateInterceptor、EasyMsPageInterceptor之前，
 * 在此处分为两种情况：
 * 1.有事务：事务一般用于增删改，因此在该方法处强制设置为主数据源，不在后续设置
 * 2.无事务：因为在EasyMsUpdateInterceptor会强制将增删改置为主数据源，因此此处无法确定具体的数据源，需要在EasyMsQueryInterceptor、EasyMsUpdateInterceptor里具体得到数据源
 *
 * @author guoguifang
 * @version 1.3.2
 * @date 2019-09-20 11:01
 */
final class EasyMsSqlSessionInterceptor implements InvocationHandler {

    private final EasyMsSqlSessionTemplate easyMsSqlSessionTemplate;

    EasyMsSqlSessionInterceptor(EasyMsSqlSessionTemplate easyMsSqlSessionTemplate) {
        this.easyMsSqlSessionTemplate = easyMsSqlSessionTemplate;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        return easyMsSqlSessionTemplate.applyWithSqlSession(sqlSession -> method.invoke(sqlSession, args));
    }

}