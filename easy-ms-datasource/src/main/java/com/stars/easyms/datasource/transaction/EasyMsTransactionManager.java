package com.stars.easyms.datasource.transaction;

import com.stars.easyms.datasource.EasyMsDataSource;
import com.stars.easyms.datasource.EasyMsMasterSlaveDataSource;
import org.springframework.core.Constants;
import org.springframework.jdbc.datasource.ConnectionHolder;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.transaction.IllegalTransactionStateException;
import org.springframework.transaction.InvalidTimeoutException;
import org.springframework.transaction.NestedTransactionNotSupportedException;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.interceptor.TransactionAttribute;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * <p>className: EasyMsTransactionManager</p>
 * <p>description: EasyMs自定义事务信息类</p>
 *
 * @author guoguifang
 * @date 2019-12-01 21:22
 * @since 1.4.2
 */
final class EasyMsTransactionManager {

    private static final Constants CONSTANTS_FOR_DEFINITION = new Constants(TransactionDefinition.class);

    private final EasyMsDataSourceTransactionManager dataSourceTransactionManager;

    private final TransactionDefinition transactionDefinition;

    private final Map<EasyMsMasterSlaveDataSource, EasyMsDataSource> fixedEasyMsDataSourceMap;

    private final Map<EasyMsDataSource, EasyMsTransactionStatusHolder> transactionStatusHolderMap;

    private boolean annotationTransaction;

    private String joinPointIdentification;

    private String transactionName;

    private boolean transactionReadOnly;

    private Integer transactionIsolationLevel;

    private boolean actualTransactionActive;

    private boolean joinTransaction;

    private boolean nestedAndUseSavepoint;

    private boolean rollbackOnly;

    private EasyMsTransactionManager previousTransactionManager;

    private EasyMsTransactionManager topTransactionManager;

    EasyMsTransactionManager(@NonNull EasyMsDataSourceTransactionManager dataSourceTransactionManager,
                             @NonNull TransactionAttribute transactionAttribute,
                             @NonNull String joinPointIdentification) {
        this(dataSourceTransactionManager, transactionAttribute);
        this.joinPointIdentification = joinPointIdentification;
        this.annotationTransaction = true;
    }

    EasyMsTransactionManager(@NonNull EasyMsDataSourceTransactionManager dataSourceTransactionManager,
                             @NonNull TransactionDefinition transactionDefinition) {
        this.dataSourceTransactionManager = dataSourceTransactionManager;
        this.transactionDefinition = transactionDefinition;
        this.fixedEasyMsDataSourceMap = new HashMap<>(8);
        this.transactionStatusHolderMap = new LinkedHashMap<>();
        this.initTransactionInfo();
    }

    private void initTransactionInfo() {
        // 保存前一个事务管理器
        this.previousTransactionManager = EasyMsTransactionSynchronizationManager.getTransactionManager();

        // 获取事务参数
        this.transactionIsolationLevel = transactionDefinition.getIsolationLevel() != TransactionDefinition.ISOLATION_DEFAULT ?
                transactionDefinition.getIsolationLevel() : null;
        this.transactionReadOnly = transactionDefinition.isReadOnly();
        this.transactionName = transactionDefinition.getName();

        this.analysisTransactionPropagation();

        // 即使没有创建一个新的事务也总是将TransactionInfo绑定到线程，这保证了即使没有创建任何事务，TransactionInfo堆栈也能得到正确的管理。
        EasyMsTransactionSynchronizationManager.setTransactionManager(this);
    }

    private void analysisTransactionPropagation() {

        int transactionPropagation = this.transactionDefinition.getPropagationBehavior();

        // 如果事务传播机制为'PROPAGATION_REQUIRES_NEW'则为新事务则直接当做顶层事务处理
        if (transactionPropagation == TransactionDefinition.PROPAGATION_REQUIRES_NEW) {
            this.checkTransactionTimeout();
            this.joinTransaction = false;
            this.nestedAndUseSavepoint = false;
            this.actualTransactionActive = true;
            this.topTransactionManager = this;
            return;
        }

        // 如果当前不存在事务
        if (!isExistPreviousTransaction()) {

            // 若传播机制为PROPAGATION_MANDATORY，且当前事务不存在，则抛出异常
            if (transactionPropagation == TransactionDefinition.PROPAGATION_MANDATORY) {
                throw new IllegalTransactionStateException(
                        "No existing transaction found for transaction marked with propagation 'mandatory'");
            }

            this.checkTransactionTimeout();
            this.joinTransaction = false;
            this.nestedAndUseSavepoint = false;
            this.actualTransactionActive = transactionPropagation == TransactionDefinition.PROPAGATION_REQUIRED
                    || transactionPropagation == TransactionDefinition.PROPAGATION_NESTED;
            if (this.actualTransactionActive) {
                this.topTransactionManager = this;
            }
            return;
        }

        // 如果已经存在事务，但是传播机制为PROPAGATION_NEVER时抛出异常
        if (transactionPropagation == TransactionDefinition.PROPAGATION_NEVER) {
            throw new IllegalTransactionStateException(
                    "Existing transaction found for transaction marked with propagation 'never'");
        }

        // 如果是嵌套事务，但是当前数据源事务管理器不允许嵌套事务则抛出异常
        if (transactionPropagation == TransactionDefinition.PROPAGATION_NESTED
                && !this.dataSourceTransactionManager.isNestedTransactionAllowed()) {
            throw new NestedTransactionNotSupportedException(
                    "Transaction manager does not allow nested transactions by default - " +
                            "specify 'nestedTransactionAllowed' property with value 'true'");
        }

        // 判断是否需要加入上个事务：
        this.joinTransaction = transactionPropagation == TransactionDefinition.PROPAGATION_REQUIRED
                || transactionPropagation == TransactionDefinition.PROPAGATION_MANDATORY
                || transactionPropagation == TransactionDefinition.PROPAGATION_SUPPORTS
                || (transactionPropagation == TransactionDefinition.PROPAGATION_NESTED
                && !this.dataSourceTransactionManager.isUseSavepointForNestedTransaction());

        // 如果需要加入上个事务且数据源事务管理器需要校验已存在的事务则判断事务隔离级别和事务只读是否一致
        if (this.joinTransaction && dataSourceTransactionManager.isValidateExistingTransaction()) {
            Integer previousIsolationLevel = previousTransactionManager.transactionIsolationLevel;
            boolean isIsolationMismatch = this.transactionDefinition.getIsolationLevel() != TransactionDefinition.ISOLATION_DEFAULT
                    && (previousIsolationLevel == null || previousIsolationLevel != this.transactionDefinition.getIsolationLevel());
            if (isIsolationMismatch) {
                throw new IllegalTransactionStateException("Participating transaction with definition [" +
                        transactionDefinition + "] specifies isolation level which is incompatible with existing transaction: " +
                        (previousIsolationLevel != null ?
                                CONSTANTS_FOR_DEFINITION.toCode(previousIsolationLevel, DefaultTransactionDefinition.PREFIX_ISOLATION) : "(unknown)"));
            }
            if (!this.transactionReadOnly && previousTransactionManager.transactionReadOnly) {
                throw new IllegalTransactionStateException("Participating transaction with definition [" +
                        transactionDefinition + "] is not marked as read-only but existing transaction is");
            }
        }

        // 判断是否事务可激活，当存在事务时propagation_not_supported会挂起事务以无事务状态运行则不会激活当前事务
        this.actualTransactionActive = transactionPropagation != TransactionDefinition.PROPAGATION_NOT_SUPPORTED;

        // 判断是否是嵌套事务并且数据源事务管理器允许为嵌套事务使用savepoint
        this.nestedAndUseSavepoint = transactionPropagation == TransactionDefinition.PROPAGATION_NESTED
                && dataSourceTransactionManager.isUseSavepointForNestedTransaction();

        // 如果事务已激活则设置顶层事务
        if (this.actualTransactionActive) {
            this.topTransactionManager = this.previousTransactionManager.topTransactionManager;
        }
    }

    @NonNull
    EasyMsDataSource getFixedMasterEasyMsDataSource(@NonNull EasyMsMasterSlaveDataSource easyMsMasterSlaveDataSource) {
        return this.fixedEasyMsDataSourceMap.computeIfAbsent(easyMsMasterSlaveDataSource, e -> {
            EasyMsDataSource easyMsDataSource = null;
            EasyMsTransactionManager localPreviousTransactionManager = this.previousTransactionManager;
            while (localPreviousTransactionManager != null && easyMsDataSource == null) {
                easyMsDataSource = localPreviousTransactionManager.fixedEasyMsDataSourceMap.get(e);
                localPreviousTransactionManager = localPreviousTransactionManager.previousTransactionManager;
            }
            if (easyMsDataSource == null) {
                easyMsDataSource = e.getMasterDataSource();
            }
            return easyMsDataSource;
        });
    }

    @NonNull
    EasyMsTransactionStatusHolder newTransactionStatusHolder(@NonNull EasyMsDataSource easyMsDataSource) {
        // 区分是否是内嵌事务，如果是内嵌事务则在本事务管理器中管理事务状态，否则使用顶层事务管理器管理事务状态
        EasyMsTransactionManager localTransactionManager = this.nestedAndUseSavepoint ? this : getTopTransactionManager();
        Map<EasyMsDataSource, EasyMsTransactionStatusHolder> topTransactionStatusHolderMap = localTransactionManager.transactionStatusHolderMap;
        EasyMsTransactionStatusHolder transactionStatusHolder = topTransactionStatusHolderMap.get(easyMsDataSource);
        if (transactionStatusHolder == null) {
            transactionStatusHolder = new EasyMsTransactionStatusHolder();
            topTransactionStatusHolderMap.put(easyMsDataSource, transactionStatusHolder);
            transactionStatusHolder.setTransactionStatus(
                    this.dataSourceTransactionManager.getTransactionStatus(easyMsDataSource, this));
        }
        return transactionStatusHolder;
    }

    void bindConnectionHolder(@NonNull EasyMsDataSource easyMsDataSource, @NonNull ConnectionHolder connectionHolder) {
        EasyMsTransactionStatusHolder transactionStatusHolder = this.transactionStatusHolderMap.get(easyMsDataSource);
        Assert.notNull(transactionStatusHolder, "Easy-ms transaction synchronization is not active");
        transactionStatusHolder.setConnectionHolder(connectionHolder);
    }

    void restoreThreadLocalStatus() {
        if (this.previousTransactionManager != null) {
            this.previousTransactionManager.rollbackOnly = this.rollbackOnly;
            EasyMsTransactionSynchronizationManager.setTransactionManager(this.previousTransactionManager);
        } else {
            EasyMsTransactionSynchronizationManager.clearTransactionManager();
        }
    }

    boolean isActualTransactionActive() {
        return actualTransactionActive;
    }

    @NonNull
    TransactionDefinition getTransactionDefinition() {
        return this.transactionDefinition;
    }

    @NonNull
    TransactionAttribute getTransactionAttribute() {
        Assert.isTrue(annotationTransaction, "Current transaction is not annotated");
        return (TransactionAttribute) this.transactionDefinition;
    }

    @Nullable
    String getJoinPointIdentification() {
        Assert.isTrue(annotationTransaction, "Current transaction is not annotated");
        return this.joinPointIdentification;
    }

    @NonNull
    EasyMsTransactionManager getTopTransactionManager() {
        Assert.notNull(this.topTransactionManager, "Transaction is not active");
        return this.topTransactionManager;
    }

    @Nullable
    EasyMsTransactionStatusHolder getTransactionStatusHolder(@NonNull EasyMsDataSource easyMsDataSource) {
        return this.transactionStatusHolderMap.get(easyMsDataSource);
    }

    @NonNull
    Map<EasyMsDataSource, EasyMsTransactionStatusHolder> getTransactionStatusHolderMap() {
        return this.transactionStatusHolderMap;
    }

    @Nullable
    String getTransactionName() {
        return this.transactionName;
    }

    boolean isTransactionReadOnly() {
        return this.transactionReadOnly;
    }

    @Nullable
    Integer getTransactionIsolationLevel() {
        return this.transactionIsolationLevel;
    }

    boolean isNestedAndUseSavepoint() {
        return this.nestedAndUseSavepoint;
    }

    void setRollbackOnly() {
        this.rollbackOnly = true;
    }

    boolean isRollbackOnly() {
        return this.rollbackOnly;
    }

    boolean isJoinTransaction() {
        return this.joinTransaction;
    }

    boolean isNewTransaction() {
        return this.topTransactionManager == this;
    }

    boolean isExistPreviousTransaction() {
        return this.previousTransactionManager != null && this.previousTransactionManager.isActualTransactionActive();
    }

    private void checkTransactionTimeout() {
        // 检查新事务的定义设置
        if (transactionDefinition.getTimeout() < TransactionDefinition.TIMEOUT_DEFAULT) {
            throw new InvalidTimeoutException("Invalid transaction timeout", transactionDefinition.getTimeout());
        }
    }

    @Override
    public String toString() {
        return this.transactionDefinition.toString();
    }

}