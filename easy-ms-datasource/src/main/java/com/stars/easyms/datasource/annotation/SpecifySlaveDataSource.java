package com.stars.easyms.datasource.annotation;

import java.lang.annotation.*;

/**
 * <p>annotationName: SpecifyMasterDataSource</p>
 * <p>description: EasyMs使用从数据源时加该注解，只能用于DAO的方法上</p>
 *
 * @author guoguifang
 * @version 1.1.0
 * @date 2019-02-28 16:21
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Documented
public @interface SpecifySlaveDataSource {

}
