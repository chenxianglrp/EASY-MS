package com.stars.easyms.datasource.annotation;

import com.stars.easyms.datasource.common.EasyMsDataSourceConstant;

import java.lang.annotation.*;

/**
 * <p>className: SpecifyDataSource</p>
 * <p>description: 指定数据源：切换数据源时加该注解，可以用于类和方法上，DAO的类上加该注解优先级高于EasyMsRepository，Service和Controller优先级高于DAO</p>
 *
 * @author guoguifang
 * @date 2019/2/28 16:28
 * @version 1.1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
@Documented
public @interface SpecifyDataSource {

    /**
     * 若使用多数据源时需指定使用哪个数据源，若不设置为默认数据源
     */
    String value() default EasyMsDataSourceConstant.DEFAULT_DATASOURCE_NAME;
}