package com.stars.easyms.datasource.properties;

import com.stars.easyms.base.util.PatternUtil;
import com.stars.easyms.base.util.SpringBootUtil;
import com.stars.easyms.datasource.common.EasyMsDataSourceConstant;
import com.stars.easyms.datasource.enums.MybatisLogTypeEnum;
import com.stars.easyms.datasource.exception.IllegalDataSourcePropertiesException;
import com.stars.easyms.datasource.scan.EasyMsDataSourceMapperScan;
import com.stars.easyms.datasource.util.XmlMapperParseUtil;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>className: EasyMsPropertiesHelper</p>
 * <p>description: EasyMs属性处理类，由于需要支持springmvc版本，因此不可使用springboot的自动加载属性类的注解，使用手动加载</p>
 *
 * @author guoguifang
 * @version 1.2.1
 * @date 2019-07-09 12:18
 */
public final class EasyMsPropertiesHelper {

    private static final EasyMsDataSourceProperties EASY_MS_DATA_SOURCE_PROPERTIES = new EasyMsDataSourceProperties();

    private static final EasyMsMybatisProperties EASY_MS_MYBATIS_PROPERTIES = new EasyMsMybatisProperties();

    private static final EasyMsPageHelperProperties EASY_MS_PAGE_HELPER_PROPERTIES = new EasyMsPageHelperProperties();

    /**
     * 初始化属性，为了兼容SpringMVC，使用手动获取属性的方式，同时还可保证实时获取属性
     */
    public static void initProperties() {
        // 初始化多数据源属性
        initMultiDatasourceProperties();

        // 初始化mybatis属性
        initMybatisProperties();

        // 初始化分页插件属性
        initPageHelperProperties();
    }

    public static EasyMsDataSourceProperties getEasyMsDataSourceProperties() {
        return EASY_MS_DATA_SOURCE_PROPERTIES;
    }

    public static EasyMsMybatisProperties getEasyMsMybatisProperties() {
        return EASY_MS_MYBATIS_PROPERTIES;
    }

    public static EasyMsPageHelperProperties getEasyMsPageHelperProperties() {
        return EASY_MS_PAGE_HELPER_PROPERTIES;
    }

    private static void initMultiDatasourceProperties() {
        BasePropertiesProcessor.bindProperty(EasyMsDataSourceProperties.MULTI_DATASOURCE_ACTIVE,
                EasyMsDataSourceProperties.MULTI_DATASOURCE_ACTIVE_STANDBY,
                EASY_MS_DATA_SOURCE_PROPERTIES::setMultiDatasourceActive, Set.class);
    }

    private static void initMybatisProperties() {
        BasePropertiesProcessor mybatisPropertiesProcessor = new BasePropertiesProcessor(EasyMsMybatisProperties.MYBATIS_PREFIX);
        String typeAliasesPackage =
                mybatisPropertiesProcessor.bindProperty("typeAliasesPackage",
                        EASY_MS_MYBATIS_PROPERTIES::setTypeAliasesPackage, String.class, SpringBootUtil::getSpringApplicationPackageName);
        if (StringUtils.isBlank(typeAliasesPackage)) {
            throw new IllegalDataSourcePropertiesException("Configuration [{}.typeAliasesPackage] could not be found", EasyMsMybatisProperties.MYBATIS_PREFIX);
        }

        mybatisPropertiesProcessor.bindProperty("mapperLocations",
                EASY_MS_MYBATIS_PROPERTIES::setMapperLocations, List.class, () -> {
                    List<String> convertedList = XmlMapperParseUtil.convert(typeAliasesPackage);
                    convertedList.add(EasyMsDataSourceConstant.MYBATIS_PLUS_DEFAULT_MAPPER_LOCATIONS);
                    return convertedList;
                });

        mybatisPropertiesProcessor.bindProperty("logImpl",
                EASY_MS_MYBATIS_PROPERTIES::setLogImpl, String.class, MybatisLogTypeEnum.NO::name);

        // 获取依赖jar包中所有实现了EasyMsDataSourceMapperScan接口的扫描类并将其加进扫描队列中
        Set<String> typeAliasesPackageSet = new HashSet<>();
        Set<String> mapperLocationsSet = new HashSet<>();
        ServiceLoader<EasyMsDataSourceMapperScan> easyMsDataSourceMapperScans = ServiceLoader.load(EasyMsDataSourceMapperScan.class);
        for (EasyMsDataSourceMapperScan easyMsDataSourceMapperScan : easyMsDataSourceMapperScans) {
            String localTypeAliasesPackage = easyMsDataSourceMapperScan.typeAliasesPackage();
            if (StringUtils.isNotBlank(localTypeAliasesPackage)) {
                typeAliasesPackageSet.add(localTypeAliasesPackage);
            }
            String localMapperLocations = easyMsDataSourceMapperScan.mapperLocations();
            if (StringUtils.isNotBlank(localMapperLocations)) {
                mapperLocationsSet.addAll(Arrays.asList(PatternUtil.splitWithComma(localMapperLocations)));
            } else if (StringUtils.isNotBlank(localTypeAliasesPackage)) {
                List<String> convertedList = XmlMapperParseUtil.convert(localTypeAliasesPackage);
                mapperLocationsSet.addAll(convertedList);
            }
        }
        if (!typeAliasesPackageSet.isEmpty()) {
            EASY_MS_MYBATIS_PROPERTIES.setTypeAliasesPackage(
                    String.join(",", typeAliasesPackageSet) + "," + typeAliasesPackage);
        }
        if (!mapperLocationsSet.isEmpty()) {
            List<String> mapperLocations = EASY_MS_MYBATIS_PROPERTIES.getMapperLocations();
            mapperLocations.addAll(mapperLocationsSet);
            EASY_MS_MYBATIS_PROPERTIES.setMapperLocations(mapperLocations);
        }
    }

    private static void initPageHelperProperties() {
        BasePropertiesProcessor pageHelperPropertiesProcessor = new BasePropertiesProcessor(EasyMsPageHelperProperties.PAGE_HELPER_PREFIX);
        pageHelperPropertiesProcessor.bindProperty("offsetAsPageNum", EASY_MS_PAGE_HELPER_PROPERTIES::setOffsetAsPageNum, Boolean.TRUE);
        pageHelperPropertiesProcessor.bindProperty("rowBoundsWithCount", EASY_MS_PAGE_HELPER_PROPERTIES::setRowBoundsWithCount, Boolean.TRUE);
        pageHelperPropertiesProcessor.bindProperty("pageSizeZero", EASY_MS_PAGE_HELPER_PROPERTIES::setPageSizeZero, Boolean.TRUE);
        pageHelperPropertiesProcessor.bindProperty("reasonable", EASY_MS_PAGE_HELPER_PROPERTIES::setReasonable, Boolean.TRUE);
        pageHelperPropertiesProcessor.bindProperty("supportMethodsArguments", EASY_MS_PAGE_HELPER_PROPERTIES::setSupportMethodsArguments, Boolean.FALSE);
        pageHelperPropertiesProcessor.bindProperty("defaultCount", EASY_MS_PAGE_HELPER_PROPERTIES::setDefaultCount, Boolean.TRUE);
        pageHelperPropertiesProcessor.bindProperty("params", EASY_MS_PAGE_HELPER_PROPERTIES::setParams, "");
        pageHelperPropertiesProcessor.bindProperty("dialectAlias", EASY_MS_PAGE_HELPER_PROPERTIES::setDialectAlias, "");
        pageHelperPropertiesProcessor.bindProperty(EASY_MS_PAGE_HELPER_PROPERTIES::setAutoRuntimeDialect, Boolean.FALSE);
        pageHelperPropertiesProcessor.bindProperty(EASY_MS_PAGE_HELPER_PROPERTIES::setAutoDialect, Boolean.TRUE);
        pageHelperPropertiesProcessor.bindProperty(EASY_MS_PAGE_HELPER_PROPERTIES::setCloseConn, Boolean.TRUE);
    }

    private EasyMsPropertiesHelper() {
    }

}