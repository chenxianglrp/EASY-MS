package com.stars.easyms.datasource.mybatisplus;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.pagination.optimize.JsqlParserCountOptimize;
import org.apache.ibatis.plugin.Interceptor;

/**
 * <p>className: EasyMsMybatisPlusBeanFactory</p>
 * <p>description: EasyMsMybatisPlus相关bean创建工厂类</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/11/12 4:39 下午
 */
public final class EasyMsMybatisPlusBeanFactory {

    private PaginationInterceptor mybatisPlusPageInterceptor;

    public synchronized Interceptor getMybatisPlusPageInterceptor() {
        if (mybatisPlusPageInterceptor == null) {
            mybatisPlusPageInterceptor = new PaginationInterceptor();
            mybatisPlusPageInterceptor.setCountSqlParser(new JsqlParserCountOptimize(true));
        }
        return mybatisPlusPageInterceptor;
    }

    public static EasyMsMybatisPlusBeanFactory getInstance() {
        return EasyMsMybatisPlusBeanFactoryInstanceHolder.INSTANCE;
    }

    private EasyMsMybatisPlusBeanFactory() {
    }

    private static class EasyMsMybatisPlusBeanFactoryInstanceHolder {

        private static final EasyMsMybatisPlusBeanFactory INSTANCE = new EasyMsMybatisPlusBeanFactory();

    }
}
