package com.stars.easyms.datasource.batch;

import com.stars.easyms.base.util.ExceptionUtil;
import com.stars.easyms.base.util.MessageFormatUtil;

/**
 * 执行BatchCommit时抛出的异常
 *
 * @author guoguifang
 * @date 2018-10-19 10:07
 * @since 1.0.0
 */
public final class BatchCommitRuntimeException extends RuntimeException {

    private static final long serialVersionUID = -2134892190245466631L;

    public BatchCommitRuntimeException(String message, Object... args) {
        super(MessageFormatUtil.format(message, ExceptionUtil.trimmedCopy(args)), ExceptionUtil.extractThrowable(args));
    }
}
