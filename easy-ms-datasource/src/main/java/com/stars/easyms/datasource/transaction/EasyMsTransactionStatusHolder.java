package com.stars.easyms.datasource.transaction;

import com.stars.easyms.datasource.mybatis.EasyMsSqlSessionHolder;
import lombok.Getter;
import lombok.NonNull;
import org.springframework.jdbc.datasource.ConnectionHolder;
import org.springframework.transaction.support.DefaultTransactionStatus;
import org.springframework.transaction.support.TransactionSynchronization;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * <p>className: EasyMsTransactionStatusHolder</p>
 * <p>description: EasyMs事务状态holder类</p>
 *
 * @author guoguifang
 * @date 2019-12-05 18:23
 * @since 1.4.2
 */
@Getter
public final class EasyMsTransactionStatusHolder {

    private DefaultTransactionStatus transactionStatus;

    private ConnectionHolder connectionHolder;

    private EasyMsSqlSessionHolder sqlSessionHolder;

    private final Set<TransactionSynchronization> synchronizations;

    EasyMsTransactionStatusHolder() {
        this.synchronizations = new LinkedHashSet<>();
    }

    void setTransactionStatus(@NonNull DefaultTransactionStatus transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    void setConnectionHolder(ConnectionHolder connectionHolder) {
        this.connectionHolder = connectionHolder;
    }

    public void addSynchronizations(TransactionSynchronization synchronization) {
        this.synchronizations.add(synchronization);
    }

    public void setSqlSessionHolder(EasyMsSqlSessionHolder sqlSessionHolder) {
        this.sqlSessionHolder = sqlSessionHolder;
    }
}