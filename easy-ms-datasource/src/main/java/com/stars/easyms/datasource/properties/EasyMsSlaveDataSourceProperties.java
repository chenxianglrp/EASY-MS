package com.stars.easyms.datasource.properties;

import lombok.Data;

/**
 * <p>className: EasyMsDataSourceProperties</p>
 * <p>description: EasyMs默认数据源的属性类</p>
 *
 * @author guoguifang
 * @date 2019-02-22 11:11
 * @version 1.1.0
 */
@Data
public final class EasyMsSlaveDataSourceProperties {

    public static final String DATASOURCE_SLAVE_PREFIX = "slave";

    /**
     * 多个主数据源时使用该配置，之间用','分割，每个数据源格式：username/password@ip:port/database
     */
    private String connect;

    /**
     * 使用mysql时需配置连接参数，若不设置则为默认值
     */
    private String connectParams;

    /**
     * 负载均衡策略：主数据源默认主备模式，从数据源默认最可用模式(active_standby/round_robin/random/best-available)
     * 默认值best-available
     */
    private String loadBalancer;
}