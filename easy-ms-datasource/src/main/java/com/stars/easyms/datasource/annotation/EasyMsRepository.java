package com.stars.easyms.datasource.annotation;

import com.stars.easyms.datasource.common.EasyMsDataSourceConstant;
import com.stars.easyms.datasource.enums.DataSourceType;
import org.springframework.core.annotation.AliasFor;
import org.springframework.stereotype.Repository;

import java.lang.annotation.*;

/**
 * <p>className: EasyMsRepository</p>
 * <p>description: 用于确定该DAO下的所有方法使用的是哪个数据源，只能用于DAO的类上</p>
 *
 * @author guoguifang
 * @date 2019/2/28 16:29
 * @version 1.1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
@Repository
public @interface EasyMsRepository {

    @AliasFor(annotation = Repository.class)
    String value() default "";

    /**
     * 若使用多数据源时需指定使用哪个数据源，若不设置默认主数据源，
     *  该值表示该类中默认使用的数据源，若类中方法需使用其他数据源请使用注解@SpecifyDataSource
     */
    String datasource() default EasyMsDataSourceConstant.DEFAULT_DATASOURCE_NAME;

    /**
     * 选择当前类中默认的查询数据库，
     */
    DataSourceType query() default DataSourceType.SLAVE;
}