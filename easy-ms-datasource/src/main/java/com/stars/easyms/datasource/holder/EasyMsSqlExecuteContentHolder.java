package com.stars.easyms.datasource.holder;

import com.stars.easyms.datasource.SqlExecuteContent;
import org.springframework.core.NamedThreadLocal;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

/**
 * <p>className: EasyMsSqlExecuteContentHolder</p>
 * <p>description: EasyMs当前线程正在处理的sql执行上下文holder类</p>
 *
 * @author guoguifang
 * @date 2019-12-04 18:36
 * @since 1.4.2
 */
public final class EasyMsSqlExecuteContentHolder {

    private static final ThreadLocal<SqlExecuteContent> SQL_EXECUTE_CONTENT_THREAD_LOCAL = new NamedThreadLocal<>("Current sql execute content");

    public static void setCurrentSqlExecuteContent(@NonNull SqlExecuteContent sqlExecuteContent) {
        SQL_EXECUTE_CONTENT_THREAD_LOCAL.set(sqlExecuteContent);
    }

    @Nullable
    public static SqlExecuteContent getCurrentSqlExecuteContent() {
        return SQL_EXECUTE_CONTENT_THREAD_LOCAL.get();
    }

    public static void clear() {
        SQL_EXECUTE_CONTENT_THREAD_LOCAL.remove();
    }

    private EasyMsSqlExecuteContentHolder() {}
}