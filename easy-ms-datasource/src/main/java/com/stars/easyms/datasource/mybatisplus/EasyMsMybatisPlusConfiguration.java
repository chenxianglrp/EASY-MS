package com.stars.easyms.datasource.mybatisplus;

import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.stars.easyms.datasource.mybatis.EasyMsMapperRegistry;
import com.stars.easyms.datasource.mybatis.EasyMsMybatisHelper;
import com.stars.easyms.datasource.mybatis.EasyMsMybatisTypeAliasRegistry;
import org.apache.ibatis.binding.MapperRegistry;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.type.TypeAliasRegistry;
import org.springframework.beans.factory.InitializingBean;

/**
 * <p>className: EasyMsMybatisPlusConfiguration</p>
 * <p>description: mybatis-plus支持的配置类</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/11/5 2:05 下午
 */
public final class EasyMsMybatisPlusConfiguration extends MybatisConfiguration implements InitializingBean {

    private final TypeAliasRegistry easyMsMybatisTypeAliasRegistry = new EasyMsMybatisTypeAliasRegistry();

    private final EasyMsMapperRegistry easyMsMapperRegistry = new EasyMsMybatisPlusMapperRegistry(this);

    @Override
    public TypeAliasRegistry getTypeAliasRegistry() {
        return easyMsMybatisTypeAliasRegistry;
    }

    @Override
    public MapperRegistry getMapperRegistry() {
        return easyMsMapperRegistry;
    }

    @Override
    public void addMappers(String packageName, Class<?> superType) {
        easyMsMapperRegistry.addMappers(packageName, superType);
    }

    @Override
    public void addMappers(String packageName) {
        easyMsMapperRegistry.addMappers(packageName);
    }

    @Override
    public <T> void addMapper(Class<T> type) {
        easyMsMapperRegistry.addMapper(type);
    }

    @Override
    public <T> T getMapper(Class<T> type, SqlSession sqlSession) {
        return easyMsMapperRegistry.getMapper(type, sqlSession);
    }

    @Override
    public boolean hasMapper(Class<?> type) {
        return easyMsMapperRegistry.hasMapper(type);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        EasyMsMybatisHelper.loadCustomLogImpl(this);
    }
}