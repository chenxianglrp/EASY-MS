package com.stars.easyms.datasource.loadbalancer;

import com.stars.easyms.datasource.EasyMsDataSource;
import com.stars.easyms.datasource.EasyMsDataSourceSet;

import java.util.concurrent.ThreadLocalRandom;

/**
 * <p>className: RandomDataSourceLoadBalancer</p>
 * <p>description: 随机模式数据源负载均衡器：随机模式：随机选取数据库其中之一进行操作</p>
 *
 * @author guoguifang
 * @version 1.2.1
 * @date 2019-05-13 13:53
 */
final class RandomDataSourceLoadBalancer implements DataSourceLoadBalancer {

    @Override
    public EasyMsDataSource getDataSource(EasyMsDataSourceSet easyMsDataSourceSet) {
        return easyMsDataSourceSet.get(ThreadLocalRandom.current().nextInt(easyMsDataSourceSet.size()));
    }
}