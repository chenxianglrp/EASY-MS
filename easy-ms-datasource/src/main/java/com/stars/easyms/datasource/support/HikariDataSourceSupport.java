package com.stars.easyms.datasource.support;

import com.stars.easyms.datasource.enums.DatabaseType;
import com.stars.easyms.datasource.properties.EasyMsDataSourcePropertiesProcessor;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * <p>className: HikariDataSourceSupport</p>
 * <p>description: HikariDataSource支持类</p>
 *
 * @author guoguifang
 * @version 1.2.1
 * @date 2019-05-14 17:05
 */
public final class HikariDataSourceSupport implements DataSourceSupport<HikariDataSource> {

    @Override
    public DataSource createDataSource(EasyMsDataSourcePropertiesProcessor propertiesProcessor, DatabaseType databaseType, String driverClassName, boolean isMaster) {
        HikariDataSource baseDataSource = new HikariDataSource();
        baseDataSource.setDriverClassName(driverClassName);
        Properties properties = propertiesProcessor.getProperties(databaseType, isMaster);
        String property = properties.getProperty("druid.maxWait");
        return baseDataSource;
    }

    @Override
    public DataSource cloneDataSource(HikariDataSource hikariDataSource) {
        return hikariDataSource;
    }

    @Override
    public void setConnectParams(HikariDataSource hikariDataSource, String url, String username, String password) {
        hikariDataSource.setJdbcUrl(url);
        hikariDataSource.setUsername(username);
        hikariDataSource.setPassword(password);
    }

    @Override
    public boolean isValid(HikariDataSource dataSource) {
        return dataSource.isRunning();
    }

}