package com.stars.easyms.datasource.mybatis;

import com.stars.easyms.base.util.ClassUtil;
import com.stars.easyms.datasource.enums.MybatisLogTypeEnum;
import com.stars.easyms.datasource.properties.EasyMsPropertiesHelper;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.logging.Log;
import org.apache.ibatis.session.Configuration;
import org.springframework.util.ClassUtils;

/**
 * <p>className: EasyMsMybatisHelper</p>
 * <p>description: EasyMsMybatis帮助类</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/11/19 5:37 下午
 */
@SuppressWarnings("unchecked")
public final class EasyMsMybatisHelper {

    public static void loadCustomLogImpl(Configuration configuration) {
        String mybatisLogImpl = EasyMsPropertiesHelper.getEasyMsMybatisProperties().getLogImpl();
        if (StringUtils.isBlank(mybatisLogImpl)) {
            return;
        }
        Class<? extends Log> mybatisLogImplClass = MybatisLogTypeEnum.getMybatisLogImplClass(mybatisLogImpl);
        if (mybatisLogImplClass == null && ClassUtil.isExist(mybatisLogImpl)) {
            Class<?> resolveClass = ClassUtils.resolveClassName(mybatisLogImpl, ClassUtils.getDefaultClassLoader());
            if (Log.class.isAssignableFrom(resolveClass)) {
                mybatisLogImplClass = (Class<? extends Log>) resolveClass;
            }
        }
        if (mybatisLogImplClass != null) {
            configuration.setLogImpl(mybatisLogImplClass);
        }
    }

    private EasyMsMybatisHelper() {
    }
}
