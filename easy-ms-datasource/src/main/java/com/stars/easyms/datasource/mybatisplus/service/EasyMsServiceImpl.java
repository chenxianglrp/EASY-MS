package com.stars.easyms.datasource.mybatisplus.service;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.ExceptionUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.stars.easyms.base.bean.LazyLoadBean;
import com.stars.easyms.datasource.mybatis.EasyMsSqlSessionTemplate;
import com.stars.easyms.datasource.transaction.EasyMsTransactionSynchronizationManager;
import org.apache.ibatis.reflection.ExceptionUtil;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.MyBatisExceptionTranslator;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import java.util.function.Consumer;

/**
 * <p>className: EasyMsServiceImpl</p>
 * <p>description: EasyMs自定义service实现类</p>
 *
 * @author guoguifang
 * @version 1.7.3
 * @date 2021/3/29 10:00 上午
 */
public class EasyMsServiceImpl<M extends BaseMapper<T>, T> extends ServiceImpl<M, T> {

    private static final LazyLoadBean<PlatformTransactionManager> transactionManager =
            new LazyLoadBean<>(PlatformTransactionManager.class);

    private static final LazyLoadBean<EasyMsSqlSessionTemplate> sqlSessionTemplate =
            new LazyLoadBean<>(EasyMsSqlSessionTemplate.class);

    @Override
    protected boolean executeBatch(Consumer<SqlSession> consumer) {

        // 默认executeBatch必须有事务
        TransactionStatus transactionStatus = null;
        // 判断当前是否有事务，如果没有事务则创建一个手动事务，使用 PROPAGATION_REQUIRES_NEW 传播级别
        if (!EasyMsTransactionSynchronizationManager.isActualTransactionActive()) {
            transactionStatus = transactionManager.apply(t ->
                    t.getTransaction(new DefaultTransactionDefinition(TransactionDefinition.PROPAGATION_REQUIRES_NEW)));
        }

        try {
            sqlSessionTemplate.acceptWithThrowable(template -> template.acceptWithSqlSession(consumer::accept));

            // 如果是手动事务需要主动commit
            if (transactionStatus != null) {
                final TransactionStatus finalTransactionStatus = transactionStatus;
                transactionManager.accept(m -> m.commit(finalTransactionStatus));
            }
            return true;
        } catch (Throwable t) {

            // 如果是手动事务需要主动rollback
            if (transactionStatus != null) {
                final TransactionStatus finalTransactionStatus = transactionStatus;
                transactionManager.accept(m -> m.rollback(finalTransactionStatus));
            }

            // 处理异常情况
            Throwable unwrapped = ExceptionUtil.unwrapThrowable(t);
            if (unwrapped instanceof RuntimeException) {
                MyBatisExceptionTranslator myBatisExceptionTranslator
                        = new MyBatisExceptionTranslator(
                        SqlHelper.sqlSessionFactory(entityClass).getConfiguration().getEnvironment().getDataSource(), true);
                DataAccessException dataAccessException = myBatisExceptionTranslator.translateExceptionIfPossible((RuntimeException) unwrapped);
                if (dataAccessException != null) {
                    throw dataAccessException;
                }
            }
            throw ExceptionUtils.mpe(unwrapped);
        }
    }

}
