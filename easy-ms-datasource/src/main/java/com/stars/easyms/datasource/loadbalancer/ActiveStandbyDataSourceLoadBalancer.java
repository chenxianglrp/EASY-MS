package com.stars.easyms.datasource.loadbalancer;

import com.stars.easyms.datasource.EasyMsDataSource;
import com.stars.easyms.datasource.EasyMsDataSourceSet;

/**
 * <p>className: ActiveStandbyDataSourceLoadBalancer</p>
 * <p>description: 主备模式数据源负载均衡器：主备模式：第一个地址为主，其他为备，当主库挂了之后依次切换备库，master默认策略</p>
 *
 * @author guoguifang
 * @version 1.2.1
 * @date 2019-05-13 13:48
 */
final class ActiveStandbyDataSourceLoadBalancer implements DataSourceLoadBalancer {

    @Override
    public EasyMsDataSource getDataSource(EasyMsDataSourceSet easyMsDataSourceSet) {
        return easyMsDataSourceSet.get(0);
    }
}