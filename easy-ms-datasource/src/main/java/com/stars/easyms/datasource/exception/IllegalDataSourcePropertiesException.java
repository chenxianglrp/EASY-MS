package com.stars.easyms.datasource.exception;

import com.stars.easyms.base.util.ExceptionUtil;
import com.stars.easyms.base.util.MessageFormatUtil;

/**
 * <p>className: IllegalDataSourceException</p>
 * <p>description: 无效的数据源属性文件异常</p>
 *
 * @author guoguifang
 * @version 1.2.1
 * @date 2019-06-06 14:15
 */
public final class IllegalDataSourcePropertiesException extends RuntimeException {

    private static final long serialVersionUID = -3134894194345463631L;

    public IllegalDataSourcePropertiesException(String message, Object... args) {
        super(MessageFormatUtil.format(message, ExceptionUtil.trimmedCopy(args)), ExceptionUtil.extractThrowable(args));
    }
}