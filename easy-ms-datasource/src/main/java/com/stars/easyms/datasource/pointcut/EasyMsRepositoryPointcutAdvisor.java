package com.stars.easyms.datasource.pointcut;

import com.stars.easyms.base.pointcut.EasyMsAnnotationMatchingPointcut;
import com.stars.easyms.datasource.EasyMsMultiDataSource;
import com.stars.easyms.datasource.common.EasyMsDataSourceConstant;
import com.stars.easyms.datasource.interceptor.EasyMsRepositoryInterceptor;
import org.aopalliance.aop.Advice;
import org.springframework.aop.Pointcut;
import org.springframework.aop.support.AbstractPointcutAdvisor;
import org.springframework.lang.NonNull;
import org.springframework.util.ObjectUtils;

/**
 * <p>className: EasyMsRepositoryPointcutAdvisor</p>
 * <p>description: 注解EasyMsRepository的顾问类</p>
 *
 * @author guoguifang
 * @version 1.1.0
 * @date 2019-03-04 10:52
 */
public final class EasyMsRepositoryPointcutAdvisor extends AbstractPointcutAdvisor {

    private transient Advice advice;

    private transient EasyMsAnnotationMatchingPointcut pointcut;

    public EasyMsRepositoryPointcutAdvisor(EasyMsMultiDataSource easyMsMultiDataSource) {
        this.advice = new EasyMsRepositoryInterceptor(easyMsMultiDataSource);
        this.pointcut = new EasyMsAnnotationMatchingPointcut();
        this.pointcut.setClassFilter(new EasyMsDataSourceAnnotationClassFilter(EasyMsDataSourceConstant.SUPPORT_ANNOTATION_TYPE_LIST));
        super.setOrder(40);
    }

    @NonNull
    @Override
    public Pointcut getPointcut() {
        return this.pointcut;
    }

    @NonNull
    @Override
    public Advice getAdvice() {
        return this.advice;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || this.getClass() != other.getClass()) {
            return false;
        }
        EasyMsRepositoryPointcutAdvisor otherAdvisor = (EasyMsRepositoryPointcutAdvisor) other;
        return (ObjectUtils.nullSafeEquals(this.advice, otherAdvisor.advice) &&
                ObjectUtils.nullSafeEquals(this.pointcut, otherAdvisor.pointcut));
    }

    @Override
    public int hashCode() {
        return 41 + this.advice.hashCode() * 41 + this.pointcut.hashCode() * 41;
    }
}