package com.stars.easyms.datasource.mybatis;

import com.stars.easyms.datasource.jdbc.EasyMsSqlErrorCodeSqlExceptionTranslator;
import org.apache.ibatis.exceptions.PersistenceException;
import org.mybatis.spring.MyBatisSystemException;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.support.PersistenceExceptionTranslator;
import org.springframework.jdbc.support.SQLExceptionTranslator;
import org.springframework.transaction.TransactionException;

import javax.sql.DataSource;
import java.sql.SQLException;

/**
 * <p>className: EasyMsMyBatisExceptionTranslator</p>
 * <p>description: EasyMs自定义MyBatisExceptionTranslator类</p>
 *
 * @author guoguifang
 * @date 2019-11-22 20:07
 * @since 1.4.1
 */
public final class EasyMsMyBatisExceptionTranslator implements PersistenceExceptionTranslator {

    private final DataSource dataSource;

    private SQLExceptionTranslator exceptionTranslator;

    EasyMsMyBatisExceptionTranslator(DataSource dataSource, boolean exceptionTranslatorLazyInit) {
        this.dataSource = dataSource;
        if (!exceptionTranslatorLazyInit) {
            this.initExceptionTranslator();
        }
    }

    @Override
    public DataAccessException translateExceptionIfPossible(RuntimeException e) {
        if (e instanceof PersistenceException) {
            if (e.getCause() instanceof PersistenceException) {
                e = (PersistenceException) e.getCause();
            }
            if (e.getCause() instanceof SQLException) {
                this.initExceptionTranslator();
                return this.exceptionTranslator.translate(e.getMessage() + "\n", null, (SQLException) e.getCause());
            } else if (e.getCause() instanceof TransactionException) {
                throw (TransactionException) e.getCause();
            }
            return new MyBatisSystemException(e);
        }
        return null;
    }

    private synchronized void initExceptionTranslator() {
        if (this.exceptionTranslator == null) {
            this.exceptionTranslator = new EasyMsSqlErrorCodeSqlExceptionTranslator(this.dataSource);
        }
    }
}