package com.stars.easyms.datasource.transaction;

import org.springframework.lang.NonNull;
import org.springframework.transaction.support.AbstractTransactionStatus;

/**
 * <p>className: EasyMsTransactionStatus</p>
 * <p>description: EasyMs事务状态类</p>
 *
 * @author guoguifang
 * @date 2019-12-16 18:11
 * @since 1.4.2
 */
public final class EasyMsTransactionStatus extends AbstractTransactionStatus {

    private final EasyMsTransactionManager transactionManager;

    private final boolean newTransaction;

    EasyMsTransactionStatus(@NonNull EasyMsTransactionManager transactionManager, boolean newTransaction) {
        this.transactionManager = transactionManager;
        this.newTransaction = newTransaction;
    }

    @NonNull
    EasyMsTransactionManager getTransactionManager() {
        return this.transactionManager;
    }

    @Override
    public boolean isNewTransaction() {
        return this.newTransaction;
    }

    @Override
    public boolean isRollbackOnly() {
        return this.transactionManager.isRollbackOnly() || super.isRollbackOnly();
    }
}