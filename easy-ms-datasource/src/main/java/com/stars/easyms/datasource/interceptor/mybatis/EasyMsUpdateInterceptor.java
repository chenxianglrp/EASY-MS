package com.stars.easyms.datasource.interceptor.mybatis;

import com.stars.easyms.datasource.EasyMsMultiDataSource;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.*;
import org.springframework.core.annotation.Order;

/**
 * <p>className: EasyMsUpdateInterceptor</p>
 * <p>description: EasyMs增删改拦截器：增删改操作不管是否增加了走从数据源的注解都必须强制走主数据源</p>
 *
 * @author guoguifang
 * @version 1.1.0
 * @date 2019-03-04 14:47
 */
@Order(1)
@Intercepts({@Signature(type = Executor.class, method = "update", args = {MappedStatement.class, Object.class})})
public final class EasyMsUpdateInterceptor extends BaseEasyMsMybatisInterceptor {

    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        return determineDataSourceAndExecute(invocation, true);
    }

    public EasyMsUpdateInterceptor(EasyMsMultiDataSource easyMsMultiDataSource) {
        super(easyMsMultiDataSource);
    }

}