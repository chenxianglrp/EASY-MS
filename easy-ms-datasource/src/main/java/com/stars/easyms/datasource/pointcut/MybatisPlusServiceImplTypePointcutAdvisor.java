package com.stars.easyms.datasource.pointcut;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.stars.easyms.datasource.interceptor.mybatisplus.EasyMsMybatisPlusServiceImplInterceptor;
import org.aopalliance.aop.Advice;
import org.springframework.aop.ClassFilter;
import org.springframework.aop.MethodMatcher;
import org.springframework.aop.Pointcut;
import org.springframework.aop.support.AbstractPointcutAdvisor;
import org.springframework.lang.NonNull;
import org.springframework.util.ObjectUtils;

/**
 * <p>className: MybatisPlusServiceImplTypePointcutAdvisor</p>
 * <p>description: Mybatis-plus的继承serviceImpl的实现类拦截器</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/11/21 11:24 上午
 */
public final class MybatisPlusServiceImplTypePointcutAdvisor extends AbstractPointcutAdvisor {

    private transient Advice advice;

    private transient Pointcut pointcut;

    public MybatisPlusServiceImplTypePointcutAdvisor() {
        this.advice = new EasyMsMybatisPlusServiceImplInterceptor();
        this.pointcut = new MybatisPlusServiceImplTypePointcut();
        super.setOrder(-10);
    }

    @NonNull
    @Override
    public Pointcut getPointcut() {
        return this.pointcut;
    }

    @NonNull
    @Override
    public Advice getAdvice() {
        return this.advice;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || this.getClass() != other.getClass()) {
            return false;
        }
        MybatisPlusServiceImplTypePointcutAdvisor otherAdvisor = (MybatisPlusServiceImplTypePointcutAdvisor) other;
        return (ObjectUtils.nullSafeEquals(this.advice, otherAdvisor.advice) &&
                ObjectUtils.nullSafeEquals(this.pointcut, otherAdvisor.pointcut));
    }

    @Override
    public int hashCode() {
        return 41 + this.advice.hashCode() * 41 + this.pointcut.hashCode() * 41;
    }

    private static class MybatisPlusServiceImplTypePointcut implements Pointcut {

        @Override
        public ClassFilter getClassFilter() {
            return ServiceImpl.class::isAssignableFrom;
        }

        @Override
        public MethodMatcher getMethodMatcher() {
            return MethodMatcher.TRUE;
        }
    }
}