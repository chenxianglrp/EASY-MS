package com.stars.easyms.datasource.holder;

import org.springframework.core.NamedThreadLocal;

/**
 * <p>className: EasyMsMasterSlaveHolder</p>
 * <p>description: EasyMs主从切换器</p>
 *
 * @author guoguifang
 * @version 1.1.0
 * @date 2019-02-28 17:02
 */
public final class EasyMsMasterSlaveHolder {

    private static final ThreadLocal<String> SWITCHER = new NamedThreadLocal<>("Master and Slave switch");

    private static final String MASTER = "master";

    private static final String SLAVE = "slave";

    public static void switchMasterDataSource() {
        SWITCHER.set(MASTER);
    }

    public static void switchSlaveDataSource() {
        SWITCHER.set(SLAVE);
    }

    public static boolean isNotSwitch() {
        return SWITCHER.get() == null;
    }

    public static boolean isAlreadySwitchAndUseMaster() {
        return MASTER.equals(SWITCHER.get());
    }

    public static void clear(){
        SWITCHER.remove();
    }

    private EasyMsMasterSlaveHolder() {}
}