package com.stars.easyms.datasource.holder;

import org.springframework.core.NamedThreadLocal;

/**
 * <p>className: EasyMsInterceptorSkipHolder</p>
 * <p>description: 过滤器是否跳过Holder类</p>
 *
 * @author guoguifang
 * @date 2019/9/19 10:28
 * @since 1.3.2
 */
public final class EasyMsInterceptorSkipHolder {

    private static final ThreadLocal<Boolean> HOLDER = new NamedThreadLocal<>("Whether to skip Interceptor");

    public static void skip() {
        HOLDER.set(Boolean.TRUE);
    }

    public static boolean isSkip() {
        return Boolean.TRUE.equals(HOLDER.get());
    }

    public static void clear(){
        HOLDER.remove();
    }

    private EasyMsInterceptorSkipHolder() {}
}