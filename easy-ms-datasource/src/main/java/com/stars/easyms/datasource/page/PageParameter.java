package com.stars.easyms.datasource.page;

import lombok.Data;

/**
 * <p>className: PageParameter</p>
 * <p>description: 分页参数</p>
 *
 * @author guoguifang
 * @version 1.2.2
 * @date 2019-07-17 19:43
 */
@Data
public class PageParameter {

    private int currentPage;

    private int pageSize;

    private int totalPage;

    private int totalCount;

    public PageParameter() {
    }

    public PageParameter(int currentPage, int pageSize) {
        this.currentPage = currentPage;
        this.pageSize = pageSize;
    }
}