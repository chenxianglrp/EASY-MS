package com.stars.easyms.datasource.properties;

import lombok.Getter;

import java.util.Collections;
import java.util.Set;

/**
 * <p>className: EasyMsDataSourceProperties</p>
 * <p>description: EasyMs数据源属性类</p>
 *
 * @author guoguifang
 * @date 2019-02-22 11:11
 * @version 1.1.0
 */
@Getter
public final class EasyMsDataSourceProperties {

    public static final String MULTI_DATASOURCE_ACTIVE = "spring.multi-datasource-active";

    public static final String MULTI_DATASOURCE_ACTIVE_STANDBY = "spring.multi-datasource.active";

    public static final String DATASOURCE_NAME_PREFIX = "spring.datasource";

    /**
     * 激活的多数据源名称，不同名称之间用','分割
     */
    private Set<String> multiDatasourceActive;

    void setMultiDatasourceActive(Set<String> multiDatasourceActiveSet) {
        this.multiDatasourceActive = Collections.unmodifiableSet(multiDatasourceActiveSet);
    }

    EasyMsDataSourceProperties() {}
}