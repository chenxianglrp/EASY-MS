package com.stars.easyms.datasource.exception;

import com.stars.easyms.base.util.ExceptionUtil;
import com.stars.easyms.base.util.MessageFormatUtil;

/**
 * <p>className: EasyMsQuerySqlException</p>
 * <p>description: 区分SqlException的异常类</p>
 *
 * @author guoguifang
 * @version 1.2.2
 * @date 2019-08-08 13:51
 */
public class EasyMsQuerySqlException extends RuntimeException {

    private static final long serialVersionUID = -1234894190345465631L;

    public EasyMsQuerySqlException(Throwable throwable) {
        super(throwable);
    }

    public EasyMsQuerySqlException(String message, Object... args) {
        super(MessageFormatUtil.format(message, ExceptionUtil.trimmedCopy(args)), ExceptionUtil.extractThrowable(args));
    }

}