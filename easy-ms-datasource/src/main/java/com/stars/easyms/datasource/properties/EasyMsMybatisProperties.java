package com.stars.easyms.datasource.properties;

import lombok.Getter;

import java.util.List;

/**
 * EasyMs的Mybatis属性类型
 *
 * @author guoguifang
 * @date 2018-10-19 10:07
 * @since 1.0.0
 */
@Getter
public final class EasyMsMybatisProperties {

    public static final String MYBATIS_PREFIX = "spring.mybatis";

    /**
     * mybatis的mapper文件的扫描路径，不同路径之间用','分割，每个路径以'classpath*:'开头
     */
    private List<String> mapperLocations;

    /**
     * mybatis仓库的扫描路径
     */
    private String typeAliasesPackage;

    /**
     * 日志实现方式
     *
     * @see com.stars.easyms.datasource.enums.MybatisLogTypeEnum
     */
    private String logImpl;

    void setMapperLocations(List<String> mapperLocations) {
        this.mapperLocations = mapperLocations;
    }

    void setTypeAliasesPackage(String typeAliasesPackage) {
        this.typeAliasesPackage = typeAliasesPackage;
    }

    void setLogImpl(String logImpl) {
        this.logImpl = logImpl;
    }

    EasyMsMybatisProperties() {}
}
