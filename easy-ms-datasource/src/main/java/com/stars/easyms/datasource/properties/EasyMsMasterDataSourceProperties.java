package com.stars.easyms.datasource.properties;

import lombok.Data;

/**
 * <p>className: EasyMsDataSourceProperties</p>
 * <p>description: EasyMs默认数据源的属性类</p>
 *
 * @author guoguifang
 * @date 2019-02-22 11:11
 * @version 1.1.0
 */
@Data
public final class EasyMsMasterDataSourceProperties {

    public static final String DATASOURCE_MASTER_PREFIX = "master";

    /**
     * 数据源驱动类名
     */
    private String driverClassName;

    /**
     * 数据源连接地址
     */
    private String url;

    /**
     * 数据源连接用户名
     */
    private String username;

    /**
     * 数据源连接密码
     */
    private String password;

    /**
     * 数据库类型:oracle/mysql/db2/sqlserver_2000/sqlserver_2005/db2/postgresql
     * 默认值mysql
     */
    private String dbType;

    /**
     * 多个主数据源时使用该配置，之间用','分割，每个数据源格式：username/password@ip:port/database
     */
    private String connect;

    /**
     * 使用mysql时需配置连接参数，若不设置则为默认值
     */
    private String connectParams;

    /**
     * 负载均衡策略：主数据源默认主备模式，从数据源默认最可用模式(active_standby/round_robin/random/best-available)
     * 默认值active_standby
     */
    private String loadBalancer;

    /**
     * 初始化时建立物理连接的个数。初始化发生在显示调用init方法，或者第一次getConnection时
     * 默认值5
     */
    private Integer initialSize;

    /**
     * 最小连接池数量
     * 默认值5
     */
    private Integer minIdle;

    /**
     * 最大连接池数量
     * 默认值300
     */
    private Integer maxActive;

    /**
     * 获取连接时最大等待时间，单位毫秒。配置了maxWait之后，缺省启用公平锁，并发效率会有所下降，如果需要可以通过配置useUnfairLock属性为true使用非公平锁。
     * 默认值60000
     */
    private Long maxWait;

    /**
     * 是否使用非公平锁
     * 默认值false
     */
    private Boolean useUnfairLock;

    /**
     * 配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒
     * 默认值60000
     */
    private Long timeBetweenEvictionRunsMillis;

    /**
     * 配置一个连接在池中最小生存的时间，单位是毫秒
     * 默认值300000
     */
    private Long minEvictableIdleTimeMillis;

    /**
     * 用来检测连接是否有效的sql，要求是一个查询语句
     * 默认值SELECT 1
     */
    private String validationQuery;

    /**
     * 单位：秒，检测连接是否有效的超时时间。底层调用jdbc Statement对象的void setQueryTimeout(int seconds)方法
     * 默认值1
     */
    private String validationQueryTimeout;

    /**
     * 建议配置为true，不影响性能，并且保证安全性。申请连接的时候检测，如果空闲时间大于timeBetweenEvictionRunsMillis，执行validationQuery检测连接是否有效。
     * 默认值true
     */
    private Boolean testWhileIdle;

    /**
     * 申请连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能。
     * 默认值false
     */
    private Boolean testOnBorrow;

    /**
     * 归还连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能
     * 默认值false
     */
    private Boolean testOnReturn;

    /**
     * 是否缓存preparedStatement，也就是PSCache。PSCache对支持游标的数据库性能提升巨大，比如说oracle。在mysql下建议关闭。
     * 默认值true
     */
    private Boolean poolPreparedStatements;

    /**
     * 指定每个连接上PSCache的大小
     * 默认值20
     */
    private Integer maxPoolPreparedStatementPerConnectionSize;

    /**
     * 配置监控统计拦截的filters，去掉后监控界面sql无法统计，'wall'用于防火墙
     * 默认值：stat,wall,log4j
     */
    private String filters;

    /**
     * 通过connectProperties属性来打开mergeSql功能；慢SQL记录
     * 默认值druid.stat.mergeSql=true;druid.stat.slowSqlMillis=5000
     */
    private String connectionProperties;

    /**
     * 合并多个DruidDataSource的监控数据
     * 默认值false
     */
    private Boolean useGlobalDataSourceStat;

    /**
     * 连接池中的minIdle数量以内的连接，空闲时间超过minEvictableIdleTimeMillis，则会执行keepAlive操作。
     * 默认值true
     */
    private Boolean keepAlive;

    /**
     * mysql有效，当sql查询超时时是否kill掉mysql数据库当前线程的sql查询(只会kill掉SQL查询，不会kill掉连接)
     * 默认值false
     */
    private Boolean killWhenSocketReadTimeout;

    /**
     * 慢SQL界线，单位毫秒，超过该值的sql执行时间将会告警，默认为1000
     */
    private Long slowSqlLimit;

}
