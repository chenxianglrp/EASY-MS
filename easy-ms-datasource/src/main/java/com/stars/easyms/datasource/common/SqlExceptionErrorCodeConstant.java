package com.stars.easyms.datasource.common;

import java.util.HashSet;
import java.util.Set;

/**
 * <p>className: SqlExceptionErrorCodeConstant</p>
 * <p>description: sql异常错误码常量类</p>
 *
 * @author guoguifang
 * @date 2019-10-11 10:48
 * @since 1.3.3
 */
public final class SqlExceptionErrorCodeConstant {

    public static final int SQL_EXECUTE_FAIL_STATUS = -3;

    /**
     * oracle：执行SQL失败后重试的错误码集合
     */
    public static final Set<Integer> ORACLE_ERROR_CODE_RETRY = new HashSet<>();

    /**
     * oracle：执行SQL失败后直接抛出异常方式的错误码集合
     */
    public static final Set<Integer> ORACLE_ERROR_CODE_THROW = new HashSet<>();

    /**
     * oracle：执行SQL失败后重新使用单条提交方式重新提交的错误码集合，除该集合外的错误码剩余重试sql继续使用批量提交的方式提交
     */
    public static final Set<Integer> ORACLE_ERROR_CODE_SINGLE = new HashSet<>();

    /**
     * mysql：执行SQL失败后直接抛出异常方式的错误码集合，除该集合外的错误码全部使用重新单条提交的方式重新提交
     */
    public static final Set<Integer> MYSQL_ERROR_CODE_THROW = new HashSet<>();

    static {
        // 超出打开游标的最大数
        ORACLE_ERROR_CODE_RETRY.add(1000);

        // 表或视图不存在
        ORACLE_ERROR_CODE_THROW.add(942);

        // 超出打开游标的最大数
        ORACLE_ERROR_CODE_SINGLE.add(1000);
        // 无效数字
        ORACLE_ERROR_CODE_SINGLE.add(1722);

        // 表不存在
        MYSQL_ERROR_CODE_THROW.add(1146);
    }

    private SqlExceptionErrorCodeConstant() {}

}