package com.stars.easyms.datasource.holder;

import org.apache.ibatis.session.ExecutorType;
import org.springframework.core.NamedThreadLocal;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

/**
 * <p>className: EasyMsMybatisPlusHolder</p>
 * <p>description: EasyMsMybatisPlus相关</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/11/11 8:27 下午
 */
public final class EasyMsMybatisPlusHolder {

    private static final ThreadLocal<Boolean> IS_MYBATIS_PLUS_METHOD_HOLDER = new NamedThreadLocal<>("Whether to mybatis-plus");

    private static final ThreadLocal<ExecutorType> EXECUTOR_TYPE_HOLDER = new NamedThreadLocal<>("mybatis-plus executorType");

    public static void setCurrIsMybatisPlusMethod() {
        IS_MYBATIS_PLUS_METHOD_HOLDER.set(Boolean.TRUE);
    }

    public static void setExecutorType(@NonNull ExecutorType executorType) {
        EXECUTOR_TYPE_HOLDER.set(executorType);
    }

    public static boolean isMybatisPlusMethod() {
        return Boolean.TRUE.equals(IS_MYBATIS_PLUS_METHOD_HOLDER.get());
    }

    @Nullable
    public static ExecutorType getExecutorType() {
        return EXECUTOR_TYPE_HOLDER.get();
    }

    public static void clear(){
        EXECUTOR_TYPE_HOLDER.remove();
        IS_MYBATIS_PLUS_METHOD_HOLDER.remove();
    }

    private EasyMsMybatisPlusHolder() {}
}