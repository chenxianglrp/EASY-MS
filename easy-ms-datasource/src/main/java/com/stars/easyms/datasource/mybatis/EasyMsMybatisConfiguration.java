package com.stars.easyms.datasource.mybatis;

import org.apache.ibatis.binding.MapperRegistry;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.type.TypeAliasRegistry;
import org.springframework.beans.factory.InitializingBean;

/**
 * <p>className: EasyMsMybatisConfiguration</p>
 * <p>description: EasyMs的Mybatis配置处理类</p>
 *
 * @author guoguifang
 * @version 1.2.1
 * @date 2019-07-10 11:06
 */
public final class EasyMsMybatisConfiguration extends Configuration implements InitializingBean {

    private final TypeAliasRegistry easyMsMybatisTypeAliasRegistry = new EasyMsMybatisTypeAliasRegistry();

    private final EasyMsMapperRegistry easyMsMapperRegistry = new EasyMsMybatisMapperRegistry(this);

    @Override
    public TypeAliasRegistry getTypeAliasRegistry() {
        return easyMsMybatisTypeAliasRegistry;
    }

    @Override
    public MapperRegistry getMapperRegistry() {
        return easyMsMapperRegistry;
    }

    @Override
    public void addMappers(String packageName, Class<?> superType) {
        easyMsMapperRegistry.addMappers(packageName, superType);
    }

    @Override
    public void addMappers(String packageName) {
        easyMsMapperRegistry.addMappers(packageName);
    }

    @Override
    public <T> void addMapper(Class<T> type) {
        easyMsMapperRegistry.addMapper(type);
    }

    @Override
    public <T> T getMapper(Class<T> type, SqlSession sqlSession) {
        return easyMsMapperRegistry.getMapper(type, sqlSession);
    }

    @Override
    public boolean hasMapper(Class<?> type) {
        return easyMsMapperRegistry.hasMapper(type);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        EasyMsMybatisHelper.loadCustomLogImpl(this);
    }
}