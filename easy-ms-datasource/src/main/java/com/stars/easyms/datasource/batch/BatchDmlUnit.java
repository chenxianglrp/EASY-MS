package com.stars.easyms.datasource.batch;

import lombok.Getter;

/**
 * 需要批量处理的DML封装信息，一个单元存放一条数据
 *
 * @author guoguifang
 * @date 2018-10-19 10:07
 * @since 1.0.0
 */
@Getter
public class BatchDmlUnit<T> {

    private Class<?> daoClass;

    private String methodName;

    private String sqlId;

    private T obj;

    public BatchDmlUnit(String sqlId, T obj) {
        this.sqlId = sqlId;
        this.obj = obj;
    }

    public BatchDmlUnit(Class<?> daoClass, String methodName, T obj) {
        this.daoClass = daoClass;
        this.methodName = methodName;
        this.obj = obj;
    }
}
