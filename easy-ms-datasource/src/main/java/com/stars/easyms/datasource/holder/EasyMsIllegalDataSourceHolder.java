package com.stars.easyms.datasource.holder;

import com.stars.easyms.datasource.EasyMsDataSource;
import org.springframework.lang.NonNull;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.locks.ReentrantLock;

/**
 * <p>className: EasyMsIllegalDataSourceHolder</p>
 * <p>description: 无效数据源的持有者</p>
 *
 * @author guoguifang
 * @version 1.2.1
 * @date 2019-06-06 14:56
 */
public final class EasyMsIllegalDataSourceHolder {

    private static final Set<EasyMsDataSource> ILLEGAL_DATA_SOURCE_SET = new HashSet<>();

    private static final ReentrantLock LOCK = new ReentrantLock();

    public static void putDataSource(@NonNull EasyMsDataSource dataSource) {
        LOCK.lock();
        try {
            ILLEGAL_DATA_SOURCE_SET.add(dataSource);
        } finally {
            LOCK.unlock();
        }
    }

    @NonNull
    public static Set<EasyMsDataSource> get() {
        if (!ILLEGAL_DATA_SOURCE_SET.isEmpty()) {
            LOCK.lock();
            try {
                return new HashSet<>(ILLEGAL_DATA_SOURCE_SET);
            } finally {
                LOCK.unlock();
            }
        }
        return Collections.emptySet();
    }

    private EasyMsIllegalDataSourceHolder() {
    }
}