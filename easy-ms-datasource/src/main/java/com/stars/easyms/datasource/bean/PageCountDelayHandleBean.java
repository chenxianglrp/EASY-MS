package com.stars.easyms.datasource.bean;

import com.github.pagehelper.Dialect;
import com.stars.easyms.datasource.page.PageParameter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.ibatis.session.RowBounds;

import java.util.concurrent.Future;

/**
 * <p>className: PageCountDelayHandleBean</p>
 * <p>description: 分页查询延迟处理bean</p>
 *
 * @author guoguifang
 * @date 2019-09-20 15:17
 * @since 1.3.2
 */
@Getter
@AllArgsConstructor
public class PageCountDelayHandleBean {

    private final String countMsId;

    private final Future<Long> countFuture;

    private final long defaultCount;

    private final Object pageObject;

    private final PageParameter pageParameter;

    private final Dialect dialect;

    private final Object parameter;

    private final RowBounds rowBounds;

}