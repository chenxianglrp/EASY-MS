package com.stars.easyms.datasource.interceptor.mybatis;

import com.stars.easyms.datasource.EasyMsMultiDataSource;
import org.apache.ibatis.cache.CacheKey;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.springframework.core.annotation.Order;

/**
 * <p>className: EasyMsQueryInterceptor</p>
 * <p>description: EasyMs主从数据库读写分离拦截器</p>
 *
 * @author guoguifang
 * @version 1.1.0
 * @date 2019-02-27 10:05
 */
@Order(0)
@Intercepts({@Signature(type = Executor.class, method = "query", args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class}),
        @Signature(type = Executor.class, method = "query", args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class, CacheKey.class, BoundSql.class})})
public final class EasyMsQueryInterceptor extends BaseEasyMsMybatisInterceptor {

    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        // 判断是否已经切换主从数据源，若未切换则query默认是从数据源
        return determineDataSourceAndExecute(invocation, false);
    }

    public EasyMsQueryInterceptor(EasyMsMultiDataSource easyMsMultiDataSource) {
        super(easyMsMultiDataSource);
    }
}