package com.stars.easyms.datasource.pointcut;

import com.stars.easyms.base.pointcut.EasyMsAnnotationMatchingPointcut;
import com.stars.easyms.datasource.EasyMsMultiDataSource;
import com.stars.easyms.datasource.annotation.SpecifyDataSource;
import com.stars.easyms.datasource.interceptor.SpecifyDataSourceInterceptor;
import org.aopalliance.aop.Advice;
import org.springframework.aop.Pointcut;
import org.springframework.aop.support.AbstractPointcutAdvisor;
import org.springframework.lang.NonNull;
import org.springframework.util.ObjectUtils;

import java.lang.annotation.ElementType;

/**
 * <p>className: SpecifyDataSourceMethodPointcutAdvisor</p>
 * <p>description: 注解SpecifyDataSource在方法上的顾问类</p>
 *
 * @author guoguifang
 * @version 1.2.1
 * @date 2019-03-15 10:52
 */
public final class SpecifyDataSourceMethodPointcutAdvisor extends AbstractPointcutAdvisor {

    private transient Advice advice;

    private transient EasyMsAnnotationMatchingPointcut pointcut;

    public SpecifyDataSourceMethodPointcutAdvisor(EasyMsMultiDataSource easyMsMultiDataSource) {
        this.advice = new SpecifyDataSourceInterceptor(easyMsMultiDataSource, ElementType.METHOD);
        this.pointcut = new EasyMsAnnotationMatchingPointcut();
        this.pointcut.setMethodMatcher(SpecifyDataSource.class, true);
        super.setOrder(20);
    }

    @NonNull
    @Override
    public Pointcut getPointcut() {
        return this.pointcut;
    }

    @NonNull
    @Override
    public Advice getAdvice() {
        return this.advice;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || this.getClass() != other.getClass()) {
            return false;
        }
        SpecifyDataSourceMethodPointcutAdvisor otherAdvisor = (SpecifyDataSourceMethodPointcutAdvisor) other;
        return (ObjectUtils.nullSafeEquals(this.advice, otherAdvisor.advice) &&
                ObjectUtils.nullSafeEquals(this.pointcut, otherAdvisor.pointcut));
    }

    @Override
    public int hashCode() {
        return 41 + this.advice.hashCode() * 41 + this.pointcut.hashCode() * 41;
    }
}