package com.stars.easyms.datasource.support;

import com.stars.easyms.datasource.properties.EasyMsDataSourcePropertiesProcessor;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * <p>className: DataSourceSupportFactory</p>
 * <p>description: 创建DataSourceSupport的工厂类</p>
 *
 * @author guoguifang
 * @version 1.2.1
 * @date 2019-05-14 17:22
 */
public final class DataSourceSupportFactory {

    private static final Map<String, DataSourceSupport> DATA_SOURCE_SUPPORT_MAP = new ConcurrentHashMap<>(4);

    /**
     * 根据配置创建一个数据源支持类
     *
     * @param propertiesProcessor 配置处理器
     * @return
     */
    public static DataSourceSupport getDataSourceSupport(EasyMsDataSourcePropertiesProcessor propertiesProcessor, boolean isMaster) {
        String dataSourceType = propertiesProcessor.getPropertiesValue("type", isMaster, "druid");
        return DATA_SOURCE_SUPPORT_MAP.computeIfAbsent(dataSourceType, key -> {
            if ("hikari".equalsIgnoreCase(key)) {
                return new HikariDataSourceSupport();
            }
            return new DruidDataSourceSupport();
        });
    }

    private DataSourceSupportFactory() {}

}