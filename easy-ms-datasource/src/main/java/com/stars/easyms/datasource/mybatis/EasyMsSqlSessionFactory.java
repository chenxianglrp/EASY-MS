package com.stars.easyms.datasource.mybatis;

import com.stars.easyms.datasource.holder.EasyMsMybatisPlusHolder;
import org.apache.ibatis.session.*;

import java.sql.Connection;

/**
 * <p>className: EasyMsSqlSessionFactory</p>
 * <p>description: SqlSessionFactory外观类</p>
 *
 * @author guoguifang
 * @version 1.2.2
 * @date 2019-07-19 17:34
 */
public class EasyMsSqlSessionFactory implements SqlSessionFactory {

    private SqlSessionFactory sqlSessionFactory;

    private EasyMsSqlSessionTemplate sqlSessionTemplate;

    public EasyMsSqlSessionFactory(SqlSessionFactory sqlSessionFactory) {
        this.sqlSessionFactory = sqlSessionFactory;
    }

    @Override
    public SqlSession openSession() {
        return this.sqlSessionFactory.openSession();
    }

    @Override
    public SqlSession openSession(boolean autoCommit) {
        return this.sqlSessionFactory.openSession(autoCommit);
    }

    @Override
    public SqlSession openSession(Connection connection) {
        return this.sqlSessionFactory.openSession(connection);
    }

    @Override
    public SqlSession openSession(TransactionIsolationLevel level) {
        return this.sqlSessionFactory.openSession(level);
    }

    @Override
    public SqlSession openSession(ExecutorType execType) {
        // 如果是mybatis-plus的batch方法则直接返回sqlSessionTemplate
        if (ExecutorType.BATCH == execType && EasyMsMybatisPlusHolder.isMybatisPlusMethod()
                && EasyMsMybatisPlusHolder.getExecutorType() == null) {
            EasyMsMybatisPlusHolder.setExecutorType(execType);
            return sqlSessionTemplate;
        }
        return this.sqlSessionFactory.openSession(execType);
    }

    @Override
    public SqlSession openSession(ExecutorType execType, boolean autoCommit) {
        return this.sqlSessionFactory.openSession(execType, autoCommit);
    }

    @Override
    public SqlSession openSession(ExecutorType execType, TransactionIsolationLevel level) {
        return this.sqlSessionFactory.openSession(execType, level);
    }

    @Override
    public SqlSession openSession(ExecutorType execType, Connection connection) {
        return this.sqlSessionFactory.openSession(execType, connection);
    }

    @Override
    public Configuration getConfiguration() {
        return this.sqlSessionFactory.getConfiguration();
    }

    void setSqlSessionTemplate(EasyMsSqlSessionTemplate sqlSessionTemplate) {
        this.sqlSessionTemplate = sqlSessionTemplate;
    }
}