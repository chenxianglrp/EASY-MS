package com.stars.easyms.datasource.compatibility;

import com.stars.easyms.base.util.ApplicationContextHolder;
import com.stars.easyms.base.util.ConfigurationPropertiesUtil;
import com.stars.easyms.datasource.jdbc.EasyMsJdbcTemplate;
import org.springframework.boot.autoconfigure.jdbc.JdbcProperties;

/**
 * <p>className: EasyMsJdbcPropertiesCompatibility</p>
 * <p>description: JdbcProperties是在spring boot2.0之后才有的，此类可兼容spring boot1.0版本</p>
 *
 * @author guoguifang
 * @date 2019-12-20 15:38
 * @since 1.4.2
 */
public final class EasyMsJdbcPropertiesCompatibility {

    public static void initEasyMsJdbcTemplate(EasyMsJdbcTemplate easyMsJdbcTemplate) {
        JdbcProperties properties = ApplicationContextHolder.getBean(JdbcProperties.class);
        if (properties == null) {
            properties = new JdbcProperties();
            ConfigurationPropertiesUtil.bind(properties);
        }

        JdbcProperties.Template template = properties.getTemplate();
        easyMsJdbcTemplate.setFetchSize(template.getFetchSize());
        easyMsJdbcTemplate.setMaxRows(template.getMaxRows());
        if (template.getQueryTimeout() != null) {
            easyMsJdbcTemplate.setQueryTimeout((int) template.getQueryTimeout().getSeconds());
        }
    }

    private EasyMsJdbcPropertiesCompatibility() {
    }
}