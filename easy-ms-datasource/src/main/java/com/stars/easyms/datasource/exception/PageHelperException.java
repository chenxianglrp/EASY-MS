package com.stars.easyms.datasource.exception;

import com.stars.easyms.base.util.ExceptionUtil;
import com.stars.easyms.base.util.MessageFormatUtil;

/**
 * <p>className: DataSourceInterceptorException</p>
 * <p>description: 执行拦截器时抛出的异常</p>
 *
 * @author guoguifang
 * @date 2019/5/29 9:48
 * @version 1.2.1
 */
public class PageHelperException extends RuntimeException {

    private static final long serialVersionUID = -2034894330345455631L;

    public PageHelperException(Throwable throwable) {
        super(throwable);
    }

    public PageHelperException(String message, Object... args) {
        super(MessageFormatUtil.format(message, ExceptionUtil.trimmedCopy(args)), ExceptionUtil.extractThrowable(args));
    }
}
