package com.stars.easyms.datasource.page;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>className: QueryRequestBean</p>
 * <p>description: 查询请求类：该类不推荐使用，主要是用来兼容一些框架，如果未使用这些框架则不需要关注该类</p>
 *
 * @author guoguifang
 * @date 2019-12-28 16:36
 * @since 1.5.0
 */
public class QueryRequestBean {

    @Getter
    private PageParameter pageParameter;

    private Map<String, Object> searchParams;

    public QueryRequestBean() {
    }

    public QueryRequestBean(PageParameter pageParameter) {
        this.pageParameter = pageParameter;
    }

    public QueryRequestBean(Map<String, Object> searchParams, PageParameter pageParameter) {
        this.searchParams = searchParams;
        this.pageParameter = pageParameter;
    }

    public Map<String, Object> getSearchParams() {
        if (this.searchParams == null) {
            this.searchParams = new HashMap<>(8);
        }

        if (this.pageParameter != null) {
            this.searchParams.put("page", this.pageParameter);
        }

        return this.searchParams;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("QueryRequestBean [pageParameter=");
        builder.append(this.pageParameter);
        builder.append(", searchParams=");
        builder.append(this.searchParams);
        builder.append("]");
        return builder.toString();
    }
}