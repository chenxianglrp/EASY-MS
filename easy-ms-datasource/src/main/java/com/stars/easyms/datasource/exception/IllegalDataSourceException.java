package com.stars.easyms.datasource.exception;

import com.stars.easyms.base.util.ExceptionUtil;
import com.stars.easyms.base.util.MessageFormatUtil;

/**
 * <p>className: IllegalDataSourceException</p>
 * <p>description: 无效的数据源异常</p>
 *
 * @author guoguifang
 * @version 1.2.1
 * @date 2019-06-06 14:14
 */
public final class IllegalDataSourceException extends RuntimeException {

    private static final long serialVersionUID = -1134894193345463631L;

    public IllegalDataSourceException(String message, Object... args) {
        super(MessageFormatUtil.format(message, ExceptionUtil.trimmedCopy(args)), ExceptionUtil.extractThrowable(args));
    }
}