package com.stars.easyms.datasource.manual;

import com.stars.easyms.datasource.batch.BatchCommit;
import com.stars.easyms.datasource.EasyMsMultiDataSource;
import com.stars.easyms.datasource.autoconfigure.EasyMsDatasourceAutoConfiguration;
import com.stars.easyms.datasource.interceptor.mybatis.EasyMsPageInterceptor;
import com.stars.easyms.datasource.interceptor.mybatis.EasyMsQueryInterceptor;
import com.stars.easyms.datasource.interceptor.mybatis.EasyMsUpdateInterceptor;
import com.stars.easyms.datasource.pointcut.FixedDataSourceMethodPointcutAdvisor;
import com.stars.easyms.datasource.pointcut.FixedDataSourceTypePointcutAdvisor;
import com.stars.easyms.datasource.pointcut.SpecifyDataSourceMethodPointcutAdvisor;
import com.stars.easyms.datasource.pointcut.SpecifyDataSourceTypePointcutAdvisor;
import com.stars.easyms.datasource.mybatis.EasyMsMapperScannerConfigurer;
import com.stars.easyms.datasource.mybatis.EasyMsSqlSessionFactory;
import com.stars.easyms.datasource.mybatis.EasyMsSqlSessionTemplate;
import com.stars.easyms.base.util.BeanUtil;
import org.apache.ibatis.session.Configuration;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * <p>className: EasyMsDatasourceManualConfiguration</p>
 * <p>description: springmvc项目手动配置bean</p>
 *
 * @author guoguifang
 * @version 1.2.2
 * @date 2019-07-16 14:11
 */
public class EasyMsDatasourceManualConfiguration {

    private static final String MANUAL_BEAN_FACTORY_BEAN_NAME = "easyMsManualBeanFactory";

    /**
     * 用于spingmvc项目非自动加载EasyMsDatasourceAutoConfiguration类的情况
     */
    public void manualRegisterBean(ConfigurableApplicationContext applicationContext) {
        // 注册当前bean对象
        BeanUtil.registerSingleton(applicationContext, "easyMsDatasourceAutoConfiguration", new EasyMsDatasourceAutoConfiguration());

        // 注册当前bean对象
        EasyMsManualBeanFactory easyMsManualBeanFactory = new EasyMsManualBeanFactory(applicationContext);
        BeanUtil.registerSingleton(applicationContext, MANUAL_BEAN_FACTORY_BEAN_NAME, easyMsManualBeanFactory);

        // 注册EasyMsMultiDataSource对象
        BeanUtil.registerBean(applicationContext, "easyMsMultiDataSource", EasyMsMultiDataSource.class,
                easyMsManualBeanFactory::easyMsMultiDataSource, MANUAL_BEAN_FACTORY_BEAN_NAME, "easyMsMultiDataSource");

        // 注册EasyMsMybatisConfiguration对象
        BeanUtil.registerBean(applicationContext, "easyMsMybatisConfiguration", Configuration.class,
                easyMsManualBeanFactory::easyMsMybatisConfiguration, MANUAL_BEAN_FACTORY_BEAN_NAME, "easyMsMybatisConfiguration");

        // 注册EasyMsPageInterceptor对象
        BeanUtil.registerBean(applicationContext, "easyMsPageInterceptor", EasyMsPageInterceptor.class,
                easyMsManualBeanFactory::easyMsPageInterceptor, MANUAL_BEAN_FACTORY_BEAN_NAME, "easyMsPageInterceptor");

        // 注册EasyMsQueryInterceptor对象
        BeanUtil.registerBean(applicationContext, "easyMsQueryInterceptor", EasyMsQueryInterceptor.class,
                easyMsManualBeanFactory::easyMsQueryInterceptor, MANUAL_BEAN_FACTORY_BEAN_NAME, "easyMsQueryInterceptor");

        // 注册EasyMsUpdateInterceptor对象
        BeanUtil.registerBean(applicationContext, "easyMsUpdateInterceptor", EasyMsUpdateInterceptor.class,
                easyMsManualBeanFactory::easyMsUpdateInterceptor, MANUAL_BEAN_FACTORY_BEAN_NAME, "easyMsUpdateInterceptor");

        // 注册SqlSessionFactory对象
        BeanUtil.registerBean(applicationContext, "easyMsSqlSessionFactory", EasyMsSqlSessionFactory.class,
                easyMsManualBeanFactory::easyMsSqlSessionFactory, MANUAL_BEAN_FACTORY_BEAN_NAME, "easyMsSqlSessionFactory");

        // 注册EasyMsSqlSessionTemplate对象
        BeanUtil.registerBean(applicationContext, "easyMsSqlSessionTemplate", EasyMsSqlSessionTemplate.class,
                easyMsManualBeanFactory::easyMsSqlSessionTemplate, MANUAL_BEAN_FACTORY_BEAN_NAME, "easyMsSqlSessionTemplate");

        // 注册FixedDataSourceTypePointcutAdvisor对象
        BeanUtil.registerBean(applicationContext, "fixedDataSourceTypePointcutAdvisor", FixedDataSourceTypePointcutAdvisor.class,
                easyMsManualBeanFactory::fixedDataSourceTypePointcutAdvisor, MANUAL_BEAN_FACTORY_BEAN_NAME, "fixedDataSourceTypePointcutAdvisor");

        // 注册FixedDataSourceMethodPointcutAdvisor对象
        BeanUtil.registerBean(applicationContext, "fixedDataSourceMethodPointcutAdvisor", FixedDataSourceMethodPointcutAdvisor.class,
                easyMsManualBeanFactory::fixedDataSourceMethodPointcutAdvisor, MANUAL_BEAN_FACTORY_BEAN_NAME, "fixedDataSourceMethodPointcutAdvisor");

        // 注册SpecifyDataSourceTypePointcutAdvisor对象
        BeanUtil.registerBean(applicationContext, "specifyDataSourceTypePointcutAdvisor", SpecifyDataSourceTypePointcutAdvisor.class,
                easyMsManualBeanFactory::specifyDataSourceTypePointcutAdvisor, MANUAL_BEAN_FACTORY_BEAN_NAME, "specifyDataSourceTypePointcutAdvisor");

        // 注册SpecifyDataSourceMethodPointcutAdvisor对象
        BeanUtil.registerBean(applicationContext, "specifyDataSourceMethodPointcutAdvisor", SpecifyDataSourceMethodPointcutAdvisor.class,
                easyMsManualBeanFactory::specifyDataSourceMethodPointcutAdvisor, MANUAL_BEAN_FACTORY_BEAN_NAME, "specifyDataSourceMethodPointcutAdvisor");

        // 注册EasyMsMapperScannerConfigurer对象，先注册aspectj对象
        BeanUtil.registerBean(applicationContext, "easyMsMapperScannerConfigurer", EasyMsMapperScannerConfigurer.class,
                easyMsManualBeanFactory::easyMsMapperScannerConfigurer, MANUAL_BEAN_FACTORY_BEAN_NAME, "easyMsMapperScannerConfigurer")
                .postProcessBeanDefinitionRegistry((BeanDefinitionRegistry) applicationContext.getBeanFactory());

        // 注册BatchCommitDAO对象
        BeanUtil.registerBean(applicationContext, "batchCommitDAO", BatchCommit.class,
                easyMsManualBeanFactory::batchCommitDAO, MANUAL_BEAN_FACTORY_BEAN_NAME, "batchCommitDAO");
    }

}