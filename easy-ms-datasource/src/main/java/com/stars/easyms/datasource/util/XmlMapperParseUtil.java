package com.stars.easyms.datasource.util;

import com.stars.easyms.base.util.PatternUtil;
import com.stars.easyms.datasource.common.EasyMsDataSourceConstant;
import org.apache.ibatis.builder.xml.XMLMapperBuilder;
import org.apache.ibatis.executor.ErrorContext;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.core.NestedIOException;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.util.ClassUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * XmlMapper解析工具类
 *
 * @author guoguifang
 * @date 2018-10-19 10:07
 * @since 1.0.0
 */
public final class XmlMapperParseUtil {

    public static void parse(SqlSessionFactory sqlSessionFactory, String resources) throws IOException {
        parse(sqlSessionFactory.getConfiguration(), resources);
    }

    public static void parse(Configuration configuration, String resources) throws IOException {
        for (String resource : PatternUtil.splitWithComma(resources)) {
            Resource[] mapperLocations = new PathMatchingResourcePatternResolver().getResources(resource);
            for (Resource mapperLocation : mapperLocations) {
                if (mapperLocation != null) {
                    try {
                        XMLMapperBuilder xmlMapperBuilder = new XMLMapperBuilder(mapperLocation.getInputStream(), configuration, mapperLocation.toString(), configuration.getSqlFragments());
                        xmlMapperBuilder.parse();
                    } catch (Exception e) {
                        throw new NestedIOException("Failed to parse mapping resource: '" + mapperLocation + "'", e);
                    } finally {
                        ErrorContext.instance().reset();
                    }
                }
            }
        }
    }

    public static List<String> convert(String packageName) {
        List<String> convertList = new ArrayList<>();
        for (String str : PatternUtil.splitWithComma(packageName)) {
            convertList.add(ResourceLoader.CLASSPATH_URL_PREFIX
                    + ClassUtils.convertClassNameToResourcePath(str) + EasyMsDataSourceConstant.MAPPER_XML_SUFFIX);
        }
        return convertList;
    }

    public static boolean isMybatisMapperXml(Resource resource) {
        SAXReader reader = new SAXReader();
        try {
            Document document = reader.read(resource.getInputStream());
            Element rootElement = document.getRootElement();
            if (EasyMsDataSourceConstant.MYBATIS_MAPPER_XML_ROOT_ELEMENT_NAME.equalsIgnoreCase(rootElement.getName())) {
                return true;
            }
        } catch (Exception e) {
            // ignore
        }
        return false;
    }

    private XmlMapperParseUtil() {
    }
}
