package com.stars.easyms.datasource.common;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * <p>className: EasyMsDataSourceConstant</p>
 * <p>description: EasyMs多数据源常量类</p>
 *
 * @author guoguifang
 * @date 2019-11-26 10:08
 * @since 1.4.1
 */
public final class EasyMsDataSourceConstant {

    public static final String MODULE_NAME = "datasource";

    public static final String PROPERTY_SOURCE_NAME = "EasyMsDataSourcePropertySource";

    public static final String MASTER = "Master";

    public static final String SLAVE = "Slave";

    public static final String DEFAULT_DATASOURCE_NAME = "";

    public static final String CONNECT_URL_AT_SIGN = "@";

    public static final String CONNECT_URL_FORWARD_SLASH_SIGN = "/";

    public static final String CONNECT_URL_COLON_SIGN = ":";

    public static final String CONNECT_PARAMS_QUESTION_SIGN = "?";

    public static final String CONNECT_PARAMS_JOINT_SIGN = "&";

    public static final String CONNECT_PARAMS_EQUAL_SIGN = "=";

    public static final int FAIL_RETRY_COUNT = 3;

    public static final String JUDGE_APOLLO_IS_EXIST_CLASS = "com.ctrip.framework.apollo.Config";

    public static final String JUDGE_JDBC_PROPERTIES_IS_EXIST_CLASS = "org.springframework.boot.autoconfigure.jdbc.JdbcProperties";

    public static final String MYBATIS_PLUS_BASE_MAPPER_CLASS_NAME = "com.baomidou.mybatisplus.core.mapper.BaseMapper";

    public static final String MYBATIS_PLUS_PARENT_MAPPER_CLASS_NAME = "com.baomidou.mybatisplus.core.mapper.Mapper";

    public static final String MYBATIS_PLUS_PAGE_INTERCEPTOR_CLASS_NAME = "com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor";

    public static final String MAPPER_XML_SUFFIX = "/**/*.xml";

    public static final String MYBATIS_PLUS_DEFAULT_MAPPER_LOCATIONS = "classpath:/mapper/**/*.xml";

    public static final String MYBATIS_MAPPER_XML_ROOT_ELEMENT_NAME = "mapper";

    public static final List<Class<? extends Annotation>> SUPPORT_ANNOTATION_TYPE_LIST;

    static {
        List<Class<? extends Annotation>> supportAnnotationTypeList = new ArrayList<>();
        supportAnnotationTypeList.add(Repository.class);
        supportAnnotationTypeList.add(Mapper.class);
        SUPPORT_ANNOTATION_TYPE_LIST = Collections.unmodifiableList(supportAnnotationTypeList);
    }

    private EasyMsDataSourceConstant() {
    }
}