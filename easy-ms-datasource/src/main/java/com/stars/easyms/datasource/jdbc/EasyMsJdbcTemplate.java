package com.stars.easyms.datasource.jdbc;

import com.stars.easyms.datasource.EasyMsMultiDataSource;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

/**
 * <p>className: EasyMsJdbcTemplate</p>
 * <p>description: spring的JdbcTemplate的数据源是强制绑定死的，可能会造成事务和mybatis事务冲突</p>
 *
 * @author guoguifang
 * @date 2019-11-22 18:54
 * @since 1.4.1
 */
public final class EasyMsJdbcTemplate extends JdbcTemplate {

    private final EasyMsMultiDataSource easyMsMultiDataSource;

    public EasyMsJdbcTemplate(EasyMsMultiDataSource easyMsMultiDataSource) {
        super(easyMsMultiDataSource);
        this.easyMsMultiDataSource = easyMsMultiDataSource;
        this.setExceptionTranslator(new EasyMsSqlErrorCodeSqlExceptionTranslator(easyMsMultiDataSource));
    }

    @Override
    public DataSource getDataSource() {
        return easyMsMultiDataSource.getCurrentDataSource();
    }

    @Override
    public void afterPropertiesSet() {
        // Intentionally blank
    }

}