package com.stars.easyms.datasource.shiro;

import com.stars.easyms.monitor.shiro.ShiroAuthorityFunc;
import com.stars.easyms.datasource.common.EasyMsDataSourceConstant;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * <p>className: EasyMsShiroAuthorityFunc</p>
 * <p>description: EasyMs的基础shiro权限管理方法类</p>
 *
 * @author guoguifang
 * @date 2019-11-25 15:40
 * @since 1.4.1
 */
public class EasyMsDataSourceShiroAuthorityFunc implements ShiroAuthorityFunc {

    @Override
    public Map<String, String> getFilterChainDefinitionMap() {
        Map<String, String> map = new LinkedHashMap<>();
        map.put("/" + EasyMsDataSourceConstant.MODULE_NAME, NO_AUTHORITY_DEFINITION);
        return map;
    }

    @Override
    public String getShiroAuthorityModuleName() {
        return EasyMsDataSourceConstant.MODULE_NAME;
    }

    @Override
    public int getShiroAuthorityOrder() {
        return 7;
    }
}