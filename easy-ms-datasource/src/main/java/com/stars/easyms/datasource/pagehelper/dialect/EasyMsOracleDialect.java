package com.stars.easyms.datasource.pagehelper.dialect;

import com.github.pagehelper.Page;
import com.github.pagehelper.dialect.helper.OracleDialect;
import org.apache.ibatis.cache.CacheKey;

/**
 * <p>className: EasyMsOracleDialect</p>
 * <p>description: EasyMs优化原pageHelper自带的oracle方言</p>
 *
 * @author guoguifang
 * @version 1.2.2
 * @date 2019-07-30 09:36
 */
public final class EasyMsOracleDialect extends OracleDialect {

    @Override
    public String getPageSql(String sql, Page page, CacheKey pageKey) {
        return "SELECT * FROM (SELECT TMP_PAGE.*, ROWNUM ROW_ID FROM ( " + sql + " ) TMP_PAGE WHERE ROWNUM <= ?) WHERE ROW_ID > ?";
    }

}