package com.stars.easyms.datasource.autoconfigure;

import com.baomidou.mybatisplus.extension.plugins.OptimisticLockerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.pagination.optimize.JsqlParserCountOptimize;
import com.stars.easyms.datasource.mybatisplus.EasyMsMybatisPlusBeanFactory;
import com.stars.easyms.datasource.mybatisplus.EasyMsMybatisPlusGlobalConfig;
import com.stars.easyms.datasource.pointcut.MybatisPlusServiceImplTypePointcutAdvisor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <p>className: EasyMsMybatisPlusAutoConfiguration</p>
 * <p>description: EasyMs自定义创建mybatis-plus的自动配置类</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/11/12 5:03 下午
 */
@Configuration
@ConditionalOnClass(name = "com.baomidou.mybatisplus.core.MybatisConfiguration")
public class EasyMsMybatisPlusAutoConfiguration {

    @Bean
    public EasyMsMybatisPlusGlobalConfig easyMsMybatisPlusGlobalConfig() {
        return new EasyMsMybatisPlusGlobalConfig();
    }

    @Bean
    public MybatisPlusServiceImplTypePointcutAdvisor mybatisPlusServiceImplTypePointcutAdvisor() {
        return new MybatisPlusServiceImplTypePointcutAdvisor();
    }

    @Configuration
    @ConditionalOnClass({PaginationInterceptor.class, JsqlParserCountOptimize.class})
    public static class EasyMsMybatisPlusPaginationInterceptorAutoConfiguration {

        @Bean
        @ConditionalOnMissingBean
        public PaginationInterceptor paginationInterceptor() {
            return (PaginationInterceptor) EasyMsMybatisPlusBeanFactory.getInstance().getMybatisPlusPageInterceptor();
        }
    }

    @Configuration
    @ConditionalOnClass(OptimisticLockerInterceptor.class)
    public static class EasyMsMybatisPlusOptimisticLockerInterceptorAutoConfiguration {

        @Bean
        @ConditionalOnMissingBean
        public OptimisticLockerInterceptor optimisticLockerInterceptor() {
            return new OptimisticLockerInterceptor();
        }
    }

}
