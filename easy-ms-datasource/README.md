# Easy-Ms-Datasource

　　easy-ms-datasource是一个**基于spring和mybatis的、支持多数据源的、支持读写分离的、支持多主多从架构的、支持负载均衡的、支持高可用的、支持多数据源分布式事务的、支持自动分页的、支持批量操作的、可快速集成型框架**，是Easy-Ms的一个组件框架，**但也支持在非Easy-Ms项目的Spring Boot或Spring Mvc项目中使用**，该框架具有强大的数据源管理功能。

## 目录

　　[一、功能特点](#%E4%B8%80-%E5%8A%9F%E8%83%BD%E7%89%B9%E7%82%B9)

　　[二、依赖引入](#%E4%BA%8C-%E4%BE%9D%E8%B5%96%E5%BC%95%E5%85%A5)

　　　　[1. 使用parent的方式](#1-%E4%BD%BF%E7%94%A8parent%E7%9A%84%E6%96%B9%E5%BC%8F)

　　　　[2. 直接引用](#2-%E7%9B%B4%E6%8E%A5%E5%BC%95%E7%94%A8)

　　[三、 非Easy-Ms项目支持](#%E4%B8%89-%E9%9D%9Eeasy-ms%E9%A1%B9%E7%9B%AE%E6%94%AF%E6%8C%81)

　　　　[1. Spring Boot项目](#1-spring-boot%E9%A1%B9%E7%9B%AE)

　　　　[2. Spring Mvc项目](#2-spring-mvc%E9%A1%B9%E7%9B%AE)

　　[四、多数据源（多主多从）配置](#%E5%9B%9B-%E5%A4%9A%E6%95%B0%E6%8D%AE%E6%BA%90%E5%A4%9A%E4%B8%BB%E5%A4%9A%E4%BB%8E%E9%85%8D%E7%BD%AE)

　　[五、数据源配置](#%E4%BA%94-%E6%95%B0%E6%8D%AE%E6%BA%90%E9%85%8D%E7%BD%AE)

　　[六、 获取数据源的方式](#%E5%85%AD-%E8%8E%B7%E5%8F%96%E6%95%B0%E6%8D%AE%E6%BA%90%E7%9A%84%E6%96%B9%E5%BC%8F)

　　　　[1. 使用自动注入的方式获取](#1-%E4%BD%BF%E7%94%A8%E8%87%AA%E5%8A%A8%E6%B3%A8%E5%85%A5%E7%9A%84%E6%96%B9%E5%BC%8F%E8%8E%B7%E5%8F%96)

　　　　[2. 使用手动的方式获取](#2-%E4%BD%BF%E7%94%A8%E6%89%8B%E5%8A%A8%E7%9A%84%E6%96%B9%E5%BC%8F%E8%8E%B7%E5%8F%96)

　　[七、 如何使用](#%E4%B8%83-%E5%A6%82%E4%BD%95%E4%BD%BF%E7%94%A8)

　　　　[1. 注解声明式](#%E6%B3%A8%E8%A7%A3%E5%A3%B0%E6%98%8E%E5%BC%8F)

　　　　[2. 手动编程式](#%E6%89%8B%E5%8A%A8%E7%BC%96%E7%A8%8B%E5%BC%8F)

　　[八、 分页的使用](#%E5%85%AB-%E5%88%86%E9%A1%B5%E7%9A%84%E4%BD%BF%E7%94%A8)

　　[九、 多数据源事务使用](#%E4%B9%9D-%E5%A4%9A%E6%95%B0%E6%8D%AE%E6%BA%90%E4%BA%8B%E5%8A%A1%E4%BD%BF%E7%94%A8)

　　　　[1. 声明式事务](#1-%E5%A3%B0%E6%98%8E%E5%BC%8F%E4%BA%8B%E5%8A%A1)

　　　　[2. 编程式事务](#2-%E7%BC%96%E7%A8%8B%E5%BC%8F%E4%BA%8B%E5%8A%A1)

　　[十、 批量操作](#%E5%8D%81-%E6%89%B9%E9%87%8F%E6%93%8D%E4%BD%9C)

　　[十一、 监控](#%E5%8D%81%E4%B8%80-%E7%9B%91%E6%8E%A7)

　　[十二、负载均衡](#%E5%8D%81%E4%BA%8C-%E8%B4%9F%E8%BD%BD%E5%9D%87%E8%A1%A1)

　　[十三、 读写分离实现比较](#%E5%8D%81%E4%B8%89-%E8%AF%BB%E5%86%99%E5%88%86%E7%A6%BB%E5%AE%9E%E7%8E%B0%E6%AF%94%E8%BE%83)

## 一、 功能特点

1. 支持**单、多数据源**，同时**每个数据源又支持多主多从结构**；
2. **配置简单**，大大**简化了数据源的配置**，同时**提供了全局参数配置**；
3. 支持**负载均衡**，包含主流负载均衡策略，满足绝大多数场景，可以最大限度地提高数据库使用率；
4. 支持**高可用**，数据库宕机等原因引起的数据库连接超时或失败，负载均衡时可以自动剔除该数据库；
5. 支持**故障转移**，在执行SQL期间遇到数据库连接问题还可以自动切换其他数据库重试；
6. 支持应用运行期间**动态增加、修改、删除数据源**；
7. 支持**多数据源分布式事务**，需配合Spring的**事务注解@Transactional**或使用**编程式事务**使用；
8. 支持**慢SQL记录**，也支持开关（自定义是否开启），支持配置慢SQL的时间，可在页面进行实时展示；
9. 支持**所有数据源的监控**，可详细显示当前所有数据源正在执行的SQL，包括已用时、已完成数量，正在处理数量等等非常详细的监控数据；
10. 集成**PageHelper分页**，并使用了**异步查询总数**的方式，性能提升较大；
11. 支持以**注解声明式**或者**手动编程式**两种方式指定数据源名称以及指定是否强制选择主或者从；
12. 支持**批量操作**，可以以在mybatis的xml写SQL的方式批量提交，SQL管理更简单，代码更简洁，使用更简单，效率更高，同时支持批量事务；
13. 严格控制增删改操作必须走主数据源，降低开发人员由于代码错误造成的BUG风险；
14. 不需要引入mycat等中间件，但拥有mycat的所有功能，而且比mycat使用更灵活，使用场景更丰富，比使用mycat架构更简单，却更好用，也无需部署，节省服务器成本，也不需要运维人员维护；

## 二、 依赖引入

#### 1. 使用parent的方式

　　好处：easy-ms-parent里包含了许多的依赖版本管理，可以在依赖管理`<dependencies>`中，有较多常用的引入直接使用默认的版本而无需指定版本号

```xml
pom.xml:

    <!-- 可以直接把easy-ms-parent当做parent，或者当引入多个easy-ms组件时使用parent可以只需设置一次版本号 -->
    <parent>
        <groupId>com.stars.easyms</groupId>
        <artifactId>easy-ms-parent</artifactId>
        <version>${easy-ms.version}</version>
    </parent>

    <!-- 如果是直接引入easy-ms，则不需要引入easy-ms-datasource，easy-ms已经默认依赖了easy-ms-datasource -->
    <dependencies>
        <dependency>
            <groupId>com.stars.easyms</groupId>
            <artifactId>easy-ms-datasource</artifactId>
        </dependency>
    </dependencies>
```

#### 2. 直接引用

```xml
    <!-- 如果是直接引入easy-ms，则不需要引入easy-ms-datasource，easy-ms已经默认依赖了easy-ms-datasource -->
    <dependencies>
        <dependency>
            <groupId>com.stars.easyms</groupId>
            <artifactId>easy-ms-datasource</artifactId>
            <version>${easy-ms.version}</version>
        </dependency>
    </dependencies>
```

## 三、 非Easy-Ms项目支持

　　如果需要创建新的项目，可以直接引入easy-ms，如果是已存在的项目且无法改造成easy-ms项目的，但是想使用easy-ms-datasource框架的，可以按照下面两种方法引入后使用。

#### 1. Spring Boot项目

　　**直接在项目pom.xml中按照依赖引入即可，支持spring boot 1.x.x和spring boot 2.x.x。**

　　需要注意的是项目中是否还有创建数据源的操作，如果有需要增加`@Primary`注解，否则可能会造成数据源唯一性冲突。

　　**不建议自己再创建数据源**，Easy-Ms数据源支持多数据源及多主多从结构，可以把需要创建的数据源直接定义为Easy-Ms数据源的默认数据源。

#### 2. Spring Mvc项目

　　在spring的xml配置文件中增加以下内容：

```xml
	<!-- 数据源工厂对象 -->
	<bean id="dataSourceFactory" class="com.stars.easyms.datasource.EasyMsDataSourceFactory">
    <!-- 如果使用本地的属性文件，则需要配置该属性，如果使用配置中心则该属性不需要配置 -->
		<property name="locations">
			<list>
				<value>classpath*:/jdbc.properties</value>
			</list>
		</property>
	</bean>

	<!-- 根据工厂类数据源对象 -->
	<bean id="dataSource" factory-bean="dataSourceFactory" factory-method="createDataSource"/>
```

## 四、 多数据源（多主多从）配置

　　每个项目都需要配置一个**默认数据源**，该数据源使用（**spring.datasource.\**\***）为前缀，使用时**不需要在注解中指定数据源名称**（即使用默认），若在注解中使用了未激活或者不存在的数据源时**自动指向该默认数据源**，因此该默认数据源**必须配置**。

　　**非默认数据源**都**必须**有**唯一**的数据源名称，且使用（**spring.datasource-数据源名称.\**\***）为前缀，为了防止配置过多数据源造成未用数据源的浪费及其他问题，**需要在spring.multi-datasource-active配置中指定是否需要激活**（若不指定默认不激活），

　　**每一个数据源都支持多主多从结构**。

| **配置**                         | **说明**                                                     |
| -------------------------------- | ------------------------------------------------------------ |
| spring.datasource.***            | 与spring.datasource.master.***相同，但比之优先级高，默认数据源的配置前缀 |
| spring.datasource.master.***     | 默认数据源的主数据源配置前缀                                 |
| spring.datasource.slave.***      | 默认数据源的从数据源的配置前缀，若从数据源的druid配置为空的时候默认与主数据源的值相同 |
| spring.multi-datasource-active   | 激活的多数据源名称列表，不同的数据源名称用“,”进行分割        |
| spring.datasource-数据源名称.*** | 多数据源配置前缀，若需激活需将数据源名称在spring.multi-datasource-active中激活 |

　　yaml简易配置文件如下（默认mysql）：

```yaml
spring:
  datasource:
    master:
      connect: test/test@127.0.0.1:3306/test_master1, test/test@127.0.0.1:3306/test_master2
    slave:
      connect: test/test@127.0.0.1:3306/test_slave1, test/test@127.0.0.1:3306/test_slave2, test/test@127.0.0.1:3306/test_slave3, test/test@127.0.0.1:3306/test_slave4, test/test@127.0.0.1:3306/test_slave5
  multi-datasource-active: other1, other2
  datasource-other1:
    master:
      connect: test/test@127.0.0.1:3306/other1_master1, test/test@127.0.0.1:3306/other1_master2
    slave:
      connect: test/test@127.0.0.1:3306/other1_slave1, test/test@127.0.0.1:3306/other1_slave2
  datasource-other2:
    master:
      connect: test/test@127.0.0.1:3306/other2_master1, test/test@127.0.0.1:3306/other2_master2
    slave:
      connect: test/test@127.0.0.1:3306/other2_slave1, test/test@127.0.0.1:3306/other2_slave2
```

## 五、 数据源配置

　　提供了较多的**全局参数及默认值**，大大**简化了数据源配置**，详细数据源配置信息如下（未配置时使用缺省值）：

| **配置**                                  | **缺省值** | **说明**                                                     |
| ----------------------------------------- | :----------: | ------------------------------------------------------------ |
| connect                                   |            | 多主、多从使用该配置，多个数据源用','分割，每个数据源格式：username/password@ip:port/database，与url互斥。注意：从数据源只能使用connect不能使用url |
| connectParams                             | 自动识别   | 只有mysql有效，mysql的连接参数，若不设置则为默认值：useUnicode=true&characterEncoding=UTF-8&autoReconnect=true&failOverReadOnly=false&allowMultiQueries=true&zeroDateTimeBehavior=convertToNull&rewriteBatchedStatements=true&useSSL=false |
| loadBalancer                              | 自动识别   | 负载均衡策略：主数据源默认主备模式，从数据源默认最可用模式。可选值：active_standby/best-available  /round_robin/random。详细内容见后续章节 |
| url                                       |            | 连接数据库的url，不同数据库格式不一样，从数据源不能使用      |
| username                                  |            | 连接数据库的用户名，需与url共同使用                          |
| password                                  |            | 连接数据库的密码，需与url共同使用                            |
| driverClassName                           | 自动识别   | 这一项可配可不配，如果不配置会根据url或dbType自动识别dbType，然后选择相应的driverClassName |
| dbType                                    | 自动识别   | 数据库类型：如果配置了url或driverClassName则自动识别，否则必须指定。值可以为：oracle、mysql、sqlserver_2000、sqlserver_2005、db2、postgresql。 |
| initialSize                               | 5          | 初始化时建立物理连接的个数。初始化发生在显示调用init方法，或者第一次getConnection时 |
| maxActive                                 | 300        | 最大连接池数量                                               |
| minIdle                                   | 5          | 最小连接池数量                                               |
| maxWait                                   | 1分钟      | 获取连接时最大等待时间，单位毫秒。配置了maxWait之后，缺省启用公平锁，并发效率会有所下降，如果需要可以通过配置useUnfairLock属性为true使用非公平锁。 |
| useUnfairLock                             | false      | 是否使用非公平锁，默认不启动即使用公平锁，使用公平锁虽然会降低系统性能但会降低连接丢失率提高安全性 |
| poolPreparedStatements                    | true       | 是否缓存preparedStatement，也就是PSCache。PSCache对支持游标的数据库性能提升巨大，比如说oracle。在mysql下建议关闭。 |
| maxPoolPreparedStatementPerConnectionSize | 20         | 要启用PSCache，必须配置大于0，当大于0时，poolPreparedStatements自动触发修改为true。在Druid中，不会存在Oracle下PSCache占用内存过多的问题，可以把这个数值配置大一些，比如说100 |
| validationQuery                           | 自动识别   | 用来检测连接是否有效的sql，要求是一个查询语句，常用select 1。如果validationQuery为null，testOnBorrow、testOnReturn、testWhileIdle都不会起作用。 |
| validationQueryTimeout                    | 1          | 单位：秒，检测连接是否有效的超时时间。底层调用jdbc Statement对象的void  setQueryTimeout(int seconds)方法 |
| testOnBorrow                              | true       | 申请连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能。 |
| testOnReturn                              | false      | 归还连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能。 |
| testWhileIdle                             | false      | 建议配置为true，不影响性能，并且保证安全性。申请连接的时候检测，如果空闲时间大于timeBetweenEvictionRunsMillis，执行validationQuery检测连接是否有效。 |
| keepAlive                                 | true       | 连接池中的minIdle数量以内的连接，空闲时间超过minEvictableIdleTimeMillis，则会执行keepAlive操作。 |
| killWhenSocketReadTimeout                 | false      | mysql有效，当sql查询超时时是否kill掉mysql数据库当前线程的sql查询(只会kill掉SQL查询，不会kill掉连接) |
| timeBetweenEvictionRunsMillis             | 1分钟      | 有两个含义：   1) Destroy线程会检测连接的间隔时间，如果连接空闲时间大于等于minEvictableIdleTimeMillis则关闭物理连接。   2) testWhileIdle的判断依据，详细看testWhileIdle属性的说明 |
| minEvictableIdleTimeMillis                | 5分钟      | 连接保持空闲而不被驱逐的最小时间                             |
| exceptionSorter                           | 自动识别   | 当数据库抛出一些不可恢复的异常时，抛弃连接                   |
| filters                                   | stat       | 属性类型是字符串，通过别名的方式配置扩展插件，常用的插件有：   监控统计用的filter:stat   日志用的filter:log4j   防御sql注入的filter:wall |
| connectionProperties                      |            | 通过connectionProperties属性来打开mergeSql功能；慢SQL记录等，例如：druid.stat.mergeSql=true;druid.stat.slowSqlMillis=5000 |
| useGlobalDataSourceStat                   | false      | 合并多个DruidDataSource的监控数据                            |

　　mybatis的配置：

| **配置**                          | **缺省值**                                                   | **说明**                                                     |
| --------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| spring.mybatis.typeAliasesPackage | @SpringBootApplication注解使用类的目录地址                   | mybatis的dao的存放目录，例如：com.stars.easyms.demo          |
| spring.mybatis.mapperLocations    | 默认spring.mybatis.typeAliasesPackage配置的目录下所有以Mapper.xml结尾的xml文件 | mybatis存放mapper问你件的路径，例如：classpath*:com/stars/easyms/demo/dao/*Mapper.xml |

　　yaml配置文件如下：

```yaml
spring:
  datasource:
    master:
      # 若使用connect、且非mysql时需要设置dbType或者driverClassName，可设置多个数据源，不同数据源之间用","分割
      dbType: oracle
      connect: test/test@127.0.0.1:1521/test_master1, test/test@127.0.0.1:1521/test_master2
      # 负载均衡策略
      loadBalancer: best-available
      # 以下配置可自定义，也可不配置使用默认值，具体默认值见上表
      initialSize: 10
      minIdle: 10
      maxActive: 100
      maxWait: 60000
      timeBetweenEvictionRunsMillis: 60000
      minEvictableIdleTimeMillis: 300000
      validationQuery: SELECT 'x' FROM dual
      testWhileIdle: true
      testOnBorrow: false
      testOnReturn: false
      poolPreparedStatements: true
      maxPoolPreparedStatementPerConnectionSize: 20
      filters: stat
    slave:
      # 若使用url时不需要设置dbType和driverClassName，但只可设置单个数据源
      url: jdbc:oracle:thin:@127.0.0.1:1521/test_slave1
      username: test
      password: test
```

## 六、 获取数据源的方式

#### 1. 使用自动注入的方式获取

```java
// 如果easy-ms数据源在项目中是唯一的，可以直接使用DataSource，但是最好使用EasyMsMultiDataSource
@Autowired
private EasyMsMultiDataSource dataSource; 
```

#### 2. 使用手动的方式获取

```java
// 1.使用com.stars.easyms.base.util.ApplicationContextHolder获取bean
EasyMsMultiDataSource dataSource = ApplicationContextHolder.getBean(EasyMsMultiDataSource.class);

// 2.使用EasyMsDataSourceFactory工厂类获取，EasyMsMultiDataSource是单例，获取到的永远是同一个对象
EasyMsMultiDataSource dataSource = EasyMsDataSourceFactory.getEasyMsMultiDataSource();
```

## 七、 如何使用

### 『注解声明式』

　　**注解声明式的原则：1.方法上的注解优先级高于类上的注解；2.查询默认走从库；3.增、删、改强制走主库。**

#### 	1. @EasyMsRepository

　　**只能在DAO接口上增加**，**优先级低于@SpecifyDataSource**，详细属性如下：

| 属性           | 默认值               | 说明                                                         |
| -------------- | -------------------- | ------------------------------------------------------------ |
| value          | ""                   | spring 的beanName                                            |
| datasource     | ""                   | 数据源名称，默认为空字符串（即默认数据源），指定数据源名称后默认该类中的所有方法都统一走该数据源，例如："other1"、"other2"，如有其他注解也指定了数据源或主从库，该属性优先级最低 |
| DataSourceType | DataSourceType.SLAVE | 枚举字段，**用来指定查询方法是走主库还是从库**，默认所有的查询方法都走从库 |

#### 2. @SpecifyDataSource

　　可以在Controller（Web）层、Service层、Manager层、DAO层使用，**可以加在所有Spring 的Bean类以及Bean类的public方法上**，用来指定数据源名称，详细属性如下：

| 属性  | 默认值 | 说明                                                         |
| ----- | ------ | ------------------------------------------------------------ |
| value | ""     | 数据源名称，默认为空字符串（即默认数据源），**等同于注解@EasyMsRepository的datasource属性，但是优先级高于@EasyMsRepository** |

#### 3. @SpecifyMasterDataSource

　　**可以在DAO层方法上使用**，用来指定强制指定走主库

#### 4. @SpecifySlaveDataSource

　　**可以在DAO层方法上使用**，用来指定强制指定走从库，**如果是增删改操作则该注解失效**

#### 5. @FixedDataSource

　　可以在Controller（Web）层、Service层、Manager层、DAO层使用，**可以加在所有Spring 的Bean类以及Bean类的public方法上**，用于固定数据源，由于Easy-Ms在不使用事务时，数据源是负载均衡的，因此在多主多从情况下可能同一个方法内会使用不同的主库或不同的从库，**如果想要在多主多从情况下在同一方法及子方法中使用相同的主库或相同的从库**，请在该方法或该类上增加该注解

#### 6. 使用方法举例

```java
/**
 * @EasyMsRepository不指定属性为默认数据源，默认查询走从库
 */
//@EasyMsRepository(value = "testDAO", datasource = "other1", query = DataSourceType.MASTER)
@EasyMsRepository
public interface TestDAO {

    // 方法上不加注解则默认按照类上注解执行
    // 此方法走的是默认数据源的主库
    int insertData_test_master();

    // 使用@SpecifySlaveDataSource强制指定从库，但是insert操作必须走主库，该注解是不生效的
    // 此方法走的是默认数据源的主库
    @SpecifySlaveDataSource
    int insertData_test_slave();

    // 指定数据源名称为"other1"，方法优先类
    // 该方法走的是other1的主库
    @SpecifyDataSource("other1")
    int insertData_other1_master();

    // 指定数据源名称为"other1"，方法优先类，但是@SpecifySlaveDataSource注解不生效，insert强制走主库
    // 此方法走的是other1的主库
    @SpecifyDataSource("other1")
    @SpecifySlaveDataSource
    int insertData_other1_slave();

    // 使用@SpecifyMasterDataSource强制指定主库，没有指定数据源
    // 此方法走的是默认数据源的主库
    @SpecifyMasterDataSource
    List<UserInfo> queryData_test_master();

    // 方法上不加注解，查询方法默认走从库
    // 此方法走的是默认数据源的从库
    List<UserInfo> queryData_test_slave();

    // 指定数据源为"other1"，并使用注解@SpecifyMasterDataSource强制为主库
    // 此方法走的是other1的主库
    @SpecifyDataSource("other1")
    @SpecifyMasterDataSource
    List<UserInfo> queryData_other1_master();

    // 指定数据源为"other1"，查询默认走从库
    // 此方法走的是other1的从库
    @SpecifyDataSource("other1")
    List<UserInfo> queryData_other1_slave();

    // 指定数据源为"other2"，查询默认走从库
    // 此方法走的是other2的从库
    @SpecifyDataSource("other2")
    List<UserInfo> queryData_other2_master();

    // 指定数据源为"other2"，并使用注解@SpecifyMasterDataSource强制为主库
    //此方法走的是other2的主库
    @SpecifyDataSource("other2")
    @SpecifyMasterDataSource
    List<UserInfo> queryData_other2_slave();
}
```

### 『手动编程式』

　　使用**EasyMsDataSourceSwitcher**类手动指定，该类中提供了指定数据源名称、是否强制使用主库、是否强制固定主库或从库的方法，可以任意根据自己的使用场景进行使用。		

　　使用方法举例：

```java
/**
 * 使用相同的DAO的方法，手动指定可以插入不同的数据源及主库或者从库
 */
@Service
public class EasyMsDatasourceDemo5Service implements RestService<BaseInput, BaseOutput> {

    @Autowired
    private TestDAO testDao;

    @Override
    public BaseOutput execute(BaseInput input) {
        try {
            // 选择默认数据源，不强制使用主库
            EasyMsDataSourceSwitcher.switchDefaultDataSource(false);
            testDao.insertData_test_master();
            // 选择默认数据源并强制固定数据源
            EasyMsDataSourceSwitcher.switchAndFixedDefaultDataSource();
            testDao.insertData_test_master();
            // 选择数据源名称为"other1"的数据源
            EasyMsDataSourceSwitcher.switchDataSource("other1");
            testDao.insertData_test_master();
            // 选择数据源名称为"other2"的数据源
            EasyMsDataSourceSwitcher.switchDataSource("other2");
            testDao.insertData_test_master();
            // 选择数据源名称为"other2"的数据源，并强制固定数据源
            EasyMsDataSourceSwitcher.switchAndFixedDataSource("other2");
            testDao.insertData_test_master();
            // 选择数据源名称为"other1"的数据源
            EasyMsDataSourceSwitcher.switchDataSource("other1");
            testDao.insertData_test_master();
        } finally {
            // 清除数据源选择
            EasyMsDataSourceSwitcher.clearSwitch();
        }
        return new BaseOutput();
    }
}
```

## 八、 分页的使用

　　easy-ms-datasource框架集成了`pageHelper`开源框架，但查询count使用了异步的方式，可以提升较大性能。默认分页使用pagehelper的语法即可，方法如下：

```java
@Service
public class PageDemoService implements RestService<BaseInput, BaseOutput> {

    @Autowired
    private TestDAO testDAO;

    @Override
    public BaseOutput execute(BaseInput input) {

        // 使用pageHelper的语法
        PageMethod.startPage(1, 5);
        Page<UserInfo> page = testDAO.queryUserInfo();
       	
      	// 返回结果
        BaseOutput baseOutput = new BaseOutput();
        baseOutput.setRetMsg(page.toString());
        return baseOutput;
    }

}

@EasyMsRepository
public interface TestDAO {

  // 跟普通的查询语句一样，但是返回的类型需要为com.github.pagehelper.Page
  @SpecifyMasterDataSource
	Page<UserInfo> queryUserInfo();  
  
}
```

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="com.stars.easyms.demo.repository.dao.TestDAO">
  
  <select id="queryUserInfo" resultType="com.stars.easyms.demo.dto.UserInfo">
		select * from USER_TEST
	</select>
  
</mapper>
```

　　虽然查询count使用了异步，但是由于mysql的分页特性，当数据量越大或SQL越复杂时，查询count的效率越低，有时候查询count还会比查询分页语句耗时更长，因此建议对于复杂count的查询，使用自定义语句，其select的id为查询分页的id后增加"_COUNT"，返回类型为"java.lang.Long"。对应xml语句如下：

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="com.stars.easyms.demo.repository.dao.TestDAO">
  
  <select id="queryUserInfo" resultType="com.stars.easyms.demo.dto.UserInfo">
		select * from USER_TEST
	</select>
  
  <select id="queryUserInfo_COUNT" resultType="java.lang.Long">
		select count(*) from USER_TEST
	</select>
  
</mapper>
```

## 九、 多数据源事务使用

#### 1. 声明式事务

　　直接使用Spring的**@Transactional**注解即可（若想了解Spring事务的原理，请[点击这里](/doc/easy-ms-datasource/spring-transaction.md)查看），easy-ms-datasource多数据源事务与其使用方法一样

　　使用方法举例：

```java
@Service
public class TestService {

    @Autowired
    private TestDAO testDAO;

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void insertWithTransaction() {
      	// 插入一条数据到默认数据源的主库
        testDAO.insertData_test_master();
      
        // 这里catch住异常，不让当前事务回滚
        try {
            // Easy-Ms提供了AopUtil工具类，通过该工具类可以实现在本类中调用@Transactional方法
            AopUtil.getAopProxy(this).insertWithNestedTransaction1();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 这里catch住异常，不让当前事务回滚
        try {
            // Easy-Ms提供了AopUtil工具类，通过该工具类可以实现在本类中调用@Transactional方法
            AopUtil.getAopProxy(this).insertWithNestedTransaction2();
        } catch (Exception e) {
            e.printStackTrace();
        }
      	
      	// 插入一条数据到other2的主库
        testDAO.insertData_other2_master();
    }

    @Transactional(propagation = Propagation.NOT_SUPPORTED, rollbackFor = Exception.class)
    public void insertWithNestedTransaction1() {
        testDAO.insertData_other1_master();
        testDAO.insertData_test_master();
        testDAO.insertData_other2_master();
    }

    @Transactional(propagation = Propagation.NESTED, rollbackFor = Exception.class)
    public void insertWithNestedTransaction2() {
        testDAO.insertData_other2_master();
        testDAO.insertData_test_master();
        // other1有唯一索引，这里会抛出异常
        testDAO.insertData_other1_master();
    }

}
```

#### 2. 编程式事务

　　第一种**直接使用spring的自动注入**

```java
// 若没有自己项目中创建的PlatformTransactionManager的bean时，可以直接使用PlatformTransactionManager，否则使用EasyMsDataSourceTransactionManager
@Autowired
private PlatformTransactionManager transactionManager;

@Autowired
private EasyMsDataSourceTransactionManager transactionManager;
```

　　第二种**直接创建对象**

```java
PlatformTransactionManager transactionManager = new EasyMsDataSourceTransactionManager(); 
```

　　使用方法举例：

```java
@Service
public class TestProgrammaticService {

    @Autowired
    private TestDAO testDAO;

    @Autowired
    private PlatformTransactionManager transactionManager;
  
//  private final PlatformTransactionManager transactionManager = new EasyMsDataSourceTransactionManager();

    public void insertWithTransaction() {
        // 创建事务
        TransactionStatus transactionStatus =
                transactionManager.getTransaction(new DefaultTransactionDefinition(TransactionDefinition.PROPAGATION_REQUIRES_NEW));

        try {
            // 执行插入默认数据源的主库
            testDAO.insertData_test_master();

            // 执行嵌套事务1方法
            this.insertWithNestedTransaction1();

            // 执行嵌套事务2方法
            this.insertWithNestedTransaction2();

            // 再执行插入other2的主库
            testDAO.insertData_other2_master();
        } catch (Exception e) {
            // 若有异常执行回滚，然后把异常抛出
            transactionManager.rollback(transactionStatus);
            throw e;
        }

        // 无异常，正常提交事务
        transactionManager.commit(transactionStatus);
    }

    /**
     * 创建嵌套事务1
     */
    private void insertWithNestedTransaction1() {
        TransactionStatus transactionStatus1 =
                transactionManager.getTransaction(new DefaultTransactionDefinition(TransactionDefinition.PROPAGATION_REQUIRED));
        try {
            testDAO.insertData_other1_master();
            testDAO.insertData_test_master();
        } catch (Exception e) {
            transactionManager.rollback(transactionStatus1);
            throw e;
        }
        transactionManager.commit(transactionStatus1);
    }

    /**
     * 创建嵌套事务2
     */
    private void insertWithNestedTransaction2() {
        TransactionStatus transactionStatus2 =
                transactionManager.getTransaction(new DefaultTransactionDefinition(TransactionDefinition.PROPAGATION_NOT_SUPPORTED));
        try {
            testDAO.insertData_other2_master();
            testDAO.insertData_test_master();
            testDAO.insertData_other1_master();
        } catch (Exception e) {
            transactionManager.rollback(transactionStatus2);
            throw e;
        }
        transactionManager.commit(transactionStatus2);
    }

}
```

## 十、 批量操作

　　Easy-Ms框架里提供了**BatchCommit**类，用来专门处理**批量操作**，较单条操作性能可提升上百倍甚至上千倍。

　　批量操作方法以批量插入（批量修改、批量删除类似）举例：

```java
/**
 * 批量插入(非整体提交/回滚)，若批量插入失败时按单条操作（单条操作时性能可能会有降低）
 * 使用的是指定数据源的主库
 *
 * @param datasourceName 指定数据源名称
 * @param daoClass       DAO类
 * @param methodName     方法名
 * @param list           数据集合
 * @param <T>            数据类型
 * @return 批量处理结果：可获取哪些数据处理成功，哪些数据处理失败，结果将按照传入list的顺序返回结果
 */
public <T> BatchResult<T> batchInsert(String datasourceName, Class<?> daoClass, String methodName, List<T> list);

/**
 * 批量插入(非整体提交/回滚)，若批量插入失败时按单条操作（单条操作时性能可能会有降低）
 * 使用的是默认数据源的主库
 *
 * @param daoClass   DAO类
 * @param methodName 方法名
 * @param list       数据集合
 * @param <T>        数据类型
 * @return 批量处理结果：可获取哪些数据处理成功，哪些数据处理失败，结果将按照传入list的顺序返回结果
 */
public <T> BatchResult<T> batchInsert(Class<?> daoClass, String methodName, List<T> list);

/**
 * 批量插入(非整体提交/回滚)，若批量插入失败时按单条操作（单条操作时性能可能会有降低）
 * 使用的是指定数据源的主库
 *
 * @param datasourceName 指定数据源名称
 * @param daoClass       DAO类
 * @param methodName     方法名
 * @param list           数据集合
 * @param perCommitSize  每次最大提交数量，例如设置1000，当数据为3000时将分三次提交，默认值为3000
 * @param <T>            数据类型
 * @return 批量处理结果：可获取哪些数据处理成功，哪些数据处理失败，结果将按照传入list的顺序返回结果
 */
public <T> BatchResult<T> batchInsert(String datasourceName, Class<?> daoClass, String methodName, List<T> list, int perCommitSize);

/**
 * 批量插入(非整体提交/回滚)，若批量插入失败时按单条操作（单条操作时性能可能会有降低）
 * 使用的是默认数据源的主库
 *
 * @param daoClass      DAO类
 * @param methodName    方法名
 * @param list          数据集合
 * @param perCommitSize 每次最大提交数量，例如设置1000，当数据为3000时将分三次提交，默认值为3000
 * @param <T>           数据类型
 * @return 批量处理结果：可获取哪些数据处理成功，哪些数据处理失败，结果将按照传入list的顺序返回结果
 */
public <T> BatchResult<T> batchInsert(Class<?> daoClass, String methodName, List<T> list, int perCommitSize);

/**
 * 批量插入(非整体提交/回滚)，若批量插入失败时按单条操作（单条操作时性能可能会有降低）
 * 使用的是指定数据源的主库
 *
 * @param datasourceName 指定数据源名称
 * @param sqlId          mybatis的xml文件中的namespace+"."+sqlId为完整sqlId
 * @param list           数据集合
 * @param <T>            数据类型
 * @return 批量处理结果：可获取哪些数据处理成功，哪些数据处理失败，结果将按照传入list的顺序返回结果
 */
public <T> BatchResult<T> batchInsert(String datasourceName, String sqlId, List<T> list);

/**
 * 批量插入(非整体提交/回滚)，若批量插入失败时按单条操作（单条操作时性能可能会有降低）
 * 使用的是默认数据源的主库
 *
 * @param sqlId mybatis的xml文件中的namespace+"."+sqlId为完整sqlId
 * @param list  数据集合
 * @param <T>   数据类型
 * @return 批量处理结果：可获取哪些数据处理成功，哪些数据处理失败，结果将按照传入list的顺序返回结果
 */
public <T> BatchResult<T> batchInsert(String sqlId, List<T> list);

/**
 * 批量插入(非整体提交/回滚)，若批量插入失败时按单条操作（单条操作时性能可能会有降低）
 * 使用的是指定数据源的主库
 *
 * @param datasourceName 指定数据源名称
 * @param sqlId          mybatis的xml文件中的namespace+"."+sqlId为完整sqlId
 * @param list           数据集合
 * @param perCommitSize  每次最大提交数量，例如设置1000，当数据为3000时将分三次提交，默认值为3000
 * @param <T>            数据类型
 * @return 批量处理结果：可获取哪些数据处理成功，哪些数据处理失败，结果将按照传入list的顺序返回结果
 */
public <T> BatchResult<T> batchInsert(String datasourceName, String sqlId, List<T> list, int perCommitSize);

/**
 * 批量插入(非整体提交/回滚)，若批量插入失败时按单条操作（单条操作时性能可能会有降低）
 * 使用的是默认数据源的主库
 *
 * @param sqlId         mybatis的xml文件中的namespace+"."+sqlId为完整sqlId
 * @param list          数据集合
 * @param perCommitSize 每次最大提交数量，例如设置1000，当数据为3000时将分三次提交，默认值为3000
 * @param <T>           数据类型
 * @return 批量处理结果：可获取哪些数据处理成功，哪些数据处理失败，结果将按照传入list的顺序返回结果
 */
public <T> BatchResult<T> batchInsert(String sqlId, List<T> list, int perCommitSize);

/**
 * 批量插入(作为一个整体提交/回滚)，若批量插入有失败的情况则整体回滚
 * 使用的是指定数据源的主库
 *
 * @param datasourceName 指定数据源名称
 * @param daoClass       DAO类
 * @param methodName     方法名
 * @param list           数据集合
 * @param <T>            数据类型
 * @return 批量处理结果：true：处理成功，false：处理失败
 */
public <T> boolean batchInsertAsWhole(String datasourceName, Class<?> daoClass, String methodName, List<T> list);

/**
 * 批量插入(作为一个整体提交/回滚)，若批量插入有失败的情况则整体回滚
 * 使用的是默认数据源的主库
 *
 * @param daoClass   DAO类
 * @param methodName 方法名
 * @param list       数据集合
 * @param <T>        数据类型
 * @return 批量处理结果：true：处理成功，false：处理失败
 */
public <T> boolean batchInsertAsWhole(Class<?> daoClass, String methodName, List<T> list);

/**
 * 批量插入(作为一个整体提交/回滚)，若批量插入有失败的情况则整体回滚
 * 使用的是指定数据源的主库
 *
 * @param datasourceName 指定数据源名称
 * @param daoClass       DAO类
 * @param methodName     方法名
 * @param list           数据集合
 * @param perCommitSize  每次最大提交数量，例如设置1000，当数据为3000时将分三次提交，默认值为3000
 * @param <T>            数据类型
 * @return 批量处理结果：true：处理成功，false：处理失败
 */
public <T> boolean batchInsertAsWhole(String datasourceName, Class<?> daoClass, String methodName, List<T> list, int perCommitSize);

/**
 * 批量插入(作为一个整体提交/回滚)，若批量插入有失败的情况则整体回滚
 * 使用的是默认数据源的主库
 *
 * @param daoClass      DAO类
 * @param methodName    方法名
 * @param list          数据集合
 * @param perCommitSize 每次最大提交数量，例如设置1000，当数据为3000时将分三次提交，默认值为3000
 * @param <T>           数据类型
 * @return 批量处理结果：true：处理成功，false：处理失败
 */
public <T> boolean batchInsertAsWhole(Class<?> daoClass, String methodName, List<T> list, int perCommitSize);

/**
 * 批量插入(作为一个整体提交/回滚)，若批量插入有失败的情况则整体回滚
 * 使用的是指定数据源的主库
 *
 * @param datasourceName 指定数据源名称
 * @param sqlId          mybatis的xml文件中的namespace+"."+sqlId为完整sqlId
 * @param list           数据集合
 * @param <T>            数据类型
 * @return 批量处理结果：true：处理成功，false：处理失败
 */
public <T> boolean batchInsertAsWhole(String datasourceName, String sqlId, List<T> list);

/**
 * 批量插入(作为一个整体提交/回滚)，若批量插入有失败的情况则整体回滚
 * 使用的是默认数据源的主库
 *
 * @param sqlId mybatis的xml文件中的namespace+"."+sqlId为完整sqlId
 * @param list  数据集合
 * @param <T>   数据类型
 * @return 批量处理结果：true：处理成功，false：处理失败
 */
public <T> boolean batchInsertAsWhole(String sqlId, List<T> list);

/**
 * 批量插入(作为一个整体提交/回滚)，若批量插入有失败的情况则整体回滚
 * 使用的是指定数据源的主库
 *
 * @param datasourceName 指定数据源名称
 * @param sqlId          mybatis的xml文件中的namespace+"."+sqlId为完整sqlId
 * @param list           数据集合
 * @param perCommitSize  每次最大提交数量，例如设置1000，当数据为3000时将分三次提交，默认值为3000
 * @param <T>            数据类型
 * @return 批量处理结果：true：处理成功，false：处理失败
 */
public <T> boolean batchInsertAsWhole(String datasourceName, String sqlId, List<T> list, int perCommitSize);

/**
 * 批量插入(作为一个整体提交/回滚)，若批量插入有失败的情况则整体回滚
 * 使用的是默认数据源的主库
 *
 * @param sqlId         mybatis的xml文件中的namespace+"."+sqlId为完整sqlId
 * @param list          数据集合
 * @param perCommitSize 每次最大提交数量，例如设置1000，当数据为3000时将分三次提交，默认值为3000
 * @param <T>           数据类型
 * @return 批量处理结果：true：处理成功，false：处理失败
 */
public <T> boolean batchInsertAsWhole(String sqlId, List<T> list, int perCommitSize);
```

　　若想要**将插入、删除、修改同时放在一起批量提交**时，使用该方法：

```java
/**
 * 批量提交(非整体提交/回滚)，若批量提交失败时按单条操作（单条操作时性能可能会有降低）
 * 使用的是指定数据源的主库
 *
 * @param datasourceName   指定数据源名称
 * @param batchDMLUnitList 批量的DML（增删改）数据单元集合
 * @param <T>              数据类型
 * @return 批量处理结果：可获取哪些数据处理成功，哪些数据处理失败，结果将按照传入list的顺序返回结果
 */
public <T> BatchResult<T> batchCommit(String datasourceName, List<BatchDMLUnit<T>> batchDMLUnitList);

/**
 * 批量提交(非整体提交/回滚)，若批量提交失败时按单条操作（单条操作时性能可能会有降低）
 * 使用的是默认数据源的主库
 *
 * @param batchDMLUnitList 批量的DML（增删改）数据单元集合
 * @param <T>              数据类型
 * @return 批量处理结果：可获取哪些数据处理成功，哪些数据处理失败，结果将按照传入list的顺序返回结果
 */
public <T> BatchResult<T> batchCommit(List<BatchDMLUnit<T>> batchDMLUnitList);

/**
 * 批量提交(非整体提交/回滚)，若批量提交失败时按单条操作（单条操作时性能可能会有降低）
 * 使用的是指定数据源的主库
 *
 * @param datasourceName   指定数据源名称
 * @param batchDMLUnitList 批量的DML（增删改）数据单元集合
 * @param perCommitSize    每次最大提交数量，例如设置1000，当数据为3000时将分三次提交，默认值为3000
 * @param <T>              数据类型
 * @return 批量处理结果：可获取哪些数据处理成功，哪些数据处理失败，结果将按照传入list的顺序返回结果
 */
public <T> BatchResult<T> batchCommit(String datasourceName, List<BatchDMLUnit<T>> batchDMLUnitList, int perCommitSize);

/**
 * 批量提交(非整体提交/回滚)，若批量提交失败时按单条操作（单条操作时性能可能会有降低）
 * 使用的是默认数据源的主库
 *
 * @param batchDMLUnitList 批量的DML（增删改）数据单元集合
 * @param perCommitSize    每次最大提交数量，例如设置1000，当数据为3000时将分三次提交，默认值为3000
 * @param <T>              数据类型
 * @return 批量处理结果：可获取哪些数据处理成功，哪些数据处理失败，结果将按照传入list的顺序返回结果
 */
public <T> BatchResult<T> batchCommit(List<BatchDMLUnit<T>> batchDMLUnitList, int perCommitSize);

/**
 * 批量提交(作为一个整体提交/回滚)，若批量提交有失败的情况则整体回滚
 * 使用的是指定数据源的主库
 *
 * @param datasourceName   指定数据源名称
 * @param batchDMLUnitList 批量的DML（增删改）数据单元集合
 * @param <T>              数据类型
 * @return 批量处理结果：可获取哪些数据处理成功，哪些数据处理失败，结果将按照传入list的顺序返回结果
 */
public <T> boolean batchCommitAsWhole(String datasourceName, List<BatchDMLUnit<T>> batchDMLUnitList);

/**
 * 批量提交(作为一个整体提交/回滚)，若批量提交有失败的情况则整体回滚
 * 使用的是默认数据源的主库
 *
 * @param batchDMLUnitList 批量的DML（增删改）数据单元集合
 * @param <T>              数据类型
 * @return 批量处理结果：可获取哪些数据处理成功，哪些数据处理失败，结果将按照传入list的顺序返回结果
 */
public <T> boolean batchCommitAsWhole(List<BatchDMLUnit<T>> batchDMLUnitList);

/**
 * 批量提交(作为一个整体提交/回滚)，若批量提交有失败的情况则整体回滚
 * 使用的是指定数据源的主库
 *
 * @param datasourceName   指定数据源名称
 * @param batchDMLUnitList 批量的DML（增删改）数据单元集合
 * @param perCommitSize    每次最大提交数量，例如设置1000，当数据为3000时将分三次提交，默认值为3000
 * @param <T>              数据类型
 * @return 批量处理结果：可获取哪些数据处理成功，哪些数据处理失败，结果将按照传入list的顺序返回结果
 */
public <T> boolean batchCommitAsWhole(String datasourceName, List<BatchDMLUnit<T>> batchDMLUnitList, int perCommitSize);

/**
 * 批量提交(作为一个整体提交/回滚)，若批量提交有失败的情况则整体回滚
 * 使用的是默认数据源的主库
 *
 * @param batchDMLUnitList 批量的DML（增删改）数据单元集合
 * @param perCommitSize    每次最大提交数量，例如设置1000，当数据为3000时将分三次提交，默认值为3000
 * @param <T>              数据类型
 * @return 批量处理结果：可获取哪些数据处理成功，哪些数据处理失败，结果将按照传入list的顺序返回结果
 */
public <T> boolean batchCommitAsWhole(List<BatchDMLUnit<T>> batchDMLUnitList, int perCommitSize);
```

```java
/**
 * 需要批量处理的DML封装信息，一个单元存放一条数据
 */
@Getter
public class BatchDMLUnit<T> {

    private Class<?> daoClass;

    private String methodName;

    private String sqlId;

    private T obj;

    public BatchDMLUnit(String sqlId, T obj) {
        this.sqlId = sqlId;
        this.obj = obj;
    }

    public BatchDMLUnit(Class<?> daoClass, String methodName, T obj) {
        this.daoClass = daoClass;
        this.methodName = methodName;
        this.obj = obj;
    }
}
```

　　使用demo举例（使用easy-ms-rest）：

```java
@Service
public class EasyMsDatasourceDemo8Service implements RestService<BaseInput, BaseOutput> {

    @Autowired
    private BatchCommit batchCommit;

    @Override
    public BaseOutput execute(BaseInput input) throws InterruptedException {
        final int threadCount = 10, loopCount = 10, commitCount = 3000;
        List<Callable<Boolean>> callableList = new ArrayList<>();
        for (int i = 0; i < threadCount; i++) {
            callableList.add(() -> {
                for (int k = 0; k < loopCount; k++) {
                    List<Map<String,Object>> list = new ArrayList<>();
                    for (int j = 0; j < commitCount; j++) {
                        Map<String, Object> map = new HashMap<>(2);
                        map.put("name", "default");
                        map.put("age", j);
                        list.add(map);
                    }
                    // 使用默认数据源，可知相应sqlId且该sqlId基本不会变动的情况下使用该方法
                  	// 若可能变动时也可将sqlId写在常量类中或者在可配置的配置文件中
                    batchCommit.batchInsert(com.stars.easyms.demo.repository.daoOracleData", list);
                }
                // 多数据源方式批量提交
                for (int k = 0; k < loopCount; k++) {
                    List<Map<String,Object>> list = new ArrayList<>();
                    for (int j = 0; j < commitCount; j++) {
                        Map<String, Object> map = new HashMap<>(2);
                        map.put("name", "other1");
                        map.put("age", j);
                        list.add(map);
                    }
                    // 使用非默认数据源，在DAO类中有对应方法时可以使用该方法，否则使用batchInsert(sqlId, list)的方法
                    batchCommit.batchInsert("other1", TestDAO.class, "batchInsertMysqlData", list);
                }
                return true;
            });
        }

        long startTime = System.currentTimeMillis();
        ExecutorUtil.submitAndDone(callableList);
        BaseOutput baseOutput = new BaseOutput();
        baseOutput.setRetMsg("批量提交" + (threadCount * loopCount * commitCount) + "条数据用时：" + (System.currentTimeMillis() - startTime));
        return baseOutput;
    }

}
```

　　**注：**如果是**Mysql数据库**，需要在连接字符串中增加属性**rewriteBatchedStatements=true**才能支持真正的批量操作，**若在配置数据源时不配置连接参数，默认是true**。

## 十一、 监控

　　easy-ms-datasource提供了实时监控（目前只支持单服务实时监控，后续会提供分布式实时监控），可通过打开url（ip:port/datasource）的方式开启实时监控，目前页面只可显示Json格式输出，后续会有更多参数完善以及页面的优化。启动项目完成后截图如下：

![monitor](/doc/easy-ms-datasource/image/monitor.png)

## 十二、 负载均衡

目前支持四种负载均衡策略：

1. **主备模式（active_standby）**：connect属性中配置的第一个地址为主数据库，其余为备数据库，当主数据库因为各种原因连接失败时自动切换到备数据库，备数据库再失败再切换下一个备数据库，以此类推直到全部失败，**master数据源的默认负载均衡策略**。
2. **最可用模式（best-available）**：选取当前数据源列表中最空闲的数据源进行操作，例如：数据源A1有10个线程正在使用、数据源A2只有5个线程正在使用、数据源A3只有2个线程正在使用，此时优先选取数据源A3进行操作。最可用模式会考虑数据库连接的数量与用时长短，可以使数据库的CPU使用率变得平均化，而不会过高过低，**slave数据源的默认负载均衡策略**。
3. **轮询模式（round_robin）**：循环依次选取数据源列表中的数据源进行操作，轮询模式不会考虑每个连接的用时长短，会平均的分配数据库连接。
4. **随机模式（random）**：随机选择数据源列表中的数据源进行操作。

## 十三、 读写分离实现比较

**关于mysql的读写分离，有两种方式**：

1. 手动在代码层实现逻辑，来解析读请求或写请求，分别分发到不同的数据库中，实现读写分离。
2. 基于MyCat中间件来实现读写分离。

**传统的读写分离框架和mycat的优劣分析**：

1. **传统的读写分离框架在代码层实现读写分离**：主要的**优点**就是**灵活，可以自己根据不同的需求对读写分离的规则进行定制化开发**，但其缺点也十分明显，就是当动态增减主从库数量的时候，都需要对代码进行一个或多或少的修改。并且当主库宕机了，若没有实现相应的容灾逻辑，那么整个数据库集群将丧失对外的写功能。

2. **使用MyCat中间件实现读写分离**，**优点**就是只需要搭建好mycat应用服务，然后进行配置就可以享受读写分离带来的效率的提升，不用写一行代码，并且当主库宕机时，还可以通过配置的方式进行主从库的自动切换，这样即使主库宕机整个集群也不会丧失写的功能。其缺点就是我们得多付出一台服务器作为虚拟节点，还需要进行管理和维护，服务器也是需要成本的，搭建、配置、维护也是需要成本的。

**Easy-Ms框架和传统读写分离框架比较：**

1. **Easy-Ms框架较传统读写分离框架增加了较多的功能**，包括多主多从的支持、配置的简化、负载均衡的支持、动态增减主从库及其他数据源的支持、容灾的能力、实时监控的支持、批量操作的支持、多数据源分布式事务的支持、自动分页及异步查询总数的支持等等。
2. 传统读写分离框架大都只支持一主一从的方式，且每增加一个数据源就需要再创建一个bean，不够智能也稍显麻烦，**Easy-Ms框架不仅支持多主多从，而且无论增、减、修改数据源都无需添加、删除、修改任何代码**，大大提升了开发效率，同时使用也较为方便，不仅提供了注解声明式，还提供了手动编程式，比传统读写分离框架更灵活，功能更加强大。
3. Easy-Ms框架相比MyCat中间件来说，几乎包含了MyCatt中间件的所有功能，但是却比MyCatt中间件使用更灵活，支持的场景更多，可控制的粒度更细腻，也节约了MyCatt中间件的使用成本。
4. 传统读写分离框架基本都是入门级别的代码实现，逻辑也较为简单，Easy-Ms使用了更深层次的架构设计及代码实现，逻辑也稍显复杂，但支持的功能也更多。



> 　　我们提供框架的维护和技术支持，提供给感兴趣的开发人员学习和交流的平台，同时希望感兴趣的同学一起加入我们让框架更完善，让我们一起为了给大家提供更好更优秀的框架而努力。
>
> 　　目前该框架暂时只支持阿里巴巴的druid数据源，若需使用Hikari或其他数据源的，或该框架及本文档中有什么错误及问题的，可采用创建Issue、加群@管理、私聊作者或管理员的方式进行联系，谢谢！

　　