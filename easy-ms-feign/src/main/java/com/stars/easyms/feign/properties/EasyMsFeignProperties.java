package com.stars.easyms.feign.properties;

import com.stars.easyms.base.constant.EasyMsCommonConstants;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * <p>className: EasyMsFeignProperties</p>
 * <p>description: EasyMs的feign模块属性类</p>
 *
 * @author guoguifang
 * @version 1.7.1
 * @date 2020/12/21 11:41 上午
 */
@Data
@ConfigurationProperties(prefix = EasyMsFeignProperties.PREFIX)
public class EasyMsFeignProperties {

    public static final String PREFIX = EasyMsCommonConstants.EASY_MS_PROPERTIES_PREFIX + "feign";

    /**
     * Whether or not to print the request, default true
     */
    private boolean logRequest = true;

    /**
     * Whether or not to print the response, default true
     */
    private boolean logResponse = true;
}
