package com.stars.easyms.feign.request;

import feign.RequestInterceptor;
import feign.RequestTemplate;

/**
 * <p>className: EasyMsFeignRequestInterceptor</p>
 * <p>description: EasyMs调用feign时的请求拦截器</p>
 *
 * @author guoguifang
 * @version 1.6.3
 * @date 2020/9/17 2:09 下午
 */
public class EasyMsFeignRequestInterceptor extends FeignRequestInterceptor implements RequestInterceptor {

    @Override
    public void apply(RequestTemplate template) {
        traceRequest(template, null, null);
    }

}
