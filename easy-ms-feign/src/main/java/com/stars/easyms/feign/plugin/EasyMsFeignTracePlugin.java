package com.stars.easyms.feign.plugin;

import java.util.List;

/**
 * <p>className: EasyMsFeignTracePlugin</p>
 * <p>description: Easy-ms的feign模块trace相关插件</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/12/5 10:38 下午
 */
public interface EasyMsFeignTracePlugin {

    /**
     * 不打印feign调用日志的url列表
     *
     * @return url列表
     */
    List<String> ignoredTraceUrlList();
}
