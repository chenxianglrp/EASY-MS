package com.stars.easyms.feign.plugin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * <p>className: EasyMsFeignPluginRegister</p>
 * <p>description: EasyMsFeign插件注册器</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/12/6 9:34 下午
 */
public final class EasyMsFeignPluginRegister {

    private static final Logger logger = LoggerFactory.getLogger(EasyMsFeignPluginRegister.class);

    private static Set<String> ignoredTraceUrlSet;

    public static void init() {
        try {
            // 获取所有的实现了EasyMsConfigRefreshListener接口的类
            List<String> localIgnoredTraceUrlList = new ArrayList<>();
            ServiceLoader<EasyMsFeignTracePlugin> feignTracePlugins = ServiceLoader.load(EasyMsFeignTracePlugin.class);
            for (EasyMsFeignTracePlugin feignTracePlugin : feignTracePlugins) {
                List<String> ignoredTraceUrlList = feignTracePlugin.ignoredTraceUrlList();
                if (ignoredTraceUrlList != null && !ignoredTraceUrlList.isEmpty()) {
                    localIgnoredTraceUrlList.addAll(ignoredTraceUrlList);
                }
            }
            ignoredTraceUrlSet = new HashSet<>(localIgnoredTraceUrlList);
        } catch (Exception ex) {
            logger.error("Initialization EasyMsFeignPlugin failed!", ex);
        }
    }

    public static boolean isIgnoredTraceUrl(String url) {
        if (ignoredTraceUrlSet != null) {
            return ignoredTraceUrlSet.contains(url);
        }
        return false;
    }

    private EasyMsFeignPluginRegister() {
    }
}
