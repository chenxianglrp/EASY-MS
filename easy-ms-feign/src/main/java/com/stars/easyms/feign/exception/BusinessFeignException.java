package com.stars.easyms.feign.exception;

import feign.FeignException;
import feign.Request;
import lombok.Getter;

/**
 * feign调用异常
 *
 * @author guoguifang
 * @date 2020-08-17 18:34
 * @since 1.6.1
 */
@Getter
public class BusinessFeignException extends FeignException {

    private String retCode;

    private String retMsg;

    private String errorDesc;

    public BusinessFeignException(int status, String message, Request request, String retCode, String retMsg) {
        super(status, message, request);
        this.retCode = retCode;
        this.retMsg = retMsg;
    }

    public BusinessFeignException(int status, String message, Request request, String retCode, String retMsg, String errorDesc) {
        super(status, message, request);
        this.retCode = retCode;
        this.retMsg = retMsg;
        this.errorDesc = errorDesc;
    }

}
