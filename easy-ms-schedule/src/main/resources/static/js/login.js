(function ($) {
    'use strict';

    createGalaxy($('.base-bg'), galaxy);

    removeLoginInfo();

    $('.sign-btn button').on('click', function () {
        submit($(this))
    });

    function submit(node) {
        var userName = $('input[type="text"]').val();
        var password = $('input[type="password"]').val();
        if (userName == '' || password == '') {
            showErrorNoty({
                progressBar: false,
                timeout: 1500,
                text: 'UserName and password cannot be empty!'
            });
            return;
        }
        setCycleTextWithPoint(node, 'Signing');
        node.off('click').addClass('disabled');
        ajaxPost({
            url: "/schedule/permission/login",
            data: {
                userName: userName,
                password: CryptoJS.MD5(password).toString()
            },
            success: function (json) {
                if (json.status == 'success') {
                    setLoginInfo(json.data);
                    toIndex();
                } else {
                    removeCycle(node);
                    node.on('click', function() {
                        submit(node);
                    }).removeClass('disabled');
                    setTimeout(function () {
                        showErrorNoty({
                            progressBar: false,
                            timeout: 1500,
                            text: 'UserName or password is not correct!'
                        });
                    }, 500);
                }
            }
        });
    }
})(jQuery);