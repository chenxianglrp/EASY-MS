(function ($) {
    "use strict";

    createSearchPage($("#content"), {
        createTableHead: function(theadTr) {
            theadTr.append($('<th style="width:9rem;border-left:0"><span class="translate" translateFor="label.mqConfigId"></span></th>'))
                .append($('<th style="width:7.5rem"><span class="translate" translateFor="label.mqType"></span><i class="la la-filter filter"></i></th>'))
                .append($('<th style="min-width:10rem" class="text-align-center"><span class="translate" translateFor="label.mqTypeParams"></span></th>'))
                .append($('<th style="width:7rem;border-right:0" class="text-align-center"><span class="translate" translateFor="label.actions"></span></th>'));
        },
        createTableBody: function(tbody) {
            axiosPost({
                url: "/schedule/mq/getAllViewServer",
                success: function (json) {
                    var tbodyHtml = '';
                    if (json && json.length > 0) {
                        for (var i in json) {
                            if (json.hasOwnProperty(i)) {
                                var mqType = json[i].mqType;
                                tbodyHtml += '<tr>'
                                    + '<td class="selector text-primary"><input name="id" type="hidden" value="' + json[i].mqConfigId + '"/><span class="text-ellipsis">' + json[i].mqConfigId + '</span></td>'
                                    + '<td class="selector text-align-center"><span class="translate" translateFor="mqType" translateValue="' + mqType + '"></span></td>';
                                if (mqType == 'R') {
                                    tbodyHtml += '<td class="selector"><span class="translate hint" translateFor="hint.mqNameServer"></span> ' + (json[i].mqConnection || '')
                                        + '<span class="translate ml-4 hint"  translateFor="hint.mqProducerGroupName"></span> ' + (json[i].mqProducerGroupName || '')
                                        + '<span class="translate ml-4 hint" translateFor="hint.mqConsumerGroupName"></span> ' + (json[i].mqConsumerGroupName || '') + '</td>';
                                } else {
                                    tbodyHtml += '<td class="selector"><span class="translate hint" translateFor="hint.mqBrokerUrl"></span> ' + (json[i].mqConnection || '')
                                        + '<span class="translate ml-4 hint" translateFor="hint.mqUser"></span> ' + (json[i].mqUser || '')
                                        + '<span class="translate ml-4 hint" translateFor="hint.mqPassword"></span> ' + (json[i].mqPassword || '') + '</td>';
                                }
                                tbodyHtml += '<td class="td-actions text-align-center">'
                                    + '<a href="javascript:void(0)" class="translate" translateFor="text.edit" translateTo="popoverTitle" action="editMQConfig"><i class="la la-edit edit"></i></a>'
                                    + '<a href="javascript:void(0)" class="translate" translateFor="text.delete" translateTo="popoverTitle" action="deleteMQConfig"><i class="la la-trash  delete"></i></a>'
                                    + '</td></tr>';
                            }
                        }
                    }
                    tbody.html(tbodyHtml);
                    createSearchTableBody(tbody, {
                        ordering: true,
                        columnDefs: [{
                            'targets': [0, 2, 3, 4],
                            'orderable': false
                        }]
                    });
                }
            });
        }
    });
})(jQuery);