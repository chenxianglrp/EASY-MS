"use strict";

function resetSubTaskStatusToBlock(node, fun) {
    node = $(node);
    var data = node.data('data');
    newConfirmModal({
        title: "modal.title.resetSubTaskStatus",
        titleReplace: [cdata(data.partitionCount), cdata(data.partitionIndex), "taskStatus.B"],
        content: "modal.warn.resetSubTaskStatusContent",
        okFun: function () {
            modifySubTaskStatus({
                subTaskId: data.idStr,
                toTaskStatus: 'B',
                data: data,
                successFun: function (result) {
                    if (result.status === 'success') {
                        showSuccessNoty({translateFor: 'message.setSuccess'});
                        node.attr('translateValue', 'B');
                        translate(node);
                        fun && fun(data, result.data.subTask);
                    } else {
                        showErrorNoty({translateFor: 'message.' + (result.msgCode || 'setFail')});
                    }
                }
            });
        }
    });
}

function modifySubTaskStatus(options) {
    axiosPost({
        url: "/schedule/task/changeSubTaskStatus/" + options.subTaskId + "/" + options.toTaskStatus,
        data: options.data,
        success: function (json) {
            options.successFun && options.successFun(json);
        }
    });
}