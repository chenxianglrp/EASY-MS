(function ($) {
    "use strict";

    var modalId = "showTaskDetail";
    var id = getModalData(modalId);
    axiosPost({
        url: "/schedule/task/getTaskDetailById/" + id,
        success: function (json) {
            if (!json.id) {
                showErrorNoty({
                    progressBar: false,
                    timeout: 1500,
                    translateFor: 'message.systemException'
                });
                return;
            }
            modalContainer.find('span[name="taskId"]').text(json.taskId);
            if (json.taskGroup) {
                modalContainer.find('span[name="taskGroup"]').text(json.taskGroup);
                modalContainer.find('span[name="taskStep"]').text(json.taskStep);
            } else {
                taskStep.detach();
                modalContainer.find('span[name="taskGroup"]').hide();
                modalContainer.find('div[name="taskGroup"]').show();
            }
            modalContainer.find('span[name="taskName"]').text(json.taskName);
            modalContainer.find('span[name="batchNo"]').text(json.batchNo);
            modalContainer.find('span[name="taskType"]').addClass('translate').attr('translateFor', 'taskType').attr('translateValue', json.taskType);
            if ('N' === json.taskType) {
                task_type_remote.detach();
                task_type_mq.detach();
                modalContainer.find('span[name="beanId"]').text(json.beanId);
                modalContainer.find('span[name="beanClass"]').text(json.beanClass);
            } else if ('R' === json.taskType) {
                task_type_native.detach();
                task_type_mq.detach();
                modalContainer.find('span[name="remoteUrl"]').text(json.remoteUrl);
            } else if ('M' === json.taskType) {
                task_type_native.detach();
                task_type_remote.detach();
                modalContainer.find('span[name="mqConfigId"]').text(json.mqConfigId);
                modalContainer.find('span[name="mqDestination"]').text(json.mqDestination);
                modalContainer.find('span[name="mqCallback"]').text(json.mqCallback);
            }
            modalContainer.find('span[name="priority"]').text(json.priority);
            modalContainer.find('span[name="partitionCount"]').text(json.partitionCount);
            modalContainer.find('span[name="partitionMode"]').addClass('translate').attr('translateFor', 'partitionMode').attr('translateValue', json.partitionMode);
            modalContainer.find('span[name="taskCycle"]').addClass('translate').attr('translateFor', 'taskCycle').attr('translateValue', json.taskCycle);
            modalContainer.find('span[name="cronExpression"]').text(json.cronExpression);
            modalContainer.find('span[name="currentFireTime"]').text(json.currentFireTimeStr);
            modalContainer.find('span[name="currentFinishTime"]').text(json.currentFinishTimeStr);
            modalContainer.find('span[name="nextFireTime"]').text(json.nextFireTimeStr);
            modalContainer.find('span[name="status"]').addClass('translate ' + json.statusLevel).attr('translateFor', 'taskStatus').attr('translateValue', json.status);
            if (json.errorMessage) {
                modalContainer.find('span[name="errorMessage"]').text(json.errorMessage);
            }
            modalContainer.find('span[name="dependentTaskId"]').text(json.dependentTaskId);
            modalContainer.find('span[name="dependencePolicy"]').addClass('translate').attr('translateFor', 'dependencePolicy').attr('translateValue', json.dependencePolicy);
            modalContainer.find('span[name="blockPolicy"]').addClass('translate').attr('translateFor', 'blockPolicy').attr('translateValue', json.blockPolicy);
            modalContainer.find('span[name="errorPolicy"]').addClass('translate').attr('translateFor', 'errorPolicy').attr('translateValue', json.errorPolicy);
            modalContainer.find('span[name="retryCount"]').text(json.retryCount);
            modalContainer.find('span[name="retryIndex"]').text(json.retryIndex);
            modalContainer.find('span[name="retryInterval"]').text(json.retryInterval);
            modalContainer.find('span[name="retryPolicy"]').addClass('translate').attr('translateFor', 'retryPolicy').attr('translateValue', json.retryPolicy);
            modalContainer.find('span[name="parameters"]').text(json.parameters);
            modalContainer.find('span[name="useWhiteBlackList"]').addClass('translate').attr('translateFor', 'yesNo').attr('translateValue', json.useWhiteBlackList);
            modalContainer.find('span[name="alarm"]').addClass('translate').attr('translateFor', 'yesNo').attr('translateValue', json.alarm);
            modalContainer.find('span[name="taskSwitch"]').addClass('translate').attr('translateFor', 'taskSwitch').attr('translateValue', json.taskSwitch);
            modalContainer.find('.display-setting').each(function(){
                if ($(this).text() === '') {
                    $(this).addClass('translate').attr('translateFor', 'btn.setting');
                }
            });
            translate(modalContainer);
        }
    });

    var modalContainer = $("#" + modalId);
    var taskGroup = modalContainer.find('.task-group');
    var taskStep = modalContainer.find('.task-step');
    var taskType = modalContainer.find('.task-type');
    var task_type_native = modalContainer.find('.task-type-native');
    var task_type_remote = modalContainer.find('.task-type-remote');
    var task_type_mq = modalContainer.find('.task-type-mq');
    modalContainer.find('.display-setting').each(function () {
        var $this = $(this);
        var type = $this.attr('name');
        var input = modalContainer.find('input[name=' + type + ']');
        var cancelBtn = $('<button class="btn btn-shadow btn-w-2 cancel" style="margin:0 5px"><span class="translate" translateFor="btn.cancel"></span></button>').on('click', function(){$this.show().next().hide()});
        var okBtn = $('<button class="btn btn-primary btn-shadow btn-w-2 ok"><span class="translate" translateFor="btn.ok"></span></button>').on('click', function () {
            var text = $this.attr('translateFor') ? '' : $this.text();
            var value = $.trim(input.val());
            if (input.attr('required') && value === '') {
                showErrorNoty({translateFor: 'message.' + type + 'Null'});
                return;
            }
            if (value !== text) {
                var confirmParam = {
                    title: "modal.title.setting_" + type,
                    titleReplace: cdata(value),
                    content: "modal.warn.settingWarn_" + type,
                    okFun: function () {
                        axiosPost({
                            url: "/schedule/task/updateTask",
                            data: {
                                id: id,
                                type: type,
                                value: value
                            },
                            success: function (data) {
                                if (data.status === 'success') {
                                    data.msgCode ? showErrorNoty({translateFor: 'message.' + data.msgCode}) : showSuccessNoty({translateFor: 'message.setSuccess'});
                                    if (value === '') {
                                        $this.addClass('translate').attr('translateFor', 'btn.setting');
                                        translate($this);
                                    } else {
                                        $this.removeClass('translate').removeAttr('translateFor').text(value);
                                    }
                                    $this.show().next().hide();
                                } else {
                                    showErrorNoty({translateFor: 'message.' + (data.msgCode || 'setFail')});
                                }
                            }
                        });
                    }
                };
                if (value === '') {
                    confirmParam['title'] = "modal.title.delete_" + type;
                    confirmParam['titleReplace'] = null;
                    confirmParam['content'] = "modal.warn.deleteWarn_" + type;
                }
                newConfirmModal(confirmParam);
            } else {
                $this.show().next().hide()
            }
        });
        input.detach();
        $this.on('click', function(){
            input.val($this.attr('translateFor') ? '' : $this.text());
            $this.hide().next().show();
            !input.hasClass('datepicker') && input.focus();
        }).next().append($('<span class="padding-0 fc-button-group col-lg-6"></span>').append(input)).append(cancelBtn).append(okBtn);
    });
    modalContainer.find('input[name="nextFireTime"]').daterangepicker({
        singleDatePicker: true,
        autoApply: false,
        autoUpdateInput: false,
        timePicker24Hour: true,
        timePickerSeconds: true,
        timePicker: true,
        "locale": {
            format: 'YYYY-MM-DD HH:mm:ss'
        },
        "minDate": moment()
    }).on('apply.daterangepicker', function (ev, picker) {
        picker.element.val(picker.startDate.format(picker.locale.format));
    }).on('beforeShow.daterangepicker', function (ev, picker) {
        picker.startDate = picker.minDate = moment();
    }).on('show.daterangepicker', function (ev, picker) {
        picker.element.addClass('focus');
    }).on('hide.daterangepicker', function (ev, picker) {
        picker.element.removeClass('focus');
    });
    var prioritySlider = createSlider({ min:1, max:10, defaultValue:5, popover:true, background:'linear-gradient(to right, #5bc0de 0%, #5d5386 50%, #e23f83 100%)'});
    modalContainer.find('.priority_slider').append(prioritySlider);
    loadInputEmbedCloseBtns(modalContainer);
})(jQuery);