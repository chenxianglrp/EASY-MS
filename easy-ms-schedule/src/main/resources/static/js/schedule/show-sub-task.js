(function ($) {
    "use strict";

    var modalId = "showSubTask";
    var modalContainer = $("#" + modalId);
    var id = getModalData(modalId);

    page.createOrRefreshSubTaskTableBody = function (options) {
        var tbody = getModalContext(modalId, 'tbody');
        axiosPost({
            url: "/schedule/task/getSubTaskByTaskId/" + id,
            success: function (json) {
                if (!json) {
                    showErrorNoty({
                        progressBar: false,
                        timeout: 1500,
                        translateFor: 'message.systemException'
                    });
                    return;
                }
                var subTaskList = json.data.subTaskList;
                tbody.empty();
                if (subTaskList.length === 0) {
                    tbody.append($("<tr><td colspan='6' class='text-align-center'><span class='translate' translateFor='message.resultIsEmpty'></span></td></tr>"));
                } else {
                    for (var i in subTaskList) {
                        if (subTaskList.hasOwnProperty(i)) {
                            if (!subTaskList[i].id) {
                                page.createOrRefreshSubTaskTableBody(options);
                                return;
                            }
                            var status = subTaskList[i].status;
                            var tr = $('<tr>'
                                + '<td class="text-align-center">' + subTaskList[i].partitionCount + '</td>'
                                + '<td class="text-align-center">' + subTaskList[i].partitionIndex + '</td>'
                                + '<td class="text-align-center"><span class="badge-text badge-text-small translate status ' + subTaskList[i].statusLevel + '" translateFor="taskStatus" translateValue="' + status + '"></span></td>'
                                + '<td class="text-align-center executeServerIp">' + (subTaskList[i].executeServerIp || '-') + '</td>'
                                + '<td class="text-align-center executeServerPort">' + (subTaskList[i].executeServerPort || '-') + '</td>'
                                + '<td><span class="text-ellipsis errorMessage">' + (subTaskList[i].errorMessage || '') + '</span></td></tr>');
                            if (status === 'R' || status === 'RR') {
                                tr.find('span.status').data('data', subTaskList[i]).on('click', function () {
                                    var $this = $(this);
                                    resetSubTaskStatusToBlock($this, function (sourceData, result) {
                                        var tr = $this.off('click').addClass(result.statusLevel).removeClass(sourceData.statusLevel).parents('tr');
                                        tr.find('.executeServerIp').text('-');
                                        tr.find('.executeServerPort').text('-');
                                        tr.find('.errorMessage').text('');
                                    });
                                });
                            }
                            tbody.append(tr);
                        }
                    }
                }
                removeTextEllipsis(tbody);
                translate(modalContainer);
                textEllipsis(tbody, false);
                if (options !== undefined && (options.autoRefresh === true || options.singleRefresh === true)) {
                    if (options.singleRefresh === true) {
                        if (options.noty !== false) {
                            new Noty({
                                type: 'success',
                                layout: 'bottomRight',
                                text: '<span class="translate" translateFor="message.singleRefreshSuccess"></span>',
                                progressBar: true,
                                timeout: 1500,
                                animation: {open: 'animated bounceInRight', close: 'animated bounceOutRight'}
                            }).show();
                        }
                        options['singleRefresh'] = false;
                    }
                    if (options.autoRefresh === true) {
                        var autoRefreshInterval = options.autoRefreshInterval || 5000;
                        setModalTimeout(modalId, function () {
                            page.createOrRefreshSubTaskTableBody(options);
                        }, autoRefreshInterval);
                        tbody.data('autoRefreshInterval', autoRefreshInterval);
                    }
                }
            }
        });
    };

    createSearchPage(modalContainer, {
        createContentHeader: function (contentHeader, batchBtns, rightBtns) {
            contentHeader.append($('<button class="btn btn-gradient-04-left min-width-100" type="button"><span class="translate" translateFor="btn.openAutoRefresh"></span></button>').on('click', function () {
                var $this = $(this);
                var selectedBtn = $this.next();
                var intervalBtn = selectedBtn.next();
                page.createOrRefreshSubTaskTableBody({
                    autoRefresh: true,
                    autoRefreshInterval: intervalBtn.find('.autoRefreshInterval').text() * 1000
                });
                intervalBtn.addClass('selected');
                $this.hide();
                selectedBtn.show();
            }))
                .append($('<button class="btn btn-gradient-04-left min-width-100 selected" type="button"><span class="translate" translateFor="btn.closeAutoRefresh"></span></button>').on('click', function () {
                    var $this = $(this);
                    var unselectedBtn = $this.prev();
                    var intervalBtn = $this.next();
                    clearModalTimeout(modalId);
                    intervalBtn.removeClass('selected');
                    $this.hide();
                    unselectedBtn.show();
                }).hide())
                .append($('<button class="btn btn-gradient-04-right min-width-50" type="button"><span class="autoRefreshInterval">3</span><span class="translate unit" translateFor="timeUnit.second"></span></button>').on('click', function () {
                    var $this = $(this);
                    if ($this.data('btnSelected')) {
                        hidePopoverByNode($this);
                        return;
                    }
                    var slider = createSlider({
                        min: 1,
                        max: 10,
                        defaultValue: $this.find('.autoRefreshInterval').text(),
                        unit: $this.find('.unit').text(),
                        background: 'linear-gradient(to right,#54e38e 0%,#41c7af 50%,#54e38e 100%)'
                    });
                    showPopover($this, {
                        direction: 'top',
                        bodyStyle: 'width:250px;height:50px;margin:0px 6px 15px;',
                        bodyContent: slider,
                        animate: 'slide',
                        animateTime: 300,
                        leftOffset: -4,
                        scrollLock: true,
                        hideFun: function () {
                            $this.data('btnSelected', false);
                            var sliderValue = getSliderValue(slider);
                            getSliderValue(slider) && $this.find('.autoRefreshInterval').text(sliderValue);
                            if ($this.hasClass('selected')) {
                                clearModalTimeout(modalId);
                                page.createOrRefreshSubTaskTableBody({
                                    autoRefresh: true,
                                    autoRefreshInterval: sliderValue * 1000
                                });
                            }
                        }
                    });
                    $this.data('btnSelected', true);
                }));
            rightBtns.append($('<div class="iconBtn translate" translateFor="text.singleRefresh" translateTo="popoverTitle" ><i class="ti ti-reload"></i></div>').on('click', function () {
                page.createOrRefreshSubTaskTableBody({singleRefresh: true})
            }));
        },
        createTableHead: function (theadTr) {
            theadTr.append($('<th style="width:5rem;border-left:0" class="text-align-center"><span class="translate" translateFor="label.partitionCount"></span></th>'))
                .append($('<th style="width:5rem" class="text-align-center"><span class="translate" translateFor="label.partitionIndex"></span></th>'))
                .append($('<th style="width:8rem" class="text-align-center"><span class="translate" translateFor="label.status"></span></th>'))
                .append($('<th style="width:10rem" class="text-align-center"><span class="translate" translateFor="label.executeServerIp"></span></th>'))
                .append($('<th style="width:7rem" class="text-align-center"><span class="translate" translateFor="label.executeServerPort"></span></th>'))
                .append($('<th class="text-align-center"><span class="translate" translateFor="label.errorMessage"></span></th>'));
        },
        createTableBody: function (tbody) {
            pushModalContext(modalId, 'tbody', tbody);
            page.createOrRefreshSubTaskTableBody();
        },
        isCheck: false
    });
})(jQuery);