package com.stars.easyms.schedule.repository.oracle;

import com.stars.easyms.schedule.repository.DbScheduleRepository;
import com.stars.easyms.schedule.repository.DistributedScheduleDDLDAO;

/**
 * 分布式调度框架数据库表结构验证并创建DAO接口(oracle)
 *
 * @author guoguifang
 */
@DbScheduleRepository("oracleDistributedScheduleDDLDAO")
public interface OracleDistributedScheduleDDLDAO extends DistributedScheduleDDLDAO {

}
