package com.stars.easyms.schedule.enums;

/**
 * 重试策略
 *
 * @author guoguifang
 */
public enum RoutePolicy {

    /**
     * 争抢
     */
    SCRAMBLE("S", "争抢", "Scramble"),

    /**
     * 轮询
     */
    POLLING("P", "轮询", "Polling"),

    /**
     * 权重
     */
    WEIGHT("W", "权重", "Weight");

    private String chineseName;

    private String englishName;

    private String code;

    public static RoutePolicy forCode(String code) {
        if (POLLING.code.equalsIgnoreCase(code)) {
            return POLLING;
        } else if (WEIGHT.code.equalsIgnoreCase(code)) {
            return WEIGHT;
        }
        return SCRAMBLE;
    }

    RoutePolicy(String code, String chineseName, String englishName) {
        this.chineseName = chineseName;
        this.englishName = englishName;
        this.code = code;
    }

    public String getChineseName() {
        return chineseName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public String getCode() {
        return code;
    }
}
