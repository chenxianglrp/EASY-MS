package com.stars.easyms.schedule.bean;

import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 分布式调度框架服务器主任务记录
 *
 * @author guoguifang
 */
@Data
public class DbScheduleTaskRecord implements Serializable {

    /**
     * 主任务执行批次号
     */
    private String batchNo;

    /**
     * 主任务执行前一批次号
     */
    private String prevBatchNo;

    /**
     * 主任务主键
     */
    private Long taskId;

    /**
     * 主任务执行开始时间
     */
    private Timestamp fireTime;

    /**
     * 主任务执行结束时间
     */
    private Timestamp finishTime;

    /**
     * 主任务执行用时(毫秒)
     */
    private Long usedTime;

    /**
     * 子任务执行状态(处理异常:E,致命错误:F,处理完成:C)
     */
    private String status;

}
