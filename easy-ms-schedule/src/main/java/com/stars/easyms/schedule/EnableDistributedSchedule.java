package com.stars.easyms.schedule;

import java.lang.annotation.*;

/**
 * springboot项目使用该注解启动分布式调度框架
 *
 * @author guoguifang
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EnableDistributedSchedule {

}
