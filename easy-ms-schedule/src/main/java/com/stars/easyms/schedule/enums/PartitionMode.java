package com.stars.easyms.schedule.enums;

/**
 * 分区模式
 *
 * @author guoguifang
 */
public enum PartitionMode {

    /**
     * 静态模式
     */
    STATIC("S", "静态", "Static"),

    /**
     * 分布式模式
     */
    DYNAMIC("D", "动态", "Dynamic");

    private String chineseName;

    private String englishName;

    private String code;

    public static PartitionMode forCode(String code) {
        if (DYNAMIC.code.equalsIgnoreCase(code)) {
            return DYNAMIC;
        }
        return STATIC;
    }

    PartitionMode(String code, String chineseName, String englishName) {
        this.chineseName = chineseName;
        this.englishName = englishName;
        this.code = code;
    }

    public String getChineseName() {
        return chineseName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public String getCode() {
        return code;
    }
}
