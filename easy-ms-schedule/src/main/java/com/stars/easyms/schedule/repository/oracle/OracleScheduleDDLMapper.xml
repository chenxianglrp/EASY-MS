<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >

<mapper namespace="com.stars.easyms.schedule.repository.oracle.OracleDistributedScheduleDDLDAO">

    <!-- 根据表名验证表是否存在 -->
    <select id="getCountByTableName" parameterType="java.lang.String"  resultType="java.lang.Long">
        SELECT COUNT(*) FROM ${tableName}
    </select>

    <!-- 创建表DB_SCHEDULE_SERVER -->
    <update id="createTable_dbScheduleServer">
        CREATE TABLE DB_SCHEDULE_SERVER(
              ip  VARCHAR2(20) NOT NULL ,
              port  VARCHAR2(10) NOT NULL ,
              server_id VARCHAR2(300) NOT NULL ,
              schedule_switch VARCHAR2(1) NOT NULL ,
              core_pool_size  INTEGER NOT NULL ,
              max_pool_size INTEGER NOT NULL ,
              keep_alive_time VARCHAR2(30) NOT NULL ,
              start_delay_time VARCHAR2(30) NOT NULL ,
              heartbeat_time TIMESTAMP(3) DEFAULT systimestamp NOT NULL ,
              alive VARCHAR2(1) DEFAULT 'Y' NOT NULL ,
              wakeup VARCHAR2(1) DEFAULT 'N' NOT NULL ,
              version INTEGER DEFAULT 1 NOT NULL ,
              constraint PK_DB_SCHEDULE_SERVER primary key(ip, port)
            )
    </update>

    <!-- 创建表DB_SCHEDULE_TASK -->
    <update id="createTable_dbScheduleTask">
        CREATE TABLE DB_SCHEDULE_TASK(
              id  INTEGER NOT NULL ,
              task_id  VARCHAR2(300) NOT NULL ,
              task_group VARCHAR2(300) ,
              task_step INTEGER ,
              task_name VARCHAR2(300) ,
              task_switch VARCHAR2(1) DEFAULT 'O' NOT NULL ,
              batch_no VARCHAR2(50) ,
              partition_count INTEGER DEFAULT 10 NOT NULL ,
              partition_mode VARCHAR2(1) DEFAULT 'S' NOT NULL,
              task_type VARCHAR2(2) DEFAULT 'N' NOT NULL ,
              bean_id VARCHAR2(50),
              bean_class VARCHAR2(300),
              remote_url VARCHAR2(300),
              mq_config_id VARCHAR2(30),
              mq_destination VARCHAR2(300),
              mq_callback VARCHAR2(300),
              parameters CLOB ,
              priority INTEGER NOT NULL ,
              status  VARCHAR2(2) NOT NULL ,
              cron_expression VARCHAR2(300) ,
              current_fire_time TIMESTAMP(3) ,
              current_finish_time TIMESTAMP(3) ,
              next_fire_time TIMESTAMP(3) ,
              error_message VARCHAR2(1000) ,
              dependent_task_id VARCHAR2(500) ,
              dependence_policy VARCHAR2(1) DEFAULT 'S' NOT NULL ,
              block_policy VARCHAR2(1) DEFAULT 'C' NOT NULL ,
              error_policy VARCHAR2(1) DEFAULT 'R' NOT NULL ,
              retry_policy VARCHAR2(1),
              retry_count INTEGER,
              retry_index INTEGER,
              retry_interval VARCHAR2(30),
              use_white_black_list VARCHAR2(1) DEFAULT 'N' NOT NULL ,
              alarm VARCHAR2(1) DEFAULT 'Y' NOT NULL ,
              version INTEGER DEFAULT 1 NOT NULL ,
              constraint PK_DB_SD_TASK primary key(id)
            )
    </update>

    <update id="alterTable_dbScheduleTask_uix_0">
        create unique index UIX_DB_SE_TK_TK_ID on DB_SCHEDULE_TASK(task_id)
    </update>
    <update id="alterTable_dbScheduleTask_idx_0">
        create index IDX_DB_SE_TASK_GROUP on DB_SCHEDULE_TASK(task_group)
    </update>
    <update id="alterTable_dbScheduleTask_idx_1">
        create index IDX_DB_SE_TASK_PRIORITY on DB_SCHEDULE_TASK(priority)
    </update>
    <update id="alterTable_dbScheduleTask_idx_2">
        create index IDX_DB_SE_TASK_MQ_CONFIG on DB_SCHEDULE_TASK(mq_config_id)
    </update>

    <!-- 创建表DB_SCHEDULE_SUB_TASK -->
    <update id="createTable_dbScheduleSubTask">
        CREATE TABLE DB_SCHEDULE_SUB_TASK(
              id  INTEGER NOT NULL ,
              parent_id  INTEGER NOT NULL ,
              partition_count INTEGER NOT NULL ,
              partition_index INTEGER NOT NULL ,
              status  VARCHAR2(2) NOT NULL ,
              error_message VARCHAR2(1000) ,
              execute_server_ip VARCHAR2(20) ,
              execute_server_port VARCHAR2(10) ,
              version INTEGER DEFAULT 1 NOT NULL ,
              constraint PK_DB_SD_SUB_TASK primary key (id)
            )
    </update>

    <update id="alterTable_dbScheduleSubTask_uix_0">
        create index IDX_DB_SE_SUB_PARENT_ID on DB_SCHEDULE_SUB_TASK(parent_id)
    </update>

    <!-- 创建表DB_SCHEDULE_WHITE_BLACK_LIST -->
    <update id="createTable_dbScheduleWhiteBlackList">
        CREATE TABLE DB_SCHEDULE_WHITE_BLACK_LIST(
              task_id  INTEGER NOT NULL ,
              server_ip VARCHAR2(20) NOT NULL ,
              server_port VARCHAR2(10) NOT NULL ,
              direction  VARCHAR2(1) NOT NULL ,
              constraint PK_DB_SD_W_B_LIST primary key (task_id, server_ip, server_port)
            )
    </update>

    <!-- 创建表DB_SCHEDULE_TASK_RECORD -->
    <update id="createTable_dbScheduleTaskRecord">
        CREATE TABLE DB_SCHEDULE_TASK_RECORD(
              task_id INTEGER NOT NULL ,
              batch_no VARCHAR2(50) NOT NULL ,
              prev_batch_no VARCHAR2(50) ,
              fire_time TIMESTAMP(3) ,
              finish_time TIMESTAMP(3) ,
              used_time INTEGER ,
              status VARCHAR2(2) ,
              constraint PK_DB_SD_TK_RD primary key (task_id, batch_no)
            )
    </update>

    <!-- 创建表DB_SCHEDULE_SUB_TASK_RECORD -->
    <update id="createTable_dbScheduleSubTaskRecord">
        CREATE TABLE DB_SCHEDULE_SUB_TASK_RECORD(
              sub_task_id INTEGER NOT NULL ,
              batch_no VARCHAR2(50) NOT NULL ,
              parent_id INTEGER NOT NULL ,
              partition_count INTEGER NOT NULL ,
              partition_index INTEGER NOT NULL ,
              fire_time TIMESTAMP(3) ,
              finish_time TIMESTAMP(3) ,
              used_time INTEGER ,
              status VARCHAR2(2) ,
              error_message CLOB ,
              execute_server_ip VARCHAR2(20) ,
              execute_server_port VARCHAR2(10) ,
              execute_thread_id INTEGER ,
              constraint PK_DB_SD_SUB_TK_RD primary key (sub_task_id, batch_no)
            )
    </update>

    <update id="alterTable_dbScheduleSubTaskRecord_idx_0">
        create index IDX_DB_SD_SUB_TK_RD_TK_ID on DB_SCHEDULE_SUB_TASK_RECORD(parent_id)
    </update>
    <update id="alterTable_dbScheduleSubTaskRecord_idx_1">
        create index IDX_DB_SD_SUB_TK_RD_SERVER on DB_SCHEDULE_SUB_TASK_RECORD(execute_server_ip, execute_server_port, execute_thread_id);
    </update>

    <!-- 创建表DB_SCHEDULE_MQ_CONFIG -->
    <update id="createTable_dbScheduleMqConfig">
        CREATE TABLE DB_SCHEDULE_MQ_CONFIG(
              mq_config_id VARCHAR2(30) NOT NULL ,
              mq_type VARCHAR2(2) NOT NULL ,
              mq_connection VARCHAR2(1000) NOT NULL ,
              mq_user VARCHAR2(300) ,
              mq_password VARCHAR2(300) ,
              mq_producer_group_name VARCHAR2(300) ,
              mq_consumer_group_name VARCHAR2(300) ,
              constraint PK_DB_SD_MQ_CONFIG primary key (mq_config_id)
            )
    </update>

    <!-- 创建表DB_SCHEDULE_USER_INFO -->
    <update id="createTable_dbScheduleUserInfo">
        CREATE TABLE DB_SCHEDULE_USER_INFO(
              user_name VARCHAR2(100) NOT NULL ,
              user_password VARCHAR2(300) ,
              login_time TIMESTAMP(3) ,
              login_token VARCHAR2(100) ,
              page_layout VARCHAR2(2) NOT NULL ,
              page_language VARCHAR2(2) NOT NULL ,
              page_lightup VARCHAR2(1) NOT NULL ,
              page_theme VARCHAR2(10) NOT NULL ,
              dashboard_content VARCHAR2(1000) ,
              version INTEGER DEFAULT 1 NOT NULL ,
              constraint PK_DB_SD_USER_CONFIG primary key (user_name)
            )
    </update>

    <!-- 创建表DB_SCHEDULE_CONFIG -->
    <update id="createTable_dbScheduleConfig">
        CREATE TABLE DB_SCHEDULE_CONFIG(
          version VARCHAR2(50) NOT NULL ,
          version_iteration INTEGER NOT NULL ,
          open_login VARCHAR2(1) DEFAULT 'Y' NOT NULL ,
          native_login VARCHAR2(1) NOT NULL ,
          remote_login_url VARCHAR2(300) ,
          use_sso VARCHAR2(1) DEFAULT 'Y' NOT NULL ,
          session_timeout VARCHAR2(30) DEFAULT '30m' NOT NULL ,
          route_policy VARCHAR2(2) DEFAULT 'S' NOT NULL
        )
    </update>
</mapper>
