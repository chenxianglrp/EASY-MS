package com.stars.easyms.schedule.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Map工具类
 *
 * @author guoguifang
 */
public class MapUtil {

    /**
     * 校验json格式或者key-value格式的字符串格式是否正确
     *
     * @param str json格式或者(key1=value1,key2=value2...)的格式
     * @return true:校验通过,false:校验失败
     */
    public static boolean check(String str) {
        Boolean flag = (Boolean) parse(str, true);
        return flag != null && flag;
    }

    /**
     * 把json格式或者key-value格式的字符串转换成Map类型
     *
     * @param str json格式或者(key1=value1,key2=value2...)的格式
     * @return Map对象
     */
    @SuppressWarnings("unchecked")
    public static Map<String, Object> parse(String str) {
        return (Map<String, Object>) parse(str, false);
    }

    /**
     * 把json格式或者key-value格式的字符串转换成Map类型
     *
     * @param str     json格式或者(key1=value1,key2=value2...)的格式
     * @param isCheck 是否是检查，当在页面配置的时候需要检查格式是否正确，即校验不通过的参数信息不允许保存
     * @return Map对象
     */
    private static Object parse(String str, boolean isCheck) {
        if (StringUtils.isBlank(str)) {
            return null;
        }

        // 去掉首尾空字符
        str = str.trim();

        Map<String, Object> map = new HashMap<>(64);
        // 判断是否是json格式的字符串
        if (isCheck) {
            if (str.startsWith("{") && !str.endsWith("}")) {
                return Boolean.FALSE;
            }
            if (!str.startsWith("{") && str.endsWith("}")) {
                return Boolean.FALSE;
            }
        }
        char splitWord = '=';
        if (str.startsWith("{") && str.endsWith("}")) {
            // 去掉首尾符号以及空字符
            str = str.substring(1, str.length() - 1).trim();
            splitWord = ':';
        }

        int start = 0;
        String key = "";
        Object value = null;
        List<String> list = null;
        boolean isKey = true;
        boolean isListNest = false;
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (isKey) {
                if (c == splitWord) {
                    key = str.substring(start, i).trim().replaceAll("\"", "").replaceAll("'", "");
                    start = i + 1;
                    isKey = false;
                }
            } else {
                if (!isListNest && (i == str.length() - 1 || c == ',')) {
                    if (value == null) {
                        if (i == str.length() - 1) {
                            i++;
                        }
                        value = str.substring(start, i).trim().replaceAll("\"", "").replaceAll("'", "");
                    }
                    start = i + 1;
                    isKey = true;
                } else if (isListNest && i == str.length() - 1) {
                    if (c != ']') {
                        return null;
                    }
                    list.add(str.substring(start, i).trim().replaceAll("\"", "").replaceAll("'", ""));
                    isKey = true;
                } else if (c == '[') {
                    list = new ArrayList<>();
                    value = list;
                    isListNest = true;
                    start = i + 1;
                } else if (isListNest) {
                    if (c != ',' && c != ']') {
                        continue;
                    }
                    list.add(str.substring(start, i).trim().replaceAll("\"", "").replaceAll("'", ""));
                    start = i + 1;
                    if (c == ']') {
                        isListNest = false;
                    }
                }
                if (isKey) {
                    map.put(key, value);
                    value = null;
                    count++;
                }
            }
        }
        if (count == 0) {
            if (isCheck) {
                return Boolean.FALSE;
            }
            return null;
        } else {
            if (isCheck) {
                return Boolean.TRUE;
            }
            return map;
        }
    }
}
