package com.stars.easyms.schedule.exception;

/**
 * 分布式调度异常
 *
 * @author guoguifang
 */
public class DistributedScheduleRuntimeException extends RuntimeException {

    private static final long serialVersionUID = -1664273629732786719L;

    public DistributedScheduleRuntimeException() {
    }

    public DistributedScheduleRuntimeException(String s) {
        super(s);
    }

    public DistributedScheduleRuntimeException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public DistributedScheduleRuntimeException(Throwable throwable) {
        super(throwable);
    }

}
