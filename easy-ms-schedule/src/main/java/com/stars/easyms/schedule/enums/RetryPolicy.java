package com.stars.easyms.schedule.enums;

/**
 * 重试策略
 *
 * @author guoguifang
 */
public enum RetryPolicy {

    /**
     * 无
     */
    NONE("N", "无", "None"),

    /**
     * 递进
     */
    GRADUAL("G", "递进", "Gradual"),

    /**
     * 指数
     */
    EXPONENT("E", "指数", "Exponent");

    private String chineseName;

    private String englishName;

    private String code;

    public static RetryPolicy forCode(String code) {
        if (NONE.code.equalsIgnoreCase(code)) {
            return NONE;
        } else if (EXPONENT.code.equalsIgnoreCase(code)) {
            return EXPONENT;
        }
        return GRADUAL;
    }

    RetryPolicy(String code, String chineseName, String englishName) {
        this.chineseName = chineseName;
        this.englishName = englishName;
        this.code = code;
    }

    public String getChineseName() {
        return chineseName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public String getCode() {
        return code;
    }
}
