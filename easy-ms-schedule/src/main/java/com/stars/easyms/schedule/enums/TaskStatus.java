package com.stars.easyms.schedule.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 任务、子任务状态
 *
 * @author guoguifang
 */
public enum TaskStatus {

    /**
     * 未初始化(主任务)：任务创建完成时的状态
     */
    NOT_INIT("N", "未初始化", "Not-Init", "warning"),

    /**
     * 正在初始化(主任务)
     */
    INITING("I", "初始化中", "Initing", "warning"),

    /**
     * 初始化失败(主任务)
     */
    INIT_FAIL("IF", "初始化失败", "Init-Fail", "danger"),

    /**
     * 待命(主任务/子任务)：初始化完成(主任务与子任务分区数量一致)后未到执行时间的状态
     */
    STANDBY("S", "待命", "Standby", "secondary"),

    /**
     * 阻塞待处理(主任务/子任务)：到执行时间后没有可执行线程或调度器处于休眠时的状态
     */
    BLOCKED("B", "待处理", "Blocked", "info"),

    /**
     * 异常重试-阻塞待处理(主任务/子任务)：异常重试后的阻塞待处理状态
     */
    RETRY_BLOCKED("RB", "重试-待处理", "Retry-Blocked", "info"),

    /**
     * 处理中(主任务/子任务)：有可执行线程处理的任务状态
     */
    RUNNING("R", "处理中", "Running", "primary"),

    /**
     * 异常重试-处理中(主任务/子任务)：异常重试后的处理中状态
     */
    RETRY_RUNNING("RR", "重试-处理中", "Retry-Running", "primary"),

    /**
     * 暂停(主任务/子任务)
     */
    PAUSED("P", "暂停", "Paused", "secondary"),

    /**
     * 异常重试-暂停(主任务/子任务)：异常重试后的暂停状态
     */
    RETRY_PAUSED("RP", "重试-暂停", "Retry-Paused", "secondary"),

    /**
     * 处理异常(主任务/子任务)
     */
    ERROR("E", "处理异常", "Error", "danger"),

    /**
     * 致命错误(主任务)：主任务参数错误导致的异常，导致任务无法运行
     */
    FATAL("F", "致命错误", "Fatal", "danger"),

    /**
     * 处理完成(主任务/子任务)
     */
    COMPLETE("C", "处理完成", "Complete", "success");

    private String code;

    private String chineseName;

    private String englishName;

    private String level;

    TaskStatus(String code, String chineseName, String englishName, String level) {
        this.code = code;
        this.chineseName = chineseName;
        this.englishName = englishName;
        this.level = level;
    }

    public String getChineseName() {
        return chineseName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public String getCode() {
        return code;
    }

    public String getLevel() {
        return level;
    }

    private static Map<String, TaskStatus> codeLookup;

    public static TaskStatus forCode(String code) {
        return codeLookup.get(code);
    }

    static {
        codeLookup = new HashMap<>();
        for (TaskStatus taskStatus : values()) {
            codeLookup.put(taskStatus.code, taskStatus);
            codeLookup.put(taskStatus.code.toLowerCase(), taskStatus);
        }
    }
    
}
