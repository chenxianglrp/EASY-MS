package com.stars.easyms.schedule.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 批量管理者状态
 *
 * @author guoguifang
 */
public enum ManagerStatus {

    /**
     * 未激活
     */
    DISABLED("未激活", "disabled", 0),

    /**
     * 已激活，待初始化配置
     */
    SERVER_CONFIG_PENDING_INIT("服务器配置待初始化", "server-config-pending-init", 1),

    /**
     * 初始化配置失败
     */
    SERVER_CONFIG_INIT_FAIL("服务器配置初始化失败", "server-config-init-fail", 2),

    /**
     * 初始化配置成功
     */
    SERVER_CONFIG_INIT_SUCCESS("服务器配置初始化成功", "server-config-init-success", 3),

    /**
     * 启动心跳失败
     */
    HEARTBEAT_START_FAIL("服务器心跳启动失败", "heartbeat-start-fail", 4),

    /**
     * 启动心跳成功
     */
    HEARTBEAT_START_SUCCESS("服务器心跳启动成功", "heartbeat-start-success", 5),

    /**
     * 调度器待机
     */
    SCHEDULE_STANDBY("调度器待机", "schedule-standby", 6),

    /**
     * 调度器启动失败
     */
    SCHEDULE_START_FAIL("调度器启动失败", "schedule-start-fail", 7),

    /**
     * 调度器启动成功
     */
    SCHEDULE_START_SUCCESS("调度器启动成功", "schedule-start-success", 8),

    /**
     * 调度器暂停
     */
    SCHEDULE_PAUSE("调度器暂停", "schedule-pause", 9);

    private String chineseName;

    private String englishName;

    public final int STATUS_CODE;

    ManagerStatus(String chineseName, String englishName, int code) {
        this.chineseName = chineseName;
        this.englishName = englishName;
        this.STATUS_CODE = code;
    }

    public String getChineseName() {
        return chineseName;
    }

    public String getEnglishName() {
        return englishName;
    }

    private static Map<Integer, ManagerStatus> codeLookup;

    public static ManagerStatus forCode(int code) {
        return codeLookup.get(code);
    }

    static {
        codeLookup = new HashMap<>();
        for (ManagerStatus managerStatus : values()) {
            codeLookup.put(managerStatus.STATUS_CODE, managerStatus);
        }
    }

}
