package com.stars.easyms.schedule.client;

/**
 * 远程(mq)分布式任务接口
 *
 * @author guoguifang
 */
public interface MQDistributedTask {

    /**
     * 接收远程(mq)分布式任务执行信息并处理信息
     * @param distributedTaskExecutionContent 远程(mq)分布式任务执行上下文
     */
    void receive(DistributedTaskExecutionContent distributedTaskExecutionContent);

    /**
     * 发送远程分布式任务执行结果到mq的回调地址
     * @param mqCallback mq回调地址：若是activeMQ则为destination，若是rocketMQ则为topic
     * @param distributedTaskExecutionResult 远程分布式任务执行结果
     */
    void send(String mqCallback, DistributedTaskExecutionResult distributedTaskExecutionResult);
}
