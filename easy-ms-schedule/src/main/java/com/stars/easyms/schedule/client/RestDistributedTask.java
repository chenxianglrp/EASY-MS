package com.stars.easyms.schedule.client;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 远程(rest)分布式任务接口
 *
 * @author guoguifang
 */
@RestController
public interface RestDistributedTask {

    /**
     * 远程(rest)分布式任务执行方法
     * @param request 请求参数
     * @param distributedTaskExecutionContent 远程(rest)分布式任务执行上下文
     * @return 远程分布式任务执行结果
     */
    @RequestMapping(method = RequestMethod.POST)
    DistributedTaskExecutionResult execute(HttpServletRequest request, @RequestBody DistributedTaskExecutionContent distributedTaskExecutionContent);
}
