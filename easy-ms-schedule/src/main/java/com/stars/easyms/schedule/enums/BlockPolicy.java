package com.stars.easyms.schedule.enums;

/**
 * 任务阻塞策略
 *
 * @author guoguifang
 */
public enum BlockPolicy {

    /**
     * 常规模式
     */
    COMMON("C", "常规模式", "Common"),

    /**
     * 幂等模式
     */
    IDEMPOTENT("I", "幂等模式", "Idempotent");

    private String chineseName;

    private String englishName;

    private String code;

    public static BlockPolicy forCode(String code) {
        if (IDEMPOTENT.code.equalsIgnoreCase(code)) {
            return IDEMPOTENT;
        }
        return COMMON;
    }

    BlockPolicy(String code, String chineseName, String englishName) {
        this.chineseName = chineseName;
        this.englishName = englishName;
        this.code = code;
    }

    public String getChineseName() {
        return chineseName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public String getCode() {
        return code;
    }
}
