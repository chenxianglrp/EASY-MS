package com.stars.easyms.schedule.bean;

import com.stars.easyms.schedule.enums.TaskStatus;
import com.stars.easyms.schedule.util.DateUtil;
import lombok.Getter;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 分布式调度框架任务在页面展示信息的
 *
 * @author guoguifang
 */
@Getter
public class DbScheduleTaskView implements Serializable {

    /**
     * 物理主键
     */
    private String id = "";

    /**
     * 任务ID
     */
    private String taskId = "";

    /**
     * 任务名称
     */
    private String taskName = "";

    /**
     * 任务类型
     */
    private String taskType = "";

    /**
     * 任务开关
     */
    private String taskSwitch = "";

    /**
     * 状态
     */
    private String status = "";

    /**
     * 状态:级别
     */
    private String statusLevel = "";

    /**
     * 本次任务执行时间
     */
    private String currentFireTime = "";

    /**
     * 本次任务完成时间
     */
    private String currentFinishTime = "";

    /**
     * 下次任务执行时间
     */
    private String nextFireTime = "";

    public void setId(Long id) {
        this.id = giveNullWithDefaultValue(giveNullWithDefaultValue(id));
    }

    public void setTaskId(String taskId) {
        this.taskId = giveNullWithDefaultValue(giveNullWithDefaultValue(taskId));
    }

    public void setTaskName(String taskName) {
        this.taskName = giveNullWithDefaultValue(taskName);
    }

    public void setTaskType(String taskType) {
        this.taskType = giveNullWithDefaultValue(taskType);
    }

    public void setTaskSwitch(String taskSwitch) {
        this.taskSwitch = giveNullWithDefaultValue(taskSwitch);
    }

    public void setStatus(String status) {
        TaskStatus ts = TaskStatus.forCode(status);
        if (ts != null) {
            this.status = ts.getCode();
            this.statusLevel = ts.getLevel();
        }
    }

    public void setCurrentFireTime(Timestamp currentFireTime) {
        if (currentFireTime != null) {
            this.currentFireTime = DateUtil.getDateStr(currentFireTime);
        }
    }

    public void setCurrentFinishTime(Timestamp currentFinishTime) {
        if (currentFinishTime != null) {
            this.currentFinishTime = DateUtil.getDateStr(currentFinishTime);
        }
    }

    public void setNextFireTime(Timestamp nextFireTime) {
        if (nextFireTime != null) {
            this.nextFireTime = DateUtil.getDateStr(nextFireTime);
        }
    }

    private String giveNullWithDefaultValue(Object obj) {
        if (obj == null) {
            return "";
        }
        return obj.toString();
    }

}
