package com.stars.easyms.schedule.handler.mvchandler;

import com.stars.easyms.schedule.bean.DataMsg;
import com.stars.easyms.schedule.constant.DistributedScheduleConstants;
import com.stars.easyms.schedule.enums.LoginStatus;

import javax.servlet.http.HttpServletRequest;

/**
 * 分布式调库异常处理类
 *
 * @author guoguifang
 */
public class DbScheduleExceptionHandler extends BaseDbScheduleMvcHandler {

    /**
     * 处理登录异常信息
     */
    @DbSchedulePost(value = DistributedScheduleConstants.LOGIN_EXCEPTION_HANDLER)
    public DataMsg handlerLoginException(HttpServletRequest request) {
        LoginStatus loginStatus = (LoginStatus) request.getAttribute("loginStatus");
        if (LoginStatus.SUCCESS == loginStatus) {
            return DataMsg.getSuccessDataMsg();
        }
        DataMsg dataMsg = DataMsg.getFailDataMsg();
        dataMsg.setStatus("loginFail");
        dataMsg.setMsgCode(loginStatus.getCode());
        return dataMsg;
    }

    @Override
    public String getPath() {
        return "";
    }

}
