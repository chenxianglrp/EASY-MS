package com.stars.easyms.schedule.enums;

/**
 * 黑白名单
 *
 * @author guoguifang
 */
public enum WhiteBlackList {

    /**
     * 白名单
     */
    WHITE("白名单", "White-List", "W"),

    /**
     * 黑名单
     */
    BLACK("黑名单", "Black-List", "B");

    private String chineseName;

    private String englishName;

    private String code;

    public static WhiteBlackList forCode(String code) {
        if (WHITE.code.equalsIgnoreCase(code)) {
            return WHITE;
        } else if (BLACK.code.equalsIgnoreCase(code)) {
            return BLACK;
        }
        return null;
    }

    WhiteBlackList(String chineseName, String englishName, String code) {
        this.chineseName = chineseName;
        this.englishName = englishName;
        this.code = code;
    }

    public String getChineseName() {
        return chineseName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public String getCode() {
        return code;
    }
}
