package com.stars.easyms.schedule.exception;

/**
 * 分布式任务执行异常
 *
 * @author guoguifang
 */
public class DistributedTaskExecutionException extends Exception {

    private static final long serialVersionUID = -2764273623732786718L;

    public DistributedTaskExecutionException() {
    }

    public DistributedTaskExecutionException(String s) {
        super(s);
    }

    public DistributedTaskExecutionException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public DistributedTaskExecutionException(Throwable throwable) {
        super(throwable);
    }

}
