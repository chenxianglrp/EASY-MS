package com.stars.easyms.schedule.mq;

import com.stars.easyms.schedule.exception.DistributedScheduleMQException;

/**
 * MQ监听器接口
 *
 * @author guoguifang
 */
public interface MQListener {

    /**
     * 启动监听器
     */
    void start() throws DistributedScheduleMQException;

    /**
     * 关闭监听器
     */
    void shutdown();

    /**
     * 监听器是否启动中
     * @return 启动中：true
     */
    boolean isRunning();

}