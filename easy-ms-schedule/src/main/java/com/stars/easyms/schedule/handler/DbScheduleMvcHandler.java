package com.stars.easyms.schedule.handler;

import com.stars.easyms.schedule.enums.LoginStatus;

import javax.servlet.http.HttpServletRequest;

/**
 * 分布式框架MVC处理器
 *
 * @author guoguifang
 */
public interface DbScheduleMvcHandler {

    /**
     * mvc处理器的路径
     */
    String getPath();

    /**
     * 检查并刷新用户登录状态
     * @param request 请求对象
     * @return 获取当前用户的登录状态
     */
    LoginStatus checkAndRefreshLoginStatus(HttpServletRequest request);

}
