package com.stars.easyms.schedule.record;

import com.stars.easyms.schedule.bean.BatchResult;
import com.stars.easyms.schedule.bean.DbScheduleSubTaskRecord;
import com.stars.easyms.schedule.service.DistributedScheduleService;

import java.util.List;

/**
 * 异步插入分布式调度子任务记录
 *
 * @author guoguifang
 */
public class AsynUpdateSubTaskRecord extends BaseAsynchronousTask<DbScheduleSubTaskRecord> {

    private static final AsynUpdateSubTaskRecord ASYN_UPDATE_SUB_TASK_RECORD = new AsynUpdateSubTaskRecord();

    public static void add(DbScheduleSubTaskRecord dbScheduleSubTaskRecord) {
        ASYN_UPDATE_SUB_TASK_RECORD.offer(dbScheduleSubTaskRecord);
    }


    @Override
    protected BatchResult execute(List<DbScheduleSubTaskRecord> list) throws Exception {
        return DistributedScheduleService.getInstance().batchUpdateDbScheduleSubTaskRecord(list);
    }

    @Override
    protected long delayTime() {
        return 500;
    }

    private AsynUpdateSubTaskRecord() {}
}
