package com.stars.easyms.schedule;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * 任务注解
 *
 * @author guoguifang
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
@Component
public @interface ScheduleTask {
    String value() default "";
}
