package com.stars.easyms.schedule.util;

import java.util.Random;

/**
 * 生成ID、批次号等工具类
 *
 * @author guoguifang
 */
public class GenerateUtil {

    private static final Random SHARED_RANDOM = new Random();

    /**
     * 生成主任务的任务主键，共16位数字组成，精确到毫秒的15位数字再加一位随机数
     */
    public static Long getTaskId() {
        String currentDateStr = DateUtil.getCurrentDateStr("yyMMddHHmmssSSS");
        String randomNum = String.valueOf(SHARED_RANDOM.nextInt(10));
        return Long.parseLong(StringUtils.join(new Object[]{currentDateStr, randomNum}, ""));
    }

    /**
     * 生成主任务的任务执行批次号，由一位类型字符与20位数字组成，精确到毫秒的17位数字再加三位随机数
     */
    public static String getTaskBatchNo(String type) {
        String currentDateStr = DateUtil.getCurrentDateStr("yyyyMMddHHmmssSSS");
        String randomNum = StringUtils.leftPad(String.valueOf(SHARED_RANDOM.nextInt(1000)), 3, "0");
        return StringUtils.join(new Object[]{type, currentDateStr, randomNum}, "");
    }
}
