<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >

<mapper namespace="com.stars.easyms.schedule.repository.mysql.MysqlDistributedScheduleDDLDAO">

    <!-- 根据表名验证表是否存在 -->
    <select id="getCountByTableName" parameterType="java.lang.String" resultType="java.lang.Long">
        SELECT COUNT(*) FROM ${tableName}
    </select>

    <!-- 创建表DB_SCHEDULE_SERVER -->
    <update id="createTable_dbScheduleServer">
        CREATE TABLE DB_SCHEDULE_SERVER(
          `ip`  VARCHAR(20) NOT NULL comment '服务器IP地址',
          `port`  VARCHAR(10) NOT NULL comment '服务器端口号',
          `server_id` VARCHAR(300) NOT NULL comment '服务器标识，默认：服务器IP地址:服务器端口号',
          `schedule_switch` VARCHAR(1) NOT NULL comment '调度器开关（关闭:0，打开：1）',
          `core_pool_size`  SMALLINT NOT NULL comment '核心线程池大小',
          `max_pool_size` SMALLINT NOT NULL comment '线程池最大容量',
          `keep_alive_time` VARCHAR(30) NOT NULL comment '当线程数大于核心线程数时,空闲线程存活时间,支持单位(天:d/D,时:h/H,分:m/M,秒:s/S,毫秒:ms/MS),不写单位默认秒',
          `start_delay_time` VARCHAR(30) NOT NULL comment '第一次启动时延迟启动时间,支持单位(天:d/D,时:h/H,分:m/M,秒:s/S,毫秒:ms/MS),不写单位默认秒',
          `heartbeat_time` DATETIME(3) DEFAULT now(3) NOT NULL comment '最近一次心跳时间',
          `alive` VARCHAR(1) DEFAULT 'Y' NOT NULL comment '服务器是否存活(是：Y，否：N),若停止心跳30秒以上认为该服务器死亡',
          `wakeup` VARCHAR(1) DEFAULT 'N' NOT NULL comment '是否立即唤醒调度器(是：Y，否：N)，当手动修改了任务信息时需要设置为Y，否则手动修改任务信息不会立即生效',
          `version` BIGINT DEFAULT 1 NOT NULL comment '乐观锁',
          primary key(`ip`, `port`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 comment='分布式调度服务器信息表'
    </update>

    <!-- 创建表DB_SCHEDULE_TASK -->
    <update id="createTable_dbScheduleTask">
        CREATE TABLE DB_SCHEDULE_TASK(
          `id` BIGINT NOT NULL comment '物理主键',
          `task_id` VARCHAR(300) NOT NULL comment '任务ID',
          `task_group` VARCHAR(300) comment '任务组,若为空则表示该任务为单体任务,分组后一个任务组为一个整体按照task_step顺序执行',
          `task_step` SMALLINT comment '任务组步骤,任务组不为空时有效,同一个任务组可有相同的step表示可以同时执行',
          `task_name` VARCHAR(300) comment '任务名称',
          `task_switch` VARCHAR(1) DEFAULT 'O' NOT NULL comment '任务开关(打开:O,关闭:C)',
          `batch_no` VARCHAR(50) comment '批次号(一个主任务从开始到结束为一个批次,用于记录日志信息,N开头表示一般批次,R开头表示失败重试批次)',
          `partition_count` SMALLINT DEFAULT 10 NOT NULL comment '分区总数量(最大1000)',
          `partition_mode` VARCHAR(1) DEFAULT 'S' NOT NULL comment '分区模式(静态:S,动态智能:D)',
          `task_type` VARCHAR(2) DEFAULT 'N' NOT NULL comment '任务类型(本地:N,远程:R,MQ:M)',
          `bean_id` VARCHAR(50) comment '任务类型为本地时有效,spring已注册的beanId,与bean_class设置其一即可,若都设置优先级高于bean_class',
          `bean_class` VARCHAR(300) comment '任务类型为本地时有效,任务执行类的完整类名,优先级低于bean_id',
          `remote_url` VARCHAR(300) comment '任务类型为远程时有效,远程调用的url地址',
          `mq_config_id` VARCHAR(30) comment '任务类型为MQ时有效,与表db_schedule_mq_config关联',
          `mq_destination` VARCHAR(300) comment '任务类型为MQ时有效,MQ的发送目标地址',
          `mq_callback` VARCHAR(300) comment '任务类型为MQ时有效,MQ的回调通知地址',
          `parameters` TEXT comment '任务执行参数,使用(key1=value1,key2=value2...)的格式',
          `priority` TINYINT NOT NULL comment '优先级(1-7),数值越大优先级越高',
          `status` VARCHAR(2) NOT NULL comment '状态(未初始化:N,正在初始化:I,初始化失败:IF,待命:S,阻塞待处理:B,异常重试-阻塞待处理:RB,处理中:R,异常重试-处理中:RR,暂停:P,异常重试-暂停:RP,处理异常:E,致命错误:F,处理完成:C)',
          `cron_expression` VARCHAR(300) comment '周期性任务的cron表达式',
          `current_fire_time` DATETIME(3) comment '本次任务执行时间',
          `current_finish_time` DATETIME(3) comment '本次任务完成时间',
          `next_fire_time` DATETIME(3) comment '下次任务执行时间',
          `error_message` VARCHAR(1000) comment '错误信息',
          `dependent_task_id` VARCHAR(500) comment '依赖的任务ID',
          `dependence_policy` VARCHAR(1) DEFAULT 'S' NOT NULL comment '依赖策略(强依赖:S,弱依赖:W)',
          `block_policy` VARCHAR(1) DEFAULT 'C' NOT NULL comment '任务阻塞策略(常规模式:C、幂等模式:I)',
          `error_policy` VARCHAR(1) DEFAULT 'R' NOT NULL comment '异常策略(重试:R、人工:M、忽略:I)',
          `retry_policy` VARCHAR(1) comment '重试策略(无:N,递进:G,指数:E)',
          `retry_count` SMALLINT comment '重试次数',
          `retry_index` SMALLINT comment '当前已重试次数',
          `retry_interval` VARCHAR(30) comment '重试间隔,支持单位(天:d/D,时:h/H,分:m/M,秒:s/S,毫秒:ms/MS),不写单位默认秒',
          `use_white_black_list` VARCHAR(1) DEFAULT 'N' NOT NULL comment '是否使用白、黑名单(是:Y，否:N)',
          `alarm` VARCHAR(1) DEFAULT 'Y' NOT NULL comment '是否错误报警(是:Y，否:N)',
          `version` BIGINT DEFAULT 1 NOT NULL comment '乐观锁',
          primary key (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 comment='分布式调度任务信息表'
    </update>

    <update id="alterTable_dbScheduleTask_uix_0">
        create unique index UIX_DB_SE_TK_TK_ID on DB_SCHEDULE_TASK(`task_id`)
    </update>
    <update id="alterTable_dbScheduleTask_idx_0">
        create index IDX_DB_SE_TASK_GROUP on DB_SCHEDULE_TASK(`task_group`)
    </update>
    <update id="alterTable_dbScheduleTask_idx_1">
        create index IDX_DB_SE_TASK_PRIORITY on DB_SCHEDULE_TASK(`priority`)
    </update>
    <update id="alterTable_dbScheduleTask_idx_2">
        create index IDX_DB_SE_TASK_MQ_CONFIG on DB_SCHEDULE_TASK(`mq_config_id`)
    </update>

    <!-- 创建表DB_SCHEDULE_SUB_TASK -->
    <update id="createTable_dbScheduleSubTask">
        CREATE TABLE DB_SCHEDULE_SUB_TASK(
          `id`  BIGINT NOT NULL comment '物理主键',
          `parent_id`  BIGINT NOT NULL comment '主任务ID',
          `partition_count` SMALLINT NOT NULL comment '分区总数量',
          `partition_index` SMALLINT NOT NULL comment '当前分区索引',
          `status`  VARCHAR(2) NOT NULL comment '状态',
          `error_message` VARCHAR(1000) comment '错误信息',
          `execute_server_ip` VARCHAR(20) comment '执行该子任务的服务器IP',
          `execute_server_port` VARCHAR(10) comment '执行该子任务的服务器端口',
          `version` BIGINT DEFAULT 1 NOT NULL comment '乐观锁',
          primary key (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 comment='分布式调度子任务信息表'
    </update>

    <update id="alterTable_dbScheduleSubTask_uix_0">
        create index IDX_DB_SE_SUB_PARENT_ID on DB_SCHEDULE_SUB_TASK(`parent_id`)
    </update>

    <!-- 创建表DB_SCHEDULE_WHITE_BLACK_LIST -->
    <update id="createTable_dbScheduleWhiteBlackList">
        CREATE TABLE DB_SCHEDULE_WHITE_BLACK_LIST(
          `task_id`  BIGINT NOT NULL comment '主任务主键',
          `server_ip` VARCHAR(20) NOT NULL comment '服务器IP',
          `server_port` VARCHAR(10) NOT NULL comment '服务器端口号',
          `direction`  VARCHAR(1) NOT NULL comment '所属方向(白名单:W,黑名单:B)',
          primary key (`task_id`, `server_ip`, `server_port`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 comment='分布式调度任务白、黑名单'
    </update>

    <!-- 创建表DB_SCHEDULE_TASK_RECORD -->
    <update id="createTable_dbScheduleTaskRecord">
        CREATE TABLE DB_SCHEDULE_TASK_RECORD(
          `task_id` BIGINT NOT NULL comment '主任务主键',
          `batch_no` VARCHAR(50) NOT NULL comment '主任务执行批次号',
          `prev_batch_no` VARCHAR(50) comment '主任务执行前一批次号',
          `fire_time` DATETIME(3) comment '主任务执行开始时间',
          `finish_time` DATETIME(3) comment '主任务执行结束时间',
          `used_time` BIGINT comment '主任务执行用时(毫秒)',
          `status` VARCHAR(2) comment '主任务执行状态(阻塞待处理:B,处理异常:E,致命错误:F,处理完成:C)',
          primary key (`task_id`, `batch_no`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 comment='分布式调度主任务执行记录表'
    </update>

    <!-- 创建表DB_SCHEDULE_SUB_TASK_RECORD -->
    <update id="createTable_dbScheduleSubTaskRecord">
        CREATE TABLE DB_SCHEDULE_SUB_TASK_RECORD(
          `sub_task_id` BIGINT NOT NULL comment '子任务主键',
          `batch_no` VARCHAR(50) NOT NULL comment '主任务执行批次号',
          `parent_id` BIGINT NOT NULL comment '主任务主键',
          `partition_count` SMALLINT NOT NULL comment '分区总数量',
          `partition_index` SMALLINT NOT NULL comment '分区索引',
          `fire_time` DATETIME(3) comment '子任务执行开始时间',
          `finish_time` DATETIME(3) comment '子任务执行结束时间',
          `used_time` BIGINT comment '子任务执行用时(毫秒)',
          `status` VARCHAR(2) comment '子任务执行状态(处理异常:E,致命错误:F,处理完成:C)',
          `error_message` TEXT comment '子任务执行错误信息',
          `execute_server_ip` VARCHAR(20) comment '子任务执行服务器IP',
          `execute_server_port` VARCHAR(10) comment '子任务执行服务器端口',
          `execute_thread_id` SMALLINT comment '子任务执行服务器线程编号',
          primary key (`sub_task_id`, `batch_no`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 comment='分布式调度主任务执行记录表'
    </update>

    <update id="alterTable_dbScheduleSubTaskRecord_idx_0">
        create index IDX_DB_SD_SUB_TK_RD_TK_ID on DB_SCHEDULE_SUB_TASK_RECORD(`parent_id`)
    </update>
    <update id="alterTable_dbScheduleSubTaskRecord_idx_1">
        create index IDX_DB_SD_SUB_TK_RD_SERVER on DB_SCHEDULE_SUB_TASK_RECORD(`execute_server_ip`, `execute_server_port`, `execute_thread_id`);
    </update>

    <!-- 创建表DB_SCHEDULE_MQ_CONFIG -->
    <update id="createTable_dbScheduleMqConfig">
        CREATE TABLE DB_SCHEDULE_MQ_CONFIG(
          `mq_config_id` VARCHAR(30) NOT NULL comment '物理主键',
          `mq_type` VARCHAR(2) NOT NULL comment 'mq类型(ActiveMQ:A,RocketMQ:R)',
          `mq_connection` VARCHAR(1000) NOT NULL comment '如果是rocketmq则代表nameServer，如果是activemq则代表brokerUrl',
          `mq_user` VARCHAR(300) comment '连接mq的用户名',
          `mq_password` VARCHAR(300) comment '连接mq的密码',
          `mq_producer_group_name` VARCHAR(300) comment 'rocketmq的生产者groupName,若为空默认"DISTRIBUTED_PRODUCER"',
          `mq_consumer_group_name` VARCHAR(300) comment 'rocketmq的消费者groupName,若为空默认"DISTRIBUTED_CONSUMER"',
          primary key (`mq_config_id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 comment='分布式调度MQ配置表'
    </update>

    <!-- 创建表DB_SCHEDULE_USER_INFO -->
    <update id="createTable_dbScheduleUserInfo">
        CREATE TABLE DB_SCHEDULE_USER_INFO(
          `user_name` VARCHAR(100) NOT NULL comment '用户名',
          `user_password` VARCHAR(300) comment '用户密码',
          `login_time` DATETIME(2) comment '登录时间',
          `login_token` VARCHAR(100) comment '登录标识',
          `page_layout` VARCHAR(2) NOT NULL comment '页面布局(左:L,上:T)',
          `page_language` VARCHAR(2) NOT NULL comment '页面语言(简体中文:zh,英文:en)',
          `page_lightup` VARCHAR(1) NOT NULL comment '是否启动开灯模式(是:Y,否:N)',
          `page_theme` VARCHAR(10) NOT NULL comment '页面主题颜色',
          `dashboard_content` VARCHAR(1000) comment 'dashboard页面的内容ID及顺序，模块之间用","分割',
          `version` BIGINT DEFAULT 1 NOT NULL comment '乐观锁',
          primary key (`user_name`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 comment='分布式调度用户信息表'
    </update>

    <!-- 创建表DB_SCHEDULE_CONFIG -->
    <update id="createTable_dbScheduleConfig">
        CREATE TABLE DB_SCHEDULE_CONFIG(
          `version` VARCHAR(50) NOT NULL comment '版本号',
          `version_iteration` INTEGER NOT NULL comment '版本编号',
          `open_login` VARCHAR(1) DEFAULT 'Y' NOT NULL comment '是否开启登录模式(是:Y,否:N)',
          `native_login` VARCHAR(1) NOT NULL comment '是否使用本地登录(是:Y,否:N),如果是则使用本表中密码登录,如果否则使用配置中的远程登录',
          `remote_login_url` VARCHAR(300) comment '远程登录地址',
          `use_sso` VARCHAR(1) DEFAULT 'Y' NOT NULL comment '若开启登录模式，是否使用单点登录模式(是:Y,否:N)',
          `session_timeout` VARCHAR(30) DEFAULT '30m' NOT NULL comment '若开启登录模式，登录的会话超时时间，支持单位(天:d/D,时:h/H,分:m/M,秒:s/S,毫秒:ms/MS),不写单位默认秒',
          `route_policy` VARCHAR(2) DEFAULT 'S' NOT NULL comment '路由策略(争抢:S,轮询:P,权重:W)'
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 comment='分布式调度总配置表'
    </update>
</mapper>
