package com.stars.easyms.schedule.factory;

import com.stars.easyms.schedule.mq.MQListener;
import com.stars.easyms.schedule.mq.activemq.ActiveMQListener;
import com.stars.easyms.schedule.mq.rocketmq.RocketMQListener;
import com.stars.easyms.schedule.util.ReflectUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * MQ监听器工厂类
 *
 * @author guoguifang
 */
public class MQListenerFactory implements MQFactory {

    private static final Logger logger = LoggerFactory.getLogger(MQListenerFactory.class);

    /**
     * 获取activeMq的监听器
     */
    public static MQListener getActiveMQListener(String brokerUrl, String user, String password, String destination) {
        if (ReflectUtil.forNameWithNoException(JMS_CONN_FACTORY) == null) {
            logger.error("Get active MQ listener '{}' failure: Could not find class {}!", destination, JMS_CONN_FACTORY);
            return null;
        }
        if (ReflectUtil.forNameWithNoException(ACTIVEMQ_CONN_FACTORY) == null) {
            logger.error("Get active MQ listener '{}' failure: Could not find class {}!", destination, ACTIVEMQ_CONN_FACTORY);
            return null;
        }
        return new ActiveMQListener(brokerUrl, user, password, destination);
    }

    /**
     * 获取rocketMq的监听器，优先使用高版本rocketmq
     */
    public static MQListener getRocketMQListener(String rocketMQNameServer, String rocketMQConsumerGroupName, String topic) {
        if (ReflectUtil.forNameWithNoException(ROCKETMQ_CONSUMER) != null) {
            return new RocketMQListener(rocketMQNameServer, rocketMQConsumerGroupName, topic);
        }
        logger.error("Get rocket MQ listener '{}' failure: Could not find class {}!", topic, ROCKETMQ_CONSUMER);
        return null;
    }

}
