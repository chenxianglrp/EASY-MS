package com.stars.easyms.schedule.bean;

import com.stars.easyms.schedule.enums.TaskStatus;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 分布式调度框架子任务
 *
 * @author guoguifang
 */
@Data
public class DbScheduleSubTask implements Serializable {

    /**
     * 物理主键
     */
    private Long id;

    /**
     * 主任务ID
     */
    private Long parentId;

    /**
     * 分区总数量
     */
    private Integer partitionCount;

    /**
     * 当前分区索引
     */
    private Integer partitionIndex;

    /**
     * 状态
     */
    private String status;

    /**
     * 状态:级别
     */
    private String statusLevel = "";

    /**
     * 子任务错误信息
     */
    private String errorMessage;

    /**
     * 执行该子任务的服务器IP
     */
    private String executeServerIp;

    /**
     * 执行该子任务的服务器端口
     */
    private String executeServerPort;

    /**
     * 乐观锁
     */
    private Long version;

    /**
     * 主任务任务ID
     */
    private String taskId;

    /**
     * 主任务名称
     */
    private String taskName;

    /**
     * 批次号(一个主任务从开始到结束为一个批次,用于记录日志信息,N开头表示一般批次,R开头表示失败重试批次)
     */
    private String batchNo;

    /**
     * 任务类型(本地,远程,ActiveMQ,RocketMQ)
     */
    private String taskType;

    /**
     * spring已注册的beanId
     */
    private String beanId;

    /**
     * 任务执行类的完整类名
     */
    private String beanClass;

    /**
     * 任务类型为远程时有效,远程调用的url地址
     */
    private String remoteUrl;

    /**
     * mq类型
     */
    private String mqType;

    /**
     * 任务类型为MQ时有效,如果是rocketmq则代表nameServer，如果是activemq则代表brokerUrl
     */
    private String mqConnection;

    /**
     * 任务类型为MQ时有效,连接mq的用户名
     */
    private String mqUser;

    /**
     * 任务类型为MQ时有效,连接mq的密码
     */
    private String mqPassword;

    /**
     * 任务类型为MQ并且使用rocketmq时有效,rocketmq的发送目标groupName
     */
    private String mqProducerGroupName;

    /**
     * 任务类型为MQ时有效,包含activemq和rocketmq,MQ的发送目标地址
     */
    private String mqDestination;

    /**
     * 任务类型为MQ并且使用rocketmq时有效,rocketmq的回调地址groupName
     */
    private String mqConsumerGroupName;

    /**
     * 任务类型为MQ时有效,包含activemq和rocketmq,MQ的回调通知地址
     */
    private String mqCallback;

    /**
     * 任务执行参数,使用(key1=value1,key2=value2...)的格式
     */
    private String parameters;

    /**
     * 主任务状态
     */
    private String taskStatus;

    /**
     * 主任务下次执行时间
     */
    private Timestamp nextFireTime;

    /**
     * 周期性任务的cron表达式
     */
    private String cronExpression;

    /**
     * 主任务乐观锁
     */
    private Long taskVersion;

    /**
     * 创建修改主任务的数据传输对象：修改主任务需要主任务ID与主任务乐观锁
     */
    public DbScheduleTask createUpdateTask() {
        DbScheduleTask updateTask = new DbScheduleTask();
        updateTask.setId(this.parentId);
        updateTask.setVersion(this.taskVersion);
        return updateTask;
    }

    /**
     * 创建修改子任务的数据传输对象：修改子任务需要子任务ID与子任务乐观锁
     */
    public DbScheduleSubTask createUpdateSubTask() {
        DbScheduleSubTask updateSubTask = new DbScheduleSubTask();
        updateSubTask.setId(this.id);
        updateSubTask.setVersion(this.version);
        return updateSubTask;
    }

    public void setStatus(String status) {
        TaskStatus ts = TaskStatus.forCode(status);
        if (ts != null) {
            this.status = ts.getCode();
            this.statusLevel = ts.getLevel();
        }
    }

}
