package com.stars.easyms.schedule;

import com.stars.easyms.schedule.exception.DistributedTaskExecutionException;

/**
 * 分布式任务实现接口
 *
 * @author guoguifang
 */
public interface DistributedTask {

    /**
     * 分布式任务执行方法
     * @param distributedTaskContext 分布式任务上下文
     * @throws DistributedTaskExecutionException 分布式任务执行异常
     */
    void execute(DistributedTaskContext distributedTaskContext) throws DistributedTaskExecutionException;

}
