package com.stars.easyms.schedule.handler;

import com.stars.easyms.schedule.util.ApplicationContextHolder;
import com.stars.easyms.schedule.handler.mvchandler.*;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * 分布式调度框架HandlerMapping注册类
 *
 * @author guoguifang
 */
public class DbScheduleHandlerMappingRegister {

    private static AtomicBoolean registered = new AtomicBoolean(false);

    private static DefaultListableBeanFactory defaultListableBeanFactory;

    public static void register() {
        if (registered.compareAndSet(false, true)) {
            defaultListableBeanFactory = (DefaultListableBeanFactory) ApplicationContextHolder.getApplicationContext().getAutowireCapableBeanFactory();
            registerDbScheduleMvcHandler();
        }
    }

    private static void registerDbScheduleMvcHandler() {
        Set<DbScheduleMvcHandler> dbScheduleMvcHandlers = new HashSet<>();
        dbScheduleMvcHandlers.add(new DbSchedulePermissionMvcHandler());
        dbScheduleMvcHandlers.add(new DbScheduleForwardMvcHandler());
        dbScheduleMvcHandlers.add(new DbScheduleTaskMvcHandler());
        dbScheduleMvcHandlers.add(new DbScheduleMQMvcHandler());
        dbScheduleMvcHandlers.add(new DbScheduleServerMvcHandler());
        dbScheduleMvcHandlers.add(new DbScheduleSettingMvcHandler());
        dbScheduleMvcHandlers.add(new DbScheduleExceptionHandler());
        DbScheduleHandlerMapping dbScheduleInnerHandlerMapping = new DbScheduleHandlerMapping("", dbScheduleMvcHandlers, ApplicationContextHolder.getApplicationContext());
        defaultListableBeanFactory.registerSingleton("dbScheduleInnerHandlerMapping", dbScheduleInnerHandlerMapping);
    }

}
