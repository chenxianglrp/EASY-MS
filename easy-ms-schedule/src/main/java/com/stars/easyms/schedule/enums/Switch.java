package com.stars.easyms.schedule.enums;

/**
 * 开关
 *
 * @author guoguifang
 */
public enum Switch {

    /**
     * 打开
     */
    OPEN("打开", "open", "O"),

    /**
     * 关闭
     */
    CLOSE("关闭", "close", "C");

    private String chineseName;

    private String englishName;

    private String code;

    public static Switch forCode(String code) {
        if (OPEN.code.equalsIgnoreCase(code)) {
            return OPEN;
        } else if (CLOSE.code.equalsIgnoreCase(code)) {
            return CLOSE;
        }
        return null;
    }

    Switch(String chineseName, String englishName, String code) {
        this.chineseName = chineseName;
        this.englishName = englishName;
        this.code = code;
    }

    public String getChineseName() {
        return chineseName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public String getCode() {
        return code;
    }
}
