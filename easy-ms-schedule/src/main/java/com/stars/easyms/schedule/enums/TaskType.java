package com.stars.easyms.schedule.enums;

/**
 * 任务类型
 *
 * @author guoguifang
 */
public enum TaskType {

    /**
     * 本地
     */
    NATIVE("N", "本地", "Native"),

    /**
     * 远程
     */
    REMOTE("R", "远程", "Remote"),

    /**
     * MQ
     */
    MQ("M", "MQ", "MQ");

    private String chineseName;

    private String englishName;

    private String code;

    public static TaskType forCode(String code) {
        if (MQ.code.equalsIgnoreCase(code)) {
            return MQ;
        } else if (REMOTE.code.equalsIgnoreCase(code)) {
            return REMOTE;
        }
        return NATIVE;
    }

    TaskType(String code, String chineseName, String englishName) {
        this.chineseName = chineseName;
        this.englishName = englishName;
        this.code = code;
    }

    public String getChineseName() {
        return chineseName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public String getCode() {
        return code;
    }
}
