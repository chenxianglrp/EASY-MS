package com.stars.easyms.schedule.mq.activemq;

import com.stars.easyms.schedule.util.StringUtils;
import org.apache.activemq.ActiveMQConnectionFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 基础ActiveMQ连接工厂类
 *
 * @author guoguifang
 */
public class BasicActiveMQConnectionFactory {

    private static Map<String, ActiveMQConnectionFactory> activeMQConnectionFactoryMap;

    static ActiveMQConnectionFactory getActiveMQConnectionFactory(String brokerUrl, String user, String password) {
        if (activeMQConnectionFactoryMap == null) {
            synchronized (BasicActiveMQConnectionFactory.class) {
                if (activeMQConnectionFactoryMap == null) {
                    activeMQConnectionFactoryMap = new ConcurrentHashMap<>(64);
                }
            }
        }
        String key = StringUtils.join(new Object[]{brokerUrl, user, password}, "-");
        ActiveMQConnectionFactory activeMQConnectionFactory = activeMQConnectionFactoryMap.get(key);
        if (activeMQConnectionFactory == null) {
            activeMQConnectionFactory = new ActiveMQConnectionFactory(user, password, brokerUrl);
            activeMQConnectionFactoryMap.put(key, activeMQConnectionFactory);
        }
        return activeMQConnectionFactory;
    }
}
