package com.stars.easyms.schedule.bean;

import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 分布式调度框架服务器子任务记录
 *
 * @author guoguifang
 */
@Data
public class DbScheduleSubTaskRecord implements Serializable {

    /**
     * 子任务主键
     */
    private Long subTaskId;

    /**
     * 主任务执行批次号
     */
    private String batchNo;

    /**
     * 主任务主键
     */
    private Long parentId;

    /**
     * 分区总数量
     */
    private Integer partitionCount;

    /**
     * 分区索引
     */
    private Integer partitionIndex;

    /**
     * 子任务执行开始时间
     */
    private Timestamp fireTime;

    /**
     * 子任务执行结束时间
     */
    private Timestamp finishTime;

    /**
     * 子任务执行用时(毫秒)
     */
    private Long usedTime;

    /**
     * 子任务执行状态(处理异常:E,致命错误:F,处理完成:C)
     */
    private String status;

    /**
     * 子任务执行异常
     */
    private Throwable throwable;

    /**
     * 子任务执行错误信息
     */
    private String errorMessage;

    /**
     * 子任务执行服务器IP
     */
    private String executeServerIp;

    /**
     * 子任务执行服务器端口
     */
    private String executeServerPort;

    /**
     * 子任务执行服务器线程编号
     */
    private Integer executeThreadId;

}
