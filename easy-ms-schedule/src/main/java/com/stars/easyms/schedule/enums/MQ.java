package com.stars.easyms.schedule.enums;

/**
 * MQ
 *
 * @author guoguifang
 */
public enum MQ {

    /**
     * ActiveMQ
     */
    ACTIVEMQ("A", "ActiveMQ", "ActiveMQ"),

    /**
     * RocketMQ
     */
    ROCKETMQ("R", "RocketMQ", "RocketMQ");

    private String chineseName;

    private String englishName;

    private String code;

    public static MQ forCode(String code) {
        if (ACTIVEMQ.code.equalsIgnoreCase(code)) {
            return ACTIVEMQ;
        }
        return ROCKETMQ;
    }

    MQ(String code, String chineseName, String englishName) {
        this.chineseName = chineseName;
        this.englishName = englishName;
        this.code = code;
    }

    public String getChineseName() {
        return chineseName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public String getCode() {
        return code;
    }
}
