package com.stars.easyms.schedule.enums;

/**
 * 依赖策略
 *
 * @author guoguifang
 */
public enum DependencePolicy {

    /**
     * 强依赖
     */
    STRONG("S", "强依赖", "Strong"),

    /**
     * 弱依赖
     */
    WEAK("W", "弱依赖", "Weak");

    private String chineseName;

    private String englishName;

    private String code;

    public static DependencePolicy forCode(String code) {
        if (WEAK.code.equalsIgnoreCase(code)) {
            return WEAK;
        }
        return STRONG;
    }

    DependencePolicy(String code, String chineseName, String englishName) {
        this.chineseName = chineseName;
        this.englishName = englishName;
        this.code = code;
    }

    public String getChineseName() {
        return chineseName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public String getCode() {
        return code;
    }

}
