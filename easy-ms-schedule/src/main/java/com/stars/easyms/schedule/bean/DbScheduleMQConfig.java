package com.stars.easyms.schedule.bean;

import lombok.Data;

/**
 * 分布式调度MQ配置表信息
 *
 * @author guoguifang
 */
@Data
public class DbScheduleMQConfig {

    /**
     * MQ配置表主键
     */
    private String mqConfigId;

    /**
     * mq类型
     */
    private String mqType;

    /**
     * 如果是rocketmq则代表nameServer，如果是activemq则代表brokerUrl
     */
    private String mqConnection;

    /**
     * 连接mq的用户名
     */
    private String mqUser;

    /**
     * 连接mq的密码
     */
    private String mqPassword;

    /**
     * rocketmq的生产者groupName,若为空默认"DISTRIBUTED_PRODUCER"
     */
    private String mqProducerGroupName;

    /**
     * rocketmq的消费者groupName,若为空默认"DISTRIBUTED_CONSUMER"
     */
    private String mqConsumerGroupName;
}
