package com.stars.easyms.schedule.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 分布式调度框架任务优先级
 *
 * @author guoguifang
 */
public enum TaskPriority {

    /**
     * 优先级:非常高
     */
    HIGHEST("非常高", "Highest", 7),

    /**
     * 优先级:很高
     */
    HIGHER("很高", "Higher", 6),

    /**
     * 优先级:高
     */
    HIGH("高", "High", 5),

    /**
     * 优先级:一般
     */
    NORMAL("一般", "Normal", 4),

    /**
     * 优先级:低
     */
    LOW("低", "Low", 3),

    /**
     * 优先级:很低
     */
    LOWER("很低", "Lower", 2),

    /**
     * 优先级:非常低
     */
    LOWEST("非常低", "Lowest", 1);

    private String chineseName;

    private String englishName;

    public int level;

    TaskPriority(String chineseName, String englishName, int level) {
        this.chineseName = chineseName;
        this.englishName = englishName;
        this.level = level;
    }

    public String getChineseName() {
        return chineseName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public int getLevel() {
        return level;
    }

    private static Map<Integer, TaskPriority> levelLookup;

    public static TaskPriority forLevel(int level) {
        if (level <= LOWEST.level) {
            return LOWEST;
        } else if (level >= HIGHEST.level) {
            return HIGHEST;
        }
        return levelLookup.get(level);
    }

    static {
        levelLookup = new HashMap<>();
        for (TaskPriority taskPriority : values()) {
            levelLookup.put(taskPriority.level, taskPriority);
        }
    }

}