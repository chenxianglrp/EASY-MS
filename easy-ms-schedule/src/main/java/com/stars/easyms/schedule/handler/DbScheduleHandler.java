package com.stars.easyms.schedule.handler;

/**
 * 分布式框架处理器
 *
 * @author guoguifang
 */
public interface DbScheduleHandler<T> {

    String getId();

    T invoke();
}
