package com.stars.easyms.schedule.handler;

/**
 * 分布式框架MVC处理器
 *
 * @author guoguifang
 */
public interface DbScheduleMvcAdapter<E extends DbScheduleHandler<?>> extends DbScheduleMvcHandler {

    String getName();

    E getDbScheduleHandler();

    Class<E> getDbScheduleHandlerType();
}
