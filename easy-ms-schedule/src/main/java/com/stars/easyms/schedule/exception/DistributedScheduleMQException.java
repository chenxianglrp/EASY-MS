package com.stars.easyms.schedule.exception;

/**
 * 分布式调度异常
 *
 * @author guoguifang
 */
public class DistributedScheduleMQException extends Exception {

    private static final long serialVersionUID = 2664273629732786719L;

    public DistributedScheduleMQException() {
    }

    public DistributedScheduleMQException(String s) {
        super(s);
    }

    public DistributedScheduleMQException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public DistributedScheduleMQException(Throwable throwable) {
        super(throwable);
    }

}
