package com.stars.easyms.schedule.bean;

import com.stars.easyms.schedule.core.DistributedTaskExecutor;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 分布式调度框架服务器信息
 *
 * @author guoguifang
 */
@Data
public class DbScheduleServerInfo implements Serializable {

    private List<DistributedTaskExecutor> distributedTaskExecutorList;

}
