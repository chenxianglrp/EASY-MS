package com.stars.easyms.schedule.mq;

import com.stars.easyms.schedule.exception.DistributedScheduleMQException;

/**
 * MQ消息发送者接口
 *
 * @author guoguifang
 */
public interface MQMessageSender {

    /**
     * MQ消息转换并发送
     * @param destination 发送目标地址
     * @param message 消息
     * @throws DistributedScheduleMQException MQ异常
     */
    boolean convertAndSend(String destination, Object message) throws DistributedScheduleMQException;

}
