package com.stars.easyms.schedule.enums;

/**
 * 异常处理策略
 *
 * @author guoguifang
 */
public enum ErrorPolicy {

    /**
     * 忽略
     */
    IGNORE("I", "忽略", "Ignore"),

    /**
     * 重试
     */
    RETRY("R", "重试", "Retry"),

    /**
     * 人工
     */
    MANUAL("M", "人工", "Manual");

    private String chineseName;

    private String englishName;

    private String code;

    public static ErrorPolicy forCode(String code) {
        if (MANUAL.code.equalsIgnoreCase(code)) {
            return MANUAL;
        } else if (IGNORE.code.equalsIgnoreCase(code)) {
            return IGNORE;
        }
        return RETRY;
    }

    ErrorPolicy(String code, String chineseName, String englishName) {
        this.chineseName = chineseName;
        this.englishName = englishName;
        this.code = code;
    }

    public String getChineseName() {
        return chineseName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public String getCode() {
        return code;
    }
}
