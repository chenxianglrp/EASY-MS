package com.stars.easyms.schedule.enums;

/**
 * 用户登录状态
 *
 * @author guoguifang
 */
public enum LoginStatus {

    /**
     * 登录成功
     */
    SUCCESS("success"),

    /**
     * 登录失败
     */
    FAIL("fail"),

    /**
     * 单点登录生效引起的登录失败
     */
    SSO_EFFECT("ssoEffect"),

    /**
     * 会话超时
     */
    SESSION_TIMEOUT("sessionTimeout"),

    /**
     * 会话失效
     */
    SESSION_INVALID("sessionInvalid");

    private String code;

    LoginStatus(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
