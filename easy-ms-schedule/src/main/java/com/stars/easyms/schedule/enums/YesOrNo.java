package com.stars.easyms.schedule.enums;

/**
 * 是否
 *
 * @author guoguifang
 */
public enum YesOrNo {

    /**
     * 是
     */
    YES("是", "Yes", "Y"),

    /**
     * 否
     */
    NO("否", "No", "N");

    private String chineseName;

    private String englishName;

    private String code;

    public static YesOrNo forCode(String code) {
        if (YES.code.equalsIgnoreCase(code)) {
            return YES;
        } else if (NO.code.equalsIgnoreCase(code)) {
            return NO;
        }
        return null;
    }

    YesOrNo(String chineseName, String englishName, String code) {
        this.chineseName = chineseName;
        this.englishName = englishName;
        this.code = code;
    }

    public String getChineseName() {
        return chineseName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public String getCode() {
        return code;
    }
}
