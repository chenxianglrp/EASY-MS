package com.stars.easyms.schedule.bean;

import lombok.Data;

import java.io.Serializable;

/**
 * 分布式调度框架黑白名单信息
 *
 * @author guoguifang
 */
@Data
public class DbScheduleWhiteBlackList implements Serializable {

    /**
     * 主任务主键
     */
    private Long taskId;

    /**
     * 服务器IP
     */
    private String serverIp;

    /**
     * 服务器端口号
     */
    private String serverPort;

    /**
     * 所属方向(白名单,黑名单)
     */
    private String direction;
}
