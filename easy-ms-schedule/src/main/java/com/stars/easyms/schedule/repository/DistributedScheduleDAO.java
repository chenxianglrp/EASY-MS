package com.stars.easyms.schedule.repository;

import com.stars.easyms.schedule.bean.*;

import java.util.List;
import java.util.Map;

/**
 * 分布式调度框架基础DAO接口
 *
 * @author guoguifang
 */
public interface DistributedScheduleDAO {

    /**
     * 得到分布式调度框架的配置参数
     * @param dbScheduleServer 查询条件
     * @return 配置信息
     */
    DbScheduleServer getDbScheduleServerConfig(DbScheduleServer dbScheduleServer);

    /**
     * 得到分布式调度框架的所有服务器配置参数
     * @return 所有服务器配置信息
     */
    List<DbScheduleServer> getAllDbScheduleServerConfig();

    /**
     * 插入分布式调度框架的配置参数
     * @param dbScheduleServer 配置信息
     * @return 插入成功数量
     */
    int insertDbScheduleServer(DbScheduleServer dbScheduleServer);

    /**
     * 分布式调度心跳
     * @param dbScheduleServer 配置信息
     * @return 修改成功数量
     */
    int heartbeat(DbScheduleServer dbScheduleServer);

    /**
     * 修改服务器配置信息
     * @param dbScheduleServer 配置信息
     * @return 修改成功数量
     */
    int updateDbScheduleServer(DbScheduleServer dbScheduleServer);

    /**
     * 唤醒现在存活的并且调度器为打开的服务器
     * @param dbScheduleServer 修改参数
     * @return 返回修改数量
     */
    int signalAliveServerSchedule(DbScheduleServer dbScheduleServer);

    /**
     * 更新已死亡服务器状态
     * @param paramMap 更新参数
     * @return 修改成功数量
     */
    int updateDeadSeverStatus(Map<String, Object> paramMap);

    /**
     * 得到所有的任务ID
     * @return 查询结果
     */
    List<Map<String, String>> getAllTaskId();

    /**
     * 得到所有的MQConfigId
     * @return 查询结果
     */
    List<Map<String, String>> getAllMqConfigId();

    /**
     * 得到所有的服务器ID
     * @return 查询结果
     */
    List<Map<String, String>> getAllServerId();

    /**
     * 得到主任务列表
     * @param paramMap 查询参数
     * @return 查询结果
     */
    List<DbScheduleTaskView> getViewTask(Map<String, Object> paramMap);

    /**
     * 根据主任务主键得到主任务详细信息
     * @param id 任务主键
     * @return 查询结果
     */
    DbScheduleTask getTaskDetailById(Long id);

    /**
     * 根据主任务主键获取对应的子任务信息
     * @param id 任务主键
     * @return 查询结果
     */
    List<DbScheduleSubTask> getSubTaskByTaskId(Long id);

    /**
     * 得到主任务分区数量与子任务数量不符合的任务
     * @param paramMap 查询参数
     * @return 查询结果
     */
    List<DbScheduleTask> getMismatchTask(Map<String, Object> paramMap);

    /**
     * 得到需要启动MQ监听服务的主任务
     * @param paramMap 查询参数
     * @return 查询结果
     */
    List<DbScheduleTask> getNeedMQListenerTask(Map<String, Object> paramMap);

    /**
     * 得到所有子任务已经完成或异常的所有任务
     * @param paramMap 查询参数
     * @return 查询结果
     */
    List<DbScheduleTask> getFinishedTask(Map<String, Object> paramMap);

    /**
     * 得到已经到执行时间的任务以及失败后需要重试的任务
     * @param paramMap 查询参数
     * @return 查询结果
     */
    List<DbScheduleTask> getExecutableTask(Map<String, Object> paramMap);

    /**
     * 根据主任务id得到对应的依赖任务
     * @param paramMap 查询参数
     * @return 查询结果
     */
    List<DbScheduleTask> getDependentTask(Map<String, Object> paramMap);

    /**
     * 得到距离当前时间最近的可执行任务的下次执行时间
     * @param paramMap 查询参数
     * @return 查询结果
     */
    DbScheduleTask getNearestExecutableTask(Map<String, Object> paramMap);

    /**
     * 得到可执行的子任务
     * @param paramMap 查询参数
     * @return 查询结果
     */
    List<DbScheduleSubTask> getExecutableSubTask(Map<String, Object> paramMap);

    /**
     * 当服务器初启动时获取上一次服务器因突然关闭造成的失效子任务
     * @param paramMap 查询参数
     * @return 查询结果
     */
    List<DbScheduleSubTask> getOutOfWorkSubTask(Map<String, Object> paramMap);

    /**
     * 修改分布式调度任务
     * @param updateMap 任务修改参数
     * @return 修改成功数量
     */
    int updateDbScheduleTaskByIdAndVersion(Map<String, Object> updateMap);

    /**
     * 插入分布式调度框架的子任务
     * @param dbScheduleSubTask 子任务持久化数据
     * @return 插入成功数量
     */
    int insertDbScheduleSubTask(DbScheduleSubTask dbScheduleSubTask);

    /**
     * 根据子任务ID修改分布式调度任务
     * @param updateSubTask 任务修改参数
     * @return 修改成功数量
     */
    int updateDbScheduleSubTaskById(DbScheduleSubTask updateSubTask);

    /**
     * 根据主任务ID删除主任务
     * @param id 主任务ID
     * @return 删除成功数量
     */
    int deleteDbScheduleTaskById(Long id);

    /**
     * 根据主任务ID删除所有该ID下的子任务
     * @param parentId 主任务ID
     * @return 删除成功数量
     */
    int deleteDbScheduleSubTaskByParentId(Long parentId);

    /**
     * 插入分布式调度主任务记录信息
     * @param dbScheduleTaskRecord 主任务记录信息
     * @return 插入成功数量
     */
    int insertDbScheduleTaskRecord(DbScheduleTaskRecord dbScheduleTaskRecord);

    /**
     * 插入分布式调度子任务记录信息
     * @return 插入成功数量
     */
    int updateDbScheduleTaskRecord();

    /**
     * 插入分布式调度子任务记录信息
     * @param dbScheduleSubTaskRecord 子任务记录信息
     * @return 插入成功数量
     */
    int insertDbScheduleSubTaskRecord(DbScheduleSubTaskRecord dbScheduleSubTaskRecord);

    /**
     * 修改更新子任务执行日志信息
     * @param dbScheduleSubTaskRecord 子任务记录信息
     * @return 修改成功数量
     */
    int updateDbScheduleSubTaskRecord(DbScheduleSubTaskRecord dbScheduleSubTaskRecord);

    /**
     * 插入分布式调度主任务信息
     * @param dbScheduleTask 主任务信息
     * @return 插入成功数量
     */
    int insertDbScheduleTask(DbScheduleTask dbScheduleTask);

    /**
     * 插入分布式调度黑白名单信息表
     * @param dbScheduleWhiteBlackList 黑白名单信息
     * @return 插入成功数量
     */
    int insertDbScheduleWhiteBlackList(DbScheduleWhiteBlackList dbScheduleWhiteBlackList);

    /**
     * 修改分布式调度主任务信息
     * @param dbScheduleTask 主任务信息
     * @return 修改成功数量
     */
    int updateDbScheduleTaskById(DbScheduleTask dbScheduleTask);

    /**
     * 修改分布式调度主任务字段值为Null
     * @param updateMap 修改参数
     * @return 修改成功数量
     */
    int updateTaskFieldValueToNullById(Map<String, Object> updateMap);

    /**
     * 修改主任务开关
     * @param paramMap 修改参数
     * @return 修改成功数量
     */
    int updateTaskSwitchById(Map<String, Object> paramMap);

    /**
     * 修改所有主任务开关
     * @param paramMap 修改参数
     * @return 修改成功数量
     */
    int updateAllTaskSwitch(Map<String, Object> paramMap);

    /**
     * 根据taskId查询当前taskId是否已存在
     * @param taskId 任务Id
     * @return 存在数量
     */
    int checkExistByTaskId(String taskId);

    /**
     * 插入分布式调度MQ配置信息
     * @param dbScheduleMQConfig MQ配置信息
     * @return 插入成功数量
     */
    int insertDbScheduleMQConfig(DbScheduleMQConfig dbScheduleMQConfig);

    /**
     * 修改分布式调度MQ配置信息
     * @param dbScheduleMQConfig MQ配置信息
     * @return 修改成功数量
     */
    int updateDbScheduleMQConfig(DbScheduleMQConfig dbScheduleMQConfig);

    /**
     * 查询mqConfigId是否已存在
     * @param mqConfigId MQ配置ID
     * @return 数量
     */
    long getCountByMqConfigId(String mqConfigId);

    /**
     * 得到MQ配置列表
     * @param paramMap 查询参数
     * @return 查询结果
     */
    List<DbScheduleMQConfig> getViewMQConfig(Map<String, Object> paramMap);

    /**
     * 插入分布式调度用户配置信息
     * @param dbScheduleUserInfo 用户配置信息
     * @return 插入成功数量
     */
    int insertDbScheduleUserInfo(DbScheduleUserInfo dbScheduleUserInfo);

    /**
     * 修改分布式调度用户配置信息
     * @param dbScheduleUserInfo 用户配置信息
     * @return 修改成功数量
     */
    int updateDbScheduleUserInfo(DbScheduleUserInfo dbScheduleUserInfo);

    /**
     * 查询分布式调度用户配置信息
     * @param dbScheduleUserInfo 用户名或用户密码
     * @return 查询到的用户信息
     */
    DbScheduleUserInfo getDbScheduleUserInfo(DbScheduleUserInfo dbScheduleUserInfo);

    /**
     * 插入分布式调度总配置信息
     * @param dbScheduleConfig 总配置信息
     * @return 插入成功数量
     */
    int insertDbScheduleConfig(DbScheduleConfig dbScheduleConfig);

    /**
     * 修改分布式调度总配置信息
     * @param dbScheduleConfig 总配置信息
     * @return 修改成功数量
     */
    int updateDbScheduleConfig(DbScheduleConfig dbScheduleConfig);

    /**
     * 查询分布式调度总配置信息
     * @return 查询到的总配置信息
     */
    DbScheduleConfig getDbScheduleConfig();

}
