package com.stars.easyms.schedule.repository.mysql;

import com.stars.easyms.schedule.repository.DbScheduleRepository;
import com.stars.easyms.schedule.repository.DistributedScheduleDDLDAO;

/**
 * 分布式调度框架数据库表结构验证并创建DAO接口(mysql)
 *
 * @author guoguifang
 */
@DbScheduleRepository("mysqlDistributedScheduleDDLDAO")
public interface MysqlDistributedScheduleDDLDAO extends DistributedScheduleDDLDAO {

}
