package com.stars.easyms.schedule.handler.mvchandler;

import com.stars.easyms.schedule.bean.DataMsg;
import com.stars.easyms.schedule.bean.DbScheduleMQConfig;
import com.stars.easyms.schedule.enums.MQ;
import com.stars.easyms.schedule.service.DistributedScheduleService;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 任务处理类
 *
 * @author guoguifang
 */
public class DbScheduleMQMvcHandler extends BaseDbScheduleMvcHandler {

    @DbSchedulePost(value = "/addMQConfig")
    public DataMsg addMQConfig(HttpServletRequest request) {
        DataMsg dataMsg = DataMsg.getFailDataMsg();

        // 校验参数
        String mqConfigId = getParameterValue(request, "mqConfigId");
        String mqType = getParameterValue(request, "mqType");
        if (mqConfigId == null || mqType == null) {
            dataMsg.setMsgCode("parameterNull");
            return dataMsg;
        }

        // 判断mqConfigId是否已存在
        long count = DistributedScheduleService.getInstance().getCountByMqConfigId(mqConfigId);
        if (count > 0) {
            dataMsg.setMsgCode("mqConfigIdExist");
            return dataMsg;
        }

        DbScheduleMQConfig dbScheduleMQConfig = new DbScheduleMQConfig();
        dbScheduleMQConfig.setMqConfigId(mqConfigId);
        dbScheduleMQConfig.setMqType(mqType);
        if (MQ.ROCKETMQ == MQ.forCode(mqType)) {
            String mqNameServer = getParameterValue(request, "mqNameServer");
            String mqProducerGroupName = getParameterValue(request, "mqProducerGroupName");
            String mqConsumerGroupName = getParameterValue(request, "mqConsumerGroupName");
            if (mqNameServer == null || mqProducerGroupName == null || mqConsumerGroupName == null) {
                dataMsg.setMsgCode("parameterNull");
                return dataMsg;
            }
            dbScheduleMQConfig.setMqConnection(mqNameServer);
            dbScheduleMQConfig.setMqProducerGroupName(mqProducerGroupName);
            dbScheduleMQConfig.setMqConsumerGroupName(mqConsumerGroupName);
        } else {
            String mqBrokerUrl = getParameterValue(request, "mqBrokerUrl");
            String mqUser = getParameterValue(request, "mqUser");
            String mqPassword = getParameterValue(request, "mqPassword");
            if (mqBrokerUrl == null || mqUser == null || mqPassword == null) {
                dataMsg.setMsgCode("parameterNull");
                return dataMsg;
            }
            dbScheduleMQConfig.setMqConnection(mqBrokerUrl);
            dbScheduleMQConfig.setMqUser(mqUser);
            dbScheduleMQConfig.setMqPassword(mqPassword);
        }
        DistributedScheduleService.getInstance().insertDbScheduleMQConfig(dbScheduleMQConfig);
        return DataMsg.getSuccessDataMsg();
    }

    @DbSchedulePost(value = "/getAllMqConfigId")
    public DataMsg getAllMqConfigId() {
        DataMsg dataMsg = DataMsg.getSuccessDataMsg();
        dataMsg.setAttribute("list", DistributedScheduleService.getInstance().getAllMqConfigId());
        return dataMsg;
    }

    /**
     * 显示所有MQ配置信息
     */
    @DbSchedulePost(value = "/getAllViewMQConfig")
    public DataMsg getAllViewMQConfig() {
        DataMsg dataMsg = DataMsg.getSuccessDataMsg();
        dataMsg.setAttribute("list", DistributedScheduleService.getInstance().getViewMQConfig(null));
        return dataMsg;
    }

    /**
     * 根据分页信息显示对应的MQ配置信息
     */
    @DbSchedulePost(value = "/getPagingViewMQConfig")
    public DataMsg getPagingMQConfig(HttpServletRequest request) {
        Map<String, Object> params = new HashMap<>(16);
        params.put("pageSize", Integer.parseInt(getParameterValue(request, "pageSize", "10")));
        params.put("currentPage", Integer.parseInt(getParameterValue(request, "currentPage", "1")));
        List<DbScheduleMQConfig> pagingList = DistributedScheduleService.getInstance().getViewMQConfig(params);
        DataMsg dataMsg = DataMsg.getSuccessDataMsg();
        dataMsg.setAttribute("pagingList", pagingList);
        return dataMsg;
    }

    @Override
    public String getPath() {
        return "/schedule/mq";
    }
}
