package com.stars.easyms.schedule.client;

/**
 * 分布式调度任务远程调用、MQ调用执行返回的结果信息包装类
 *
 * @author guoguifang
 */
public class DistributedTaskExecutionResultWrapper {

    private DistributedTaskExecutionResult distributedTaskExecutionResult;

    public Long getId() {
        return this.distributedTaskExecutionResult.getId();
    }

    public Long getVersion() {
        return this.distributedTaskExecutionResult.getVersion();
    }

    public String getTaskId() {
        return this.distributedTaskExecutionResult.getTaskId();
    }

    public String getBatchNo() {
        return this.distributedTaskExecutionResult.getBatchNo();
    }

    public Integer getPartitionCount() {
        return this.distributedTaskExecutionResult.getPartitionCount();
    }

    public Integer getPartitionIndex() {
        return this.distributedTaskExecutionResult.getPartitionIndex();
    }

    public String getStatus() {
        return this.distributedTaskExecutionResult.getStatus();
    }

    public String getErrorMsg() {
        return this.distributedTaskExecutionResult.getErrorMsg();
    }

    public boolean isExecutionSuccess() {
        return DistributedTaskExecutionResult.RESULT_STATUS_SUCCESS.equals(getStatus());
    }

    public DistributedTaskExecutionResultWrapper(DistributedTaskExecutionResult distributedTaskExecutionResult) {
        this.distributedTaskExecutionResult = distributedTaskExecutionResult;
    }
}
