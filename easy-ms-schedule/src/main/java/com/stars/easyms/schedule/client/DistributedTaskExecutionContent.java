package com.stars.easyms.schedule.client;

import java.util.Map;

/**
 * 分布式调度任务远程调用、MQ调用执行接收到的参数信息
 *
 * @author guoguifang
 */
public final class DistributedTaskExecutionContent {

    /**
     * 子任务主键
     */
    private Long id;

    /**
     * 子任务版本号
     */
    private Long version;

    /**
     * 任务ID
     */
    private String taskId;

    /**
     * 任务名称
     */
    private String taskName;

    /**
     * 任务批次号
     */
    private String batchNo;

    /**
     * 当使用MQ调用分布式任务时，需要按照该回调地址返回回调结果，如果是rocketMQ则表示的是topic，如果是activeMQ则表示的destination
     */
    private String mqCallback;

    /**
     * 分区总数量
     */
    private Integer partitionCount;

    /**
     * 分区当前索引
     */
    private Integer partitionIndex;

    /**
     * 任务参数
     */
    private Map<String, Object> parameters;

    public void setId(Long id) {
        if (this.id == null) {
            this.id = id;
        }
    }

    public void setVersion(Long version) {
        if (this.version == null) {
            this.version = version;
        }
    }

    public void setTaskId(String taskId) {
        if (this.taskId == null) {
            this.taskId = taskId;
        }
    }

    public void setTaskName(String taskName) {
        if (this.taskName == null) {
            this.taskName = taskName;
        }
    }

    public void setBatchNo(String batchNo) {
        if (this.batchNo == null) {
            this.batchNo = batchNo;
        }
    }

    public void setPartitionCount(Integer partitionCount) {
        if (this.partitionCount == null) {
            this.partitionCount = partitionCount;
        }
    }

    public void setPartitionIndex(Integer partitionIndex) {
        if (this.partitionIndex == null) {
            this.partitionIndex = partitionIndex;
        }
    }

    public void setMqCallback(String mqCallback) {
        if (this.mqCallback == null) {
            this.mqCallback = mqCallback;
        }
    }

    public void setParameters(Map<String, Object> parameters) {
        if (this.parameters == null) {
            this.parameters = parameters;
        }
    }

    public Long getId() {
        return id;
    }

    public Long getVersion() {
        return version;
    }

    public String getTaskId() {
        return this.taskId;
    }

    public String getTaskName() {
        return this.taskName;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public String getMqCallback() {
        return this.mqCallback;
    }

    public Integer getPartitionCount() {
        return this.partitionCount;
    }

    public Integer getPartitionIndex() {
        return this.partitionIndex;
    }

    public Map<String, Object> getParameters() {
        return this.parameters;
    }

    public Object get(String key) {
        if (this.parameters != null) {
            return this.parameters.get(key);
        }
        return null;
    }

    public DistributedTaskExecutionResult getSuccessResult() {
        DistributedTaskExecutionResult distributedTaskExecutionResult = getExecutionResult();
        distributedTaskExecutionResult.setStatus(DistributedTaskExecutionResult.RESULT_STATUS_SUCCESS);
        return distributedTaskExecutionResult;
    }

    public DistributedTaskExecutionResult getFailResult(String errorMsg) {
        DistributedTaskExecutionResult distributedTaskExecutionResult = getExecutionResult();
        distributedTaskExecutionResult.setStatus(DistributedTaskExecutionResult.RESULT_STATUS_FAIL);
        distributedTaskExecutionResult.setErrorMsg(errorMsg);
        return distributedTaskExecutionResult;
    }

    private DistributedTaskExecutionResult getExecutionResult() {
        DistributedTaskExecutionResult distributedTaskExecutionResult = new DistributedTaskExecutionResult();
        distributedTaskExecutionResult.setId(this.id);
        distributedTaskExecutionResult.setVersion(this.version);
        distributedTaskExecutionResult.setTaskId(this.taskId);
        distributedTaskExecutionResult.setBatchNo(this.batchNo);
        distributedTaskExecutionResult.setPartitionCount(this.partitionCount);
        distributedTaskExecutionResult.setPartitionIndex(this.partitionIndex);
        return distributedTaskExecutionResult;
    }

    DistributedTaskExecutionContent(){}

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DistributedTaskExecutionContent[taskId=").append(this.taskId).append(", taskName=").append(this.taskName).append(", batchNo=").append(this.batchNo)
                .append(", partitionCount=").append(this.partitionCount).append(", partitionIndex=").append(this.partitionIndex).append(", parameters=").append(this.parameters);
        if (this.mqCallback != null) {
            sb.append(", mqCallback=").append(this.mqCallback);
        }
        return sb.append("]").toString();
    }


}
