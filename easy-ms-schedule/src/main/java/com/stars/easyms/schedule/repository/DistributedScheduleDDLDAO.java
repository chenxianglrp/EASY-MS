package com.stars.easyms.schedule.repository;

import org.apache.ibatis.annotations.Param;

/**
 * 分布式调度框架数据库表结构验证并创建DAO接口
 *
 * @author guoguifang
 */
public interface DistributedScheduleDDLDAO {

    /**
     * 根据表名验证表是否存在
     * @param tableName 表名
     * @return 表中数据
     */
    long getCountByTableName(@Param(value = "tableName") String tableName);

    /**
     * 创建表DB_SCHEDULE_SERVER
     */
    void createTable_dbScheduleServer();

    /**
     * 创建表DB_SCHEDULE_TASK
     */
    void createTable_dbScheduleTask();

    /**
     * 创建表DB_SCHEDULE_TASK索引
     */
    void alterTable_dbScheduleTask_uix_0();
    void alterTable_dbScheduleTask_idx_0();
    void alterTable_dbScheduleTask_idx_1();
    void alterTable_dbScheduleTask_idx_2();

    /**
     * 创建表DB_SCHEDULE_SUB_TASK
     */
    void createTable_dbScheduleSubTask();

    /**
     * 创建表DB_SCHEDULE_SUB_TASK索引
     */
    void alterTable_dbScheduleSubTask_uix_0();

    /**
     * 创建表DB_SCHEDULE_WHITE_BLACK_LIST
     */
    void createTable_dbScheduleWhiteBlackList();

    /**
     * 创建表DB_SCHEDULE_TASK_RECORD
     */
    void createTable_dbScheduleTaskRecord();

    /**
     * 创建表DB_SCHEDULE_SUB_TASK_RECORD
     */
    void createTable_dbScheduleSubTaskRecord();

    /**
     * 创建表DB_SCHEDULE_SUB_TASK_RECORD索引
     */
    void alterTable_dbScheduleSubTaskRecord_idx_0();
    void alterTable_dbScheduleSubTaskRecord_idx_1();

    /**
     * 创建表DB_SCHEDULE_MQ_CONFIG
     */
    void createTable_dbScheduleMqConfig();

    /**
     * 创建表DB_SCHEDULE_USER_INFO
     */
    void createTable_dbScheduleUserInfo();

    /**
     * 创建表DB_SCHEDULE_CONFIG
     */
    void createTable_dbScheduleConfig();
}
