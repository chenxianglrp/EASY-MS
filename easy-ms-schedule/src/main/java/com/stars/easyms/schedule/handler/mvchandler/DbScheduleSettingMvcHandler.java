package com.stars.easyms.schedule.handler.mvchandler;

import com.stars.easyms.schedule.bean.DataMsg;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * 任务处理类
 *
 * @author guoguifang
 */
public class DbScheduleSettingMvcHandler extends BaseDbScheduleMvcHandler {

    /**
     * 修改页面语言
     */
    @DbSchedulePost(value = "/changeLang/{lang}")
    public DataMsg changeLang(@PathVariable("lang") String lang) {
//        DistributedScheduleService.getInstance().updateDbScheduleSetting();
        return DataMsg.getSuccessDataMsg();
    }

    @Override
    public String getPath() {
        return "/schedule/setting";
    }

}
