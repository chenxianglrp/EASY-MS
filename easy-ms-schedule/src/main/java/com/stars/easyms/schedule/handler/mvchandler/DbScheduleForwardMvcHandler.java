package com.stars.easyms.schedule.handler.mvchandler;

import com.stars.easyms.schedule.enums.LoginStatus;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 页面跳转处理类
 *
 * @author guoguifang
 */
public class DbScheduleForwardMvcHandler extends BaseDbScheduleMvcHandler {

    @DbScheduleGet("schedule")
    public void schedule(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        if (isOpenLogin()) {
            sendRedirect(request, response, "/schedule/login");
        } else {
            sendRedirect(request, response, "/schedule/index");
        }
    }

    @DbScheduleGet("schedule/login")
    public void login(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        if (isOpenLogin()) {
            forward(request, response, "/webjars/html/login.html");
        } else {
            sendRedirect(request, response, "/schedule/index");
        }
    }

    @DbScheduleGet("schedule/index")
    public void index(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        forward(request, response, "/webjars/html/index.html");
    }

    @Override
    public String getPath() {
        return "";
    }

    @Override
    public LoginStatus checkAndRefreshLoginStatus(HttpServletRequest request) {
        return LoginStatus.SUCCESS;
    }

    private void sendRedirect(HttpServletRequest request, HttpServletResponse response, String url) throws ServletException, IOException {
        response.sendRedirect(request.getContextPath() + url);
    }

    private void forward(HttpServletRequest request, HttpServletResponse response, String url) throws ServletException, IOException {
        request.getRequestDispatcher(url).forward(request, response);
    }
}
