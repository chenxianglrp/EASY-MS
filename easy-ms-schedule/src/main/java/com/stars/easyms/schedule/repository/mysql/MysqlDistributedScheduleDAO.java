package com.stars.easyms.schedule.repository.mysql;

import com.stars.easyms.schedule.repository.DbScheduleRepository;
import com.stars.easyms.schedule.repository.DistributedScheduleDAO;

/**
 * 分布式调度框架DAO接口(mysql)
 *
 * @author guoguifang
 */
@DbScheduleRepository("mysqlDistributedScheduleDAO")
public interface MysqlDistributedScheduleDAO extends DistributedScheduleDAO {

}
