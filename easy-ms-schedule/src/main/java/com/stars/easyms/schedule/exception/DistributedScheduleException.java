package com.stars.easyms.schedule.exception;

/**
 * 分布式调度异常
 *
 * @author guoguifang
 */
public class DistributedScheduleException extends Exception {

    private static final long serialVersionUID = -1664273629732786719L;

    public DistributedScheduleException() {
    }

    public DistributedScheduleException(String s) {
        super(s);
    }

    public DistributedScheduleException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public DistributedScheduleException(Throwable throwable) {
        super(throwable);
    }

}
