package com.stars.easyms.schedule;

import org.springframework.context.ApplicationContext;

/**
 * 分布式任务上下文接口
 *
 * @author guoguifang
 */
public interface DistributedTaskContext {

    /**
     * 获取spring框架的上下文
     */
    ApplicationContext getApplicationContext();

    /**
     * 获取当前任务ID
     */
    String getTaskId();

    /**
     * 获取当前任务名称
     */
    String getTaskName();

    /**
     * 获取当前子任务分区总数量
     */
    Integer getPartitionCount();

    /**
     * 获取当前子任务分区索引
     */
    Integer getPartitionIndex();

    /**
     * 任务执行参数
     */
    Object get(String key);

}
