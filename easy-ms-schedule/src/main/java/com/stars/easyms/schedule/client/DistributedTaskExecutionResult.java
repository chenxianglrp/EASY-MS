package com.stars.easyms.schedule.client;

/**
 * 分布式调度任务远程调用、MQ调用执行返回的结果信息：
 * 若执行成功则返回状态为"success"的结果信息
 * 若执行失败则返回状态为"fail"的包含详细错误原因的errorMsg的结果信息
 */
public final class DistributedTaskExecutionResult {

    /**
     * 子任务主键
     */
    private Long id;

    /**
     * 子任务版本号
     */
    private Long version;

    /**
     * 任务ID
     */
    private String taskId;

    /**
     * 任务批次号
     */
    private String batchNo;

    /**
     * 分区总数量
     */
    private Integer partitionCount;

    /**
     * 分区当前索引
     */
    private Integer partitionIndex;

    /**
     * 执行状态:执行成功："success",执行失败："fail"
     */
    private String status;

    /**
     * 执行错误时需要把错误信息返回给任务调度器
     */
    private String errorMsg;

    /**
     * 成功状态码
     */
    static final String RESULT_STATUS_SUCCESS = "S";

    /**
     * 失败状态码
     */
    static final String RESULT_STATUS_FAIL = "F";

    void setId(Long id) {
        this.id = id;
    }

    void setVersion(Long version) {
        this.version = version;
    }

    void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    void setPartitionCount(Integer partitionCount) {
        this.partitionCount = partitionCount;
    }

    void setPartitionIndex(Integer partitionIndex) {
        this.partitionIndex = partitionIndex;
    }

    void setStatus(String status) {
        this.status = status;
    }

    void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public Long getId() {
        return id;
    }

    public Long getVersion() {
        return version;
    }

    public String getTaskId() {
        return taskId;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public Integer getPartitionCount() {
        return partitionCount;
    }

    public Integer getPartitionIndex() {
        return partitionIndex;
    }

    public String getStatus() {
        return status;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    DistributedTaskExecutionResult(){}

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DistributedTaskExecutionResult[taskId=").append(this.taskId).append(", batchNo=").append(this.batchNo).append(", partitionCount=").append(this.partitionCount)
                .append(", partitionIndex=").append(this.partitionIndex).append(", status=").append(this.status);
        if (this.errorMsg != null) {
            sb.append(", errorMsg=").append(this.errorMsg);
        }
        return sb.append("]").toString();
    }

}
