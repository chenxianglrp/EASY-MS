package com.stars.easyms.schedule.factory;

/**
 * MQFactory接口
 *
 * @author guoguifang
 */
public interface MQFactory {

    String JMS_CONN_FACTORY = "javax.jms.ConnectionFactory";

    String ACTIVEMQ_CONN_FACTORY = "org.apache.activemq.ActiveMQConnectionFactory";

    String ROCKETMQ_PRODUCER = "org.apache.rocketmq.client.producer.DefaultMQProducer";

    String ROCKETMQ_CONSUMER = "org.apache.rocketmq.client.consumer.DefaultMQPushConsumer";

}
