package com.stars.easyms.schedule.bean;

import com.stars.easyms.schedule.util.TimeUtil;
import com.stars.easyms.schedule.enums.Switch;
import com.stars.easyms.schedule.enums.YesOrNo;
import com.stars.easyms.schedule.constant.DistributedScheduleConstants;
import com.stars.easyms.schedule.util.SystemUtils;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 分布式调度框架服务器信息
 *
 * @author guoguifang
 */
@Data
public class DbScheduleServer implements Serializable {

    /**
     * 当前服务器IP地址
     */
    private String ip = SystemUtils.getHostIp();

    /**
     * 当前服务端口号
     */
    private String port = SystemUtils.getPort();

    /**
     * 服务器标识，不指定的话默认是"IP:PORT"
     */
    private String serverId = SystemUtils.getHostIp() + ":" + SystemUtils.getPort();

    /**
     * 是否激活(激活：true，未激活：false)，默认激活状态
     */
    private boolean enabled = true;

    /**
     * 分布式调度框架开关，默认打开
     */
    private String scheduleSwitch = Switch.OPEN.getCode();

    /**
     * 核心线程池大小
     */
    private Integer corePoolSize = DistributedScheduleConstants.DEFAULT_CORE_POOL_SIZE;

    /**
     * 线程池最大容量大小
     */
    private Integer maxPoolSize = DistributedScheduleConstants.DEFAULT_MAX_POOL_SIZE;

    /**
     * 当线程数大于核心线程数时,空闲线程存活时间,支持单位(天:d/D,时:h/H,分:m/M,秒:s/S,毫秒:ms/MS),不写单位默认秒
     */
    private String keepAliveTime = DistributedScheduleConstants.DEFAULT_KEEP_ALIVE_TIME;

    /**
     * 当线程数大于核心线程数时,空闲线程存活时间转换成毫秒
     */
    private transient Long keepAliveTimeMilli = DistributedScheduleConstants.DEFAULT_KEEP_ALIVE_TIME_MILLI;

    /**
     * 第一次启动时延迟启动时间,支持单位(天:d/D,时:h/H,分:m/M,秒:s/S,毫秒:ms/MS),不写单位默认秒
     */
    private String startDelayTime = DistributedScheduleConstants.DEFAULT_START_DELAY_TIME;

    /**
     * 第一次启动时延迟启动时间
     */
    private transient Long startDelayTimeMilli = DistributedScheduleConstants.DEFAULT_START_DELAY_TIME_MILLI;

    /**
     * 心跳时间
     */
    private Timestamp heartbeatTime;

    /**
     * 服务器是否存活(是：Y,否：N)，若停止心跳30秒以上认为该服务器死亡
     */
    private String alive = YesOrNo.YES.getCode();

    /**
     * 是否立即唤醒调度器(是：Y，否：N)，当手动修改了任务信息时需要设置为Y，否则手动修改任务信息不会立即生效
     */
    private String wakeup = YesOrNo.NO.getCode();

    /**
     * 乐观锁
     */
    private Long version;

    public void copy(DbScheduleServer dbScheduleServer) {
        this.enabled = dbScheduleServer.enabled;
        this.serverId = dbScheduleServer.serverId;
        this.scheduleSwitch = dbScheduleServer.scheduleSwitch;
        this.corePoolSize = dbScheduleServer.corePoolSize;
        this.maxPoolSize = dbScheduleServer.maxPoolSize;
        this.keepAliveTime = dbScheduleServer.keepAliveTime;
        this.keepAliveTimeMilli = dbScheduleServer.keepAliveTimeMilli;
        this.startDelayTime = dbScheduleServer.startDelayTime;
        this.startDelayTimeMilli = dbScheduleServer.startDelayTimeMilli;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public void setScheduleSwitch(String scheduleSwitch) {
        this.scheduleSwitch = scheduleSwitch;
    }

    public void setCorePoolSize(int corePoolSize) {
        this.corePoolSize = corePoolSize;
    }

    public void setMaxPoolSize(int maxPoolSize) {
        this.maxPoolSize = maxPoolSize;
    }

    public void setKeepAliveTime(String keepAliveTime) {
        Long keepAliveTimeMilli = TimeUtil.parseToMilliseconds(keepAliveTime);
        if (keepAliveTimeMilli != null) {
            if (keepAliveTimeMilli < DistributedScheduleConstants.MIN_KEEP_ALIVE_TIME_MILLI) {
                keepAliveTimeMilli = DistributedScheduleConstants.MIN_KEEP_ALIVE_TIME_MILLI;
                keepAliveTime = DistributedScheduleConstants.MIN_KEEP_ALIVE_TIME;
            }
            this.keepAliveTimeMilli = keepAliveTimeMilli;
            this.keepAliveTime = keepAliveTime;
        }
    }

    public void setKeepAliveTimeMilli(long keepAliveTimeMilli) {
        if (keepAliveTimeMilli > 0) {
            this.keepAliveTimeMilli = keepAliveTimeMilli;
            this.keepAliveTime = TimeUtil.parseFromMilliseconds(keepAliveTimeMilli);
        }
    }

    public void setStartDelayTime(String startDelayTime) {
        Long startDelayTimeMilli = TimeUtil.parseToMilliseconds(startDelayTime);
        if (startDelayTimeMilli != null) {
            if (startDelayTimeMilli < DistributedScheduleConstants.MIN_START_DELAY_TIME_MILLI) {
                startDelayTimeMilli = DistributedScheduleConstants.MIN_START_DELAY_TIME_MILLI;
                startDelayTime = DistributedScheduleConstants.MIN_START_DELAY_TIME;
            }
            this.startDelayTimeMilli = startDelayTimeMilli;
            this.startDelayTime = startDelayTime;
        }
    }

    public void setAlive(String alive) {
        this.alive = alive;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

}
