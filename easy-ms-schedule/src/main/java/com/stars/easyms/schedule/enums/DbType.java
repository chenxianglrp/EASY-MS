package com.stars.easyms.schedule.enums;

/**
 * 数据库类型
 *
 * @author guoguifang
 */
public enum DbType {

    /**
     * ORACLE
     */
    ORACLE("O", "oracle"),

    /**
     * MYSQL
     */
    MYSQL("M", "mysql");

    private String name;

    private String code;

    public static DbType forCode(String code) {
        if (ORACLE.code.equalsIgnoreCase(code)) {
            return ORACLE;
        }
        return MYSQL;
    }

    DbType(String code, String name) {
        this.name = name;
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }
}
