package com.stars.easyms.schedule.handler.mvchandler;

import com.stars.easyms.schedule.bean.DataMsg;
import com.stars.easyms.schedule.service.DistributedScheduleService;

/**
 * 任务处理类
 *
 * @author guoguifang
 */
public class DbScheduleServerMvcHandler extends BaseDbScheduleMvcHandler {

    @DbSchedulePost(value = "/getAllServerId")
    public DataMsg getAllServerId() {
        DataMsg dataMsg = DataMsg.getSuccessDataMsg();
        dataMsg.setAttribute("list", DistributedScheduleService.getInstance().getAllServerId());
        return dataMsg;
    }

    @Override
    public String getPath() {
        return "/schedule/server";
    }

}
