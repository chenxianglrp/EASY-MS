package com.stars.easyms.schedule.handler.mvcadapter;

import com.stars.easyms.schedule.handler.DbScheduleHandler;
import com.stars.easyms.schedule.handler.DbScheduleMvcAdapter;
import com.stars.easyms.schedule.util.StringUtils;
import org.springframework.util.Assert;

/**
 * 分布式调度框架抽象MVC适配器
 *
 * @author guoguifang
 */
public abstract class AbstractDbScheduleMvcAdapter<E extends DbScheduleHandler<?>> implements DbScheduleMvcAdapter {

    private final E delegate;

    private String path;

    public AbstractDbScheduleMvcAdapter(E delegate) {
        Assert.notNull(delegate, "Delegate must not be null");
        this.delegate = delegate;
    }

    @Override
    public E getDbScheduleHandler() {
        return this.delegate;
    }

    @Override
    public String getPath() {
        return this.path != null ? this.path : "/" + this.delegate.getId();
    }

    public void setPath(String path) {
        this.path = StringUtils.formattingPath(path);
    }

    @Override
    public String getName() {
        return this.delegate.getId();
    }

    @Override
    public Class<? extends DbScheduleHandler> getDbScheduleHandlerType() {
        return this.delegate.getClass();
    }
}
