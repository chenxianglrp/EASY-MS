package com.stars.easyms.schedule.bean;

import lombok.Getter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 页面数据消息传输对象
 *
 * @author guoguifang
 */
@Getter
public class DataMsg implements Serializable {

    private static final long serialVersionUID = 176365344234123L;

    private String status;

    private String msgCode;

    private String msg;

    private Map<String, Object> data = new HashMap<>(16);

    public static DataMsg getSuccessDataMsg() {
        DataMsg dataMsg = new DataMsg();
        dataMsg.status = "success";
        return dataMsg;
    }

    public static DataMsg getFailDataMsg() {
        DataMsg dataMsg = new DataMsg();
        dataMsg.status = "fail";
        return dataMsg;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setMsgCode(String msgCode) {
        this.msgCode = msgCode;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setFail() {
        this.status = "fail";
    }

    public void setData(Map<String, Object> data) {
        if (data != null) {
            this.data.putAll(data);
        }
    }

    public void setTotalPage(int totalPage) {
        this.data.put("totalPage", totalPage);
    }

    public void setTotalCount(int totalCount) {
        this.data.put("totalCount", totalCount);
    }

    public void setAttribute(String name, Object value) {
        this.data.put(name, value);
    }

    public Object getAttribute(String name) {
        return this.data.get(name);
    }
}
