package com.stars.easyms.schedule.configuration;

import com.stars.easyms.schedule.EnableDistributedSchedule;
import com.stars.easyms.schedule.starter.DistributedScheduleStarter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 批量框架的自动配置类
 *
 * @author guoguifang
 */
@Configuration
@ConditionalOnBean(annotation = EnableDistributedSchedule.class)
public class DistributedScheduleAutoConfiguration {

    @Bean(destroyMethod = "stop")
    @ConditionalOnMissingBean(DistributedScheduleStarter.class)
    public DistributedScheduleStarter distributedScheduleStarter() {
        return new DistributedScheduleStarter();
    }

}
