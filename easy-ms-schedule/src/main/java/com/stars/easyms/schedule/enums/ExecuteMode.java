package com.stars.easyms.schedule.enums;

/**
 * 任务执行模式
 *
 * @author guoguifang
 */
public enum ExecuteMode {

    /**
     * quartz模式
     */
    QUARTZ("Q", "quartz模式"),

    /**
     * 分布式模式
     */
    DISTRIBUTED("D", "分布式模式");

    private String chineseName;

    private String code;

    ExecuteMode(String code, String chineseName) {
        this.chineseName = chineseName;
        this.code = code;
    }

    public String getChineseName() {
        return chineseName;
    }

    public String getCode() {
        return code;
    }
}
