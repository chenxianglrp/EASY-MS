package com.stars.easyms.schedule.constant;

/**
 * 分布式调度框架的常量类
 * 
 * @author guoguifang
 */
public interface DistributedScheduleConstants {

    /**
     * 默认核心线程池大小
     */
    int DEFAULT_CORE_POOL_SIZE = 10;

    /**
     * 默认核心线程池最大容量
     */
    int DEFAULT_MAX_POOL_SIZE = 20;

    /**
     * 默认空闲线程存活时间,默认5分钟
     */
    String DEFAULT_KEEP_ALIVE_TIME = "5m";

    /**
     * 默认空闲线程存活时间,默认5分钟
     */
    long DEFAULT_KEEP_ALIVE_TIME_MILLI = 300000;

    /**
     * 最低空闲线程存活时间,默认1分钟
     */
    String MIN_KEEP_ALIVE_TIME = "1m";

    /**
     * 最低空闲线程存活时间,默认1分钟
     */
    long MIN_KEEP_ALIVE_TIME_MILLI = 60000;

    /**
     * 默认调度器最大闲置时间,默认60秒钟
     */
    long DEFAULT_IDLE_TIME_MILLI = 60000;

    /**
     * 默认心跳间隔，默认3秒钟
     */
    long DEFAULT_HEARTBEAT_INTERVAL_MILLI = 3000;

    /**
     * 默认第一次启动时延迟启动时间，默认15秒钟
     */
    String DEFAULT_START_DELAY_TIME = "15s";

    /**
     * 默认第一次启动时延迟启动时间，默认15秒钟
     */
    long DEFAULT_START_DELAY_TIME_MILLI = 15000;

    /**
     * 最低第一次启动时延迟启动时间，默认10秒钟
     */
    String MIN_START_DELAY_TIME = "10s";

    /**
     * 最低第一次启动时延迟启动时间，默认10秒钟
     */
    long MIN_START_DELAY_TIME_MILLI = 10000;

    /**
     * 子任务最小分区数量
     */
    int MIN_PARTITION_COUNT = 1;

    /**
     * 子任务最大分区数量
     */
    int MAX_PARTITION_COUNT = 1000;

    /**
     * 默认的rocketMQ的生产者groupName
     */
    String DEFAULT_MQ_PRODUCER_GROUP_NAME = "DISTRIBUTED_PRODUCER";

    /**
     * 默认的rocketMQ的消费者groupName
     */
    String DEFAULT_MQ_CONSUMER_GROUP_NAME = "DISTRIBUTED_CONSUMER";

    /**
     * 登录异常处理
     */
    String LOGIN_EXCEPTION_HANDLER = "/loginExceptionHandler";

    /**
     * 默认的管理员用户名
     */
    String DEFAULT_USER_NAME = "schedule";

    /**
     * 默认的登录会话超时时间
     */
    String DEFAULT_SESSION_TIMEOUT = "30m";

    /**
     * 页面布局：L：左；T：上
     */
    String PAGE_LAYOUT_LEFT = "L";

    /**
     * 页面布局：L：左；T：上
     */
    String PAGE_LAYOUT_TOP = "T";

    /**
     * 页面语言：简体中文：Z；英文：E
     */
    String PAGE_LANGUAGE_ZH = "Z";

    /**
     * 页面语言：简体中文：Z；英文：E
     */
    String PAGE_LANGUAGE_EN = "E";

    /**
     * 默认主题颜色
     */
    String DEFAULT_PAGE_THEME = "#FFF";
}
