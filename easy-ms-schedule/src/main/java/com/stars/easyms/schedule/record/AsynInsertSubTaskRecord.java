package com.stars.easyms.schedule.record;

import com.stars.easyms.schedule.bean.BatchResult;
import com.stars.easyms.schedule.bean.DbScheduleSubTaskRecord;
import com.stars.easyms.schedule.service.DistributedScheduleService;
import com.stars.easyms.schedule.util.ThrowableUtil;

import java.util.List;

/**
 * 异步插入分布式调度子任务记录
 *
 * @author guoguifang
 */
public class AsynInsertSubTaskRecord extends BaseAsynchronousTask<DbScheduleSubTaskRecord> {

    private static final AsynInsertSubTaskRecord ASYN_INSERT_SUB_TASK_RECORD = new AsynInsertSubTaskRecord();

    public static void add(DbScheduleSubTaskRecord dbScheduleSubTaskRecord) {
        ASYN_INSERT_SUB_TASK_RECORD.offer(dbScheduleSubTaskRecord);
    }

    @Override
    protected BatchResult execute(List<DbScheduleSubTaskRecord> list) throws Exception {
        for (DbScheduleSubTaskRecord dbScheduleSubTaskRecord : list) {
            Throwable throwable = dbScheduleSubTaskRecord.getThrowable();
            if (throwable != null) {
                dbScheduleSubTaskRecord.setErrorMessage(ThrowableUtil.getThrowableStackTrace(throwable));
            }
        }
        return DistributedScheduleService.getInstance().batchInsertDbScheduleSubTaskRecord(list);
    }

    @Override
    protected long delayTime() {
        return 500;
    }

    private AsynInsertSubTaskRecord() {}

}
