package com.stars.easyms.schedule.bean;

import lombok.Data;

import java.sql.Timestamp;

/**
 * 分布式调度用户配置表信息
 *
 * @author guoguifang
 */
@Data
public class DbScheduleUserInfo {

    /**
     * 用户名
     */
    private String userName;

    /**
     * 用户密码
     */
    private String userPassword;

    /**
     * 登录时间
     */
    private Timestamp loginTime;

    /**
     * 当前时间
     */
    private Timestamp currentTime;

    /**
     * 登录标识
     */
    private String loginToken;

    /**
     * 页面布局
     */
    private String pageLayout;

    /**
     * 页面语言
     */
    private String pageLanguage;

    /**
     * 是否启动开灯模式
     */
    private String pageLightup;

    /**
     * 页面主题
     */
    private String pageTheme;

    /**
     * dashboard页面的内容ID及顺序，模块之间用","分割
     */
    private String dashboardContent;

    /**
     * 乐观锁
     */
    private Long version;
}
