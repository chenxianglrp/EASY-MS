package com.stars.easyms.schedule.util;

import java.io.PrintWriter;
import java.io.StringWriter;

/***
 * 异常信息处理工具类
 *
 * @author guoguifang
 */
public class ThrowableUtil {

    /**
     * 根据异常或错误获取堆栈信息
     * @param throwable 异常或错误
     * @return 堆栈信息
     */
    public static String getThrowableStackTrace(Throwable throwable) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        try {
            throwable.printStackTrace(pw);
            return sw.toString();
        } finally {
            pw.close();
        }
    }

    /**
     * 根据异常或错误获取简要堆栈信息
     * @param throwable 异常或错误
     * @return 堆栈信息
     */
    public static String getSimpleStackTrace(Throwable throwable) {
        String simpleStackTrace;
        if (throwable == null || StringUtils.isBlank((simpleStackTrace = throwable.getMessage()))) {
            return "";
        }
        if (simpleStackTrace.length() > 100) {
            simpleStackTrace = simpleStackTrace.substring(0, 100) + "...";
        }
        return simpleStackTrace;
    }
}
