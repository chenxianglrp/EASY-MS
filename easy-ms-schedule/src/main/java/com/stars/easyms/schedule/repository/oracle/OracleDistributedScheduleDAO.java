package com.stars.easyms.schedule.repository.oracle;

import com.stars.easyms.schedule.repository.DbScheduleRepository;
import com.stars.easyms.schedule.repository.DistributedScheduleDAO;

/**
 * 分布式调度框架DAO接口(oracle)
 *
 * @author guoguifang
 */
@DbScheduleRepository("oracleDistributedScheduleDAO")
public interface OracleDistributedScheduleDAO extends DistributedScheduleDAO {

}
