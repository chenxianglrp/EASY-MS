package com.stars.easyms.schedule.util;

import java.text.ParseException;
import java.util.Date;

/**
 * Corn表达式工具类
 *
 * @author guoguifang
 */
public class CronExpressionUtil {

    /**
     * 根据当前时间及Cron表达式获取下次执行时间
     * @param cronExpressionStr cron表达式
     * @param currentTime 当前时间
     * @return 下次执行时间
     */
    public static Date getNextFireDate(String cronExpressionStr, Date currentTime) {
        try {
            Date nextFireDate = null;
            for (String singleCronExpression : cronExpressionStr.split(";")) {
                CronExpression cronExpression = new CronExpression(singleCronExpression.trim());
                Date thisNextFireDate = cronExpression.getTimeAfter(currentTime);
                if (thisNextFireDate != null && (nextFireDate == null || nextFireDate.after(thisNextFireDate))) {
                    nextFireDate = thisNextFireDate;
                }
            }
            return nextFireDate;
        } catch (ParseException e) {
            return null;
        }
    }
}
