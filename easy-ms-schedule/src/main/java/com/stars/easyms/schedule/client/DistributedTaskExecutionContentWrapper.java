package com.stars.easyms.schedule.client;

import java.util.Map;

/**
 * 分布式调度任务远程调用、MQ调用执行接收到的参数信息包装类
 *
 * @author guoguifang
 */
public class DistributedTaskExecutionContentWrapper {

    private DistributedTaskExecutionContent distributedTaskExecutionContent;

    public void setId(Long id) {
        this.distributedTaskExecutionContent.setId(id);
    }

    public void setVersion(Long version) {
        this.distributedTaskExecutionContent.setVersion(version);
    }

    public void setTaskId(String taskId) {
        this.distributedTaskExecutionContent.setTaskId(taskId);
    }

    public void setTaskName(String taskName) {
        this.distributedTaskExecutionContent.setTaskName(taskName);
    }

    public void setBatchNo(String batchNo) {
        this.distributedTaskExecutionContent.setBatchNo(batchNo);
    }

    public void setPartitionCount(Integer partitionCount) {
        this.distributedTaskExecutionContent.setPartitionCount(partitionCount);
    }

    public void setPartitionIndex(Integer partitionIndex) {
        this.distributedTaskExecutionContent.setPartitionIndex(partitionIndex);
    }

    public void setMqCallback(String mqCallback) {
        this.distributedTaskExecutionContent.setMqCallback(mqCallback);
    }

    public void setParameters(Map<String, Object> parameters) {
        this.distributedTaskExecutionContent.setParameters(parameters);
    }

    public DistributedTaskExecutionContent getObject() {
        return this.distributedTaskExecutionContent;
    }

    public DistributedTaskExecutionContentWrapper() {
        this.distributedTaskExecutionContent = new DistributedTaskExecutionContent();
    }
}
