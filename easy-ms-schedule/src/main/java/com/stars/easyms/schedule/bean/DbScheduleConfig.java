package com.stars.easyms.schedule.bean;

import com.stars.easyms.schedule.util.TimeUtil;
import lombok.Data;

/**
 * 分布式调度框架总配置信息
 *
 * @author guoguifang
 */
@Data
public class DbScheduleConfig {

    /**
     * 版本号
     */
    private String version;

    /**
     * 版本迭代编号
     */
    private Integer versionIteration;

    /**
     * 是否开启登录模式(是:Y,否:N)
     */
    private String openLogin;

    /**
     * 是否使用本地登录(是:Y,否:N),如果是则使用本表中密码登录,如果否则使用配置中的远程登录地址
     */
    private String nativeLogin;

    /**
     * 远程登录地址
     */
    private String remoteLoginUrl;

    /**
     * 若开启登录模式，是否使用单点登录模式(是:Y,否:N)
     */
    private String useSso;

    /**
     * 若开启登录模式，登录的会话超时时间，支持单位(天:d/D,时:h/H,分:m/M,秒:s/S,毫秒:ms/MS),不写单位默认秒
     */
    private String sessionTimeout;

    /**
     * 登录的会话超时时间(单位：毫秒)
     */
    private transient Long sessionTimeoutMilli;

    /**
     * 路由策略(争抢:S,轮询:P,权重:W)
     */
    private String routePolicy;

    public void setSessionTimeout(String sessionTimeout) {
        Long sessionTimeoutMilli = TimeUtil.parseToMilliseconds(sessionTimeout);
        if (sessionTimeoutMilli != null) {
            this.sessionTimeoutMilli = sessionTimeoutMilli;
            this.sessionTimeout = sessionTimeout;
        }
    }
}
