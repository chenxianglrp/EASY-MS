package com.stars.easyms.schedule.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 线程状态
 *
 * @author guoguifang
 */
public enum ThreadStatus {

    /**
     * 初始化
     */
    INIT("初始化", "init", 0),

    /**
     * 待运行
     */
    RUNABLE("待运行", "runable", 1),

    /**
     * 运行中
     */
    RUNNING("运行中", "running", 2),

    /**
     * 空闲中
     */
    IDLE("空闲中", "idle", 3),

    /**
     * 阻塞中
     */
    BLOCKED("阻塞中", "blocked", 4),

    /**
     * 正在尝试暂停...
     */
    PAUSING("正在尝试暂停...", "pausing", 5),

    /**
     * 已暂停
     */
    PAUSED("已暂停", "paused", 6);

    private String chineseName;

    private String englishName;

    private int code;

    ThreadStatus(String chineseName, String englishName, int code) {
        this.chineseName = chineseName;
        this.englishName = englishName;
        this.code = code;
    }

    public String getChineseName() {
        return chineseName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public int getCode() {
        return code;
    }

    private static Map<Integer, ThreadStatus> codeLookup;

    public static ThreadStatus forCode(int code) {
        return codeLookup.get(code);
    }

    static {
        codeLookup = new HashMap<>();
        for (ThreadStatus threadStatus : values()) {
            codeLookup.put(threadStatus.code, threadStatus);
        }
    }

}