# Easy-Ms-Logging

　　easy-ms-logging是一个基于spring的、异步输出日志的功能增强型框架，是Easy-Ms的一个组件框架，**但也支持在非Easy-Ms项目的Spring Boot或Spring Mvc项目中使用**。

## 目录

　　[一、功能特点](#%E4%B8%80-%E5%8A%9F%E8%83%BD%E7%89%B9%E7%82%B9)

　　[二、依赖引入](#%E4%BA%8C-%E4%BE%9D%E8%B5%96%E5%BC%95%E5%85%A5)

　　　　[1. 使用parent的方式](#1-%E4%BD%BF%E7%94%A8parent%E7%9A%84%E6%96%B9%E5%BC%8F)

　　　　[2. 直接引用](#2-%E7%9B%B4%E6%8E%A5%E5%BC%95%E7%94%A8)

　　[三、 非Easy-Ms项目支持](#%E4%B8%89-%E9%9D%9Eeasy-ms%E9%A1%B9%E7%9B%AE%E6%94%AF%E6%8C%81)

　　　　[1. Spring Boot项目](#1-spring-boot%E9%A1%B9%E7%9B%AE)

　　　　[2. Spring Mvc项目](#2-spring-mvc%E9%A1%B9%E7%9B%AE)

　　[四、 配置](#%E5%9B%9B-%E9%85%8D%E7%BD%AE)

　　[五、 easy-ms-logging开启与否性能比较](#%E4%BA%94-easy-ms-logging%E5%BC%80%E5%90%AF%E4%B8%8E%E5%90%A6%E6%80%A7%E8%83%BD%E6%AF%94%E8%BE%83)

　　[六、 easy-ms-logging与logback异步性能比较](#%E5%85%AD-easy-ms-logging%E4%B8%8Elogback%E5%BC%82%E6%AD%A5%E6%80%A7%E8%83%BD%E6%AF%94%E8%BE%83)

　　　　[1. logback同步](#1-logback%E5%90%8C%E6%AD%A5)

　　　　[2. logback异步](#2-logback%E5%BC%82%E6%AD%A5)

　　　　[3. 结论](#3-%E7%BB%93%E8%AE%BA)

##  一、 功能特点

1. 采用**双层异步机制**，在大幅提升性能的同时还能保证占用较少内存；
2. 采用**自研无锁队列**，**高并发下不仅无阻塞还能保证线程安全及队列顺序一致**；
3. **日志对象复用**，高并发下可大大降低内存的占用；
4. 优化日志打印逻辑，**高并发下减少IO次数**，提升日志输出性能；
5. 重写部分日志转化器（如日期转化器等），高并发下日志转化速度大大提升；
6. 支持**Logback（已兼容所有版本）、commons-logging**，可支持其他框架的扩展；
7. 支持**实时动态开关切换**（需使用配置中心），可在应用运行期间自由切换是否开启异步日志框架；
8. 支持**在线实时监控**，可实时查看开关、异步日志框架的使用情况等信息；

## 二、 依赖引入

#### 1. 使用parent的方式

　　好处：easy-ms-parent里包含了许多的依赖版本管理，可以在依赖管理`<dependencies>`中，有较多常用的引入直接使用默认的版本而无需指定版本号

```xml
pom.xml:
    <!-- 可以直接把easy-ms-parent当做parent，或者当引入多个easy-ms组件时使用parent可以只需设置一次版本号 -->
    <parent>
        <groupId>com.stars.easyms</groupId>
        <artifactId>easy-ms-parent</artifactId>
        <version>${easy-ms.version}</version>
    </parent>
		
    <!-- 如果是直接引入easy-ms，则不需要引入easy-ms-logging，easy-ms已经默认依赖了easy-ms-logging -->
    <dependencies>
        <dependency>
            <groupId>com.stars.easyms</groupId>
            <artifactId>easy-ms-logging</artifactId>
        </dependency>
    </dependencies>
```

#### 2. 直接引用

```xml
    <!-- 如果是直接引入easy-ms，则不需要引入easy-ms-datasource，easy-ms已经默认依赖了easy-ms-datasource -->
    <dependencies>
        <dependency>
            <groupId>com.stars.easyms</groupId>
            <artifactId>easy-ms-logging</artifactId>
            <version>${easy-ms.version}</version>
        </dependency>
    </dependencies>
```

## 三、 非Easy-Ms项目支持

　　如果需要创建新的项目，可以直接引入easy-ms，如果是已存在的项目且无法改造成easy-ms项目的，但是想使用easy-ms-logging框架的，可以按照下面两种方法引入后使用。

#### 1. Spring Boot项目

　　**直接在项目pom.xml中按照依赖引入即可，支持spring boot 1.x.x和spring boot 2.x.x。**

#### 2. Spring Mvc项目

　　1). 在spring的xml配置文件中增加`<easy-ms-logging:init/>`，如下所示：

```xml
<!-- 1.在beans标签中添加[xmlns:easy-ms-logging="http://www.easy-ms.com/schema/logging"] -->
<!-- 2.在beans标签中的[xsi:schemaLocation]里添加[http://www.easy-ms.com/schema/logging http://www.easy-ms.com/schema/logging.xsd] -->
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:easy-ms-logging="http://www.easy-ms.com/schema/logging"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
       http://www.easy-ms.com/schema/logging http://www.easy-ms.com/schema/logging.xsd">
    
    <!-- 初始化Easy-Ms异步日志框架 -->
    <easy-ms-logging:init/>

</beans>
```
　　2). 在web.xml里增加easy-ms-logging的监听器，**需要将该监听器放在所有监听器的最上方**
```xml
<!-- 需要将该监听器放在所有监听器的最上方 -->
<listener>
    <listener-class>com.stars.easyms.logger.config.EasyMsLoggerListener</listener-class>
</listener>

<!-- spring的监听器需要放在EasyMsLoggerListener的下方 -->
<listener>
    <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
</listener>

```

## 四、 配置

| **配置**          | **缺省值** | **说明**                                                     |
| ----------------- | ---------- | ------------------------------------------------------------ |
| logging.asyn      | true       | 是否开启异步日志框架                                         |
| logging.sift-asyn | true       | 输出到sift文件时是否使用异步，若使用异步可能造成文件大小无法精确切割 |

　　yaml配置文件如下：

```yaml
# 如果不配置，默认都是true，即默认开启
logging:
  asyn: true
  sift-asyn: false
```

## 五、 easy-ms-logging开启与否性能比较

```java
/**
 * 用于异步日志的测试demo，通过配置logging.asyn来控制是否开启异步日志</p>
 */
@Slf4j
@Service
public class EasyMsLoggingDemoService implements RestService<EasyMsLoggingDemoInput, BaseOutput> {

    @Override
    public BaseOutput execute(EasyMsLoggingDemoInput input) {
        long time = System.currentTimeMillis();
        for (int i = 0, count = input.getCount(); i < count; i++) {
            log.info("测试：" + i);
        }
        BaseOutput output = new BaseOutput();
        long usedTime = System.currentTimeMillis() - time;
        output.setRetMsg("是否启动easy-ms日志异步:" + EasyMsLoggerHelper.isLogAsynEnabled() + ", 打印" + input.getCount() + "条日志用时：" + usedTime + "毫秒");
        return output;
    }
}
```

　　未开启Easy-Ms-logging异步日志框架的截图：

![easy-ms-logging-disable-result](/doc/easy-ms-logging/image/disable-result.png)

　　开启Easy-Ms-logging异步日志框架的截图：

![easy-ms-logging-enable-result](/doc/easy-ms-logging/image/enable-result.png)

## 六、 easy-ms-logging与logback异步性能比较

　　logback可分为以下几种模式：**同步、异步（阻塞型）、异步（阻塞型-丢失模式）、异步（非阻塞型）。**

#### 1. logback同步

　　测试代码：

![easy-ms-logging-syn-demo](/doc/easy-ms-logging/image/syn-demo.png)

　　使用同步模式打印控制台、日志滚动文件两种：

![easy-ms-logging-syn-root-info](/doc/easy-ms-logging/image/syn-root-info.png)

　　测试结果：

![easy-ms-logging-syn-result](/doc/easy-ms-logging/image/syn-result.png)

#### 2. logback异步

　　测试代码：

![easy-ms-logging-asyn-demo](/doc/easy-ms-logging/image/asyn-demo.png)

　　使用异步模式打印控制台、日志滚动文件两种：

![easy-ms-logging-asyn-root-info](/doc/easy-ms-logging/image/asyn-root-info.png)

　　**阻塞型**

![easy-ms-logging-asyn-block](/doc/easy-ms-logging/image/asyn-block.png)

![easy-ms-logging-asyn-block-result](/doc/easy-ms-logging/image/asyn-block-result.png)

　　**阻塞型-允许丢弃**

![easy-ms-logging-asyn-block-discard](/doc/easy-ms-logging/image/asyn-block-discard.png)

![easy-ms-logging-asyn-block-discard-result](/doc/easy-ms-logging/image/asyn-block-discard-result.png)

![easy-ms-logging-asyn-block-discard-result-1](/doc/easy-ms-logging/image/asyn-block-discard-result-1.png)

　　**非阻塞性**

![easy-ms-logging-asyn-never-block](/doc/easy-ms-logging/image/asyn-never-block.png)

![easy-ms-logging-asyn-never-block-result](/doc/easy-ms-logging/image/asyn-never-block-result.png)

![easy-ms-logging-asyn-never-block-result-1](/doc/easy-ms-logging/image/asyn-never-block-result-1.png)

#### 3. 结论

1. **logback的`同步模式`打印效率比较低**，且打印的appender越多所需时间越多。
2. logback的**`异步阻塞模式`比`同步模式`打印效率要高一些，而且随着queueSize的值增大而有所性能提升，但是提升有限**，这个取决于logback的异步实现原理（[点击此处](/doc/easy-ms-logging/logback-asyn-theory.md)查看logback异步源码分析），原因是因为阻塞模式下日志添加到队列时，若队列已满会阻塞线程，每当队列中一条日志打印完成后才会把新日志添加进队列，即**队列满时logback的异步几乎等价于同步模式**。
3. logback的**`异步阻塞-允许丢失模式`**及**`异步非阻塞模式`**两种模式都是一百万条日志却只打印了不到六万条，**日志丢失率非常高，不推荐**。
4. easy-ms-logging框架的异步使用的是无阻塞队列，**在不丢失日志及保证日志打印顺序的前提下，还能大大提升性能。**



> 　　我们提供框架的维护和技术支持，提供给感兴趣的开发人员学习和交流的平台，同时希望感兴趣的同学一起加入我们让框架更完善，让我们一起为了给大家提供更好更优秀的框架而努力。
>
> 　　若该框架及本文档中有什么错误及问题的，可采用创建Issue、加群@管理、私聊作者或管理员的方式进行联系，谢谢！