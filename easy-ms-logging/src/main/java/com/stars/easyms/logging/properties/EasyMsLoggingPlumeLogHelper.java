package com.stars.easyms.logging.properties;

import com.plumelog.log4j2.appender.KafkaAppender;
import com.plumelog.log4j2.appender.RedisAppender;

/**
 * <p>className: EasyMsLoggingPlumeLogHelper</p>
 * <p>description: EasyMs日志属性帮助类</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/11/30 8:41 下午
 */
public final class EasyMsLoggingPlumeLogHelper {

    private static boolean enabled;

    private static String appName;

    private static String env;

    private static boolean redisAppenderEnabled;

    private static RedisAppender redisAppender;

    private static boolean kafkaAppenderEnabled;

    private static KafkaAppender kafkaAppender;

    static void setEnabled(boolean enabled) {
        EasyMsLoggingPlumeLogHelper.enabled = enabled;
    }

    static void setAppName(String appName) {
        EasyMsLoggingPlumeLogHelper.appName = appName;
    }

    static void setEnv(String env) {
        EasyMsLoggingPlumeLogHelper.env = env;
    }

    static void setRedisAppenderEnabled(boolean redisAppenderEnabled) {
        EasyMsLoggingPlumeLogHelper.redisAppenderEnabled = redisAppenderEnabled;
    }

    static void setRedisAppender(RedisAppender redisAppender) {
        EasyMsLoggingPlumeLogHelper.redisAppender = redisAppender;
    }

    static void setKafkaAppenderEnabled(boolean kafkaAppenderEnabled) {
        EasyMsLoggingPlumeLogHelper.kafkaAppenderEnabled = kafkaAppenderEnabled;
    }

    static void setKafkaAppender(KafkaAppender kafkaAppender) {
        EasyMsLoggingPlumeLogHelper.kafkaAppender = kafkaAppender;
    }

    public static boolean isEnabled() {
        return enabled;
    }

    public static String getAppName() {
        return appName;
    }

    public static String getEnv() {
        return env;
    }

    public static boolean isRedisAppenderEnabled() {
        return enabled && redisAppenderEnabled && redisAppender != null;
    }

    public static RedisAppender getRedisAppender() {
        return redisAppender;
    }

    public static boolean isKafkaAppenderEnabled() {
        return enabled && !isRedisAppenderEnabled() && kafkaAppenderEnabled && kafkaAppender != null;
    }

    public static KafkaAppender getKafkaAppender() {
        return kafkaAppender;
    }

    public static boolean isAppenderEnabled() {
        return enabled && ((redisAppenderEnabled && redisAppender != null) || (kafkaAppenderEnabled && kafkaAppender != null));
    }

    private EasyMsLoggingPlumeLogHelper() {
    }
}
