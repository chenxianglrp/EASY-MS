package com.stars.easyms.logging.masking;

import com.stars.easyms.logging.properties.EasyMsLoggingDataMaskingHelper;
import com.stars.easyms.logging.properties.EasyMsLoggingProperties.DataMasking.Replace;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.StringBuilders;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * 日志脱敏工具类.
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/8/11 3:07 下午
 */
public final class EasyMsLoggingMaskingUtil {
    
    private static final String RECURSION_PREFIX = "[...";
    
    private static final String RECURSION_SUFFIX = "...]";
    
    private static final String ERROR_PREFIX = "[!!!";
    
    private static final String ERROR_SEPARATOR = "=>";
    
    private static final String ERROR_MSG_SEPARATOR = ":";
    
    private static final String ERROR_SUFFIX = "!!!]";
    
    private static final String SEPARATOR_MARK = "∵∵∵∵EASY-MS∵∵∵∵MASKING∵∵∵∵SEPARATOR∵∵∵∵MARK∵∵∵∵";
    
    private static final ThreadLocal<SimpleDateFormat> SIMPLE_DATE_FORMAT_THREAD_LOCAL = new ThreadLocal<>();
    
    public static String masking(String message) {
        if (EasyMsLoggingDataMaskingHelper.isEnabled()) {
            message = SEPARATOR_MARK + message + SEPARATOR_MARK;
            for (Replace replace : EasyMsLoggingDataMaskingHelper.getReplaceMap().values()) {
                if (replace.isEnabled() && StringUtils.isNotBlank(replace.getRegex())) {
                    message = message.replaceAll(replace.getRegex(), replace.getReplacement());
                }
            }
        }
        return message.replaceAll(SEPARATOR_MARK, "");
    }
    
    public static Object masking(Object obj) {
        if (EasyMsLoggingDataMaskingHelper.isEnabled() && !(obj instanceof Throwable)) {
            StringBuilder stringBuilder = new StringBuilder();
            recursiveDeepToString(obj, stringBuilder, new HashSet<>());
            return masking(stringBuilder.toString());
        }
        return obj;
    }
    
    private static void recursiveDeepToString(final Object obj, final StringBuilder str, final Set<String> dejaVu) {
        if (appendSpecialTypes(obj, str)) {
            return;
        }
        if (isMaybeRecursive(obj)) {
            appendPotentiallyRecursiveValue(obj, str, dejaVu);
        } else {
            tryObjectToString(obj, str);
        }
    }
    
    private static boolean appendSpecialTypes(final Object obj, final StringBuilder str) {
        return StringBuilders.appendSpecificTypes(str, obj) || appendDate(obj, str);
    }
    
    private static boolean appendDate(final Object obj, final StringBuilder str) {
        if (!(obj instanceof Date)) {
            return false;
        }
        final Date date = (Date) obj;
        final SimpleDateFormat format = getSimpleDateFormat();
        str.append(format.format(date));
        return true;
    }
    
    private static SimpleDateFormat getSimpleDateFormat() {
        SimpleDateFormat result = SIMPLE_DATE_FORMAT_THREAD_LOCAL.get();
        if (result == null) {
            result = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
            SIMPLE_DATE_FORMAT_THREAD_LOCAL.set(result);
        }
        return result;
    }
    
    private static boolean isMaybeRecursive(final Object o) {
        return o.getClass().isArray() || o instanceof Map || o instanceof Collection;
    }
    
    private static void appendPotentiallyRecursiveValue(final Object o, final StringBuilder str,
            final Set<String> dejaVu) {
        final Class<?> oClass = o.getClass();
        if (oClass.isArray()) {
            appendArray(o, str, dejaVu, oClass);
        } else if (o instanceof Map) {
            appendMap(o, str, dejaVu);
        } else if (o instanceof Collection) {
            appendCollection(o, str, dejaVu);
        }
    }
    
    private static void appendArray(final Object o, final StringBuilder str, Set<String> dejaVu,
            final Class<?> oClass) {
        if (oClass == byte[].class) {
            str.append(Arrays.toString((byte[]) o));
        } else if (oClass == short[].class) {
            str.append(Arrays.toString((short[]) o));
        } else if (oClass == int[].class) {
            str.append(Arrays.toString((int[]) o));
        } else if (oClass == long[].class) {
            str.append(Arrays.toString((long[]) o));
        } else if (oClass == float[].class) {
            str.append(Arrays.toString((float[]) o));
        } else if (oClass == double[].class) {
            str.append(Arrays.toString((double[]) o));
        } else if (oClass == boolean[].class) {
            str.append(Arrays.toString((boolean[]) o));
        } else if (oClass == char[].class) {
            str.append(Arrays.toString((char[]) o));
        } else {
            final String id = identityToString(o);
            if (dejaVu.contains(id)) {
                str.append(RECURSION_PREFIX).append(id).append(RECURSION_SUFFIX);
            } else {
                dejaVu.add(id);
                final Object[] oArray = (Object[]) o;
                str.append('[');
                boolean first = true;
                for (final Object current : oArray) {
                    if (first) {
                        first = false;
                    } else {
                        str.append(", ");
                    }
                    recursiveDeepToString(current, str, new HashSet<>(dejaVu));
                }
                str.append(']');
            }
        }
    }
    
    private static void appendMap(final Object o, final StringBuilder str, Set<String> dejaVu) {
        final String id = identityToString(o);
        if (dejaVu.contains(id)) {
            str.append(RECURSION_PREFIX).append(id).append(RECURSION_SUFFIX);
        } else {
            dejaVu.add(id);
            final Map<?, ?> oMap = (Map<?, ?>) o;
            str.append('{');
            boolean isFirst = true;
            for (final Object o1 : oMap.entrySet()) {
                final Map.Entry<?, ?> current = (Map.Entry<?, ?>) o1;
                if (isFirst) {
                    isFirst = false;
                } else {
                    str.append(", ");
                }
                final Object key = current.getKey();
                final Object value = current.getValue();
                recursiveDeepToString(key, str, new HashSet<>(dejaVu));
                str.append('=');
                recursiveDeepToString(value, str, new HashSet<>(dejaVu));
            }
            str.append('}');
        }
    }
    
    private static void appendCollection(final Object o, final StringBuilder str, Set<String> dejaVu) {
        final String id = identityToString(o);
        if (dejaVu.contains(id)) {
            str.append(RECURSION_PREFIX).append(id).append(RECURSION_SUFFIX);
        } else {
            dejaVu.add(id);
            final Collection<?> oCol = (Collection<?>) o;
            str.append('[');
            boolean isFirst = true;
            for (final Object anOCol : oCol) {
                if (isFirst) {
                    isFirst = false;
                } else {
                    str.append(", ");
                }
                recursiveDeepToString(anOCol, str, new HashSet<>(dejaVu));
            }
            str.append(']');
        }
    }
    
    private static void tryObjectToString(final Object o, final StringBuilder str) {
        try {
            str.append(o.toString());
        } catch (final Throwable t) {
            handleErrorInObjectToString(o, str, t);
        }
    }
    
    private static void handleErrorInObjectToString(final Object o, final StringBuilder str, final Throwable t) {
        str.append(ERROR_PREFIX);
        str.append(identityToString(o));
        str.append(ERROR_SEPARATOR);
        final String msg = t.getMessage();
        final String className = t.getClass().getName();
        str.append(className);
        if (!className.equals(msg)) {
            str.append(ERROR_MSG_SEPARATOR);
            str.append(msg);
        }
        str.append(ERROR_SUFFIX);
    }
    
    private static String identityToString(final Object obj) {
        if (obj == null) {
            return null;
        }
        return obj.getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(obj));
    }
    
    private EasyMsLoggingMaskingUtil() {
    }
}