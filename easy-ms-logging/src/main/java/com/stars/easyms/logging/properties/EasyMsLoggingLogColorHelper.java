package com.stars.easyms.logging.properties;

import com.stars.easyms.logging.ansi.EasyMsAnsiGenerator;
import org.springframework.lang.NonNull;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>className: EasyMsLoggingLogColorHelper</p>
 * <p>description: easy-ms的logging模块的LogColor帮助类</p>
 *
 * @author guoguifang
 * @version 1.7.2
 * @date 2021/1/20 6:02 下午
 */
public final class EasyMsLoggingLogColorHelper {

    private static boolean enabled;

    private static Map<String, String> levelColorMap = Collections.emptyMap();

    private static Map<String, String> levelAnsiColorMap = Collections.emptyMap();

    static void setEnabled(boolean enabled) {
        EasyMsLoggingLogColorHelper.enabled = enabled;
    }

    static void setLevelColorMap(Map<String, String> levelColorMap) {
        if (levelColorMap == null) {
            EasyMsLoggingLogColorHelper.levelColorMap = Collections.emptyMap();
            levelAnsiColorMap = Collections.emptyMap();
            return;
        }
        EasyMsLoggingLogColorHelper.levelColorMap = Collections.unmodifiableMap(levelColorMap);
        Map<String, String> localLevelAnsiColorMap = new HashMap<>(8);
        levelColorMap.forEach((key, value) -> localLevelAnsiColorMap.put(key, EasyMsAnsiGenerator.getAnsiStr(value)));
        EasyMsLoggingLogColorHelper.levelAnsiColorMap = Collections.unmodifiableMap(localLevelAnsiColorMap);
    }

    public static boolean isEnabled() {
        return enabled;
    }

    @NonNull
    public static Map<String, String> getLevelColorMap() {
        return levelColorMap;
    }

    @NonNull
    public static Map<String, String> getLevelAnsiColorMap() {
        return levelAnsiColorMap;
    }

    private EasyMsLoggingLogColorHelper() {
    }
}
