package com.stars.easyms.logging.listener;

import com.stars.easyms.base.event.EasyMsConfigChangeEvent;
import com.stars.easyms.base.listener.EasyMsConfigChangeListener;
import com.stars.easyms.logging.properties.EasyMsLoggingInitialization;
import com.stars.easyms.logging.properties.EasyMsLoggingProperties;

/**
 * <p>className: EasyMsLoggingConfigChangeListener</p>
 * <p>description: EasyMs的日志配置修改监听类</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/12/1 9:57 上午
 */
public class EasyMsLoggingConfigChangeListener implements EasyMsConfigChangeListener {

    @Override
    public void listen(EasyMsConfigChangeEvent configChangeEvent) {
        EasyMsLoggingInitialization.init(getProperties(EasyMsLoggingProperties.class));
    }

    @Override
    public String listenConfigPrefix() {
        return EasyMsLoggingProperties.PREFIX;
    }

}
