package com.stars.easyms.logging.monitor;

import com.plumelog.core.MessageAppenderFactory;
import com.plumelog.log4j2.appender.KafkaAppender;
import com.plumelog.log4j2.appender.RedisAppender;
import com.stars.easyms.logging.appender.EasyMsPlumeLogAppender;
import com.stars.easyms.logging.constant.EasyMsLoggingConstants;
import com.stars.easyms.logging.properties.EasyMsLoggingDataMaskingHelper;
import com.stars.easyms.logging.properties.EasyMsLoggingLogColorHelper;
import com.stars.easyms.logging.properties.EasyMsLoggingPlumeLogHelper;
import com.stars.easyms.monitor.MonitorRequestHandler;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * <p>className: EasyMsLoggingMonitorRequestHandler</p>
 * <p>description: EasyMs日志模块的监控方法类</p>
 *
 * @author guoguifang
 * @date 2019-11-20 10:25
 * @since 1.4.0
 */
public final class EasyMsLoggingMonitorRequestHandler implements MonitorRequestHandler {
    
    @Override
    public Map<String, Object> getMonitorInfo() {
        Map<String, Object> returnMap = new LinkedHashMap<>();
        returnMap.put("logColor", getLogColorMap());
        returnMap.put("plumeLog", getPlumeLogMap());
        returnMap.put("masking", getMaskingMap());
        return returnMap;
    }
    
    @Override
    public Map<String, Object> getBriefMonitorInfo() {
        Map<String, Object> returnMap = new LinkedHashMap<>();
        returnMap.put("logColor", Collections.singletonMap("enabled", EasyMsLoggingLogColorHelper.isEnabled()));
        returnMap.put("plumeLog", Collections.singletonMap("enabled",
                EasyMsLoggingPlumeLogHelper.isEnabled() && (EasyMsLoggingPlumeLogHelper.isRedisAppenderEnabled()
                        || EasyMsLoggingPlumeLogHelper.isKafkaAppenderEnabled())));
        returnMap.put("masking", Collections.singletonMap("enabled", EasyMsLoggingDataMaskingHelper.isEnabled()));
        return returnMap;
    }
    
    private Map<String, Object> getLogColorMap() {
        Map<String, Object> logColorMap = new LinkedHashMap<>();
        if (!EasyMsLoggingLogColorHelper.isEnabled()) {
            logColorMap.put("enabled", false);
            return logColorMap;
        }
        logColorMap.put("enabled", true);
        logColorMap.put("levelColor", EasyMsLoggingLogColorHelper.getLevelColorMap());
        logColorMap.put("levelAnsiColor", EasyMsLoggingLogColorHelper.getLevelAnsiColorMap());
        return logColorMap;
    }
    
    private Map<String, Object> getPlumeLogMap() {
        Map<String, Object> plumeLogMap = new LinkedHashMap<>();
        if (!EasyMsLoggingPlumeLogHelper.isEnabled()) {
            plumeLogMap.put("enabled", false);
            return plumeLogMap;
        }
        plumeLogMap.put("enabled", true);
        plumeLogMap.put("appName", EasyMsLoggingPlumeLogHelper.getAppName());
        plumeLogMap.put("env", EasyMsLoggingPlumeLogHelper.getEnv());
        plumeLogMap.put("send count", EasyMsPlumeLogAppender.getCount());
        
        LinkedBlockingQueue linkedBlockingQueue = (LinkedBlockingQueue) MessageAppenderFactory.rundataQueue;
        plumeLogMap.put("run data queue count", linkedBlockingQueue.size());
        plumeLogMap.put("run data queue remaining capacity", linkedBlockingQueue.remainingCapacity());
        
        boolean isRedisEnabled = EasyMsLoggingPlumeLogHelper.isRedisAppenderEnabled();
        Map<String, Object> redisMap = new LinkedHashMap<>();
        if (isRedisEnabled) {
            RedisAppender redisAppender = EasyMsLoggingPlumeLogHelper.getRedisAppender();
            redisMap.put("enabled", true);
            redisMap.put("model", redisAppender.getModel());
            redisMap.put("host", redisAppender.getRedisHost());
            redisMap.put("port", redisAppender.getRedisPort());
            redisMap.put("db", redisAppender.getRedisDb());
            redisMap.put("run model",
                    "1".equals(redisAppender.getRunModel()) ? "high performance" : "all information");
        } else {
            redisMap.put("enabled", false);
        }
        plumeLogMap.put("redis", redisMap);
        
        boolean isKafkaEnabled = EasyMsLoggingPlumeLogHelper.isKafkaAppenderEnabled();
        Map<String, Object> kafkaMap = new LinkedHashMap<>();
        if (isKafkaEnabled) {
            KafkaAppender kafkaAppender = EasyMsLoggingPlumeLogHelper.getKafkaAppender();
            kafkaMap.put("enabled", true);
            kafkaMap.put("hosts", kafkaAppender.getKafkaHosts());
            kafkaMap.put("run model",
                    "1".equals(kafkaAppender.getRunModel()) ? "high performance" : "all information");
        } else {
            kafkaMap.put("enabled", false);
        }
        plumeLogMap.put("kafka", kafkaMap);
        return plumeLogMap;
    }
    
    private Map<String, Object> getMaskingMap() {
        Map<String, Object> maskingMap = new LinkedHashMap<>();
        if (!EasyMsLoggingDataMaskingHelper.isEnabled()) {
            maskingMap.put("enabled", false);
            return maskingMap;
        }
        maskingMap.put("enabled", true);
        maskingMap.put("replace", EasyMsLoggingDataMaskingHelper.getReplaceMap());
        return maskingMap;
    }
    
    @Override
    public String getModuleName() {
        return EasyMsLoggingConstants.MODULE_NAME;
    }
    
    @Override
    public int getOrder() {
        return 20;
    }
    
}