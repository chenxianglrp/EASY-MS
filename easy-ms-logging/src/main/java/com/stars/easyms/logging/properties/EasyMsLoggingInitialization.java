package com.stars.easyms.logging.properties;

import com.plumelog.log4j2.appender.KafkaAppender;
import com.plumelog.log4j2.appender.RedisAppender;
import com.stars.easyms.base.util.SpringBootUtil;
import com.stars.easyms.logging.properties.EasyMsLoggingProperties.DataMasking.Replace;
import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.stars.easyms.logging.constant.EasyMsLoggingConstants.DEFAULT_DATA_MASKING_REPLACEMENT;
import static com.stars.easyms.logging.constant.EasyMsLoggingConstants.DEFAULT_DATA_MASKING_REPLACE_MAP;
import static com.stars.easyms.logging.constant.EasyMsLoggingConstants.DEFAULT_LEVEL_COLOR_MAP;
import static com.stars.easyms.logging.constant.EasyMsLoggingConstants.PLUME_LOG_KAFKA_APPENDER_NAME;
import static com.stars.easyms.logging.constant.EasyMsLoggingConstants.PLUME_LOG_REDIS_APPENDER_NAME;

/**
 * <p>className: EasyMsLoggingInitialization</p>
 * <p>description: EasyMs日志模块初始化类</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/12/1 9:59 上午
 */
public final class EasyMsLoggingInitialization {
    
    public static void init(EasyMsLoggingProperties easyMsLoggingProperties) {
        initLogColor(easyMsLoggingProperties);
        initPlumeLog(easyMsLoggingProperties);
        initMessageFactory(easyMsLoggingProperties);
    }
    
    private static void initLogColor(EasyMsLoggingProperties easyMsLoggingProperties) {
        EasyMsLoggingProperties.LogColor logColor = easyMsLoggingProperties.getLogColor();
        boolean enabled = logColor.isEnabled();
        EasyMsLoggingLogColorHelper.setEnabled(enabled);
        if (!enabled) {
            EasyMsLoggingLogColorHelper.setLevelColorMap(null);
            return;
        }
        
        Map<String, String> levelColorMap = new HashMap<>(8);
        levelColorMap.putAll(DEFAULT_LEVEL_COLOR_MAP);
        Map<String, String> levelMap = logColor.getLevel();
        if (!levelMap.isEmpty()) {
            levelMap.forEach((key, value) -> levelColorMap.put(key.trim().toUpperCase(), value.trim().toUpperCase()));
        }
        EasyMsLoggingLogColorHelper.setLevelColorMap(levelColorMap);
    }
    
    private static void initPlumeLog(EasyMsLoggingProperties easyMsLoggingProperties) {
        EasyMsLoggingProperties.PlumeLog plumeLog = easyMsLoggingProperties.getPlumeLog();
        
        // 设置plumeLog是否激活，如果plumeLog未激活则appender都不激活
        if (!plumeLog.isEnabled()) {
            EasyMsLoggingPlumeLogHelper.setEnabled(false);
            return;
        }
        EasyMsLoggingPlumeLogHelper.setEnabled(true);
        
        String appName = plumeLog.getAppName() != null ? plumeLog.getAppName() : SpringBootUtil.getApplicationName();
        EasyMsLoggingPlumeLogHelper.setAppName(appName);
        
        String env = plumeLog.getEnv() != null ? plumeLog.getEnv() : SpringBootUtil.getActiveProfile();
        EasyMsLoggingPlumeLogHelper.setEnv(env);
        
        EasyMsLoggingProperties.PlumeLog.Redis redis = plumeLog.getRedis();
        String redisHost = redis.getHost();
        EasyMsLoggingPlumeLogHelper.setRedisAppenderEnabled(StringUtils.isNotBlank(redisHost));
        
        EasyMsLoggingProperties.PlumeLog.Kafka kafka = plumeLog.getKafka();
        String kafkaHosts = kafka.getHosts();
        EasyMsLoggingPlumeLogHelper.setKafkaAppenderEnabled(StringUtils.isNotBlank(kafkaHosts));
        
        // redisAppender和kafkaAppender只能有一个
        if (EasyMsLoggingPlumeLogHelper.getRedisAppender() == null
                && EasyMsLoggingPlumeLogHelper.getKafkaAppender() == null) {
            if (StringUtils.isNotBlank(redisHost)) {
                EasyMsLoggingPlumeLogHelper.setRedisAppender(RedisAppender
                        .createAppender(PLUME_LOG_REDIS_APPENDER_NAME, appName, env, redisHost, redis.getPort(),
                                redis.getAuth(), plumeLog.getMaxCount(), plumeLog.getRunModel(), plumeLog.getExpand(),
                                redis.getDb(), plumeLog.getLogQueueSize(), plumeLog.getThreadPoolSize(),
                                plumeLog.isCompressor(), redis.getModel(), redis.getMasterName(), null, null));
            } else if (StringUtils.isNotBlank(kafkaHosts)) {
                EasyMsLoggingPlumeLogHelper.setKafkaAppender(KafkaAppender
                        .createAppender(PLUME_LOG_KAFKA_APPENDER_NAME, appName, env, kafkaHosts, null,
                                plumeLog.getExpand(), plumeLog.getRunModel(), plumeLog.getMaxCount(),
                                plumeLog.getLogQueueSize(), plumeLog.getThreadPoolSize(), plumeLog.isCompressor(), null,
                                null));
            }
        }
    }
    
    private static void initMessageFactory(EasyMsLoggingProperties easyMsLoggingProperties) {
        EasyMsLoggingProperties.DataMasking dataMasking = easyMsLoggingProperties.getDataMasking();
        if (!dataMasking.isEnabled()) {
            EasyMsLoggingDataMaskingHelper.setEnabled(false);
            return;
        }
        EasyMsLoggingDataMaskingHelper.setEnabled(true);
        
        Map<String, Replace> localReplaceMap = new LinkedHashMap<>(8);
        localReplaceMap.putAll(DEFAULT_DATA_MASKING_REPLACE_MAP);
        Map<String, Replace> replaceMap = dataMasking.getReplace();
        if (!replaceMap.isEmpty()) {
            replaceMap.forEach((type, replace) -> {
                String key = type.trim().toUpperCase();
                if (replace.isEnabled()) {
                    String regex = replace.getRegex();
                    String replacement = replace.getReplacement();
                    Replace defaultReplace = localReplaceMap.get(key);
                    if (defaultReplace == null) {
                        if (StringUtils.isNotBlank(regex) && StringUtils.isBlank(replacement)) {
                            replace.setReplacement(DEFAULT_DATA_MASKING_REPLACEMENT);
                        }
                    } else if (StringUtils.isBlank(regex) && StringUtils.isBlank(replacement)) {
                        replace.setRegex(defaultReplace.getRegex());
                        replace.setReplacement(defaultReplace.getReplacement());
                    }
                    // 如果是默认的replace配置需要是配套的regex和replacement,所以只要其中一个为空则置为未激活
                    else if (StringUtils.isBlank(regex) || StringUtils.isBlank(replacement)) {
                        replace.setEnabled(false);
                    }
                }
                localReplaceMap.put(key, replace);
            });
        }
        EasyMsLoggingDataMaskingHelper.setReplaceMap(Collections.unmodifiableMap(localReplaceMap));
    }
    
    private EasyMsLoggingInitialization() {
    }
}
