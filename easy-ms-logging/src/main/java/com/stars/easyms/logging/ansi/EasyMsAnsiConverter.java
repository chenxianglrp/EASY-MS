package com.stars.easyms.logging.ansi;

import com.stars.easyms.logging.properties.EasyMsLoggingLogColorHelper;
import org.apache.commons.lang3.StringUtils;

import static com.stars.easyms.logging.constant.EasyMsLoggingConstants.*;

/**
 * <p>className: EasyMsAnsiConverter</p>
 * <p>description: Ansi转换类</p>
 *
 * @author guoguifang
 * @version 1.7.2
 * @date 2021/1/21 6:21 下午
 */
public final class EasyMsAnsiConverter {

    public static StringBuilder convertForLevel(String level, StringBuilder stringBuilder) {
        return new StringBuilder(convertForLevel(level, stringBuilder.toString()));
    }

    public static String convertForLevel(String level, String message) {
        if (EasyMsLoggingLogColorHelper.isEnabled() && StringUtils.isNotBlank(level)) {
            return convertWithAnsi(EasyMsLoggingLogColorHelper.getLevelAnsiColorMap().get(level.trim().toUpperCase()), message);
        }
        return message;
    }

    public static String convertWithAnsi(String ansiStr, String message) {
        if (StringUtils.isBlank(ansiStr)) {
            return message;
        }
        return ansiStr + message + RESET;
    }

    private EasyMsAnsiConverter() {
    }

}
