package com.stars.easyms.logging.boot;

import com.stars.easyms.base.util.SpringBootUtil;
import com.stars.easyms.logging.log4j2.EasyMsLog4J2Initialization;
import com.stars.easyms.logging.log4j2.EasyMsLog4J2LoggingSystem;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringApplicationRunListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.Ordered;
import org.springframework.core.env.ConfigurableEnvironment;

import static org.springframework.boot.logging.LoggingSystem.SYSTEM_PROPERTY;

/**
 * <p>className: EasyMsLoggingSpringBootListener</p>
 * <p>description: EasyMs的logging模块SpringBoot启动监听器</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/11/16 2:48 下午
 */
public class EasyMsLoggingSpringBootListener implements SpringApplicationRunListener, Ordered {

    public EasyMsLoggingSpringBootListener(SpringApplication application, String[] args) {
        SpringBootUtil.setSpringApplication(application, args);
        System.setProperty(SYSTEM_PROPERTY, EasyMsLog4J2LoggingSystem.class.getName());
        EasyMsLog4J2Initialization.init();
    }

    @Override
    public void starting() {
        // Intentionally blank
    }

    @Override
    public void environmentPrepared(ConfigurableEnvironment environment) {
        // Intentionally blank
    }

    @Override
    public void contextPrepared(ConfigurableApplicationContext context) {
        // Intentionally blank
    }

    @Override
    public void contextLoaded(ConfigurableApplicationContext context) {
        // Intentionally blank
    }

    @Override
    public void started(ConfigurableApplicationContext context) {
        // Intentionally blank
    }

    @Override
    public void running(ConfigurableApplicationContext context) {
        // Intentionally blank
    }

    @Override
    public void failed(ConfigurableApplicationContext context, Throwable exception) {
        // Intentionally blank
    }

    @Override
    public int getOrder() {
        return HIGHEST_PRECEDENCE;
    }
}