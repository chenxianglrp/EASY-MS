package com.stars.easyms.logging.trace;

import com.stars.easyms.base.alarm.EasyMsAlarmAssistor;
import com.stars.easyms.base.interceptor.EasyMsAopMethodInterceptor;
import com.stars.easyms.base.trace.EasyMsTraceHelper;
import com.stars.easyms.base.util.ReflectUtil;
import lombok.extern.slf4j.Slf4j;
import org.aopalliance.intercept.MethodInvocation;

/**
 * <p>className: EasyMsLoggingSchedulingTraceAspect</p>
 * <p>description: EasyMsLoggingSchedulingTraceAspect拦截器</p>
 *
 * @author guoguifang
 * @version 1.7.1
 * @date 2020/12/17 2:05 下午
 */
@Slf4j
final class EasyMsLoggingSchedulingTraceAspect implements EasyMsAopMethodInterceptor {

    @Override
    public Object intercept(MethodInvocation methodInvocation) throws Throwable {
        return EasyMsTraceHelper.setTraceIdIfNecessary(t -> {
            try {
                return proceed(methodInvocation);
            } catch (Throwable throwable) {

                // 记录异常信息
                log.error("The scheduling task of method({}) failed to execute!",
                        ReflectUtil.getMethodFullName(methodInvocation.getMethod()), throwable);

                // 增加告警
                EasyMsAlarmAssistor.sendExceptionAlarmMessage(throwable);

                // 继续抛出异常信息
                throw throwable;
            }
        });
    }

}