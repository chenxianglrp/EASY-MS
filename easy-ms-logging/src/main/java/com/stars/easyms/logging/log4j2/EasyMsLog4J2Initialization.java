package com.stars.easyms.logging.log4j2;

import com.stars.easyms.base.util.ReflectUtil;
import com.stars.easyms.logging.log4j2.message.EasyMsLog4J2MessageFactory;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.logging.log4j.spi.AbstractLogger;
import org.apache.logging.log4j.spi.LoggerRegistry;
import org.apache.logging.slf4j.Log4jLogger;
import org.apache.logging.slf4j.Log4jLoggerFactory;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;

/**
 * EasyMs log4j2 日志初始化.
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/8/10 7:58 下午
 */
@Slf4j
public final class EasyMsLog4J2Initialization {
    
    public static void init() {
        try {
            ReflectUtil.writeStaticFinalField(AbstractLogger.class, "DEFAULT_MESSAGE_FACTORY_CLASS",
                    EasyMsLog4J2MessageFactory.class);
            ReflectUtil.writeStaticFinalField(LoggerRegistry.class, "DEFAULT_FACTORY_KEY",
                    EasyMsLog4J2MessageFactory.class.getName());
        } catch (Exception e) {
            log.error("Easy-ms Log4j2 init fail!", e);
            System.exit(1);
        }
        
        Field loggerFieldInLog4jLogger = FieldUtils.getField(Log4jLogger.class, "logger", true);
        Log4jLoggerFactory log4jLoggerFactory = (Log4jLoggerFactory) LoggerFactory.getILoggerFactory();
        log4jLoggerFactory.getLoggerContexts().forEach(
                loggerContext -> log4jLoggerFactory.getLoggersInContext(loggerContext).values().forEach(logger -> {
                    Log4jLogger log4jLogger = (Log4jLogger) logger;
                    try {
                        ReflectUtil.writeFinalField(loggerFieldInLog4jLogger.get(log4jLogger), "messageFactory",
                                new EasyMsLog4J2MessageFactory());
                    } catch (Exception e) {
                        log.error("Easy-ms Log4j2 init fail!", e);
                        System.exit(1);
                    }
                }));
    }
    
    private EasyMsLog4J2Initialization() {
    }
}
