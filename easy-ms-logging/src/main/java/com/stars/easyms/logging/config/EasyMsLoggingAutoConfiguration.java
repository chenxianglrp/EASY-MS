package com.stars.easyms.logging.config;

import com.stars.easyms.logging.boot.EasyMsLoggingBeanPostProcessor;
import com.stars.easyms.logging.properties.EasyMsLoggingInitialization;
import com.stars.easyms.logging.trace.EasyMsLoggingPlumeLogTracePointcutAdvisor;
import com.stars.easyms.logging.trace.EasyMsLoggingSchedulingTracePointcutAdvisor;
import com.stars.easyms.logging.properties.EasyMsLoggingProperties;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncAnnotationBeanPostProcessor;

/**
 * <p>className: EasyMsLoggingAutoConfiguration</p>
 * <p>description: EasyMsLogging自动加载类</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/11/17 1:53 下午
 */
@Configuration
@EnableConfigurationProperties(EasyMsLoggingProperties.class)
public class EasyMsLoggingAutoConfiguration implements InitializingBean {

    private final EasyMsLoggingProperties easyMsLoggingProperties;

    public EasyMsLoggingAutoConfiguration(EasyMsLoggingProperties easyMsLoggingProperties) {
        this.easyMsLoggingProperties = easyMsLoggingProperties;
    }

    @Bean
    public EasyMsLoggingPlumeLogTracePointcutAdvisor easyMsLoggingTracePointcutAdvisor() {
        return new EasyMsLoggingPlumeLogTracePointcutAdvisor();
    }

    @Bean
    public EasyMsLoggingSchedulingTracePointcutAdvisor easyMsLoggingSchedulingTracePointcutAdvisor() {
        return new EasyMsLoggingSchedulingTracePointcutAdvisor();
    }

    @Bean
    @ConditionalOnBean(AsyncAnnotationBeanPostProcessor.class)
    public EasyMsLoggingBeanPostProcessor easyMsLoggingRunner() {
        return new EasyMsLoggingBeanPostProcessor();
    }

    @Override
    public void afterPropertiesSet() {
        EasyMsLoggingInitialization.init(easyMsLoggingProperties);
    }
}
