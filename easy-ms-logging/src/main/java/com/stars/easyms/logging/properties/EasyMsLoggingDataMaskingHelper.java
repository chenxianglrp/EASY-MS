package com.stars.easyms.logging.properties;

import java.util.Collections;
import java.util.Map;

/**
 * 数据脱敏帮助类.
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/8/10 6:02 下午
 */
public final class EasyMsLoggingDataMaskingHelper {

    private static boolean enabled;
    
    private static Map<String, EasyMsLoggingProperties.DataMasking.Replace> replaceMap = Collections.emptyMap();

    static void setEnabled(boolean enabled) {
        EasyMsLoggingDataMaskingHelper.enabled = enabled;
    }
    
    static void setReplaceMap(Map<String, EasyMsLoggingProperties.DataMasking.Replace> replaceMap) {
        EasyMsLoggingDataMaskingHelper.replaceMap = replaceMap;
    }
    
    public static boolean isEnabled() {
        return enabled;
    }
    
    public static Map<String, EasyMsLoggingProperties.DataMasking.Replace> getReplaceMap() {
        return replaceMap;
    }
    
    private EasyMsLoggingDataMaskingHelper() {
    }
}
