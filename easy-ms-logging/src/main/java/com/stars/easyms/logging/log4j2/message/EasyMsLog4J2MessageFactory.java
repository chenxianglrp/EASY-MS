package com.stars.easyms.logging.log4j2.message;

import com.stars.easyms.logging.masking.EasyMsLoggingMaskingUtil;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.MessageFactory2;
import org.apache.logging.log4j.message.SimpleMessage;

import java.io.Serializable;

/**
 * EasyMs自定义消息工厂类.
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/8/10 7:30 下午
 */
public class EasyMsLog4J2MessageFactory implements MessageFactory2, Serializable {
    
    @Override
    public Message newMessage(final CharSequence message) {
        return new SimpleMessage(EasyMsLoggingMaskingUtil.masking(String.valueOf(message)));
    }
    
    @Override
    public Message newMessage(final Object message) {
        return new SimpleMessage(EasyMsLoggingMaskingUtil.masking(String.valueOf(message)));
    }
    
    @Override
    public Message newMessage(final String message) {
        return new SimpleMessage(EasyMsLoggingMaskingUtil.masking(message));
    }
    
    @Override
    public Message newMessage(final String message, final Object... params) {
        return new EasyMsParameterizedMessage(message, params);
    }
    
    @Override
    public Message newMessage(final String message, final Object p0) {
        return new EasyMsParameterizedMessage(message, p0);
    }
    
    @Override
    public Message newMessage(final String message, final Object p0, final Object p1) {
        return new EasyMsParameterizedMessage(message, p0, p1);
    }
    
    @Override
    public Message newMessage(final String message, final Object p0, final Object p1, final Object p2) {
        return new EasyMsParameterizedMessage(message, p0, p1, p2);
    }
    
    @Override
    public Message newMessage(final String message, final Object p0, final Object p1, final Object p2,
            final Object p3) {
        return new EasyMsParameterizedMessage(message, p0, p1, p2, p3);
    }
    
    @Override
    public Message newMessage(final String message, final Object p0, final Object p1, final Object p2, final Object p3,
            final Object p4) {
        return new EasyMsParameterizedMessage(message, p0, p1, p2, p3, p4);
    }
    
    @Override
    public Message newMessage(final String message, final Object p0, final Object p1, final Object p2, final Object p3,
            final Object p4, final Object p5) {
        return new EasyMsParameterizedMessage(message, p0, p1, p2, p3, p4, p5);
    }
    
    @Override
    public Message newMessage(final String message, final Object p0, final Object p1, final Object p2, final Object p3,
            final Object p4, final Object p5, final Object p6) {
        return new EasyMsParameterizedMessage(message, p0, p1, p2, p3, p4, p5, p6);
    }
    
    @Override
    public Message newMessage(final String message, final Object p0, final Object p1, final Object p2, final Object p3,
            final Object p4, final Object p5, final Object p6, final Object p7) {
        return new EasyMsParameterizedMessage(message, p0, p1, p2, p3, p4, p5, p6, p7);
    }
    
    @Override
    public Message newMessage(final String message, final Object p0, final Object p1, final Object p2, final Object p3,
            final Object p4, final Object p5, final Object p6, final Object p7, final Object p8) {
        return new EasyMsParameterizedMessage(message, p0, p1, p2, p3, p4, p5, p6, p7, p8);
    }
    
    @Override
    public Message newMessage(final String message, final Object p0, final Object p1, final Object p2, final Object p3,
            final Object p4, final Object p5, final Object p6, final Object p7, final Object p8, final Object p9) {
        return new EasyMsParameterizedMessage(message, p0, p1, p2, p3, p4, p5, p6, p7, p8, p9);
    }
    
}
