package com.stars.easyms.logging.trace;

import com.stars.easyms.base.pointcut.EasyMsAnnotationMatchingPointcut;
import org.aopalliance.aop.Advice;
import org.springframework.aop.Pointcut;
import org.springframework.aop.support.AbstractPointcutAdvisor;
import org.springframework.core.Ordered;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.util.ObjectUtils;

/**
 * <p>className: EasyMsLoggingSchedulingTracePointcutAdvisor</p>
 * <p>description: EasyMsLogging的Scheduling的Trace切面管理类</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/11/17 1:55 下午
 */
public final class EasyMsLoggingSchedulingTracePointcutAdvisor extends AbstractPointcutAdvisor {

    private transient EasyMsLoggingSchedulingTraceAspect advice;

    private transient EasyMsAnnotationMatchingPointcut pointcut;

    public EasyMsLoggingSchedulingTracePointcutAdvisor() {
        this.advice = new EasyMsLoggingSchedulingTraceAspect();
        this.pointcut = new EasyMsAnnotationMatchingPointcut();
        this.pointcut.setMethodMatcher(Scheduled.class, true);
        super.setOrder(Ordered.HIGHEST_PRECEDENCE);
    }

    @Override
    public Pointcut getPointcut() {
        return this.pointcut;
    }

    @Override
    public Advice getAdvice() {
        return this.advice;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || this.getClass() != other.getClass()) {
            return false;
        }
        EasyMsLoggingSchedulingTracePointcutAdvisor otherAdvisor = (EasyMsLoggingSchedulingTracePointcutAdvisor) other;
        return (ObjectUtils.nullSafeEquals(this.advice, otherAdvisor.advice) &&
                ObjectUtils.nullSafeEquals(this.pointcut, otherAdvisor.pointcut));
    }

    @Override
    public int hashCode() {
        return 41 + this.advice.hashCode() * 41 + this.pointcut.hashCode() * 41;
    }

}