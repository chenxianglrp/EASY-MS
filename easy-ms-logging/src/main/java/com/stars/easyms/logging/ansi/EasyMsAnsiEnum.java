package com.stars.easyms.logging.ansi;

import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * <p>className: EasyMsAnsiEnum</p>
 * <p>description: Ansi枚举类</p>
 *
 * @author guoguifang
 * @version 1.7.2
 * @date 2021/1/25 12:01 上午
 */
public enum EasyMsAnsiEnum {

    NORMAL("0"),

    BOLD("1"),

    BRIGHT("1"),

    FAINT("2"),

    DIM("2"),

    ITALIC("3"),

    UNDERLINE("4"),

    BLINK("5"),

    REVERSE("7"),

    HIDDEN("8"),

    BLACK("30"),

    FG_BLACK("30"),

    RED("31"),

    FG_RED("31"),

    GREEN("32"),

    FG_GREEN("32"),

    YELLOW("33"),

    FG_YELLOW("33"),

    BLUE("34"),

    FG_BLUE("34"),

    MAGENTA("35"),

    FG_MAGENTA("35"),

    CYAN("36"),

    FG_CYAN("36"),

    WHITE("37"),

    FG_WHITE("37"),

    DEFAULT("39"),

    FG_DEFAULT("39"),

    BG_BLACK("40"),

    BG_RED("41"),

    BG_GREEN("42"),

    BG_YELLOW("43"),

    BG_BLUE("44"),

    BG_MAGENTA("45"),

    BG_CYAN("46"),

    BG_WHITE("47"),

    BG_DEFAULT("49"),

    BRIGHT_BLACK("90"),

    FG_BRIGHT_BLACK("90"),

    BRIGHT_RED("91"),

    FG_BRIGHT_RED("91"),

    BRIGHT_GREEN("92"),

    FG_BRIGHT_GREEN("92"),

    BRIGHT_YELLOW("93"),

    FG_BRIGHT_YELLOW("93"),

    BRIGHT_BLUE("94"),

    FG_BRIGHT_BLUE("94"),

    BRIGHT_MAGENTA("95"),

    FG_BRIGHT_MAGENTA("95"),

    BRIGHT_CYAN("96"),

    FG_BRIGHT_CYAN("96"),

    BRIGHT_WHITE("97"),

    FG_BRIGHT_WHITE("97"),

    BG_BRIGHT_BLACK("100"),

    BG_BRIGHT_RED("101"),

    BG_BRIGHT_GREEN("102"),

    BG_BRIGHT_YELLOW("103"),

    BG_BRIGHT_BLUE("104"),

    BG_BRIGHT_MAGENTA("105"),

    BG_BRIGHT_CYAN("106"),

    BG_BRIGHT_WHITE("107");

    private final String code;

    EasyMsAnsiEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    private static Map<String, EasyMsAnsiEnum> nameLookForMap;

    static {
        Map<String, EasyMsAnsiEnum> localNameLookForMap = new HashMap<>(32);
        for (EasyMsAnsiEnum easyMsAnsiEnum : values()) {
            localNameLookForMap.put(easyMsAnsiEnum.name().toUpperCase(Locale.ENGLISH), easyMsAnsiEnum);
        }
        nameLookForMap = Collections.unmodifiableMap(localNameLookForMap);
    }

    public static EasyMsAnsiEnum of(String name) {
        if (StringUtils.isBlank(name)) {
            return null;
        }
        return nameLookForMap.get(name.trim().toUpperCase(Locale.ENGLISH));
    }
}
