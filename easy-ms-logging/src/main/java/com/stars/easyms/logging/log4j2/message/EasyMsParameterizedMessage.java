package com.stars.easyms.logging.log4j2.message;

import com.stars.easyms.logging.masking.EasyMsLoggingMaskingUtil;
import org.apache.logging.log4j.message.ParameterizedMessage;

import java.util.stream.Stream;

/**
 * EasyMs自定义ParameterizedMessage.
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/9/2 10:11 上午
 */
final class EasyMsParameterizedMessage extends ParameterizedMessage {
    
    EasyMsParameterizedMessage(String messagePattern, Object... arguments) {
        super(EasyMsLoggingMaskingUtil.masking(messagePattern),
                Stream.of(arguments).map(EasyMsLoggingMaskingUtil::masking).toArray());
    }
    
    EasyMsParameterizedMessage(String messagePattern, Object arg) {
        super(EasyMsLoggingMaskingUtil.masking(messagePattern), EasyMsLoggingMaskingUtil.masking(arg));
    }
    
    EasyMsParameterizedMessage(String messagePattern, Object arg0, Object arg1) {
        super(EasyMsLoggingMaskingUtil.masking(messagePattern), EasyMsLoggingMaskingUtil.masking(arg0),
                EasyMsLoggingMaskingUtil.masking(arg1));
    }
    
}
