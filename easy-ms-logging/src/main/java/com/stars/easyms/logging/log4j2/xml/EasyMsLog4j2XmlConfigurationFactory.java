package com.stars.easyms.logging.log4j2.xml;

import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.ConfigurationFactory;
import org.apache.logging.log4j.core.config.ConfigurationSource;
import org.apache.logging.log4j.core.config.Order;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.xml.XmlConfigurationFactory;

/**
 * <p>className: EasyMsLog4j2XmlConfigurationFactory</p>
 * <p>description: EasyMs的Log4j2的xml配置工厂类</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/12/2 1:51 下午
 */
@Plugin(name = "EasyMsLog4j2XmlConfigurationFactory", category = ConfigurationFactory.CATEGORY)
@Order(6)
public class EasyMsLog4j2XmlConfigurationFactory extends ConfigurationFactory {

    @Override
    protected String[] getSupportedTypes() {
        return XmlConfigurationFactory.SUFFIXES;
    }

    @Override
    public Configuration getConfiguration(LoggerContext loggerContext, ConfigurationSource source) {
        return new EasyMsLog4j2XmlConfiguration(loggerContext, source);
    }
}
