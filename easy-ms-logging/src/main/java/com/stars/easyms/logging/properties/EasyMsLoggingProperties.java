package com.stars.easyms.logging.properties;

import com.stars.easyms.base.constant.EasyMsCommonConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * <p>className: EasyMsLoggingProperties</p>
 * <p>description: EasyMsLogging属性类</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/11/17 2:49 下午
 */
@Data
@ConfigurationProperties(prefix = EasyMsLoggingProperties.PREFIX)
public class EasyMsLoggingProperties {

    public static final String PREFIX = EasyMsCommonConstants.EASY_MS_PROPERTIES_PREFIX + "logging";
    
    /**
     * 日志颜色：日志按级别区分颜色打印
     */
    private final LogColor logColor = new LogColor();
    
    private final PlumeLog plumeLog = new PlumeLog();
    
    /**
     * 敏感数据脱敏
     */
    private final DataMasking dataMasking = new DataMasking();

    @Data
    public static class LogColor {
    
        /**
         * 是否激活日志的颜色，如果不配置默认是激活的true
         */
        private boolean enabled = true;
    
        /**
         * 配置日志级别对应的颜色，颜色可选项有：类型、字体颜色、背景颜色，eg: trace: FAINT,CYAN;warn: bold BRIGHT_YELLOW
         */
        private Map<String, String> level = new HashMap<>(8);

    }

    @Data
    public static class PlumeLog {
    
        /**
         * 是否激活plumeLog（即是否发送日志到plumeLog服务端），如果不配置默认是激活的true
         */
        private boolean enabled = true;
    
        /**
         * 项目名称，如果不配置默认使用spring.application.name作为项目名称
         */
        private String appName;
    
        /**
         * 应用环境，如果不配置默认使用spring.profile.active
         */
        private String env;

        private String runModel = "1";

        private String expand;

        private int maxCount = 500;

        private int logQueueSize = 10000;

        private int threadPoolSize = 5;
        
        private boolean compressor;

        private Redis redis = new Redis();

        private Kafka kafka = new Kafka();

        @Data
        public static class Redis {

            private String host;

            private String port = "6379";

            private String auth;

            private int db = 0;
    
            /**
             * redis模式：空/standalone:单例，cluster:集群，sentinel:哨兵
             */
            private String model;

            private String masterName;

        }

        @Data
        public static class Kafka {

            private String hosts;

        }

    }
    
    @Data
    public static class DataMasking {
    
        /**
         * 是否激活数据脱敏，如果不配置默认是true
         */
        private boolean enabled = true;
    
        /**
         * 脱敏规则:key为规则类型,如mobile、email、idCard等
         */
        private Map<String, Replace> replace = new LinkedHashMap<>();
        
        @Data
        public static class Replace {
    
            /**
             * 是否激活该替换规则,默认激活
             */
            private boolean enabled = true;
    
            /**
             * 需要替换的正则表达式
             */
            private String regex;
    
            /**
             * 需要替换的字符,默认为***
             */
            private String replacement;
        }
        
    }
}
