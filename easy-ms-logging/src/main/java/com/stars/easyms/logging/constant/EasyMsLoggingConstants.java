package com.stars.easyms.logging.constant;

import com.stars.easyms.logging.ansi.EasyMsAnsiEnum;
import com.stars.easyms.logging.properties.EasyMsLoggingProperties;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * <p>className: EasyMsLoggingConstants</p>
 * <p>description: EasyMsLogging常量类</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/11/16 6:14 下午
 */
public final class EasyMsLoggingConstants {
    
    public static final String MODULE_NAME = "logging";
    
    public static final String EASY_MS_PLUME_LOG_APPENDER_NAME = "easyMsPlumeLogAppender";
    
    public static final String PLUME_LOG_REDIS_APPENDER_NAME = "plumeLogRedisAppender";
    
    public static final String PLUME_LOG_KAFKA_APPENDER_NAME = "plumeLogKafkaAppender";
    
    public static final String ELEMENTS_NAME_APPENDERS = "Appenders";
    
    public static final String ELEMENTS_NAME_ROOT = "root";
    
    public static final String PLUME_LOG_METHOD_ENTRY_POSITION = "<";
    
    public static final String PLUME_LOG_METHOD_EXIT_POSITION = ">";
    
    public static final String FILE_PROTOCOL = "file";
    
    public static final Map<String, String> DEFAULT_LEVEL_COLOR_MAP;
    
    static {
        Map<String, String> localLevelColorMap = new HashMap<>(8);
        localLevelColorMap.put("TRACE", "7a7a7a, bg_2b2b2b");
        localLevelColorMap.put("DEBUG", "7a7a7a, bg_2b2b2b");
        localLevelColorMap.put("INFO", "92c584, bg_2b2b2b");
        localLevelColorMap.put("WARN", "e5e431, bg_2b2b2b");
        localLevelColorMap.put("ERROR", "fc5759, bg_2b2b2b");
        localLevelColorMap.put("FATAL", "fc5759, bg_2b2b2b");
        DEFAULT_LEVEL_COLOR_MAP = Collections.unmodifiableMap(localLevelColorMap);
    }
    
    public static final String ENCODE_JOIN = ";";
    
    public static final String ENCODE_START = "\033[";
    
    public static final String ENCODE_END = "m";
    
    public static final String RESET =
            ENCODE_START + EasyMsAnsiEnum.NORMAL.getCode() + ENCODE_JOIN + EasyMsAnsiEnum.DEFAULT.getCode()
                    + ENCODE_END;
    
    public static final String DEFAULT_DATA_MASKING_REPLACEMENT = "***";
    
    public static final Map<String, EasyMsLoggingProperties.DataMasking.Replace> DEFAULT_DATA_MASKING_REPLACE_MAP;
    
    static {
        Map<String, EasyMsLoggingProperties.DataMasking.Replace> localDataMaskingReplaceMap = new LinkedHashMap<>(8);
        EasyMsLoggingProperties.DataMasking.Replace emailReplace = new EasyMsLoggingProperties.DataMasking.Replace();
        emailReplace.setEnabled(true);
        emailReplace.setRegex("(\\w*)\\w{2}@\\w*\\.(\\w*)");
        emailReplace.setReplacement("$1****@****.$2");
        localDataMaskingReplaceMap.put("EMAIL", emailReplace);
        
        EasyMsLoggingProperties.DataMasking.Replace idCardReplace = new EasyMsLoggingProperties.DataMasking.Replace();
        idCardReplace.setEnabled(true);
        idCardReplace.setRegex(
                "(\\w*)(\\D+)([1-9]\\d{3})\\d{2}(19|20)\\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)(\\d{3})([0-9Xx])(\\D+)(\\w*)");
        idCardReplace.setReplacement("$1$2$3**********$10$11$12$13");
        localDataMaskingReplaceMap.put("IDCARD", idCardReplace);
        
        EasyMsLoggingProperties.DataMasking.Replace mobileReplace = new EasyMsLoggingProperties.DataMasking.Replace();
        mobileReplace.setEnabled(true);
        mobileReplace.setRegex("(\\w*)(\\D+)(\\+?0?86\\-?)?1([3-9]\\d)\\d{4}(\\d{4})(\\D+)(\\w*)");
        mobileReplace.setReplacement("$1$2$31$4****$5$6$7");
        localDataMaskingReplaceMap.put("MOBILE", mobileReplace);
        DEFAULT_DATA_MASKING_REPLACE_MAP = Collections.unmodifiableMap(localDataMaskingReplaceMap);
    }
    
    private EasyMsLoggingConstants() {
    }
}
