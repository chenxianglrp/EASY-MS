package com.stars.easyms.proxy;

import com.stars.easyms.base.interceptor.EasyMsJdkMethodInterceptor;

import java.lang.reflect.Method;

/**
 * <p>className: EasyMsProxyJdkMethodInterceptor</p>
 * <p>description: EasyMs代理方法拦截器</p>
 *
 * @author guoguifang
 * @version 1.7.3
 * @date 2021/3/24 10:30 上午
 */
class EasyMsProxyJdkMethodInterceptor extends BaseEasyMsProxyMethodInterceptor implements EasyMsJdkMethodInterceptor {

    private Class<?> type;

    EasyMsProxyJdkMethodInterceptor(Class<?> type) {
        this.type = type;
    }

    @Override
    public Object intercept(Object obj, Method method, Object[] args) {
        return super.intercept(obj, method, args);
    }

    @Override
    public Class<?> getInterfaceType() {
        return type;
    }

}
