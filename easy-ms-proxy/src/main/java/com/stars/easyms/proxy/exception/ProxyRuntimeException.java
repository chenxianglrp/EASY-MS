package com.stars.easyms.proxy.exception;

import com.stars.easyms.base.util.ExceptionUtil;
import com.stars.easyms.base.util.MessageFormatUtil;

/**
 * <p>className: ProxyRuntimeException</p>
 * <p>description: EasyMs代理运行时异常</p>
 *
 * @author guoguifang
 * @version 1.7.3
 * @date 2021/3/24 11:16 上午
 */
public final class ProxyRuntimeException extends RuntimeException {

    private static final long serialVersionUID = -3034893230245866632L;

    public ProxyRuntimeException(String message, Object... args) {
        super(MessageFormatUtil.format(message, ExceptionUtil.trimmedCopy(args)), ExceptionUtil.extractThrowable(args));
    }
}
