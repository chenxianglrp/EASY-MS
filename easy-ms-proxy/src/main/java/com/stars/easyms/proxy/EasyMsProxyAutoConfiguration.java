package com.stars.easyms.proxy;

import com.stars.easyms.dubbo.autoconfigure.EasyMsDubboAutoConfiguration;
import com.stars.easyms.feign.autoconfigure.EasyMsFeignAutoConfiguration;
import com.stars.easyms.proxy.client.dubbo.DubboProxyClient;
import com.stars.easyms.proxy.client.rest.RestProxyClient;
import com.stars.easyms.proxy.feign.ProxyClientFeign;
import feign.Client;
import feign.Feign;
import feign.RequestInterceptor;
import feign.Retryer;
import feign.codec.Decoder;
import feign.codec.Encoder;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.Ordered;

import java.util.List;
import java.util.Objects;

/**
 * <p>className: EasyMsProxyAutoConfiguration</p>
 * <p>description: EasyMsProxy自动配置类</p>
 *
 * @author guoguifang
 * @version 1.7.3
 * @date 2021/3/5 11:14 上午
 */
@Configuration
@EnableConfigurationProperties(EasyMsProxyProperties.class)
@Import(EasyMsProxyServiceRegistrar.class)
@AutoConfigureOrder(Ordered.HIGHEST_PRECEDENCE + 1)
class EasyMsProxyAutoConfiguration {

    private EasyMsProxyProperties easyMsProxyProperties;

    public EasyMsProxyAutoConfiguration(EasyMsProxyProperties easyMsProxyProperties) {
        this.easyMsProxyProperties = easyMsProxyProperties;
    }

    @Configuration
    @ConditionalOnClass(EasyMsDubboAutoConfiguration.class)
    public class DubboProxyClientAutoConfiguration {

        @Bean
        public DubboProxyClient easyMsDubboProxyClient() {
            return new DubboProxyClient();
        }

    }

    @Configuration
    @ConditionalOnClass(EasyMsFeignAutoConfiguration.class)
    @ConditionalOnProperty(prefix = EasyMsProxyProperties.PREFIX, name = "serverUrl")
    public class RestProxyClientAutoConfiguration {

        @Bean
        public ProxyClientFeign easyMsProxyClientFeign(ObjectProvider<Decoder> decoderObjectProvider,
                                                       ObjectProvider<Encoder> encoderObjectProvider,
                                                       ObjectProvider<Client> clientObjectProvider,
                                                       ObjectProvider<List<RequestInterceptor>> requestInterceptorsObjectProvider) {
            Feign.Builder feignBuilder = Feign.builder()
                    .encoder(encoderObjectProvider.getObject())
                    .decoder(decoderObjectProvider.getObject())
                    .requestInterceptors(Objects.requireNonNull(requestInterceptorsObjectProvider.getIfAvailable()))
                    .contract(new SpringMvcContract())
                    .retryer(Retryer.NEVER_RETRY);
            String serverUrl = easyMsProxyProperties.getServerUrl();
            // 如果服务器url开头是http则不使用loadBalance模式
            if (!serverUrl.startsWith("http")) {
                if (serverUrl.startsWith("lb://")) {
                    serverUrl = serverUrl.replace("lb://", "http://");
                } else {
                    serverUrl = "http://" + serverUrl;
                }
                Client client = clientObjectProvider.getIfAvailable();
                if (client != null) {
                    feignBuilder.client(client);
                }
            }
            return feignBuilder.target(ProxyClientFeign.class, serverUrl);
        }

        @Bean
        public RestProxyClient easyMsRestProxyClient(ProxyClientFeign easyMsProxyClientFeign) {
            return new RestProxyClient(easyMsProxyClientFeign);
        }

    }

}
