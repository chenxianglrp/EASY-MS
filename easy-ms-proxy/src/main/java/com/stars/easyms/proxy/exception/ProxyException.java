package com.stars.easyms.proxy.exception;

import com.stars.easyms.base.util.ExceptionUtil;
import com.stars.easyms.base.util.MessageFormatUtil;

/**
 * <p>className: ProxyException</p>
 * <p>description: EasyMs代理异常</p>
 *
 * @author guoguifang
 * @version 1.7.3
 * @date 2021/3/24 11:16 上午
 */
public final class ProxyException extends Exception {

    private static final long serialVersionUID = -3134893230245866632L;

    public ProxyException(String message, Object... args) {
        super(MessageFormatUtil.format(message, ExceptionUtil.trimmedCopy(args)), ExceptionUtil.extractThrowable(args));
    }
}
