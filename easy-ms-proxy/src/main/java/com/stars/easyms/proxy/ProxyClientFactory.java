package com.stars.easyms.proxy;

import com.stars.easyms.base.bean.LazyLoadBean;
import com.stars.easyms.base.util.ApplicationContextHolder;
import com.stars.easyms.proxy.client.ProxyClient;
import com.stars.easyms.proxy.client.dubbo.DubboProxyClient;
import com.stars.easyms.proxy.client.local.LocalProxyClient;
import com.stars.easyms.proxy.client.mq.MqProxyClient;
import com.stars.easyms.proxy.client.rest.RestProxyClient;
import com.stars.easyms.proxy.mq.ProxyClientMqManager;
import org.apache.commons.lang3.StringUtils;

/**
 * <p>className: ProxyClientFactory</p>
 * <p>description: Proxy客户端工厂类</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/31 10:25 上午
 */
class ProxyClientFactory {

    private static final LazyLoadBean<EasyMsProxyProperties> EASY_MS_PROXY_PROPERTIES = new LazyLoadBean<>(EasyMsProxyProperties.class);

    private static final LazyLoadBean<RestProxyClient> REST_PROXY_CLIENT = new LazyLoadBean<>(RestProxyClient.class);

    private static final LazyLoadBean<DubboProxyClient> DUBBO_PROXY_CLIENT = new LazyLoadBean<>(DubboProxyClient.class);

    private static final LazyLoadBean<MqProxyClient> MQ_PROXY_CLIENT = new LazyLoadBean<>(ProxyClientFactory::getMqProxyClient);

    static ProxyClient getProxyClient(EasyMsProxyInfo easyMsProxyInfo) {
        ProxyType proxyType = easyMsProxyInfo.getProxyType();
        ProxyClient proxyClient;
        switch (proxyType) {
            case LOCAL:
                proxyClient = getLocalProxyClient(easyMsProxyInfo);
                if (proxyClient != null) {
                    break;
                }
                proxyType = ProxyType.REST;
            case REST:
                proxyClient = REST_PROXY_CLIENT.getBean();
                if (proxyClient != null) {
                    break;
                }
                proxyType = ProxyType.DUBBO;
            case DUBBO:
                proxyClient = DUBBO_PROXY_CLIENT.getBean();
                if (proxyClient != null) {
                    break;
                }
                proxyType = ProxyType.MQ;
            default:
                proxyClient = MQ_PROXY_CLIENT.getBean();
        }
        if (proxyClient != null && easyMsProxyInfo.getProxyType() != proxyType) {
            easyMsProxyInfo.setProxyType(proxyType);
        }
        return proxyClient;
    }

    private static LocalProxyClient getLocalProxyClient(EasyMsProxyInfo easyMsProxyInfo) {
        EasyMsProxyProperties easyMsProxyProperties = EASY_MS_PROXY_PROPERTIES.getNonNullBean();
        String targetCode = easyMsProxyInfo.getTarget();
        EasyMsProxyProperties.Target target = StringUtils.isBlank(targetCode) ? easyMsProxyProperties.getTarget() :
                easyMsProxyProperties.getTargets().get(targetCode);
        if (target != null && StringUtils.isNotBlank(target.getAddr())) {
            return new LocalProxyClient();
        }
        return null;
    }

    private static MqProxyClient getMqProxyClient() {
        ProxyClientMqManager proxyClientMqManager = ApplicationContextHolder.getBean(ProxyClientMqManager.class);
        if (proxyClientMqManager != null) {
            return new MqProxyClient(proxyClientMqManager);
        }
        return null;
    }
}
