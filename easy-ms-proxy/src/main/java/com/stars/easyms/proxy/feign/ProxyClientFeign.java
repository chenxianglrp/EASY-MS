package com.stars.easyms.proxy.feign;

import com.stars.easyms.proxy.constant.ProxyConstants;
import com.stars.easyms.proxy.dto.ProxyDTO;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * <p>className: ProxyClientFeign</p>
 * <p>description: 代理客户端feign调用服务端</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/31 5:25 下午
 */
public interface ProxyClientFeign {

    @PostMapping(path = ProxyConstants.PROXY_REST_URL)
    String proxy(ProxyDTO proxyDTO);

}
