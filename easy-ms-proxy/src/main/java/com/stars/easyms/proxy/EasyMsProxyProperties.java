package com.stars.easyms.proxy;

import com.stars.easyms.base.constant.EasyMsCommonConstants;
import com.stars.easyms.base.enums.EncryptTypeEnum;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * <p>className: EasyMsRestProxyProperties</p>
 * <p>description: EasyMsRest代理属性类</p>
 *
 * @author guoguifang
 * @version 1.7.3
 * @date 2021/3/5 11:33 上午
 */
@Data
@ConfigurationProperties(prefix = EasyMsProxyProperties.PREFIX)
public class EasyMsProxyProperties {

    public static final String PREFIX = EasyMsCommonConstants.EASY_MS_PROPERTIES_PREFIX + "proxy";

    /**
     * 如果需要调用远程的代理微服务，则需要配置远程微服务的url:
     * 1. lb(注册中心中服务名字)方式 eg: lb://test-demo
     * 2. http方式或https eg: http://localhost:8080/
     */
    private String serverUrl;

    /**
     * Scanning Package Path, multiple split using ','
     */
    private Set<String> basePackage;

    /**
     * 代理的名称
     */
    private String name;

    /**
     * 默认的目标配置
     */
    private Target target = new Target();

    /**
     * 多个目标时的配置
     */
    private Map<String, Target> targets = new HashMap<>(8);

    @Data
    public static class Target {

        /**
         * 目标的地址,不可为空
         */
        private String addr;

        /**
         * 加密信息
         */
        private Encrypt encrypt = new Encrypt();

    }

    @Data
    public static class Encrypt {

        /**
         * 是否开启加密，默认开启
         */
        private boolean enabled = true;

        /**
         * 加密方式: symmetric/asymmetric，默认对称加密
         */
        private EncryptTypeEnum type = EncryptTypeEnum.SYMMETRIC;

        /**
         * 对称加密
         */
        private Symmetric symmetric = new Symmetric();

        /**
         * 非对称加密
         */
        private Asymmetric asymmetric = new Asymmetric();
    }

    @Data
    public static class Symmetric {

        /**
         * 对称加密的秘钥
         */
        private String secret;

        /**
         * 对称加密的秘钥偏移量
         */
        private String iv;

        /**
         * 加密json中已加密的请求值
         */
        private String key = EasyMsCommonConstants.DEFAULT_ENCRYPT_KEY;

    }

    @Data
    public static class Asymmetric {

        private String publicKey;

        private String privateKey;
    }

}
