package com.stars.easyms.proxy.client.mq;

import com.stars.easyms.proxy.client.ProxyClient;
import com.stars.easyms.proxy.constant.ProxyConstants;
import com.stars.easyms.proxy.dto.ProxyDTO;
import com.stars.easyms.proxy.mq.ProxyClientMqManager;

/**
 * <p>className: MqProxyClient</p>
 * <p>description: MQ方式的代理客户端实现</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/31 3:38 下午
 */
public class MqProxyClient implements ProxyClient {

    private final ProxyClientMqManager proxyClientMqManager;

    public MqProxyClient(ProxyClientMqManager proxyClientMqManager) {
        this.proxyClientMqManager = proxyClientMqManager;
    }

    @Override
    public String proxy(ProxyDTO proxyDTO) {
        proxyClientMqManager.proxy(proxyDTO);
        return ProxyConstants.PROXY_MQ_RETURN_SUCCESS;
    }

}
