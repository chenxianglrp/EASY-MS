package com.stars.easyms.proxy;

import com.stars.easyms.base.interceptor.EasyMsCglibMethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * <p>className: EasyMsProxyMethodInterceptor</p>
 * <p>description: EasyMs代理方法拦截器</p>
 *
 * @author guoguifang
 * @version 1.7.3
 * @date 2021/3/24 10:30 上午
 */
class EasyMsProxyCglibMethodInterceptor extends BaseEasyMsProxyMethodInterceptor implements EasyMsCglibMethodInterceptor {

    private Class<?> type;

    EasyMsProxyCglibMethodInterceptor(Class<?> type) {
        this.type = type;
    }

    @Override
    public Object intercept(MethodProxy methodProxy, Object proxy, Method method, Object[] args) throws Throwable {
        return super.intercept(proxy, method, args);
    }

    @Override
    public Class<?> getSuperType() {
        return type;
    }

}
