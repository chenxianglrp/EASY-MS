package com.stars.easyms.proxy.constant;

/**
 * <p>className: ProxyConstants</p>
 * <p>description: 代理模块常量类</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/31 3:51 下午
 */
public class ProxyConstants {

    public static final String PROXY_MQ_KEY = "easy-ms-proxy-mq-key";

    public static final String PROXY_MQ_RETURN_SUCCESS = "easy-ms-proxy:mq:success";

    public static final String PROXY_REST_URL = "/easy-ms-proxy/rest";

}
