package com.stars.easyms.proxy.client;

import com.stars.easyms.proxy.dto.ProxyDTO;

/**
 * <p>className: ProxyClient</p>
 * <p>description: 代理客户端接口</p>
 *
 * @author guoguifang
 * @version 1.7.3
 * @date 2021/3/5 11:30 上午
 */
public interface ProxyClient {

    String proxy(ProxyDTO proxyDTO);

}
