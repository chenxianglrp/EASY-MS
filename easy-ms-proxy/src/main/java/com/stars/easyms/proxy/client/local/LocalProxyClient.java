package com.stars.easyms.proxy.client.local;

import com.stars.easyms.base.bean.LazyLoadBean;
import com.stars.easyms.base.constant.HttpHeaderConstants;
import com.stars.easyms.base.encrypt.EasyMsEncrypt;
import com.stars.easyms.base.enums.EncryptTypeEnum;
import com.stars.easyms.proxy.EasyMsProxyProperties;
import com.stars.easyms.proxy.dto.ProxyDTO;
import com.stars.easyms.proxy.exception.ProxyRuntimeException;
import com.stars.easyms.proxy.client.ProxyClient;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 * <p>className: LocalProxyClient</p>
 * <p>description: 本地代理客户端</p>
 *
 * @author guoguifang
 * @version 1.7.3
 * @date 2021/3/5 11:30 上午
 */
public class LocalProxyClient implements ProxyClient {

    private final LazyLoadBean<EasyMsProxyProperties> easyMsProxyProperties = new LazyLoadBean<>(EasyMsProxyProperties.class);

    private final LazyLoadBean<RestTemplate> restTemplate = new LazyLoadBean<>(RestTemplate.class);

    @Override
    public String proxy(ProxyDTO proxyDTO) {

        // 获取代理的path
        String path = proxyDTO.getPath();
        if (StringUtils.isBlank(path)) {
            throw new ProxyRuntimeException("代理目标path不能为空!");
        }

        // 获取代理目标信息的code并根据code获取详细的目标信息
        String targetCode = proxyDTO.getTargetCode();
        EasyMsProxyProperties.Target target = getTarget(targetCode);

        // 获取代理的目标url，如果自定义了使用自定义的，否则使用已配置好的目标信息url，将url+path合并为详细的url连接地址
        String url = proxyDTO.getUrl();
        url = formatUrl(StringUtils.isBlank(url) ? target.getAddr() : url, path);

        // 获取请求参数并加密
        String arg = proxyDTO.getArg();
        Object requestObj = getEncryptRequestObj(target, arg);

        // 在HttpHeaders里设置request-source信息
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaderConstants.HEADER_KEY_REQUEST_SOURCE, easyMsProxyProperties.getNonNullBean().getName());

        // 使用post方式请求，如果返回的http结果状态不是200则抛出异常
        HttpEntity requestEntity = new HttpEntity<>(requestObj, headers);
        ResponseEntity<String> responseEntity = restTemplate.getNonNullBean().postForEntity(url, requestEntity, String.class);
        if (responseEntity.getStatusCode() != HttpStatus.OK) {
            throw new ProxyRuntimeException(responseEntity.getStatusCode().getReasonPhrase());
        }

        // 将返回的结果进行解密，返回的result为EasyMsResponseEntity格式
        return getDecodeResponseObj(target, responseEntity.getBody());
    }

    private Object getEncryptRequestObj(EasyMsProxyProperties.Target target, Object requestObj) {
        EasyMsProxyProperties.Encrypt encrypt = target.getEncrypt();
        if (encrypt.isEnabled()) {
            if (EncryptTypeEnum.SYMMETRIC == encrypt.getType()) {
                EasyMsProxyProperties.Symmetric symmetric = encrypt.getSymmetric();
                requestObj = EasyMsEncrypt.encrypt(requestObj, symmetric.getSecret(), symmetric.getIv(), symmetric.getKey());
            }
            // todo:非对称加密
        }
        return requestObj;
    }

    private String getDecodeResponseObj(EasyMsProxyProperties.Target target, String responseObj) {
        EasyMsProxyProperties.Encrypt encrypt = target.getEncrypt();
        if (encrypt.isEnabled()) {
            if (EncryptTypeEnum.SYMMETRIC == encrypt.getType()) {
                EasyMsProxyProperties.Symmetric symmetric = encrypt.getSymmetric();
                return EasyMsEncrypt.decode(responseObj, symmetric.getSecret(), symmetric.getIv(), symmetric.getKey());
            }
            // todo:非对称加密
        }
        return responseObj;
    }

    private EasyMsProxyProperties.Target getTarget(String code) {
        if (StringUtils.isBlank(code)) {
            return easyMsProxyProperties.getNonNullBean().getTarget();
        }
        EasyMsProxyProperties.Target target = easyMsProxyProperties.getNonNullBean().getTargets().get(code);
        if (target == null) {
            throw new ProxyRuntimeException("根据targetCode({}})找不到对应的target!", code);
        }
        return target;
    }

    private String formatUrl(String addr, String path) {
        if (StringUtils.isBlank(addr)) {
            return path;
        }
        while (addr.endsWith("/")) {
            addr = addr.substring(0, addr.length() - 1);
        }
        return addr + (path.startsWith("/") ? "" : "/") + path;
    }
}
