package com.stars.easyms.proxy.annotation;

import org.springframework.core.annotation.AliasFor;
import org.springframework.stereotype.Service;

import java.lang.annotation.*;

/**
 * <p>className: EasyMsProxyService</p>
 * <p>description: 提供代理服务的类注解</p>
 *
 * @author guoguifang
 * @version 1.7.3
 * @date 2021/3/10 8:00 下午
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Service
public @interface EasyMsProxyService {

    /**
     * bean名称
     */
    @AliasFor(annotation = Service.class)
    String value() default "";

    /**
     * 代理目标系统，在该类中的所有方法级映射都继承
     */
    String target() default "";

    /**
     * 绝对路径URL或可解析的主机名:在该类中的所有方法级映射都继承这个主映射
     */
    String url() default "";

    /**
     * 调用地址:在该类中的所有方法级映射都继承这个主映射
     */
    String path() default "";
}
