package com.stars.easyms.proxy.client.rest;

import com.stars.easyms.proxy.client.ProxyClient;
import com.stars.easyms.proxy.dto.ProxyDTO;
import com.stars.easyms.proxy.feign.ProxyClientFeign;

/**
 * <p>className: RestProxyClient</p>
 * <p>description: Rest方式的代理客户端</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/31 4:18 下午
 */
public class RestProxyClient implements ProxyClient {

    private final ProxyClientFeign proxyClientFeign;

    public RestProxyClient(ProxyClientFeign proxyClientFeign) {
        this.proxyClientFeign = proxyClientFeign;
    }

    @Override
    public String proxy(ProxyDTO proxyDTO) {
        return proxyClientFeign.proxy(proxyDTO);
    }

}
