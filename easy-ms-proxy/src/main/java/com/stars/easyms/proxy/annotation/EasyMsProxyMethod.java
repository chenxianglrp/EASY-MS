package com.stars.easyms.proxy.annotation;

import com.stars.easyms.proxy.ProxyType;

import java.lang.annotation.*;

/**
 * <p>className: EasyMsProxyMethod</p>
 * <p>description: 提供代理服务的方法注解</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/26 5:53 下午
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface EasyMsProxyMethod {

    /**
     * 调用地址
     */
    String path() default "";

    /**
     * 代理类型：默认为MQ
     */
    ProxyType proxyType() default ProxyType.LOCAL;

}
