package com.stars.easyms.proxy.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>className: ProxyDTO</p>
 * <p>description: 代理DTO</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/31 3:16 下午
 */
@Data
public class ProxyDTO implements Serializable {

    private String targetCode;

    private String url;

    private String path;

    private String arg;

    public static ProxyDTO of(String targetCode, String url, String path, String arg) {
        ProxyDTO proxyDTO = new ProxyDTO();
        proxyDTO.setTargetCode(targetCode);
        proxyDTO.setUrl(url);
        proxyDTO.setPath(path);
        proxyDTO.setArg(arg);
        return proxyDTO;
    }

}
