package com.stars.easyms.proxy.client.dubbo;

import com.stars.easyms.proxy.client.ProxyClient;
import com.stars.easyms.proxy.dto.ProxyDTO;
import com.stars.easyms.proxy.dubbo.ProxyClientDubboFacade;
import org.apache.dubbo.config.annotation.DubboReference;

/**
 * <p>className: DubboProxyClient</p>
 * <p>description: 使用Dubbo客户端模式调用远程服务器</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/31 10:44 上午
 */
public class DubboProxyClient implements ProxyClient {

    @DubboReference
    private ProxyClientDubboFacade proxyClientDubboFacade;

    @Override
    public String proxy(ProxyDTO proxyDTO) {
        return proxyClientDubboFacade.proxy(proxyDTO);
    }

}
