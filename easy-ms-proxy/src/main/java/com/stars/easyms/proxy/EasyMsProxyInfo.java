package com.stars.easyms.proxy;

import lombok.Data;

import java.lang.reflect.Parameter;
import java.lang.reflect.Type;

/**
 * <p>className: EasyMsProxyInfo</p>
 * <p>description: EasyMs代理信息类</p>
 *
 * @author guoguifang
 * @version 1.7.3
 * @date 2021/3/25 2:42 下午
 */
@Data
class EasyMsProxyInfo {

    private Parameter[] methodParameters;

    private Type returnType;

    private String target;

    private String url;

    private String path;

    private ProxyType proxyType;

}
