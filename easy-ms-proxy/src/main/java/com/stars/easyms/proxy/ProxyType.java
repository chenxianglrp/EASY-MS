package com.stars.easyms.proxy;

/**
 * <p>className: ProxyType</p>
 * <p>description: 代理类型：MQ、Dubbo、Rest</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/26 7:29 下午
 */
public enum ProxyType {

    LOCAL, MQ, DUBBO, REST;
}
