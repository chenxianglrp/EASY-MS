package com.stars.easyms.proxy.dubbo;

import com.stars.easyms.proxy.dto.ProxyDTO;

/**
 * <p>className: ProxyClientDubboFacade</p>
 * <p>description: 代理dubbo接口</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/31 4:14 下午
 */
public interface ProxyClientDubboFacade {

    String proxy(ProxyDTO proxyDTO);

}
