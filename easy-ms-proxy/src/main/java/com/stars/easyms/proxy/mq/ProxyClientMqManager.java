package com.stars.easyms.proxy.mq;

import com.stars.easyms.mq.annotation.MQManager;
import com.stars.easyms.mq.annotation.MQSender;
import com.stars.easyms.proxy.constant.ProxyConstants;
import com.stars.easyms.proxy.dto.ProxyDTO;

/**
 * <p>className: ProxyClientMqManager</p>
 * <p>description: 代理MQ管理类</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/31 3:18 下午
 */
@MQManager
public interface ProxyClientMqManager {

    @MQSender(key = ProxyConstants.PROXY_MQ_KEY)
    void proxy(ProxyDTO proxyDTO);

}
