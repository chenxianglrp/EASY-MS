package com.stars.easyms.ruoyi.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * <p>className: EasyMsRuoYiAutoConfiguration</p>
 * <p>description: EasyMs 若依自动配置类</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/21 3:39 下午
 */
@Configuration
@ComponentScan(basePackages = "com.stars.easyms.ruoyi")
public class EasyMsRuoYiAutoConfiguration {
}
