package com.stars.easyms.ruoyi.config;

import com.stars.easyms.datasource.scan.EasyMsDataSourceMapperScan;

/**
 * <p>className: EasyMsRuoYiMapperScan</p>
 * <p>description: EasyMs若依mapper扫描配置</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/21 5:43 下午
 */
public class EasyMsRuoYiMapperScan implements EasyMsDataSourceMapperScan {

    @Override
    public String typeAliasesPackage() {
        return "com.stars.easyms.ruoyi.**.domain";
    }
}
