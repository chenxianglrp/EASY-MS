# Easy-Ms-Rest

　　easy-ms-rest是一个基于spring的、标准化接口开发的功能增强型框架，是Easy-Ms的一个组件框架，**但也支持在非Easy-Ms项目的Spring Boot项目中使用**。

## 一、 功能特点

1. 标准化rest接口，开发更简单，代码更整洁；
2. 支持接口全局是否加密、单接口是否加密两种加密方式；
3. 支持阿里巴巴sentinel的接口限流，可返回接口
4. 支持自定义处理返回类；
5. 支持接口分布式锁（需依赖easy-ms-redis）；
6. 优化request和response处理链路，拥有更好的性能；
7. 集成swagger，重写swagger-ui，具有比swagger-ui更丰富的功能；
8. 支持在线调试，调试结果更方便、更快捷；

## 二、 依赖引入

#### 1. 使用parent的方式

　　好处：easy-ms-parent里包含了许多的依赖版本管理，可以在依赖管理`<dependencies>`中，有较多常用的引入直接使用默认的版本而无需指定版本号

```xml
pom.xml:
    <!-- 可以直接把easy-ms-parent当做parent，或者当引入多个easy-ms组件时使用parent可以只需设置一次版本号 -->
    <parent>
        <groupId>com.stars.easyms</groupId>
        <artifactId>easy-ms-parent</artifactId>
        <version>${easy-ms.version}</version>
    </parent>
		
    <!-- 如果是直接引入easy-ms，则不需要引入easy-ms-rest，easy-ms已经默认依赖了easy-ms-rest -->
    <dependencies>
        <dependency>
            <groupId>com.stars.easyms</groupId>
            <artifactId>easy-ms-rest</artifactId>
        </dependency>
    </dependencies>
```

#### 2. 直接引用

```xml
    <!-- 如果是直接引入easy-ms，则不需要引入easy-ms-rest，easy-ms已经默认依赖了easy-ms-rest -->
    <dependencies>
        <dependency>
            <groupId>com.stars.easyms</groupId>
            <artifactId>easy-ms-rest</artifactId>
            <version>${easy-ms.version}</version>
        </dependency>
    </dependencies>
```

> 　　我们提供框架的维护和技术支持，提供给感兴趣的开发人员学习和交流的平台，同时希望感兴趣的同学一起加入我们让框架更完善，让我们一起为了给大家提供更好更优秀的框架而努力。
>
> 　　若该框架及本文档中有什么错误及问题的，可采用创建Issue、加群@管理、私聊作者或管理员的方式进行联系，谢谢！