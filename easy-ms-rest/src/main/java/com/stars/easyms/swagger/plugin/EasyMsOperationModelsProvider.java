package com.stars.easyms.swagger.plugin;

import com.fasterxml.classmate.ResolvedType;
import com.stars.easyms.base.util.ReflectUtil;
import com.stars.easyms.rest.annotation.EasyMsRestMapping;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import springfox.documentation.RequestHandler;
import springfox.documentation.service.ResolvedMethodParameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.OperationModelsProviderPlugin;
import springfox.documentation.spi.service.contexts.RequestMappingContext;

import java.util.List;
import java.util.Optional;

/**
 * <p>className: EasyMsOperationModelsProvider</p>
 * <p>description: EasyMs的swagger的Models提供者类</p>
 *
 * @author guoguifang
 * @version 1.2.2
 * @date 2019-07-30 18:41
 */
public class EasyMsOperationModelsProvider extends EasyMsSwaggerCheckPlugin implements OperationModelsProviderPlugin {
    
    @Override
    public void apply(RequestMappingContext context) {
        if (context.findAnnotation(EasyMsRestMapping.class).isPresent()) {
            List<ResolvedMethodParameter> parameterTypes = context.getParameters();
            if (parameterTypes.size() != 1) {
                return;
            }
            ResolvedMethodParameter parameterType = parameterTypes.get(0);
            ResolvedType parameterResolvedType = parameterType.getParameterType();
            if (!parameterType.hasParameterAnnotation(RequestBody.class) && !parameterType
                    .hasParameterAnnotation(RequestPart.class)) {
                context.operationModelsBuilder().addInputParam(context.alternateFor(parameterResolvedType));
            }
        }
        if (isSwaggerCheck()) {
            checkRequestMappingContext(context);
        }
    }
    
    @Override
    public boolean supports(DocumentationType delimiter) {
        return true;
    }
    
    private void checkRequestMappingContext(RequestMappingContext context) {
        RequestHandler requestHandler = ReflectUtil.readFieldWithoutException(context, "handler");
        if (requestHandler == null || requestHandler.getHandlerMethod() == null) {
            return;
        }
    
        Class<?> controllerClass = requestHandler.getHandlerMethod().getBeanType();
        checkControllerClass(controllerClass);
        
        String controllerClassName = controllerClass.getName();
        String methodName = requestHandler.getHandlerMethod().getMethod().getName();
        List<ResolvedMethodParameter> parameterTypes = context.getParameters();
        
        Optional<EasyMsRestMapping> easyMsRestMappingOptional = context.findAnnotation(EasyMsRestMapping.class);
        if (easyMsRestMappingOptional.isPresent()) {
            EasyMsRestMapping easyMsRestMapping = easyMsRestMappingOptional.get();
            if (StringUtils.isBlank(easyMsRestMapping.name())) {
                addCheckErrorMsg(controllerClassName,
                        format("Class '{}' method '{}' annotation '@EasyMsRestMapping' name must not be blank!",
                                controllerClassName, methodName));
            }
            if (parameterTypes.size() != 1) {
                addCheckErrorMsg(controllerClassName,
                        format("Class '{}' method '{}' parameter count must be one!", controllerClassName, methodName));
            } else {
                checkResolvedType(parameterTypes.get(0).getParameterType());
            }
        } else if (context.findAnnotation(RequestMapping.class).isPresent()) {
            Optional<ApiOperation> apiOperationOptional = context.findAnnotation(ApiOperation.class);
            if (!apiOperationOptional.isPresent()) {
                addCheckErrorMsg(controllerClassName,
                        format("Class '{}' method '{}' missing annotated '@ApiOperation'!", controllerClassName,
                                methodName));
            } else {
                ApiOperation apiOperation = apiOperationOptional.get();
                if (StringUtils.isBlank(apiOperation.value())) {
                    addCheckErrorMsg(controllerClassName,
                            format("Class '{}' method '{}' annotation '@ApiOperation' value must not be blank!",
                                    controllerClassName, methodName));
                }
            }
            parameterTypes.forEach(parameterType -> checkResolvedType(parameterType.getParameterType()));
        }
        checkResolvedType(context.getReturnType());
    }
    
}