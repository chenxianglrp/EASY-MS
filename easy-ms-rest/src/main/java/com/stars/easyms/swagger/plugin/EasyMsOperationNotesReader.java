package com.stars.easyms.swagger.plugin;

import com.stars.easyms.rest.annotation.EasyMsRestMapping;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.env.Environment;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.OperationBuilderPlugin;
import springfox.documentation.spi.service.contexts.OperationContext;
import springfox.documentation.spring.web.DescriptionResolver;
import springfox.documentation.swagger.common.SwaggerPluginSupport;

import java.util.Optional;

/**
 * <p>className: EasyMsOperationSummaryReader</p>
 * <p>description: 设置swagger上显示rest的接口名称</p>
 *
 * @author guoguifang
 * @version 1.2.1
 * @date 2019-07-04 17:36
 */
public class EasyMsOperationNotesReader implements OperationBuilderPlugin {

    private final DescriptionResolver descriptions;

    public EasyMsOperationNotesReader(DescriptionResolver descriptions, Environment environment) {
        this.descriptions = descriptions != null ? descriptions : new DescriptionResolver(environment);
    }

    @Override
    public void apply(OperationContext context) {
        Optional<EasyMsRestMapping> restRequestMappingAnnotation = context.findAnnotation(EasyMsRestMapping.class);
        if (restRequestMappingAnnotation.isPresent() && StringUtils.isNotBlank(restRequestMappingAnnotation.get().notes())) {
            context.operationBuilder().notes(descriptions.resolve(restRequestMappingAnnotation.get().notes()));
        }
    }

    @Override
    public boolean supports(DocumentationType delimiter) {
        return SwaggerPluginSupport.pluginDoesApply(delimiter);
    }

}