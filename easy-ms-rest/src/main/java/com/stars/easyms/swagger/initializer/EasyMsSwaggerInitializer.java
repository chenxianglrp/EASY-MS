package com.stars.easyms.swagger.initializer;

import com.stars.easyms.base.util.BeanUtil;
import com.stars.easyms.monitor.handler.EasyMsRequestMappingHandlerMapping;
import com.stars.easyms.swagger.handler.EasyMsSwaggerRequestHandler;
import com.stars.easyms.swagger.properties.EasyMsSwaggerProperties;
import org.springframework.context.ConfigurableApplicationContext;
import springfox.documentation.spring.web.plugins.Docket;

import javax.annotation.PostConstruct;

/**
 * EasyMsSwagger初始化类.
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/7/16 8:11 下午
 */
public final class EasyMsSwaggerInitializer {
    
    private final EasyMsSwaggerProperties easyMsSwaggerProperties;
    
    private final ConfigurableApplicationContext applicationContext;
    
    private final Docket docket;
    
    public EasyMsSwaggerInitializer(EasyMsSwaggerProperties easyMsSwaggerProperties,
            ConfigurableApplicationContext applicationContext, Docket docket) {
        this.easyMsSwaggerProperties = easyMsSwaggerProperties;
        this.applicationContext = applicationContext;
        this.docket = docket;
    }
    
    @PostConstruct
    private void init() {
        docket.enable(easyMsSwaggerProperties.isEnabled());
        if (docket.isEnabled()) {
            EasyMsRequestMappingHandlerMapping easyMsSwaggerHandlerMapping = new EasyMsRequestMappingHandlerMapping(
                    applicationContext);
            easyMsSwaggerHandlerMapping.detectHandlerMethods(new EasyMsSwaggerRequestHandler(easyMsSwaggerProperties));
            BeanUtil.registerSingleton(applicationContext, "easyMsSwaggerHandlerMapping", easyMsSwaggerHandlerMapping);
        }
    }
    
}
