package com.stars.easyms.swagger.shiro;

import com.stars.easyms.monitor.shiro.ShiroAuthorityFunc;
import com.stars.easyms.swagger.constant.EasyMsSwaggerConstants;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * <p>className: EasyMsShiroAuthorityFunc</p>
 * <p>description: EasyMs的基础shiro权限管理方法类</p>
 *
 * @author guoguifang
 * @date 2019-11-25 15:40
 * @since 1.4.1
 */
public class EasyMsSwaggerShiroAuthorityFunc implements ShiroAuthorityFunc {

    @Override
    public Map<String, String> getFilterChainDefinitionMap() {
        Map<String, String> map = new LinkedHashMap<>();
        map.put(EasyMsSwaggerConstants.EASY_MS_INDEX_URI, NO_AUTHORITY_DEFINITION);
        map.put(EasyMsSwaggerConstants.SWAGGER_RESOURCE, NO_AUTHORITY_DEFINITION);
        map.put(EasyMsSwaggerConstants.API_DOCS, NO_AUTHORITY_DEFINITION);
        return map;
    }

    @Override
    public String getShiroAuthorityModuleName() {
        return EasyMsSwaggerConstants.MODULE_NAME;
    }

}