package com.stars.easyms.swagger.util;

import com.fasterxml.classmate.ResolvedType;
import com.google.common.collect.ImmutableMap;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Currency;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import static com.google.common.collect.Sets.newHashSet;

/**
 * swagger检查-类型工具类.
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/7/7 6:05 下午
 */
public class TypeUtil {
    
    private TypeUtil() {}
    
    private static final Set<String> baseTypes = newHashSet(
            "int",
            "date",
            "time",
            "string",
            "double",
            "float",
            "boolean",
            "byte",
            "object",
            "long",
            "date-time",
            "__file",
            "biginteger",
            "bigdecimal",
            "uuid",
            "entity");
    private static final Map<Type, String> typeNameLookup = ImmutableMap.<Type, String>builder()
            .put(Long.TYPE, "long")
            .put(Short.TYPE, "int")
            .put(Integer.TYPE, "int")
            .put(Double.TYPE, "double")
            .put(Float.TYPE, "float")
            .put(Byte.TYPE, "byte")
            .put(Boolean.TYPE, "boolean")
            .put(Character.TYPE, "string")
            
            .put(Date.class, "date-time")
            .put(java.sql.Date.class, "date")
            .put(LocalDateTime.class, "date-time")
            .put(LocalDate.class, "date")
            .put(LocalTime.class, "time")
            .put(String.class, "string")
            .put(Object.class, "object")
            .put(Long.class, "long")
            .put(Integer.class, "int")
            .put(Short.class, "int")
            .put(Double.class, "double")
            .put(Float.class, "float")
            .put(Boolean.class, "boolean")
            .put(Byte.class, "byte")
            .put(BigDecimal.class, "bigdecimal")
            .put(BigInteger.class, "biginteger")
            .put(Currency.class, "string")
            .put(UUID.class, "uuid")
            .put(MultipartFile.class, "__file")
            .put(ResponseEntity.class, "entity")
            .put(HttpEntity.class, "entity")
            .build();
    
    private static String typeNameFor(Type type) {
        return typeNameLookup.get(type);
    }
    
    public static boolean isBaseType(String typeName) {
        return baseTypes.contains(typeName);
    }
    
    public static boolean isBaseType(ResolvedType type) {
        return baseTypes.contains(typeNameFor(type.getErasedType()));
    }
    
    public static boolean isVoid(ResolvedType returnType) {
        return Void.class.equals(returnType.getErasedType()) || Void.TYPE.equals(returnType.getErasedType());
    }
    
    public static boolean isJdkType(ResolvedType type) {
        String className = type.getErasedType().getName();
        return className.startsWith("java.") || className.startsWith("javax.") || className.startsWith("jdk.")
                || className.startsWith("sun.");
    }
    
}
