package com.stars.easyms.swagger.properties;

import com.stars.easyms.base.constant.EasyMsCommonConstants;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * EasyMs的swagger属性类型
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
@ConfigurationProperties(prefix = EasyMsSwaggerProperties.PREFIX)
public class EasyMsSwaggerProperties {

    public static final String PREFIX = EasyMsCommonConstants.EASY_MS_PROPERTIES_PREFIX + "swagger";

    /**
     * 是否激活EasyMs的swagger模块
     */
    private boolean enabled;

    /**
     * 是否启动在线debug模式
     */
    private boolean onlineDebug;

    /**
     * swagger的扫描包
     */
    private String basePackage;
    
    /**
     * 是否检查swagger接口的输入、输出参数注解完整性
     */
    private boolean check = true;

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isOnlineDebug() {
        return onlineDebug;
    }

    public void setOnlineDebug(boolean onlineDebug) {
        this.onlineDebug = onlineDebug;
    }

    public String getBasePackage() {
        return basePackage;
    }

    public void setBasePackage(String basePackage) {
        this.basePackage = basePackage;
    }
    
    public boolean isCheck() {
        return check;
    }
    
    public void setCheck(boolean check) {
        this.check = check;
    }
}
