package com.stars.easyms.swagger.plugin;

import com.google.common.collect.Sets;
import com.stars.easyms.rest.annotation.EasyMsRestController;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.env.Environment;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.OperationBuilderPlugin;
import springfox.documentation.spi.service.contexts.OperationContext;
import springfox.documentation.spring.web.DescriptionResolver;

import java.util.Optional;

/**
 * <p>className: EasyMsOperationTagsReader</p>
 * <p>description: 设置swagger上显示rest的接口类的名称</p>
 *
 * @author guoguifang
 * @version 1.2.1
 * @date 2019-07-04 17:36
 */
public class EasyMsOperationTagsReader implements OperationBuilderPlugin {
    
    private final DescriptionResolver descriptions;
    
    public EasyMsOperationTagsReader(DescriptionResolver descriptions, Environment environment) {
        this.descriptions = descriptions != null ? descriptions : new DescriptionResolver(environment);
    }
    
    @Override
    public void apply(OperationContext context) {
        Optional<EasyMsRestController> controllerAnnotation = context
                .findControllerAnnotation(EasyMsRestController.class);
        if (controllerAnnotation.isPresent() && StringUtils.isNotBlank(controllerAnnotation.get().name())) {
            context.operationBuilder().tags(Sets.newHashSet(descriptions.resolve(controllerAnnotation.get().name())));
        }
    }
    
    @Override
    public boolean supports(DocumentationType delimiter) {
        return true;
    }
}