package com.stars.easyms.swagger.autoconfigure;

import com.stars.easyms.rest.autoconfigure.EasyMsRestAutoConfiguration;
import com.stars.easyms.swagger.plugin.EasyMsOperationModelsProvider;
import com.stars.easyms.swagger.plugin.EasyMsOperationNotesReader;
import com.stars.easyms.swagger.plugin.EasyMsOperationParameterReader;
import com.stars.easyms.swagger.plugin.EasyMsOperationSummaryReader;
import com.stars.easyms.swagger.plugin.EasyMsOperationTagsReader;
import com.stars.easyms.swagger.plugin.EasyMsParameterCheckPlugin;
import com.stars.easyms.swagger.plugin.EasyMsSwaggerApiListingReader;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import springfox.documentation.schema.TypeNameExtractor;
import springfox.documentation.spi.schema.EnumTypeDeterminer;
import springfox.documentation.spring.web.DescriptionResolver;

/**
 * <p>className: EasyMsRestSwaggerAutoConfiguration</p>
 * <p>description: EasyMs的rest专用swagger的插件自动注册类</p>
 *
 * @author guoguifang
 * @version 1.2.2
 * @date 2019-07-30 14:00
 */
@Configuration
@ConditionalOnBean({EasyMsSwaggerAutoConfiguration.class, EasyMsRestAutoConfiguration.class})
public class EasyMsSwaggerRestAutoConfiguration {
    
    @Bean
    public EasyMsOperationParameterReader easyMsOperationParameterReader(ObjectProvider<EnumTypeDeterminer> enumTypeDeterminerObjectProvider,
            ObjectProvider<TypeNameExtractor> typeNameExtractorObjectProvider) {
        return new EasyMsOperationParameterReader(enumTypeDeterminerObjectProvider.getIfAvailable(), typeNameExtractorObjectProvider.getIfAvailable());
    }
    
    @Bean
    public EasyMsOperationTagsReader easyMsOperationTagsReader(
            ObjectProvider<DescriptionResolver> resolverObjectProvider, Environment environment) {
        return new EasyMsOperationTagsReader(resolverObjectProvider.getIfAvailable(), environment);
    }
    
    @Bean
    public EasyMsOperationSummaryReader easyMsOperationSummaryReader(
            ObjectProvider<DescriptionResolver> resolverObjectProvider, Environment environment) {
        return new EasyMsOperationSummaryReader(resolverObjectProvider.getIfAvailable(), environment);
    }
    
    @Bean
    public EasyMsOperationNotesReader easyMsOperationNotesReader(
            ObjectProvider<DescriptionResolver> resolverObjectProvider, Environment environment) {
        return new EasyMsOperationNotesReader(resolverObjectProvider.getIfAvailable(), environment);
    }
    
    @Bean
    public EasyMsSwaggerApiListingReader easyMsSwaggerApiListingReader() {
        return new EasyMsSwaggerApiListingReader();
    }
    
    @Bean
    public EasyMsOperationModelsProvider easyMsOperationModelsProvider() {
        return new EasyMsOperationModelsProvider();
    }
    
    @Bean
    public EasyMsParameterCheckPlugin easyMsParameterCheckPlugin() {
        return new EasyMsParameterCheckPlugin();
    }
}