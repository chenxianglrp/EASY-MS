package com.stars.easyms.swagger.plugin;

import com.fasterxml.classmate.ResolvedType;
import com.fasterxml.classmate.TypeResolver;
import com.stars.easyms.base.bean.LazyLoadBean;
import com.stars.easyms.base.util.MessageFormatUtil;
import com.stars.easyms.rest.annotation.EasyMsRestController;
import com.stars.easyms.swagger.properties.EasyMsSwaggerProperties;
import com.stars.easyms.swagger.util.TypeUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.schema.Collections;
import springfox.documentation.schema.Maps;

import javax.validation.constraints.AssertFalse;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Email;
import javax.validation.constraints.Negative;
import javax.validation.constraints.NegativeOrZero;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * 基础swagger检查插件.
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/7/5 4:43 下午
 */
@SuppressWarnings("unchecked")
class EasyMsSwaggerCheckPlugin {
    
    private static final LazyLoadBean<EasyMsSwaggerProperties> EASY_MS_SWAGGER_PROPERTIES_LAZY_LOAD_BEAN = new LazyLoadBean<>(
            EasyMsSwaggerProperties.class);
    
    private static Class<? extends Annotation>[] onlyCharSequenceAnnotationArray;
    
    private static Class<? extends Annotation>[] onlyBooleanAnnotationArray;
    
    private static Class<? extends Annotation>[] arrayAnnotationArray;
    
    private static Class<? extends Annotation>[] numberAnnotationArray;
    
    private static Class<?>[] arrayAllowTypeArray;
    
    private static Class<?>[] numberAllowTypeArray;
    
    static {
        onlyCharSequenceAnnotationArray = new Class[] {NotBlank.class, Email.class, Pattern.class};
        
        onlyBooleanAnnotationArray = new Class[] {AssertFalse.class, AssertTrue.class};
        
        arrayAnnotationArray = new Class[] {NotEmpty.class, Size.class};
        
        numberAnnotationArray = new Class[] {Negative.class, NegativeOrZero.class, Positive.class,
                PositiveOrZero.class};
        
        arrayAllowTypeArray = new Class[] {CharSequence.class, Collection.class, Map.class};
        
        numberAllowTypeArray = new Class[] {BigDecimal.class, BigInteger.class, Byte.class, CharSequence.class,
                Double.class, Float.class, Integer.class, Long.class, Number.class, Short.class};
        
    }
    
    static final Map<String, List<String>> CHECK_ERROR_MSG_MAP = new ConcurrentHashMap<>();
    
    static final Map<String, Class<?>> API_MODEL_VALUE_MAP = new ConcurrentHashMap<>();
    
    static final Map<Class<?>, Object> CHECKED_CLASS_MAP = new ConcurrentHashMap<>();
    
    static boolean isSwaggerCheck() {
        return EASY_MS_SWAGGER_PROPERTIES_LAZY_LOAD_BEAN.getNonNullBean().isCheck();
    }
    
    void checkControllerClass(Class<?> controllerClass) {
        if (controllerClass != null && !Object.class.equals(controllerClass) && !CHECKED_CLASS_MAP
                .containsKey(controllerClass)) {
            String className = controllerClass.getName();
            if (controllerClass.isAnnotationPresent(EasyMsRestController.class)) {
                EasyMsRestController easyMsRestController = controllerClass
                        .getDeclaredAnnotation(EasyMsRestController.class);
                if (StringUtils.isBlank(easyMsRestController.name())) {
                    addCheckErrorMsg(className,
                            format("Class '{}' annotation '@EasyMsRestController' name must not be blank!", className));
                }
                return;
            }
            if (controllerClass.isAnnotationPresent(RestController.class) || controllerClass
                    .isAnnotationPresent(Controller.class)) {
                if (!controllerClass.isAnnotationPresent(Api.class)) {
                    addCheckErrorMsg(className, format("Class '{}' missing annotated '@Api'!", className));
                    return;
                }
                Api api = controllerClass.getDeclaredAnnotation(Api.class);
                if (StringUtils.isBlank(api.value())) {
                    addCheckErrorMsg(className,
                            format("Class '{}' annotation '@Api' value must not be blank!", className));
                }
            }
        }
    }
    
    void checkResolvedType(ResolvedType resolvedType) {
        Class<?> clazz = resolvedType.getErasedType();
        while (clazz != null && !Object.class.equals(clazz) && !clazz.isInterface() && !clazz.isEnum() && !TypeUtil
                .isBaseType(resolvedType) && !TypeUtil.isVoid(resolvedType) && !TypeUtil.isJdkType(resolvedType)
                && !CHECKED_CLASS_MAP.containsKey(clazz)) {
            CHECKED_CLASS_MAP.put(clazz, ObjectUtils.NULL);
            checkParameterOrReturnType(clazz);
            checkParameterOrReturnTypeField(clazz);
            clazz = clazz.getSuperclass();
        }
    }
    
    void addCheckErrorMsg(String className, String msg) {
        CHECK_ERROR_MSG_MAP.computeIfAbsent(className, k -> new ArrayList<>()).add(msg);
    }
    
    String format(final String message, final Object... params) {
        return MessageFormatUtil.format(message, params);
    }
    
    private void checkParameterOrReturnType(Class<?> clazz) {
        String className = clazz.getName();
        if (!clazz.isAnnotationPresent(ApiModel.class)) {
            addCheckErrorMsg(className, format("Class '{}' missing annotated '@ApiModel'!", className));
            return;
        }
        ApiModel apiModel = clazz.getDeclaredAnnotation(ApiModel.class);
        String apiModelValue = apiModel.value();
        if (StringUtils.isBlank(apiModelValue)) {
            addCheckErrorMsg(className,
                    format("Class '{}' annotation '@ApiModel' value must not be blank!", className));
            return;
        }
        
        Class<?> existClass = API_MODEL_VALUE_MAP.putIfAbsent(apiModelValue, clazz);
        if (existClass != null) {
            addCheckErrorMsg(className,
                    format("Class '{}' annotation '@ApiModel' value is already used by the class '{}'!", className,
                            existClass.getName()));
        }
    }
    
    private void checkParameterOrReturnTypeField(Class<?> clazz) {
        String className = clazz.getName();
        for (Field field : clazz.getDeclaredFields()) {
            if (Modifier.isStatic(field.getModifiers())) {
                continue;
            }
            checkParameterOrReturnTypeField(className, field);
            ResolvedType fieldResolve = new TypeResolver().resolve(field.getGenericType());
            if (Collections.isContainerType(fieldResolve)) {
                checkResolvedType(Collections.collectionElementType(fieldResolve));
            } else if (Maps.isMapType(fieldResolve)) {
                addCheckErrorMsg(className,
                        format("Class '{}' field '{}' type must not be Map!", className, field.getName()));
            } else {
                checkResolvedType(fieldResolve);
            }
        }
    }
    
    private void checkParameterOrReturnTypeField(String className, Field field) {
        if (!field.isAnnotationPresent(ApiModelProperty.class)) {
            addCheckErrorMsg(className,
                    format("Class '{}' field '{}' missing annotated '@ApiModelProperty'!", className, field.getName()));
            return;
        }
        ApiModelProperty apiModelProperty = field.getDeclaredAnnotation(ApiModelProperty.class);
        String apiModelPropertyValue = apiModelProperty.value();
        if (StringUtils.isBlank(apiModelPropertyValue)) {
            addCheckErrorMsg(className,
                    format("Class '{}' field '{}' annotation '@ApiModelProperty' value must not be blank!", className,
                            field.getName()));
        }
        if (isNotBlankOrNotNull(field)) {
            if (!apiModelProperty.required()) {
                addCheckErrorMsg(className,
                        format("Class '{}' field '{}' annotation '@ApiModelProperty' required must be true!", className,
                                field.getName()));
            }
        } else if (apiModelProperty.required()) {
            addCheckErrorMsg(className,
                    format("Class '{}' field '{}' must add one of '@NotBlank', '@NotNull', '@NotEmpty' annotations!",
                            className, field.getName()));
        }
        if (field.getType().isEnum() && StringUtils.isBlank(apiModelProperty.allowableValues())) {
            addCheckErrorMsg(className,
                    format("Class '{}' field '{}' annotation '@ApiModelProperty' allowableValues must not be blank!",
                            className, field.getName()));
        }
        checkCorrectAnnotation(className, field);
    }
    
    private void checkCorrectAnnotation(String className, Field field) {
        Class<?> fieldType = field.getType();
        String fieldName = field.getName();
        if (!CharSequence.class.isAssignableFrom(fieldType)) {
            checkAnnotationClassListAndAddErrorMsg(className, fieldName,
                    onlySomeAnnotationsWithAnnotation(field, onlyCharSequenceAnnotationArray));
        }
        if (!Boolean.class.equals(fieldType) && !boolean.class.equals(fieldType)) {
            checkAnnotationClassListAndAddErrorMsg(className, fieldName,
                    onlySomeAnnotationsWithAnnotation(field, onlyBooleanAnnotationArray));
        }
        Class<? extends Annotation> annotation = getOneAnnotation(field, arrayAnnotationArray);
        if (annotation != null) {
            if (!fieldType.isArray() && notAssignableFrom(fieldType, arrayAllowTypeArray)) {
                addCheckErrorMsg(className,
                        format("Class '{}' field '{}' cannot use annotation '{}'!", className, fieldName,
                                annotation.getSimpleName()));
            }
        }
        annotation = getOneAnnotation(field, numberAnnotationArray);
        if (annotation != null) {
            if (notAssignableFrom(fieldType, numberAllowTypeArray)) {
                addCheckErrorMsg(className,
                        format("Class '{}' field '{}' cannot use annotation '{}'!", className, fieldName,
                                annotation.getSimpleName()));
            }
        }
    }
    
    private List<Class<? extends Annotation>> onlySomeAnnotationsWithAnnotation(Field field,
            Class<? extends Annotation>[] onlyOneTypeAnnotationArray) {
        List<Class<? extends Annotation>> onlySomeAnnotationsWithAnnotations = new ArrayList<>();
        for (Class<? extends Annotation> clazz : onlyOneTypeAnnotationArray) {
            if (field.isAnnotationPresent(clazz)) {
                onlySomeAnnotationsWithAnnotations.add(clazz);
            }
        }
        return onlySomeAnnotationsWithAnnotations;
    }
    
    private void checkAnnotationClassListAndAddErrorMsg(String className, String fieldName,
            List<Class<? extends Annotation>> annotationClassList) {
        if (!annotationClassList.isEmpty()) {
            addCheckErrorMsg(className,
                    format("Class '{}' field '{}' cannot use annotation '{}'!", className, fieldName,
                            annotationClassList.stream().map(Class::getSimpleName).collect(Collectors.joining(", "))));
        }
    }
    
    private boolean notAssignableFrom(Class<?> fieldType, Class<?>[] classArray) {
        for (Class<?> clazz : classArray) {
            if (clazz.isAssignableFrom(fieldType)) {
                return false;
            }
        }
        return true;
    }
    
    private Class<? extends Annotation> getOneAnnotation(Field field, Class<? extends Annotation>[] annotationArray) {
        for (Class<? extends Annotation> clazz : annotationArray) {
            if (field.isAnnotationPresent(clazz)) {
                return clazz;
            }
        }
        return null;
    }
    
    private boolean isNotBlankOrNotNull(Field field) {
        return field.isAnnotationPresent(NotBlank.class) || field.isAnnotationPresent(NotBlank.List.class) || field
                .isAnnotationPresent(NotNull.class) || field.isAnnotationPresent(NotNull.List.class) || field
                .isAnnotationPresent(NotEmpty.class) || field.isAnnotationPresent(NotEmpty.List.class);
    }
    
}
