package com.stars.easyms.swagger.plugin;

import com.stars.easyms.swagger.properties.EasyMsSwaggerProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * swaggerCheckPluginRunner.
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/7/7 4:22 下午
 */
@Slf4j
public class EasyMsSwaggerCheckPluginRunner implements ApplicationRunner {
    
    private final EasyMsSwaggerProperties easyMsSwaggerProperties;
    
    public EasyMsSwaggerCheckPluginRunner(EasyMsSwaggerProperties easyMsSwaggerProperties) {
        this.easyMsSwaggerProperties = easyMsSwaggerProperties;
    }
    
    @Override
    public void run(ApplicationArguments args) {
        if (easyMsSwaggerProperties.isEnabled() && easyMsSwaggerProperties.isCheck()) {
            Map<String, List<String>> checkErrorMsgMap = EasyMsSwaggerCheckPlugin.CHECK_ERROR_MSG_MAP;
            if (!checkErrorMsgMap.isEmpty()) {
                StringBuilder stringBuilder = new StringBuilder(
                        "Swagger check fail, the details of the failure info are as follows:\n");
                checkErrorMsgMap.entrySet().stream().sorted(Map.Entry.comparingByKey()).map(Map.Entry::getValue)
                        .collect(Collectors.toList()).forEach(list -> {
                    list.forEach(s -> stringBuilder.append(s).append("\n"));
                    stringBuilder.append("\n");
                });
                log.error(stringBuilder.toString());
                System.exit(1);
            }
            EasyMsSwaggerCheckPlugin.API_MODEL_VALUE_MAP.clear();
            EasyMsSwaggerCheckPlugin.CHECKED_CLASS_MAP.clear();
        }
    }
}
