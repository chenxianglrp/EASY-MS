package com.stars.easyms.swagger.plugin;

import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.ParameterBuilderPlugin;
import springfox.documentation.spi.service.contexts.ParameterContext;

/**
 * EasyMs自定义ExpandedParameter检查插件.
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/7/5 4:50 下午
 */
public class EasyMsParameterCheckPlugin extends EasyMsSwaggerCheckPlugin implements ParameterBuilderPlugin {
    
    @Override
    public void apply(ParameterContext parameterContext) {
        if (isSwaggerCheck()) {
            checkResolvedType(parameterContext.resolvedMethodParameter().getParameterType());
            checkResolvedType(parameterContext.getOperationContext().getReturnType());
        }
    }
    
    @Override
    public boolean supports(DocumentationType documentationType) {
        return true;
    }
    
}
