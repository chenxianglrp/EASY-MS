package com.stars.easyms.swagger.autoconfigure;

import com.stars.easyms.base.util.PatternUtil;
import com.stars.easyms.base.util.PropertyPlaceholderUtil;
import com.stars.easyms.base.util.SpringBootUtil;
import com.stars.easyms.swagger.initializer.EasyMsSwaggerInitializer;
import com.stars.easyms.swagger.plugin.EasyMsSwaggerCheckPluginRunner;
import com.stars.easyms.swagger.properties.EasyMsSwaggerProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;
import springfox.documentation.RequestHandler;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.ApiSelectorBuilder;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Swagger自动配置类
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
@Configuration
@EnableSwagger2WebMvc
@EnableConfigurationProperties(EasyMsSwaggerProperties.class)
public class EasyMsSwaggerAutoConfiguration {
    
    private final ConfigurableApplicationContext applicationContext;
    
    private final EasyMsSwaggerProperties easyMsSwaggerProperties;
    
    public EasyMsSwaggerAutoConfiguration(ConfigurableApplicationContext applicationContext,
            EasyMsSwaggerProperties easyMsSwaggerProperties) {
        this.applicationContext = applicationContext;
        this.easyMsSwaggerProperties = easyMsSwaggerProperties;
    }
    
    @Bean
    @ConditionalOnMissingBean(Docket.class)
    public Docket createRestApi() {
        ApiSelectorBuilder bulider = new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).select();
        String basePackage = easyMsSwaggerProperties.getBasePackage();
        if (!StringUtils.hasText(basePackage)) {
            basePackage = SpringBootUtil.getSpringApplicationPackageName();
        }
        return bulider.apis(basePackage(basePackage)).paths(PathSelectors.any()).build();
    }
    
    @Bean
    public EasyMsSwaggerInitializer easyMsSwaggerInitializer(Docket docket) {
        return new EasyMsSwaggerInitializer(easyMsSwaggerProperties, applicationContext, docket);
    }
    
    @Bean
    public EasyMsSwaggerCheckPluginRunner easyMsSwaggerCheckPluginRunner() {
        return new EasyMsSwaggerCheckPluginRunner(easyMsSwaggerProperties);
    }
    
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title(applicationContext.getId() + " RESTful APIs")
                .description(PropertyPlaceholderUtil.replace("spring.application.description", "")).version("1.0")
                .build();
    }
    
    private static Predicate<RequestHandler> basePackage(final String basePackage) {
        return input -> declaringClass(input).map(handlerPackage(basePackage)).orElse(true);
    }
    
    private static Function<Class<?>, Boolean> handlerPackage(final String basePackage) {
        return input -> {
            if (input != null) {
                for (String strPackage : PatternUtil.splitWithComma(basePackage)) {
                    Package packagei = input.getPackage();
                    if (packagei != null) {
                        boolean isMatch = packagei.getName().startsWith(strPackage);
                        if (isMatch) {
                            return true;
                        }
                    }
                }
            }
            return false;
        };
    }
    
    private static Optional<? extends Class<?>> declaringClass(RequestHandler input) {
        return Optional.ofNullable(input.declaringClass());
    }
}
