package com.stars.easyms.swagger.plugin;

import com.google.common.collect.Sets;
import com.stars.easyms.rest.annotation.EasyMsRestController;
import org.apache.commons.lang3.reflect.FieldUtils;
import springfox.documentation.builders.ApiListingBuilder;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.ApiListingBuilderPlugin;
import springfox.documentation.spi.service.contexts.ApiListingContext;

import java.lang.reflect.Field;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newTreeSet;
import static org.springframework.core.annotation.AnnotationUtils.findAnnotation;
import static springfox.documentation.service.Tags.emptyTags;
import static springfox.documentation.swagger.common.SwaggerPluginSupport.pluginDoesApply;

/**
 * <p>className: EasyMsSwaggerApiListingReader</p>
 * <p>description: 设置swagger上api监控类</p>
 *
 * @author guoguifang
 * @version 1.2.1
 * @date 2019-07-04 17:36
 */
public class EasyMsSwaggerApiListingReader implements ApiListingBuilderPlugin {
    
    @Override
    @SuppressWarnings("unchecked")
    public void apply(ApiListingContext apiListingContext) {
        Optional<? extends Class<?>> controllerClassOptional = apiListingContext.getResourceGroup()
                .getControllerClass();
        if (controllerClassOptional.isPresent()) {
            Class<?> controllerClass = controllerClassOptional.get();
            Optional<EasyMsRestController> easyMsRestControllerOptional = Optional
                    .ofNullable(findAnnotation(controllerClass, EasyMsRestController.class));
            Set<String> tagSet = easyMsRestControllerOptional.map(input -> newTreeSet(
                    newArrayList(input.name()).stream().filter(emptyTags()).collect(Collectors.toSet())))
                    .orElse(Sets.newTreeSet());
            if (tagSet.isEmpty()) {
                tagSet.add(apiListingContext.getResourceGroup().getGroupName());
            }
            ApiListingBuilder apiListingBuilder = apiListingContext.apiListingBuilder();
            Field field = FieldUtils.getDeclaredField(ApiListingBuilder.class, "tagNames", true);
            if (field != null) {
                try {
                    Set<String> tagNames = (Set<String>) field.get(apiListingBuilder);
                    tagNames.clear();
                } catch (IllegalAccessException e) {
                    // ignore
                }
            }
            apiListingBuilder.tagNames(tagSet);
        }
    }
    
    @Override
    public boolean supports(DocumentationType delimiter) {
        return pluginDoesApply(delimiter);
    }
}