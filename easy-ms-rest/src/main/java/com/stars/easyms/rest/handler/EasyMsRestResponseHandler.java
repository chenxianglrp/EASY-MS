package com.stars.easyms.rest.handler;

import com.stars.easyms.base.encrypt.EasyMsEncrypt;
import com.stars.easyms.rest.bean.EasyMsRestContext;
import com.stars.easyms.rest.initializer.RequestMappingPathForRestInfo;
import org.springframework.lang.NonNull;

/**
 * <p>className: EasyMsRestResponseHandler</p>
 * <p>description: easy-ms-rest响应处理类</p>
 *
 * @author guoguifang
 * @version 1.7.1
 * @date 2020/12/14 10:30 上午
 */
final class EasyMsRestResponseHandler {

    /**
     * 对返回信息进行加密，如果不需要加密则返回null
     */
    void handleRestResponse(@NonNull EasyMsRestContext easyMsRestContext, boolean useDefaultResponseEntityHandler) {
        // 请求处理成功后封装返回信息
        Object responseEntity = ResponseEntityHelper.getResponseEntity(easyMsRestContext, useDefaultResponseEntityHandler);

        // 处理加密响应包装信息
        RequestMappingPathForRestInfo requestMappingPathForRestInfo = easyMsRestContext.getRequestMappingPathForRestInfo();
        if (requestMappingPathForRestInfo != null && requestMappingPathForRestInfo.isEncrypt()) {
            Object encryptedResponseEntity = EasyMsEncrypt.encrypt(responseEntity, requestMappingPathForRestInfo.getSecret(),
                    requestMappingPathForRestInfo.getIv(), requestMappingPathForRestInfo.getEncryptResponseKey());
            easyMsRestContext.setEncryptedResponseEntity(encryptedResponseEntity);
        }
    }

}
