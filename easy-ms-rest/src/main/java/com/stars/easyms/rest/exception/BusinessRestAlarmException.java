package com.stars.easyms.rest.exception;

import com.stars.easyms.base.exception.AlarmException;

/**
 * rest接口业务相关异常：默认告警，若不想告警需使用BusinessRestException
 *
 * @author guoguifang
 * @date 2020-08-21 11:34
 * @since 1.6.1
 */
public class BusinessRestAlarmException extends BusinessRestException implements AlarmException {

    public BusinessRestAlarmException(String retMsg) {
        super(retMsg);
    }

    public BusinessRestAlarmException(String retMsg, Exception e) {
        super(retMsg, e);
    }

    public BusinessRestAlarmException(String retCode, String retMsg) {
        super(retCode, retMsg);
    }

    public BusinessRestAlarmException(String retCode, String retMsg, Exception e) {
        super(retCode, retMsg, e);
    }
    
    public BusinessRestAlarmException(String retCode, String retMsg, String errorDesc) {
        super(retCode, retMsg, errorDesc);
    }
}
