package com.stars.easyms.rest;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

/**
 * <p>interfaceName: ResponseEntityHandler</p>
 * <p>description: 响应处理接口</p>
 *
 * @author guoguifang
 * @version 1.2.1
 * @date 2019-07-11 14:33
 */
public interface ResponseEntityHandler<T> {

    /**
     * 请求处理成功后封装返回信息
     *
     * @param responseBody 返回参数
     * @return 封装信息
     */
    @NonNull
    Object getResponseEntity(@Nullable T responseBody);

    /**
     * 请求处理异常后封装返回信息
     *
     * @param t 异常信息
     * @return 封装信息
     */
    @NonNull
    Object getResponseEntityWithException(@NonNull Throwable t);

}
