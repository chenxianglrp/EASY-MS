package com.stars.easyms.rest.constant;

import com.stars.easyms.rest.bean.RestInfo;
import com.stars.easyms.rest.initializer.RequestMappingPathForRestInfo;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Rest服务相关常量类
 *
 * @author guoguifang
 * @date 2018-10-12 11:24
 * @since 1.0.0
 */
public final class RestConstants {

    /**
     * 存放requestMappingPath和RequestMappingPathForRestInfo的映射关系
     */
    public static final Map<String, RequestMappingPathForRestInfo> REQUEST_MAPPING_PATH_MAP = new ConcurrentHashMap<>(32);

    /**
     * 存放注册后的requestMappingPath和RestInfo的映射关系
     */
    public static final Map<String, RestInfo> REGISTER_REQUEST_MAPPING_PATH_MAP = new ConcurrentHashMap<>(32);

    /**
     * 默认的接口版本号
     */
    public static final String DEFAULT_INTERFACE_VERSION = "v1";

    /**
     * rest锁前缀
     */
    public static final String REST_LOCK = "REST_LOCK";

    /**
     * INTERFACE接口的匹配url
     */
    public static final String INTERFACE_URL_WITH_PREFIX = "/interface/**";

    /**
     * SERVICE接口的匹配url
     */
    public static final String SERVICE_URL_WITH_PREFIX = "/service/**";

    /**
     * redis中存放的重复请求的KEY值
     */
    public static final String REDIS_DUPLICATE_REQUEST_KEY = "duplicateRequest";

    private RestConstants() {}
}
