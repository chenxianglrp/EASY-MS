package com.stars.easyms.rest.pointcut;

import com.stars.easyms.base.pointcut.EasyMsAnnotationMatchingPointcut;
import com.stars.easyms.rest.handler.EasyMsControllerAspect;
import com.stars.easyms.rest.properties.EasyMsRestProperties;
import org.aopalliance.aop.Advice;
import org.springframework.aop.Pointcut;
import org.springframework.aop.support.AbstractPointcutAdvisor;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.reflect.Modifier;

/**
 * <p>className: EasyMsControllerPointcutAdvisor</p>
 * <p>description: EasyMs的controller层PointcutAdvisor</p>
 *
 * @author guoguifang
 * @version 1.7.1
 * @date 2020/12/10 5:30 下午
 */
public final class EasyMsRequestMappingPointcutAdvisor extends AbstractPointcutAdvisor {

    private transient EasyMsControllerAspect advice;

    private transient EasyMsAnnotationMatchingPointcut pointcut;

    public EasyMsRequestMappingPointcutAdvisor(EasyMsRestProperties easyMsRestProperties) {
        this.advice = new EasyMsControllerAspect(easyMsRestProperties);
        this.pointcut = new EasyMsAnnotationMatchingPointcut();
        this.pointcut.setMethodMatcher(RequestMapping.class, true);
        this.pointcut.setClassFilter(clazz -> !Modifier.isFinal(clazz.getModifiers()));
    }

    @Override
    public Pointcut getPointcut() {
        return this.pointcut;
    }

    @Override
    public Advice getAdvice() {
        return this.advice;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || this.getClass() != other.getClass()) {
            return false;
        }
        EasyMsRequestMappingPointcutAdvisor otherAdvisor = (EasyMsRequestMappingPointcutAdvisor) other;
        return (ObjectUtils.nullSafeEquals(this.advice, otherAdvisor.advice) &&
                ObjectUtils.nullSafeEquals(this.pointcut, otherAdvisor.pointcut));
    }

    @Override
    public int hashCode() {
        return 41 + this.advice.hashCode() * 41 + this.pointcut.hashCode() * 41;
    }

}