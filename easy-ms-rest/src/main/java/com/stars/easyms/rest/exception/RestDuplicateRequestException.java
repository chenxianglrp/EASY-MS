package com.stars.easyms.rest.exception;

import com.stars.easyms.base.util.ExceptionUtil;
import com.stars.easyms.base.util.MessageFormatUtil;

/**
 * <p>className: RestDuplicateRequestException</p>
 * <p>description: rest的重复请求异常</p>
 *
 * @author guoguifang
 * @version 1.6.3
 * @date 2020/9/15 9:47 上午
 */
public final class RestDuplicateRequestException extends RuntimeException {

    private static final long serialVersionUID = -3034892231245466632L;

    public RestDuplicateRequestException(String message, Object... args) {
        super(MessageFormatUtil.format(message, ExceptionUtil.trimmedCopy(args)), ExceptionUtil.extractThrowable(args));
    }
}
