package com.stars.easyms.rest.exception;

import com.stars.easyms.base.exception.BusinessException;

/**
 * rest接口业务相关异常：默认不告警，若想告警需使用BusinessRestAlarmException
 *
 * @author guoguifang
 * @date 2020-08-17 18:34
 * @since 1.6.1
 */
public class BusinessRestException extends BusinessException {

    public BusinessRestException(String retMsg) {
        super(retMsg);
    }

    public BusinessRestException(String retMsg, Exception e) {
        super(retMsg, e);
    }

    public BusinessRestException(String retCode, String retMsg) {
        super(retCode, retMsg);
    }

    public BusinessRestException(String retCode, String retMsg, Exception e) {
        super(retCode, retMsg, e);
    }
    
    public BusinessRestException(String retCode, String retMsg, String errorDesc) {
        super(retCode, retMsg, errorDesc);
    }
}
