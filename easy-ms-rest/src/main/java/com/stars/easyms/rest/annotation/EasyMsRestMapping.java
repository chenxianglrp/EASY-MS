package com.stars.easyms.rest.annotation;

import com.stars.easyms.rest.constant.RestConstants;
import com.stars.easyms.rest.RestService;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * <p>className: EasyMsRestMapping</p>
 * <p>description: rest请求mapping类，只开放post方式</p>
 *
 * @author guoguifang
 * @version 1.2.1
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EasyMsRestMapping {

    /**
     * 接口名称
     */
    String name();

    /**
     * 接口描述
     */
    String notes() default "";
    
    /**
     * 接口编码，支持多个接口编码对应同一个接口实现类，若为空时默认为方法名
     */
    @AliasFor("code")
    String[] path() default {};
    
    /**
     * 接口编码，支持多个接口编码对应同一个接口实现类，若为空时默认为方法名
     */
    @AliasFor("path")
    String[] code() default {};

    /**
     * 接口版本，不支持多版本对应同一个接口实现类，不输入时默认为v1
     */
    String version() default RestConstants.DEFAULT_INTERFACE_VERSION;

    /**
     * 取请求参数中的某些字段的值，若值与methodFor共同定位一个接口实现类
     */
    String[] method() default {};

    /**
     * 与请求参数中method中的定义的值共同定位一个接口实现类
     */
    String[] methodFor() default {};

    /**
     * 默认method字段数量越多优先级越高，若字段数量相同的时候的优先级，值越小优先级越高，默认最小
     */
    int methodPriority() default Integer.MAX_VALUE;

    /**
     * 服务类型:交易类-T,查询类-Q
     */
    String type() default "T";

    /**
     * 实现类
     */
    Class<? extends RestService> service();

    /**
     * 与全局encrypt共同作用，默认使用全局加密参数，如果当前加密参数不为空优先当前加密参数
     */
    boolean encrypt() default true;

    /**
     * 当前接口单独使用该秘钥加密，优先级高于全局secret
     */
    String secret() default "";

    /**
     * 当前接口单独使用该秘钥偏移量加密，优先级高于全局iv
     */
    String iv() default "";

    /**
     * 当前接口单独使用该加密请求参数，优先级高于全局encryptRequestKey
     */
    String encryptRequestKey() default "";

    /**
     * 当前接口单独使用该加密返回参数，优先级高于全局encryptResponseKey
     */
    String encryptResponseKey() default "";

    /**
     * 当前接口路径尾部是否拼接 "/"
     */
    boolean allowEndIsForwardSlash() default true;

    /**
     * 是否强制使用默认的响应体处理器
     */
    boolean useDefaultResponseEntityHandler() default false;
}
