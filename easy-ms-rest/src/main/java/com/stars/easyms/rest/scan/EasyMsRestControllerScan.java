package com.stars.easyms.rest.scan;

/**
 * <p>className: EasyMsRestControllerScan</p>
 * <p>description: EasyMsRestController扫描包注册类</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/6/1 8:35 下午
 */
public interface EasyMsRestControllerScan {

    String[] scanPackages();

}
