package com.stars.easyms.rest.exception;

import com.stars.easyms.base.util.ExceptionUtil;
import com.stars.easyms.base.util.MessageFormatUtil;

/**
 * Rest抛出的异常
 *
 * @author guoguifang
 * @date 2018-10-12 11:24
 * @since 1.0.0
 */
public final class RestRuntimeException extends RuntimeException {

    private static final long serialVersionUID = -3034892230245466631L;

    public RestRuntimeException(String message, Object... args) {
        super(MessageFormatUtil.format(message, ExceptionUtil.trimmedCopy(args)), ExceptionUtil.extractThrowable(args));
    }
}
