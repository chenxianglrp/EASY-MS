package com.stars.easyms.rest.bean;

import com.stars.easyms.rest.enums.ServiceType;
import com.stars.easyms.rest.RestService;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.RequestMethod;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Map;

/**
 * <p>className: RestInfo</p>
 * <p>description: Rest信息DTO类</p>
 *
 * @author guoguifang
 * @version 1.2.1
 */
public final class RestInfo<I, O> {

    /**
     * 接口服务名称
     */
    private String name;

    /**
     * 接口服务编码
     */
    private String code;

    /**
     * 接口服务版本号
     */
    private String version;

    /**
     * 接口服务路径
     */
    private String requestMappingPath;

    /**
     * 缩写服务路径：如果是默认版本号，则版本号可以不带
     */
    private String abbrRequestMappingPath;

    /**
     * 取请求参数中的某些字段的值，判断其所有字段的值是否与methodMap相同来定位一个接口实现类
     */
    private Map<String, String> methodMap;

    /**
     * 默认method字段数量越多优先级越高，若字段数量相同的时候的优先级，值越小优先级越高，默认最小
     */
    private int methodPriority;

    /**
     * 注册在RequestMappingHandlerMapping中的requestMappingPath路径
     */
    private String registerRequestMappingPath;

    /**
     * 注册在RequestMappingHandlerMapping中的requestMappingPath缩写路径
     */
    private String abbrRegisterRequestMappingPath;

    /**
     * 接口服务类型 Q:查询类 T:交易类
     */
    private ServiceType type;

    /**
     * 接口请求类型
     */
    private RequestMethod[] requestMethods;

    /**
     * RestController类
     */
    private Class<?> restControllerClass;

    /**
     * 当前rest服务的RestController类对应的方法
     */
    private Method restControllerMethod;

    /**
     * 接口服务类
     */
    private Class<? extends RestService> restServiceClass;

    /**
     * 接口服务方法
     */
    private Method restServiceMethod;

    /**
     * 接口服务实例化对象
     */
    private RestService<I, O> restServiceInstance;

    /**
     * 输入参数类型
     */
    private Class<?> parameterType;
    
    /**
     * 输入参数
     */
    private Parameter parameter;

    /**
     * 输出参数类型
     */
    private Class<?> returnType;

    /**
     * 当前接口是否需要加密
     */
    private boolean encrypt;

    /**
     * 当前接口的秘钥，在初始化阶段对秘钥进行校验，防止秘钥错误引起的异常
     */
    private String secret;

    /**
     * 当前接口的秘钥偏移量
     */
    private String iv;

    /**
     * 当前接口使用该加密请求参数
     */
    private String encryptRequestKey;

    /**
     * 当前接口使用该加密返回参数
     */
    private String encryptResponseKey;

    /**
     * 当前接口路径尾部是否拼接 "/"
     */
    private boolean allowEndIsForwardSlash;

    /**
     * 用于jwt传输的userInfo信息
     */
    private Class<?> userInfoClass;
    
    /**
     * 是否强制使用默认的响应体处理器
     */
    private boolean useDefaultResponseEntityHandler;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getRequestMappingPath() {
        return requestMappingPath;
    }

    public void setRequestMappingPath(String requestMappingPath) {
        this.requestMappingPath = requestMappingPath;
    }

    public String getAbbrRequestMappingPath() {
        return abbrRequestMappingPath;
    }

    public void setAbbrRequestMappingPath(String abbrRequestMappingPath) {
        this.abbrRequestMappingPath = abbrRequestMappingPath;
    }

    public Map<String, String> getMethodMap() {
        return methodMap;
    }

    public void setMethodMap(Map<String, String> methodMap) {
        this.methodMap = methodMap;
    }

    public int getMethodPriority() {
        return methodPriority;
    }

    public void setMethodPriority(int methodPriority) {
        this.methodPriority = methodPriority;
    }

    public String getRegisterRequestMappingPath() {
        return registerRequestMappingPath;
    }

    public void setRegisterRequestMappingPath(String registerRequestMappingPath) {
        this.registerRequestMappingPath = registerRequestMappingPath;
    }

    public String getAbbrRegisterRequestMappingPath() {
        return abbrRegisterRequestMappingPath;
    }

    public void setAbbrRegisterRequestMappingPath(String abbrRegisterRequestMappingPath) {
        this.abbrRegisterRequestMappingPath = abbrRegisterRequestMappingPath;
    }

    public ServiceType getType() {
        return type;
    }

    public void setType(ServiceType type) {
        this.type = type;
    }

    public RequestMethod[] getRequestMethods() {
        return requestMethods;
    }

    public void setRequestMethods(RequestMethod[] requestMethods) {
        this.requestMethods = requestMethods;
    }

    public Class<?> getRestControllerClass() {
        return restControllerClass;
    }

    public void setRestControllerClass(Class<?> restControllerClass) {
        this.restControllerClass = restControllerClass;
    }

    public Method getRestControllerMethod() {
        return restControllerMethod;
    }

    public void setRestControllerMethod(Method restControllerMethod) {
        this.restControllerMethod = restControllerMethod;
    }

    public Class<? extends RestService> getRestServiceClass() {
        return restServiceClass;
    }

    public void setRestServiceClass(Class<? extends RestService> restServiceClass) {
        this.restServiceClass = restServiceClass;
    }

    public Method getRestServiceMethod() {
        return restServiceMethod;
    }

    public void setRestServiceMethod(Method restServiceMethod) {
        this.restServiceMethod = restServiceMethod;
    }

    public RestService<I, O> getRestServiceInstance() {
        return restServiceInstance;
    }

    public void setRestServiceInstance(RestService<I, O> restServiceInstance) {
        this.restServiceInstance = restServiceInstance;
    }

    public Class<?> getParameterType() {
        return parameterType;
    }

    public void setParameterType(Class<?> parameterType) {
        this.parameterType = parameterType;
    }
    
    @Nullable
    public Parameter getParameter() {
        return parameter;
    }
    
    public void setParameter(@Nullable Parameter parameter) {
        this.parameter = parameter;
    }
    
    public Class<?> getReturnType() {
        return returnType;
    }

    public void setReturnType(Class<?> returnType) {
        this.returnType = returnType;
    }

    public boolean isEncrypt() {
        return encrypt;
    }

    public void setEncrypt(boolean encrypt) {
        this.encrypt = encrypt;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getIv() {
        return iv;
    }

    public void setIv(String iv) {
        this.iv = iv;
    }

    public String getEncryptRequestKey() {
        return encryptRequestKey;
    }

    public void setEncryptRequestKey(String encryptRequestKey) {
        this.encryptRequestKey = encryptRequestKey;
    }

    public String getEncryptResponseKey() {
        return encryptResponseKey;
    }

    public void setEncryptResponseKey(String encryptResponseKey) {
        this.encryptResponseKey = encryptResponseKey;
    }

    public boolean isAllowEndIsForwardSlash() {
        return allowEndIsForwardSlash;
    }

    public void setAllowEndIsForwardSlash(boolean allowEndIsForwardSlash) {
        this.allowEndIsForwardSlash = allowEndIsForwardSlash;
    }

    public Class<?> getUserInfoClass() {
        return userInfoClass;
    }

    public void setUserInfoClass(Class<?> userInfoClass) {
        this.userInfoClass = userInfoClass;
    }
    
    public boolean isUseDefaultResponseEntityHandler() {
        return useDefaultResponseEntityHandler;
    }
    
    public void setUseDefaultResponseEntityHandler(boolean useDefaultResponseEntityHandler) {
        this.useDefaultResponseEntityHandler = useDefaultResponseEntityHandler;
    }
}
