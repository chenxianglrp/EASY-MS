package com.stars.easyms.rest.handler;

import com.stars.easyms.rest.bean.EasyMsRestContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>className: EasyMsControllerExceptionAdvice</p>
 * <p>description: Controller层异常拦截器</p>
 *
 * @author guoguifang
 * @version 1.7.1
 * @date 2020/12/15 7:08 下午
 */
@Slf4j
@ControllerAdvice
public class EasyMsControllerExceptionAdvice implements Ordered {

    @ExceptionHandler(Throwable.class)
    @ResponseBody
    public Object handleException(Throwable throwable) throws Throwable {
        EasyMsRestContext easyMsRestContext = EasyMsRestSynchronizationManager.getEasyMsRestContext();
        if (easyMsRestContext != null) {
            easyMsRestContext.setThrowable(throwable);
            return ResponseEntityHelper.getResponseEntity(easyMsRestContext, false);
        }
        log.error(throwable.getMessage(), throwable);
        throw throwable;
    }

    @Override
    public int getOrder() {
        return Ordered.LOWEST_PRECEDENCE;
    }
}
