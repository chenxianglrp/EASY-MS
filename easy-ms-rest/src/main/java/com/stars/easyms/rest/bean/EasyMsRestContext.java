package com.stars.easyms.rest.bean;

import com.stars.easyms.base.bean.EasyMsRequestEntity;
import com.stars.easyms.rest.initializer.RequestMappingPathForRestInfo;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.lang.Nullable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>className: EasyMsRestContext</p>
 * <p>description: rest请求上下文</p>
 *
 * @author guoguifang
 * @version 1.6.1
 * @date 2020/8/16 14:17
 */
public class EasyMsRestContext {
    
    /**
     * 请求
     */
    private HttpServletRequest httpServletRequest;
    
    /**
     * 响应
     */
    private HttpServletResponse httpServletResponse;
    
    /**
     * 请求InputMessage
     */
    private HttpInputMessage httpInputMessage;
    
    /**
     * 请求参数
     */
    private EasyMsRequestEntity easyMsRequestEntity;
    
    /**
     * 请求mapping指向的rest信息,只有easy-ms才有该值
     */
    private RequestMappingPathForRestInfo requestMappingPathForRestInfo;
    
    /**
     * rest信息
     */
    private RestInfo restInfo;
    
    /**
     * 请求体json串
     */
    private String requestBodyStr;
    
    /**
     * 请求体对象
     */
    private Object requestBodyObj;
    
    /**
     * 是否返回@ResponseBody类型
     */
    private boolean returnResponseBody;
    
    /**
     * 是否记录返回结果，默认需要记录
     */
    private boolean notRecordReturnData;
    
    /**
     * 结果响应值
     */
    private Object responseBodyObj;
    
    /**
     * 包装的响应值
     */
    private Object responseEntity;
    
    /**
     * 加密后的包装的响应值
     */
    private Object encryptedResponseEntity;
    
    /**
     * rest异常
     */
    private Throwable throwable;
    
    /**
     * 是否是异常信息的responseBody
     */
    private boolean responseBodyWithThrowable;
    
    public void setHttpServletRequest(HttpServletRequest httpServletRequest) {
        this.httpServletRequest = httpServletRequest;
        if (httpServletRequest instanceof RepeatableReaderHttpServletRequest) {
            this.httpInputMessage = new ServletServerHttpRequest(httpServletRequest);
        }
    }
    
    public void setHttpServletResponse(HttpServletResponse httpServletResponse) {
        this.httpServletResponse = httpServletResponse;
    }
    
    public void setEasyMsRequestEntity(EasyMsRequestEntity easyMsRequestEntity) {
        this.easyMsRequestEntity = easyMsRequestEntity;
    }
    
    public void setRequestMappingPathForRestInfo(RequestMappingPathForRestInfo requestMappingPathForRestInfo) {
        this.requestMappingPathForRestInfo = requestMappingPathForRestInfo;
    }
    
    public void setRequestBody(byte[] body) {
        if (httpServletRequest instanceof RepeatableReaderHttpServletRequest) {
            RepeatableReaderHttpServletRequest repeatableReaderHttpServletRequest = (RepeatableReaderHttpServletRequest) this.httpServletRequest;
            repeatableReaderHttpServletRequest.setBody(body);
        }
    }
    
    public void setRequestBodyObj(Object requestBodyObj) {
        this.requestBodyObj = requestBodyObj;
    }
    
    public void setRequestBodyStr(@Nullable String requestBodyStr) {
        this.requestBodyStr = requestBodyStr;
    }
    
    public void setRestInfo(RestInfo restInfo) {
        this.restInfo = restInfo;
    }
    
    public void setResponseBodyObj(Object responseBodyObj) {
        this.responseBodyObj = responseBodyObj;
    }
    
    public void setResponseEntity(Object responseEntity) {
        this.responseEntity = responseEntity;
    }
    
    public void setEncryptedResponseEntity(Object encryptedResponseEntity) {
        this.encryptedResponseEntity = encryptedResponseEntity;
    }
    
    public void setThrowable(Throwable throwable) {
        this.throwable = throwable;
    }
    
    public void setReturnResponseBody(boolean returnResponseBody) {
        this.returnResponseBody = returnResponseBody;
    }
    
    public void setNotRecordReturnData(boolean notRecordReturnData) {
        this.notRecordReturnData = notRecordReturnData;
    }
    
    public void setResponseBodyWithThrowable(boolean responseBodyWithThrowable) {
        this.responseBodyWithThrowable = responseBodyWithThrowable;
    }
    
    public HttpServletRequest getHttpServletRequest() {
        return httpServletRequest;
    }
    
    public HttpServletResponse getHttpServletResponse() {
        return httpServletResponse;
    }
    
    public HttpInputMessage getHttpInputMessage() {
        return httpInputMessage;
    }
    
    public EasyMsRequestEntity getEasyMsRequestEntity() {
        return easyMsRequestEntity;
    }
    
    public RequestMappingPathForRestInfo getRequestMappingPathForRestInfo() {
        return requestMappingPathForRestInfo;
    }
    
    public RestInfo getRestInfo() {
        return restInfo;
    }
    
    public byte[] getRequestBody() {
        if (httpServletRequest instanceof RepeatableReaderHttpServletRequest) {
            RepeatableReaderHttpServletRequest repeatableReaderHttpServletRequest = (RepeatableReaderHttpServletRequest) this.httpServletRequest;
            return repeatableReaderHttpServletRequest.getBody();
        }
        return null;
    }
    
    public String getRequestBodyStr() {
        return requestBodyStr;
    }
    
    public Object getRequestBodyObj() {
        return requestBodyObj;
    }
    
    public boolean isReturnResponseBody() {
        return returnResponseBody;
    }
    
    public boolean isNotRecordReturnData() {
        return notRecordReturnData;
    }
    
    public Object getResponseBodyObj() {
        return responseBodyObj;
    }
    
    public Object getResponseEntity() {
        return responseEntity;
    }
    
    public Object getEncryptedResponseEntity() {
        return encryptedResponseEntity;
    }
    
    public Throwable getThrowable() {
        return throwable;
    }
    
    public boolean isEasyMsRest() {
        return requestMappingPathForRestInfo != null;
    }
    
    public boolean isResponseBodyWithThrowable() {
        return responseBodyWithThrowable;
    }
}
