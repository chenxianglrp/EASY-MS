package com.stars.easyms.rest.bean;

import com.stars.easyms.rest.exception.RestRuntimeException;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

/**
 * 可重复读的HttpServletRequest.
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/7/4 7:55 下午
 */
public class RepeatableReaderHttpServletRequest extends HttpServletRequestWrapper {
    
    private byte[] body;
    
    public RepeatableReaderHttpServletRequest(HttpServletRequest request) {
        super(request);
        this.body = getBodyString(request).getBytes(StandardCharsets.UTF_8);
    }
    
    public void setBody(byte[] body) {
        this.body = body;
    }
    
    public byte[] getBody() {
        return body;
    }
    
    @Override
    public BufferedReader getReader() {
        return new BufferedReader(new InputStreamReader(getInputStream()));
    }
    
    @Override
    public ServletInputStream getInputStream() {
        
        final ByteArrayInputStream bais = new ByteArrayInputStream(body);
        
        return new ServletInputStream() {
            
            @Override
            public synchronized int available() {
                return bais.available();
            }
            
            @Override
            public synchronized int read() {
                int b = bais.read();
                if (b == -1) {
                    this.reset();
                }
                return b;
            }
            
            @Override
            public synchronized int read(byte[] b, int off, int len) {
                int rc = bais.read(b, off, len);
                if (rc <= 0) {
                    this.reset();
                }
                return rc;
            }
            
            @Override
            public void mark(int readAheadLimit) {
                bais.mark(readAheadLimit);
            }
            
            @Override
            public boolean markSupported() {
                return bais.markSupported();
            }
            
            @Override
            public synchronized void reset() {
                bais.reset();
            }
            
            @Override
            public synchronized long skip(long n) {
                return bais.skip(n);
            }
            
            @Override
            public boolean isFinished() {
                return false;
            }
            
            @Override
            public boolean isReady() {
                return false;
            }
            
            @Override
            public void setReadListener(ReadListener readListener) {
            }
        };
    }
    
    private String getBodyString(HttpServletRequest request) {
        StringBuilder sb = new StringBuilder();
        try (InputStream inputStream = request.getInputStream();
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(inputStream, StandardCharsets.UTF_8))) {
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            throw new RestRuntimeException("I/O error while reading request body input message", e);
        }
        return sb.toString();
    }
}