package com.stars.easyms.rest.properties;

import com.stars.easyms.base.constant.EasyMsCommonConstants;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Set;

/**
 * EasyMs的REST属性类型
 *
 * @author guoguifang
 * @date 2018-10-12 11:24
 * @since 1.0.0
 */
@ConfigurationProperties(prefix = EasyMsRestProperties.PREFIX)
public class EasyMsRestProperties {

    public static final String PREFIX = EasyMsCommonConstants.EASY_MS_PROPERTIES_PREFIX + "rest";

    /**
     * Scanning Package Path, multiple split using ','
     */
    private Set<String> basePackage;

    /**
     * URL pattern for rest filter, default is /*
     */
    private Set<String> urlPatterns;

    /**
     * Duplicate request removal is or not enabled, default false
     */
    private boolean duplicateRemovalEnabled;

    /**
     * Rest filter chain order.
     */
    private int order = 0;

    /**
     * Whether default use FastJsonHttpMessageConverter.
     */
    private boolean fastJson = true;

    /**
     * Whether global encrypt, value default false
     */
    private boolean encrypt;

    /**
     * Global secret key, if null default not encrypted
     */
    private String secret;

    /**
     * Global secret iv, if null default not encrypted
     */
    private String iv;

    /**
     * Global encryption request parameters
     */
    private String encryptRequestKey;

    /**
     * Global encryption response parameters
     */
    private String encryptResponseKey;

    /**
     * Whether allow end is forward slash '/', value default false
     */
    private boolean allowEndIsForwardSlash;

    /**
     * UserInfo information for JWT transfer
     */
    private String userInfoClass;

    /**
     * Interface execution time limit, beyond which an alarm is raised, in milliseconds, default 1000ms
     */
    private Long alarmLimitTime = 2000L;

    /**
     * Whether or not to print the request, default true
     */
    private boolean logRequest = true;

    /**
     * Whether or not to print the response, default true
     */
    private boolean logResponse = true;

    public Set<String> getBasePackage() {
        return basePackage;
    }

    public void setBasePackage(Set<String> basePackage) {
        this.basePackage = basePackage;
    }

    public Set<String> getUrlPatterns() {
        return urlPatterns;
    }

    public void setUrlPatterns(Set<String> urlPatterns) {
        this.urlPatterns = urlPatterns;
    }

    public boolean isDuplicateRemovalEnabled() {
        return duplicateRemovalEnabled;
    }

    public void setDuplicateRemovalEnabled(boolean duplicateRemovalEnabled) {
        this.duplicateRemovalEnabled = duplicateRemovalEnabled;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public boolean isFastJson() {
        return fastJson;
    }

    public void setFastJson(boolean fastJson) {
        this.fastJson = fastJson;
    }

    public boolean isEncrypt() {
        return encrypt;
    }

    public void setEncrypt(boolean encrypt) {
        this.encrypt = encrypt;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getIv() {
        return iv;
    }

    public void setIv(String iv) {
        this.iv = iv;
    }

    public String getEncryptRequestKey() {
        return encryptRequestKey;
    }

    public void setEncryptRequestKey(String encryptRequestKey) {
        this.encryptRequestKey = encryptRequestKey;
    }

    public String getEncryptResponseKey() {
        return encryptResponseKey;
    }

    public void setEncryptResponseKey(String encryptResponseKey) {
        this.encryptResponseKey = encryptResponseKey;
    }

    public boolean isAllowEndIsForwardSlash() {
        return allowEndIsForwardSlash;
    }

    public void setAllowEndIsForwardSlash(boolean allowEndIsForwardSlash) {
        this.allowEndIsForwardSlash = allowEndIsForwardSlash;
    }

    public String getUserInfoClass() {
        return userInfoClass;
    }

    public void setUserInfoClass(String userInfoClass) {
        this.userInfoClass = userInfoClass;
    }

    public Long getAlarmLimitTime() {
        return alarmLimitTime;
    }

    public void setAlarmLimitTime(Long alarmLimitTime) {
        this.alarmLimitTime = alarmLimitTime;
    }

    public void setLogRequest(boolean logRequest) {
        this.logRequest = logRequest;
    }

    public boolean isLogRequest() {
        return logRequest;
    }

    public void setLogResponse(boolean logResponse) {
        this.logResponse = logResponse;
    }

    public boolean isLogResponse() {
        return logResponse;
    }
}
