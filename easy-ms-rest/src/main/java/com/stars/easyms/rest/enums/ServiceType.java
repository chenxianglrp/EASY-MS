package com.stars.easyms.rest.enums;

/**
 * 服务类型 T：交易类服务 Q:查询类服务
 *
 * @author guoguifang
 * @date 2018-10-12 11:24
 * @since 1.0.0
 */
public enum ServiceType {

    /**
     * 交易类服务
     */
    TRANSACTION("T", "交易类服务"),

    /**
     * 查询类服务
     */
    QUERY("Q", "查询类服务");

    private String code;

    private String name;

    public static ServiceType forCode(String code) {
        if (TRANSACTION.code.equalsIgnoreCase(code)) {
            return TRANSACTION;
        } else if (QUERY.code.equalsIgnoreCase(code)) {
            return QUERY;
        }
        return null;
    }

    ServiceType(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
