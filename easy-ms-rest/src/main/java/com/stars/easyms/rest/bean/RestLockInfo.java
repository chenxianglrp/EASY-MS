package com.stars.easyms.rest.bean;

/**
 * <p>className: RestLockInfo</p>
 * <p>description: Rest的锁信息</p>
 *
 * @author guoguifang
 * @version 1.2.1
 * @date 2019-07-11 16:05
 */
public final class RestLockInfo {

    /**
     * 锁名称
     */
    private String lockName;

    /**
     * 过期时间，单位秒
     */
    private int expireSecond = 60;

    /**
     * 超时时间，单位毫秒
     */
    private long timeout;

    /**
     * 失败后每次重试时间间隔
     */
    private long retryInterval = 100;

    /**
     * 加锁失败后的返回信息
     */
    private String lockFailMsg;

    public String getLockName() {
        return lockName;
    }

    public void setLockName(String lockName) {
        this.lockName = lockName;
    }

    public int getExpireSecond() {
        return expireSecond;
    }

    public void setExpireSecond(int expireSecond) {
        this.expireSecond = expireSecond;
    }

    public long getTimeout() {
        return timeout;
    }

    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }

    public long getRetryInterval() {
        return retryInterval;
    }

    public void setRetryInterval(long retryInterval) {
        this.retryInterval = retryInterval;
    }

    public String getLockFailMsg() {
        return lockFailMsg;
    }

    public void setLockFailMsg(String lockFailMsg) {
        this.lockFailMsg = lockFailMsg;
    }
}