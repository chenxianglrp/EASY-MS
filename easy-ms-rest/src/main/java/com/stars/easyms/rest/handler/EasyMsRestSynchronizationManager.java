package com.stars.easyms.rest.handler;

import com.stars.easyms.rest.bean.EasyMsRestContext;
import org.springframework.core.NamedThreadLocal;

/**
 * <p>className: EasyMsRestSynchronizationManager</p>
 * <p>description: EasyMs的rest接口同步管理器</p>
 *
 * @author guoguifang
 * @date 2019-12-04 13:38
 * @since 1.4.2
 */
public final class EasyMsRestSynchronizationManager {

    private static final ThreadLocal<EasyMsRestContext> EASY_MS_REST_CONTEXT_THREAD_LOCAL = new NamedThreadLocal<>("Current rest context");

    static void setEasyMsRestContext(EasyMsRestContext restRequest) {
        EASY_MS_REST_CONTEXT_THREAD_LOCAL.set(restRequest);
    }

    public static EasyMsRestContext getEasyMsRestContext() {
        return EASY_MS_REST_CONTEXT_THREAD_LOCAL.get();
    }

    static void clear() {
        EASY_MS_REST_CONTEXT_THREAD_LOCAL.remove();
    }

    private EasyMsRestSynchronizationManager() {
    }

}