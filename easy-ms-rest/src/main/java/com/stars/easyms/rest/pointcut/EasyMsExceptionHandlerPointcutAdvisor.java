package com.stars.easyms.rest.pointcut;

import com.stars.easyms.base.pointcut.EasyMsAnnotationMatchingPointcut;
import com.stars.easyms.rest.handler.EasyMsExceptionHandlerAspect;
import org.aopalliance.aop.Advice;
import org.springframework.aop.Pointcut;
import org.springframework.aop.support.AbstractPointcutAdvisor;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * <p>className: EasyMsExceptionHandlerPointcutAdvisor</p>
 * <p>description: EasyMs的controller层ExceptionHandler方法拦截器</p>
 *
 * @author guoguifang
 * @version 1.7.1
 * @date 2020/12/16 10:03 上午
 */
public final class EasyMsExceptionHandlerPointcutAdvisor extends AbstractPointcutAdvisor {

    private transient EasyMsExceptionHandlerAspect advice;

    private transient EasyMsAnnotationMatchingPointcut pointcut;

    public EasyMsExceptionHandlerPointcutAdvisor() {
        this.advice = new EasyMsExceptionHandlerAspect();
        this.pointcut = new EasyMsAnnotationMatchingPointcut();
        this.pointcut.setMethodMatcher(ExceptionHandler.class, true);
    }

    @Override
    public Pointcut getPointcut() {
        return this.pointcut;
    }

    @Override
    public Advice getAdvice() {
        return this.advice;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || this.getClass() != other.getClass()) {
            return false;
        }
        EasyMsExceptionHandlerPointcutAdvisor otherAdvisor = (EasyMsExceptionHandlerPointcutAdvisor) other;
        return (ObjectUtils.nullSafeEquals(this.advice, otherAdvisor.advice) &&
                ObjectUtils.nullSafeEquals(this.pointcut, otherAdvisor.pointcut));
    }

    @Override
    public int hashCode() {
        return 41 + this.advice.hashCode() * 41 + this.pointcut.hashCode() * 41;
    }

}