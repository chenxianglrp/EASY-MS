package com.stars.easyms.rest.annotation;

import java.lang.annotation.*;

/**
 * <p>className: EasyMsRestController</p>
 * <p>description: EasyMsRestController接口类注解，只可用于类或接口上</p>
 *
 * @author guoguifang
 * @version 1.2.2
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EasyMsRestController {
    
    /**
     * 类名称，用于在swagger场景下显示
     * For example {@code @EasyMsRestController(name = "例子")}.
     */
    String name();
    
    /**
     * 在该类中的所有方法级映射都继承这个主映射
     * For example {@code @EasyMsRestController(path = "/demo")}.
     */
    String path() default "";

}
