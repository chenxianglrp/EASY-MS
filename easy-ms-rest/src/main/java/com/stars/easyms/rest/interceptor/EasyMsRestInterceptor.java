package com.stars.easyms.rest.interceptor;

import com.stars.easyms.rest.bean.EasyMsRestContext;
import org.springframework.lang.NonNull;

/**
 * <p>className: EasyMsRestInterceptor</p>
 * <p>description: rest接口调用切面接口</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/11/17 4:53 下午
 */
public interface EasyMsRestInterceptor {
    
    /**
     * 在初始化状态(未解析RequestBody时)的可扩展的自定义操作
     *
     * @param easyMsRestContext rest请求上下文
     */
    default void initRest(@NonNull EasyMsRestContext easyMsRestContext) throws Exception {
    }
    
    /**
     * 在处理rest接口前的可扩展的自定义操作
     *
     * @param easyMsRestContext rest请求上下文
     */
    default void beforeRest(@NonNull EasyMsRestContext easyMsRestContext) throws Exception {
    }
    
    /**
     * 在处理rest接口后的可扩展的自定义操作
     *
     * @param easyMsRestContext rest请求上下文
     */
    default void afterRest(@NonNull EasyMsRestContext easyMsRestContext) throws Exception {
    }
}
