package com.stars.easyms.rest.function;

import com.stars.easyms.rest.bean.EasyMsRestContext;
import org.springframework.lang.NonNull;

/**
 * <p>className: EasyMsRestFunction</p>
 * <p>description: EasyMs的rest处理接口</p>
 *
 * @author guoguifang
 * @version 1.2.2
 * @date 2019-08-01 11:23
 */
@FunctionalInterface
public interface EasyMsRestFunction {

    /**
     * 根据请求参数和restInfo信息获取返回值
     *
     * @param restRequest 请求参数
     * @return 返回EasyMsResponseEntity对象
     * @throws Exception 允许抛出异常
     */
    @NonNull
    Object execute(EasyMsRestContext restRequest) throws Exception;

}