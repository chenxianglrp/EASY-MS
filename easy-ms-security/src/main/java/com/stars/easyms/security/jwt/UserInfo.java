package com.stars.easyms.security.jwt;

import io.jsonwebtoken.Claims;
import lombok.Data;
import org.springframework.lang.Nullable;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * <p>interfaceName: UserInfo</p>
 * <p>description: jwt保存的用户信息</p>
 *
 * @author guoguifang
 * @version 1.6.1
 * @date 2020-08-18 09:33
 */
@Data
public class UserInfo {

    private Long userId;

    private String mobile;

    Map<String, Object> toClaims() {
        Map<String, Object> claims = new LinkedHashMap<>(4);
        claims.put("userId", userId);
        claims.put("mobile", mobile);
        return claims;
    }

    @Nullable
    static UserInfo parseClaims(Claims claims) {
        Long cUserId = claims.get("userId", Long.class);
        if (cUserId == null) {
            return null;
        }

        String cMobile = claims.get("mobile", String.class);
        if (cMobile == null) {
            return null;
        }

        UserInfo userInfo = new UserInfo();
        userInfo.userId = cUserId;
        userInfo.mobile = cMobile;
        return userInfo;
    }
}
