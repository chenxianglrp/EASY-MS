package com.stars.easyms.security.constant;

import java.util.ArrayList;
import java.util.List;

/**
 * 常量类
 *
 * @author guoguifang
 * @date 2020-08-12 17:24
 * @since 1.6.0
 */
public final class EasyMsSecurityConstants {

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String USER = "ROLE_USER";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

    public static final String PROPERTIES_SEPARATOR = ",";

    public static final String HEADER_TOKEN_PREFIX = "Bearer ";

    public static final List<String> EASY_MS_URL = new ArrayList<>();

    static {
        EASY_MS_URL.add("/api/**");
        EASY_MS_URL.add("/easy-ms-swagger/**");
        EASY_MS_URL.add("/swagger/**");
        EASY_MS_URL.add("/swagger-resources");
        EASY_MS_URL.add("/v2/api-docs");
        EASY_MS_URL.add("/webjars/**");
        EASY_MS_URL.add("/easy-ms/**");
        EASY_MS_URL.add("/datasource/**");
        EASY_MS_URL.add("/git/**");
        EASY_MS_URL.add("/redis/**");
        EASY_MS_URL.add("/mq/**");
        EASY_MS_URL.add("/schedule/**");
        EASY_MS_URL.add("/error");
    }

    private EasyMsSecurityConstants() {
    }
}
