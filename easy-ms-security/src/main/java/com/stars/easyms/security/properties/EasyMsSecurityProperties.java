package com.stars.easyms.security.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * EasyMsSecurity属性类
 *
 * @author guoguifang
 * @date 2020-08-12 17:24
 * @since 1.6.0
 */
@Data
@ConfigurationProperties(prefix = EasyMsSecurityProperties.PREFIX)
public class EasyMsSecurityProperties {

    public static final String PREFIX = "easy-ms.security";

    /**
     * 是否激活security的权限校验
     */
    private boolean enabled;

    /**
     * 不需要security权限控制的url
     */
    private String permit;

    /**
     * jwt
     */
    private final JsonWebToken jwt = new JsonWebToken();

    @Data
    public static class JsonWebToken {

        /**
         * 是否激活jwt
         */
        private boolean enabled;

        /**
         * 不需要jwt权限控制的url
         */
        private String permit;

        /**
         * 存放在header头的key值，如不填写默认"Authorization"
         */
        private String headerKey = "Authorization";

        /**
         * jwt的发行方
         */
        private String issuer;

        /**
         * jwt的秘钥(明文)
         */
        private String secret;

        /**
         * jwt的秘钥(base64)
         */
        private String base64Secret;

        /**
         * token的有效时间(单位：秒)：默认30分钟
         */
        private long tokenValidityInSeconds = 1800L;

        /**
         * token在选了记住我的情况下的有效时间(单位：秒)：默认72小时
         */
        private long tokenValidityInSecondsForRememberMe = 2592000L;
    }

}
