create user 'test'@'%' IDENTIFIED by 'test';

grant all PRIVILEGES on test_master1.* to 'test'@'%' with grant option;
grant all PRIVILEGES on test_master2.* to 'test'@'%' with grant option;
grant all PRIVILEGES on other1_master1.* to 'test'@'%' with grant option;
grant all PRIVILEGES on other1_master2.* to 'test'@'%' with grant option;
grant all PRIVILEGES on other2_master1.* to 'test'@'%' with grant option;
grant all PRIVILEGES on other2_master2.* to 'test'@'%' with grant option;
grant all PRIVILEGES on test_slave1.* to 'test'@'%' with grant option;
grant all PRIVILEGES on test_slave2.* to 'test'@'%' with grant option;
grant all PRIVILEGES on other1_slave1.* to 'test'@'%' with grant option;
grant all PRIVILEGES on other1_slave2.* to 'test'@'%' with grant option;
grant all PRIVILEGES on other2_slave1.* to 'test'@'%' with grant option;
grant all PRIVILEGES on other2_slave2.* to 'test'@'%' with grant option;