use test_master1;
DROP TABLE IF EXISTS user_test;
CREATE TABLE user_test
(
    `name`       varchar(32) not null,
    `age`        int         not null,
    `login_time` datetime    not null,
    PRIMARY KEY (`name`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 comment ='用户测试表';

use test_master2;
DROP TABLE IF EXISTS user_test;
CREATE TABLE user_test
(
    `name`       varchar(32) not null,
    `age`        int         not null,
    `login_time` datetime    not null,
    PRIMARY KEY (`name`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 comment ='用户测试表';

use other1_master1;
DROP TABLE IF EXISTS user_test;
CREATE TABLE user_test
(
    `name`       varchar(32) not null,
    `age`        int         not null,
    `login_time` datetime    not null,
    PRIMARY KEY (`name`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 comment ='用户测试表';

use other1_master2;
DROP TABLE IF EXISTS user_test;
CREATE TABLE user_test
(
    `name`       varchar(32) not null,
    `age`        int         not null,
    `login_time` datetime    not null,
    PRIMARY KEY (`name`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 comment ='用户测试表';

use other2_master1;
DROP TABLE IF EXISTS user_test;
CREATE TABLE user_test
(
    `name`       varchar(32) not null,
    `age`        int         not null,
    `login_time` datetime    not null,
    PRIMARY KEY (`name`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 comment ='用户测试表';

use other2_master2;
DROP TABLE IF EXISTS user_test;
CREATE TABLE user_test
(
    `name`       varchar(32) not null,
    `age`        int         not null,
    `login_time` datetime    not null,
    PRIMARY KEY (`name`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 comment ='用户测试表';

use test_slave1;
DROP TABLE IF EXISTS user_test;
CREATE TABLE user_test
(
    `name`       varchar(32) not null,
    `age`        int         not null,
    `login_time` datetime    not null,
    PRIMARY KEY (`name`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 comment ='用户测试表';

use test_slave2;
DROP TABLE IF EXISTS user_test;
CREATE TABLE user_test
(
    `name`       varchar(32) not null,
    `age`        int         not null,
    `login_time` datetime    not null,
    PRIMARY KEY (`name`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 comment ='用户测试表';

use other1_slave1;
DROP TABLE IF EXISTS user_test;
CREATE TABLE user_test
(
    `name`       varchar(32) not null,
    `age`        int         not null,
    `login_time` datetime    not null,
    PRIMARY KEY (`name`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 comment ='用户测试表';

use other1_slave2;
DROP TABLE IF EXISTS user_test;
CREATE TABLE user_test
(
    `name`       varchar(32) not null,
    `age`        int         not null,
    `login_time` datetime    not null,
    PRIMARY KEY (`name`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 comment ='用户测试表';

use other2_slave1;
DROP TABLE IF EXISTS user_test;
CREATE TABLE user_test
(
    `name`       varchar(32) not null,
    `age`        int         not null,
    `login_time` datetime    not null,
    PRIMARY KEY (`name`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 comment ='用户测试表';

use other2_slave2;
DROP TABLE IF EXISTS user_test;
CREATE TABLE user_test
(
    `name`       varchar(32) not null,
    `age`        int         not null,
    `login_time` datetime    not null,
    PRIMARY KEY (`name`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 comment ='用户测试表';

