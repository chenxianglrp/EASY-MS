package com.stars.easyms.demo.rest.dto.input.sentinel;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SentinelDemo7Input {

    private static final long serialVersionUID = 1332056467576302896L;

    @ApiModelProperty(value = "授权类型(0: 白名单, 1: 黑名单)", required = true, position = 1, example = "0")
    private int strategy;

    @ApiModelProperty(value = "限制app名称，用','分割", required = true, position = 2, example = "0")
    private String limitApp;
}
