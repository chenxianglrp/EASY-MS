package com.stars.easyms.demo.rest.service.jdk8;

import com.stars.easyms.rest.RestService;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * <p>className: EasyMsJdk8Demo8Service</p>
 * <p>description: Optional容器</p>
 *
 * @author guoguifang
 * @date 2019/9/23 16:53
 * @since 1.3.2
 */
@Service
public class EasyMsJdk8Demo8Service implements RestService<Void, Void> {

    @Override
    public Void execute(Void input) {

        Integer value1 = null;
        Integer value2 = new Integer(10);
        // Optional.ofNullable - 允许传递为 null 参数
        Optional<Integer> a = Optional.ofNullable(value1);
        // Optional.of - 如果传递的参数是 null，抛出异常 NullPointerException
        Optional<Integer> b = Optional.of(value2);
        // Optional.isPresent - 判断值是否存在
        System.out.println("第一个参数值存在: " + a.isPresent());
        System.out.println("第二个参数值存在: " + b.isPresent());
        // Optional.orElse - 如果值存在，返回它，否则返回默认值
        Integer value3 = a.orElse(new Integer(5));
        //Optional.get - 获取值，值需要存在
        Integer value4 = b.get();
        System.out.println(value3 + value4);

        return null;
    }

}