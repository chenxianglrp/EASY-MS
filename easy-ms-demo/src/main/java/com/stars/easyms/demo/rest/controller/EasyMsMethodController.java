package com.stars.easyms.demo.rest.controller;

import com.stars.easyms.demo.rest.dto.input.method.*;
import com.stars.easyms.demo.rest.dto.output.MethodDemo1Output;
import com.stars.easyms.demo.rest.service.method.*;
import com.stars.easyms.rest.annotation.EasyMsRestController;
import com.stars.easyms.rest.annotation.EasyMsRestMapping;
/**
 * <p>className: EasyMsEncryptController</p>
 * <p>description: 相同的path，但传入参数的不同而调用不同的接口
 * method是传入参数的字段名称，methodFor是传入参数的值，只有当全部匹配时才能成功调用到相应接口</p>
 *
 * @author guoguifang
 * @version 1.2.2
 * @date 2019-08-06 16:22
 */
@EasyMsRestController(name = "method")
public interface EasyMsMethodController {

    @EasyMsRestMapping(code = "method", service = EasyMsMethodDemo1Service.class, type = "Q",
            name = "demo-v1-type-demo", secret = "abcdefghi1000000", iv = "abcdefghi1000000", notes = "普通接口demo")
    MethodDemo1Output demo(MethodDemo1Input input);

    @EasyMsRestMapping(code = "method", method = "type1", methodFor = "demo1",
            service = EasyMsMethodDemo1Service.class, type = "Q",
            secret = "abcdefghi1000000", iv = "abcdefghi1000000", name = "demo-v1-type-demo1", notes = "单method的demo")
    MethodDemo1Output demo1(MethodDemo1Input input);

    @EasyMsRestMapping(code = "method", method = {"type1", "type2"}, methodFor = {"demo1", "demo2"},
            service = EasyMsMethodDemo2Service.class, type = "Q",
            secret = "abcdefghi1000000", iv = "abcdefghi1000000", notes = "双method的demo", name = "多method的demo")
    Void demo2(MethodDemo2Input input);

    @EasyMsRestMapping(code = "method", method = {"type1", "type2", "type3"}, methodFor = {"demo1", "demo2", "demo3"},
            service = EasyMsMethodDemo3Service.class, type = "Q", name = "demo-v1-type-demo3",
            secret = "abcdefghi1000000", iv = "abcdefghi1000000", notes = "三method的demo")
    Void demo3(MethodDemo3Input input);

    @EasyMsRestMapping(code = "method", method = {"type1", "type2", "type3", "type4"}, methodFor = {"demo1", "demo2", "demo3", "demo4"},
            service = EasyMsMethodDemo4Service.class, type = "Q", name = "demo-v1-type-demo4",
            secret = "abcdefghi1000000", iv = "abcdefghi1000000", notes = "四method的demo")
    Void demo4(MethodDemo4Input input);

    @EasyMsRestMapping(code = "method", method = {"type2", "type5"}, methodFor = {"demo2", "demo5"},
            service = EasyMsMethodDemo5Service.class, type = "Q", name = "demo-v1-type-demo5",
            secret = "abcdefghi1000000", iv = "abcdefghi1000000", notes = "双method的demo")
    Void demo5(MethodDemo5Input input);

    @EasyMsRestMapping(code = "method6", version = "v2", method = {"type1", "type2"}, methodFor = {"demo1", "demo2"}, methodPriority = 1,
            service = EasyMsMethodDemo6Service.class, type = "Q", name = "demo-v2-type-demo6",
            secret = "abcdefghi1000000", iv = "abcdefghi1000000", notes = "非默认版本、双method、优先级高(值越小优先级越高)的demo")
    Void demo6(MethodDemo6Input input);

    @EasyMsRestMapping(code = "method6", version = "v2", method = {"type2", "type3"}, methodFor = {"demo2", "demo3"}, methodPriority = 2,
            service = EasyMsMethodDemo7Service.class, type = "Q", name = "demo-v2-type-demo7",
            secret = "abcdefghi1000000", iv = "abcdefghi1000000", notes = "非默认版本、双method、优先级低(值越小优先级越高)的demo")
    Void demo7(MethodDemo7Input input);
}