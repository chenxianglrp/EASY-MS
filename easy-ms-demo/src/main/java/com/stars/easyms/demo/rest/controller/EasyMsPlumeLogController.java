package com.stars.easyms.demo.rest.controller;

import com.stars.easyms.demo.rest.service.plumelog.EasyMsPlumeLogDemoService;
import com.stars.easyms.rest.annotation.EasyMsRestController;
import com.stars.easyms.rest.annotation.EasyMsRestMapping;

/**
 * <p>className: EasyMsPlumeLogController</p>
 * <p>description: EasyMsPlumeLogController</p>
 *
 * @author guoguifang
 * @version 1.7.3
 * @date 2021/3/23 5:44 下午
 */
@EasyMsRestController(path = "plumeLog", name = "PlumeLog使用方法")
public interface EasyMsPlumeLogController {

    @EasyMsRestMapping(service = EasyMsPlumeLogDemoService.class, name = "PlumeLog")
    Void demo1(Void input);

}