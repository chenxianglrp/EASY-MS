package com.stars.easyms.demo.rest.service.datasource;

import com.stars.easyms.demo.repository.dao.TestDAO;
import com.stars.easyms.rest.RestService;
import com.stars.easyms.base.util.MessageFormatUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class EasyMsDatasourceDemo1Service implements RestService<Void, Void> {

    @Autowired
    private TestDAO testDao;

    @Override
    public Void execute(Void input) {
        testDao.insertData_test_master();
        testDao.insertData_test_slave();
        testDao.insertData_other1_master();
        testDao.insertData_other1_slave();
        testDao.insertData_other2_master();
        testDao.insertData_other2_slave();
        String retMsg = MessageFormatUtil.format("Rest Service[{}], param[{}]!", this.getClass().getSimpleName(), input);
        System.out.println(retMsg);
        return null;
    }

}