package com.stars.easyms.demo.rest.dto.input.sentinel;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SentinelDemo6Input {

    private static final long serialVersionUID = 1332056467576302896L;

    @ApiModelProperty(value = "执行次数", required = true, position = 0, example = "30")
    private int times;

    @ApiModelProperty(value = "最大系统负载，-1为不检查", required = true, position = 1, example = "3.0")
    private double highestSystemLoad;

    @ApiModelProperty(value = "最大cpu使用率([0-1])，-1为不检查", required = true, position = 2, example = "0.6")
    private double highestCpuUsage;

    @ApiModelProperty(value = "入口允许最大的每秒请求量", required = true, position = 3, example = "20")
    private double qps;

    @ApiModelProperty(value = "所有入口流量的平均响应时间", required = true, position = 4, example = "10")
    private long avgRt;

    @ApiModelProperty(value = "入口流量的最大并发数", required = true, position = 5, example = "10")
    private long maxThread;
}
