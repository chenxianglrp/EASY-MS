package com.stars.easyms.demo.rest.service.mybatisplus;

import com.stars.easyms.demo.repository.entity.UserTest;
import com.stars.easyms.demo.service.ITestUserService;
import com.stars.easyms.rest.RestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class EasyMsMybatisPlusDemo4Service implements RestService<Void, Void> {

    @Autowired
    private ITestUserService iTestUserService;

    @Override
    public Void execute(Void input) {
        List<UserTest> userTests = new ArrayList<>();
        userTests.add(UserTest.builder().name("1").age(1).loginTime(LocalDateTime.now()).build());
        userTests.add(UserTest.builder().name("2").age(2).loginTime(LocalDateTime.now()).build());
        userTests.add(UserTest.builder().name("3").age(3).loginTime(LocalDateTime.now()).build());
        userTests.add(UserTest.builder().name("4").age(4).loginTime(LocalDateTime.now()).build());
        userTests.forEach(iTestUserService::insert);
        return null;
    }

}