package com.stars.easyms.demo.repository.dao;

import com.github.pagehelper.Page;
import com.stars.easyms.datasource.annotation.SpecifyDataSource;
import com.stars.easyms.datasource.annotation.SpecifyMasterDataSource;
import com.stars.easyms.datasource.annotation.SpecifySlaveDataSource;
import com.stars.easyms.demo.repository.entity.UserTest;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @EasyMsRepository不指定属性为默认数据源，默认查询走从库
 */
//@EasyMsRepository(value = "testDAO", datasource = "other1", query = DataSourceType.MASTER)
@Mapper
public interface TestDAO {

    // 方法上不加注解则默认按照类上注解执行
    // 此方法走的是默认数据源的主库
    int insertData_test_master();

    // 使用@SpecifySlaveDataSource强制指定从库，但是insert操作必须走主库，该注解是不生效的
    // 此方法走的是默认数据源的主库
    @SpecifySlaveDataSource
    int insertData_test_slave();

    // 指定数据源名称为"other1"，方法优先类
    // 该方法走的是other1的主库
    @SpecifyDataSource("other1")
    int insertData_other1_master();

    // 指定数据源名称为"other1"，方法优先类，但是@SpecifySlaveDataSource注解不生效，insert强制走主库
    // 此方法走的是other1的主库
    @SpecifyDataSource("other1")
    @SpecifySlaveDataSource
    int insertData_other1_slave();

    // 指定数据源名称为"other2"，方法优先类
    // 该方法走的是other2的主库
    @SpecifyDataSource("other2")
    int insertData_other2_master();

    // 指定数据源名称为"other2"，方法优先类，但是@SpecifySlaveDataSource注解不生效，insert强制走主库
    // 此方法走的是other2的主库
    @SpecifyDataSource("other2")
    @SpecifySlaveDataSource
    int insertData_other2_slave();

    // 使用@SpecifyMasterDataSource强制指定主库，没有指定数据源
    // 此方法走的是默认数据源的主库
    @SpecifyMasterDataSource
    List<UserTest> queryData_test_master();

    // 方法上不加注解，查询方法默认走从库
    // 此方法走的是默认数据源的从库
    List<UserTest> queryData_test_slave();

    // 指定数据源为"other1"，并使用注解@SpecifyMasterDataSource强制为主库
    // 此方法走的是other1的主库
    @SpecifyDataSource("other1")
    @SpecifyMasterDataSource
    List<UserTest> queryData_other1_master();

    // 指定数据源为"other1"，查询默认走从库
    // 此方法走的是other1的从库
    @SpecifyDataSource("other1")
    List<UserTest> queryData_other1_slave();

    // 指定数据源为"other2"，查询默认走从库
    // 此方法走的是other2的从库
    @SpecifyDataSource("other2")
    List<UserTest> queryData_other2_master();

    // 指定数据源为"other2"，并使用注解@SpecifyMasterDataSource强制为主库
    //此方法走的是other2的主库
    @SpecifyDataSource("other2")
    @SpecifyMasterDataSource
    List<UserTest> queryData_other2_slave();

    @SpecifyMasterDataSource
    Page<UserTest> queryUserInfo();

    @SpecifyMasterDataSource
    Page<UserTest> queryUserInfoByPaging();
}