package com.stars.easyms.demo.rest.service.jdk8.demo10;

public @interface Hints {
    Hint[] value();
}