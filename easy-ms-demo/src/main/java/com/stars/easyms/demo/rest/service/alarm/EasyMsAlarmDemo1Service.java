package com.stars.easyms.demo.rest.service.alarm;

import com.stars.easyms.alarm.EasyMsAlarmService;
import com.stars.easyms.alarm.message.EasyMsExceptionAlarmMessage;
import com.stars.easyms.demo.rest.dto.input.alarm.AlarmInput;
import com.stars.easyms.rest.RestService;
import com.stars.easyms.rest.exception.BusinessRestException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * <p>className: EasyMsAlarmService</p>
 * <p>description: 告警</p>
 *
 * @author caotieshuan
 * @version 1.0.0
 * @date 2019-12-23 09:54
 */
public class EasyMsAlarmDemo1Service implements RestService<AlarmInput, Void> {

    @Autowired
    private EasyMsAlarmService sendAlarmService;

    @Override
    public Void execute(AlarmInput input) throws BusinessRestException {
        EasyMsExceptionAlarmMessage exceptionAlarmMessage = new EasyMsExceptionAlarmMessage();
        exceptionAlarmMessage.setContext("abc");
        sendAlarmService.sendExceptionAlarmMessage(exceptionAlarmMessage);
        return null;
    }
}