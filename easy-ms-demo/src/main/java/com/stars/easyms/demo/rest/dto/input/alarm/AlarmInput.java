package com.stars.easyms.demo.rest.dto.input.alarm;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>className: AlarmInput</p>
 * <p>description: 告警入参</p>
 *
 * @author cts
 * @date 2019-12-23 09:55
 * @version 1.0.0
 */
@Data
public class AlarmInput {

    @ApiModelProperty(value = "告警标题",required = true,example = "系统处理异常")
    public String title;

    @ApiModelProperty(value = "告警内容",required = true,example = "系统处理异常")
    public String content;
}