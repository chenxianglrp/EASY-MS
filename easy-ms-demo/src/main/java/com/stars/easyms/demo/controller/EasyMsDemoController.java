package com.stars.easyms.demo.controller;

import com.stars.easyms.demo.repository.entity.UserTest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

/**
 * <p>className: EasyMsDemoController</p>
 * <p>description: EasyMsDemoController</p>
 *
 * @author guoguifang
 * @version 1.7.1
 * @date 2020/12/11 2:53 下午
 */
@Controller
public class EasyMsDemoController {

    @GetMapping(path = "/getUserTest")
    @ResponseBody
    public UserTest getUserTest(@RequestBody String name, @RequestParam("name") String aaa) {
        System.out.println(name);
        return UserTest.builder().name(aaa).age(10).loginTime(LocalDateTime.now()).build();
    }

    @PostMapping(path = "/postUserTest")
    @ResponseBody
    public UserTest postUserTest(@RequestBody String name, @RequestParam("name") String aaa) {
        System.out.println(name);
        return UserTest.builder().name(aaa).age(11).loginTime(LocalDateTime.now()).build();
    }
}
