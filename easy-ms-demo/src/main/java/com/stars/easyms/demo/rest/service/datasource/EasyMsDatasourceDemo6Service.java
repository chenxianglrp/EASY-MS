package com.stars.easyms.demo.rest.service.datasource;

import com.stars.easyms.base.util.MessageFormatUtil;
import com.stars.easyms.demo.service.TestService;
import com.stars.easyms.rest.RestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class EasyMsDatasourceDemo6Service implements RestService<Void, Void> {

    @Autowired
    private TestService testService;

    @Override
    public Void execute(Void input) {
        testService.insertWithTransaction();
        String retMsg = MessageFormatUtil.format("Rest Service[{}], param[{}]!", this.getClass().getSimpleName(), input);
        System.out.println(retMsg);
        return null;
    }

}