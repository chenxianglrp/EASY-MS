package com.stars.easyms.demo.rest.service.jdk8;

import com.stars.easyms.rest.RestService;
import org.springframework.stereotype.Service;

/**
 * <p>className: EasyMsJdk8Demo4Service</p>
 * <p>description: 函数式接口</p>
 *
 * @author guoguifang
 * @date 2019/9/23 16:53
 * @since 1.3.2
 */
@Service
public class EasyMsJdk8Demo4Service implements RestService<Void, Void> {

    @Override
    public Void execute(Void input) {
        FunctionalInterfaceDemo interfaceDemo1 = s -> System.out.println(s);
        FunctionalInterfaceDemo interfaceDemo2 = System.out::println;
        return null;
    }

    @FunctionalInterface
    private interface FunctionalInterfaceDemo {

        void test(String s);

        default void test1(String s) {
            System.out.println(s);
        }

        default void test2() {
            System.out.println("test2");
        }
    }

}