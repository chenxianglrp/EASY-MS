package com.stars.easyms.demo.rest.service.mq;

import com.stars.easyms.demo.repository.entity.UserTest;
import com.stars.easyms.demo.manager.mq.MQDemoManager;
import com.stars.easyms.rest.RestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>className: EasyMsMQDemo1Service</p>
 * <p>description: 发送端demo</p>
 *
 * @author guoguifang
 * @date 2020-01-02 15:57
 * @since 1.5.0
 */
@Service
public class EasyMsMQDemo1Service implements RestService<Void, Void> {

    @Autowired
    private MQDemoManager mqDemoManager;

    @Override
    public Void execute(Void input) {
        UserTest userInfo = new UserTest();
        userInfo.setName("小明");
        userInfo.setAge(10);
        mqDemoManager.send(userInfo);
        return null;
    }
}