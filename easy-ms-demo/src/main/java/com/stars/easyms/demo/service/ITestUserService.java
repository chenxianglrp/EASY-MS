package com.stars.easyms.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.stars.easyms.datasource.annotation.SpecifyDataSource;
import com.stars.easyms.demo.repository.dao.TestMapper;
import com.stars.easyms.demo.repository.entity.UserTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@SpecifyDataSource("other1")
@Service
public class ITestUserService extends ServiceImpl<TestMapper, UserTest> implements IService<UserTest> {

    @Autowired
    private TestMapper testMapper;

    @Transactional(rollbackFor = Exception.class)
    public int insert(UserTest userTest) {
        return testMapper.insert(userTest);
    }

    @Transactional(rollbackFor = Exception.class)
    public int insert1(UserTest userTest) {
        return testMapper.insert(userTest);
    }

}
