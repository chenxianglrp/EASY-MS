package com.stars.easyms.demo.rest.controller;

import com.stars.easyms.demo.rest.dto.input.sentinel.*;
import com.stars.easyms.demo.rest.dto.output.sentinel.*;
import com.stars.easyms.demo.rest.service.sentinel.*;
import com.stars.easyms.rest.annotation.EasyMsRestController;
import com.stars.easyms.rest.annotation.EasyMsRestMapping;
/**
 * sentinel的demo接口：若接口编号为空则默认取方法名
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
@EasyMsRestController(name = "用于展示sentinel的规则")
public interface SentinelDemoController {

    @EasyMsRestMapping(service = SentinelDemo1Service.class, name = "sentinel流控规则-QPS", notes = "本demo只测试流控规则-阈值类型为'QPS'的场景（单线程）")
    SentinelDemo1Output demo1(SentinelDemo1Input input);

    @EasyMsRestMapping(service = SentinelDemo2Service.class, name = "sentinel流控规则-线程", notes = "本demo只测试流控规则-阈值类型为'threadCount'的场景")
    SentinelDemo2Output demo2(SentinelDemo2Input input);

    @EasyMsRestMapping(service = SentinelDemo3Service.class, name = "sentinel降级规则-RT", notes = "本demo只测试降级规则-RT的场景（默认单次处理时间是200毫秒）")
    SentinelDemo3Output demo3(SentinelDemo3Input input);

    @EasyMsRestMapping(service = SentinelDemo4Service.class, name = "sentinel降级规则-异常比例", notes = "本demo只测试降级规则-异常比例的场景")
    SentinelDemo4Output demo4(SentinelDemo4Input input);

    @EasyMsRestMapping(service = SentinelDemo5Service.class, name = "sentinel降级规则-异常数", notes = "本demo只测试降级规则-异常数的场景")
    SentinelDemo5Output demo5(SentinelDemo5Input input);

    @EasyMsRestMapping(service = SentinelDemo6Service.class, name = "sentinel系统规则", notes = "本demo只测试系统规则的场景")
    SentinelDemo6Output demo6(SentinelDemo6Input input);

    @EasyMsRestMapping(service = SentinelDemo7Service.class, name = "sentinel授权规则", notes = "本demo只测试授权规则的场景")
    SentinelDemo7Output demo7(SentinelDemo7Input input);

    @EasyMsRestMapping(service = SentinelDemo8Service.class, name = "sentinel使用本地文件注入流控规则", notes = "使用classpath目录、非classpath目录下的文件")
    SentinelDemo8Output demo8(SentinelDemo8Input input);

    @EasyMsRestMapping(service = SentinelDemo9Service.class, name = "sentinel使用apollo注入流控规则")
    SentinelDemo9Output demo9(SentinelDemo9Input input);

    @EasyMsRestMapping(service = SentinelDemo10Service.class, name = "rest接口调用-异常", notes = "调用rest接口后抛出异常")
    Void demo10(Void input);

}


