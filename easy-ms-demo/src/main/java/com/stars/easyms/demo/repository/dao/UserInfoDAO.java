package com.stars.easyms.demo.repository.dao;

import com.stars.easyms.datasource.annotation.EasyMsRepository;
import com.stars.easyms.demo.repository.entity.UserTest;

import java.util.List;

@EasyMsRepository
public interface UserInfoDAO {

    List<UserTest> getAllUserInfo();

    UserTest getUserInfoByName(String name);

    int deleteAllUserInfo();

    int deleteUserInfoByName(String name);
}
