package com.stars.easyms.demo.rest.service.interfaces;

import com.stars.easyms.demo.rest.dto.input.InterfaceDemo1Input;
import com.stars.easyms.demo.rest.dto.output.InterfaceDemo1Output;
import com.stars.easyms.rest.RestService;
import com.stars.easyms.base.util.MessageFormatUtil;
import com.stars.easyms.rest.exception.BusinessRestException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class EasyMsInterfaceDemo1Service implements RestService<InterfaceDemo1Input, InterfaceDemo1Output> {

    @Override
    public InterfaceDemo1Output execute(InterfaceDemo1Input input) throws BusinessRestException {
        String retMsg = MessageFormatUtil.format("Rest Service[{}], param[{}]!", this.getClass().getSimpleName(), input);
        InterfaceDemo1Output output = new InterfaceDemo1Output();
        output.setName("xiaoming");
        System.out.println(retMsg);
        return output;
    }
}