package com.stars.easyms.demo.repository.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * <p>className: TestUser</p>
 * <p>description: 测试用户表实体类</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/10/26 6:10 下午
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserTest {

    @TableId
    private String name;

    private Integer age;

    private LocalDateTime loginTime;
}
