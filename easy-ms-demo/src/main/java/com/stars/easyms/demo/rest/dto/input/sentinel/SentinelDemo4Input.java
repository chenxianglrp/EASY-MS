package com.stars.easyms.demo.rest.dto.input.sentinel;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SentinelDemo4Input {

    private static final long serialVersionUID = 1332056467576302896L;

    @ApiModelProperty(value = "执行次数", required = true, position = 0, example = "20")
    private int times;

    @ApiModelProperty(value = "降级策略(0: RT, 1: 异常比例, 2: 异常数)", required = true, position = 1, example = "1")
    private int grade;

    @ApiModelProperty(value = "RT阈值、异常比阈值、异常计数", required = true, position = 2, example = "0.1")
    private double count;

    @ApiModelProperty(value = "降级发生时的恢复超时(以秒为单位)。", required = true, position = 3, example = "10")
    private int timeWindow;

}
