package com.stars.easyms.demo.rest.service.datasource;

import com.stars.easyms.datasource.EasyMsDataSourceSwitcher;
import com.stars.easyms.demo.repository.dao.TestDAO;
import com.stars.easyms.rest.RestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 使用相同的DAO的方法，可以插入不同的数据源及主库或者从库
 */
@Service
public class EasyMsDatasourceDemo5Service implements RestService<Void, Void> {

    @Autowired
    private TestDAO testDao;

    @Override
    public Void execute(Void input) {
        try {
            // 选择默认数据源，不强制使用主库
            EasyMsDataSourceSwitcher.switchDefaultDataSource(false);
            testDao.insertData_test_master();
            // 选择默认数据源并强制固定数据源
            EasyMsDataSourceSwitcher.switchAndFixedDefaultDataSource();
            testDao.insertData_test_master();
            // 选择数据源名称为"other1"的数据源
            EasyMsDataSourceSwitcher.switchDataSource("other1");
            testDao.insertData_test_master();
            // 选择数据源名称为"other2"的数据源
            EasyMsDataSourceSwitcher.switchDataSource("other2");
            testDao.insertData_test_master();
            // 选择数据源名称为"other2"的数据源，并强制固定数据源
            EasyMsDataSourceSwitcher.switchAndFixedDataSource("other2");
            testDao.insertData_test_master();
            // 选择数据源名称为"other1"的数据源
            EasyMsDataSourceSwitcher.switchDataSource("other1");
            testDao.insertData_test_master();
        } finally {
            // 清除数据源选择
            EasyMsDataSourceSwitcher.clearSwitch();
        }
        return null;
    }

}