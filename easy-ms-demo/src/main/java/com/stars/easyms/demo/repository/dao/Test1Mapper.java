package com.stars.easyms.demo.repository.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.stars.easyms.datasource.annotation.EasyMsRepository;
import com.stars.easyms.demo.repository.entity.UserTest;

/**
 * <p>className: TestMapper</p>
 * <p>description: 测试MybatisPlus的Mapper接口类</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/10/26 6:09 下午
 */
@EasyMsRepository(datasource = "other1")
public interface Test1Mapper extends BaseMapper<UserTest> {
}
