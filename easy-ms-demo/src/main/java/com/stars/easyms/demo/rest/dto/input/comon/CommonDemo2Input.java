package com.stars.easyms.demo.rest.dto.input.comon;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>className: CommonDemo2Input</p>
 * <p>description: 日志打印demo入参</p>
 *
 * @author guoguifang
 * @date 2019-08-21 17:36
 * @since 1.3.0
 */
@Data
public class CommonDemo2Input {

    @ApiModelProperty(value = "日志打印数量", required = true, position = 0, example = "1000")
    private int count;
}