package com.stars.easyms.demo.rest.dto.output;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class InterfaceDemo1Output {

    @ApiModelProperty(value = "姓名", required = true, position = 0, example = "")
    private String name;

    @ApiModelProperty(value = "性别", required = true, position = 1, example = "")
    private String sex;
}