package com.stars.easyms.demo.rest.service.plumelog;

import com.stars.easyms.rest.RestService;
import com.stars.easyms.rest.exception.BusinessRestException;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>className: EasyMsPlumeLogDemoService</p>
 * <p>description: EasyMsPlumeLogDemoService</p>
 *
 * @author guoguifang
 * @version 1.7.3
 * @date 2021/3/23 4:43 下午
 */
@Slf4j
public class EasyMsPlumeLogDemoService implements RestService<Void, Void> {

    @Override
    public Void execute(Void input) throws BusinessRestException {
        try {
            int a = 5 / 0;
        } catch (Exception e) {
            log.error("First word: {}, Second word: {}!", "first", "second", e);
        }
        return emptyReturn();
    }
}