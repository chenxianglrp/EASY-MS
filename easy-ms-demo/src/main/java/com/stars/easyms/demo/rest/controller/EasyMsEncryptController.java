package com.stars.easyms.demo.rest.controller;

import com.stars.easyms.demo.rest.dto.input.encrypt.EncryptDemoInput;
import com.stars.easyms.demo.rest.service.encrypt.*;
import com.stars.easyms.rest.annotation.EasyMsRestController;
import com.stars.easyms.rest.annotation.EasyMsRestMapping;
/**
 * <p>className: EasyMsEncryptController</p>
 * <p>description: Rest加密接口demo类：每个接口实现都可以定义自己的加密信息，</p>
 *
 * @author guoguifang
 * @version 1.2.2
 * @date 2019-08-06 16:22
 */
@EasyMsRestController(path = "encrypt", name = "用于展示加密类型接口编写方式")
public interface EasyMsEncryptController {

    @EasyMsRestMapping(service = EasyMsEncryptDemo1Service.class, name = "使用全局加密")
    Void demo1(Void input);

    @EasyMsRestMapping(service = EasyMsEncryptDemo2Service.class, name = "自定义接口加密", notes = "自定义秘钥、encryptRequestKey、encryptResponseKey",
            secret = "abcdefghi1000000", iv = "abcdefghi1000000", encryptRequestKey = "requestKey1", encryptResponseKey = "responseKey1")
    Void demo2(Void input);

    @EasyMsRestMapping(service = EasyMsEncryptDemo3Service.class, name = "自定义接口加密",  notes = "自定义秘钥，使用全局encryptRequestKey和encryptResponseKey",
            secret = "abcdefghi1000001", iv = "abcdefghi1000000")
    Void demo3(Void input);

    @EasyMsRestMapping(code = "demo4", method = "type1", methodFor = "demo1",
            service = EasyMsEncryptDemo4Service.class, name = "method接口加密", notes = "相同接口编号的接口加密信息必须保持一致",
            secret = "abcdefghi1000002", iv = "abcdefghi1000000", encryptRequestKey = "requestKey2", encryptResponseKey = "responseKey2")
    Void demo4(EncryptDemoInput input);

    @EasyMsRestMapping(code = "demo4", method = {"type2", "type3"}, methodFor = {"demo2", "demo3"},
            service = EasyMsEncryptDemo5Service.class, name = "method接口加密", notes = "相同接口编号的接口加密信息必须保持一致",
            secret = "abcdefghi1000002", iv = "abcdefghi1000000", encryptRequestKey = "requestKey2", encryptResponseKey = "responseKey2")
    Void demo5(EncryptDemoInput input);

    @EasyMsRestMapping(service = EasyMsEncryptDemo6Service.class, encrypt = false, name = "不加密接口", notes = "若全局加密未开启，即使不加[encrypt = false]时也不会加密")
    Void demo6(Void input);

}