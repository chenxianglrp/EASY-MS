package com.stars.easyms.demo.manager.mq;

import com.stars.easyms.demo.repository.entity.UserTest;
import com.stars.easyms.demo.service.UserInfoService;
import com.stars.easyms.mq.annotation.MQManager;
import com.stars.easyms.mq.annotation.MQReceiver;
import com.stars.easyms.mq.annotation.MQSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>className: MQDemoManager</p>
 * <p>description: MQ的manager的demo类</p>
 *
 * @author guoguifang
 * @date 2020-01-02 15:16
 * @since 1.5.0
 */
@Service
public class MQDemoManager {

    @Autowired
    private UserInfoService userInfoService;

    @MQSender(key = "test", name = "发送端demo")
    public void send(UserTest userInfo) {
        // Intentionally blank
    }

    /**
     * key可以使用${}表达式，使用easy-ms.mq.key.test=test_dev的方式配置
     * 接收的参数需要跟注解的batch对应，默认是开启批量，因此需要接收时的参数为List类型，若未开启批量则接收类型为UserInfo
     */
    @MQReceiver(key = "${test}", name = "接收端demo")
    public void receive(List<UserTest> userInfoList) {
        userInfoService.test(userInfoList);
    }

}