package com.stars.easyms.demo.rest.service.datasource;

import com.stars.easyms.base.util.MessageFormatUtil;
import com.stars.easyms.demo.repository.dao.TestDAO;
import com.stars.easyms.rest.RestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
public class EasyMsDatasourceDemo4Service implements RestService<Void, Void> {

    @Autowired
    private TestDAO testDao;

    @Override
//    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
//    @Transactional(propagation = Propagation.NEVER, rollbackFor = Exception.class)
    @Transactional(propagation = Propagation.NESTED, rollbackFor = Exception.class)
    public Void execute(Void input) {
        testDao.insertData_test_master();
        testDao.insertData_test_master();
        testDao.insertData_test_slave();
        testDao.insertData_other1_master();
        testDao.insertData_test_master();
        testDao.insertData_other1_slave();
        testDao.insertData_test_master();
        testDao.insertData_other2_master();
        testDao.insertData_test_master();
        testDao.insertData_other2_slave();
        String retMsg = MessageFormatUtil.format("Rest Service[{}], param[{}]!", this.getClass().getSimpleName(), input);
        System.out.println(retMsg);
        return null;
    }

}