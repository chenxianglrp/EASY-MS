package com.stars.easyms.demo.rest.controller;

import com.stars.easyms.demo.rest.service.mybatisplus.*;
import com.stars.easyms.rest.annotation.EasyMsRestController;
import com.stars.easyms.rest.annotation.EasyMsRestMapping;

/**
 * <p>className: EasyMsMybatisPlusController</p>
 * <p>description: 用于展示mybatisPlus相关demo</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/10/26 5:14 下午
 */
@EasyMsRestController(path = "mybatisPlus", name = "用于展示mybatisPlus相关")
public interface EasyMsMybatisPlusController {

    @EasyMsRestMapping(service = EasyMsMybatisPlusDemo1Service.class, name = "插入-使用dao")
    Void demo1(Void input);

    @EasyMsRestMapping(service = EasyMsMybatisPlusDemo2Service.class, name = "插入-使用dao")
    Void demo2(Void input);

    @EasyMsRestMapping(service = EasyMsMybatisPlusDemo3Service.class, name = "插入-使用dao")
    Void demo3(Void input);

    @EasyMsRestMapping(service = EasyMsMybatisPlusDemo4Service.class, name = "加了事务的添加")
    Void demo4(Void input);

    @EasyMsRestMapping(service = EasyMsMybatisPlusDemo5Service.class, name = "批量修改")
    Void demo5(Void input);

    @EasyMsRestMapping(service = EasyMsMybatisPlusDemo6Service.class, name = "批量修改-事务")
    Void demo6(Void input);
}