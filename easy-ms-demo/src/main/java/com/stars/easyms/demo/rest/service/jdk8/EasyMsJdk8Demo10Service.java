package com.stars.easyms.demo.rest.service.jdk8;

import com.stars.easyms.demo.rest.service.jdk8.demo10.Hint;
import com.stars.easyms.demo.rest.service.jdk8.demo10.Hints;
import com.stars.easyms.demo.rest.service.jdk8.demo10.Jdk8MultipleAnnotationWritten;
import com.stars.easyms.rest.RestService;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Service;

/**
 * <p>className: EasyMsJdk8Demo10Service</p>
 * <p>description: 多重注解</p>
 *
 * @author guoguifang
 * @date 2019/9/23 16:53
 * @since 1.3.2
 */
@Service
public class EasyMsJdk8Demo10Service implements RestService<Void, Void> {

    @Override
    public Void execute(Void input) {
        Hint hint = AnnotationUtils.getAnnotation(Jdk8MultipleAnnotationWritten.class, Hint.class);
        System.out.println(hint);

        Hints hints1 = AnnotationUtils.getAnnotation(Jdk8MultipleAnnotationWritten.class, Hints.class);
        System.out.println(hints1 != null ? hints1.value().length : null);

        Hint[] hints2 = Jdk8MultipleAnnotationWritten.class.getDeclaredAnnotationsByType(Hint.class);
        System.out.println(hints2.length);

        return null;
    }

}