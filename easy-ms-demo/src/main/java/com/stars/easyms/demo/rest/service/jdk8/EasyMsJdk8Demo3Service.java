package com.stars.easyms.demo.rest.service.jdk8;

import com.stars.easyms.rest.RestService;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.function.Supplier;

/**
 * <p>className: EasyMsJdk8Demo3Service</p>
 * <p>description: 方法引用</p>
 *
 * @author guoguifang
 * @date 2019/9/23 16:53
 * @since 1.3.2
 */
@Service
public class EasyMsJdk8Demo3Service implements RestService<Void, Void> {

    @Override
    public Void execute(Void input) {
        // 构造器引用:它的语法是Class::new,实例如下：
        Car car = Car.create(Car::new);
        List<Car> cars = Collections.singletonList(car);

        // 静态方法引用:它的语法是Class::static_method,实例如下：
        cars.forEach(Car::collide);

        // 特定类的任意对象的方法引用:它的语法是Class::method,实例如下：
        cars.forEach(Car::repair);

        // 特定对象的方法引用:它的语法是instance::method,实例如下：
        Car police = Car.create(Car::new);
        cars.forEach(police::follow);

        return null;
    }

    private static class Car {

        //Supplier是jdk1.8的函数式接口，这里和lambda一起使用了
        private static Car create(final Supplier<Car> supplier) {
            return supplier.get();
        }

        private static void collide(final Car car) {
            System.out.println("Collided " + car.toString());
        }

        private void follow(final Car another) {
            System.out.println("Following the " + another.toString());
        }

        private void repair() {
            System.out.println("Repaired " + this.toString());
        }
    }

}