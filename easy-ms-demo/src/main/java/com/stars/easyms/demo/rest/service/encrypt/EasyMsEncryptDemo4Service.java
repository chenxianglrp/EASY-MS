package com.stars.easyms.demo.rest.service.encrypt;

import com.stars.easyms.demo.rest.dto.input.encrypt.EncryptDemoInput;
import com.stars.easyms.rest.RestService;
import com.stars.easyms.base.util.MessageFormatUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class EasyMsEncryptDemo4Service implements RestService<EncryptDemoInput, Void> {

    @Override
    public Void execute(EncryptDemoInput input) {
        String retMsg = MessageFormatUtil.format("Rest Service[{}], param[{}]!", this.getClass().getSimpleName(), input);
        System.out.println(retMsg);
        return null;
    }
}