package com.stars.easyms.demo.rest.service.datasource;

import com.stars.easyms.demo.repository.dao.TestDAO;
import com.stars.easyms.rest.RestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class EasyMsDatasourceDemo2Service implements RestService<Void, Void> {

    @Autowired
    private TestDAO testDao;

    @Override
    public Void execute(Void input) {
        StringBuilder sb = new StringBuilder();
        sb.append("queryData_test_master:").append(testDao.queryData_test_master())
                .append(", queryData_test_slave:").append(testDao.queryData_test_slave())
                .append(", queryData_other1_master:").append(testDao.queryData_other1_master())
                .append(", queryData_other1_slave:").append(testDao.queryData_other1_slave())
                .append(", queryData_other2_master:").append(testDao.queryData_other2_master())
                .append(", queryData_other2_slave:").append(testDao.queryData_other2_slave());
        System.out.println(sb.toString());
        return null;
    }
}