package com.stars.easyms.demo.rest.controller;

import com.stars.easyms.demo.rest.dto.output.jdk8.Jdk8Demo7Output;
import com.stars.easyms.demo.rest.service.jdk8.*;
import com.stars.easyms.rest.annotation.EasyMsRestController;
import com.stars.easyms.rest.annotation.EasyMsRestMapping;
/**
 * <p>interfaceName: EasyMsJdk8DemoController</p>
 * <p>description: Jdk8的demo</p>
 *
 * @author guoguifang
 * @date 2019-08-21 15:53
 * @since 1.3.0
 */
@EasyMsRestController(path = "jdk8", name = "jdk8特性demo")
public interface EasyMsJdk8DemoController {

    @EasyMsRestMapping(service = EasyMsJdk8Demo1Service.class, name = "jdk8特性-default关键字")
    Void demo1(Void input);

    @EasyMsRestMapping(service = EasyMsJdk8Demo2Service.class, name = "jdk8特性-lambda表达式")
    Void demo2(Void input);

    @EasyMsRestMapping(service = EasyMsJdk8Demo3Service.class, name = "jdk8特性-方法引用")
    Void demo3(Void input);

    @EasyMsRestMapping(service = EasyMsJdk8Demo4Service.class, name = "jdk8特性-函数式接口")
    Void demo4(Void input);

    @EasyMsRestMapping(service = EasyMsJdk8Demo5Service.class, name = "jdk8特性-本地化日期")
    Void demo5(Void input);

    @EasyMsRestMapping(service = EasyMsJdk8Demo6Service.class, name = "jdk8特性-本地化日期格式化")
    Void demo6(Void input);

    @EasyMsRestMapping(service = EasyMsJdk8Demo7Service.class, name = "jdk8特性-SimpleDateFormat与DateTimeFormatter比较")
    Jdk8Demo7Output demo7(Void input);

    @EasyMsRestMapping(service = EasyMsJdk8Demo8Service.class, name = "jdk8特性-Optional容器")
    Void demo8(Void input);

    @EasyMsRestMapping(service = EasyMsJdk8Demo9Service.class, name = "jdk8特性-Stream API")
    Void demo9(Void input);

    @EasyMsRestMapping(service = EasyMsJdk8Demo10Service.class, name = "jdk8特性-多重注解")
    Void demo10(Void input);

    @EasyMsRestMapping(service = EasyMsJdk8Demo11Service.class, name = "jdk8特性-参数名字")
    Void demo11(Void input);

    @EasyMsRestMapping(service = EasyMsJdk8Demo12Service.class, name = "jdk8特性-Collection新方法")
    Void demo12(Void input);

    @EasyMsRestMapping(service = EasyMsJdk8Demo13Service.class, name = "jdk8特性-Map新方法")
    Void demo13(Void input);
}
