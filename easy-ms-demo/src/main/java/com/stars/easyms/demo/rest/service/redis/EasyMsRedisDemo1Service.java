package com.stars.easyms.demo.rest.service.redis;

import com.stars.easyms.demo.manager.redis.RedisDemoManager;
import com.stars.easyms.rest.RestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class EasyMsRedisDemo1Service implements RestService<Void, Void> {

    @Autowired
    private RedisDemoManager redisDemoManager;

    @Override
    public Void execute(Void input) {
        redisDemoManager.setAllUserInfo();
        redisDemoManager.getAllUserInfo();
        return null;
    }

}