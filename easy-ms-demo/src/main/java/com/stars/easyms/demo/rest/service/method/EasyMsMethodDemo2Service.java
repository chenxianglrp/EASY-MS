package com.stars.easyms.demo.rest.service.method;

import com.stars.easyms.demo.rest.dto.input.method.MethodDemo2Input;
import com.stars.easyms.rest.RestService;
import com.stars.easyms.base.util.MessageFormatUtil;
import com.stars.easyms.rest.exception.BusinessRestException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class EasyMsMethodDemo2Service implements RestService<MethodDemo2Input, Void> {

    @Override
    public Void execute(MethodDemo2Input input) throws BusinessRestException {
        String retMsg = MessageFormatUtil.format("Rest Service[{}], param[{}]!", this.getClass().getSimpleName(), input);
        System.out.println(retMsg);
        return null;
    }
}