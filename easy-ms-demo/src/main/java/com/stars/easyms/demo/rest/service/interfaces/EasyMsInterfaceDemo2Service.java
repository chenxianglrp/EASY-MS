package com.stars.easyms.demo.rest.service.interfaces;

import com.stars.easyms.demo.rest.dto.input.InterfaceDemo2Input;
import com.stars.easyms.demo.rest.dto.output.InterfaceDemo2Output;
import com.stars.easyms.rest.RestService;
import com.stars.easyms.base.util.MessageFormatUtil;
import com.stars.easyms.rest.exception.BusinessRestException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class EasyMsInterfaceDemo2Service implements RestService<InterfaceDemo2Input, InterfaceDemo2Output> {

    @Override
    public InterfaceDemo2Output execute(InterfaceDemo2Input input) throws BusinessRestException {
        String retMsg = MessageFormatUtil.format("Rest Service[{}], param[{}]!", this.getClass().getSimpleName(), input);
        InterfaceDemo2Output output = new InterfaceDemo2Output();
        output.setName("xiaohong");
        System.out.println(retMsg);
        int a = 5 / 0;
        return output;
    }
}