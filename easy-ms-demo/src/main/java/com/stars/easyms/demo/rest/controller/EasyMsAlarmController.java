package com.stars.easyms.demo.rest.controller;

import com.stars.easyms.demo.rest.dto.input.alarm.AlarmInput;
import com.stars.easyms.demo.rest.service.alarm.EasyMsAlarmDemo1Service;
import com.stars.easyms.rest.annotation.EasyMsRestController;
import com.stars.easyms.rest.annotation.EasyMsRestMapping;
/**
 * <p>className: EasyMsAlarmController</p>
 * <p>description: 告警</p>
 *
 * @author caotieshuan
 * @date 2019/12/23 10:56
 * @version 1.0.0
 */
@EasyMsRestController(path = "alarm", name = "告警模块使用方法")
public interface EasyMsAlarmController {

    @EasyMsRestMapping(service = EasyMsAlarmDemo1Service.class, name = "alarm")
    Void demo1(AlarmInput input);

}