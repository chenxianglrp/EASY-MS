package com.stars.easyms.demo.rest.service.jdk8;

import com.stars.easyms.rest.RestService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>className: EasyMsJdk8Demo12Service</p>
 * <p>description: Collection新方法</p>
 *
 * @author guoguifang
 * @date 2019/9/23 16:53
 * @since 1.3.2
 */
@Service
public class EasyMsJdk8Demo12Service implements RestService<Void, Void> {

    @Override
    public Void execute(Void input) {
        Map<String, String> map = new HashMap<>(32);

        // stream(), parallelStream(), forEach(), removeIf()

        return null;
    }

}