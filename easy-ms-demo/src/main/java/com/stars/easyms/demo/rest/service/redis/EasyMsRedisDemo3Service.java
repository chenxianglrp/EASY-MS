package com.stars.easyms.demo.rest.service.redis;

import com.stars.easyms.base.util.ThreadUtil;
import com.stars.easyms.demo.repository.entity.UserTest;
import com.stars.easyms.demo.manager.redis.RedisDemoManager;
import com.stars.easyms.rest.RestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class EasyMsRedisDemo3Service implements RestService<Void, Void> {

    @Autowired
    private RedisDemoManager redisDemoManager;

    @Override
    public Void execute(Void input) {
        String key = "userInfo";

        redisDemoManager.pushUserInfo(key, UserTest.builder().age(10).name("abc1").loginTime(LocalDateTime.now()).build());
        ThreadUtil.sleep(2, TimeUnit.SECONDS);

        redisDemoManager.pushUserInfo(key, UserTest.builder().age(11).name("abc2").loginTime(LocalDateTime.now()).build());
        ThreadUtil.sleep(2, TimeUnit.SECONDS);

        redisDemoManager.pushUserInfo(key, UserTest.builder().age(12).name("abc3").loginTime(LocalDateTime.now()).build());
        ThreadUtil.sleep(2, TimeUnit.SECONDS);

        UserTest userTest1 = redisDemoManager.popUserInfo(key);
        System.out.println(userTest1);
        ThreadUtil.sleep(2, TimeUnit.SECONDS);

        UserTest userTest2 = redisDemoManager.popUserInfo(key);
        System.out.println(userTest2);
        ThreadUtil.sleep(2, TimeUnit.SECONDS);

        UserTest userTest3 = redisDemoManager.popUserInfo(key);
        System.out.println(userTest3);

        return null;
    }

}