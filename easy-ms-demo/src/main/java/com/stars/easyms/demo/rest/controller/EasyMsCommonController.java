package com.stars.easyms.demo.rest.controller;

import com.stars.easyms.demo.rest.dto.input.comon.CommonDemo2Input;
import com.stars.easyms.demo.rest.service.common.*;
import com.stars.easyms.rest.annotation.EasyMsRestController;
import com.stars.easyms.rest.annotation.EasyMsRestMapping;
/**
 * <p>className: EasyMsCommonController</p>
 * <p>description: 用于展示普通模块中的工具类等</p>
 *
 * @author guoguifang
 * @date 2019/8/9 10:56
 * @version 1.2.2
 */
@EasyMsRestController(path = "common", name = "用于展示普通模块中的工具类等")
public interface EasyMsCommonController {

    @EasyMsRestMapping(service = EasyMsCommonDemo1Service.class, name = "线程池", allowEndIsForwardSlash = false)
    void demo1();

    @EasyMsRestMapping(service = EasyMsCommonDemo2Service.class, name = "日志")
    Void demo2(CommonDemo2Input input);

}