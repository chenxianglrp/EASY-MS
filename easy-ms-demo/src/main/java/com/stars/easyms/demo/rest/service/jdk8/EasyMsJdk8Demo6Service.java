package com.stars.easyms.demo.rest.service.jdk8;

import com.stars.easyms.rest.RestService;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * <p>className: EasyMsJdk8Demo6Service</p>
 * <p>description: 本地化日期格式化</p>
 *
 * @author guoguifang
 * @date 2019/9/23 16:53
 * @since 1.3.2
 */
@Service
public class EasyMsJdk8Demo6Service implements RestService<Void, Void> {

    @Override
    public Void execute(Void input) {

        ZoneId zoneId = ZoneId.systemDefault();

        // 本地化日期类型的使用LocalDateTime
        String localPattern = "yyyy-MM-dd HH:mm:ss.SSS";
        DateTimeFormatter localFormatter = DateTimeFormatter.ofPattern(localPattern);
        String s1 = localFormatter.format(LocalDateTime.now());
        String s2 = LocalDateTime.ofInstant(Instant.now(), zoneId).format(localFormatter);
        System.out.println(s1 + "," + s2);

        // 时区类型的必须使用ZonedDateTime
        String zonePattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
        DateTimeFormatter zonedFormatter = DateTimeFormatter.ofPattern(zonePattern);
        String s3 = zonedFormatter.format(ZonedDateTime.now());
        String s4 = ZonedDateTime.ofInstant(Instant.now(), zoneId).format(zonedFormatter);
        System.out.println(s3 + "," + s4);
        return null;
    }

}