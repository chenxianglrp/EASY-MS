package com.stars.easyms.demo.rest.service.datasource;

import com.github.pagehelper.Page;
import com.github.pagehelper.page.PageMethod;
import com.stars.easyms.demo.repository.dao.TestDAO;
import com.stars.easyms.demo.repository.entity.UserTest;
import com.stars.easyms.rest.RestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>className: EasyMsDatasourceDemo9Service</p>
 * <p>description: 分页</p>
 *
 * @author guoguifang
 * @date 2019/9/27 16:18
 * @since 1.3.2
 */
@Service
public class EasyMsDatasourceDemo9Service implements RestService<Void, Void> {

    @Autowired
    private TestDAO testDAO;

    @Override
    public Void execute(Void input) {

        // 使用pageHelper的语法
        PageMethod.startPage(1, 5);
        Page<UserTest> page = testDAO.queryUserInfo();
        System.out.println((page.toString()));
        return null;
    }

}