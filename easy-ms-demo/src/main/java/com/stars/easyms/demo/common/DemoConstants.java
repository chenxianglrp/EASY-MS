package com.stars.easyms.demo.common;

/**
 * <p>className: DemoConstants</p>
 * <p>description: demo的常量类</p>
 *
 * @author guoguifang
 * @date 2019-11-06 15:47
 * @since 1.3.3
 */
public final class DemoConstants {

    public static final String REDIS_ALL_USER_INFO = "allUserInfo";

    public static final String REDIS_USER_INFO = "userInfo";

    private DemoConstants() {}

}