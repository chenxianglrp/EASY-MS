package com.stars.easyms.demo.rest.service.datasource;

import com.stars.easyms.demo.repository.dao.TestDAO;
import com.stars.easyms.rest.RestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
@Service
public class EasyMsDatasourceDemo3Service implements RestService<Void, Void> {

    @Autowired
    private TestDAO testDao;

    private static final ExecutorService EXECUTOR_SERVICE = Executors.newFixedThreadPool(5);

    @Override
    public Void execute(Void input) {
        for (int i = 0; i < 100; i++) {
            EXECUTOR_SERVICE.execute(() -> {
                System.out.println(Thread.currentThread() + ":" + testDao.queryData_other1_master());
            });
        }
        return null;
    }
}