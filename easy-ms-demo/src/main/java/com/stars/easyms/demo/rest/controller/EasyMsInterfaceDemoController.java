package com.stars.easyms.demo.rest.controller;

import com.stars.easyms.demo.rest.dto.input.InterfaceDemo1Input;
import com.stars.easyms.demo.rest.dto.input.InterfaceDemo2Input;
import com.stars.easyms.demo.rest.dto.output.InterfaceDemo1Output;
import com.stars.easyms.demo.rest.dto.output.InterfaceDemo2Output;
import com.stars.easyms.demo.rest.service.interfaces.EasyMsInterfaceDemo1Service;
import com.stars.easyms.demo.rest.service.interfaces.EasyMsInterfaceDemo2Service;
import com.stars.easyms.rest.annotation.EasyMsRestController;
import com.stars.easyms.rest.annotation.EasyMsRestMapping;

/**
 * <p>className: EasyMsInterfaceDemoController</p>
 * <p>description: 用于展示Interface接口demo</p>
 *
 * @author guoguifang
 * @date 2019/8/9 10:56
 * @version 1.2.2
 */
@EasyMsRestController(path = "interface", name = "Interface接口demo")
public interface EasyMsInterfaceDemoController {

    @EasyMsRestMapping(code = "/api/demo1", service = EasyMsInterfaceDemo1Service.class, notes = "最简单的接口定义demo", name = "最简单的接口定义demo")
    InterfaceDemo1Output demo1(InterfaceDemo1Input input);

    @EasyMsRestMapping(code = {"api1/demo2", "/api2/demo2"}, version = "v2", service = EasyMsInterfaceDemo2Service.class,
            type = "Q", name = "interface-demo2-v2", notes = "用于展示多code、非默认版本v1、非默认接口类型的接口定义demo")
    InterfaceDemo2Output demo2(InterfaceDemo2Input input);

}