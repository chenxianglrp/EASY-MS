package com.stars.easyms.demo.rest.service.mybatisplus;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.stars.easyms.demo.repository.dao.TestMapper;
import com.stars.easyms.demo.repository.entity.UserTest;
import com.stars.easyms.rest.RestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Slf4j
@Service
public class EasyMsMybatisPlusDemo3Service implements RestService<Void, Void> {

    @Autowired
    private TestMapper testMapper;

    @Override
    public Void execute(Void input) {
        for (int i = 0; i < 10; i++) {
            testMapper.insert(UserTest.builder().name("xiaoming" + i).age(i).loginTime(LocalDateTime.now()).build());
        }

        IPage<UserTest> page = new Page<>(1, 2);
        QueryWrapper<UserTest> wrapper = new QueryWrapper<>();
        wrapper.gt("age", 1);
        IPage<UserTest> channelInwardIPage = testMapper.selectPage(page, wrapper);

        Page<UserTest> orderDtoPage = new Page<>();
        orderDtoPage.setCurrent(channelInwardIPage.getCurrent());
        orderDtoPage.setSize(channelInwardIPage.getSize());
        orderDtoPage.setTotal(channelInwardIPage.getTotal());
        orderDtoPage.setRecords(channelInwardIPage.getRecords());
        System.out.println(orderDtoPage);

        testMapper.delete(new QueryWrapper<>());
        return null;
    }

}