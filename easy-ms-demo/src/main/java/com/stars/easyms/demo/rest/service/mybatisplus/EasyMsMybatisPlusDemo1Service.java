package com.stars.easyms.demo.rest.service.mybatisplus;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.stars.easyms.base.util.MessageFormatUtil;
import com.stars.easyms.demo.repository.dao.TestMapper;
import com.stars.easyms.demo.repository.entity.UserTest;
import com.stars.easyms.rest.RestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Slf4j
@Service
public class EasyMsMybatisPlusDemo1Service implements RestService<Void, Void> {

    @Autowired
    private TestMapper testMapper;

    @Override
    public Void execute(Void input) {
        testMapper.insert(UserTest.builder().name("xiaoming").age(1).loginTime(LocalDateTime.now()).build());
        testMapper.insert(UserTest.builder().name("xiaoli").age(2).loginTime(LocalDateTime.now()).build());
        QueryWrapper<UserTest> userQueryWrapper1 = new QueryWrapper<>();
        userQueryWrapper1.eq("name", "xiaoming");
        UserTest userTest1 = testMapper.selectOne(userQueryWrapper1);
        System.out.println("第一次查询：" + userTest1);

        QueryWrapper<UserTest> userQueryWrapper2 = new QueryWrapper<>();
        userQueryWrapper2.eq("name", "xiaoming1111");
        UserTest userTest2 = testMapper.selectOne(userQueryWrapper2);
        System.out.println("第二次查询：" + userTest2);

        String retMsg = MessageFormatUtil.format("Rest Service[{}], param[{}]!", this.getClass().getSimpleName(), input);
        System.out.println(retMsg);
        return null;
    }

}