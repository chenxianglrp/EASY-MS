package com.stars.easyms.demo.rest.service.jdk8;

import com.stars.easyms.rest.RestService;
import org.springframework.stereotype.Service;

import java.time.*;
import java.util.Date;

/**
 * <p>className: EasyMsJdk8Demo5Service</p>
 * <p>description: 本地化日期</p>
 *
 * @author guoguifang
 * @date 2019/9/23 16:53
 * @since 1.3.2
 */
@Service
public class EasyMsJdk8Demo5Service implements RestService<Void, Void> {

    @Override
    public Void execute(Void input) {
        // 获取当前的日期时间
        LocalDateTime currentTime = LocalDateTime.now();
        System.out.println("当前时间: " + currentTime);

        // 当前时间转换当前日期
        LocalDate date1 = currentTime.toLocalDate();
        System.out.println("当前日期: " + date1);

        // 根据当前时间获取年月日时分秒毫秒
        Month month = currentTime.getMonth();
        int day = currentTime.getDayOfMonth();
        int seconds = currentTime.getSecond();
        System.out.println("当前月: " + month + ", 当前日: " + day + ", 当前秒: " + seconds);

        // 使用withYear等方法修改LocalDateTime的值
        LocalDateTime date2 = LocalDateTime.now().withDayOfMonth(20).withYear(2020);
        System.out.println("修改时间: " + date2);

        // 设置日期
        LocalDate date3 = LocalDate.of(2029, Month.APRIL, 21);
        System.out.println("设置日期: " + date3);

        // 设置时间
        LocalTime date4 = LocalTime.of(21, 15);
        System.out.println("设置时间: " + date4);

        // 解析时间字符串，2015-05-18 05:56:27.278是错误的必须有T
        LocalDateTime date5 = LocalDateTime.parse("2015-05-18T05:56:27.278");
        System.out.println("转换后的时间1: " + date5);

        // 解析日期字符串
        LocalDate date6 = LocalDate.parse("2013-02-12");
        System.out.println("转换后的时间2: " + date6);

        // 解析时间字符串
        LocalTime date7 = LocalTime.parse("20:15:30");
        System.out.println("转换后的时间3: " + date7);

        // Clock转换为Instant
        Clock clock = Clock.systemDefaultZone();
        Instant instant = Instant.now(clock);

        // Instant转换成LocalDateTime
        LocalDateTime instantToLocalDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());

        // Date与Instant互相转换
        Date instantToDate = Date.from(instant);
        Instant dateToInstant = new Date().toInstant();

        // 获取表示多个标准小时数的持续时间。
        Duration duration = Duration.ofMillis(2000);
        System.out.println(duration.getSeconds());

        // 获取表示指定单位中总数的Period，参数可以为负数
        Period period = Period.of(1,5,2);
        System.out.println("Years: " + period.getYears()
                + ", Months: " + period.getMonths()
                +", Days: " + period.getDays());

        return null;
    }

}