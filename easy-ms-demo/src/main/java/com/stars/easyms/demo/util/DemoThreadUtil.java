package com.stars.easyms.demo.util;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

/**
 * <p>className: DemoThreadUtil</p>
 * <p>description: 处理线程的工具类</p>
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
public final class DemoThreadUtil {

    public static <T> Future<T> submit(ExecutorService executorService, Callable<T> callable) {
        return executorService.submit(callable);
    }

    public static <T> void getFutureResult(List<Future<T>> futureList) {
        futureList.forEach(future -> {
            try {
                future.get();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}