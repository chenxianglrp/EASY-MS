package com.stars.easyms.demo.rest.controller;

import com.stars.easyms.demo.rest.service.redis.EasyMsRedisDemo1Service;
import com.stars.easyms.demo.rest.service.redis.EasyMsRedisDemo2Service;
import com.stars.easyms.demo.rest.service.redis.EasyMsRedisDemo3Service;
import com.stars.easyms.rest.annotation.EasyMsRestController;
import com.stars.easyms.rest.annotation.EasyMsRestMapping;
/**
 * <p>className: EasyMsRedisController</p>
 * <p>description: 用于展示redis模块使用方法</p>
 *
 * @author guoguifang
 * @date 2019/11/5 10:56
 * @version 1.3.3
 */
@EasyMsRestController(path = "redis", name = "用于展示redis模块使用方法")
public interface EasyMsRedisController {

    @EasyMsRestMapping(service = EasyMsRedisDemo1Service.class, name = "redis")
    Void demo1(Void input);

    @EasyMsRestMapping(service = EasyMsRedisDemo2Service.class, name = "redis分布式锁")
    Void demo2(Void input);

    @EasyMsRestMapping(service = EasyMsRedisDemo3Service.class, name = "redis的list使用方法")
    Void demo3(Void input);

}