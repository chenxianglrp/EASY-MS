package com.stars.easyms.demo.rest.service.jdk8;

import com.stars.easyms.rest.RestService;
import com.stars.easyms.base.util.ExecutorUtil;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * <p>className: EasyMsJdk8Demo2Service</p>
 * <p>description: lambda表达式</p>
 *
 * @author guoguifang
 * @date 2019/9/23 16:53
 * @since 1.3.2
 */
@Service
public class EasyMsJdk8Demo2Service implements RestService<Void, Void> {

    private final Map<String, String> map = new HashMap<>(4);

    {
        map.put("aaa", "bbb");
        map.put("ccc", "ddd");
    }

    private final List<String> list = Arrays.asList("aaa", "bbb", "ccc", "ddd");

    @Override
    public Void execute(Void input) {

        // 无参数无返回值
        ExecutorUtil.execute(new Runnable() {
            @Override
            public void run() {
                System.out.println("This is a normal object!");
            }
        });

        // 无参数无返回值lambda写法
        ExecutorUtil.execute(() -> System.out.println("This is a lambda expression!"));

        // 单参数无返回值
        list.forEach(new Consumer<String>(){
            @Override
            public void accept(String s) {
                System.out.println(s);
            }
        });

        // 单参数无返回值lambda表达式
        list.forEach(s -> System.out.println(s));

        // 单参数无返回值::表达式
        list.forEach(System.out::println);

        // 多参数（无返回值或者有返回值但代码单行实现）
        map.forEach(new BiConsumer<String, String>() {
            @Override
            public void accept(String key, String value) {
                System.out.println("key:" + key + ",value:" + value);
            }
        });

        // 多参数（无返回值或者有返回值但代码单行实现）lambda表达式
        map.forEach((key, value) -> System.out.println("key:" + key + ",value:" + value));

        // 多参数（无返回值或者有返回值但代码多行实现）
        list.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                System.out.println(o1);
                System.out.println(o2);
                return o2.compareTo(o1);
            }
        });

        // 多参数（无返回值或者有返回值但代码多行实现）lambda表达式
        list.sort((o1, o2) -> {
            System.out.println(o1);
            System.out.println(o2);
            return o2.compareTo(o1);
        });



        return null;
    }

}