package com.stars.easyms.demo.rest.dto.input;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class InterfaceDemo1Input {

    private static final long serialVersionUID = 1332056467576302896L;

    @ApiModelProperty(value = "interface-id2", required = true, position = 0, example = "5")
    @NotBlank(message = "interface-id2不能为空")
    private String id;

}
