package com.stars.easyms.demo.rest.dto.input.sentinel;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SentinelDemo8Input {

    private static final long serialVersionUID = 1332056467576302896L;

    @ApiModelProperty(value = "执行次数", required = true, position = 0, example = "20")
    private int times;

    @ApiModelProperty(value = "资源名称", required = true, position = 1, example = "sentinelDemo-flow")
    private String resource;
}
