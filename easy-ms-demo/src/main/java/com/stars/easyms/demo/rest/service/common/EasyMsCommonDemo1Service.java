package com.stars.easyms.demo.rest.service.common;

import com.stars.easyms.base.util.ThreadUtil;
import com.stars.easyms.rest.RestService;
import com.stars.easyms.base.util.ExecutorUtil;
import com.stars.easyms.base.util.MessageFormatUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Slf4j
@Service
public class EasyMsCommonDemo1Service implements RestService<Void, Void> {

    @Override
    public Void execute(Void input) {
        List<Future> futureList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            futureList.add(ExecutorUtil.submit(() -> ThreadUtil.sleep(5000)));
        }
        System.out.println("activeCount: " + ExecutorUtil.getActiveCount());
        futureList.forEach(future -> {
            try {
                future.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        });

        String retMsg = MessageFormatUtil.format("Rest Service[{}], param[{}]!", this.getClass().getSimpleName(), input);
        System.out.println(retMsg);
        return null;
    }

}