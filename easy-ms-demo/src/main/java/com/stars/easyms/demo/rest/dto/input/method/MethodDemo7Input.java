package com.stars.easyms.demo.rest.dto.input.method;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class MethodDemo7Input {

    private static final long serialVersionUID = 1332056467576302896L;

    @ApiModelProperty(value = "用来区分不同接口的字段", required = true, position = 0, example = "")
    private String type1;

    @ApiModelProperty(value = "用来区分不同接口的字段", required = true, position = 1, example = "demo2")
    private String type2;

    @ApiModelProperty(value = "用来区分不同接口的字段", required = true, position = 2, example = "demo3")
    private String type3;

    @ApiModelProperty(value = "用来区分不同接口的字段", required = true, position = 3, example = "demo4")
    private String type4;

    @ApiModelProperty(value = "id2", required = true, position = 2, example = "4")
    @NotBlank(message = "id2不能为空")
    private String id;

}
