package com.stars.easyms.demo.rest.service.common;

import com.stars.easyms.demo.rest.dto.input.comon.CommonDemo2Input;
//import com.stars.easyms.logger.config.EasyMsLoggerHelper;
import com.stars.easyms.rest.RestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * <p>className: EasyMsCommonDemo2Service</p>
 * <p>description: 用于异步日志的测试demo，通过配置logging.asyn来控制是否开启异步日志</p>
 *
 * @author guoguifang
 * @date 2019/11/5 9:50
 * @since 1.3.3
 */
@Slf4j
@Service
public class EasyMsCommonDemo2Service implements RestService<CommonDemo2Input, Void> {

    @Override
    public Void execute(CommonDemo2Input input) {
        long time = System.currentTimeMillis();
        for (int i = 0, count = input.getCount(); i < count; i++) {
            log.info("测试：" + i);
        }
//        long usedTime = System.currentTimeMillis() - time;
//        System.out.println("是否启动easy-ms日志异步:" + EasyMsLoggerHelperoggerHelper.isLogAsynEnabled() + ", 打印" + input.getCount() + "条日志用时：" + usedTime + "毫秒");
        return null;
    }
}