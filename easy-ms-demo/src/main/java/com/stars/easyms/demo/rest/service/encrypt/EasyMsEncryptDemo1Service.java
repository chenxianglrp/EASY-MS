package com.stars.easyms.demo.rest.service.encrypt;

import com.stars.easyms.rest.RestService;
import com.stars.easyms.base.util.MessageFormatUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class EasyMsEncryptDemo1Service implements RestService<Void, Void> {

    @Override
    public Void execute(Void input) {
        String retMsg = MessageFormatUtil.format("Rest Service[{}], param[{}]!", this.getClass().getSimpleName(), input);
        System.out.println(retMsg);
        return null;
    }
}