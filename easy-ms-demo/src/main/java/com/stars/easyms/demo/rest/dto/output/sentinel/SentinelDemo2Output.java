package com.stars.easyms.demo.rest.dto.output.sentinel;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SentinelDemo2Output {

    @ApiModelProperty(value = "成功数量", required = true, position = 0, example = "")
    private Integer successCount;

    @ApiModelProperty(value = "阻塞数量", required = true, position = 1, example = "")
    private Integer blockCount;
}