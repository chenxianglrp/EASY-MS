package com.stars.easyms.demo.rest.dto.output.sentinel;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SentinelDemo4Output {

    @ApiModelProperty(value = "成功数量", required = true, position = 0, example = "0")
    private Integer successCount;

    @ApiModelProperty(value = "阻塞数量", required = true, position = 1, example = "0")
    private Integer blockCount;

    @ApiModelProperty(value = "异常数量", required = true, position = 2, example = "0")
    private Integer exceptionCount;
}