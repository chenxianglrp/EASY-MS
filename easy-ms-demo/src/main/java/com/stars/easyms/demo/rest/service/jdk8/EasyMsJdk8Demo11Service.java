package com.stars.easyms.demo.rest.service.jdk8;

import com.stars.easyms.rest.RestService;
import org.springframework.stereotype.Service;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

/**
 * <p>className: EasyMsJdk8Demo11Service</p>
 * <p>description: 参数名字</p>
 *
 * @author guoguifang
 * @date 2019/9/23 16:53
 * @since 1.3.2
 */
@Service
public class EasyMsJdk8Demo11Service implements RestService<Void, Void> {

    @Override
    public Void execute(Void input) {

        Method method = null;
        try {
            method = EasyMsJdk8Demo11Service.class.getMethod("execute", Void.class);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        for (final Parameter parameter : method.getParameters()) {
            System.out.println("Parameter: " + parameter.getName());
        }
        return null;
    }

}



