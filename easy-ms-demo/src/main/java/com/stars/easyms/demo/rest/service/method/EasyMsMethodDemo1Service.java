package com.stars.easyms.demo.rest.service.method;

import com.stars.easyms.demo.rest.dto.input.method.MethodDemo1Input;
import com.stars.easyms.demo.rest.dto.output.MethodDemo1Output;
import com.stars.easyms.rest.RestService;
import com.stars.easyms.base.util.MessageFormatUtil;
import com.stars.easyms.rest.exception.BusinessRestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class EasyMsMethodDemo1Service implements RestService<MethodDemo1Input, MethodDemo1Output> {

    @Override
    public MethodDemo1Output execute(MethodDemo1Input input) throws BusinessRestException {
        Logger logger1 = LoggerFactory.getLogger(EasyMsMethodDemo1Service.class);
        logger1.info("123");
        Logger logger2 = LoggerFactory.getLogger(EasyMsMethodDemo2Service.class);
        logger2.info("456");
        String retMsg = MessageFormatUtil.format("Rest Service[{}], param[{}]!", this.getClass().getSimpleName(), input);
        MethodDemo1Output output = new MethodDemo1Output();
        System.out.println(retMsg);
        return null;
    }
}