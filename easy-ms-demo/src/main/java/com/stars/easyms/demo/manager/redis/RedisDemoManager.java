package com.stars.easyms.demo.manager.redis;

import com.stars.easyms.demo.common.DemoConstants;
import com.stars.easyms.demo.repository.dao.UserInfoDAO;
import com.stars.easyms.demo.repository.entity.UserTest;
import com.stars.easyms.redis.annotion.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * <p>className: RedisDemoManager</p>
 * <p>description: manager层：redis服务类</p>
 *
 * @author guoguifang
 * @date 2019-11-06 15:25
 * @since 1.3.3
 */
@RedisManager
public class RedisDemoManager {

    @Autowired
    private UserInfoDAO userInfoDAO;

    /**
     * RedisStringSet注解用于redis的String类型，返回值为缓存值（不读取redis是否有缓存，直接执行方法然后把返回值放入redis）
     */
    @RedisStringSet(group = DemoConstants.REDIS_ALL_USER_INFO, expire = "2h")
    public List<UserTest> setAllUserInfo() {
        return userInfoDAO.getAllUserInfo();
    }

    /**
     * RedisStringGet注解用于redis的String类型，返回值为缓存值（读取redis，若存在则返回，若不存在则执行方法并把返回值放入redis）
     */
    @RedisStringGet(group = DemoConstants.REDIS_ALL_USER_INFO, expire = "2h")
    public List<UserTest> getAllUserInfo() {
        return userInfoDAO.getAllUserInfo();
    }

    /**
     * RedisStringSet注解用于redis的String类型，返回值为缓存值（不读取redis是否有缓存，直接执行方法然后把返回值放入redis）
     */
    @RedisStringSet(group = DemoConstants.REDIS_ALL_USER_INFO, expire = "2h")
    public List<UserTest> setUserInfoByName(@RedisKey String name) {
        return userInfoDAO.getAllUserInfo();
    }

    /**
     * RedisStringGet注解用于redis的String类型，返回值为缓存值（读取redis，若存在则返回，若不存在则执行方法并把返回值放入redis）
     */
    @RedisStringGet(group = DemoConstants.REDIS_ALL_USER_INFO, expire = "2h")
    public List<UserTest> getUserInfoByName(@RedisKey String name) {
        return userInfoDAO.getAllUserInfo();
    }

    /**
     * RedisStringSet注解用于redis的hash类型，RedisHashKey为具体的hash的key值，返回值为缓存值（不读取redis是否有缓存，直接执行方法然后把返回值放入redis）
     */
    @RedisHashSet(group = DemoConstants.REDIS_USER_INFO, expire = "5m3s")
    public UserTest setUserInfo(@RedisHashKey String name) {
        return userInfoDAO.getUserInfoByName(name);
    }

    /**
     * RedisStringSet注解用于redis的hash类型，RedisHashKey为具体的hash的key值，返回值为缓存值（读取redis，若存在则返回，若不存在则执行方法并把返回值放入redis）
     */
    @RedisHashGet(group = DemoConstants.REDIS_USER_INFO, expire = "10m5s3ms")
    public UserTest getUserInfo(@RedisHashKey String name) {
        return userInfoDAO.getUserInfoByName(name);
    }

    /**
     * RedisDelete用于删除redis缓存，方法体可有可无，方法体无删除数据库操作的情况下只删除redis不删除数据库
     */
    @RedisDelete(group = DemoConstants.REDIS_ALL_USER_INFO)
    public void deleteAllUserInfo() {
        userInfoDAO.deleteAllUserInfo();
    }

    /**
     * RedisDelete用于删除redis缓存，方法体可有可无，方法体无删除数据库操作的情况下只删除redis不删除数据库
     */
    @RedisDelete(group = DemoConstants.REDIS_USER_INFO)
    public void deleteUserInfoByName(@RedisHashKey String name) {
        userInfoDAO.deleteUserInfoByName(name);
    }

    /**
     * RedisExpire用于设置redis的过期时间，优先方法返回的值，若返回值小于等于0则使用注解的expire属性值，若expire属性值大于0时则设置超时时间
     */
    @RedisExpire(group = DemoConstants.REDIS_ALL_USER_INFO, expire = "2h")
    public Long setUserInfoExpire(@RedisKey String name) {
        return 0L;
    }

    /**
     * RedisExists用于判断redis中是否有该缓存
     */
    @RedisExists(group = DemoConstants.REDIS_ALL_USER_INFO)
    public boolean isExistUserInfo(@RedisKey String name) {
        return false;
    }

    /**
     * RedisEval用于执行redis的lua脚本
     */
    @RedisEval(script = "local messageKey=KEYS[1];local filedKey=ARGV[1]; local batchNo=ARGV[2];redis.pcall('hset', messageKey, filedKey, batchNo);")
    public String getPushMessageBatchNo(@RedisEvalKey List<String> keys, @RedisEvalArg List<String> args) {
        return null;
    }

    @RedisListPush(group = DemoConstants.REDIS_USER_INFO)
    public UserTest pushUserInfo(@RedisKey String name, UserTest userTest) {
        return userTest;
    }

    @RedisListPop(group = DemoConstants.REDIS_USER_INFO)
    public UserTest popUserInfo(@RedisKey String name) {
        return null;
    }
}