package com.stars.easyms.demo.service;

import com.stars.easyms.demo.repository.entity.UserTest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserInfoService {

    /**
     * 处理逻辑
     */
    public void test(List<UserTest> userInfoList) {
        for (UserTest userInfo : userInfoList) {
            System.out.println(userInfo.getName() + ":" + userInfo.getAge());
        }
    }

}