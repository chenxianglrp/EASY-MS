package com.stars.easyms.demo.rest.dto.input.encrypt;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>className: EncryptDemoInput</p>
 * <p>description: 加密demo的输入参数</p>
 *
 * @author guoguifang
 * @date 2019-08-06 16:39
 * @version 1.2.2
 */
@Data
public class EncryptDemoInput {

    @ApiModelProperty(value = "用来区分不同接口的字段", required = true, position = 0, example = "demo1")
    private String type1;

    @ApiModelProperty(value = "用来区分不同接口的字段", required = true, position = 1, example = "demo2")
    private String type2;

    @ApiModelProperty(value = "用来区分不同接口的字段", required = true, position = 2, example = "demo3")
    private String type3;
}