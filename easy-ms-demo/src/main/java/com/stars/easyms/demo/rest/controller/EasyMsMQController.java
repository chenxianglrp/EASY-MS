package com.stars.easyms.demo.rest.controller;

import com.stars.easyms.demo.rest.service.mq.EasyMsMQDemo1Service;
import com.stars.easyms.rest.annotation.EasyMsRestController;
import com.stars.easyms.rest.annotation.EasyMsRestMapping;
/**
 * <p>className: EasyMsMQController</p>
 * <p>description: EasyMsMQ的Demo</p>
 *
 * @author guoguifang
 * @date 2020-01-02 15:56
 * @since 1.5.0
 */
@EasyMsRestController(path = "mq", name = "用于展示mq模块使用方法")
public interface EasyMsMQController {

    @EasyMsRestMapping(service = EasyMsMQDemo1Service.class, name = "发送端demo")
    Void demo1(Void input);

}