package com.stars.easyms.demo.rest.dto.output;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MethodDemo1Output {

    @ApiModelProperty(value = "姓名", required = true, position = 0, example = "MethodDemo1Output-name")
    private String name;

    @ApiModelProperty(value = "性别", required = true, position = 1, example = "MethodDemo1Output-sex")
    private String sex;
}