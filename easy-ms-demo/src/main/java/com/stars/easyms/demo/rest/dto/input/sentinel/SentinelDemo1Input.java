package com.stars.easyms.demo.rest.dto.input.sentinel;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SentinelDemo1Input {

    private static final long serialVersionUID = 1332056467576302896L;

    @ApiModelProperty(value = "执行次数", required = true, position = 0, example = "20")
    private int times;

    @ApiModelProperty(value = "阈值类型(0: threadCount, 1: QPS)", required = true, position = 1, example = "1")
    private int grade;

    @ApiModelProperty(value = "单机阈值", required = true, position = 2, example = "10")
    private int count;

    @ApiModelProperty(value = "是否集群", required = true, position = 3, example = "false")
    private boolean clusterMode;

    @ApiModelProperty(value = "流控模式(0: 直接(默认), 1: 关联, 2: 链路)", required = true, position = 4, example = "0")
    private int strategy;

    @ApiModelProperty(value = "关联资源(流控模式为1时有效)", required = true, position = 5, example = "sentinelRefResourceDemo1")
    private String refResource;

    @ApiModelProperty(value = "流控效果(0: 快速失败(默认), 1: 预热, 2: 排队等待, 3: 预热 + 排队等待)", required = true, position = 6, example = "0")
    private int controlBehavior;

    @ApiModelProperty(value = "预热时长,默认10,单位:秒(流控效果为2或3时有效)", required = true, position = 7, example = "20")
    private int warmUpPeriodSec;

    @ApiModelProperty(value = "最大排队等待时间,默认500,单位:毫秒(流控效果为2或3时有效)", required = true, position = 8, example = "1000")
    private int maxQueueingTimeMs;
}
