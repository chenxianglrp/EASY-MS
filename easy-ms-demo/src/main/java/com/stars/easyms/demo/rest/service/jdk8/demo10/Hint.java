package com.stars.easyms.demo.rest.service.jdk8.demo10;

import java.lang.annotation.Repeatable;

@Repeatable(Hints.class)
public @interface Hint {
    String value();
}