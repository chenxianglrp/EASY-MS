package com.stars.easyms.demo.rest.service.jdk8;

import com.stars.easyms.rest.RestService;
import org.springframework.stereotype.Service;

/**
 * <p>className: EasyMsJdk8Demo1Service</p>
 * <p>description: default关键字</p>
 *
 * @author guoguifang
 * @date 2019/9/23 16:53
 * @since 1.3.2
 */
@Service
public class EasyMsJdk8Demo1Service implements RestService<Void, Void> {

    @Override
    public Void execute(Void input) {
        DefaultKeywordDemoInterface.staticOut();
        DefaultKeywordDemoInterfaceImpl dkdii = new DefaultKeywordDemoInterfaceImpl();
        dkdii.out();
        dkdii.defaultOut();
        return null;
    }

    private interface DefaultKeywordDemoInterface {
        void out();

        default void defaultOut() {
            System.out.println("This is default out!");
        }

        static void staticOut() {
            System.out.println("This is static out!");
        }
    }

    private class DefaultKeywordDemoInterfaceImpl implements DefaultKeywordDemoInterface {
        @Override
        public void out() {
            System.out.println("This is normal out!");
        }
    }

}