package com.stars.easyms.demo.rest.dto.input.sentinel;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SentinelDemo2Input {

    private static final long serialVersionUID = 1332056467576302896L;

    @ApiModelProperty(value = "执行次数", required = true, position = 0, example = "20")
    private int times;

    @ApiModelProperty(value = "阈值类型(0: threadCount, 1: QPS)", required = true, position = 1, example = "0")
    private int grade;

    @ApiModelProperty(value = "单机阈值", required = true, position = 2, example = "10")
    private int count;

    @ApiModelProperty(value = "是否集群", required = true, position = 3, example = "false")
    private boolean clusterMode;

    @ApiModelProperty(value = "流控模式(0: 直接(默认), 1: 关联, 2: 链路)", required = true, position = 4, example = "0")
    private int strategy;

    @ApiModelProperty(value = "关联资源(流控模式为1时有效)", required = true, position = 5, example = "sentinelRefResourceDemo2")
    private String refResource;
}
