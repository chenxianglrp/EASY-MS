package com.stars.easyms.demo.rest.service.sentinel;

import com.stars.easyms.rest.RestService;
import com.stars.easyms.base.util.MessageFormatUtil;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicInteger;

@Service
public class SentinelDemo10Service implements RestService<Void, Void> {

    private static final AtomicInteger INDEX = new AtomicInteger();

    @Override
    public Void execute(Void input) {
        if (INDEX.incrementAndGet() % 2 == 0) {
            throw new RuntimeException(SentinelDemo10Service.class.getSimpleName());
        }
        System.out.println(MessageFormatUtil.format("Rest Service[{}], param[{}]!", this.getClass().getSimpleName(), input));
        return null;
    }
}