package com.stars.easyms.demo.rest.dto.output.jdk8;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>className: Jdk8Demo7Output</p>
 * <p>description: Jdk8的输出参数</p>
 *
 * @author guoguifang
 * @date 2019-08-21 16:19
 * @since 1.3.0
 */
@Data
public class Jdk8Demo7Output {

    @ApiModelProperty(value = "单线程下format执行情况", required = true, position = 0)
    private String singleThreadFormat;

    @ApiModelProperty(value = "单线程下parse执行情况", required = true, position = 1)
    private String singleThreadParse;

    @ApiModelProperty(value = "多线程（不加锁）下format执行情况", required = true, position = 2)
    private String multiThreadFormatWithoutLock;

    @ApiModelProperty(value = "多线程（不加锁）下parse执行情况", required = true, position = 3)
    private String multiThreadParseWithoutLock;

    @ApiModelProperty(value = "多线程（加锁）下format执行情况", required = true, position = 4)
    private String multiThreadFormatWithLock;

    @ApiModelProperty(value = "多线程（加锁）下parse执行情况", required = true, position = 5)
    private String multiThreadParseWithLock;
}