package com.stars.easyms.demo.rest.controller;

import com.stars.easyms.demo.rest.service.datasource.*;
import com.stars.easyms.rest.annotation.EasyMsRestController;
import com.stars.easyms.rest.annotation.EasyMsRestMapping;
/**
 * <p>className: EasyMsDatasourceController</p>
 * <p>description: 用于多数据源、主从数据源、读写分离测试demo</p>
 *
 * @author guoguifang
 * @date 2019/8/9 10:56
 * @version 1.2.2
 */
@EasyMsRestController(path = "datasource", name = "用于展示多数据源、主从数据源、读写分离")
public interface EasyMsDatasourceController {

    @EasyMsRestMapping(service = EasyMsDatasourceDemo1Service.class, name = "插入-使用dao")
    Void demo1(Void input);

    @EasyMsRestMapping(service = EasyMsDatasourceDemo2Service.class, name = "查询-使用dao")
    Void demo2(Void input);

    @EasyMsRestMapping(service = EasyMsDatasourceDemo3Service.class, name = "多线程查询-使用dao")
    Void demo3(Void input);

    @EasyMsRestMapping(service = EasyMsDatasourceDemo4Service.class, name = "注解切换多数据源（声明式）")
    Void demo4(Void input);

    @EasyMsRestMapping(service = EasyMsDatasourceDemo5Service.class, name = "手动切换多数据源（编程式）")
    Void demo5(Void input);

    @EasyMsRestMapping(service = EasyMsDatasourceDemo6Service.class, name = "多层事务嵌套（声明式）")
    Void demo6(Void input);

    @EasyMsRestMapping(service = EasyMsDatasourceDemo7Service.class, name = "多层事务嵌套（编程式）")
    Void demo7(Void input);

    @EasyMsRestMapping(service = EasyMsDatasourceDemo8Service.class, name = "批量提交")
    Void demo8(Void input);

    @EasyMsRestMapping(service = EasyMsDatasourceDemo9Service.class, name = "分页")
    Void demo9(Void input);
}