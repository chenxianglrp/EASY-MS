package com.stars.easyms.demo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Springboot启动类
 *
 * @author guoguifang
 * @version 1.2.1
 */
@Slf4j
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        log.info("=============== Easy Ms Demo start begin =========================");
        SpringApplication.run(Application.class, args);
        log.info("=============== Easy Ms Demo start success =========================");
    }

}
