package com.stars.easyms.demo.rest.dto.input;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class InterfaceDemo2Input {

    private static final long serialVersionUID = 1332056467576302896L;

    @ApiModelProperty(value = "interface-id1", required = true, position = 0, example = "5")
    @NotBlank(message = "interface-id1不能为空")
    private String id;

}
