# Easy-Ms-Redis

　　easy-ms-redis是一个基于spring的、用于redis的功能增强型框架，是Easy-Ms的一个组件框架，**但也支持在非Easy-Ms项目的Spring Boot项目中使用**，阿里巴巴开发规范里的应用分层规范里增加了通用处理层（Manager层），easy-ms-redis框架让该规范得到更好的展现。。

## 一、 功能特点

1. 支持添加前缀（当多个系统共用一个redis时，增加前缀可避免key值重复）
2. 使用注解可增加开发效率，使开发标准化，可防止开发不规范引起的BUG
3. 统一管理，可实时在页面查询所有使用中的redis的key值，防止使用keys查询所有key值时引起系统崩溃
4. 支持分布式锁（使用lua脚本编写，可以防止死锁）

## 二、 依赖引入

#### 1. 使用parent的方式

　　好处：easy-ms-parent里包含了许多的依赖版本管理，可以在依赖管理`<dependencies>`中，有较多常用的引入直接使用默认的版本而无需指定版本号

```xml
pom.xml:
    <!-- 可以直接把easy-ms-parent当做parent，或者当引入多个easy-ms组件时使用parent可以只需设置一次版本号 -->
    <parent>
        <groupId>com.stars.easyms</groupId>
        <artifactId>easy-ms-parent</artifactId>
        <version>${easy-ms.version}</version>
    </parent>
		
    <!-- 如果是直接引入easy-ms，则不需要引入easy-ms-redis，easy-ms已经默认依赖了easy-ms-redis -->
    <dependencies>
        <dependency>
            <groupId>com.stars.easyms</groupId>
            <artifactId>easy-ms-redis</artifactId>
        </dependency>
    </dependencies>
```

#### 2. 直接引用

```xml
    <!-- 如果是直接引入easy-ms，则不需要引入easy-ms-redis，easy-ms已经默认依赖了easy-ms-redis -->
    <dependencies>
        <dependency>
            <groupId>com.stars.easyms</groupId>
            <artifactId>easy-ms-redis</artifactId>
            <version>${easy-ms.version}</version>
        </dependency>
    </dependencies>
```   
#### 3. redis配置
   ```
   <!-- redis 开关，true则是使用redis false则不使用redis -->
    spring.redis.enabled = true
    <!-- redis前缀 -->
    spring.redis.prefix = xxx
    <!-- redis最大连接数 -->
    spring.redis.lettuce.pool.max-active = 200
    <!-- redis最大等待毫秒数-->
    spring.redis.lettuce.pool.max-wait = 10ms
    <!-- redis最大空闲数-->
    spring.redis.lettuce.pool.max-idle = 100
    <!-- redis最小空闲数-->
    spring.redis.lettuce.pool.min-idle = 10
    <!-- redis节点以及密码的配置-->
    spring.redis.sentinel.master = 
    spring.redis.sentinel.nodes =
    spring.redis.password = XXXX
   ```
#### 4.注解属性详解
『注解声明式』      
  &emsp;&emsp; **注解声明式的原则： 
  ##### 1. **类或者接口的注解（**Service层以及Manager层**）** 
   + @RedisManager （**统一REDIS管理类**）  
        
  | 属性           | 默认值               | 说明                                                                   |   
  | -------------- | -------------------- | -----------------------------------------------------------------------|
  | value          | ""                   | 缓存组名称                                                             |
  | group          |                      | redis缓存组名称，表示该类统一的group，如果方法上也指定了group则方法优先|     
   
  ##### 2. **方法上的注解(**Service层以及Manager层**)**  
   ##### 1. string注解  
   + @RedisStringGet (**String类型缓存获取**) 
    
   | 属性           | 默认值               | 说明                                                         |   
   | -------------- | -------------------- | ------------------------------------------------------------ |
   | group          | ""                   | 缓存组名称                                                   |
   | expire         |"-1ms"                | 缓存失效时间.默认是没有缓存时间                              |
   | prefix         |true                 | 前缀标识， 默认是使用前缀（配置中的前缀）                    |
           
   + @RedisStringSet(**String类型缓存存储**) 
   
   | 属性           | 默认值               | 说明                                                         |   
   | -------------- | -------------------- | ------------------------------------------------------------ |
   | group          | ""                   | 缓存组名称                                                   |
   | expire         |"-1ms"                | 缓存失效时间.默认是没有缓存时间                              |
   | resetExpire    |true                  | 是否强制重置失效时间                                         |
   | prefix         |true                 | 前缀标识， 默认是使用前缀（配置中的前缀）                    |
   
##### 2. Hash注解
  + @RedisHashGet (**HASH类型缓存获取**)
  
   | 属性           | 默认值               | 说明                                                         |   
   | -------------- | -------------------- | ------------------------------------------------------------ |
   | group          | ""                   | 缓存组名称                                                   |
   | expire         |"-1ms"                | 缓存失效时间.默认是没有缓存时间                              |
   | prefix         |true                  | 前缀标识， 默认是使用前缀（配置中的前缀）                    |
   
   + @RedisHashSet (**HASH类型缓存存储**)
    
   | 属性           | 默认值               | 说明                                                         |   
   | -------------- | -------------------- | ------------------------------------------------------------ |
   | group          | ""                   | 缓存组名称                                                   |
   | expire         |"-1ms"                | 缓存失效时间.默认是没有缓存时间                              |
   | resetExpire    |true                  | 是否强制重置失效时间                                         |
   | override       |false                 | set结果覆盖之前的缓存数据                                    |
   | prefix         |true                  | 前缀标识， 默认是使用前缀（配置中的前缀）                    |

##### 3. List注解
   + @RedisListPush (**LIST类型缓存存储**)

   | 属性           | 默认值               | 说明                                                         |   
   | -------------- | -------------------- | ------------------------------------------------------------ |
   | group          | ""                   | 缓存组名称                                                   |
   | expire         |"-1ms"                | 缓存失效时间.默认是没有缓存时间                              |
   | resetExpire    |true                  | 是否强制重置失效时间                                         |
   | type           |"L"                   | L代表为 LPUSH R代表为 RPUSH                                  |
   | prefix         |true                  | 前缀标识， 默认是使用前缀（配置中的前缀）                    |
   
   + @RedisListRange (**LIST类型缓存获取**)

   | 属性           | 默认值               | 说明                                                         |   
   | -------------- | -------------------- | ------------------------------------------------------------ |
   | group          | ""                   | 缓存组名称                                                   |
   | expire         |"-1ms"                | 缓存失效时间.默认是没有缓存时间                              |
   | override       |false                 | 结果覆盖之前的缓存数据                                       |
   | type           |"L"                   | 如果缓存不存在时，指定重新存入缓存（L代表为 LPUSH R代表为 RPUSH）                                  |
   | prefix         |true                  | 前缀标识， 默认是使用前缀（配置中的前缀）                    |   
   
#### 4.Set注解
   + @RedisSetAdd (**SET类型缓存存储**)   

   | 属性           | 默认值               | 说明                                                         |   
   | -------------- | -------------------- | ------------------------------------------------------------ |
   | group          | ""                   | 缓存组名称                                                   |
   | expire         |"-1ms"                | 缓存失效时间.默认是没有缓存时间                              |
   | resetExpire    |true                  | 是否强制重置失效时间                                         |
   | prefix         |true                  | 前缀标识， 默认是使用前缀（配置中的前缀）                    |   

   + @RedisSetMembers (**SET类型缓存获取**)

   | 属性           | 默认值               | 说明                                                         |   
   | -------------- | -------------------- | ------------------------------------------------------------ |
   | group          | ""                   | 缓存组名称                                                   |
   | expire         |"-1ms"                | 缓存失效时间.默认是没有缓存时间                              |
   | override       |false                 | 结果覆盖之前的缓存数据                                       |
   | prefix         |true                  | 前缀标识， 默认是使用前缀（配置中的前缀）                    |
   
##### 5. Delete注解
   + @RedisDelete (**缓存清除**)

   | 属性           | 默认值               | 说明                                                         |   
   | -------------- | -------------------- | ------------------------------------------------------------ |
   | group          | ""                   | 缓存组名称                                                   |
   | prefix         |true                  | 前缀标识， 默认是使用前缀（配置中的前缀）                    |
   
##### 6.LUA脚本注解
   + @RedisEval (**LUA脚本**)
   
   | 属性           | 默认值               | 说明                                                         |   
   | -------------- | -------------------- | ------------------------------------------------------------ |
   | script         | ""                   | lua脚本内容                                                  |
   
##### 7. Expire注解
   + @RedisExpire (**有效时间设置**)
    
   | 属性           | 默认值               | 说明                                                         |   
   | -------------- | -------------------- | ------------------------------------------------------------ |
   | group          | ""                   | 缓存组名称                                                   |
   | expire         |"-1ms"                | 缓存失效时间.默认是没有缓存时间                              |
   | resetExpire    |true                  | 是否强制重置失效时间                                         |
   | prefix         |true                  | 前缀标识， 默认是使用前缀（配置中的前缀）                    |
   
   + @RedisTtl (**剩余有效时间获取**)      

   | 属性           | 默认值               | 说明                                                         |   
   | -------------- | -------------------- | ------------------------------------------------------------ |
   | group          | ""                   | 缓存组名称                                                   |
   | prefix         |true                  | 前缀标识， 默认是使用前缀（配置中的前缀）                    |
   
##### 8. RedisExists注解          
   + @RedisExists (**缓存是否存在**)     
 
   | 属性           | 默认值               | 说明                                                         |   
   | -------------- | -------------------- | ------------------------------------------------------------ |
   | group          | ""                   | 缓存组名称                                                   |
   | prefix         |true                  | 前缀标识， 默认是使用前缀（配置中的前缀）                    |   
    
#### 5.使用方法举例    
    
   ```java
       @RedisManager
       public class RedisDemoManager {
       
           @Autowired
           private UserInfoDAO userInfoDAO;
       
           /**
            * RedisStringSet注解用于redis的String类型，返回值为缓存值（不读取redis是否有缓存，直接执行方法然后把返回值放入redis）
            */
           @RedisStringSet(group = DemoConstants.REDIS_ALL_USER_INFO, expire = "2h")
           public List<UserInfo> setAllUserInfo() {
               return userInfoDAO.getAllUserInfo();
           }
       
           /**
            * RedisStringGet注解用于redis的String类型，返回值为缓存值（读取redis，若存在则返回，若不存在则执行方法并把返回值放入redis）
            */
           @RedisStringGet(group = DemoConstants.REDIS_ALL_USER_INFO, expire = "2h")
           public List<UserInfo> getAllUserInfo() {
               return userInfoDAO.getAllUserInfo();
           }
       
           /**
            * RedisStringSet注解用于redis的String类型，返回值为缓存值（不读取redis是否有缓存，直接执行方法然后把返回值放入redis）
            */
           @RedisStringSet(group = DemoConstants.REDIS_ALL_USER_INFO, expire = "2h")
           public List<UserInfo> setUserInfoByName(@RedisKey String name) {
               return userInfoDAO.getAllUserInfo();
           }
       
           /**
            * RedisStringGet注解用于redis的String类型，返回值为缓存值（读取redis，若存在则返回，若不存在则执行方法并把返回值放入redis）
            */
           @RedisStringGet(group = DemoConstants.REDIS_ALL_USER_INFO, expire = "2h")
           public List<UserInfo> getUserInfoByName(@RedisKey String name) {
               return userInfoDAO.getAllUserInfo();
           }
       
           /**
            * RedisStringSet注解用于redis的hash类型，RedisHashKey为具体的hash的key值，返回值为缓存值（不读取redis是否有缓存，直接执行方法然后把返回值放入redis）
            */
           @RedisHashSet(group = DemoConstants.REDIS_USER_INFO, expire = "5m3s")
           public UserInfo setUserInfo(@RedisHashKey String name) {
               return userInfoDAO.getUserInfoByName(name);
           }
       
           /**
            * RedisStringSet注解用于redis的hash类型，RedisHashKey为具体的hash的key值，返回值为缓存值（读取redis，若存在则返回，若不存在则执行方法并把返回值放入redis）
            */
           @RedisHashGet(group = DemoConstants.REDIS_USER_INFO, expire = "10m5s3ms")
           public UserInfo getUserInfo(@RedisHashKey String name) {
               return userInfoDAO.getUserInfoByName(name);
           }
       
           /**
            * RedisDelete用于删除redis缓存，方法体可有可无，方法体无删除数据库操作的情况下只删除redis不删除数据库
            */
           @RedisDelete(group = DemoConstants.REDIS_ALL_USER_INFO)
           public void deleteAllUserInfo() {
               userInfoDAO.deleteAllUserInfo();
           }
       
           /**
            * RedisDelete用于删除redis缓存，方法体可有可无，方法体无删除数据库操作的情况下只删除redis不删除数据库
            */
           @RedisDelete(group = DemoConstants.REDIS_USER_INFO)
           public void deleteUserInfoByName(@RedisHashKey String name) {
               userInfoDAO.deleteUserInfoByName(name);
           }
       
           /**
            * RedisExpire用于设置redis的过期时间，优先方法返回的值，若返回值小于等于0则使用注解的expire属性值，若expire属性值大于0时则设置超时时间
            */
           @RedisExpire(group = DemoConstants.REDIS_ALL_USER_INFO, expire = "2h")
           public Long setUserInfoExpire(@RedisKey String name) {
               return 0L;
           }
       
           /**
            * RedisExists用于判断redis中是否有该缓存
            */
           @RedisExists(group = DemoConstants.REDIS_ALL_USER_INFO)
           public boolean isExistUserInfo(@RedisKey String name) {
               return false;
           }
       
           /**
            * RedisEval用于执行redis的lua脚本
            */
           @RedisEval(script = "local messageKey=KEYS[1];local filedKey=ARGV[1]; local batchNo=ARGV[2];redis.pcall('hset', messageKey, filedKey, batchNo);")
           public String getPushMessageBatchNo(@RedisEvalKey List<String> keys, @RedisEvalArg List<String> args) {
               return null;
           }
       
           /**
            * RedisListPush 用户获取查询的结果集合按照List集合方式存储到Redis中(不读取redis是否有缓存，直接执行方法然后把返回值放入redis)
            * Type表示List存储的方式（LPush和RPush）
            * @param name
            * @return
            */
           @RedisListPush(group =  DemoConstants.REDIS_USER_INFO, expire = "2h", type = "R")
           public List<UserInfo> setUserInfoList(@RedisKey String name){
               return userInfoDAO.getAllUserInfo();
           }
       
           /**
            * RedisListRange注解用于redis的List类型
            * 如果缓存中存在, 则从缓存中获取
            * 否则执行方法, 处理结果进行返回（type 设定如果缓存中不存在redisKey， 执行存储方式）
            * @param name
            * @param indexStart 截取开始区间
            * @param indexEnd 截取结束区间
            * @return
            */
           @RedisListRange(group =  DemoConstants.REDIS_USER_INFO, expire = "2h", type = "R")
           public List<UserInfo> getUserInfoList(@RedisKey String name, @RedisIndexStart Long indexStart, @RedisIndexEnd Long indexEnd){
               return userInfoDAO.getAllUserInfo();
           }
       
           /**
            * RedisSetAdd注解用于redis的set类型
            * RedisSetAdd 用户获取查询的结果集合按照Set集合方式存储到Redis中(不读取redis是否有缓存，直接执行方法然后把返回值放入redis)
            * RedisSetAdd 返回类型支持List和Set两种类型
            * @param name
            * @return
            */
           @RedisSetAdd(group =  DemoConstants.REDIS_USER_INFO, expire = "2h")
           public List<UserInfo> setUserInfoSet(@RedisKey String name){
               return userInfoDAO.getAllUserInfo();
           }
           /**
            * RedisSetAdd注解用于redis的set类型
            * RedisSetAdd 用户获取查询的结果集合按照Set集合方式存储到Redis中(不读取redis是否有缓存，直接执行方法然后把返回值放入redis)
            * RedisSetAdd 返回类型支持List和Set两种类型
            * @param name
            * @return
            */
           @RedisSetAdd(group =  DemoConstants.REDIS_USER_INFO, expire = "2h")
           public Set<UserInfo> setUserInfoSet1(@RedisKey String name){
               return userInfoDAO.getAllUserInfoSet();
           }
       
           /**
            * RedisSetMembers注解用于redis的set类型，返回值为缓存值（读取redis，若存在则返回，若不存在则执行方法并把返回值放入redis）
            * RedisSetMembers 返回类型支持List和Set两种类型
            * @param name
            * @return
            */
           @RedisSetMembers(group =  DemoConstants.REDIS_USER_INFO, expire = "2h")
           public List<UserInfo> getUserInfoSet(@RedisKey String name){
               return userInfoDAO.getAllUserInfoNoResult();
           }
       
       }
```

> 　　我们提供框架的维护和技术支持，提供给感兴趣的开发人员学习和交流的平台，同时希望感兴趣的同学一起加入我们让框架更完善，让我们一起为了给大家提供更好更优秀的框架而努力。
>
> 　　若该框架及本文档中有什么错误及问题的，可采用创建Issue、加群@管理、私聊作者或管理员的方式进行联系，谢谢！