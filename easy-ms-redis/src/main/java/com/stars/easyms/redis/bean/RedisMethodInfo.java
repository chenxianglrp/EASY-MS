package com.stars.easyms.redis.bean;

import lombok.Data;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * <p>className: RedisMethodInfo</p>
 * <p>description: RedisMethodInfo</p>
 *
 * @author guoguifang
 * @version 1.2.1
 * @date 2019-03-20 15:56
 */
@Data
public class RedisMethodInfo {

    /**
     * 是否使用前缀
     */
    private Boolean usePrefix;

    /**
     * redis的key前缀
     */
    private String prefix;

    /**
     * 包含prefix的业务分组
     */
    private String group;

    /**
     * redis的方法
     */
    private Method method;

    /**
     * redis方法的详细方法名
     */
    private String fullMethodName;

    /**
     * redis方法上的注解类型
     */
    private Class<? extends Annotation> redisAnnotation;

    /**
     * redis方法上的注解类型名字全称
     */
    private String redisAnnotationString;

}