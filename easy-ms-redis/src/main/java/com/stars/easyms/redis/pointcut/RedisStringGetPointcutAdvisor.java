package com.stars.easyms.redis.pointcut;

import com.stars.easyms.redis.annotion.RedisStringGet;
import com.stars.easyms.redis.initializer.RedisManagerInitializer;
import com.stars.easyms.redis.interceptor.RedisStringGetInterceptor;
import com.stars.easyms.redis.template.EasyMsRedisTemplate;
import org.aopalliance.aop.Advice;
import org.springframework.aop.Pointcut;
import org.springframework.aop.support.AbstractPointcutAdvisor;
import org.springframework.aop.support.annotation.AnnotationMatchingPointcut;
import org.springframework.util.ObjectUtils;

/**
 * <p>className: RedisStringGetPointcutAdvisor</p>
 * <p>description: RedisStringGet的顾问类</p>
 *
 * @author guoguifang
 * @version 1.2.1
 * @date 2019-03-18 15:32
 */
public final class RedisStringGetPointcutAdvisor extends AbstractPointcutAdvisor {

    private transient RedisStringGetInterceptor advice;

    private transient Pointcut pointcut;

    public RedisStringGetPointcutAdvisor(RedisManagerInitializer redisManagerInitializer, EasyMsRedisTemplate easyMsRedisTemplate) {
        this.advice = new RedisStringGetInterceptor();
        this.advice.setRedisManagerInitializer(redisManagerInitializer);
        this.advice.setEasyMsRedisTemplate(easyMsRedisTemplate);
        this.advice.setClassAnnotationType(RedisStringGet.class);
        this.pointcut = new AnnotationMatchingPointcut(null, RedisStringGet.class, false);
    }

    @Override
    public Pointcut getPointcut() {
        return this.pointcut;
    }

    @Override
    public Advice getAdvice() {
        return this.advice;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || this.getClass() != other.getClass()) {
            return false;
        }
        RedisStringGetPointcutAdvisor otherAdvisor = (RedisStringGetPointcutAdvisor) other;
        return (ObjectUtils.nullSafeEquals(this.advice, otherAdvisor.advice) &&
                ObjectUtils.nullSafeEquals(this.pointcut, otherAdvisor.pointcut));
    }

    @Override
    public int hashCode() {
        return 41 + this.advice.hashCode() * 41 + this.pointcut.hashCode() * 41;
    }
}