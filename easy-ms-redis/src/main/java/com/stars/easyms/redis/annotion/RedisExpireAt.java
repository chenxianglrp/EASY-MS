package com.stars.easyms.redis.annotion;

import java.lang.annotation.*;

/**
 * <p>className: RedisExpireAt</p>
 * <p>description: 过期时间为绝对时间</p>
 *
 * @author guoguifang
 * @version 1.6.3
 * @date 2020/9/9 4:17 下午
 */
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RedisExpireAt {
}
