package com.stars.easyms.redis.annotion;

import java.lang.annotation.*;

/**
 * 提供redis服务的方法参数注解(只用于RedisType.HASH类型)
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RedisHashKey {
}
