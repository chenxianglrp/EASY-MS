package com.stars.easyms.redis.annotion;

import java.lang.annotation.*;

/**
 * 提供redis del服务的方法注解
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RedisDelete {

    /**
     * redis缓存组名称
     */
    String group() default "";

    /**
     * key值是否带前缀
     */
    boolean usePrefix() default true;

    /**
     * 自定义前缀，支持el表达式
     */
    String prefix() default "";

}
