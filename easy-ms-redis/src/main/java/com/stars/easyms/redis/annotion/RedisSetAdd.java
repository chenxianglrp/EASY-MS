package com.stars.easyms.redis.annotion;

import java.lang.annotation.*;

/**
 * 提供redis set add 服务的方法注解
 *
 * @author jinzhilong
 * @date 2019-12-18 14:07
 * @since 1.0.0
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RedisSetAdd {

    /**
     * redis缓存组名称
     */
    String group() default "";

    /**
     * redis缓存失效时间，默认没有失效时间
     */
    String expire() default "-1ms";

    /**
     * 若redis缓存未失效，是否强制重置失效时间
     */
    boolean resetExpire() default true;

    /**
     * key值是否带前缀
     */
    boolean usePrefix() default true;

    /**
     * 自定义前缀，支持el表达式
     */
    String prefix() default "";

}
