package com.stars.easyms.redis.pointcut;

import com.stars.easyms.redis.annotion.RedisSetAdd;
import com.stars.easyms.redis.initializer.RedisManagerInitializer;
import com.stars.easyms.redis.interceptor.RedisSetAddInterceptor;
import com.stars.easyms.redis.template.EasyMsRedisTemplate;
import org.aopalliance.aop.Advice;
import org.springframework.aop.Pointcut;
import org.springframework.aop.support.AbstractPointcutAdvisor;
import org.springframework.aop.support.annotation.AnnotationMatchingPointcut;
import org.springframework.util.ObjectUtils;

/**
 * <p>className: RedisSetAddPointcutAdvisor</p>
 * <p>description: RedisSetAdd的顾问类</p>
 *
 * @author jinzhilong
 * @version 1.0.0
 * @date 2019-12-25 10:41
 */
public final class RedisSetAddPointcutAdvisor extends AbstractPointcutAdvisor {

    private transient RedisSetAddInterceptor advice;

    private transient Pointcut pointcut;

    public RedisSetAddPointcutAdvisor(RedisManagerInitializer redisManagerInitializer, EasyMsRedisTemplate easyMsRedisTemplate) {
        this.advice = new RedisSetAddInterceptor();
        this.advice.setRedisManagerInitializer(redisManagerInitializer);
        this.advice.setEasyMsRedisTemplate(easyMsRedisTemplate);
        this.advice.setClassAnnotationType(RedisSetAdd.class);
        this.pointcut = new AnnotationMatchingPointcut(null, RedisSetAdd.class, false);
    }

    @Override
    public Pointcut getPointcut() {
        return this.pointcut;
    }

    @Override
    public Advice getAdvice() {
        return this.advice;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || this.getClass() != other.getClass()) {
            return false;
        }
        RedisSetAddPointcutAdvisor otherAdvisor = (RedisSetAddPointcutAdvisor) other;
        return (ObjectUtils.nullSafeEquals(this.advice, otherAdvisor.advice) &&
                ObjectUtils.nullSafeEquals(this.pointcut, otherAdvisor.pointcut));
    }

    @Override
    public int hashCode() {
        return 41 + this.advice.hashCode() * 41 + this.pointcut.hashCode() * 41;
    }
}