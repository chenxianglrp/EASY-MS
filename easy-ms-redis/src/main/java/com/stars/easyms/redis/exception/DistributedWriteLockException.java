package com.stars.easyms.redis.exception;

/**
 * 分布式写锁异常.
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/7/29 6:38 下午
 */
public class DistributedWriteLockException extends RuntimeException {
    
    private static final long serialVersionUID = -2034292390445464637L;
    
    public DistributedWriteLockException(Throwable throwable) {
        super(throwable);
    }
}
