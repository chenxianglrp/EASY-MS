package com.stars.easyms.redis.annotion;

import com.stars.easyms.redis.enums.RedisListPushType;

import java.lang.annotation.*;

/**
 * <p>annotationName: RedisListRange</p>
 * <p>description: redis的list类型范围取值</p>
 *
 * @author jinzhilong
 * @date 2019-12-18 14:47
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RedisListRange {
    /**
     * redis缓存组名称
     */
    String group() default "";

    /**
     * redis缓存失效时间，默认没有失效时间
     */
    String expire() default "-1ms";

    /**
     * redis list L-LPUSH R-RPUSH ，默认从左侧取出
     */
    RedisListPushType type() default RedisListPushType.LEFT;

    /**
     * 是否获取数据时候返回结果覆盖之前的数据
     */
    boolean override() default false;

    /**
     * key值是否带前缀
     */
    boolean usePrefix() default true;

    /**
     * 自定义前缀，支持el表达式
     */
    String prefix() default "";

}
