package com.stars.easyms.redis.core;

import com.stars.easyms.base.util.GenericTypeUtil;
import com.stars.easyms.base.util.JsonUtil;
import org.springframework.data.redis.core.Cursor;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Map;

/**
 * <p>className: EasyMsCursor</p>
 * <p>description: EasyMs自定义redis scan返回游标</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/7 1:31 下午
 */
@SuppressWarnings("unchecked")
public class EasyMsCursor<K, V> implements Cursor<Map.Entry<K, V>> {

    private Cursor<Map.Entry<String, String>> delegate;

    private ParameterizedType entryKeyType;

    private Class<K> entryKeyClass;

    private ParameterizedType entryValueType;

    private Class<V> entryValueClass;

    public EasyMsCursor(Cursor<Map.Entry<String, String>> cursor, Type convertType) {
        Assert.notNull(cursor, "Cursor delegate must not be 'null'.");
        Assert.notNull(cursor, "Converter must not be 'null'.");
        this.delegate = cursor;
        this.entryKeyType = GenericTypeUtil.getGenericType(convertType, 0);
        this.entryValueType = GenericTypeUtil.getGenericType(convertType, 1);
        this.entryKeyClass = GenericTypeUtil.getGenericClass(convertType, 0);
        this.entryValueClass = GenericTypeUtil.getGenericClass(convertType, 1);
    }

    @Override
    public boolean hasNext() {
        return delegate.hasNext();
    }

    @Override
    @Nullable

    public Map.Entry<K, V> next() {
        Map.Entry<String, String> nextValue = delegate.next();
        return new Map.Entry<K, V>() {

            @Override
            public K getKey() {
                return (K) JsonUtil.parseObject(nextValue.getKey(), entryKeyType, entryKeyClass);
            }

            @Override
            public V getValue() {
                return (V) JsonUtil.parseObject(nextValue.getValue(), entryValueType, entryValueClass);
            }

            @Override
            public V setValue(V value) {
                throw new UnsupportedOperationException("Values cannot be set when scanning through entries.");
            }
        };
    }

    @Override
    public void remove() {
        delegate.remove();
    }

    @Override
    public void close() throws IOException {
        delegate.close();
    }

    @Override
    public long getCursorId() {
        return delegate.getCursorId();
    }

    @Override
    public boolean isClosed() {
        return delegate.isClosed();
    }

    @NonNull
    @Override
    public Cursor<Map.Entry<K, V>> open() {
        this.delegate = delegate.open();
        return this;
    }

    @Override
    public long getPosition() {
        return delegate.getPosition();
    }
}
