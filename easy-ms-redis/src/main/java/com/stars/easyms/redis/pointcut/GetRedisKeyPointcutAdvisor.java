package com.stars.easyms.redis.pointcut;

import com.stars.easyms.redis.annotion.GetRedisKey;
import com.stars.easyms.redis.initializer.RedisManagerInitializer;
import com.stars.easyms.redis.interceptor.GetRedisKeyInterceptor;
import com.stars.easyms.redis.template.EasyMsRedisTemplate;
import org.aopalliance.aop.Advice;
import org.springframework.aop.Pointcut;
import org.springframework.aop.support.AbstractPointcutAdvisor;
import org.springframework.aop.support.annotation.AnnotationMatchingPointcut;
import org.springframework.util.ObjectUtils;

/**
 * <p>className: GetRedisKeyPointcutAdvisor</p>
 * <p>description: GetRedisKey的顾问类</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/7 11:32 上午
 */
public final class GetRedisKeyPointcutAdvisor extends AbstractPointcutAdvisor {

    private transient GetRedisKeyInterceptor advice;

    private transient Pointcut pointcut;

    public GetRedisKeyPointcutAdvisor(RedisManagerInitializer redisManagerInitializer, EasyMsRedisTemplate easyMsRedisTemplate) {
        this.advice = new GetRedisKeyInterceptor();
        this.advice.setRedisManagerInitializer(redisManagerInitializer);
        this.advice.setEasyMsRedisTemplate(easyMsRedisTemplate);
        this.advice.setClassAnnotationType(GetRedisKey.class);
        this.pointcut = new AnnotationMatchingPointcut(null, GetRedisKey.class, false);
    }

    @Override
    public Pointcut getPointcut() {
        return this.pointcut;
    }

    @Override
    public Advice getAdvice() {
        return this.advice;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || this.getClass() != other.getClass()) {
            return false;
        }
        GetRedisKeyPointcutAdvisor otherAdvisor = (GetRedisKeyPointcutAdvisor) other;
        return (ObjectUtils.nullSafeEquals(this.advice, otherAdvisor.advice) &&
                ObjectUtils.nullSafeEquals(this.pointcut, otherAdvisor.pointcut));
    }

    @Override
    public int hashCode() {
        return 41 + this.advice.hashCode() * 41 + this.pointcut.hashCode() * 41;
    }
}