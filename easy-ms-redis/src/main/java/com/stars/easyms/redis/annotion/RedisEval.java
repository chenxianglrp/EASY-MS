package com.stars.easyms.redis.annotion;

import java.lang.annotation.*;

/**
 * 提供redis get服务的方法注解
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RedisEval {

    /**
     * 可执行的lua脚本内容
     */
    String script();

}
