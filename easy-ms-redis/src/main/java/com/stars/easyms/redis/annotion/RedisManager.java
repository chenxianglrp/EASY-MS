package com.stars.easyms.redis.annotion;

import com.stars.easyms.base.annotation.Manager;
import org.springframework.core.annotation.AliasFor;
import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * 提供redis服务的类注解
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Manager
public @interface RedisManager {

    @AliasFor(annotation = Component.class)
    String value() default "";

    /**
     * redis缓存组名称，表示该类统一的group，如果方法上也指定了group则方法优先
     */
    String group() default "";
}
