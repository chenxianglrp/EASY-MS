package com.stars.easyms.redis.interceptor;

import com.stars.easyms.redis.annotion.RedisExists;
import com.stars.easyms.redis.exception.RedisInterceptorException;
import org.aopalliance.intercept.MethodInvocation;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

/**
 * <p>className: RedisExistsInterceptor</p>
 * <p>description: RedisExists注解拦截器</p>
 *
 * @author guoguifang
 * @version 1.2.1
 * @date 2019-03-18 14:39
 */
public class RedisExistsInterceptor extends AbstractRedisInterceptor<RedisExists> {

    @Override
    protected Object invoke(MethodInvocation methodInvocation, RedisExists redisExists) throws RedisInterceptorException {
        if (isEnabled()) {
            Method pointMethod = methodInvocation.getMethod();
            Class<?> returnType = pointMethod.getReturnType();
            String redisKey = getRedisKey(methodInvocation, redisExists.group());
            Set<String> hashKeySet = getRedisHashKey(methodInvocation);

            // 如果hashKeySet为null，表示没有设置hashKey，则只查询redisKey
            if (hashKeySet == null) {
                boolean hasKey = easyMsRedisTemplate.hasKey(redisKey);
                if (Map.class.isAssignableFrom(returnType)) {
                    return Collections.singletonMap(redisKey, hasKey);
                }
                return hasKey;
            }

            // 如果hashKeySet不为null,但是为空set,则不需要查询直接返回false
            if (hashKeySet.isEmpty()) {
                if (Map.class.isAssignableFrom(returnType)) {
                    return Collections.emptyMap();
                }
                return false;
            }

            // 如果hashKeySet不为null,但是只有一个hashKey,则查询直接返回false
            if (hashKeySet.size() == 1) {
                String hashKey = (String) hashKeySet.toArray()[0];
                boolean hasKey = easyMsRedisTemplate.hashHasKey(redisKey, hashKey);
                if (Map.class.isAssignableFrom(returnType)) {
                    return Collections.singletonMap(hashKey, hasKey);
                }
                return hasKey;
            }

            // 如果hashKeySet不为null,但是只有一个hashKey,则查询直接返回false
            Map<String, Boolean> hasKeyMap = easyMsRedisTemplate.hashHasKey(redisKey, hashKeySet);
            if (Map.class.isAssignableFrom(returnType)) {
                return hasKeyMap;
            }
            return !hasKeyMap.isEmpty();
        }
        return proceed(methodInvocation);
    }

}