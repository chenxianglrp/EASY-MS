package com.stars.easyms.redis.constant;

/**
 * <p>className: EasyMsRedisConstants</p>
 * <p>description: EasyMsRedis常量类</p>
 *
 * @author guoguifang
 * @date 2019-11-25 17:52
 * @since 1.4.1
 */
public final class EasyMsRedisConstants {

    public static final String MODULE_NAME = "redis";

    private EasyMsRedisConstants() {}
}