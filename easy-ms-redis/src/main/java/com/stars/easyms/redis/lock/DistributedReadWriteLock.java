package com.stars.easyms.redis.lock;

import com.stars.easyms.base.constant.EasyMsCommonConstants;
import com.stars.easyms.base.function.EasyMsRunnable;
import com.stars.easyms.base.util.ThreadUtil;
import com.stars.easyms.redis.exception.DistributedLockTimeoutException;
import com.stars.easyms.redis.exception.DistributedWriteLockException;
import com.stars.easyms.redis.exception.RedisRuntimeException;
import com.stars.easyms.redis.template.EasyMsRedisTemplate;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.script.RedisScript;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

/**
 * 分布式读写锁.
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/7/28 10:00 上午
 */
@Slf4j
public final class DistributedReadWriteLock {
    
    private static EasyMsRedisTemplate easyMsRedisTemplate;
    
    
    private static final RedisScript READ_LOCK_REDIS_SCRIPT = RedisScript
            .of("local rs=redis.call('exists',KEYS[2]);if(rs==1) then return 'F';end;redis.call('rpush',KEYS[1],ARGV[1]);redis.call('expire',KEYS[1],tonumber(ARGV[2]));return 'S';",
                    String.class);
    
    private static final RedisScript WRITE_LOCK_REDIS_SCRIPT = RedisScript
            .of("local rs=redis.call('setnx',KEYS[1],ARGV[1]);if(rs<1) then return 'F';end;redis.call('expire',KEYS[1],tonumber(ARGV[2]));return 'S';",
                    String.class);
    
    private static final RedisScript WRITE_LOCK_UNLOCK_REDIS_SCRIPT = RedisScript
            .of("local rs=redis.call('get',KEYS[1]);if(rs==ARGV[1]) then redis.call('del',KEYS[1]);end;", String.class);
    
    private static final String DEFAULT_DISTRIBUTED_READ_WRITE_LOCK_PREFIX = "DistributedReadWriteLock";
    
    private static final String DEFAULT_READ_LOCK_SUFFIX = "readLock";
    
    private static final String DEFAULT_WRITE_LOCK_SUFFIX = "writeLock";
    
    private static final int DEFAULT_EXPIRE_SECOND = 60;
    
    private static final int DEFAULT_RETRY_INTERVAL = 100;
    
    private final String redisReadKey;
    
    private final String redisWriteKey;
    
    private int expireSecond;
    
    /**
     * 创建一个分布式读写锁
     *
     * @param lockName 锁名称
     */
    public DistributedReadWriteLock(String lockName) {
        this(lockName, null, DEFAULT_EXPIRE_SECOND);
    }
    
    /**
     * 创建一个分布式读写锁
     *
     * @param lockName 锁名称
     * @param item     子项
     */
    public DistributedReadWriteLock(String lockName, String item) {
        this(lockName, item, DEFAULT_EXPIRE_SECOND);
    }
    
    /**
     * 创建一个分布式读写锁
     *
     * @param lockName     锁名称
     * @param expireSecond 锁失效时间(单位：秒)
     */
    public DistributedReadWriteLock(String lockName, int expireSecond) {
        this(lockName, null, expireSecond);
    }
    
    /**
     * 创建一个分布式读写锁
     *
     * @param lockName     锁名称
     * @param item         子项
     * @param expireSecond 锁失效时间(单位：秒)
     */
    public DistributedReadWriteLock(String lockName, String item, int expireSecond) {
        if (StringUtils.isBlank(lockName)) {
            throw new IllegalArgumentException("DistributedReadWriteLock lockName can't be blank!");
        }
        String redisKey = getEasyMsRedisTemplate()
                .getRedisKeyWithDefaultPrefix(DEFAULT_DISTRIBUTED_READ_WRITE_LOCK_PREFIX, lockName, item);
        this.redisReadKey = StringUtils
                .join(new Object[] {redisKey, DEFAULT_READ_LOCK_SUFFIX}, EasyMsCommonConstants.REDIS_KEY_SEPARATOR);
        this.redisWriteKey = StringUtils
                .join(new Object[] {redisKey, DEFAULT_WRITE_LOCK_SUFFIX}, EasyMsCommonConstants.REDIS_KEY_SEPARATOR);
        this.expireSecond = expireSecond <= 0 ? DEFAULT_EXPIRE_SECOND : expireSecond;
    }
    
    /**
     * 不需指定超时时间的读锁，慎用，容易造成死锁
     *
     * @param supplier 获取到读锁后执行的任务
     */
    public <T> T executeRead(Supplier<T> supplier) {
        String uuid = UUID.randomUUID().toString();
        // 判断是否存在写锁，如果存在写锁则等待写锁释放
        while (!getReadLock(uuid)) {
            ThreadUtil.sleep(DEFAULT_RETRY_INTERVAL, TimeUnit.MILLISECONDS);
        }
        try {
            return supplier.get();
        } finally {
            unlockReadLock(uuid);
        }
    }
    
    /**
     * 指定超时时间的读锁，超过后报获取锁超时异常
     *
     * @param supplier 获取到读锁后执行的任务
     * @param timeout  超时时间，单位为毫秒
     */
    public <T> T executeRead(Supplier<T> supplier, long timeout) throws DistributedLockTimeoutException {
        long currTime = System.currentTimeMillis();
        String uuid = UUID.randomUUID().toString();
        // 判断是否存在写锁，如果存在写锁则等待写锁释放
        while (!getReadLock(uuid)) {
            if (System.currentTimeMillis() - currTime >= timeout) {
                throw new DistributedLockTimeoutException("Get redis distributed read lock {} time out!", redisReadKey);
            }
            ThreadUtil.sleep(DEFAULT_RETRY_INTERVAL, TimeUnit.MILLISECONDS);
        }
        try {
            return supplier.get();
        } finally {
            unlockReadLock(uuid);
        }
    }
    
    /**
     * 单次加写锁并执行任务: 获取到写锁后无论是否当前有读锁都执行任务，如果没有获取到写锁则跳过
     *
     * @param runnable 获取到写锁后执行的任务
     */
    public void tryExecuteWrite(EasyMsRunnable runnable) {
        getWriteLockAndExecuteRunnable(runnable, UUID.randomUUID().toString(), false, true);
    }
    
    /**
     * 单次加写锁并执行任务: 获取到写锁后如果当前有读锁则等待读锁释放后再执行任务，如果没有获取到写锁则跳过（为了防止写数据后读取的历史数据不可用的情况）
     *
     * @param runnable 获取到写锁后执行的任务
     */
    public void tryExecuteWriteExcludeRead(EasyMsRunnable runnable) {
        getWriteLockAndExecuteRunnable(runnable, UUID.randomUUID().toString(), true, true);
    }
    
    /**
     * 阻塞式加写锁，即如果加锁失败，则循环重试，直到加锁成功或者超时(默认加锁失败后重试间隔为100毫秒): 获取到写锁后无论是否当前有读锁都执行任务，如果没有获取到写锁则跳过
     *
     * @param timeout  超时时间(单位：毫秒)
     * @param runnable 获取到写锁后执行的任务
     * @throws DistributedLockTimeoutException 分布式锁超时异常
     */
    public void blockExecuteWrite(EasyMsRunnable runnable, long timeout) throws DistributedLockTimeoutException {
        blockWriteLock(timeout, DEFAULT_RETRY_INTERVAL, runnable, false);
    }
    
    /**
     * 阻塞式加写锁，即如果加锁失败，则循环重试，直到加锁成功或者超时: 获取到写锁后无论是否当前有读锁都执行任务，如果没有获取到写锁则跳过
     *
     * @param timeout       超时时间(单位：毫秒)
     * @param retryInterval 加锁失败后重试间隔(单位：毫秒)
     * @param runnable      获取到写锁后执行的任务
     * @throws DistributedLockTimeoutException 分布式锁超时异常
     */
    public void blockExecuteWrite(EasyMsRunnable runnable, long timeout, long retryInterval)
            throws DistributedLockTimeoutException {
        blockWriteLock(timeout, retryInterval, runnable, false);
    }
    
    /**
     * 阻塞式加写锁，即如果加锁失败，则循环重试，直到加锁成功或者超时(默认加锁失败后重试间隔为100毫秒): 获取到写锁后如果当前有读锁则等待读锁释放后再执行任务，如果没有获取到写锁则跳过（为了防止写数据后读取的历史数据不可用的情况）
     *
     * @param timeout  超时时间(单位：毫秒)
     * @param runnable 获取到写锁后执行的任务
     * @throws DistributedLockTimeoutException 分布式锁超时异常
     */
    public void blockExecuteWriteExcludeRead(EasyMsRunnable runnable, long timeout)
            throws DistributedLockTimeoutException {
        blockWriteLock(timeout, DEFAULT_RETRY_INTERVAL, runnable, true);
    }
    
    /**
     * 阻塞式加写锁，即如果加锁失败，则循环重试，直到加锁成功或者超时(默认加锁失败后重试间隔为100毫秒): 获取到写锁后如果当前有读锁则等待读锁释放后再执行任务，如果没有获取到写锁则跳过（为了防止写数据后读取的历史数据不可用的情况）
     *
     * @param timeout  超时时间(单位：毫秒)
     * @param runnable 获取到写锁后执行的任务
     * @throws DistributedLockTimeoutException 分布式锁超时异常
     */
    public void blockExecuteWriteExcludeRead(EasyMsRunnable runnable, long timeout, long retryInterval)
            throws DistributedLockTimeoutException {
        blockWriteLock(timeout, retryInterval, runnable, true);
    }
    
    @SuppressWarnings("unchecked")
    private boolean getReadLock(String uuid) {
        List<String> argList = new ArrayList<>();
        argList.add(uuid);
        argList.add(String.valueOf(expireSecond));
        List<String> keyList = new ArrayList<>();
        keyList.add(redisReadKey);
        keyList.add(redisWriteKey);
        String result = getEasyMsRedisTemplate().execute(READ_LOCK_REDIS_SCRIPT, keyList, argList);
        if ("S".equals(result)) {
            if (log.isDebugEnabled()) {
                log.debug("Redis distributed read lock ({}) lock success!", redisReadKey);
            }
            return true;
        }
        return false;
    }
    
    private void unlockReadLock(String uuid) {
        getEasyMsRedisTemplate().listRemove(redisReadKey, 1, uuid);
    }
    
    private void blockWriteLock(long timeout, long retryInterval, EasyMsRunnable runnable, boolean isExcludeRead)
            throws DistributedLockTimeoutException {
        if (timeout < 0 || retryInterval < 0) {
            throw new IllegalArgumentException("Argument blockMilliseconds or retryIntervalMilliseconds is invalid!");
        }
        String uuid = UUID.randomUUID().toString();
        long limitTime = System.currentTimeMillis() + timeout;
        do {
            if (getWriteLockAndExecuteRunnable(runnable, uuid, isExcludeRead, false)) {
                return;
            }
            ThreadUtil.sleep(retryInterval);
        } while (System.currentTimeMillis() < limitTime);
        throw new DistributedLockTimeoutException("Get redis distributed write lock {} time out!", redisWriteKey);
    }
    
    @SuppressWarnings("unchecked")
    private boolean getWriteLockAndExecuteRunnable(EasyMsRunnable runnable, String uuid, boolean isExcludeRead,
            boolean isRecordFailLog) {
        EasyMsRedisTemplate localEasyMsRedisTemplate = getEasyMsRedisTemplate();
        List<String> argList = new ArrayList<>();
        argList.add(uuid);
        argList.add(String.valueOf(expireSecond));
        List<String> keyList = new ArrayList<>();
        keyList.add(redisWriteKey);
        String result = localEasyMsRedisTemplate.execute(WRITE_LOCK_REDIS_SCRIPT, keyList, argList);
        if ("S".equals(result)) {
            if (log.isDebugEnabled()) {
                log.debug("Redis distributed write lock ({}) lock success, expire time {}s!", redisWriteKey,
                        expireSecond);
            }
            // 如果配置了排读，则获取到写锁后，需要判断读锁是否存在，如果存在则需要等待读锁释放才可执行任务(为了防止写数据后读取的历史数据不可用的情况)
            if (isExcludeRead) {
                while (localEasyMsRedisTemplate.hasKey(redisReadKey)) {
                    ThreadUtil.sleep(DEFAULT_RETRY_INTERVAL, TimeUnit.MILLISECONDS);
                }
            }
            try {
                runnable.run();
            } catch (Throwable t) {
                throw new DistributedWriteLockException(t);
            } finally {
                unlockWriteLock(uuid);
            }
            return true;
        }
        if (isRecordFailLog && log.isDebugEnabled()) {
            log.debug("Redis distributed write lock ({}) lock failure!", redisWriteKey);
        }
        return false;
    }
    
    @SuppressWarnings("unchecked")
    private void unlockWriteLock(String uuid) {
        getEasyMsRedisTemplate().execute(WRITE_LOCK_UNLOCK_REDIS_SCRIPT, Collections.singletonList(redisWriteKey),
                Collections.singletonList(uuid));
    }
    
    public static void setEasyMsRedisTemplate(EasyMsRedisTemplate easyMsRedisTemplate) {
        if (DistributedReadWriteLock.easyMsRedisTemplate == null) {
            DistributedReadWriteLock.easyMsRedisTemplate = easyMsRedisTemplate;
        }
    }
    
    public static EasyMsRedisTemplate getEasyMsRedisTemplate() {
        if (easyMsRedisTemplate == null) {
            throw new RedisRuntimeException("Redis is disabled, cannot use redis distributed read write locks");
        }
        return easyMsRedisTemplate;
    }
}
