package com.stars.easyms.redis.annotion;

import java.lang.annotation.*;

/**
 * 提供redis set服务的方法注解
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RedisStringSet {

    /**
     * redis缓存组名称
     */
    String group() default "";

    /**
     * redis缓存失效时间，默认没有失效时间
     */
    String expire() default "-1ms";

    /**
     * 若redis缓存未失效，是否强制重置失效时间
     */
    boolean resetExpire() default true;

    /**
     * key值是否带前缀
     */
    boolean usePrefix() default true;

    /**
     * 自定义前缀，支持el表达式
     */
    String prefix() default "";

    /**
     * 只有不存在的时候才存入，默认不开启
     */
    boolean onlyAbsent() default false;

}
