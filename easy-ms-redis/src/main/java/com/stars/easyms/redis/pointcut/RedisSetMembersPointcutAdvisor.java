package com.stars.easyms.redis.pointcut;

import com.stars.easyms.redis.annotion.RedisSetMembers;
import com.stars.easyms.redis.initializer.RedisManagerInitializer;
import com.stars.easyms.redis.interceptor.RedisSetMembersInterceptor;
import com.stars.easyms.redis.template.EasyMsRedisTemplate;
import org.aopalliance.aop.Advice;
import org.springframework.aop.Pointcut;
import org.springframework.aop.support.AbstractPointcutAdvisor;
import org.springframework.aop.support.annotation.AnnotationMatchingPointcut;
import org.springframework.util.ObjectUtils;

/**
 * <p>className: RedisSetMembersPointcutAdvisor</p>
 * <p>description: RedisSetMembers的顾问类</p>
 *
 * @author jinzhilong
 * @version 1.0.0
 * @date 2019-12-25 10:41
 */
public final class RedisSetMembersPointcutAdvisor extends AbstractPointcutAdvisor {

    private transient RedisSetMembersInterceptor advice;

    private transient Pointcut pointcut;

    public RedisSetMembersPointcutAdvisor(RedisManagerInitializer redisManagerInitializer, EasyMsRedisTemplate easyMsRedisTemplate) {
        this.advice = new RedisSetMembersInterceptor();
        this.advice.setRedisManagerInitializer(redisManagerInitializer);
        this.advice.setEasyMsRedisTemplate(easyMsRedisTemplate);
        this.advice.setClassAnnotationType(RedisSetMembers.class);
        this.pointcut = new AnnotationMatchingPointcut(null, RedisSetMembers.class, false);
    }

    @Override
    public Pointcut getPointcut() {
        return this.pointcut;
    }

    @Override
    public Advice getAdvice() {
        return this.advice;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || this.getClass() != other.getClass()) {
            return false;
        }
        RedisSetMembersPointcutAdvisor otherAdvisor = (RedisSetMembersPointcutAdvisor) other;
        return (ObjectUtils.nullSafeEquals(this.advice, otherAdvisor.advice) &&
                ObjectUtils.nullSafeEquals(this.pointcut, otherAdvisor.pointcut));
    }

    @Override
    public int hashCode() {
        return 41 + this.advice.hashCode() * 41 + this.pointcut.hashCode() * 41;
    }
}