package com.stars.easyms.redis.connection.jedis;

import com.stars.easyms.redis.connection.EasyMsRedisCommands;
import com.stars.easyms.redis.core.EasyMsScanCursor;
import lombok.AllArgsConstructor;
import org.springframework.data.redis.connection.jedis.JedisConnection;
import org.springframework.data.redis.connection.jedis.JedisConverters;
import org.springframework.data.redis.core.ScanIteration;
import org.springframework.data.redis.core.ScanOptions;
import org.springframework.util.Assert;
import redis.clients.jedis.ScanParams;
import redis.clients.jedis.ScanResult;

import java.util.Map;

/**
 * <p>className: EasyMsJedisHashCommands</p>
 * <p>description: EasyMs自定义jedis的hash类型命令类</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/7 3:59 下午
 */
@AllArgsConstructor
public class EasyMsJedisCommands implements EasyMsRedisCommands {

    private JedisConnection connection;

    @Override
    public EasyMsScanCursor<Map.Entry<byte[], byte[]>> hScan(byte[] key, long cursorId, ScanOptions options) {

        Assert.notNull(key, "Key must not be null!");

        return new EasyMsScanCursor<Map.Entry<byte[], byte[]>>(key, cursorId, options) {

            @Override
            protected ScanIteration<Map.Entry<byte[], byte[]>> doScan(byte[] key, long cursorId, ScanOptions options) {

                ScanParams params = JedisConverters.toScanParams(options);

                ScanResult<Map.Entry<byte[], byte[]>> result = connection.getJedis()
                        .hscan(key, JedisConverters.toBytes(cursorId), params);
                return new ScanIteration<>(Long.parseLong(result.getCursor()), result.getResult());
            }

            @Override
            protected void doClose() {
                EasyMsJedisCommands.this.connection.close();
            }

        }.open();
    }

}
