package com.stars.easyms.redis.annotion;

import java.lang.annotation.*;

/**
 * 提供redis scan服务的方法注解
 *
 * @author guoguifang
 * @date 2021-05-10 13:54
 * @since 1.8.0
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RedisHashScan {

    /**
     * redis缓存组名称
     */
    String group() default "";

    /**
     * key值是否带前缀
     */
    boolean usePrefix() default true;

    /**
     * 自定义前缀，支持el表达式
     */
    String prefix() default "";

}
