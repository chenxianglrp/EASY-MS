package com.stars.easyms.redis.interceptor;

import com.stars.easyms.redis.annotion.RedisStringSet;
import com.stars.easyms.redis.exception.RedisInterceptorException;
import org.aopalliance.intercept.MethodInvocation;

import java.util.concurrent.TimeUnit;

/**
 * <p>className: RedisStringSetInterceptor</p>
 * <p>description: RedisStringSet注解拦截器</p>
 *
 * @author guoguifang
 * @version 1.2.1
 * @date 2019-03-18 13:55
 */
public class RedisStringSetInterceptor extends AbstractRedisInterceptor<RedisStringSet> {

    @Override
    protected Object invoke(MethodInvocation methodInvocation, RedisStringSet redisStringSet) throws RedisInterceptorException {
        Object result = proceed(methodInvocation);
        if (isEnabled() && result != null) {
            String redisKey = getRedisKey(methodInvocation, redisStringSet.group());
            Long expire = null;
            if (redisStringSet.resetExpire()) {
                expire = getExpire(methodInvocation, redisStringSet.expire());
            }
            if (expire == null || expire <= 0) {
                expire = easyMsRedisTemplate.getExpire(redisKey, TimeUnit.MILLISECONDS);
            }
            if (expire > 0) {
                if (redisStringSet.onlyAbsent()) {
                    easyMsRedisTemplate.setIfAbsent(redisKey, result, expire, TimeUnit.MILLISECONDS);
                } else {
                    easyMsRedisTemplate.set(redisKey, result, expire, TimeUnit.MILLISECONDS);
                }
            } else {
                if (redisStringSet.onlyAbsent()) {
                    easyMsRedisTemplate.setIfAbsent(redisKey, result);
                } else {
                    easyMsRedisTemplate.set(redisKey, result);
                }
            }
        }
        return result;
    }

}