package com.stars.easyms.redis.interceptor;

import com.stars.easyms.redis.annotion.GetRedisKey;
import org.aopalliance.intercept.MethodInvocation;

/**
 * <p>className: GetRedisKeyInterceptor</p>
 * <p>description: GetRedisKey注解拦截器</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/7 11:31 上午
 */
public class GetRedisKeyInterceptor extends AbstractRedisInterceptor<GetRedisKey> {

    @Override
    protected Object invoke(MethodInvocation methodInvocation, GetRedisKey getRedisKey) {
        if (isEnabled()) {
            return getRedisKey(methodInvocation, getRedisKey.group());
        }
        return null;
    }
}