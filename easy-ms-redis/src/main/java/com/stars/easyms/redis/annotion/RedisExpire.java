package com.stars.easyms.redis.annotion;

import java.lang.annotation.*;

/**
 * 提供redis expire服务的方法注解
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
@Target({ElementType.METHOD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RedisExpire {

    /**
     * redis缓存组名称
     */
    String group() default "";

    /**
     * redis缓存失效时间，默认没有缓存时间
     */
    String expire() default "-1ms";

    /**
     * 若redis缓存未失效，是否强制重置失效时间
     */
    boolean resetExpire() default true;

    /**
     * key值是否带前缀
     */
    boolean usePrefix() default true;

    /**
     * 自定义前缀，支持el表达式
     */
    String prefix() default "";

}
