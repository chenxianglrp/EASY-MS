package com.stars.easyms.redis.exception;

import com.stars.easyms.base.util.MessageFormatUtil;

/**
 * redis异常
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
public class RedisException extends Exception {

    private static final long serialVersionUID = -1034292190345464637L;

    public RedisException(Throwable throwable) {
        super(throwable);
    }

    public RedisException(String message, Object... args) {
        super(MessageFormatUtil.format(message, args));
    }
}
