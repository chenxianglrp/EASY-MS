package com.stars.easyms.redis.connection.lettuce;

import com.stars.easyms.base.util.ReflectUtil;
import com.stars.easyms.redis.connection.EasyMsRedisCommands;
import com.stars.easyms.redis.core.EasyMsScanCursor;
import io.lettuce.core.MapScanCursor;
import io.lettuce.core.ScanArgs;
import io.lettuce.core.ScanCursor;
import io.lettuce.core.cluster.api.sync.RedisClusterCommands;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.lettuce.LettuceConnection;
import org.springframework.data.redis.core.ScanIteration;
import org.springframework.data.redis.core.ScanOptions;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;

import java.lang.reflect.Method;
import java.util.Map;

/**
 * <p>className: EasyMsJedisHashCommands</p>
 * <p>description: EasyMs自定义jedis的hash类型命令类</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/7 3:59 下午
 */
@Slf4j
@AllArgsConstructor
public class EasyMsLettuceCommands implements EasyMsRedisCommands {

    private LettuceConnection connection;

    private static Method getConnectMethod;

    static {
        getConnectMethod = ReflectUtil.getMatchingMethod(LettuceConnection.class, "getConnection");
        getConnectMethod.setAccessible(true);
    }

    @Override
    public EasyMsScanCursor<Map.Entry<byte[], byte[]>> hScan(byte[] key, long cursorId, ScanOptions options) {

        Assert.notNull(key, "Key must not be null!");

        return new EasyMsScanCursor<Map.Entry<byte[], byte[]>>(key, cursorId, options) {

            @SneakyThrows
            @Override
            @SuppressWarnings("unchecked")
            protected ScanIteration<Map.Entry<byte[], byte[]>> doScan(byte[] key, long cursorId, ScanOptions options) {

                ScanCursor scanCursor = ScanCursor.of(Long.toString(cursorId));
                ScanArgs scanArgs = toScanArgs(options);

                MapScanCursor<byte[], byte[]> mapScanCursor =
                        ((RedisClusterCommands<byte[], byte[]>) getConnectMethod.invoke(connection))
                                .hscan(key, scanCursor, scanArgs);
                String nextCursorId = mapScanCursor.getCursor();

                Map<byte[], byte[]> values = mapScanCursor.getMap();
                return new ScanIteration<>(Long.parseLong(nextCursorId), values.entrySet());
            }

            @Override
            protected void doClose() {
                EasyMsLettuceCommands.this.connection.close();
            }

        }.open();
    }

    @Nullable
    private static ScanArgs toScanArgs(@Nullable ScanOptions options) {

        if (options == null) {
            return null;
        }

        ScanArgs scanArgs = new ScanArgs();

        if (options.getPattern() != null) {
            scanArgs.match(options.getPattern());
        }

        if (options.getCount() != null) {
            scanArgs.limit(options.getCount());
        }

        return scanArgs;
    }

}
