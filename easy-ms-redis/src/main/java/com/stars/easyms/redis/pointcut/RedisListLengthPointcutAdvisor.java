package com.stars.easyms.redis.pointcut;

import com.stars.easyms.redis.annotion.RedisListLength;
import com.stars.easyms.redis.initializer.RedisManagerInitializer;
import com.stars.easyms.redis.interceptor.RedisListLengthInterceptor;
import com.stars.easyms.redis.template.EasyMsRedisTemplate;
import org.aopalliance.aop.Advice;
import org.springframework.aop.Pointcut;
import org.springframework.aop.support.AbstractPointcutAdvisor;
import org.springframework.aop.support.annotation.AnnotationMatchingPointcut;
import org.springframework.util.ObjectUtils;

/**
 * <p>className: RedisListLengthPointcutAdvisor</p>
 * <p>description: redis的list类型的长度注解顾问类</p>
 *
 * @author guoguifang
 * @version 1.7.2
 * @date 2021/1/19 5:14 下午
 */
public final class RedisListLengthPointcutAdvisor extends AbstractPointcutAdvisor {

    private transient RedisListLengthInterceptor advice;

    private transient Pointcut pointcut;

    public RedisListLengthPointcutAdvisor(RedisManagerInitializer redisManagerInitializer, EasyMsRedisTemplate easyMsRedisTemplate) {
        this.advice = new RedisListLengthInterceptor();
        this.advice.setRedisManagerInitializer(redisManagerInitializer);
        this.advice.setEasyMsRedisTemplate(easyMsRedisTemplate);
        this.advice.setClassAnnotationType(RedisListLength.class);
        this.pointcut = new AnnotationMatchingPointcut(null, RedisListLength.class, false);
    }

    @Override
    public Pointcut getPointcut() {
        return this.pointcut;
    }

    @Override
    public Advice getAdvice() {
        return this.advice;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || this.getClass() != other.getClass()) {
            return false;
        }
        RedisListLengthPointcutAdvisor otherAdvisor = (RedisListLengthPointcutAdvisor) other;
        return (ObjectUtils.nullSafeEquals(this.advice, otherAdvisor.advice) &&
                ObjectUtils.nullSafeEquals(this.pointcut, otherAdvisor.pointcut));
    }

    @Override
    public int hashCode() {
        return 41 + this.advice.hashCode() * 41 + this.pointcut.hashCode() * 41;
    }
}