package com.stars.easyms.redis.initializer;

import com.stars.easyms.redis.bean.RedisMethodInfo;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.lang.reflect.Method;
import java.util.*;

/**
 * <p>className: RedisInfoHolder</p>
 * <p>description: Redis信息存放类</p>
 *
 * @author guoguifang
 * @version 1.2.1
 * @date 2019-03-20 15:43
 */
public final class RedisInfoHolder {

    private static final Map<String, Map<String, Set<RedisMethodInfo>>> REDIS_PREFIX_GROUP_INFO_MAPPING = new HashMap<>(64);

    private static final Map<Method, RedisMethodInfo> REDIS_METHOD_INFO_MAPPING = new HashMap<>(64);

    private static final List<RedisMethodInfo> REDIS_SCRIPT_INFO_HOLDER = new ArrayList<>();

    static void putRedisMethod(String prefix, String group, Method redisMethod, RedisMethodInfo redisMethodInfo) {
        REDIS_PREFIX_GROUP_INFO_MAPPING.computeIfAbsent(prefix, key -> new TreeMap<>()).computeIfAbsent(group, key -> new HashSet<>()).add(redisMethodInfo);
        REDIS_METHOD_INFO_MAPPING.put(redisMethod, redisMethodInfo);
    }

    static void putScriptRedisMethod(Method redisMethod, RedisMethodInfo redisMethodInfo) {
        REDIS_SCRIPT_INFO_HOLDER.add(redisMethodInfo);
        REDIS_METHOD_INFO_MAPPING.put(redisMethod, redisMethodInfo);
    }

    @NonNull
    public static List<RedisMethodInfo> getAllRedisMethodInfo() {
        return new ArrayList<>(REDIS_METHOD_INFO_MAPPING.values());
    }

    @Nullable
    public static RedisMethodInfo getRedisMethodInfo(Method method) {
        return REDIS_METHOD_INFO_MAPPING.get(method);
    }

    @NonNull
    public static Map<String, Map<String, Set<RedisMethodInfo>>> getRedisMethodMappingForGroup() {
        return Collections.unmodifiableMap(REDIS_PREFIX_GROUP_INFO_MAPPING);
    }

    @NonNull
    public static List<RedisMethodInfo> getRedisScriptMethodInfo() {
        return Collections.unmodifiableList(REDIS_SCRIPT_INFO_HOLDER);
    }

    private RedisInfoHolder() {}
}