package com.stars.easyms.redis.properties;

import com.stars.easyms.base.constant.EasyMsCommonConstants;
import com.stars.easyms.base.util.SpringBootUtil;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * EasyMs的Redis属性类型
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
@Data
@ConfigurationProperties(prefix = EasyMsRedisProperties.PREFIX)
public class EasyMsRedisProperties {

    public static final String PREFIX = EasyMsCommonConstants.EASY_MS_PROPERTIES_PREFIX + "redis";

    /**
     * redis的key的前缀：默认使用 "项目名称:环境名称" 为前缀
     */
    private String prefix = SpringBootUtil.getApplicationName() + EasyMsCommonConstants.REDIS_KEY_SEPARATOR + SpringBootUtil.getActiveProfile();

}
