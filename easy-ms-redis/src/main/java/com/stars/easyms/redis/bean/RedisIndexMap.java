package com.stars.easyms.redis.bean;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * <p>className: RedisEvalMap</p>
 * <p>description: RedisEvalMap</p>
 *
 * @author guoguifang
 * @version 1.7.2
 * @date 2021/1/23 12:43 下午
 */
@Getter
@AllArgsConstructor
public class RedisIndexMap {

    private Long indexStart;

    private Long indexEnd;

}
