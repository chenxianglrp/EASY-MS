package com.stars.easyms.redis.annotion;

import com.stars.easyms.redis.enums.RedisListPushType;

import java.lang.annotation.*;

/**
 * <p>annotationName: RedisListPush</p>
 * <p>description: redis的list类型存值</p>
 *
 * @author jinzhilong
 * @date 2019-12-18 14:45
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RedisListPush {

    /**
     * redis缓存组名称
     */
    String group() default "";

    /**
     * redis缓存失效时间，默认没有失效时间
     */
    String expire() default "-1ms";

    /**
     * 若redis缓存未失效，是否强制重置失效时间
     */
    boolean resetExpire() default true;

    /**
     * push时是否重写：即删除旧数据
     * 如果是全量push则建议为true,如果是增量push则建议为false
     */
    boolean override() default false;

    /**
     * redis list L-LPUSH R-RPUSH， 默认从右侧插入
     */
    RedisListPushType type() default RedisListPushType.RIGHT;

    /**
     * key值是否带前缀
     */
    boolean usePrefix() default true;

    /**
     * 自定义前缀，支持el表达式
     */
    String prefix() default "";

}
