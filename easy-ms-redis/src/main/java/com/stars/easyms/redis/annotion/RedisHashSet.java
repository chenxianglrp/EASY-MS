package com.stars.easyms.redis.annotion;

import java.lang.annotation.*;

/**
 * 提供redis set服务的方法注解
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RedisHashSet {

    /**
     * redis缓存组名称
     */
    String group() default "";

    /**
     * redis缓存失效时间，默认没有失效时间
     */
    String expire() default "-1ms";

    /**
     * 若redis缓存未失效，是否强制重置失效时间
     */
    boolean resetExpire() default true;

    /**
     * set时是否删除旧数据,只在hash的set方法的无RedisHashKey时生效(为了防止在无RedisField时set整个key的时候无法删除一些不再需要的key,造成垃圾数据)
     * 如果是全量set则建议为true,如果是增量set则建议为false
     */
    boolean override() default false;

    /**
     * key值是否带前缀
     */
    boolean usePrefix() default true;

    /**
     * 自定义前缀，支持el表达式
     */
    String prefix() default "";

}
