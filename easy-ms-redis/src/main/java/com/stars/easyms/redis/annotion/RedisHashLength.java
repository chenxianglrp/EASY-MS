package com.stars.easyms.redis.annotion;

import java.lang.annotation.*;

/**
 * <p>className: RedisHashLength</p>
 * <p>description: redis的hash类型取长度</p>
 *
 * @author guoguifang
 * @version 1.7.2
 * @date 2021/1/19 5:10 下午
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RedisHashLength {

    /**
     * redis缓存组名称
     */
    String group() default "";

    /**
     * key值是否带前缀
     */
    boolean usePrefix() default true;

    /**
     * 自定义前缀，支持el表达式
     */
    String prefix() default "";

}
