package com.stars.easyms.redis.annotion;

import java.lang.annotation.*;

/**
 * <p>annotationName: RedisSetMembers</p>
 * <p>description: Redis set members 服务的方法注解</p>
 *
 * @author jinzhilong
 * @date 2019-12-18 14:08
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RedisSetMembers {

    /**
     * redis缓存组名称
     */
    String group() default "";

    /**
     * redis缓存失效时间，默认没有失效时间
     */
    String expire() default "-1ms";

    /**
     * key值是否带前缀
     */
    boolean usePrefix() default true;

    /**
     * 自定义前缀，支持el表达式
     */
    String prefix() default "";

}
