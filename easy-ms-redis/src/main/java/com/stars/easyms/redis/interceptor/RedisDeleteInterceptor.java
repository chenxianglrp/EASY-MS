package com.stars.easyms.redis.interceptor;

import com.stars.easyms.redis.annotion.RedisDelete;
import com.stars.easyms.redis.exception.RedisInterceptorException;
import org.aopalliance.intercept.MethodInvocation;

import java.util.Set;

/**
 * <p>className: RedisDeleteInterceptor</p>
 * <p>description: RedisDelete注解拦截器</p>
 *
 * @author guoguifang
 * @version 1.2.1
 * @date 2019-03-18 14:25
 */
public class RedisDeleteInterceptor extends AbstractRedisInterceptor<RedisDelete> {

    @Override
    protected Object invoke(MethodInvocation methodInvocation, RedisDelete redisDelete) throws RedisInterceptorException {
        if (isEnabled()) {
            String redisKey = getRedisKey(methodInvocation, redisDelete.group());
            Set<String> hashKeySet = getRedisHashKey(methodInvocation);
            // 如果hashKeySet为null，表示方法参数上未设置RedisHashKey注解，则全部删除，否则按照设置的hashKey删除
            if (hashKeySet == null) {
                easyMsRedisTemplate.delete(redisKey);
            } else if (!hashKeySet.isEmpty()) {
                easyMsRedisTemplate.hashDelete(redisKey, hashKeySet);
            }
        }
        return proceed(methodInvocation);
    }

}