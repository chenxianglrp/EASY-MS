package com.stars.easyms.redis.interceptor;

import com.stars.easyms.redis.annotion.RedisHashScan;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.data.redis.core.ScanOptions;

import java.lang.reflect.Method;
import java.lang.reflect.Type;

/**
 * <p>className: RedisHashScanInterceptor</p>
 * <p>description: RedisHashScan注解拦截器</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/7 11:31 上午
 */
public class RedisHashScanInterceptor extends AbstractRedisInterceptor<RedisHashScan> {

    @Override
    protected Object invoke(MethodInvocation methodInvocation, RedisHashScan redisHashScan) {
        if (isEnabled()) {
            Method pointMethod = methodInvocation.getMethod();
            Type genericReturnType = pointMethod.getGenericReturnType();
            String redisKey = getRedisKey(methodInvocation, redisHashScan.group());
            ScanOptions scanOptions = ScanOptions.scanOptions().count(1000).match("*").build();
            // 获取cursorId，如果小于0则表示查询全部，否则按照cursorId查询
            long cursorId = getRedisScanCursorId(methodInvocation);
            if (cursorId < 0) {
                return easyMsRedisTemplate.hashScan(redisKey, scanOptions, genericReturnType);
            }
            return easyMsRedisTemplate.hashScan(redisKey, cursorId, scanOptions, genericReturnType);
        }
        return null;
    }
}