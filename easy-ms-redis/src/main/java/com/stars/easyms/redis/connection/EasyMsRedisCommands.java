package com.stars.easyms.redis.connection;

import com.stars.easyms.redis.core.EasyMsScanCursor;
import org.springframework.data.redis.core.ScanOptions;

import java.util.Map;

/**
 * <p>className: EasyMsRedisHashCommands</p>
 * <p>description: EasyMs自定义redis的hash类型命令接口</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/7 4:00 下午
 */
public interface EasyMsRedisCommands {

    EasyMsScanCursor<Map.Entry<byte[], byte[]>> hScan(byte[] key, long cursorId, ScanOptions options);

}
