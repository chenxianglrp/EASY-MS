package com.stars.easyms.redis.annotion;

import com.stars.easyms.redis.enums.RedisListPushType;

import java.lang.annotation.*;

/**
 * <p>annotationName: RedisListPop</p>
 * <p>description: redis的list类型取值：取完即删除</p>
 *
 * @author guoguifang
 * @date 2019-12-18 14:47
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RedisListPop {
    /**
     * redis缓存组名称
     */
    String group() default "";

    /**
     * redis list L-LPUSH R-RPUSH ，默认从左侧取出
     */
    RedisListPushType type() default RedisListPushType.LEFT;

    /**
     * key值是否带前缀
     */
    boolean usePrefix() default true;

    /**
     * 自定义前缀，支持el表达式
     */
    String prefix() default "";

}
