package com.stars.easyms.redis.scan;

/**
 * <p>className: EasyMsRedisManagerScan</p>
 * <p>description: EasyMsRedis扫描包注册类</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/4/12 8:35 下午
 */
public interface EasyMsRedisManagerScan {

    String[] scanPackages();

}
