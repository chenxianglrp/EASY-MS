package com.stars.easyms.redis.pointcut;

import com.stars.easyms.redis.annotion.RedisHashScan;
import com.stars.easyms.redis.initializer.RedisManagerInitializer;
import com.stars.easyms.redis.interceptor.RedisHashScanInterceptor;
import com.stars.easyms.redis.template.EasyMsRedisTemplate;
import org.aopalliance.aop.Advice;
import org.springframework.aop.Pointcut;
import org.springframework.aop.support.AbstractPointcutAdvisor;
import org.springframework.aop.support.annotation.AnnotationMatchingPointcut;
import org.springframework.util.ObjectUtils;

/**
 * <p>className: RedisHashScanPointcutAdvisor</p>
 * <p>description: RedisHashScan的顾问类</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/7 11:32 上午
 */
public final class RedisHashScanPointcutAdvisor extends AbstractPointcutAdvisor {

    private transient RedisHashScanInterceptor advice;

    private transient Pointcut pointcut;

    public RedisHashScanPointcutAdvisor(RedisManagerInitializer redisManagerInitializer, EasyMsRedisTemplate easyMsRedisTemplate) {
        this.advice = new RedisHashScanInterceptor();
        this.advice.setRedisManagerInitializer(redisManagerInitializer);
        this.advice.setEasyMsRedisTemplate(easyMsRedisTemplate);
        this.advice.setClassAnnotationType(RedisHashScan.class);
        this.pointcut = new AnnotationMatchingPointcut(null, RedisHashScan.class, false);
    }

    @Override
    public Pointcut getPointcut() {
        return this.pointcut;
    }

    @Override
    public Advice getAdvice() {
        return this.advice;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || this.getClass() != other.getClass()) {
            return false;
        }
        RedisHashScanPointcutAdvisor otherAdvisor = (RedisHashScanPointcutAdvisor) other;
        return (ObjectUtils.nullSafeEquals(this.advice, otherAdvisor.advice) &&
                ObjectUtils.nullSafeEquals(this.pointcut, otherAdvisor.pointcut));
    }

    @Override
    public int hashCode() {
        return 41 + this.advice.hashCode() * 41 + this.pointcut.hashCode() * 41;
    }
}