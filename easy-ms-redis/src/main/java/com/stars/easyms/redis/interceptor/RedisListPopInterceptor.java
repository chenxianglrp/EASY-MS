package com.stars.easyms.redis.interceptor;

import com.stars.easyms.redis.annotion.RedisListPop;
import com.stars.easyms.redis.enums.RedisListPushType;
import com.stars.easyms.redis.exception.RedisInterceptorException;
import org.aopalliance.intercept.MethodInvocation;

import java.lang.reflect.Method;

/**
 * <p>className: RedisListPopInterceptor</p>
 * <p>description: RedisListPop注解拦截器</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/11/11 4:20 下午
 */
public class RedisListPopInterceptor extends AbstractRedisInterceptor<RedisListPop> {

    @Override
    Object invoke(MethodInvocation methodInvocation, RedisListPop redisListPop) throws RedisInterceptorException {
        Object result = null;
        if (isEnabled()) {
            Method pointMethod = methodInvocation.getMethod();
            Class<?> returnType = pointMethod.getReturnType();
            String redisKey = getRedisKey(methodInvocation, redisListPop.group());
            RedisListPushType type = redisListPop.type();
            if (RedisListPushType.LEFT == type) {
                result = easyMsRedisTemplate.listLeftPop(redisKey, returnType);
            } else {
                result = easyMsRedisTemplate.listRightPop(redisKey, returnType);
            }
        }
        if (result == null) {
            result = proceed(methodInvocation);
        }
        return result;
    }

}