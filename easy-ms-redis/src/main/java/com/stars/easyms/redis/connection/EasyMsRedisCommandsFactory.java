package com.stars.easyms.redis.connection;

import com.stars.easyms.redis.connection.jedis.EasyMsJedisCommands;
import com.stars.easyms.redis.connection.lettuce.EasyMsLettuceCommands;
import org.springframework.data.redis.connection.DefaultStringRedisConnection;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.jedis.JedisConnection;
import org.springframework.data.redis.connection.lettuce.LettuceConnection;
import org.springframework.lang.NonNull;

/**
 * <p>className: EasyMsRedisCommandsFactory</p>
 * <p>description: EasyMs自定义redis命令工厂类</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/7 4:37 下午
 */
public class EasyMsRedisCommandsFactory {

    @NonNull
    public static EasyMsRedisCommands getEasyMsRedisCommands(RedisConnection connection) {
        if (connection instanceof DefaultStringRedisConnection) {
            connection = ((DefaultStringRedisConnection) connection).getDelegate();
        }
        if (connection instanceof JedisConnection) {
            return new EasyMsJedisCommands((JedisConnection) connection);
        }
        return new EasyMsLettuceCommands((LettuceConnection) connection);
    }
}
