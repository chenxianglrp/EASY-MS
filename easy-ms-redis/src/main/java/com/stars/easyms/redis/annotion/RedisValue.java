package com.stars.easyms.redis.annotion;

import java.lang.annotation.*;

/**
 * <p>className: RedisValue</p>
 * <p>description: redis值，增加在参数上，如果不设置该注解则默认使用方法返回值作为值</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/14 4:54 下午
 */
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RedisValue {
}
