package com.stars.easyms.redis.core;

import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.data.redis.core.*;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * <p>className: EasyMsScanCursor</p>
 * <p>description: EasyMs自定义ScanCursor类</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/10 11:26 上午
 */
public abstract class EasyMsScanCursor<T> implements Cursor<T> {

    private byte[] key;
    private CursorState state;
    private long currCursorId;
    private long nextCursorId;
    private Iterator<T> delegate;
    private final ScanOptions scanOptions;
    private long position;

    public EasyMsScanCursor(byte[] key) {
        this(key, ScanOptions.NONE);
    }

    public EasyMsScanCursor(byte[] key, ScanOptions options) {
        this(key, 0, options);
    }

    public EasyMsScanCursor(byte[] key, long currCursorId) {
        this(key, currCursorId, ScanOptions.NONE);
    }

    public EasyMsScanCursor(byte[] key, long currCursorId, ScanOptions options) {
        this.key = key;
        this.scanOptions = options != null ? options : ScanOptions.NONE;
        this.currCursorId = currCursorId;
        this.nextCursorId = -1;
        this.state = CursorState.READY;
        this.delegate = Collections.emptyIterator();
    }

    protected abstract ScanIteration<T> doScan(byte[] key, long cursorId, ScanOptions options);

    @Override
    public final EasyMsScanCursor<T> open() {

        if (!isReady()) {
            throw new InvalidDataAccessApiUsageException("Cursor already " + this.state + ". Cannot (re)open it.");
        }

        this.state = CursorState.OPEN;
        ScanIteration<T> result = doScan(this.key, this.currCursorId, this.scanOptions);
        if (result == null) {
            this.nextCursorId = 0;
            this.delegate = Collections.emptyIterator();
            return this;
        }

        this.nextCursorId = result.getCursorId();
        if (!CollectionUtils.isEmpty(result.getItems())) {
            this.delegate = result.iterator();
        } else {
            this.delegate = Collections.emptyIterator();
        }
        return this;
    }

    public long getCurrCursorId() {
        return this.currCursorId;
    }

    @Override
    public long getCursorId() {
        if (this.nextCursorId < 0) {
            throw new InvalidDataAccessApiUsageException("Cannot get cursorId. Did you forget to call open()?");
        }
        return this.nextCursorId;
    }

    @Override
    public boolean hasNext() {
        assertCursorIsOpen();
        return this.delegate.hasNext();
    }

    private void assertCursorIsOpen() {
        if (isReady() || isClosed()) {
            throw new InvalidDataAccessApiUsageException("Cannot access closed cursor. Did you forget to call open()?");
        }
    }

    @Override
    public T next() {
        assertCursorIsOpen();
        if (!hasNext()) {
            throw new NoSuchElementException("No more elements available for cursor " + this.currCursorId + ".");
        }
        T next = this.delegate.next();
        this.position++;
        return next;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Remove is not supported");
    }

    protected void doClose() {}

    @Override
    public final void close() {
        try {
            doClose();
        } finally {
            this.state = CursorState.CLOSED;
        }
    }

    @Override
    public boolean isClosed() {
        return this.state == CursorState.CLOSED;
    }

    public final boolean isOpen() {
        return this.state == CursorState.OPEN;
    }

    @Override
    public long getPosition() {
        return position;
    }

    public byte[] getKey() {
        return key;
    }

    private boolean isReady() {
        return this.state == CursorState.READY;
    }

    enum CursorState {
        READY, OPEN, CLOSED;
    }
}
