package com.stars.easyms.redis.exception;

import com.stars.easyms.base.util.MessageFormatUtil;

/**
 * 分布式锁超时异常
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
public class DistributedLockTimeoutException extends Exception {

    private static final long serialVersionUID = -2034292390445464637L;

    public DistributedLockTimeoutException(Throwable throwable) {
        super(throwable);
    }

    public DistributedLockTimeoutException(String message, Object... args) {
        super(MessageFormatUtil.format(message, args));
    }
}
