package com.stars.easyms.redis.exception;

import com.stars.easyms.base.util.ExceptionUtil;
import com.stars.easyms.base.util.MessageFormatUtil;

/**
 * redis异常
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
public class RedisRuntimeException extends RuntimeException {

    private static final long serialVersionUID = -2034292190345464637L;

    public RedisRuntimeException(Throwable throwable) {
        super(throwable);
    }

    public RedisRuntimeException(String message, Object... args) {
        super(MessageFormatUtil.format(message, ExceptionUtil.trimmedCopy(args)), ExceptionUtil.extractThrowable(args));
    }
}
