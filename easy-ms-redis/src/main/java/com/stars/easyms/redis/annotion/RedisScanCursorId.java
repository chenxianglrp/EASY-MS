package com.stars.easyms.redis.annotion;

import java.lang.annotation.*;

/**
 * <p>className: RedisScanCursorId</p>
 * <p>description: redis的scan功能的cursor起始位置</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/8 10:05 上午
 */
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RedisScanCursorId {
}
