package com.stars.easyms.redis.interceptor;

import com.stars.easyms.redis.annotion.RedisTtl;
import com.stars.easyms.redis.exception.RedisInterceptorException;
import org.aopalliance.intercept.MethodInvocation;

import java.util.concurrent.TimeUnit;

/**
 * <p>className: RedisTtlInterceptor</p>
 * <p>description: RedisTtl注解拦截器</p>
 *
 * @author guoguifang
 * @version 1.2.1
 * @date 2019-03-18 14:39
 */
public class RedisTtlInterceptor extends AbstractRedisInterceptor<RedisTtl> {

    @Override
    protected Object invoke(MethodInvocation methodInvocation, RedisTtl redisTtl) throws RedisInterceptorException {
        return isEnabled() ?
                easyMsRedisTemplate.getExpire(getRedisKey(methodInvocation, redisTtl.group()), TimeUnit.MILLISECONDS) : proceed(methodInvocation);
    }

}