package com.stars.easyms.redis.monitor;

import com.stars.easyms.monitor.MonitorRequestHandler;
import com.stars.easyms.redis.constant.EasyMsRedisConstants;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * <p>className: EasyMsRedisMonitorRequestHandler</p>
 * <p>description: redis页面跳转控制类</p>
 *
 * @author guoguifang
 * @date 2019/3/20 19:31
 * @version 1.2.1
 */
public final class EasyMsRedisMonitorRequestHandler implements MonitorRequestHandler {

    @Override
    public Map<String, Object> getMonitorInfo() {
        Map<String, Object> returnMap = new LinkedHashMap<>();
        returnMap.put("enabled", true);
        return returnMap;
    }
    
    @Override
    public Map<String, Object> getBriefMonitorInfo() {
        return getMonitorInfo();
    }
    
    @Override
    public String getModuleName() {
        return EasyMsRedisConstants.MODULE_NAME;
    }

    @Override
    public int getOrder() {
        return 10;
    }
}
