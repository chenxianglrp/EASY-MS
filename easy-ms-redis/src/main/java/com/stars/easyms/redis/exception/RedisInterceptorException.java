package com.stars.easyms.redis.exception;

import com.stars.easyms.base.util.ExceptionUtil;
import com.stars.easyms.base.util.MessageFormatUtil;

/**
 * <p>className: RedisInterceptorException</p>
 * <p>description: 执行redis拦截器时抛出的异常</p>
 *
 * @author guoguifang
 * @date 2019/5/29 9:48
 * @version 1.2.1
 */
public class RedisInterceptorException extends Exception {

    private static final long serialVersionUID = -1234894193345495631L;

    public RedisInterceptorException(String message, Object... args) {
        super(MessageFormatUtil.format(message, ExceptionUtil.trimmedCopy(args)), ExceptionUtil.extractThrowable(args));
    }
}
