package com.stars.easyms.redis.autoconfigure;

import com.stars.easyms.redis.initializer.RedisManagerInitializer;
import com.stars.easyms.redis.lock.DistributedLock;
import com.stars.easyms.redis.lock.DistributedReadWriteLock;
import com.stars.easyms.redis.pointcut.*;
import com.stars.easyms.redis.properties.EasyMsRedisProperties;
import com.stars.easyms.redis.template.EasyMsRedisTemplate;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * spring boot Redis缓存配置类
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
@Configuration
@EnableConfigurationProperties(EasyMsRedisProperties.class)
public class EasyMsRedisAutoConfiguration {

    private EasyMsRedisProperties easyMsRedisProperties;

    private StringRedisTemplate stringRedisTemplate;

    private EasyMsRedisTemplate easyMsRedisTemplate;

    public EasyMsRedisAutoConfiguration(EasyMsRedisProperties easyMsRedisProperties,
                                        RedisConnectionFactory redisConnectionFactory) {
        this.easyMsRedisProperties = easyMsRedisProperties;
        this.stringRedisTemplate = new StringRedisTemplate(redisConnectionFactory);
        this.easyMsRedisTemplate = new EasyMsRedisTemplate(easyMsRedisProperties, this.stringRedisTemplate);
        DistributedLock.setEasyMsRedisTemplate(easyMsRedisTemplate);
        DistributedReadWriteLock.setEasyMsRedisTemplate(easyMsRedisTemplate);
    }

    @Bean
    @ConditionalOnMissingBean(StringRedisTemplate.class)
    public StringRedisTemplate easyMsStringRedisTemplate() {
        return this.stringRedisTemplate;
    }

    @Bean
    public EasyMsRedisTemplate easyMsRedisTemplate() {
        return this.easyMsRedisTemplate;
    }

    @Bean
    public RedisManagerInitializer easyMsRedisServiceInitializer() {
        return new RedisManagerInitializer(easyMsRedisProperties);
    }

    @Bean
    public GetRedisKeyPointcutAdvisor getRedisKeyPointcutAdvisor(RedisManagerInitializer easyMsRedisManagerInitializer) {
        return new GetRedisKeyPointcutAdvisor(easyMsRedisManagerInitializer, easyMsRedisTemplate);
    }

    @Bean
    public RedisStringGetPointcutAdvisor redisStringGetPointcutAdvisor(RedisManagerInitializer easyMsRedisManagerInitializer) {
        return new RedisStringGetPointcutAdvisor(easyMsRedisManagerInitializer, easyMsRedisTemplate);
    }

    @Bean
    public RedisStringSetPointcutAdvisor redisStringSetPointcutAdvisor(RedisManagerInitializer easyMsRedisManagerInitializer) {
        return new RedisStringSetPointcutAdvisor(easyMsRedisManagerInitializer, easyMsRedisTemplate);
    }

    @Bean
    public RedisHashSetPointcutAdvisor redisHashSetPointcutAdvisor(RedisManagerInitializer easyMsRedisManagerInitializer) {
        return new RedisHashSetPointcutAdvisor(easyMsRedisManagerInitializer, easyMsRedisTemplate);
    }

    @Bean
    public RedisHashGetPointcutAdvisor redisHashGetPointcutAdvisor(RedisManagerInitializer easyMsRedisManagerInitializer) {
        return new RedisHashGetPointcutAdvisor(easyMsRedisManagerInitializer, easyMsRedisTemplate);
    }

    @Bean
    public RedisHashLengthPointcutAdvisor redisHashLengthPointcutAdvisor(RedisManagerInitializer easyMsRedisManagerInitializer) {
        return new RedisHashLengthPointcutAdvisor(easyMsRedisManagerInitializer, easyMsRedisTemplate);
    }

    @Bean
    public RedisHashScanPointcutAdvisor redisHashScanPointcutAdvisor(RedisManagerInitializer easyMsRedisManagerInitializer) {
        return new RedisHashScanPointcutAdvisor(easyMsRedisManagerInitializer, easyMsRedisTemplate);
    }

    @Bean
    public RedisDeletePointcutAdvisor redisDeletePointcutAdvisor(RedisManagerInitializer easyMsRedisManagerInitializer) {
        return new RedisDeletePointcutAdvisor(easyMsRedisManagerInitializer, easyMsRedisTemplate);
    }

    @Bean
    public RedisExistsPointcutAdvisor redisExistsPointcutAdvisor(RedisManagerInitializer easyMsRedisManagerInitializer) {
        return new RedisExistsPointcutAdvisor(easyMsRedisManagerInitializer, easyMsRedisTemplate);
    }

    @Bean
    public RedisExpirePointcutAdvisor redisExpirePointcutAdvisor(RedisManagerInitializer easyMsRedisManagerInitializer) {
        return new RedisExpirePointcutAdvisor(easyMsRedisManagerInitializer, easyMsRedisTemplate);
    }

    @Bean
    public RedisTtlPointcutAdvisor redisTtlPointcutAdvisor(RedisManagerInitializer easyMsRedisManagerInitializer) {
        return new RedisTtlPointcutAdvisor(easyMsRedisManagerInitializer, easyMsRedisTemplate);
    }

    @Bean
    public RedisEvalPointcutAdvisor redisEvalPointcutAdvisor(RedisManagerInitializer easyMsRedisManagerInitializer) {
        return new RedisEvalPointcutAdvisor(easyMsRedisManagerInitializer, easyMsRedisTemplate);
    }

    @Bean
    public RedisListPushPointcutAdvisor redisListPushPointcutAdvisor(RedisManagerInitializer easyMsRedisManagerInitializer) {
        return new RedisListPushPointcutAdvisor(easyMsRedisManagerInitializer, easyMsRedisTemplate);
    }

    @Bean
    public RedisListRangePointcutAdvisor redisListRangePointcutAdvisor(RedisManagerInitializer easyMsRedisManagerInitializer) {
        return new RedisListRangePointcutAdvisor(easyMsRedisManagerInitializer, easyMsRedisTemplate);
    }

    @Bean
    public RedisListPopPointcutAdvisor redisListPopPointcutAdvisor(RedisManagerInitializer easyMsRedisManagerInitializer) {
        return new RedisListPopPointcutAdvisor(easyMsRedisManagerInitializer, easyMsRedisTemplate);
    }

    @Bean
    public RedisListLengthPointcutAdvisor redisListLengthPointcutAdvisor(RedisManagerInitializer easyMsRedisManagerInitializer) {
        return new RedisListLengthPointcutAdvisor(easyMsRedisManagerInitializer, easyMsRedisTemplate);
    }

    @Bean
    public RedisSetAddPointcutAdvisor redisSetAddPointcutAdvisor(RedisManagerInitializer easyMsRedisManagerInitializer) {
        return new RedisSetAddPointcutAdvisor(easyMsRedisManagerInitializer, easyMsRedisTemplate);
    }

    @Bean
    public RedisSetMembersPointcutAdvisor redisSetMembersPointcutAdvisor(RedisManagerInitializer easyMsRedisManagerInitializer) {
        return new RedisSetMembersPointcutAdvisor(easyMsRedisManagerInitializer, easyMsRedisTemplate);
    }
}
