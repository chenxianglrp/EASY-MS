package com.stars.easyms.redis.pointcut;

import com.stars.easyms.redis.annotion.RedisExists;
import com.stars.easyms.redis.initializer.RedisManagerInitializer;
import com.stars.easyms.redis.interceptor.RedisExistsInterceptor;
import com.stars.easyms.redis.template.EasyMsRedisTemplate;
import org.aopalliance.aop.Advice;
import org.springframework.aop.Pointcut;
import org.springframework.aop.support.AbstractPointcutAdvisor;
import org.springframework.aop.support.annotation.AnnotationMatchingPointcut;
import org.springframework.util.ObjectUtils;

/**
 * <p>className: RedisExistsPointcutAdvisor</p>
 * <p>description: RedisExists的顾问类</p>
 *
 * @author guoguifang
 * @version 1.2.1
 * @date 2019-03-18 15:11
 */
public final class RedisExistsPointcutAdvisor extends AbstractPointcutAdvisor {

    private transient RedisExistsInterceptor advice;

    private transient Pointcut pointcut;

    public RedisExistsPointcutAdvisor(RedisManagerInitializer redisManagerInitializer, EasyMsRedisTemplate easyMsRedisTemplate) {
        this.advice = new RedisExistsInterceptor();
        this.advice.setRedisManagerInitializer(redisManagerInitializer);
        this.advice.setEasyMsRedisTemplate(easyMsRedisTemplate);
        this.advice.setClassAnnotationType(RedisExists.class);
        this.pointcut = new AnnotationMatchingPointcut(null, RedisExists.class, false);
    }

    @Override
    public Pointcut getPointcut() {
        return this.pointcut;
    }

    @Override
    public Advice getAdvice() {
        return this.advice;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || this.getClass() != other.getClass()) {
            return false;
        }
        RedisExistsPointcutAdvisor otherAdvisor = (RedisExistsPointcutAdvisor) other;
        return (ObjectUtils.nullSafeEquals(this.advice, otherAdvisor.advice) &&
                ObjectUtils.nullSafeEquals(this.pointcut, otherAdvisor.pointcut));
    }

    @Override
    public int hashCode() {
        return 41 + this.advice.hashCode() * 41 + this.pointcut.hashCode() * 41;
    }
}