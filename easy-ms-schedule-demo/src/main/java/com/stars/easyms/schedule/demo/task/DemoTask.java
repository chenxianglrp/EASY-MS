package com.stars.easyms.schedule.demo.task;

import com.stars.easyms.schedule.DistributedTask;
import com.stars.easyms.schedule.DistributedTaskContext;
import com.stars.easyms.schedule.ScheduleTask;
import com.stars.easyms.schedule.exception.DistributedTaskExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>className: DemoTask</p>
 * <p>description: demo</p>
 *
 * @author guoguifang
 * @date 2020/11/2 7:47 下午
 */
@ScheduleTask
public class DemoTask implements DistributedTask {

    private static final Logger logger = LoggerFactory.getLogger(DemoTask.class);

    @Override
    public void execute(DistributedTaskContext distributedTaskContext) throws DistributedTaskExecutionException {
        logger.info("{} -- demoTask 开始执行: ,partition number{},partition count: {}", Thread.currentThread().getName(),
                distributedTaskContext.getPartitionCount(), distributedTaskContext.getPartitionIndex());
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            logger.debug(e.getMessage(), e);
            Thread.currentThread().interrupt();
        }
        logger.info("{} -- demoTask 开始结束", Thread.currentThread().getName());
    }
}
