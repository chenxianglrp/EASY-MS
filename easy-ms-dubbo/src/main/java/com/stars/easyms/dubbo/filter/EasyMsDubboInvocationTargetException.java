package com.stars.easyms.dubbo.filter;

import com.stars.easyms.base.exception.BusinessException;

import java.lang.reflect.InvocationTargetException;

/**
 * EasyMs的dubbo模块传输异常.
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/8/10 1:43 下午
 */
class EasyMsDubboInvocationTargetException extends InvocationTargetException {
    
    private final BusinessException businessException;
    
    EasyMsDubboInvocationTargetException(BusinessException businessException) {
        super(businessException);
        this.businessException = businessException;
    }
    
    BusinessException getBusinessException() {
        return businessException;
    }
}
