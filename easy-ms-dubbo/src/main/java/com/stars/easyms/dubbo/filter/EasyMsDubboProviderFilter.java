package com.stars.easyms.dubbo.filter;

import com.alibaba.fastjson.JSON;
import com.stars.easyms.base.alarm.EasyMsAlarmAssistor;
import com.stars.easyms.base.bean.EasyMsRequestEntity;
import com.stars.easyms.base.constant.HttpHeaderConstants;
import com.stars.easyms.base.exception.AlarmException;
import com.stars.easyms.base.exception.BusinessException;
import com.stars.easyms.base.trace.EasyMsTraceSynchronizationManager;
import com.stars.easyms.base.util.DateTimeUtil;
import com.stars.easyms.base.util.ExceptionUtil;
import com.stars.easyms.base.util.JsonUtil;
import com.stars.easyms.base.util.MessageFormatUtil;
import com.stars.easyms.base.util.SpringBootUtil;
import com.stars.easyms.base.util.TraceUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.common.extension.Activate;
import org.apache.dubbo.rpc.Invocation;
import org.apache.dubbo.rpc.Invoker;
import org.apache.dubbo.rpc.Result;
import org.springframework.util.StringUtils;

import static com.stars.easyms.base.constant.EasyMsCommonConstants.ASYNC_ID_CONNECTION_SYMBOL;
import static org.apache.dubbo.common.constants.CommonConstants.PROVIDER;

/**
 * <p>className: EasyMsDubboProviderFilter</p>
 * <p>description: dubbo的提供者端过滤器</p>
 * <p>order为1,由于skywalking的增强类为{@link org.apache.dubbo.monitor.support.MonitorFilter},因此必须在该类的后边执行</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/11/13 2:06 下午
 */
@Slf4j
@Activate(group = PROVIDER, order = 1)
public class EasyMsDubboProviderFilter extends BaseEasyMsDubboFilter {
    
    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) {
        // 获取接口类型
        String serviceName = invocation.getServiceName();
        if (isDubboInnerService(serviceName)) {
            return invoker.invoke(invocation);
        }
        
        // 获取接收请求时间
        long receiveRequestTime = System.currentTimeMillis();
        String receiveRequestTimeStr = DateTimeUtil.getDatetimeNormalStrWithMills(receiveRequestTime);
        String responseSys = SpringBootUtil.getApplicationName();
        String requestPath = null;
        String requestSys = null;
        String asyncId = null;
        String requestTime = null;
        String requestId = null;
        EasyMsDubboRequestInfo easyMsDubboRequestInfo = getEasyMsDubboRequestInfo(invocation);
        if (easyMsDubboRequestInfo != null) {
            requestPath = easyMsDubboRequestInfo.getRequestPath();
            requestSys = easyMsDubboRequestInfo.getRequestSys();
            requestId = easyMsDubboRequestInfo.getRequestId();
            asyncId = easyMsDubboRequestInfo.getAsyncId();
            requestTime = easyMsDubboRequestInfo.getRequestTime();
        } else {
            easyMsDubboRequestInfo = new EasyMsDubboRequestInfo();
        }
        if (StringUtils.isEmpty(requestId)) {
            requestId = TraceUtil.getTraceId();
            easyMsDubboRequestInfo.setRequestId(requestId);
        }
        easyMsDubboRequestInfo.setReceiveRequestTime(receiveRequestTime);
        // 由于部分场景dubbo转换异常，因此统一使用String类型
        String easyMsDubboRequestInfoStr = JSON.toJSONString(easyMsDubboRequestInfo);
        invocation.setObjectAttachment(HttpHeaderConstants.DUBBO_REQUEST_INFO, easyMsDubboRequestInfoStr);
        invocation.put(HttpHeaderConstants.DUBBO_REQUEST_INFO, easyMsDubboRequestInfoStr);
        
        // 获取traceId
        String traceId = (String) invocation.getObjectAttachment(HttpHeaderConstants.TRACE_KEY);
        if (StringUtils.isEmpty(traceId)) {
            traceId = TraceUtil.getTraceId();
            invocation.setObjectAttachment(HttpHeaderConstants.TRACE_KEY, traceId);
        }
        invocation.put(HttpHeaderConstants.TRACE_KEY, traceId);
        
        // 将traceId和requestId放入日志线程本地变量中
        EasyMsTraceSynchronizationManager.setTraceInfo(traceId, requestId);
        if (asyncId != null) {
            asyncId = asyncId + ASYNC_ID_CONNECTION_SYMBOL + SpringBootUtil.getApplicationName();
            EasyMsTraceSynchronizationManager.setAsyncId(asyncId);
        }
        
        // 记录调用信息
        log.info("【接收服务-请求】-【接口类型: {}】-【请求系统: {}】-【服务系统: {}】-【链路Id: {}】-【请求ID: {}】{}-【发起请求时间: {}】-【接收请求时间: {}】{}.",
                TraceUtil.withUnknown(requestPath), TraceUtil.withUnknown(requestSys), responseSys, traceId, requestId,
                TraceUtil.getAsyncIdTrace(asyncId), TraceUtil.withUnknown(requestTime), receiveRequestTimeStr,
                easyMsDubboProperties.isLogRequest() ? "-【请求数据: " + JsonUtil.toJSONString(invocation.getArguments())
                        + "】" : "");
        
        // 封装请求参数
        EasyMsRequestEntity requestEntity = new EasyMsRequestEntity();
        requestEntity.setRequestPath(requestPath);
        requestEntity.setTraceId(traceId);
        requestEntity.setRequestSys(requestSys);
        requestEntity.setRequestId(requestId);
        requestEntity.setAsyncId(asyncId);
        requestEntity.setRequestTime(requestTime);
        requestEntity.setReceiveRequestTime(receiveRequestTime);
        requestEntity.setReceiveRequestTimeStr(receiveRequestTimeStr);
        
        // 将requestEntity放入本地变量中
        EasyMsTraceSynchronizationManager.setRequestEntity(requestEntity);
        
        // 往下继续执行
        try {
            return invoker.invoke(invocation);
        } finally {
            // 清除trace信息
            clearTraceId();
        }
    }
    
    @Override
    public void onResponse(Result appResponse, Invoker<?> invoker, Invocation invocation) {
        if (!isDubboInnerService(invocation.getServiceName())) {
            Throwable throwable = appResponse.getException();
            try {
                handleResponse(invocation, appResponse.getValue(), throwable);
            } finally {
                // 清除trace信息
                clearTraceId();
            }
            if (throwable instanceof BusinessException) {
                appResponse.setException(new EasyMsDubboInvocationTargetException((BusinessException) throwable));
            }
        }
    }
    
    @Override
    public void onError(Throwable throwable, Invoker<?> invoker, Invocation invocation) {
        if (!isDubboInnerService(invocation.getServiceName())) {
            try {
                handleResponse(invocation, null, throwable);
            } finally {
                // 清除trace信息
                clearTraceId();
            }
        }
    }
    
    private void handleResponse(Invocation invocation, Object result, Throwable throwable) {
        String traceId = (String) invocation.get(HttpHeaderConstants.TRACE_KEY);
        EasyMsDubboRequestInfo easyMsDubboRequestInfo = getEasyMsDubboRequestInfo(invocation);
        if (traceId == null || easyMsDubboRequestInfo == null) {
            return;
        }
        String requestPath = easyMsDubboRequestInfo.getRequestPath();
        String requestId = easyMsDubboRequestInfo.getRequestId();
        String asyncId = easyMsDubboRequestInfo.getAsyncId();
        String requestSys = easyMsDubboRequestInfo.getRequestSys();
        
        // 由于是异步，需要重新将traceId和requestId放入日志线程本地变量中
        EasyMsTraceSynchronizationManager.setTraceInfo(traceId, requestId);
        if (asyncId != null) {
            EasyMsTraceSynchronizationManager.setAsyncId(asyncId);
        }
        
        // 如果是未申报的异常类型则获取实际异常
        throwable = ExceptionUtil.exactThrowable(throwable);
        boolean isBusinessException = throwable instanceof BusinessException;
        
        // 获取返回响应时间及接口耗时
        long responseTime = System.currentTimeMillis();
        String responseTimeStr = DateTimeUtil.getDatetimeNormalStrWithMills(responseTime);
        Long costTime = responseTime - easyMsDubboRequestInfo.getReceiveRequestTime();
        
        // 记录响应信息
        if (throwable != null) {
            if (isBusinessException) {
                BusinessException businessException = (BusinessException) throwable;
                // 如果不是由异常引起的业务异常，只需要使用info级别
                if (businessException.getCause() == null) {
                    log.info(
                            "【接收服务-响应-异常】-【接口类型: {}】-【请求系统: {}】-【服务系统: {}】-【链路Id: {}】-【请求ID: {}】{}-【响应时间: {}】-【用时: {}ms】-【异常信息: {}】.",
                            requestPath, requestSys, SpringBootUtil.getApplicationName(), traceId, requestId,
                            TraceUtil.getAsyncIdTrace(asyncId), responseTimeStr, costTime,
                            businessException.toString());
                } else {
                    log.error(
                            "【接收服务-响应-异常】-【接口类型: {}】-【请求系统: {}】-【服务系统: {}】-【链路Id: {}】-【请求ID: {}】{}-【响应时间: {}】-【用时: {}ms】-【异常信息: {}】.",
                            requestPath, requestSys, SpringBootUtil.getApplicationName(), traceId, requestId,
                            TraceUtil.getAsyncIdTrace(asyncId), responseTimeStr, costTime, businessException.toString(),
                            businessException.getCause());
                }
            } else {
                log.error(
                        "【接收服务-响应-异常】-【接口类型: {}】-【请求系统: {}】-【服务系统: {}】-【链路Id: {}】-【请求ID: {}】{}-【响应时间: {}】-【用时: {}ms】.",
                        requestPath, requestSys, SpringBootUtil.getApplicationName(), traceId, requestId,
                        TraceUtil.getAsyncIdTrace(asyncId), responseTimeStr, costTime, throwable);
            }
            
            // 告警前检查EasyMsRequestEntity
            checkEasyMsRequestEntity(requestPath, requestSys, traceId, requestId);
            
            // 判断是否需要告警，告警异常为AlarmException或者非BusinessException
            if (throwable instanceof AlarmException || !isBusinessException) {
                EasyMsAlarmAssistor.sendExceptionAlarmMessage(throwable);
            }
        } else {
            log.info("【接收服务-响应】-【接口类型: {}】-【请求系统: {}】-【服务系统: {}】-【链路Id: {}】-【请求ID: {}】{}-【响应时间: {}】-【用时: {}ms】{}.",
                    requestPath, requestSys, SpringBootUtil.getApplicationName(), traceId, requestId,
                    TraceUtil.getAsyncIdTrace(asyncId), responseTimeStr, costTime,
                    easyMsDubboProperties.isLogResponse() ? "-响应数据: " + JsonUtil.toJSONString(result) : "");
        }
        
        // 如果接口用时超过预警值则告警
        if (costTime >= easyMsDubboProperties.getAlarmLimitTime()) {
            String warnMessage = MessageFormatUtil.format("接口【{}】用时较长，共{}ms，请优化接口！", requestPath, costTime);
            log.warn(warnMessage);
            
            // 告警前检查EasyMsRequestEntity
            checkEasyMsRequestEntity(requestPath, requestSys, traceId, requestId);
            
            EasyMsAlarmAssistor.sendNormalAlarmMessage(warnMessage);
        }
    }
    
    private void checkEasyMsRequestEntity(String requestPath, String requestSys, String traceId, String requestId) {
        if (EasyMsTraceSynchronizationManager.getRequestEntity() == null) {
            // 封装请求参数
            EasyMsRequestEntity requestEntity = new EasyMsRequestEntity();
            requestEntity.setRequestPath(requestPath);
            requestEntity.setTraceId(traceId);
            requestEntity.setRequestSys(requestSys);
            requestEntity.setRequestId(requestId);
            EasyMsTraceSynchronizationManager.setRequestEntity(requestEntity);
        }
    }
    
    private void clearTraceId() {
        EasyMsTraceSynchronizationManager.clear();
    }
    
}
