package com.stars.easyms.dubbo.scan;

/**
 * <p>className: EasyMsDubboScan</p>
 * <p>description: EasyMsDubbo扫描包注册类</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/4/12 8:35 下午
 */
public interface EasyMsDubboScan {

    String[] scanPackages();

}
