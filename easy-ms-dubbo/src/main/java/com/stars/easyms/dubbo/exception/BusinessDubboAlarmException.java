package com.stars.easyms.dubbo.exception;

import com.stars.easyms.base.exception.AlarmException;

/**
 * dubbo业务相关异常：默认告警，若不想告警需使用BusinessDubboException.
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/7/22 10:20 上午
 */
public class BusinessDubboAlarmException extends BusinessDubboException implements AlarmException {

    public BusinessDubboAlarmException(String retMsg) {
        super(retMsg);
    }

    public BusinessDubboAlarmException(String retMsg, Exception e) {
        super(retMsg, e);
    }

    public BusinessDubboAlarmException(String retCode, String retMsg) {
        super(retCode, retMsg);
    }

    public BusinessDubboAlarmException(String retCode, String retMsg, Exception e) {
        super(retCode, retMsg, e);
    }
    
    public BusinessDubboAlarmException(String retCode, String retMsg, String errorDesc) {
        super(retCode, retMsg, errorDesc);
    }
}
