package com.stars.easyms.dubbo.properties;

import com.stars.easyms.base.constant.EasyMsCommonConstants;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * <p>className: EasyMsDubboProperties</p>
 * <p>description: EasyMs的dubbo模块属性类</p>
 *
 * @author guoguifang
 * @version 1.7.1
 * @date 2020/12/21 11:41 上午
 */
@Data
@ConfigurationProperties(prefix = EasyMsDubboProperties.PREFIX)
public class EasyMsDubboProperties {

    public static final String PREFIX = EasyMsCommonConstants.EASY_MS_PROPERTIES_PREFIX + "dubbo";

    /**
     * Whether or not to print the request, default true
     */
    private boolean logRequest = true;

    /**
     * Whether or not to print the response, default true
     */
    private boolean logResponse = true;

    /**
     * Interface execution time limit, beyond which an alarm is raised, in milliseconds, default 2000ms
     */
    private Long alarmLimitTime = 2000L;

}
