package com.stars.easyms.dubbo.autoconfigure;

import com.stars.easyms.dubbo.properties.EasyMsDubboProperties;
import com.stars.easyms.dubbo.scan.EasyMsDubboScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.context.properties.source.ConfigurationPropertySources;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.*;

import java.util.*;

import static com.alibaba.spring.util.PropertySourcesUtils.getSubProperties;
import static org.apache.dubbo.spring.boot.util.DubboUtils.*;

/**
 * <p>className: EasyMsDubboAutoConfiguration</p>
 * <p>description: EasyMs的dubbo模块自动配置类</p>
 *
 * @author guoguifang
 * @version 1.7.1
 * @date 2020/12/21 11:44 上午
 */
@Configuration
@SuppressWarnings("unchecked")
@EnableConfigurationProperties(EasyMsDubboProperties.class)
public class EasyMsDubboAutoConfiguration {

    @Primary
    @Bean(name = BASE_PACKAGES_BEAN_NAME)
    public Set<String> dubboBasePackages(ConfigurableEnvironment environment) {
        PropertyResolver propertyResolver = dubboScanBasePackagesPropertyResolver(environment);
        Set<String> dubboBasePackageSet = propertyResolver.getProperty(BASE_PACKAGES_PROPERTY_NAME, Set.class);
        if (dubboBasePackageSet == null) {
            dubboBasePackageSet = new HashSet<>();
        }
        ServiceLoader<EasyMsDubboScan> easyMsDubboScans = ServiceLoader.load(EasyMsDubboScan.class);
        for (EasyMsDubboScan easyMsDubboScan : easyMsDubboScans) {
            String[] scanPackages = easyMsDubboScan.scanPackages();
            if (scanPackages != null && scanPackages.length > 0) {
                dubboBasePackageSet.addAll(Arrays.asList(scanPackages));
            }
        }
        return dubboBasePackageSet;
    }

    private PropertyResolver dubboScanBasePackagesPropertyResolver(ConfigurableEnvironment environment) {
        ConfigurableEnvironment propertyResolver = new AbstractEnvironment() {
            @Override
            protected void customizePropertySources(MutablePropertySources propertySources) {
                Map<String, Object> dubboScanProperties = getSubProperties(environment.getPropertySources(), DUBBO_SCAN_PREFIX);
                propertySources.addLast(new MapPropertySource("dubboScanProperties", dubboScanProperties));
            }
        };
        ConfigurationPropertySources.attach(propertyResolver);
        return propertyResolver;
    }

}
