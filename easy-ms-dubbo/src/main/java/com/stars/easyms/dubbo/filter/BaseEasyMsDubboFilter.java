package com.stars.easyms.dubbo.filter;

import com.alibaba.fastjson.JSON;
import com.stars.easyms.base.constant.HttpHeaderConstants;
import com.stars.easyms.base.util.ApplicationContextHolder;
import com.stars.easyms.dubbo.properties.EasyMsDubboProperties;
import org.apache.dubbo.rpc.Filter;
import org.apache.dubbo.rpc.Invocation;

import java.util.Map;

/**
 * <p>className: BaseEasyMsDubboFilter</p>
 * <p>description: 基础dubbo过滤器</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/11/13 4:22 下午
 */
abstract class BaseEasyMsDubboFilter implements Filter, Filter.Listener {

    EasyMsDubboProperties easyMsDubboProperties;

    BaseEasyMsDubboFilter() {
        this.easyMsDubboProperties = ApplicationContextHolder.getApplicationContext().getBean(EasyMsDubboProperties.class);
    }

    private static final String[] DUBBO_INNER_SERVICE_NAMES = new String[]{
            "com.alibaba.cloud.dubbo.service.DubboMetadataService",
            "com.alibaba.dubbo.rpc.service.GenericService",
            "org.apache.dubbo.rpc.service.GenericService"
    };

    boolean isDubboInnerService(String serviceName) {
        for (String dubboInnerServiceName : DUBBO_INNER_SERVICE_NAMES) {
            if (dubboInnerServiceName.equals(serviceName)) {
                return true;
            }
        }
        return false;
    }

    EasyMsDubboRequestInfo getEasyMsDubboRequestInfo(Invocation invocation) {
        // 部分异常场景dubbo会将该值转换成Map类型
        Object easyMsDubboRequestInfoObj = invocation.get(HttpHeaderConstants.DUBBO_REQUEST_INFO);
        if (easyMsDubboRequestInfoObj == null) {
            easyMsDubboRequestInfoObj = invocation.getObjectAttachment(HttpHeaderConstants.DUBBO_REQUEST_INFO);
        }
        EasyMsDubboRequestInfo easyMsDubboRequestInfo = null;
        if (easyMsDubboRequestInfoObj != null) {
            if (easyMsDubboRequestInfoObj instanceof String) {
                easyMsDubboRequestInfo = JSON.parseObject((String) easyMsDubboRequestInfoObj, EasyMsDubboRequestInfo.class);
            } else if (easyMsDubboRequestInfoObj instanceof Map) {
                easyMsDubboRequestInfo = JSON.parseObject(JSON.toJSONString(easyMsDubboRequestInfoObj), EasyMsDubboRequestInfo.class);
            }
        }
        return easyMsDubboRequestInfo;
    }
}
