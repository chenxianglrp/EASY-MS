package com.stars.easyms.dubbo.exception;

import com.stars.easyms.base.exception.BusinessException;

/**
 * dubbo业务相关异常：默认不告警，若想告警需使用BusinessRestAlarmException.
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/7/22 10:20 上午
 */
public class BusinessDubboException extends BusinessException {

    public BusinessDubboException(String retMsg) {
        super(retMsg);
    }

    public BusinessDubboException(String retMsg, Exception e) {
        super(retMsg, e);
    }

    public BusinessDubboException(String retCode, String retMsg) {
        super(retCode, retMsg);
    }

    public BusinessDubboException(String retCode, String retMsg, Exception e) {
        super(retCode, retMsg, e);
    }
    
    public BusinessDubboException(String retCode, String retMsg, String errorDesc) {
        super(retCode, retMsg, errorDesc);
    }
}
