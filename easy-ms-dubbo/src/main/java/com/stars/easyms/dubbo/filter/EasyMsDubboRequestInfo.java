package com.stars.easyms.dubbo.filter;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>className: EasyMsDubboRequestInfo</p>
 * <p>description: EasyMs自定义dubbo请求信息bean</p>
 *
 * @author guoguifang
 * @version 1.7.3
 * @date 2021/3/31 3:56 下午
 */
@Data
class EasyMsDubboRequestInfo implements Serializable {

    private String requestPath;

    private String requestSys;

    private String requestTime;

    private String requestId;

    private String asyncId;

    private long receiveRequestTime;

}
