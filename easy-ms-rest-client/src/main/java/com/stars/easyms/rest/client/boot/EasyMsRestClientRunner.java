package com.stars.easyms.rest.client.boot;

import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.stars.easyms.base.util.FastJsonUtil;
import com.stars.easyms.rest.client.interceptor.EasyMsRestTemplateInterceptor;
import com.stars.easyms.rest.client.properties.EasyMsRestClientProperties;
import com.stars.easyms.rest.client.util.EasyMsFastJsonHttpMessageConverter;
import org.springframework.beans.BeansException;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>className: EasyMsRestClientRunner</p>
 * <p>description: EasyMsRestClientRunner</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/6/16 11:04 上午
 */
public class EasyMsRestClientRunner implements ApplicationRunner, ApplicationContextAware {
    
    private ApplicationContext applicationContext;
    
    private EasyMsRestClientProperties easyMsRestClientProperties;
    
    public EasyMsRestClientRunner(EasyMsRestClientProperties easyMsRestClientProperties) {
        this.easyMsRestClientProperties = easyMsRestClientProperties;
    }
    
    @Override
    public void run(ApplicationArguments args) {
        RestTemplate restTemplate = applicationContext.getBean(RestTemplate.class);
        List<HttpMessageConverter<?>> localMessageConverters = new ArrayList<>();
        boolean hasFastJson = false;
        for (HttpMessageConverter<?> messageConverter : restTemplate.getMessageConverters()) {
            if (messageConverter instanceof StringHttpMessageConverter) {
                StringHttpMessageConverter stringHttpMessageConverter = (StringHttpMessageConverter) messageConverter;
                stringHttpMessageConverter.setDefaultCharset(StandardCharsets.UTF_8);
                localMessageConverters.add(stringHttpMessageConverter);
            } else if (messageConverter instanceof FastJsonHttpMessageConverter) {
                localMessageConverters.add(0,
                        new EasyMsFastJsonHttpMessageConverter((FastJsonHttpMessageConverter) messageConverter,
                                easyMsRestClientProperties));
                hasFastJson = true;
            } else {
                localMessageConverters.add(messageConverter);
            }
        }
        if (!hasFastJson) {
            localMessageConverters.add(0,
                    new EasyMsFastJsonHttpMessageConverter(FastJsonUtil.getFastJsonHttpMessageConverter(),
                            easyMsRestClientProperties));
        }
        restTemplate.setMessageConverters(localMessageConverters);
        
        // 设置EasyMsRestTemplateInterceptor并使用setInterceptors保证可正常排序
        List<ClientHttpRequestInterceptor> interceptors = new ArrayList<>(restTemplate.getInterceptors());
        interceptors.add(new EasyMsRestTemplateInterceptor(easyMsRestClientProperties));
        restTemplate.setInterceptors(interceptors);
    }
    
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
