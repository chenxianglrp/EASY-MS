package com.stars.easyms.rest.client.properties;

import com.stars.easyms.base.constant.EasyMsCommonConstants;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * <p>className: EasyMsRestClientProperties</p>
 * <p>description: EasyMsRestClient属性类</p>
 *
 * @author guoguifang
 * @version 1.7.3
 * @date 2021/3/6 1:32 下午
 */
@Data
@ConfigurationProperties(prefix = EasyMsRestClientProperties.PREFIX)
public class EasyMsRestClientProperties {

    public static final String PREFIX = EasyMsCommonConstants.EASY_MS_PROPERTIES_PREFIX + "rest.client";

    /**
     * connect timeout, unit: milliseconds
     */
    private int connectTimeout = 5000;

    /**
     * read timeout, unit: milliseconds
     */
    private int readTimeout = 60000;

    /**
     * connection request timeout, unit: milliseconds
     */
    private int connectionRequestTimeout = 60000;

    /**
     * Whether to verify HTTPS, default false
     */
    private boolean httpsCheck;
    
    /**
     * Whether or not to print the request, default false
     */
    private boolean logRequest;
    
    /**
     * Whether or not to print the response, default false
     */
    private boolean logResponse;

    /**
     * http client pool configuration
     */
    private Pool pool = new Pool();

    @Data
    public static class Pool {

        /**
         * Maximum number of connection pools
         */
        private int maxTotal = 100;

        /**
         * Maximum number of connection pools per route
         */
        private int maxPerRoute = 20;

    }
}
