package com.stars.easyms.rest.client.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.stars.easyms.rest.client.properties.EasyMsRestClientProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

import java.io.IOException;
import java.util.List;

/**
 * EasyMs拦截fastJson转换器部分方法.
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/7/6 8:15 下午
 */
@Slf4j
public class EasyMsFastJsonHttpMessageConverter implements HttpMessageConverter {
    
    private FastJsonHttpMessageConverter delegate;
    
    private EasyMsRestClientProperties easyMsRestClientProperties;
    
    public EasyMsFastJsonHttpMessageConverter(FastJsonHttpMessageConverter delegate,
            EasyMsRestClientProperties easyMsRestClientProperties) {
        this.delegate = delegate;
        this.easyMsRestClientProperties = easyMsRestClientProperties;
    }
    
    @Override
    public boolean canRead(Class clazz, MediaType mediaType) {
        return delegate.canRead(clazz, mediaType);
    }
    
    @Override
    public boolean canWrite(Class clazz, MediaType mediaType) {
        return delegate.canWrite(clazz, mediaType);
    }
    
    @Override
    public List<MediaType> getSupportedMediaTypes() {
        return delegate.getSupportedMediaTypes();
    }
    
    @Override
    public Object read(Class clazz, HttpInputMessage inputMessage) throws IOException, HttpMessageNotReadableException {
        Object result = delegate.read(clazz, inputMessage);
        if (easyMsRestClientProperties.isLogResponse()) {
            log.info("Received response data: {}", JSON.toJSONString(result));
        }
        return result;
    }
    
    @Override
    public void write(Object o, MediaType contentType, HttpOutputMessage outputMessage)
            throws IOException, HttpMessageNotWritableException {
        delegate.write(o, contentType, outputMessage);
    }
}
