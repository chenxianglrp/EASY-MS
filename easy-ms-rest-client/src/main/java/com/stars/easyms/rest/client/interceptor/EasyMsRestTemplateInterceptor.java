package com.stars.easyms.rest.client.interceptor;

import com.stars.easyms.base.constant.HttpHeaderConstants;
import com.stars.easyms.base.trace.EasyMsTraceSynchronizationManager;
import com.stars.easyms.base.util.DateTimeUtil;
import com.stars.easyms.base.util.SpringBootUtil;
import com.stars.easyms.base.util.TraceUtil;
import com.stars.easyms.rest.client.properties.EasyMsRestClientProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.lang.NonNull;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * <p>className: EasyMsRestTemplateInterceptor</p>
 * <p>description: EasyMs自定义restTemplate拦截器</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/6/15 10:15 上午
 */
@Slf4j
public class EasyMsRestTemplateInterceptor implements ClientHttpRequestInterceptor {
    
    private EasyMsRestClientProperties easyMsRestClientProperties;
    
    public EasyMsRestTemplateInterceptor(EasyMsRestClientProperties easyMsRestClientProperties) {
        this.easyMsRestClientProperties = easyMsRestClientProperties;
    }
    
    @Override
    public ClientHttpResponse intercept(@NonNull HttpRequest request, @NonNull byte[] body,
            @NonNull ClientHttpRequestExecution execution) throws IOException {
        
        // 获取全局追踪ID，用于微服务日志全链路跟踪，如果为空则创建一个
        String traceId = TraceUtil.getTraceId(EasyMsTraceSynchronizationManager.getTraceId());
        String applicationName = SpringBootUtil.getApplicationName();
        String requestId = TraceUtil.getTraceId();
        String requestTime = DateTimeUtil.now();
        String asyncId = EasyMsTraceSynchronizationManager.getAsyncId();
        String requestPath = request.getURI().getPath();
        
        // 将参数放入请求头中
        HttpHeaders requestHeaders = request.getHeaders();
        requestHeaders.set(HttpHeaderConstants.TRACE_KEY, traceId);
        requestHeaders.set(HttpHeaderConstants.HEADER_KEY_REQUEST_PATH, requestPath);
        requestHeaders.set(HttpHeaderConstants.HEADER_KEY_REQUEST_SYS, applicationName);
        requestHeaders.set(HttpHeaderConstants.HEADER_KEY_REQUEST_ID, requestId);
        requestHeaders.set(HttpHeaderConstants.HEADER_KEY_REQUEST_TIME, requestTime);
        if (asyncId != null) {
            requestHeaders.set(HttpHeaderConstants.HEADER_KEY_ASYNC_ID, asyncId);
        }
        
        // 将用户信息放入请求头中
        String userInfo = EasyMsTraceSynchronizationManager.getCurrentUserInfo();
        if (userInfo != null) {
            requestHeaders.set(HttpHeaderConstants.HEADER_KEY_USER_INFO, userInfo);
        }
        
        log.info("【调用服务-请求】-【请求地址: {}】-【请求系统: {}】-【链路ID: {}】-【请求ID: {}】{}-【请求时间: {}】{}{}.", requestPath,
                applicationName, traceId, requestId, TraceUtil.getAsyncIdTrace(asyncId), requestTime,
                request.getMethod() != null ? "-【请求Method: " + request.getMethod().name() + "】" : "",
                easyMsRestClientProperties.isLogRequest() ? new String(body, StandardCharsets.UTF_8) : "");
        
        ClientHttpResponse response = null;
        Exception exception = null;
        try {
            response = execution.execute(request, body);
        } catch (Exception e) {
            exception = e;
            throw e;
        } finally {
            if (exception == null) {
                String responseSys = null;
                String responseTime = null;
                if (response != null) {
                    HttpHeaders responseHeaders = response.getHeaders();
                    responseSys = responseHeaders.getFirst(HttpHeaderConstants.HEADER_KEY_RESPONSE_SYS);
                    responseTime = responseHeaders.getFirst(HttpHeaderConstants.HEADER_KEY_RESPONSE_TIME);
                }
                log.info("【调用服务-响应】-【请求地址: {}】-【请求系统: {}】-【服务系统: {}】-【链路ID: {}】-【请求ID: {}】{}"
                                + "-【响应时间: {}】-【接收响应时间: {}】{}.", requestPath, applicationName,
                        TraceUtil.withUnknown(responseSys), traceId, requestId, TraceUtil.getAsyncIdTrace(asyncId),
                        TraceUtil.withUnknown(responseTime), DateTimeUtil.now(),
                        response != null ? "-【响应状态: " + response.getStatusCode() + "】" : "");
            } else {
                log.error("【调用服务-响应】-【请求地址: {}】-【请求系统: {}】-【链路ID: {}】-【请求ID: {}】{}-【接收响应时间: {}】.", requestPath,
                        applicationName, traceId, requestId, TraceUtil.getAsyncIdTrace(asyncId), DateTimeUtil.now(),
                        exception);
            }
        }
        return response;
    }
    
}
