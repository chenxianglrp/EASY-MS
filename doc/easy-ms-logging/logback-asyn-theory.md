# logback异步源码分析

　　logback异步输出日志的实现类是`ch.qos.logback.classic.AsyncAppender`，而该类的父类`ch.qos.logback.classic.AsyncAppenderBase`中的append方法为添加日志事件的起始方法：

```java
public class AsyncAppenderBase<E> extends UnsynchronizedAppenderBase<E> implements AppenderAttachable<E> {
    
    ...
    
    static final int UNDEFINED = -1;
    
    int discardingThreshold = UNDEFINED;
    
    Worker worker = new Worker();
    
    @Override
    public void start() {
        if (isStarted())
            return;
        if (appenderCount == 0) {
            addError("No attached appenders found.");
            return;
        }
        if (queueSize < 1) {
            addError("Invalid queue size [" + queueSize + "]");
            return;
        }
        
        // 采用数组阻塞队列
        blockingQueue = new ArrayBlockingQueue<E>(queueSize);

        // 如果该值设置为-1时默认会取设置队列大小的20%为阈值
        if (discardingThreshold == UNDEFINED)
            discardingThreshold = queueSize / 5;
        
        addInfo("Setting discardingThreshold to " + discardingThreshold);
        
        // 启动一个异步线程
        worker.setDaemon(true);
        worker.setName("AsyncAppender-Worker-" + getName());
        // make sure this instance is marked as "started" before staring the worker Thread
        super.start();
        worker.start();
    }
    
    /**
    * 添加日志事件的方法
    */
    @Override
    protected void append(E eventObject) {
        // 判断是否需要抛弃该条日志：
        //    首先判断队列剩余是否低于丢弃阈值，如果不低于则不抛弃
        //    如果低于丢弃阈值，则进入AsyncAppender类中的isDiscardable判断是否允许丢弃
        if (isQueueBelowDiscardingThreshold() && isDiscardable(eventObject)) {
            return;
        }
        
        ...
        
        // 若不丢弃则将日志事件放入队列中
        put(eventObject);
    }
    
    private boolean isQueueBelowDiscardingThreshold() {
        // 判断队列剩余是否低于丢弃阈值：
        //    如果设置为0时永远不会小于即永远不会丢弃，
        //    如果该值设置为-1时默认会取设置队列大小的20%为阈值
        return (blockingQueue.remainingCapacity() < discardingThreshold);
    }
    
    private void put(E eventObject) {
        // 如果nerverBlock为true时使用了队列的offer方法，而队列的offer方法当队列满时直接返回false即插入失败即表示直接丢弃
        // 如果nerverBlock为默认值false时将执行putUninterruptibly方法
        if (neverBlock) {
            blockingQueue.offer(eventObject);
        } else {
            putUninterruptibly(eventObject);
        }
    }
    
    /**
    * 在putUninterruptibly方法中使用了队列的put方法，ArrayBlockingQueue队列的put方法是线程阻塞的，即当队列满时阻塞线程直到队列有空间
    */
    private void putUninterruptibly(E eventObject) {
        boolean interrupted = false;
        try {
            while (true) {
                try {
                    blockingQueue.put(eventObject);
                    break;
                } catch (InterruptedException e) {
                    interrupted = true;
                }
            }
        } finally {
            if (interrupted) {
                Thread.currentThread().interrupt();
            }
        }
    }
    
    ...

}

public class AsyncAppender extends AsyncAppenderBase<ILoggingEvent> {

    ...

    /**
     * 级别为TRACE, DEBUG and INFO的事件被认为是可丢弃的。
     */
    protected boolean isDiscardable(ILoggingEvent event) {
        Level level = event.getLevel();
        return level.toInt() <= Level.INFO_INT;
    }

    ...
}
```

　　logback的异步实现就是有一个单独的Worker线程，此线程**会循环读取队列的日志然后打印**。写文件是通过Worker线程执行，**主线程将日志扔到阻塞队列中就去做其他事情，但是当队列满时会阻塞主线程相当于又变成了同步模式**。

```java
/**
* 异步处理工作线程
*/
class Worker extends Thread {

    public void run() {
        AsyncAppenderBase<E> parent = AsyncAppenderBase.this;
        AppenderAttachableImpl<E> aai = parent.aai;

        // loop while the parent is started
        while (parent.isStarted()) {
            try {
                E e = parent.blockingQueue.take();
                aai.appendLoopOnAppenders(e);
            } catch (InterruptedException ie) {
                break;
            }
        }

        addInfo("Worker thread will flush remaining events before exiting. ");

        for (E e : parent.blockingQueue) {
            aai.appendLoopOnAppenders(e);
            parent.blockingQueue.remove(e);
        }

        aai.detachAndStopAllAppenders();
    }
}  
```

　　结论：logback异步输出日志原理是启动一个异步线程处理，使用了一个数组阻塞队列，如果为**不允许阻塞模式当队列满时不管是什么级别都直接将日志丢弃**，如果**允许阻塞且允许丢弃时当队列达到80%时会丢弃INFO级别及以下的日志**，如果**允许阻塞且不允许丢弃时当队列满时将进入阻塞状态相当于是同步模式**，若不允许丢失日志时使用logback的异步在高并发场景下性能提升不明显，若不想丢失日志又想要获得高性能请使用easy-ms-logging框架。