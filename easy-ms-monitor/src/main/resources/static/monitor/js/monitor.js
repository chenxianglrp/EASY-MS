(function ($) {
    var url = getUrlRelativePath();
    $.ajax({
        url: 'easy-ms/getInfo',
        data: {url: url},
        type: 'POST',
        success: function (data) {
            $('#content').JSONView(formatterJson(typeof (data) != 'string' ? JSON.stringify(data) : data), {
                collapsed: isMonitor(url),
                nl2br: false,
                recursive_collapser: true,
                key_marks: true
            });
        }
    });
})(jQuery);

function formatterJson(text_value) {
    var res = "";
    for (var i = 0, j = 0, k = 0, ii, ele; i < text_value.length; i++) {//k:缩进，j:""个数
        ele = text_value.charAt(i);
        if (j % 2 === 0 && ele === "}") {
            k--;
            for (ii = 0; ii < k; ii++) ele = "    " + ele;
            ele = "\n" + ele;
        } else if (j % 2 === 0 && ele === "{") {
            ele += "\n";
            k++;
            for (ii = 0; ii < k; ii++) ele += "    ";
        } else if (j % 2 === 0 && ele === ",") {
            ele += "\n";
            for (ii = 0; ii < k; ii++) ele += "    ";
        } else if (ele === "\"") j++;
        res += ele;
    }
    return res;
}

function getUrlRelativePath() {
    var url = document.location.toString();
    var arrUrl = url.split("//");

    var start = arrUrl[1].indexOf("/");
    var relUrl = arrUrl[1].substring(start);//stop省略，截取从start开始到结尾的所有字符

    if (relUrl.indexOf("?") !== -1) {
        relUrl = relUrl.split("?")[0];
    }
    return relUrl;
}

function isMonitor(url) {
    var monitorUrl = "/monitor";
    if (url.length < monitorUrl.length) {
        return false;
    }
    return url.substring(url.length - monitorUrl.length) === monitorUrl;
}