package com.stars.easyms.monitor.config;

import com.stars.easyms.base.util.JsonUtil;
import com.stars.easyms.monitor.constant.EasyMsMonitorConstants;
import com.stars.easyms.monitor.register.EasyMsMonitorRegister;
import com.stars.easyms.monitor.register.EasyMsServiceRegistrationEventPublishingAspect;
import com.stars.easyms.monitor.register.event.EasyMsMonitorServicePreRegisteredEvent;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.aop.support.AopUtils;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.client.serviceregistry.Registration;
import org.springframework.cloud.consul.serviceregistry.ConsulRegistration;
import org.springframework.cloud.netflix.eureka.serviceregistry.EurekaRegistration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.event.EventListener;

/**
 * EasyMs服务注册自动配置类.
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/6/29 4:06 下午
 */
@Configuration(proxyBeanMethods = false)
@Import({EasyMsServiceRegistrationEventPublishingAspect.class})
@ConditionalOnProperty(value = "spring.cloud.service-registry.auto-registration.enabled", matchIfMissing = true)
@AutoConfigureAfter(name = {"org.springframework.cloud.netflix.eureka.EurekaClientAutoConfiguration",
        "org.springframework.cloud.consul.serviceregistry.ConsulAutoServiceRegistrationAutoConfiguration",
        "org.springframework.cloud.client.serviceregistry.AutoServiceRegistrationAutoConfiguration"})
public class EasyMsServiceRegistrationAutoConfiguration {
    
    @EventListener(EasyMsMonitorServicePreRegisteredEvent.class)
    public void onServiceInstancePreRegistered(EasyMsMonitorServicePreRegisteredEvent event) {
        Registration registration = event.getSource();
        if (registration == null) {
            return;
        }
        registration.getMetadata().put(EasyMsMonitorConstants.MONITOR_METADATA_KEY,
                JsonUtil.toJSONString(EasyMsMonitorRegister.getBriefMonitorInfo()));
    }
    
    @Configuration(proxyBeanMethods = false)
    @ConditionalOnBean(name = "org.springframework.cloud.netflix.eureka.EurekaClientAutoConfiguration")
    @Aspect
    private static class EurekaServiceRegisterAutoConfiguration {
        
        @EventListener(EasyMsMonitorServicePreRegisteredEvent.class)
        public void onServiceInstancePreRegistered(EasyMsMonitorServicePreRegisteredEvent event) {
            Registration registration = event.getSource();
            EurekaRegistration eurekaRegistration = (EurekaRegistration) registration;
            eurekaRegistration.getApplicationInfoManager().getInfo().getMetadata()
                    .put(EasyMsMonitorConstants.MONITOR_METADATA_KEY,
                            JsonUtil.toJSONString(EasyMsMonitorRegister.getBriefMonitorInfo()));
        }
        
    }
    
    @Configuration(proxyBeanMethods = false)
    @ConditionalOnBean(name = "org.springframework.cloud.consul.serviceregistry.ConsulAutoServiceRegistrationAutoConfiguration")
    @AutoConfigureOrder
    private static class ConsulServiceRegisterAutoConfiguration {
        
        @EventListener(EasyMsMonitorServicePreRegisteredEvent.class)
        public void onServiceInstancePreRegistered(EasyMsMonitorServicePreRegisteredEvent event) {
            Registration registration = event.getSource();
            Class<?> registrationClass = AopUtils.getTargetClass(registration);
            String registrationClassName = registrationClass.getName();
            if ("org.springframework.cloud.consul.serviceregistry.ConsulAutoRegistration"
                    .equalsIgnoreCase(registrationClassName)) {
                ConsulRegistration consulRegistration = (ConsulRegistration) registration;
                consulRegistration.getService().getTags()
                        .add(EasyMsMonitorConstants.MONITOR_METADATA_KEY + "=" + JsonUtil
                                .toJSONString(EasyMsMonitorRegister.getBriefMonitorInfo()));
            }
        }
        
    }
    
}
