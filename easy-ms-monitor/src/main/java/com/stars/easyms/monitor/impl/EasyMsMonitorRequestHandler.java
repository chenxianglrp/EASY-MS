package com.stars.easyms.monitor.impl;

import com.stars.easyms.monitor.constant.EasyMsMonitorConstants;
import com.stars.easyms.monitor.handler.EasyMsRequestHandler;
import com.stars.easyms.monitor.register.EasyMsMonitorRegister;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * <p>className: EasyMsMonitorRequestHandler</p>
 * <p>description: EasyMs监控请求处理类</p>
 *
 * @author guoguifang
 * @date 2019/11/20 11:38
 * @since 1.4.0
 */
public final class EasyMsMonitorRequestHandler implements EasyMsRequestHandler {

    @GetMapping(EasyMsMonitorConstants.MONITOR_URL)
    public void toIndex(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.getRequestDispatcher("/webjars/easyms/monitor/monitor.html").forward(request, response);
    }

    @PostMapping(EasyMsMonitorConstants.MONITOR_INFO_API)
    @ResponseBody
    public Map<String, Object> getMonitorInfo() {
        return EasyMsMonitorRegister.getMonitorInfo();
    }
}
