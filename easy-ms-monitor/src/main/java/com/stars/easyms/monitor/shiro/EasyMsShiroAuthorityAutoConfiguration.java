package com.stars.easyms.monitor.shiro;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <p>className: EasyMsShiroAuthorityAutoConfiguration</p>
 * <p>description: EasyMs的shiro权限管理的自动配置类</p>
 *
 * @author guoguifang
 * @date 2019-11-22 15:37
 * @since 1.4.1
 */
@Configuration
public class EasyMsShiroAuthorityAutoConfiguration {

    @Bean(initMethod = "doShiroAuthorityManagement")
    public EasyMsShiroAuthorityExecutor easyMsShiroAuthorityExecutor() {
        return new EasyMsShiroAuthorityExecutor();
    }

}