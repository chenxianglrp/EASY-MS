package com.stars.easyms.monitor.time;

import com.stars.easyms.base.util.DateTimeUtil;
import com.stars.easyms.monitor.MonitorRequestHandler;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * <p>className: EasyMsTimeMonitorRequestHandler</p>
 * <p>description: 时间监控请求处理类</p>
 *
 * @author guoguifang
 * @version 1.6.4
 * @date 2020/10/9 8:08 下午
 */
public final class EasyMsTimeMonitorRequestHandler implements MonitorRequestHandler {

    @Override
    public Map<String, Object> getMonitorInfo() {
        Map<String, Object> result = new LinkedHashMap<>();
        long currTime = System.currentTimeMillis();
        String localDateTimeStr = DateTimeUtil.getDatetimeNormalStrWithMills(currTime);
        result.put("local time", localDateTimeStr);
        String beijingDataTimeStr = DateTimeUtil.getBeijingDateTimeNormalStrWithMills(currTime);
        if (!beijingDataTimeStr.equals(localDateTimeStr)) {
            result.put("beijing time", beijingDataTimeStr);
        }
        result.put("country", System.getProperty("user.country"));
        result.put("timezone", System.getProperty("user.timezone"));
        return result;
    }
    
    @Override
    public Map<String, Object> getBriefMonitorInfo() {
        Map<String, Object> result = new LinkedHashMap<>();
        result.put("country", System.getProperty("user.country"));
        result.put("timezone", System.getProperty("user.timezone"));
        return result;
    }
    
    @Override
    public int getOrder() {
        return -100;
    }

    @Override
    public String getModuleName() {
        return "time";
    }
}
