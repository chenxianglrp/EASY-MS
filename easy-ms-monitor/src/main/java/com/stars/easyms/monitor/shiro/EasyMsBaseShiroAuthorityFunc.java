package com.stars.easyms.monitor.shiro;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * <p>className: EasyMsMonitorShiroAuthorityFunc</p>
 * <p>description: EasyMs的基础shiro权限管理方法类</p>
 *
 * @author guoguifang
 * @date 2019-11-22 15:40
 * @since 1.4.1
 */
public class EasyMsBaseShiroAuthorityFunc implements ShiroAuthorityFunc {

    @Override
    public Map<String, String> getFilterChainDefinitionMap() {
        Map<String, String> map = new LinkedHashMap<>();
        map.put("/easy-ms/**", NO_AUTHORITY_DEFINITION);
        map.put("/webjars/easyms/**", NO_AUTHORITY_DEFINITION);
        return map;
    }

    @Override
    public String getShiroAuthorityModuleName() {
        return "easy-ms";
    }

    @Override
    public int getShiroAuthorityOrder() {
        return 0;
    }
}