package com.stars.easyms.monitor;

import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Collections;
import java.util.Map;

/**
 * <p>interfaceName: MonitorFunc</p>
 * <p>description: 各个模块的监控方法接口</p>
 *
 * @author guoguifang
 * @date 2019-11-20 10:10
 * @since 1.4.0
 */
public interface MonitorFunc {
    
    /**
     * 获取单个模块的监控信息
     *
     * @return 单个模块的监控信息
     */
    @ResponseBody
    Map<String, Object> getMonitorInfo();
    
    /**
     * 获取单个模块的简要的监控信息，用来服务注册显示
     *
     * @return 单个模块的简要的监控信息
     */
    default Map<String, Object> getBriefMonitorInfo() {
        return Collections.emptyMap();
    }
    
    /**
     * 获取模块的名称
     *
     * @return 模块名称
     */
    String getModuleName();
    
    /**
     * 获取顺序位置，默认最低级别
     *
     * @return 顺序
     */
    default int getOrder() {
        return Integer.MAX_VALUE;
    }
    
}
