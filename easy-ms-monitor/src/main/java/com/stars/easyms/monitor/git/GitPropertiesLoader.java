package com.stars.easyms.monitor.git;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.parser.Feature;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.UrlResource;
import org.springframework.util.ClassUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * <p>className: GitPropertiesLoader</p>
 * <p>description: git.properties属性文件加载器</p>
 *
 * @author guoguifang
 * @version 1.2.1
 * @date 2019-06-05 16:05
 */
@Slf4j
final class GitPropertiesLoader {
    
    private static final String GIT_RESOURCE_LOCATION = "git.properties";
    
    private static final String SEPARATOR = "@#%!&";
    
    private static final Map<String, Object> GIT_PROPERTIES_MAP = new LinkedHashMap<>();
    
    private static final Map<String, Object> GIT_PROPERTIES_BRIEF_MAP = new LinkedHashMap<>();
    
    private GitPropertiesLoader() {
    }
    
    static {
        try {
            // 加载所有的git的properties文件
            ClassLoader classLoader = ClassUtils.getDefaultClassLoader();
            Enumeration<URL> urls = (classLoader != null ? classLoader.getResources(GIT_RESOURCE_LOCATION)
                    : ClassLoader.getSystemResources(GIT_RESOURCE_LOCATION));
            while (urls.hasMoreElements()) {
                URL url = urls.nextElement();
                UrlResource resource = new UrlResource(url);
                Map<String, Object> value = loadGitProperties(resource);
                GIT_PROPERTIES_MAP.put(url.toString(), value);
                GIT_PROPERTIES_BRIEF_MAP.put(getBriefGitPropertiesUrl(url.toString()), getBriefGitProperties(value));
            }
        } catch (IOException ex) {
            log.error("Unable to load git info from location [{}]", GIT_RESOURCE_LOCATION, ex);
        }
    }
    
    static Map<String, Object> getAllGitProperties() {
        return GIT_PROPERTIES_MAP;
    }
    
    static Map<String, Object> getAllBriefGitProperties() {
        return GIT_PROPERTIES_BRIEF_MAP;
    }
    
    private static Map<String, Object> loadGitProperties(UrlResource urlResource) {
        StringBuilder sb = new StringBuilder();
        String line;
        String errorMsg;
        try (InputStream inputStream = urlResource.getInputStream();
                Reader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
                BufferedReader br = new BufferedReader(reader)) {
            while ((line = br.readLine()) != null) {
                line = line.trim();
                // 如果当前行有字符并且不是注释则保存
                if (line.length() > 0 && !line.startsWith("#")) {
                    // 判断首字符是否是\uFEFF(UTF-16大端序、UTF-32)或\uFFFE(UTF-16小端序)，如果是则去掉
                    sb.append((line.charAt(0) == 65279 || line.charAt(0) == 65534) ? line.substring(1) : line);
                    
                    // git.properties文件有两种格式，一种是json，一种是普通的properties格式，因此每一行增加分隔符
                    sb.append(SEPARATOR);
                }
            }
            // 去掉末尾的分隔符并转换
            if (sb.toString().length() > 0) {
                return parse(sb.toString().substring(0, sb.lastIndexOf(SEPARATOR)));
            }
            errorMsg = "The file has no valid data";
        } catch (Exception e) {
            log.error("Load {} from {} fail!", GIT_RESOURCE_LOCATION, urlResource.getURL(), e);
            errorMsg = "Version information could not be retrieved";
        }
        Map<String, Object> resultMap = new HashMap<>(2);
        resultMap.put("errorMsg", errorMsg);
        return resultMap;
    }
    
    private static Map<String, Object> parse(String str) {
        // 如果是json格式直接使用JSON转换成JSONObject对象
        if (str.startsWith("{") && str.endsWith("}")) {
            return JSON.parseObject(str.replaceAll(SEPARATOR, ""), Feature.OrderedField);
        }
        Map<String, Object> resultMap = new TreeMap<>();
        // 如果不是json格式则按普通properties格式转换
        for (String s : str.split(SEPARATOR)) {
            String[] properties = s.split("=");
            if (properties.length == 1) {
                resultMap.put(properties[0], "");
            } else if (properties.length == 2) {
                resultMap.put(properties[0], properties[1].replaceAll("\\\\:", ":"));
            }
        }
        return resultMap;
    }
    
    private static String getBriefGitPropertiesUrl(String url) {
        if (url.startsWith("jar:file:") && url.endsWith("!/git.properties")) {
            url = url.substring(0, url.lastIndexOf(".jar"));
            url = url.substring(url.lastIndexOf("/") + 1);
        } else if (url.startsWith("file:") && url.endsWith("/target/classes/git.properties")) {
            url = url.substring(0, url.lastIndexOf("/target/classes/git.properties"));
            url = url.substring(url.lastIndexOf("/") + 1);
        }
        return url;
    }
    
    private static Map<String, Object> getBriefGitProperties(Map<String, Object> map) {
        Map<String, Object> resultMap = new LinkedHashMap<>();
        transfer(map, resultMap, "branch", "branch");
        transfer(map, resultMap, "commit.id.abbrev", "commitId");
        transfer(map, resultMap, "commit.time", "commitTime");
        transfer(map, resultMap, "build.time", "buildTime");
        return resultMap;
    }
    
    private static void transfer(Map<String, Object> sourceMap, Map<String, Object> targetMap, String sourceKey,
            String targetKey) {
        targetMap.put(targetKey, sourceMap.get("git." + sourceKey));
    }
    
}