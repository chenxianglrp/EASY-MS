package com.stars.easyms.monitor.register.event;

import org.springframework.cloud.client.serviceregistry.Registration;
import org.springframework.context.ApplicationEvent;

/**
 * EasyMs服务注册前事件实现类.
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/6/29 4:59 下午
 */
public class EasyMsMonitorServicePreRegisteredEvent extends ApplicationEvent {
    
    public EasyMsMonitorServicePreRegisteredEvent(Registration source) {
        super(source);
    }
    
    @Override
    public Registration getSource() {
        return (Registration) super.getSource();
    }
    
}
