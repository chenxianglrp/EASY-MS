package com.stars.easyms.monitor.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * <p>className: BaseEasyMsRequestHandler</p>
 * <p>description: 基础Handler类</p>
 *
 * @author guoguifang
 * @version 1.1.0
 * @date 2019-03-13 17:18
 */
public abstract class BaseEasyMsRequestHandler implements EasyMsRequestHandler {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    public Map<String, String[]> getParameterMap() {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = servletRequestAttributes.getRequest();
        return request.getParameterMap();
    }

    public String getParameter(String name) {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = servletRequestAttributes.getRequest();
        return request.getParameter(name);
    }

}