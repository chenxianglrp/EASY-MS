package com.stars.easyms.monitor.git;

import com.stars.easyms.monitor.MonitorRequestHandler;

import java.util.Map;

/**
 * <p>className: EasyMsGitMonitorRequestHandler</p>
 * <p>description: EasyMsGit版本监控请求处理类</p>
 *
 * @author guoguifang
 * @date 2019/11/26 14:59
 * @since 1.4.1
 */
public final class EasyMsGitMonitorRequestHandler implements MonitorRequestHandler {

    @Override
    public Map<String, Object> getMonitorInfo() {
        return GitPropertiesLoader.getAllGitProperties();
    }
    
    @Override
    public Map<String, Object> getBriefMonitorInfo() {
        return GitPropertiesLoader.getAllBriefGitProperties();
    }
    
    @Override
    public int getOrder() {
        return 30;
    }

    @Override
    public String getModuleName() {
        return "git";
    }

}
