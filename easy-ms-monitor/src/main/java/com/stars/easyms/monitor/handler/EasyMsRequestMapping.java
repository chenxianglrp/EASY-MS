package com.stars.easyms.monitor.handler;

import org.springframework.core.annotation.AliasFor;
import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.annotation.*;

/**
 * <p>className: EasyMsRequestMapping</p>
 * <p>description: RequestMethod不区分Get和Post的RequestMapping</p>
 *
 * @author guoguifang
 * @date 2019/3/21 15:51
 * @version 1.2.1
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@RequestMapping
public @interface EasyMsRequestMapping {

    @AliasFor(annotation = RequestMapping.class) String[] value() default {};

}
