package com.stars.easyms.monitor.constant;

/**
 * <p>className: EasyMsMonitorConstants</p>
 * <p>description: EasyMs监控常量类</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/6/29 3:49 下午
 */
public class EasyMsMonitorConstants {
    
    public static final String MONITOR_URL = "/monitor";
    
    public static final String MONITOR_INFO_API = "/easy-ms/monitor/getInfo";
    
    public static final String MONITOR_METADATA_KEY = "easy-ms.info";
    
    private EasyMsMonitorConstants() {
    
    }
}
