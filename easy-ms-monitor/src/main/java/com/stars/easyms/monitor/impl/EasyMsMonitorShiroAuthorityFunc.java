package com.stars.easyms.monitor.impl;

import com.stars.easyms.monitor.shiro.ShiroAuthorityFunc;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * <p>className: EasyMsMonitorShiroAuthorityFunc</p>
 * <p>description: EasyMs的基础shiro权限管理方法类</p>
 *
 * @author guoguifang
 * @date 2019-11-22 15:40
 * @since 1.4.1
 */
public class EasyMsMonitorShiroAuthorityFunc implements ShiroAuthorityFunc {

    @Override
    public Map<String, String> getFilterChainDefinitionMap() {
        Map<String, String> map = new LinkedHashMap<>();
        map.put("/monitor", NO_AUTHORITY_DEFINITION);
        return map;
    }

    @Override
    public String getShiroAuthorityModuleName() {
        return "monitor";
    }

    @Override
    public int getShiroAuthorityOrder() {
        return 1;
    }
}