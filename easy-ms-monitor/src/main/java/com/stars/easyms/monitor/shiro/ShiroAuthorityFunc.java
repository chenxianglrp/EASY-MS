package com.stars.easyms.monitor.shiro;

import java.util.Map;

/**
 * <p>interfaceName: ShiroAuthorityFunc</p>
 * <p>description: Shiro权限管理接口</p>
 *
 * @author guoguifang
 * @date 2019-11-22 13:41
 * @since 1.4.1
 */
public interface ShiroAuthorityFunc {

    String NO_AUTHORITY_DEFINITION = "noSessionCreation,anon";

    /**
     * 获取Shiro权限过滤定义Map
     *
     * @return Shiro权限过滤定义Map
     */
    Map<String, String> getFilterChainDefinitionMap();

    /**
     * 获取模块的名称
     *
     * @return 模块名称
     */
    String getShiroAuthorityModuleName();

    /**
     * 获取顺序位置，默认最低级别
     *
     * @return 顺序
     */
    default int getShiroAuthorityOrder() {
        return Integer.MAX_VALUE;
    }

}
