package com.stars.easyms.monitor;

import com.stars.easyms.monitor.handler.EasyMsRequestHandler;
import com.stars.easyms.base.util.StringFormatUtil;
import com.stars.easyms.monitor.shiro.ShiroAuthorityFunc;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * <p>interfaceName: EasyMsRequestHandler</p>
 * <p>description: RequestMappingHandler接口</p>
 *
 * @author guoguifang
 * @version 1.2.1
 * @date 2019-03-21 15:07
 */
public interface MonitorRequestHandler extends EasyMsRequestHandler, MonitorFunc, ShiroAuthorityFunc {

    /**
     * 跳转到Index页面
     *
     * @param request  请求
     * @param response 响应
     */
    default void toIndex(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.getRequestDispatcher("/webjars/easyms/monitor/monitor.html").forward(request, response);
    }

    /**
     * 获取Index页面的url
     *
     * @return Index页面的url
     */
    default String getIndexUrl() {
        return getModuleName();
    }

    @Override
    default Map<String, String> getFilterChainDefinitionMap() {
        Map<String, String> map = new LinkedHashMap<>();
        map.put(StringFormatUtil.formatRequestMappingPath(getIndexUrl()), NO_AUTHORITY_DEFINITION);
        return map;
    }

    @Override
    default String getShiroAuthorityModuleName() {
        return getModuleName();
    }

}
