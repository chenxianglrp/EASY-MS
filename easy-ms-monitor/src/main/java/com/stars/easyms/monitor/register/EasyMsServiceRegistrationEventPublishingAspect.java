package com.stars.easyms.monitor.register;

import com.stars.easyms.monitor.register.event.EasyMsMonitorServicePreRegisteredEvent;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.cloud.client.serviceregistry.Registration;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.lang.NonNull;

/**
 * EasyMs服务注册事件拦截器.
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/6/29 5:12 下午
 */
@Aspect
public class EasyMsServiceRegistrationEventPublishingAspect implements ApplicationEventPublisherAware {
    
    private ApplicationEventPublisher applicationEventPublisher;
    
    @Before("execution(* org.springframework.cloud.client.serviceregistry.ServiceRegistry.register(*)) && args(registration)")
    public void beforeRegister(Registration registration) {
        applicationEventPublisher.publishEvent(new EasyMsMonitorServicePreRegisteredEvent(registration));
    }
    
    @Override
    public void setApplicationEventPublisher(@NonNull ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }
    
}
