package com.stars.easyms.monitor.system;

import com.stars.easyms.base.util.DateTimeUtil;
import com.stars.easyms.monitor.MonitorRequestHandler;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import java.lang.management.RuntimeMXBean;
import java.text.NumberFormat;
import java.util.*;

/**
 * <p>className: EasyMsGitMonitorRequestHandler</p>
 * <p>description: EasyMsGit版本监控请求处理类</p>
 *
 * @author guoguifang
 * @date 2019/11/26 14:59
 * @since 1.4.1
 */
public final class EasyMsSystemMonitorRequestHandler implements MonitorRequestHandler {

    private static final int GB = 1073741824;

    private static final int MB = 1048576;

    private static final int KB = 1024;

    private static final NumberFormat NUMBER_FORMAT = NumberFormat.getInstance();

    @Override
    public Map<String, Object> getMonitorInfo() {
        Runtime runtime = Runtime.getRuntime();

        long maxMemory = runtime.maxMemory();
        long allocatedMemory = runtime.totalMemory();
        long freeMemory = runtime.freeMemory();

        long physicalMemory;
        try {
            physicalMemory = ((com.sun.management.OperatingSystemMXBean) ManagementFactory
                    .getOperatingSystemMXBean()).getTotalPhysicalMemorySize();
        } catch (Exception e) {
            physicalMemory = -1L;
        }

        int availableCores = Runtime.getRuntime().availableProcessors();

        Map<String, Object> result = new LinkedHashMap<>();
        result.put("Java version", System.getProperty("java.vendor") + " " + System.getProperty("java.version"));
        result.put("Operating system", System.getProperty("os.name") + " " + System.getProperty("os.version"));
        result.put("File Encoding", System.getProperty("file.encoding"));
        result.put("CPU Cores", availableCores);
        if (physicalMemory != -1L) {
            result.put("Physical Memory", parseFromByteSize(physicalMemory));
        }
        result.put("Max allowed memory", parseFromByteSize(maxMemory));
        result.put("Allocated memory", parseFromByteSize(allocatedMemory));
        result.put("Used memory in allocated", parseFromByteSize((allocatedMemory - freeMemory)));
        result.put("Free memory in allocated", parseFromByteSize(freeMemory));
        result.put("Total free memory", parseFromByteSize((freeMemory + (maxMemory - allocatedMemory))));

        MemoryMXBean memoryMxBean = ManagementFactory.getMemoryMXBean();
        result.put("Heap Memory Usage", getMemoryUsageMap(memoryMxBean.getHeapMemoryUsage()));
        result.put("Non-Heap Memory Usage", getMemoryUsageMap(memoryMxBean.getNonHeapMemoryUsage()));
    
        RuntimeMXBean runtimeMX = ManagementFactory.getRuntimeMXBean();
        long startTime = runtimeMX.getStartTime();
        String localDateTimeStr = DateTimeUtil.getDatetimeNormalStrWithMills(startTime);
        result.put("Start time", localDateTimeStr);
        String beijingDataTimeStr = DateTimeUtil.getBeijingDateTimeNormalStrWithMills(startTime);
        if (!beijingDataTimeStr.equals(localDateTimeStr)) {
            result.put("Start beijing time", beijingDataTimeStr);
        }
        return result;
    }
    
    @Override
    public Map<String, Object> getBriefMonitorInfo() {
        Runtime runtime = Runtime.getRuntime();
    
        long maxMemory = runtime.maxMemory();
        long allocatedMemory = runtime.totalMemory();
        long freeMemory = runtime.freeMemory();
    
        Map<String, Object> result = new LinkedHashMap<>();
        result.put("maxMemory", parseFromByteSize(maxMemory));
        result.put("totalMemory", parseFromByteSize(allocatedMemory));
        result.put("freeMemory", parseFromByteSize(freeMemory));
        return result;
    }
    
    @Override
    public int getOrder() {
        return -20;
    }

    @Override
    public String getModuleName() {
        return "system";
    }
    
    private Map<String, Object> getMemoryUsageMap(MemoryUsage memoryUsage) {
        Map<String, Object> memoryUsageMap = new LinkedHashMap<>();
        memoryUsageMap.put("init", parseFromByteSize(memoryUsage.getInit()));
        memoryUsageMap.put("used", parseFromByteSize(memoryUsage.getUsed()));
        memoryUsageMap.put("committed", parseFromByteSize(memoryUsage.getCommitted()));
        memoryUsageMap.put("max", parseFromByteSize(memoryUsage.getMax()));
        return memoryUsageMap;
    }

    private String parseFromByteSize(long byteSize) {
        if (byteSize <= 0) {
            return String.valueOf(byteSize);
        }
        long gb = byteSize >> 30;
        if (byteSize - gb * GB == 0 || gb > 100) {
            return NUMBER_FORMAT.format(gb) + " GB";
        }
        long mb = byteSize >> 20;
        if (byteSize - mb * MB == 0 || mb > 100) {
            return NUMBER_FORMAT.format(mb) + " MB";
        }
        long kb = byteSize >> 10;
        if (byteSize - kb * KB == 0 || kb > 100) {
            return NUMBER_FORMAT.format(kb) + " KB";
        }
        return NUMBER_FORMAT.format(byteSize) + " B";
    }

}
