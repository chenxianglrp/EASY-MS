package com.stars.easyms.monitor.spring;

import com.stars.easyms.base.util.SpringBootUtil;
import com.stars.easyms.monitor.MonitorRequestHandler;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * <p>className: EasyMsSpringMonitorRequestHandler</p>
 * <p>description: Spring相关监控</p>
 *
 * @author guoguifang
 * @version 1.6.4
 * @date 2020/10/14 11:16 上午
 */
public final class EasyMsSpringMonitorRequestHandler implements MonitorRequestHandler {

    @Override
    public Map<String, Object> getMonitorInfo() {
        Map<String, Object> result = new LinkedHashMap<>();
        result.put("spring.application.name", SpringBootUtil.getApplicationName());
        result.put("spring.profiles.active", SpringBootUtil.getActiveProfile());
        result.put("spring.boot.version", SpringBootUtil.getSpringBootVersion());
        return result;
    }

    @Override
    public int getOrder() {
        return -80;
    }

    @Override
    public String getModuleName() {
        return "spring";
    }
}
