package com.stars.easyms.monitor.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

/**
 * <p>className: EasyMsMonitorHandlerAutoConfiguration</p>
 * <p>description: EasyMs的monitor模块自动配置类</p>
 *
 * @author guoguifang
 * @date 2019-11-22 15:37
 * @since 1.4.1
 */
@Configuration
@ConditionalOnClass({RequestMappingHandlerMapping.class})
public class EasyMsMonitorHandlerAutoConfiguration {

    @Bean(initMethod = "init")
    public EasyMsMonitorRequestMappingInitializer easyMsMonitorRequestMappingInitializer() {
        return new EasyMsMonitorRequestMappingInitializer();
    }

}