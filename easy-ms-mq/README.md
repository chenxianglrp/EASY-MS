# Easy-Ms-Mq

　　easy-ms-mq是一个基于spring的、可支持多种MQ的功能增强型框架，是Easy-Ms的一个组件框架，**但也支持在非Easy-Ms项目的Spring Boot项目中使用**。阿里巴巴开发规范里的应用分层规范里增加了通用处理层（Manager层），easy-ms-mq框架让该规范得到更好的展现。

## 一、 功能特点

1. 支持单条与批量两种发送方式（生产者把多条拼成一条数据发送，消费者把多条拆分成单条处理或多条批量处理）；
2. 支持MQ消息缓存，可实时查询生产与消费信息，5天后自动把成功消息归档；
3. 支持MQ消息发送、消费失败后重新发送、消费，有失败告警；
4. 支持ActiveMQ和RocketMQ

## 二、 依赖引入

#### 1. 使用parent的方式

　　好处：easy-ms-parent里包含了许多的依赖版本管理，可以在依赖管理`<dependencies>`中，有较多常用的引入直接使用默认的版本而无需指定版本号

```xml
pom.xml:
    <!-- 可以直接把easy-ms-parent当做parent，或者当引入多个easy-ms组件时使用parent可以只需设置一次版本号 -->
    <parent>
        <groupId>com.stars.easyms</groupId>
        <artifactId>easy-ms-parent</artifactId>
        <version>${easy-ms.version}</version>
    </parent>
		
    <!-- 如果是直接引入easy-ms，则不需要引入easy-ms-mq，easy-ms已经默认依赖了easy-ms-mq -->
    <dependencies>
        <dependency>
            <groupId>com.stars.easyms</groupId>
            <artifactId>easy-ms-mq</artifactId>
        </dependency>
    </dependencies>
```

#### 2. 直接引用

```xml
    <!-- 如果是直接引入easy-ms，则不需要引入easy-ms-mq，easy-ms已经默认依赖了easy-ms-mq -->
    <dependencies>
        <dependency>
            <groupId>com.stars.easyms</groupId>
            <artifactId>easy-ms-mq</artifactId>
            <version>${easy-ms.version}</version>
        </dependency>
    </dependencies>
```

## 三、 数据库表创建

　　目前支持oracle和mysql数据库的表创建：

　　Oracle: [请点击这里](/easy-ms-mq/doc/mq/1.mq表创建脚本_oracle.sql)

　　Mysql: [请点击这里](/easy-ms-mq/doc/mq/2.mq表创建脚本_mysql.sql)

## 四、 使用方式

#### 1. 发送端

　　将注解@MQSender增加在对应的方法上即可，@MQSender详细参数如下：

| 参数               | 默认值                       | 说明                                                         |
| ------------------ | ---------------------------- | ------------------------------------------------------------ |
| key                |                              | MQ发送消息的KEY值，rocketMQ表示的是topic值，支持表达式，例如${test}可通过spring.mq.key.test=test的方式配置 |
| name               |                              | 当前消息对应的中文名称                                       |
| destinationType    | DestinationType.QUEUE        | 枚举类，QUEUE:点对点，TOPIC:发布/订阅，rocketMQ只有topic模式 |
| batch              | true                         | 是否批量发送：将相同key值的消息合并起来发送，可降低连接次数，大大提升性能 |
| channelCount       | 1                            | 发送通道数量，为了避免消息拥堵，该值表示一个相同的KEY使用几个通道发送与接收，发送端和接收端的数值必须一致，不能超过5个，超过5个时默认5个，发送的消息会均匀分配到各个通道中发送，适用于高并发的情况 |
| channelSize        | 300                          | 当使用批量发送时，单个通道允许的最大发送数量，当达到该值时立即发送，否则需等待100ms后合并这10ms内的所有消息合并起来发送 |
| maxRetryCount       | 5                            | 最大失败重试次数                                             |
| redeliveryPolicy   | RedeliveryPolicy.EXPONENTIAL | 消息发送失败后的重发机制                                     |
| redeliveryDelay    | 60                           | 消息重发时间间隔(单位:秒)                                    |
| allowNonPersistent | false                        | 是否允许使用非持久化模式发送消息，使用非持久化模式会提升MQ性能，但会有丢失数据风险：该参数ActiveMQ有效,RocketMQ无效 |
| isDynamicKey       | false                        | key值是否随着环境变化(默认false，如果是true的话key值结尾会增加"_${spring.profiles.active}")，用于自动区分不同的环境 |

　　使用demo如下：

```java
@MQManager
public class MQDemoManager {

    @MQSender(key = "test_dev", name = "发送端demo")
    public void send(UserInfo userInfo) {
        // Intentionally blank
    }
    
}

@Service
public class EasyMsMQDemoService implements RestService<BaseInput, BaseOutput> {

    @Autowired
    private MQDemoManager mqDemoManager;

    @Override
    public BaseOutput execute(BaseInput input) {
        UserInfo userInfo = new UserInfo();
        userInfo.setName("小明");
        userInfo.setAge(10);
        mqDemoManager.send(userInfo);
        return new BaseOutput();
    }
}
```

#### 2. 接收端

　　将注解@MQReceiver增加在对应的方法上即可，@MQReceiver详细参数如下：

| 参数             | 默认值                       | 说明                                                         |
| ---------------- | ---------------------------- | ------------------------------------------------------------ |
| key              |                              | MQ接收消息的KEY值，rocketMQ表示的是topic值，支持表达式，例如${test}可通过spring.mq.key.test=test的方式配置 |
| name             |                              | 当前消息对应的中文名称                                       |
| destinationType  | DestinationType.QUEUE        | 枚举类，QUEUE:点对点，TOPIC:发布/订阅，rocketMQ只有topic模式 |
| batch            | true                         | 是否批量发送：将相同key值的消息合并起来发送，可降低连接次数，大大提升性能 |
| overall          | false                        | 当使用批量处理数据时是否整体提交或者整体回滚，默认是非整体   |
| channelCount     | 1                            | 消费通道数量，发送端和接收端的数值必须一致，适用于高并发的情况 |
| maxRetryCount     | 5                            | 最大失败重试次数                                             |
| redeliveryPolicy | RedeliveryPolicy.EXPONENTIAL | 消息消费失败后的重新消费机制                                 |
| redeliveryDelay  | 60                           | 消息重新消费时间间隔(单位:秒)                                |
| isDynamicKey     | false                        | key值是否随着环境变化(默认false，如果是true的话key值结尾会增加"_${spring.profiles.active}")，用于自动区分不同的环境 |

　　使用demo如下：

```java
@MQManager
public class MQDemoManager {

    @Autowired
    private UserInfoService userInfoService;
  
  	/**
     * key可以使用${}表达式，使用spring.mq.key.test=test_dev的方式配置
     * 接收的参数需要跟注解的batch对应，默认是开启批量，因此需要接收时的参数为List类型，若未开启批量则接收类型为UserInfo
     */
    @MQReceiver(key = "${test}", name = "接收端demo")
    public BatchResult receive(List<UserInfo> userInfoList) {
        userInfoService.test(userInfoList);
        return BatchResult.ofSuccess(userInfoList);
    }

}

@Service
public class UserInfoService {

  	/**
     * 处理逻辑
     */
    public void test(List<UserInfo> userInfoList) {
        for (UserInfo userInfo : userInfoList) {
            System.out.println(userInfo.getName() + ":" + userInfo.getAge());
        }
    }

}
```

> 　　我们提供框架的维护和技术支持，提供给感兴趣的开发人员学习和交流的平台，同时希望感兴趣的同学一起加入我们让框架更完善，让我们一起为了给大家提供更好更优秀的框架而努力。
>
> 　　若该框架及本文档中有什么错误及问题的，可采用创建Issue、加群@管理、私聊作者或管理员的方式进行联系，谢谢！
