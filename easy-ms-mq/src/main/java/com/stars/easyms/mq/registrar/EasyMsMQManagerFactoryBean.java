package com.stars.easyms.mq.registrar;

import com.stars.easyms.mq.exception.EasyMsMQException;
import com.stars.easyms.mq.interceptor.EasyMsMQCglibMethodInterceptor;
import com.stars.easyms.mq.interceptor.EasyMsMQJdkMethodInterceptor;
import lombok.Setter;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.cglib.core.SpringNamingPolicy;
import org.springframework.cglib.proxy.Enhancer;

import java.lang.reflect.Modifier;
import java.lang.reflect.Proxy;
import java.util.Objects;

/**
 * <p>className: EasyMsMQManagerFactoryBean</p>
 * <p>description: EasyMsMQManager工厂Bean</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/4/14 2:34 下午
 */
@Setter
class EasyMsMQManagerFactoryBean implements FactoryBean<Object> {

    private static AutowireCapableBeanFactory beanFactory;

    private Class<?> type;

    private String beanName;

    @Override
    public Object getObject() {
        if (Modifier.isFinal(type.getModifiers())) {
            throw new EasyMsMQException("The class '{}' with @MQManager annotation cannot be final!", type.getName());
        }

        if (type.isInterface()) {
            return Proxy.newProxyInstance(type.getClassLoader(), new Class[]{type}, new EasyMsMQJdkMethodInterceptor(type));
        }

        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(type);
        enhancer.setNamingPolicy(SpringNamingPolicy.INSTANCE);
        enhancer.setCallback(new EasyMsMQCglibMethodInterceptor(type));
        Object instance = enhancer.create();
        beanFactory.autowireBean(instance);
        beanFactory.initializeBean(instance, beanName);
        return instance;

    }

    static void setBeanFactory(AutowireCapableBeanFactory beanFactory) {
        EasyMsMQManagerFactoryBean.beanFactory = beanFactory;
    }

    @Override
    public Class<?> getObjectType() {
        return type;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EasyMsMQManagerFactoryBean that = (EasyMsMQManagerFactoryBean) o;
        return Objects.equals(type, that.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type);
    }

    @Override
    public String toString() {
        return "EasyMsMQManagerFactoryBean{type=" + type + "}";
    }

}
