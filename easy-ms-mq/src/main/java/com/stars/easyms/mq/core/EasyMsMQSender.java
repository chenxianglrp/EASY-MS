package com.stars.easyms.mq.core;

import com.alibaba.fastjson.JSON;
import com.stars.easyms.mq.enums.MQStatus;
import com.stars.easyms.mq.enums.RedeliveryPolicy;
import com.stars.easyms.mq.bean.MQMessageInfo;
import com.stars.easyms.mq.enums.DestinationType;
import com.stars.easyms.mq.enums.MQType;
import com.stars.easyms.mq.registrar.EasyMsMQHolder;
import com.stars.easyms.mq.util.EasyMsMQIDUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;

/**
 * MQ消息发送类
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
public class EasyMsMQSender {

    private static final Logger logger = LoggerFactory.getLogger(EasyMsMQSender.class);

    private final MQType mqType = MQType.SEND;

    /**
     * 发送MQ消息默认方法：点对点消息、重试策略为指数策略、重试间隔为60秒
     *
     * @param key 发送目标:ActiveMQ对应destination，RocketMQ对应topic
     * @param obj 消息对象
     */
    public void send(String key, Object obj) {
        send(key, DestinationType.TOPIC, RedeliveryPolicy.EXPONENTIAL, 60, obj);
    }

    /**
     * 发送MQ消息方法
     *
     * @param key             发送目标:ActiveMQ对应destination，RocketMQ对应topic
     * @param destinationType 发送类型：点对点、广播
     * @param obj             消息对象
     */
    public void send(String key, DestinationType destinationType, Object obj) {
        send(key, destinationType, RedeliveryPolicy.EXPONENTIAL, 60, obj);
    }

    /**
     * 发送MQ消息方法：点对点个
     *
     * @param key              发送目标:ActiveMQ对应destination，RocketMQ对应topic
     * @param redeliveryPolicy 重试策略
     * @param redeliveryDelay  重试间隔
     * @param obj              消息对象
     */
    public void send(String key, RedeliveryPolicy redeliveryPolicy, long redeliveryDelay, Object obj) {
        send(key, DestinationType.TOPIC, redeliveryPolicy, redeliveryDelay, obj);
    }

    /**
     * 发送MQ消息方法:只要执行了该方法就表示已经发送成功，如果有事务则不可回滚
     *
     * @param key              发送目标:ActiveMQ对应destination，RocketMQ对应topic
     * @param destinationType  发送类型：点对点、广播
     * @param redeliveryPolicy 重试策略
     * @param redeliveryDelay  重试间隔
     * @param obj              消息对象
     */
    public void send(String key, DestinationType destinationType, RedeliveryPolicy redeliveryPolicy, long redeliveryDelay, Object obj) {
        // 将参数转换成字符串
        String message;
        if (obj == null) {
            return;
        } else if (obj instanceof String) {
            message = (String) obj;
        } else {
            message = JSON.toJSONString(obj);
        }

        // 记录待发送消息日志
        logger.info("发送MQ消息到目标({}),消息内容:{}", key, message);

        // 采用异步处理机制处理MQ消息发送
        MQMessageInfo mqMessageInfo = new MQMessageInfo();
        mqMessageInfo.setId(EasyMsMQIDUtil.getId());
        mqMessageInfo.setMessageId(EasyMsMQIDUtil.getMessageId());
        mqMessageInfo.setMqType(mqType);
        mqMessageInfo.setDestinationType(destinationType.getCode());
        mqMessageInfo.setKey(key);
        mqMessageInfo.setSourceSys(EasyMsMQHolder.getApplicationId());
        mqMessageInfo.setMessage(message);
        mqMessageInfo.setMqStatus(MQStatus.INIT);
        mqMessageInfo.setRedeliveryPolicy(redeliveryPolicy.getCode());
        mqMessageInfo.setRedeliveryDelay(redeliveryDelay);

        // 设置消息最后存活时间与消息创建时间
        long currTime = System.currentTimeMillis();
        mqMessageInfo.setLastLiveTime(currTime);
        mqMessageInfo.setCreateTime(new Timestamp(currTime));

        // 将MQ消息缓存在redis中一份数据，防止由于服务重启、宕机等情况引起的异步插入数据库时数据丢失，尽可能的保证MQ发送的可用性、稳定性
        MQHolder.getEasyMsMQRedisManager()
                .setMQMessageInfoToRedis(mqType.getName(), mqMessageInfo.getMessageId(), mqMessageInfo);

        // 这里使用异步方式发送mq消息，用于削峰的同时可以增大并发量及增加MQ的吞吐量，在异步的时候可以分批次批量发送
        AsyncSendMQMessageHandler.handle(mqMessageInfo);
    }

}
