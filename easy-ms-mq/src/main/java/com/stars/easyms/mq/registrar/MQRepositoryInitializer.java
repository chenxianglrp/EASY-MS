package com.stars.easyms.mq.registrar;

import com.stars.easyms.datasource.mybatis.EasyMsMapperScannerConfigurer;
import com.stars.easyms.mq.annotation.MQRepository;
import com.stars.easyms.mq.util.EasyMsMQPackageUtil;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * <p>className: MQRepositoryInitializer</p>
 * <p>description: MQ数据持久化初始化类</p>
 *
 * @author guoguifang
 * @date 2019-10-29 18:08
 * @since 1.3.3
 */
final class MQRepositoryInitializer {

    static void init(ConfigurableApplicationContext applicationContext) {
        initDdl(applicationContext);
        initDml(applicationContext);
    }

    /**
     * 扫描Repository Package下所有DAO接口
     */
   private static void initDdl(ConfigurableApplicationContext applicationContext) {
        EasyMsMapperScannerConfigurer mapperScannerConfigurer = new EasyMsMapperScannerConfigurer(applicationContext);
        mapperScannerConfigurer.setSqlSessionFactoryBeanName("easyMsMQDDLSqlSessionFactory");
        mapperScannerConfigurer.setBasePackage(EasyMsMQPackageUtil.getRepositoryDdlPackageName());
        mapperScannerConfigurer.addAnnotationClass(MQRepository.class);
        DefaultListableBeanFactory defaultListableBeanFactory = (DefaultListableBeanFactory) applicationContext.getAutowireCapableBeanFactory();
        defaultListableBeanFactory.registerSingleton(
                EasyMsMQPackageUtil.getRepositoryDdlPackageName() + ".mapperScannerConfigurer", mapperScannerConfigurer);
        mapperScannerConfigurer.postProcessBeanDefinitionRegistry(defaultListableBeanFactory);
    }

    /**
     * 扫描Repository Package下所有DAO接口
     */
    private static void initDml(ConfigurableApplicationContext applicationContext) {
        EasyMsMapperScannerConfigurer mapperScannerConfigurer = new EasyMsMapperScannerConfigurer(applicationContext);
        mapperScannerConfigurer.setSqlSessionFactoryBeanName("easyMsMQSqlSessionFactory");
        mapperScannerConfigurer.setBasePackage(EasyMsMQPackageUtil.getRepositoryDmlPackageName());
        mapperScannerConfigurer.addAnnotationClass(MQRepository.class);
        DefaultListableBeanFactory defaultListableBeanFactory = (DefaultListableBeanFactory) applicationContext.getAutowireCapableBeanFactory();
        defaultListableBeanFactory.registerSingleton(
                EasyMsMQPackageUtil.getRepositoryDmlPackageName() + ".mapperScannerConfigurer", mapperScannerConfigurer);
        mapperScannerConfigurer.postProcessBeanDefinitionRegistry(defaultListableBeanFactory);
    }

    private MQRepositoryInitializer() {}

}