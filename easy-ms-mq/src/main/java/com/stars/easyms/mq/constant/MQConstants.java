package com.stars.easyms.mq.constant;

/**
 * MQ常量类
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
public final class MQConstants {
    
    public static final String MODULE_NAME = "mq";

    /**
     * 分布式锁
     */
    public static final String MQ_SCHEDULER_LOCK = "mqSchedulerLock";

    /**
     * MQ最大循环调度异常数量
     */
    public static final int MAX_SCHEDULE_EXCEPTION_COUNT = 10000;

    /**
     * 单次批量查询数量
     */
    public static final int PER_CATCH_SIZE = 3000;

    /**
     * 每次最大处理数量
     */
    public static final int MAX_HANDLE_COUNT_PER_TIME = 100;

    /**
     * 处理失败默认结果
     */
    public static final String RECEIVE_HANDLE_FAIL = "处理失败";

    /**
     * 处理成功默认结果
     */
    public static final String RECEIVE_HANDLE_SUCCESS = "处理成功";

    /**
     * MQ服务未激活
     */
    public static final String MQ_DISABLED = "MQ服务未开启";

    /**
     * 无效的MQ key值
     */
    public static final String INVALID_MQ_KEY = "未找到有效的MQ注册信息";

    /**
     * 发送、接收失败结果最大length
     */
    public static final int MAX_FAIL_RESULT_LENGTH = 800;

    /**
     * MQ消息信息表名
     */
    public static final String TABLE_NAME_MQ_MESSAGE_INFO = "mq_message_info";

    /**
     * MQ消息历史表名
     */
    public static final String TABLE_NAME_MQ_MESSAGE_HIS = "mq_message_his";

    private MQConstants() {
    }
}
