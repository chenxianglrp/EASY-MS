package com.stars.easyms.mq.registrar;

import com.stars.easyms.base.util.DefaultThreadFactory;
import com.stars.easyms.base.util.ExecutorUtil;
import com.stars.easyms.mq.bean.EasyMsMQInfo;
import com.stars.easyms.mq.core.MQFailRetryScheduler;
import com.stars.easyms.mq.core.MQListener;
import com.stars.easyms.mq.core.MQListenerFactory;
import com.stars.easyms.mq.core.MQScheduler;
import com.stars.easyms.mq.enums.MQType;
import com.stars.easyms.mq.properties.EasyMsMQProperties;
import org.springframework.beans.BeansException;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.lang.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

/**
 * <p>className: EasyMsMQListenerRunner</p>
 * <p>description: EasyMsMQ监听器在springboot启动完成后启动</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/6/16 11:04 上午
 */
@SuppressWarnings("unchecked")
public class EasyMsMQListenerRunner implements ApplicationRunner, ApplicationContextAware {

    private ApplicationContext applicationContext;

    @Override
    public void run(ApplicationArguments args) {
        int mqReceiveChannelInfoMapSize = EasyMsMQHolder.MQ_RECEIVER_INFO_MAP.size();
        if (mqReceiveChannelInfoMapSize > 0) {
            ThreadFactory schedulerThreadFactory = new DefaultThreadFactory().setNameFormat("easy-ms-mq-scheduler-pool1-%d").build();
            ExecutorService scheduleThreadPool = new ThreadPoolExecutor(1, 1,
                    0, TimeUnit.SECONDS, new LinkedBlockingQueue<>(), schedulerThreadFactory);
            MQScheduler.initMQReceiveScheduler(mqReceiveChannelInfoMapSize);
            scheduleThreadPool.execute(new MQFailRetryScheduler(MQType.RECEIVE));

            // 启动MQ监听器
            EasyMsMQProperties easyMsMQProperties = applicationContext.getBean(EasyMsMQProperties.class);
            ExecutorUtil.execute(() -> {
                for (Map.Entry<String, EasyMsMQInfo> entry : EasyMsMQHolder.MQ_RECEIVER_INFO_MAP.entrySet()) {
                    String multichannelKey = entry.getKey();
                    EasyMsMQInfo easyMsMqInfo = entry.getValue();
                    List<MQListener> mqListenerList = easyMsMqInfo.getMqListenerList();
                    if (mqListenerList == null) {
                        mqListenerList = new ArrayList<>();
                        easyMsMqInfo.setMqListenerList(mqListenerList);
                    }
                    MQListenerFactory mqListenerFactory = new MQListenerFactory(applicationContext, easyMsMQProperties);
                    MQListener mqListener = mqListenerFactory.getMQListener(easyMsMqInfo, multichannelKey);
                    mqListener.start();
                    mqListenerList.add(mqListener);
                }
            });
        }
    }

    @Override
    public void setApplicationContext(@NonNull ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
