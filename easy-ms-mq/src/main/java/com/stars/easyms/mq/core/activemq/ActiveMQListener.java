package com.stars.easyms.mq.core.activemq;

import com.alibaba.fastjson.JSON;
import com.stars.easyms.mq.core.BaseMQListener;
import com.stars.easyms.mq.core.MQListener;
import com.stars.easyms.mq.bean.EasyMsMQInfo;
import com.stars.easyms.mq.enums.DestinationType;
import com.stars.easyms.mq.properties.EasyMsMQProperties;
import com.stars.easyms.base.util.*;
import org.springframework.context.ApplicationContext;

import javax.jms.*;
import java.util.*;

/**
 * ActiveMQ监听类
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
public class ActiveMQListener extends BaseMQListener implements MQListener {

    private EasyMsMQInfo easyMsMqInfo;
    private String key;
    private ConnectionFactory connectionFactory;
    private Connection connection;
    private boolean running;

    @Override
    public synchronized void start() {
        if (running) {
            logger.error("The MQ listener({}) is already running!", key);
            return;
        }

        Session session = null;
        try {
            connection = connectionFactory.createConnection();
            connection.start();
            session = connection.createSession(false, 2);
            Destination destination;
            if (DestinationType.QUEUE == easyMsMqInfo.getDestinationType()) {
                destination = session.createQueue(key);
            } else {
                destination = session.createTopic(key);
            }
            MessageConsumer consumer = session.createConsumer(destination);
            consumer.setMessageListener(message -> listen(
                    mqMessageInfoList -> {
                        if (message instanceof TextMessage) {
                            // 如果是String消息
                            TextMessage tm = (TextMessage) message;
                            receiveMQMessage(mqMessageInfoList, tm.getText());
                        } else if (message instanceof MapMessage) {
                            // 如果是Map消息
                            MapMessage mm = (MapMessage) message;
                            Map<String, Object> map = new HashMap<>(64);
                            Enumeration enums = mm.getMapNames();
                            while (enums.hasMoreElements()) {
                                String element = (String) enums.nextElement();
                                map.put(element, mm.getObject(element));
                            }
                            mqMessageInfoList.add(getMQMessageInfo(map));
                        } else if (message instanceof ObjectMessage) {
                            // 如果是Object消息
                            ObjectMessage om = (ObjectMessage) message;
                            mqMessageInfoList.add(getMQMessageInfo(om.getObject()));
                        } else if (message instanceof BytesMessage || message instanceof StreamMessage) {
                            List<Byte> byteList = new ArrayList<>();
                            byte[] byteArray = new byte[1024];
                            if (message instanceof BytesMessage) {
                                // 如果是bytes消息
                                BytesMessage bm = (BytesMessage) message;
                                while (bm.readBytes(byteArray) != -1) {
                                    for (byte b : byteArray) {
                                        byteList.add(b);
                                    }
                                }
                            } else {
                                // 如果是Stream消息
                                StreamMessage sm = (StreamMessage) message;
                                while (sm.readBytes(byteArray) != -1) {
                                    for (byte b : byteArray) {
                                        byteList.add(b);
                                    }
                                }
                            }
                            byte[] bytes = new byte[byteList.size()];
                            int index = 0;
                            for (Byte b : byteList) {
                                bytes[index++] = b;
                            }
                            mqMessageInfoList.add(getMQMessageInfo(SerializeUtil.unserialize(bytes)));
                        }
                    },
                    () -> {
                        try {
                            message.acknowledge();
                        } catch (JMSException e) {
                            logger.error("MQ消息acknowledge失败!", e);
                        }
                        return null;
                    },
                    e -> {
                        logger.error("接收目标({})MQ消息失败,消息内容:{}!", key, JSON.toJSONString(message), e);
                        return null;
                    })
            );
            running = true;
            logger.info("The MQ listener({}) destination type({}) start up success!", key, easyMsMqInfo.getDestinationType());
        } catch (Exception e) {
            logger.error("The MQ listener({}) destination type({}) start up failure!", key, easyMsMqInfo.getDestinationType(), e);
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e1) {
                    logger.error("The MQ listener({}) session close failure!", key, e1);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception e1) {
                    logger.error("The MQ listener({}) connection close failure!", key, e1);
                }
            }
        }
    }

    @Override
    public synchronized void stop() {
        if (!running) {
            logger.error("The MQ listener({}) is not started!", key);
            return;
        }
        try {
            connection.stop();
            running = false;
            logger.info("The MQ listener({}) shut down success!", key);
        } catch (Exception e) {
            logger.error("The MQ listener({}) shut down failure!", key, e);
        }
    }

    @Override
    public synchronized boolean isRunning() {
        return running;
    }

    public ActiveMQListener(ApplicationContext applicationContext, EasyMsMQProperties easyMsMQProperties,
                            EasyMsMQInfo easyMsMqInfo, String key) {
        super(easyMsMqInfo, key);
        this.connectionFactory = BasicActiveMQConnectionFactory.getActiveMQConnectionFactory(applicationContext, easyMsMQProperties);
        this.easyMsMqInfo = easyMsMqInfo;
        this.key = key;
    }
}
