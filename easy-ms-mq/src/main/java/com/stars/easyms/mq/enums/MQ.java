package com.stars.easyms.mq.enums;

/**
 * MQ的区分枚举
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
public enum MQ {

    /**
     * ActiveMQ
     */
    ACTIVEMQ("ActiveMQ"),

    /**
     * RocketMQ
     */
    ROCKETMQ("RocketMQ");

    private String code;

    public static MQ forCode(String code) {
        if (ACTIVEMQ.code.equalsIgnoreCase(code)) {
            return ACTIVEMQ;
        }
        return ROCKETMQ;
    }

    MQ(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
