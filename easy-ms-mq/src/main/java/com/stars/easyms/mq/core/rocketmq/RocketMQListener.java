package com.stars.easyms.mq.core.rocketmq;

import com.stars.easyms.mq.bean.EasyMsMQInfo;
import com.stars.easyms.mq.core.BaseMQListener;
import com.stars.easyms.mq.properties.EasyMsMQProperties;
import com.stars.easyms.mq.core.MQListener;
import com.stars.easyms.mq.registrar.EasyMsMQHolder;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.message.MessageExt;

import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * RocketMQ监听类
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
public final class RocketMQListener extends BaseMQListener implements MQListener {

    private DefaultMQPushConsumer consumer;
    private String nameServer;
    private String groupName;
    private String topic;
    private boolean running;

    @Override
    public synchronized void start() {
        if (running) {
            logger.error("The MQ listener({}) is already running!", topic);
            return;
        }

        consumer = new DefaultMQPushConsumer(groupName + topic);
        consumer.setNamesrvAddr(nameServer);
        consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_FIRST_OFFSET);
        consumer.setConsumeMessageBatchMaxSize(10);
        try {
            consumer.subscribe(topic, "*");
        } catch (MQClientException e) {
            logger.error("The MQ listener({}) subscribe failure!", topic, e);
        }
        consumer.registerMessageListener((List<MessageExt> msgs, ConsumeConcurrentlyContext context) -> listen(
                mqMessageInfoList -> {
                    for (MessageExt msg : msgs) {
                        receiveMQMessage(mqMessageInfoList, new String(msg.getBody(), StandardCharsets.UTF_8));
                    }
                },
                () -> ConsumeConcurrentlyStatus.CONSUME_SUCCESS,
                e -> {
                    logger.error("接收目标({})MQ消息失败,消息内容:{}!", topic, msgs, e);
                    return ConsumeConcurrentlyStatus.RECONSUME_LATER;
                })
        );
        try {
            consumer.start();
            running = true;
            logger.info("The MQ listener({}) start up success!", topic);
        } catch (MQClientException e) {
            logger.error("The MQ listener({}) start up failure!", topic, e);
        }
    }

    @Override
    public synchronized void stop() {
        if (!running) {
            logger.error("The MQ listener({}) is not started!", topic);
            return;
        }
        try {
            consumer.shutdown();
            running = false;
            logger.info("The MQ listener({}) shut down success!", topic);
        } catch (Exception e) {
            logger.error("The MQ listener({}) shut down failure!", topic, e);
        }
    }

    @Override
    public synchronized boolean isRunning() {
        return running;
    }

    public RocketMQListener(EasyMsMQProperties easyMsMQProperties, EasyMsMQInfo easyMsMqInfo, String topic) {
        super(easyMsMqInfo, topic);
        this.nameServer = easyMsMQProperties.getRocketmq().getNameServer();
        this.groupName = EasyMsMQHolder.getApplicationId() + "_CONSUMER_";
        this.topic = topic;
    }
}
