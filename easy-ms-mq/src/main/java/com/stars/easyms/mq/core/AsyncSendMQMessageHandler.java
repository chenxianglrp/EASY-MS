package com.stars.easyms.mq.core;

import com.stars.easyms.mq.bean.MQMessageInfo;
import com.stars.easyms.mq.enums.MQType;

import java.util.List;

/**
 * 异步发送mq消息，由于发送mq信息速度较慢因此采用异步的方式：先把数据批量存入数据库，然后采用线程池的方式批量发送MQ消息
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
final class AsyncSendMQMessageHandler extends BaseAsyncMQMessageHandler {

    private static final AsyncSendMQMessageHandler INSTANCE = new AsyncSendMQMessageHandler();

    static void handle(MQMessageInfo mqMessageInfo) {
        INSTANCE.add(mqMessageInfo);
    }

    @Override
    protected List<MQMessageInfo> filterRepeatMQMessageInfo(List<MQMessageInfo> list) {
        // 发送端不做重复校验
        return list;
    }

    @Override
    protected MQType mqType() {
        return MQType.SEND;
    }

    private AsyncSendMQMessageHandler() {
    }
}
