package com.stars.easyms.mq.service;

import com.stars.easyms.base.batch.BatchResult;
import com.stars.easyms.base.bean.LazyLoadBean;
import com.stars.easyms.datasource.batch.BatchCommit;
import com.stars.easyms.datasource.enums.DatabaseType;
import com.stars.easyms.mq.repository.dml.MQMessageDAO;
import com.stars.easyms.mq.bean.MQMessageInfo;
import com.stars.easyms.mq.enums.MQStatus;
import com.stars.easyms.mq.util.EasyMsMQPackageUtil;
import com.stars.easyms.base.util.ApplicationContextHolder;
import org.springframework.lang.NonNull;
import org.springframework.util.CollectionUtils;

import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

/**
 * MQ消息服务类
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
public final class MQMessageService {

    private static final MQMessageService INSTANCE = new MQMessageService();

    private final LazyLoadBean<MQMessageDAO> mqMessageDAO = new LazyLoadBean<>(this::getMQMessageDAO);

    private static final LazyLoadBean<BatchCommit> batchCommit = new LazyLoadBean<>(BatchCommit.class);

    private static final LazyLoadBean<String> insertMQMessageSqlId = new LazyLoadBean<>(
            () -> EasyMsMQPackageUtil.getRepositoryClassName() + "." + "insertMQMessage");

    private static final LazyLoadBean<String> updateMQMessageByPrimaryKeySqlId = new LazyLoadBean<>(
            () -> EasyMsMQPackageUtil.getRepositoryClassName() + "." + "updateMQMessageByPrimaryKey");

    public BatchResult<MQMessageInfo> batchInsertMQMessage(List<MQMessageInfo> mqMessageInfoList) {
        if (!mqMessageInfoList.isEmpty()) {
            Timestamp currTime = new Timestamp(System.currentTimeMillis());
            mqMessageInfoList.forEach(m -> {
                m.setStorageTime(currTime);
                m.setUpdateTime(currTime);
            });
            return batchCommit.getNonNullBean()
                    .batchInsert(insertMQMessageSqlId.getBean(), mqMessageInfoList);
        }
        return BatchResult.ofSuccess();
    }

    public BatchResult<MQMessageInfo> batchUpdateMQMessageByPrimaryKey(List<MQMessageInfo> mqMessageInfoList) {
        if (!mqMessageInfoList.isEmpty()) {
            Timestamp currTime = new Timestamp(System.currentTimeMillis());
            mqMessageInfoList.forEach(mqMessageInfo -> mqMessageInfo.setUpdateTime(currTime));
            return batchCommit.getNonNullBean()
                    .batchUpdate(updateMQMessageByPrimaryKeySqlId.getBean(), mqMessageInfoList);
        }
        return BatchResult.ofSuccess();
    }

    public BatchResult<MQMessageInfo> batchUpdateMQMessageStatusToInit(List<MQMessageInfo> mqMessageInfoList) {
        Timestamp currTime = new Timestamp(System.currentTimeMillis());
        mqMessageInfoList.forEach(mqMessageInfo -> {
            mqMessageInfo.setMqStatus(MQStatus.INIT);
            mqMessageInfo.setVersion(mqMessageInfo.getVersion() + 1);
            mqMessageInfo.setUpdateTime(currTime);
        });
        return batchCommit.getNonNullBean().batchUpdate(updateMQMessageByPrimaryKeySqlId.getBean(), mqMessageInfoList);
    }

    @NonNull
    public List<String> getMQMessageId(MQMessageInfo mqMessageInfo) {
        Map<String, Object> paramMap = new HashMap<>(2);
        paramMap.put("searchField", "messageIdField");
        paramMap.put("dto", mqMessageInfo);
        List<MQMessageInfo> resultList = mqMessageDAO.apply(m -> m.getMQMessage(paramMap));
        if (CollectionUtils.isEmpty(resultList)) {
            return Collections.emptyList();
        }
        return resultList.stream().map(MQMessageInfo::getMessageId).collect(Collectors.toList());
    }

    @NonNull
    public List<MQMessageInfo> getMQMessage(MQMessageInfo mqMessageInfo) {
        Map<String, Object> paramMap = new HashMap<>(2);
        paramMap.put("searchField", "wholeMessageField");
        paramMap.put("dto", mqMessageInfo);
        return mqMessageDAO.apply(m -> m.getMQMessage(paramMap));
    }

    @NonNull
    public List<String> getMQMessageId(MQMessageInfo mqMessageInfo, Timestamp startTime, Timestamp endTime) {
        Map<String, Object> paramMap = new HashMap<>(4);
        paramMap.put("searchField", "messageIdField");
        paramMap.put("dto", mqMessageInfo);
        paramMap.put("startTime", startTime);
        paramMap.put("endTime", endTime);
        List<MQMessageInfo> resultList = mqMessageDAO.apply(m -> m.getMQMessage(paramMap));
        if (CollectionUtils.isEmpty(resultList)) {
            return Collections.emptyList();
        }
        return resultList.stream().map(MQMessageInfo::getMessageId).collect(Collectors.toList());
    }

    @NonNull
    public List<MQMessageInfo> getMQMessage(MQMessageInfo mqMessageInfo, Timestamp startTime, Timestamp endTime) {
        Map<String, Object> paramMap = new HashMap<>(4);
        paramMap.put("searchField", "wholeMessageField");
        paramMap.put("dto", mqMessageInfo);
        paramMap.put("startTime", startTime);
        paramMap.put("endTime", endTime);
        return mqMessageDAO.apply(m -> m.getMQMessage(paramMap));
    }

    @NonNull
    public List<String> getMQMessageId(MQMessageInfo mqMessageInfo, List<String> messageIdList) {
        Map<String, Object> paramMap = new HashMap<>(4);
        paramMap.put("searchField", "messageIdField");
        paramMap.put("dto", mqMessageInfo);
        if (!CollectionUtils.isEmpty(messageIdList)) {
            paramMap.put("messageIdList", messageIdList);
        }
        List<MQMessageInfo> resultList = mqMessageDAO.apply(m -> m.getMQMessage(paramMap));
        if (CollectionUtils.isEmpty(resultList)) {
            return Collections.emptyList();
        }
        return resultList.stream().map(MQMessageInfo::getMessageId).collect(Collectors.toList());
    }

    public List<MQMessageInfo> getExistingMQMessageIdInDb(Map<String, Object> paramMap) {
        return mqMessageDAO.apply(m -> m.getExistingMQMessageId(paramMap));
    }

    public Long getMQMessageCount(Map<String, Object> paramMap) {
        return mqMessageDAO.apply(m -> m.getMQMessageCount(paramMap));
    }

    public List<MQMessageInfo> getMQMessageForFailRetry(Map<String, Object> paramMap) {
        return mqMessageDAO.apply(m -> m.getMQMessageForFailRetry(paramMap));
    }

    public List<MQMessageInfo> getMQMessageForInit(Map<String, Object> paramMap) {
        return mqMessageDAO.apply(m -> m.getMQMessageForInit(paramMap));
    }

    public List<String> getAllPendingArchivingMQMessageId(Map<String, Object> paramMap) {
        return mqMessageDAO.apply(m -> m.getAllPendingArchivingMQMessageId(paramMap));
    }

    public int insertMQMessageOldData(List<String> idList) {
        return mqMessageDAO.apply(m -> m.insertMQMessageOldData(idList));
    }

    public void deleteMQMessageOldData(List<String> idList) {
        mqMessageDAO.accept(m -> m.deleteMQMessageOldData(idList));
    }

    public List<MQMessageInfo> getCountWithStatusAndType(Map<String, Object> paramMap) {
        return mqMessageDAO.apply(m -> m.getCountWithStatusAndType(paramMap));
    }

    private MQMessageDAO getMQMessageDAO() {
        if (EasyMsMQPackageUtil.getDefaultDbType() == DatabaseType.ORACLE) {
            return (MQMessageDAO) ApplicationContextHolder.getApplicationContext().getBean("oracleMQMessageDAO");
        }
        return (MQMessageDAO) ApplicationContextHolder.getApplicationContext().getBean("mysqlMQMessageDAO");
    }

    public static MQMessageService getInstance() {
        return INSTANCE;
    }

    private MQMessageService() {}
}
