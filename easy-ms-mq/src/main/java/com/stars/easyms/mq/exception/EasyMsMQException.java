package com.stars.easyms.mq.exception;

import com.stars.easyms.base.util.ExceptionUtil;
import com.stars.easyms.base.util.MessageFormatUtil;

/**
 * MQ发送或接收时抛出的异常
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
public final class EasyMsMQException extends RuntimeException {

    private static final long serialVersionUID = -2034892190245466631L;

    public EasyMsMQException(String message, Object... args) {
        super(MessageFormatUtil.format(message, ExceptionUtil.trimmedCopy(args)), ExceptionUtil.extractThrowable(args));
    }
}
