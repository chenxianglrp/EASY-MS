package com.stars.easyms.mq.core;

import com.alibaba.fastjson.JSON;
import com.stars.easyms.base.alarm.EasyMsAlarmAssistor;
import com.stars.easyms.base.trace.EasyMsTraceHelper;
import com.stars.easyms.mq.bean.EasyMsMQInfo;
import com.stars.easyms.mq.bean.MQReceiveContext;
import com.stars.easyms.mq.bean.MQSendResult;
import com.stars.easyms.mq.constant.MQConstants;
import com.stars.easyms.mq.dto.MQSendMessageDTO;
import com.stars.easyms.mq.enums.MQStatus;
import com.stars.easyms.mq.exception.EasyMsMQException;
import com.stars.easyms.mq.registrar.EasyMsMQHolder;
import com.stars.easyms.mq.bean.MQMessageInfo;
import com.stars.easyms.mq.enums.MQType;
import com.stars.easyms.base.util.*;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/**
 * MQ消息调度者线程
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
@Slf4j
public final class MQScheduler {

    private static ThreadPoolExecutor mqSendAssorterThreadPool;

    private static ThreadPoolExecutor mqReceiveAssorterThreadPool;

    private static MQMessageSenderFactory mqMessageSenderFactory;

    static void schedule(MQType type, List<MQMessageInfo> mqMessageInfoList) {
        if (MQType.SEND == type) {
            if (mqSendAssorterThreadPool == null) {
                log.error(MQConstants.MQ_DISABLED);
                MQMessageStatusHandler.batchUpdateMQMessageStatus(mqMessageInfoList, MQConstants.MQ_DISABLED, MQStatus.FAIL_RETRY, null);
                return;
            }
            mqSendAssorterThreadPool.execute(new MQAssorter(type, mqMessageInfoList));
        } else {
            mqReceiveAssorterThreadPool.execute(new MQAssorter(type, mqMessageInfoList));
        }
    }

    public static void initMQSendSchedule(int mqSendChannelInfoMapSize) {
        int sendAssorterCorePoolSize = mqSendChannelInfoMapSize + 2;
        int sendAssorterMaxPoolSize = (mqSendChannelInfoMapSize + 1) * 3 + 2;
        mqSendAssorterThreadPool = new ThreadPoolExecutor(sendAssorterCorePoolSize, sendAssorterMaxPoolSize,
                30, TimeUnit.SECONDS, new LinkedBlockingQueue<>(10000),
                new DefaultThreadFactory().setNameFormat("easyms-mq-send-assorter-%d").build(),
                new ThreadPoolExecutor.CallerRunsPolicy());
        MQAssorter.mqSingleSendHandlerThreadPool = new ThreadPoolExecutor(
                sendAssorterCorePoolSize * 2, sendAssorterMaxPoolSize * 2,
                30, TimeUnit.SECONDS, new LinkedBlockingQueue<>(10000),
                new DefaultThreadFactory().setNameFormat("easyms-mq-single-send-handler-%d").build(),
                new ThreadPoolExecutor.CallerRunsPolicy());
        MQAssorter.mqBatchSendHandlerThreadPool = new ThreadPoolExecutor(
                sendAssorterCorePoolSize * 2, sendAssorterMaxPoolSize * 2,
                30, TimeUnit.SECONDS, new LinkedBlockingQueue<>(10000),
                new DefaultThreadFactory().setNameFormat("easyms-mq-batch-send-handler-%d").build(),
                new ThreadPoolExecutor.CallerRunsPolicy());
    }

    public static void initMQReceiveScheduler(int mqReceiveChannelInfoMapSize) {
        int receiveAssorterCorePoolSize = mqReceiveChannelInfoMapSize + 2;
        int receiveAssorterMaxPoolSize = (mqReceiveChannelInfoMapSize + 1) * 3 + 2;
        mqReceiveAssorterThreadPool = new ThreadPoolExecutor(receiveAssorterCorePoolSize,
                receiveAssorterMaxPoolSize, 30, TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(10000),
                new DefaultThreadFactory().setNameFormat("easyms-mq-receive-assorter-%d").build(),
                new ThreadPoolExecutor.CallerRunsPolicy());
        MQAssorter.mqSingleReceiveHandlerThreadPool = new ThreadPoolExecutor(
                receiveAssorterCorePoolSize * 2, receiveAssorterMaxPoolSize * 2,
                30, TimeUnit.SECONDS, new LinkedBlockingQueue<>(10000),
                new DefaultThreadFactory().setNameFormat("easyms-mq-single-receive-handler-%d").build(),
                new ThreadPoolExecutor.CallerRunsPolicy());
        MQAssorter.mqBatchReceiveHandlerThreadPool = new ThreadPoolExecutor(
                receiveAssorterCorePoolSize * 2, receiveAssorterMaxPoolSize * 2,
                30, TimeUnit.SECONDS, new LinkedBlockingQueue<>(10000),
                new DefaultThreadFactory().setNameFormat("easyms-mq-batch-receive-handler-%d").build(),
                new ThreadPoolExecutor.CallerRunsPolicy());
    }

    public static void setMqMessageSenderFactory(MQMessageSenderFactory mqMessageSenderFactory) {
        MQScheduler.mqMessageSenderFactory = mqMessageSenderFactory;
    }

    /**
     * MQ消息分类者线程
     */
    @Slf4j
    private static final class MQAssorter implements Runnable {

        private static ThreadPoolExecutor mqSingleSendHandlerThreadPool;

        private static ThreadPoolExecutor mqBatchSendHandlerThreadPool;

        private static ThreadPoolExecutor mqSingleReceiveHandlerThreadPool;

        private static ThreadPoolExecutor mqBatchReceiveHandlerThreadPool;

        private MQType type;

        private List<MQMessageInfo> mqMessageInfoList;

        @Override
        public void run() {
            // 根据MQ类型选择对应的线程池
            ExecutorService mqBatchHandlerThreadPool;
            if (MQType.SEND == type) {
                mqBatchHandlerThreadPool = mqBatchSendHandlerThreadPool;
            } else {
                mqBatchHandlerThreadPool = mqBatchReceiveHandlerThreadPool;
            }

            // 对list进行遍历分类
            Map<String, List<MQMessageInfo>> assortedMap = new HashMap<>(64);
            for (MQMessageInfo mqMessageInfo : mqMessageInfoList) {
                String currentKey = mqMessageInfo.getKey();
                // 判断发送是否使用批量
                EasyMsMQInfo easyMsMqInfo;
                if (MQType.SEND == type) {
                    easyMsMqInfo = EasyMsMQHolder.getMqSenderInfoMap().get(currentKey);
                } else {
                    easyMsMqInfo = EasyMsMQHolder.getMqReceiverInfoMap().get(currentKey);
                    if (easyMsMqInfo == null) {
                        log.error("{}[type:{}, key:{}]!", MQConstants.INVALID_MQ_KEY, type, currentKey);
                        MQMessageStatusHandler.updateMQMessageStatus(mqMessageInfo, MQConstants.INVALID_MQ_KEY, MQStatus.FAIL_RETRY, 1L);
                        continue;
                    }
                }
                boolean isBatch = false;
                Integer channelSize = 1;
                if (easyMsMqInfo != null) {
                    isBatch = easyMsMqInfo.getBatch();
                    channelSize = easyMsMqInfo.getChannelSize();
                }
                if (isBatch) {
                    List<MQMessageInfo> assortedList = assortedMap.get(currentKey);
                    if (assortedList == null) {
                        assortedList = new ArrayList<>();
                        assortedMap.put(currentKey, assortedList);
                    } else if (MQType.SEND == type && assortedList.size() == channelSize) {
                        List<MQMessageInfo> copyList = new ArrayList<>(channelSize);
                        copyList.addAll(assortedList);
                        assort(mqBatchHandlerThreadPool, currentKey, null, copyList);
                        assortedList.clear();
                    }
                    assortedList.add(mqMessageInfo);
                } else {
                    if (MQType.SEND == type) {
                        assort(mqSingleSendHandlerThreadPool, currentKey, mqMessageInfo, null);
                    } else {
                        assort(mqSingleReceiveHandlerThreadPool, currentKey, mqMessageInfo, null);
                    }
                }
            }
            assortedMap.forEach((currentKey, currentValue) ->
                    assort(mqBatchHandlerThreadPool, currentKey, null, currentValue));
        }

        private void assort(ExecutorService mqHandlerThreadPool, String currentKey,
                            MQMessageInfo mqMessageInfo, List<MQMessageInfo> mqMessageInfoList) {
            if (mqMessageInfo != null) {
                mqHandlerThreadPool.execute(new MQHandler(type, currentKey, mqMessageInfo));
            } else {
                mqHandlerThreadPool.execute(new MQHandler(type, currentKey, mqMessageInfoList));
            }
        }

        private MQAssorter(MQType type, List<MQMessageInfo> mqMessageInfoList) {
            this.type = type;
            this.mqMessageInfoList = mqMessageInfoList;
        }

        /**
         * MQ发送执行者
         */
        @Slf4j
        private static final class MQHandler implements Runnable {

            private MQType type;

            private String key;

            private MQMessageInfo mqMessageInfo;

            private List<MQMessageInfo> mqMessageInfoList;

            @Override
            @SneakyThrows
            public void run() {
                // 为每一个mq处理设置一个traceId，用作日志链路跟踪
                EasyMsTraceHelper.setTraceIdIfNecessary(t -> {
                    if (MQType.SEND == type) {
                        sendHandle();
                    } else {
                        receiveHandle();
                    }
                    return null;
                });
            }

            private void sendHandle() {
                EasyMsMQInfo easyMsMqInfo = EasyMsMQHolder.getMqSenderInfoMap().get(key);
                boolean currMessIsAllowNonPersistent = false;
                Long maxRetryCount = 5L;
                if (easyMsMqInfo != null) {
                    currMessIsAllowNonPersistent = easyMsMqInfo.getAllowNonPersistent();
                    maxRetryCount = easyMsMqInfo.getMaxRetryCount();
                }
                if (mqMessageInfo != null) {
                    String message = mqMessageInfo.getMessage();
                    String destinationType = mqMessageInfo.getDestinationType();
                    try {
                        MQMessageStatusHandler.updateMQMessageStatusToProcessing(mqMessageInfo);
                        MQSendResult sendResult = mqMessageSenderFactory.getMQMessageSender()
                                .convertAndSend(key, message, destinationType, currMessIsAllowNonPersistent);
                        if (sendResult.isSendSuccess()) {
                            if (log.isDebugEnabled()) {
                                log.debug("发送MQ消息到目标({})成功,消息模式:{},消息内容:{}", key, destinationType, message);
                            }
                            MQMessageStatusHandler.updateMQMessageStatus(mqMessageInfo, sendResult, MQStatus.SUCCESS, null);
                        } else {
                            log.error("发送MQ消息到目标({})失败,消息模式:{},消息内容:{}", key, destinationType, message);
                            MQMessageStatusHandler.updateMQMessageStatus(mqMessageInfo, sendResult, MQStatus.FAIL_RETRY, maxRetryCount);
                        }
                    } catch (Exception exception) {
                        log.error("发送MQ消息到目标({})失败,消息模式:{},消息内容:{}", key, destinationType, message, exception);
                        MQMessageStatusHandler.updateMQMessageStatusWithException(mqMessageInfo, exception, maxRetryCount);
                    }
                } else {
                    String message = JSON.toJSONString(
                            mqMessageInfoList.stream()
                                    .map(MQSendMessageDTO::of)
                                    .collect(Collectors.toList()));
                    String destinationType = mqMessageInfoList.get(0).getDestinationType();
                    try {
                        MQMessageStatusHandler.batchUpdateMQMessageStatusToProcessing(mqMessageInfoList);
                        MQSendResult sendResult = mqMessageSenderFactory.getMQMessageSender()
                                .convertAndSend(key, message, destinationType, currMessIsAllowNonPersistent);
                        if (sendResult.isSendSuccess()) {
                            if (log.isDebugEnabled()) {
                                log.debug("批量发送MQ消息到目标({})成功,消息模式:{},消息内容:{}", key, destinationType, message);
                            }
                            MQMessageStatusHandler.batchUpdateMQMessageStatus(mqMessageInfoList, sendResult, MQStatus.SUCCESS, null);
                        } else {
                            log.error("批量发送MQ消息到目标({})失败,消息模式:{},消息内容:{}", key, destinationType, message);
                            MQMessageStatusHandler.batchUpdateMQMessageStatus(mqMessageInfoList, sendResult, MQStatus.FAIL_RETRY, maxRetryCount);
                        }
                    } catch (Exception exception) {
                        log.error("批量发送MQ消息到目标({})失败,消息模式:{},消息内容:{}", key, destinationType, message, exception);
                        MQMessageStatusHandler.batchUpdateMQMessageStatusWithException(mqMessageInfoList, exception, maxRetryCount);
                    }
                }
            }

            private void receiveHandle() {
                EasyMsMQInfo easyMsMqInfo = EasyMsMQHolder.getMqReceiverInfoMap().get(key);
                Long maxRetryCount = easyMsMqInfo.getMaxRetryCount();
                Method method = easyMsMqInfo.getMethod();
                if (mqMessageInfo != null) {
                    String message = mqMessageInfo.getMessage();
                    try {
                        // 将消息内容转换成方法定义的类型
                        Object messageObj = JsonUtil.parseObject(message, easyMsMqInfo.getParameterType());

                        // 设置消息状态为处理中
                        MQMessageStatusHandler.updateMQMessageStatusToProcessing(mqMessageInfo);

                        // 为了防止重复消费此处使用threadLocal传递mqMessageInfo到执行方法事务内部修改消息状态为成功或失败
                        MQReceiveContext mqReceiveContext = MQReceiveContext.builder()
                                .mqMessageInfo(mqMessageInfo)
                                .maxRetryCount(maxRetryCount)
                                .build();
                        MQHolder.MQ_RECEIVE_CONTENT_THREAD_LOCAL.set(mqReceiveContext);

                        // 处理接收数据
                        Object obj = method.invoke(easyMsMqInfo.getInstance(), messageObj);
                        if (obj instanceof Boolean && Boolean.FALSE.equals(obj)) {
                            log.error("接收到目标({})MQ消息处理失败,消息内容:{}", key, message);
                            if (!mqReceiveContext.isUpdateStatusFinish()) {
                                MQMessageStatusHandler.updateMQMessageStatus(mqMessageInfo, MQConstants.RECEIVE_HANDLE_FAIL, MQStatus.FAIL_RETRY, maxRetryCount);
                            }
                            EasyMsAlarmAssistor.sendExceptionAlarmMessage(new EasyMsMQException("接收到目标({})MQ消息处理失败!", key));
                            return;
                        }
                        if (log.isDebugEnabled()) {
                            log.debug("接收到目标({})MQ消息处理成功,消息内容:{}", key, message);
                        }
                        if (!mqReceiveContext.isUpdateStatusFinish()) {
                            MQMessageStatusHandler.updateMQMessageStatus(mqMessageInfo, MQConstants.RECEIVE_HANDLE_SUCCESS, MQStatus.SUCCESS, null);
                        }
                    } catch (Exception exception) {
                        log.error("接收到目标({})MQ消息处理失败,消息内容:{}", key, message, exception);
                        MQMessageStatusHandler.updateMQMessageStatusWithException(mqMessageInfo, exception, maxRetryCount);
                        EasyMsAlarmAssistor.sendExceptionAlarmMessage(new EasyMsMQException("接收到目标({})MQ消息处理失败!", key, exception));
                    } finally {
                        MQHolder.MQ_RECEIVE_CONTENT_THREAD_LOCAL.remove();
                    }
                } else {
                    try {
                        Class<?> parameterGenericClass = GenericTypeUtil.getGenericClass(easyMsMqInfo.getParameterType(), 0);
                        ParameterizedType parameterType = GenericTypeUtil.getGenericType(easyMsMqInfo.getParameterType(), 0);
                        List<Object> paramList = new ArrayList<>();
                        mqMessageInfoList.forEach(currentMqMessageInfo ->
                                paramList.add(JsonUtil.parseObject(currentMqMessageInfo.getMessage(), parameterType, parameterGenericClass))
                        );
                        MQMessageStatusHandler.batchUpdateMQMessageStatusToProcessing(mqMessageInfoList);

                        // 为了防止重复消费此处使用threadLocal传递mqMessageInfo到执行方法事务内部修改消息状态为成功或失败
                        MQReceiveContext mqReceiveContext = MQReceiveContext.builder()
                                .mqMessageInfoList(mqMessageInfoList)
                                .maxRetryCount(maxRetryCount)
                                .build();
                        MQHolder.MQ_RECEIVE_CONTENT_THREAD_LOCAL.set(mqReceiveContext);

                        Object resultObj = method.invoke(easyMsMqInfo.getInstance(), paramList);
                        if (resultObj instanceof Boolean && Boolean.FALSE.equals(resultObj)) {
                            log.error("批量接收到目标({})MQ消息处理失败,消息内容:{}", key, paramList);
                            if (!mqReceiveContext.isUpdateStatusFinish()) {
                                MQMessageStatusHandler.batchUpdateMQMessageStatus(mqMessageInfoList, MQConstants.RECEIVE_HANDLE_FAIL, MQStatus.FAIL_RETRY, maxRetryCount);
                            }
                            EasyMsAlarmAssistor.sendExceptionAlarmMessage(new EasyMsMQException("批量接收到目标({})MQ消息处理失败!", key));
                            return;
                        }
                        if (log.isDebugEnabled()) {
                            log.debug("批量接收到目标({})MQ消息处理成功,消息内容:{}", key, paramList);
                        }
                        if (!mqReceiveContext.isUpdateStatusFinish()) {
                            MQMessageStatusHandler.batchUpdateMQMessageStatus(mqMessageInfoList, MQConstants.RECEIVE_HANDLE_SUCCESS, MQStatus.SUCCESS, null);
                        }
                    } catch (Exception exception) {
                        log.error("批量接收到目标({})MQ消息处理失败,消息内容:{}", key, mqMessageInfoList, exception);
                        MQMessageStatusHandler.batchUpdateMQMessageStatusWithException(mqMessageInfoList, exception, maxRetryCount);
                        EasyMsAlarmAssistor.sendExceptionAlarmMessage(new EasyMsMQException("批量接收到目标({})MQ消息处理失败!", key, exception));
                    } finally {
                        MQHolder.MQ_RECEIVE_CONTENT_THREAD_LOCAL.remove();
                    }
                }
            }

            private MQHandler(MQType type, String key, MQMessageInfo mqMessageInfo) {
                this.type = type;
                this.key = key;
                this.mqMessageInfo = mqMessageInfo;
            }

            private MQHandler(MQType type, String key, List<MQMessageInfo> mqMessageInfoList) {
                this.type = type;
                this.key = key;
                this.mqMessageInfoList = mqMessageInfoList;
            }
        }
    }
}