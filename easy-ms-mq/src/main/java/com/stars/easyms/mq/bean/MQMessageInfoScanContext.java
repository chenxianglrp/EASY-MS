package com.stars.easyms.mq.bean;

import lombok.Data;

/**
 * <p>className: MQMessageInfoScanContext</p>
 * <p>description: MQ的redis中的消息hscan扫描上下文</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/11 5:57 下午
 */
@Data
public class MQMessageInfoScanContext {

    private long scanCursorId;

    private long scanTime;

    /**
     * 为了防止在分布式场景下频繁调用，保存下上次扫描时间，距离上次调用间隔30秒
     */
    public boolean canScan() {
        return scanCursorId != 0 || scanTime < System.currentTimeMillis() - 30000;
    }

}
