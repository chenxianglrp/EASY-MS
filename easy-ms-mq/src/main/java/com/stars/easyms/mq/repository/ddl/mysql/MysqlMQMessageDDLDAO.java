package com.stars.easyms.mq.repository.ddl.mysql;

import com.stars.easyms.mq.annotation.MQRepository;
import com.stars.easyms.mq.repository.ddl.MQMessageDDLDAO;

/**
 * <p>className: MysqlMQMessageDDLDAO</p>
 * <p>description: Mysql版本的MQ数据库表ddl语句dao</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/17 10:11 上午
 */
@MQRepository("mysqlMQMessageDDLDAO")
public interface MysqlMQMessageDDLDAO extends MQMessageDDLDAO {

}
