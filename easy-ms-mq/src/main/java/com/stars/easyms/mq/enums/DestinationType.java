package com.stars.easyms.mq.enums;

/**
 * mq消息模式
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
public enum DestinationType {

    /**
     * 点对点，不可重复消费
     */
    QUEUE("Q"),

    /**
     * 发布/订阅，可以重复消费
     */
    TOPIC("T");

    private String code;

    DestinationType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
