package com.stars.easyms.mq.registrar;

import com.stars.easyms.mq.core.*;
import com.stars.easyms.mq.bean.EasyMsMQInfo;
import com.stars.easyms.mq.enums.MQType;
import com.stars.easyms.mq.exception.EasyMsMQException;
import com.stars.easyms.mq.properties.EasyMsMQProperties;
import com.stars.easyms.base.util.*;
import com.stars.easyms.mq.service.MQMessageDDLService;
import com.stars.easyms.mq.util.EasyMsMQUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ConfigurableApplicationContext;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * MQ服务初始化启动类
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
@SuppressWarnings("unchecked")
public final class MQManagerInitializer {

    private static final Logger logger = LoggerFactory.getLogger(MQManagerInitializer.class);

    private static AtomicBoolean init = new AtomicBoolean();

    private ConfigurableApplicationContext applicationContext;

    private EasyMsMQProperties easyMsMQProperties;

    /**
     * 有异常直接启动服务器失败
     */
    @PostConstruct
    private void init() {
        if (init.compareAndSet(false, true)) {
            if (!easyMsMQProperties.isEnabled()) {
                return;
            }
            logger.info("Begin to register MQ service......");
            try {
                initMQRepository();
                initMQManager();
                logger.info("Register MQ service success!");
            } catch (Exception e) {
                logger.error("Register MQ service fail!", e);
                System.exit(1);
            }
        }
    }

    private void initMQRepository() {
        // 初始化MQ的mapper
        MQRepositoryInitializer.init(applicationContext);

        // 判断表是否存在，若不存在则创建
        MQMessageDDLService.getInstance().initTable();
    }

    private void initMQManager() {
        if (EasyMsMQHolder.MQ_METHOD_INFO_MAP.isEmpty()) {
            if (logger.isDebugEnabled()) {
                logger.debug("Effective MQSender/MQReceiver method was not found！");
            }
            return;
        }

        for (EasyMsMQInfo easyMsMQInfo : EasyMsMQHolder.MQ_METHOD_INFO_MAP.values()) {
            String key = easyMsMQInfo.getSourceKey();
            Class<?> mqManagerClass = easyMsMQInfo.getMqManagerClass();
            String mqManagerClassName = mqManagerClass.getName();
            String mqManagerMethodName = easyMsMQInfo.getMethod().getName();
            String type = easyMsMQInfo.getAnnotationClass().getSimpleName();
            if (EasyMsMQUtil.isElExpression(key)) {
                String sourceKey = key;
                key = easyMsMQProperties.getKey().get(EasyMsMQUtil.getReplaceKey(key));
                if (StringUtils.isBlank(key)) {
                    throw new EasyMsMQException("MQ manager ({}) method({}) annotation({}) key '{}' didn't find in config!",
                            mqManagerClassName, mqManagerMethodName, type, sourceKey);
                }
            }
            if (StringUtils.isBlank(key)) {
                throw new EasyMsMQException("MQ manager({}) method({}) annotation({}) key is blank!",
                        mqManagerClassName, mqManagerMethodName, type);
            }

            // 初始化key
            Map<String, EasyMsMQInfo> easyMsMQInfoMap =
                    easyMsMQInfo.getType() == MQType.RECEIVE ? EasyMsMQHolder.MQ_RECEIVER_INFO_MAP : EasyMsMQHolder.MQ_SENDER_INFO_MAP;
            if (easyMsMQInfoMap.containsKey(key)) {
                throw new EasyMsMQException("MQ manager({}) method({}) annotation({}) key({}) already exist in the class({}) method({})!",
                        mqManagerClassName, mqManagerMethodName, type, key, easyMsMQInfoMap.get(key).getMqManagerClass().getName(),
                        easyMsMQInfoMap.get(key).getMethod().getName());
            }
            easyMsMQInfo.setStaticKey(key);
            if (easyMsMQInfo.isDynamicKey()) {
                key = StringUtils.join(new Object[]{key, SpringBootUtil.getActiveProfile()}, "_");
            }
            easyMsMQInfo.setKey(key);
            easyMsMQInfo.setInstance(ApplicationContextHolder.getApplicationContext().getBean(mqManagerClass));

            // 如果执行类存在则获取其spring容器中的对象，如果不存在则报错
            Class<?> executeClass = easyMsMQInfo.getExecuteClass();
            if (executeClass != null) {
                easyMsMQInfo.setExecuteInstance(ApplicationContextHolder.getApplicationContext().getBean(executeClass));
            }

            registerMultichannel(easyMsMQInfoMap, key, easyMsMQInfo);

            // 记录日志信息
            logger.info("Found MQ service: {}:{} ==> {}.{}()", type, key, mqManagerClassName, mqManagerMethodName);
        }

        // 启动MQ调度器
        MQScheduler.setMqMessageSenderFactory(new MQMessageSenderFactory(applicationContext, easyMsMQProperties));
        MQScheduler.initMQSendSchedule(EasyMsMQHolder.MQ_SENDER_INFO_MAP.size());

        // 创建MQ调度执行线程
        ExecutorService scheduleThreadPool = new ThreadPoolExecutor(2, 2, 0, TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(), new DefaultThreadFactory().setNameFormat("easy-ms-mq-scheduler-pool-%d").build());

        // 启动MQ失败重试调度器
        scheduleThreadPool.execute(new MQFailRetryScheduler(MQType.SEND));

        // 启动MQ清理调度器
        scheduleThreadPool.execute(MQMigratorScheduler.createInstance());

        // 初始化MQ的高可用方案
        MQHighAvailability.init();
        MQMonitor.init();
    }

    public static boolean isInit() {
        return init.get();
    }

    private void registerMultichannel(Map<String, EasyMsMQInfo> easyMsMQInfoMap, String key, EasyMsMQInfo easyMsMQInfo) {
        easyMsMQInfoMap.put(key, easyMsMQInfo);
        int channelCount = easyMsMQInfo.getChannelCount();
        if (channelCount > 1) {
            for (int i = 1; i <= channelCount; i++) {
                String multichannelKey = StringUtils.join(
                        new Object[]{key, StringUtils.leftPad(String.valueOf(i), 3, "0")}, "-");
                easyMsMQInfoMap.put(multichannelKey, easyMsMQInfo);
            }
        }
    }

    public MQManagerInitializer(ConfigurableApplicationContext applicationContext, EasyMsMQProperties easyMsMQProperties) {
        this.applicationContext = applicationContext;
        this.easyMsMQProperties = easyMsMQProperties;
    }
}
