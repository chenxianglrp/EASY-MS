package com.stars.easyms.mq.scan;

/**
 * <p>className: EasyMsMQManagerScan</p>
 * <p>description: EasyMsMQ扫描包注册类</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/4/12 8:35 下午
 */
public interface EasyMsMQManagerScan {

    String[] scanPackages();

}
