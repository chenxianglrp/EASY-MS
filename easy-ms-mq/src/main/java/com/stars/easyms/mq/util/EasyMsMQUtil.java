package com.stars.easyms.mq.util;

/**
 * <p>className: EasyMsMQUtil</p>
 * <p>description: EasyMsMQ工具类</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/11/30 7:47 下午
 */
public final class EasyMsMQUtil {

    public static boolean isElExpression(String key) {
        return key != null && key.startsWith("${") && key.endsWith("}");
    }

    public static String getReplaceKey(String key) {
        if (key.startsWith("${") && key.endsWith("}")) {
            key = key.substring(2, key.length() - 1);
        }
        return key;
    }

    private EasyMsMQUtil() {}
}
