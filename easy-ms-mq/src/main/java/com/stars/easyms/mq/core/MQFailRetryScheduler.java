package com.stars.easyms.mq.core;

import com.stars.easyms.base.batch.BatchResult;
import com.stars.easyms.base.util.ThreadUtil;
import com.stars.easyms.mq.constant.MQConstants;
import com.stars.easyms.mq.bean.MQMessageInfo;
import com.stars.easyms.mq.enums.MQStatus;
import com.stars.easyms.mq.enums.MQType;
import com.stars.easyms.mq.registrar.EasyMsMQHolder;
import com.stars.easyms.mq.service.MQMessageService;
import com.stars.easyms.redis.exception.DistributedLockTimeoutException;
import com.stars.easyms.redis.lock.DistributedLock;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * <p>className: MQFailRetryScheduler</p>
 * <p>description: MQ失败重试调度器</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/4/28 7:27 下午
 */
@Slf4j
public class MQFailRetryScheduler implements Runnable {

    private static final AtomicInteger LOOP_EXECUTE_EXCEPTION_COUNT = new AtomicInteger();

    private final MQType mqType;

    private final DistributedLock schedulerLock;

    @Override
    public void run() {
        // 设置固定参数
        Map<String, Object> paramMap = new HashMap<>(8);
        paramMap.put("failRetryStatus", MQStatus.FAIL_RETRY.getCode());
        paramMap.put("perCatchSize", MQConstants.PER_CATCH_SIZE);
        paramMap.put("type", mqType.getCode());
        paramMap.put(MQType.SEND == mqType ? "sourceSys" : "destSys", EasyMsMQHolder.getApplicationId());

        // 循环执行mq调度任务,当失败次数过多时停止执行
        boolean isNeedPreQuery = true;
        while (LOOP_EXECUTE_EXCEPTION_COUNT.get() < MQConstants.MAX_SCHEDULE_EXCEPTION_COUNT) {
            try {
                // 1.抓取待发送或发送失败的数据
                long catchCount = catchAndScheduleData(paramMap, isNeedPreQuery);

                // 2.如果抓取数量和最大抓取数量相同则没有等待时间，否则等待30秒
                if (catchCount >= 0 && catchCount < MQConstants.PER_CATCH_SIZE) {
                    isNeedPreQuery = true;
                    ThreadUtil.sleep(30 + ThreadLocalRandom.current().nextLong(5), TimeUnit.SECONDS);
                } else if (catchCount == -1) {
                    isNeedPreQuery = true;
                    ThreadUtil.sleep(1500);
                } else {
                    isNeedPreQuery = false;
                }
            } catch (Exception e) {
                log.error("MQFailRetryScheduler execute failure!", e);
                LOOP_EXECUTE_EXCEPTION_COUNT.incrementAndGet();
            }
        }
    }

    private long catchAndScheduleData(Map<String, Object> paramMap, boolean isNeedPreQuery) {
        boolean flag = true;
        try {
            // 1.为了防止分布式服务器同时读取同一份数据,因此需要加锁,如果加锁失败则每隔一秒钟重新加锁
            schedulerLock.blockLock(1200000, 1000);

            // 2.判断是否需要预查询
            Long preQueryCount = null;
            if (isNeedPreQuery) {
                preQueryCount = MQMessageService.getInstance().getMQMessageCount(paramMap);
            }

            // 3.根据不同的类型获取对应的消息list
            long catchCount = 0;
            if (preQueryCount == null || preQueryCount > 0) {
                paramMap.put("currTime", new Timestamp(System.currentTimeMillis()));
                List<MQMessageInfo> mqMessageInfoList = MQMessageService.getInstance().getMQMessageForFailRetry(paramMap);
                if (mqMessageInfoList != null && (catchCount = mqMessageInfoList.size()) > 0) {
                    // 4.把这些数据的状态改成初始化状态
                    BatchResult<MQMessageInfo> batchResult =
                            MQMessageService.getInstance().batchUpdateMQMessageStatusToInit(mqMessageInfoList);

                    /*
                     * 5.修改成功的数据分配任务给线程池，执行任务
                     * 如果此时服务器宕机或重启等其他原因会造成未将数据同步到redis中造成数据丢失
                     * 因此需要对这些丢失的数据重新处理，@see MQHighAvailability.handleFailRetryMissingData
                     */
                    handleFailRetryData(batchResult.getSuccessDatas(), mqType);
                }
            }
            return catchCount;
        } catch (DistributedLockTimeoutException e) {
            flag = false;
            log.error(e.getMessage());
        } finally {
            if (flag) {
                schedulerLock.unlock();
            }
        }
        return -1;
    }

    static void handleFailRetryData(@NonNull List<MQMessageInfo> mqMessageInfoList, MQType mqType) {
        if (!mqMessageInfoList.isEmpty()) {
            mqMessageInfoList.forEach(mqMessageInfo -> {
                mqMessageInfo.setLastLiveTime(System.currentTimeMillis());
                mqMessageInfo.setPersistent(true);
                MQHolder.getEasyMsMQRedisManager()
                        .setMQMessageInfoToRedis(mqType.getName(), mqMessageInfo.getMessageId(), mqMessageInfo);
                if (MQType.SEND == mqType) {
                    AsyncSendMQMessageHandler.handle(mqMessageInfo);
                } else {
                    AsyncReceiveMQMessageHandler.handle(mqMessageInfo);
                }
            });
        }
    }

    public MQFailRetryScheduler(MQType mqType) {
        this.mqType = mqType;
        this.schedulerLock = new DistributedLock(MQConstants.MQ_SCHEDULER_LOCK,
                EasyMsMQHolder.getApplicationId() + ":" + mqType.getCode(), 120);
    }
}
