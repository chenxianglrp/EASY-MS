package com.stars.easyms.mq.enums;

/**
 * mq发送类型
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
public enum MQType {

    /**
     * 发送
     */
    SEND("S", "send", "发送端"),

    /**
     * 接收
     */
    RECEIVE("R", "receive", "消费端");

    private String code;

    private String name;

    private String chineseName;

    MQType(String code, String name, String chineseName) {
        this.code = code;
        this.name = name;
        this.chineseName = chineseName;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getChineseName() {
        return chineseName;
    }

    public static MQType forCode(String type) {
        if (MQType.SEND.code.equals(type)) {
            return MQType.SEND;
        } else if (MQType.RECEIVE.code.equals(type)) {
            return MQType.RECEIVE;
        }
        return null;
    }
}
