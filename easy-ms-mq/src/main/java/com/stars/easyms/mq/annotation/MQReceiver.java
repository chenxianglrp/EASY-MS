package com.stars.easyms.mq.annotation;

import com.stars.easyms.mq.enums.DestinationType;
import com.stars.easyms.mq.enums.RedeliveryPolicy;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.lang.annotation.*;

/**
 * 提供mq接收服务的方法注解:
 * 接收的整个方法作为一个大的事务处理，此注解增加@Transactional注解依赖
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
public @interface MQReceiver {

    /**
     * MQ接收消息的key
     */
    String key();

    /**
     * key所对应的中文名称
     */
    String name() default "";

    /**
     * QUEUE:点对点，不可重复消费
     * TOPIC:发布/订阅，可以重复消费
     */
    DestinationType destinationType() default DestinationType.TOPIC;

    /**
     * 是否批量处理数据，默认是
     */
    boolean batch() default true;

    /**
     * 通道数量，就是一个相同的KEY使用几个通道发送与接收，发送端和接收端的数值必须一致
     */
    int channelCount() default 1;

    /**
     * 最大重试次数，默认5
     */
    long maxRetryCount() default 5;

    /**
     * 消息重新处理机制
     */
    RedeliveryPolicy redeliveryPolicy() default RedeliveryPolicy.EXPONENTIAL;

    /**
     * 消息重发时间间隔(单位:秒)
     */
    long redeliveryDelay() default 60;

    /**
     * key值是否随着环境变化(默认true，如果是true的话key值结尾会增加"_${spring.profiles.active}")
     * 该功能主要用于多个环境共用一个MQ环境的情况，若不想使用该功能，可以手动在配置里的easy-ms.mq.key中添加用来区分不同环境
     */
    boolean isDynamicKey() default true;

    /**
     * 执行类
     */
    Class<?> executeClass() default MQManager.class;

    /**
     * 执行方法
     */
    String executeMethod() default "";

}
