package com.stars.easyms.mq.util;

import com.stars.easyms.datasource.EasyMsDataSourceFactory;
import com.stars.easyms.datasource.enums.DatabaseType;
import org.apache.commons.lang3.StringUtils;

/**
 * 包路径相关工具类
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
public final class EasyMsMQPackageUtil {

    private static volatile String repositoryPackageName;

    private static volatile String repositoryDdlPackage;

    private static volatile String repositoryDmlPackage;

    private static volatile String repositoryClassName;

    private static DatabaseType databaseType;

    /**
     * 获取存放持久化仓库包的路径
     */
    public static String getRepositoryDdlPackageName() {
        if (repositoryDdlPackage == null) {
            repositoryDdlPackage = getRepositoryPackageName() + ".ddl";
        }
        return repositoryDdlPackage;
    }

    /**
     * 获取存放持久化仓库包的路径
     */
    public static String getRepositoryDmlPackageName() {
        if (repositoryDmlPackage == null) {
            repositoryDmlPackage = getRepositoryPackageName() + ".dml";
        }
        return repositoryDmlPackage;
    }

    /**
     * 获取存放持久化仓库类的路径
     */
    public static String getRepositoryClassName() {
        if (repositoryClassName == null) {
            String dbType = getDefaultDbType().getDbType();
            repositoryClassName = getRepositoryDmlPackageName() + "." + dbType + "." + StringUtils.capitalize(dbType) + "MQMessageDAO";
        }
        return repositoryClassName;
    }

    /**
     * 获取默认数据库类型
     */
    public static DatabaseType getDefaultDbType() {
        if (databaseType == null) {
            databaseType = EasyMsDataSourceFactory.getEasyMsMultiDataSource().getDefaultDataSource().getDatabaseType();
        }
        return databaseType;
    }

    /**
     * 获取存放持久化仓库包的路径
     */
    private static String getRepositoryPackageName() {
        if (repositoryPackageName == null) {
            // 获取分布式调度框架的根路径
            String currentPackage = EasyMsMQPackageUtil.class.getPackage().getName();
            String rootPackageName = currentPackage.substring(0, currentPackage.lastIndexOf('.'));
            repositoryPackageName = rootPackageName + ".repository";
        }
        return repositoryPackageName;
    }

    private EasyMsMQPackageUtil() {
    }
}
