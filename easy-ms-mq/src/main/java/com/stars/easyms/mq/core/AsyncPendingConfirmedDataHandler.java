package com.stars.easyms.mq.core;

import com.stars.easyms.base.asynchronous.BaseBatchAsynchronousTask;
import com.stars.easyms.base.batch.BatchResult;
import com.stars.easyms.mq.bean.MQMessageInfo;
import com.stars.easyms.mq.constant.MQConstants;
import com.stars.easyms.mq.enums.MQStatus;
import com.stars.easyms.mq.enums.MQType;
import com.stars.easyms.mq.registrar.EasyMsMQHolder;
import com.stars.easyms.mq.service.MQMessageService;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 异步从redis中删除mq消息
 *
 * @author guoguifang
 * @date 2021-04-23 13:54
 * @since 1.8.0
 */
final class AsyncPendingConfirmedDataHandler extends BaseBatchAsynchronousTask<MQMessageInfo> {

    private static final long MAX_HANDLE_COUNT = 10000;

    private static final AsyncPendingConfirmedDataHandler SEND_INSTANCE = new AsyncPendingConfirmedDataHandler(MQType.SEND);

    private static final AsyncPendingConfirmedDataHandler RECEIVE_INSTANCE = new AsyncPendingConfirmedDataHandler(MQType.RECEIVE);

    private final MQType mqType;

    @Override
    protected BatchResult execute(List<MQMessageInfo> mqMessageInfoList) {
        // 为了保证消息能百分百的全部保存在数据库中，只有确认数据库中数据正常保存才删除redis中的数据
        handlePendingConfirmedData(mqMessageInfoList);
        return BatchResult.ofSuccess();
    }

    /**
     * 处理待确认的数据：为了保证数据的安全性，因此需要对一些redis中丢失的数据进行再次确认，只有确认已持久化才可以在redis中删除
     */
    private void handlePendingConfirmedData(List<MQMessageInfo> mqMessageInfoList) {
        List<MQMessageInfo> pendingInsertMQMessageInfoList = new ArrayList<>();
        List<MQMessageInfo> pendingUpdateMQMessageInfoList = new ArrayList<>();
        Set<String> pendingDeleteMQMessageIdSet = new HashSet<>();

        // 先需要判断是否已经存储在数据库中,批量查询,每次最多查询100条
        Stream.iterate(0, n -> n + 1)
                .limit((mqMessageInfoList.size() + MQConstants.MAX_HANDLE_COUNT_PER_TIME - 1) / MQConstants.MAX_HANDLE_COUNT_PER_TIME)
                .forEach(i -> {

                    // 由于查询数据库的操作比较耗时，在高并发或大数据量的情况下可能造成数据堆积，
                    // 如果查询时间过长容易造成数据长时间的无法入库，因此采用分批次的方式逐步入库
                    List<MQMessageInfo> pageMQMessageInfoInRedisList = mqMessageInfoList.stream()
                            .skip(i * MQConstants.MAX_HANDLE_COUNT_PER_TIME)
                            .limit(MQConstants.MAX_HANDLE_COUNT_PER_TIME)
                            .collect(Collectors.toList());

                    // 批量查询数据库并获取messageId以及对应版本号
                    Map<String, Long> existingMQMessageIdInDbMap = new HashMap<>(MQConstants.MAX_HANDLE_COUNT_PER_TIME);
                    Map<String, Object> paramMap = new HashMap<>(4);
                    paramMap.put("type", mqType.getCode());
                    if (MQType.SEND != mqType) {
                        paramMap.put("destSys", EasyMsMQHolder.getApplicationId());
                    }
                    paramMap.put("messageIdList",
                            pageMQMessageInfoInRedisList.stream()
                                    .map(MQMessageInfo::getMessageId)
                                    .collect(Collectors.toList()));
                    MQMessageService.getInstance()
                            .getExistingMQMessageIdInDb(paramMap)
                            .forEach(m -> existingMQMessageIdInDbMap.put(m.getMessageId(), m.getVersion()));

                    pageMQMessageInfoInRedisList.forEach(mqMessageInfoInRedis -> {
                        // 获取messageId，并通过messageId获取数据库中的版本号
                        String messageId = mqMessageInfoInRedis.getMessageId();
                        Long versionInDb = existingMQMessageIdInDbMap.get(messageId);
                        // 如果数据库中存在，且数据库的版本号大于等于redis中保存的版本号则表示redis中数据过时，则需要放入待删除redis列表
                        if (versionInDb != null && versionInDb >= mqMessageInfoInRedis.getVersion()) {
                            pendingDeleteMQMessageIdSet.add(messageId);
                            deleteMQMessageFromRedis(pendingDeleteMQMessageIdSet, false);
                        } else if (versionInDb == null) {
                            // 将数据库中不存在的数据进行重新处理或插入数据库
                            handleData(mqMessageInfoInRedis, pendingInsertMQMessageInfoList);
                            batchInsertMQMessage(pendingInsertMQMessageInfoList, false);
                        } else {
                            // 如果数据库中存在，且数据库的版本号小于redis中保存的版本号则需要重新处理或修改数据库
                            handleData(mqMessageInfoInRedis, pendingUpdateMQMessageInfoList);
                            batchUpdateMQMessageByPrimaryKey(pendingUpdateMQMessageInfoList, false);
                        }
                    });
                });

        // 完成待插入、待修改、待删除操作
        batchInsertMQMessage(pendingInsertMQMessageInfoList, true);
        batchUpdateMQMessageByPrimaryKey(pendingUpdateMQMessageInfoList, true);
        deleteMQMessageFromRedis(pendingDeleteMQMessageIdSet, true);
    }

    private void handleData(MQMessageInfo mqMessageInfoInRedis, List<MQMessageInfo> pendingList) {
        // 如果非初始化、处理中状态则放入待处理列表
        MQStatus mqStatus = mqMessageInfoInRedis.getMqStatus();
        if (MQStatus.INIT != mqStatus && MQStatus.PROCESSING != mqStatus) {
            pendingList.add(mqMessageInfoInRedis);
            return;
        }

        // 如果初始化、处理中状态且最后存活时间已经超过120秒的数据则认为该数据已经被丢失则需要重新初始化并发送或接收
        // 重置调度时间后存入redis并放入异步调度继续发送MQ消息
        mqMessageInfoInRedis.setLastLiveTime(System.currentTimeMillis());
        mqMessageInfoInRedis.setScheduleFailTimes(mqMessageInfoInRedis.getScheduleFailTimes() + 1);
        mqMessageInfoInRedis.setMqStatus(MQStatus.INIT);
        mqMessageInfoInRedis.setVersion(mqMessageInfoInRedis.getVersion() + 1);

        // 重新调度
        MQHolder.getEasyMsMQRedisManager()
                .setMQMessageInfoToRedis(mqType.getName(), mqMessageInfoInRedis.getMessageId(), mqMessageInfoInRedis);
        if (MQType.SEND == mqType) {
            AsyncSendMQMessageHandler.handle(mqMessageInfoInRedis);
        } else {
            AsyncReceiveMQMessageHandler.handle(mqMessageInfoInRedis);
        }
    }

    private void batchInsertMQMessage(List<MQMessageInfo> insertList, boolean isForce) {
        if ((isForce && !insertList.isEmpty()) || insertList.size() > MQConstants.MAX_HANDLE_COUNT_PER_TIME) {
            insertList.forEach(AsyncInsertMQMessageInfoHandler::handle);
            insertList.clear();
        }
    }

    private void batchUpdateMQMessageByPrimaryKey(List<MQMessageInfo> updateList, boolean isForce) {
        if ((isForce && !updateList.isEmpty()) || updateList.size() > MQConstants.MAX_HANDLE_COUNT_PER_TIME) {
            updateList.forEach(AsyncUpdateMQMessageInfoHandler::handle);
            updateList.clear();
        }
    }

    private void deleteMQMessageFromRedis(Set<String> deleteSet, boolean isForce) {
        if ((isForce && !deleteSet.isEmpty()) || deleteSet.size() > MQConstants.MAX_HANDLE_COUNT_PER_TIME) {
            MQHolder.getEasyMsMQRedisManager().deleteMQMessageInfoFromRedis(mqType.getName(), deleteSet);
            deleteSet.clear();
        }
    }

    static void handle(List<MQMessageInfo> mqMessageInfoList, MQType mqType) {
        if (MQType.SEND == mqType) {
            mqMessageInfoList.forEach(SEND_INSTANCE::add);
        } else {
            mqMessageInfoList.forEach(RECEIVE_INSTANCE::add);
        }
    }

    static void handle(MQMessageInfo mqMessageInfo) {
        if (MQType.SEND == mqMessageInfo.getMqType()) {
            SEND_INSTANCE.add(mqMessageInfo);
        } else {
            RECEIVE_INSTANCE.add(mqMessageInfo);
        }
    }

    static boolean isExceedMaxHandleCount() {
        return SEND_INSTANCE.getQueueCount() + RECEIVE_INSTANCE.getQueueCount() > MAX_HANDLE_COUNT;
    }

    private AsyncPendingConfirmedDataHandler(MQType mqType) {
        this.mqType = mqType;
    }
}
