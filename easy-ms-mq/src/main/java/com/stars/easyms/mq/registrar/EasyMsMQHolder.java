package com.stars.easyms.mq.registrar;

import com.stars.easyms.base.util.SpringBootUtil;
import com.stars.easyms.mq.bean.EasyMsMQInfo;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * <p>className: EasyMsMQHolder</p>
 * <p>description: EasyMsMQ状态及信息Holer类</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/4/14 11:06 上午
 */
public final class EasyMsMQHolder {

    private static String applicationId;

    static final Map<Method, EasyMsMQInfo> MQ_METHOD_INFO_MAP = new ConcurrentHashMap<>(128);

    static final Map<String, EasyMsMQInfo> MQ_SENDER_INFO_MAP = new ConcurrentHashMap<>(128);

    static final Map<String, EasyMsMQInfo> MQ_RECEIVER_INFO_MAP = new ConcurrentHashMap<>(128);

    public static void setApplicationId(String applicationId) {
        if (EasyMsMQHolder.applicationId == null) {
            EasyMsMQHolder.applicationId = applicationId;
        }
    }

    @NonNull
    public static String getApplicationId() {
        if (applicationId == null) {
            String springBootApplicationName = SpringBootUtil.getApplicationName();
            Assert.notNull(springBootApplicationName, "SpringBoot application name not configured!");
            return springBootApplicationName.toLowerCase();
        }
        return applicationId;
    }

    @Nullable
    public static EasyMsMQInfo getEasyMsMQInfo(Method method) {
        return MQ_METHOD_INFO_MAP.get(method);
    }

    @NonNull
    public static Map<String, EasyMsMQInfo> getMqSenderInfoMap() {
        return Collections.unmodifiableMap(MQ_SENDER_INFO_MAP);
    }

    @NonNull
    public static Map<String, EasyMsMQInfo> getMqReceiverInfoMap() {
        return Collections.unmodifiableMap(MQ_RECEIVER_INFO_MAP);
    }

    private EasyMsMQHolder() {
    }
}
