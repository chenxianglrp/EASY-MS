package com.stars.easyms.mq.core.activemq;

import com.stars.easyms.mq.core.activemq.template.JmsNonPersistentTemplate;
import com.stars.easyms.mq.core.activemq.template.JmsQueueTemplate;
import com.stars.easyms.mq.core.activemq.template.JmsTopicTemplate;
import com.stars.easyms.mq.enums.DestinationType;
import com.stars.easyms.mq.properties.EasyMsMQProperties;

import javax.jms.ConnectionFactory;

/**
 * MQ监听器工厂类
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
final class ActiveMQTemplateFactory {

    private JmsQueueTemplate jmsQueueTemplate;

    private JmsTopicTemplate jmsTopicTemplate;

    private JmsNonPersistentTemplate jmsNonPersistentTemplate;

    ActiveMQTemplate getActiveMQTemplate(ConnectionFactory connectionFactory, EasyMsMQProperties easyMsMQProperties, String destinationType, Boolean currMessIsAllowNonPersistent) {
        if (DestinationType.TOPIC.getCode().equals(destinationType)) {
            return getJmsTopicTemplate(connectionFactory);
        } else {
            if (currMessIsAllowNonPersistent != null && currMessIsAllowNonPersistent && easyMsMQProperties.getActivemq().isAllowNonPersistent()) {
                return getJmsNonPersistentTemplate(connectionFactory);
            } else {
                return getJmsQueueTemplate(connectionFactory);
            }
        }
    }

    private JmsTopicTemplate getJmsTopicTemplate(ConnectionFactory connectionFactory) {
        JmsTopicTemplate localJmsTopicTemplate = jmsTopicTemplate;
        if (localJmsTopicTemplate == null) {
            synchronized (this) {
                localJmsTopicTemplate = jmsTopicTemplate;
                if (localJmsTopicTemplate == null) {
                    jmsTopicTemplate = localJmsTopicTemplate = new JmsTopicTemplate(connectionFactory);
                }
            }
        }
        return localJmsTopicTemplate;
    }

    private JmsQueueTemplate getJmsQueueTemplate(ConnectionFactory connectionFactory) {
        JmsQueueTemplate localJmsQueueTemplate = jmsQueueTemplate;
        if (localJmsQueueTemplate == null) {
            synchronized (this) {
                localJmsQueueTemplate = jmsQueueTemplate;
                if (localJmsQueueTemplate == null) {
                    jmsQueueTemplate = localJmsQueueTemplate = new JmsQueueTemplate(connectionFactory);
                }
            }
        }
        return localJmsQueueTemplate;
    }

    private JmsNonPersistentTemplate getJmsNonPersistentTemplate(ConnectionFactory connectionFactory) {
        JmsNonPersistentTemplate localJmsNonPersistentTemplate = jmsNonPersistentTemplate;
        if (localJmsNonPersistentTemplate == null) {
            synchronized (this) {
                localJmsNonPersistentTemplate = jmsNonPersistentTemplate;
                if (localJmsNonPersistentTemplate == null) {
                    jmsNonPersistentTemplate = localJmsNonPersistentTemplate = new JmsNonPersistentTemplate(connectionFactory);
                }
            }
        }
        return localJmsNonPersistentTemplate;
    }
}
