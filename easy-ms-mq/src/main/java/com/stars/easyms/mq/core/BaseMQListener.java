package com.stars.easyms.mq.core;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.stars.easyms.base.function.EasyMsConsumer;
import com.stars.easyms.base.util.JsonUtil;
import com.stars.easyms.mq.bean.EasyMsMQInfo;
import com.stars.easyms.mq.bean.MQMessageInfo;
import com.stars.easyms.mq.dto.MQSendMessageDTO;
import com.stars.easyms.mq.enums.DestinationType;
import com.stars.easyms.mq.enums.MQStatus;
import com.stars.easyms.mq.enums.MQType;
import com.stars.easyms.mq.exception.EasyMsMQException;
import com.stars.easyms.mq.registrar.EasyMsMQHolder;
import com.stars.easyms.mq.util.EasyMsMQIDUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * <p>className: BaseMQListener</p>
 * <p>description: 基础MQ监听器</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/4/29 2:08 下午
 */
public class BaseMQListener {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final EasyMsMQInfo easyMsMqInfo;

    private final String topic;

    private final MQType mqType;

    protected BaseMQListener(EasyMsMQInfo easyMsMqInfo, String topic) {
        this.easyMsMqInfo = easyMsMqInfo;
        this.topic = topic;
        this.mqType = MQType.RECEIVE;
    }

    protected <T> T listen(EasyMsConsumer<List<MQMessageInfo>> receiveMQMessageConsumer,
                           Supplier<T> successSupplier, Function<Throwable, T> exceptionSupplier) {
        List<MQMessageInfo> mqMessageInfoList = new ArrayList<>();
        try {
            receiveMQMessageConsumer.accept(mqMessageInfoList);
        } catch (Throwable t) {
            return exceptionSupplier.apply(t);
        }
        // 处理接收到的消息列表
        handle(mqMessageInfoList);
        return successSupplier.get();
    }

    protected void receiveMQMessage(List<MQMessageInfo> mqMessageInfoList, String text) {
        // 首先转换成MQSendMessageDTO，如果转换后messageId不为空则表示是内部mq调用
        try {
            List<MQSendMessageDTO> mqSendMessageDTOList = JSON.parseObject(text, new TypeReference<List<MQSendMessageDTO>>() {
            });
            if (mqSendMessageDTOList != null && !mqSendMessageDTOList.isEmpty()) {
                mqMessageInfoList.addAll(receiveMQMessage(mqSendMessageDTOList));
            }
            return;
        } catch (Exception e) {
            logger.debug("消息转换List<MQSendMessageDTO>失败!", e);
        }
        mqMessageInfoList.add(getMQMessageInfo(JsonUtil.parseObject(text, easyMsMqInfo.getParameterType())));
    }

    private List<MQMessageInfo> receiveMQMessage(List<MQSendMessageDTO> mqSendMessageDTOList) {
        List<MQMessageInfo> mqMessageInfoList = new ArrayList<>();
        for (MQSendMessageDTO mqSendMessageDTO : mqSendMessageDTOList) {
            if (mqSendMessageDTO.getMessageId() == null) {
                throw new EasyMsMQException("消息messageId为空!");
            }
            MQMessageInfo mqMessageInfo = MQMessageInfo.of(mqSendMessageDTO);
            initMQMessageInfo(mqMessageInfo);
            mqMessageInfoList.add(mqMessageInfo);
        }
        return mqMessageInfoList;
    }

    protected MQMessageInfo getMQMessageInfo(Object messageObj) {
        MQMessageInfo mqMessageInfo = new MQMessageInfo();
        mqMessageInfo.setMessageId(EasyMsMQIDUtil.getMessageId());
        mqMessageInfo.setKey(topic);
        mqMessageInfo.setMessage(JsonUtil.toJSONString(messageObj));
        initMQMessageInfo(mqMessageInfo);
        return mqMessageInfo;
    }

    private void initMQMessageInfo(MQMessageInfo mqMessageInfo) {
        mqMessageInfo.setId(EasyMsMQIDUtil.getId());
        mqMessageInfo.setMqType(mqType);
        mqMessageInfo.setMqStatus(MQStatus.INIT);
        mqMessageInfo.setDestSys(EasyMsMQHolder.getApplicationId());
        mqMessageInfo.setRedeliveryPolicy(easyMsMqInfo.getRedeliveryPolicy().getCode());
        mqMessageInfo.setRedeliveryDelay(easyMsMqInfo.getRedeliveryDelay());
        if (mqMessageInfo.getDestinationType() == null) {
            mqMessageInfo.setDestinationType(DestinationType.TOPIC.getCode());
        }
        Timestamp currTimestamp = new Timestamp(System.currentTimeMillis());
        mqMessageInfo.setReceiveTime(currTimestamp);
        if (mqMessageInfo.getCreateTime() == null) {
            mqMessageInfo.setCreateTime(currTimestamp);
        }
    }

    private void handle(List<MQMessageInfo> mqMessageInfoList) {
        if (!mqMessageInfoList.isEmpty()) {
            for (MQMessageInfo mqMessageInfo : mqMessageInfoList) {

                // 记录待接收处理消息日志
                logger.info("接收到目标({})待处理MQ消息,消息内容:{}", topic, mqMessageInfo.getMessage());

                // 为了防止消费方重复消费，这里先在redis中查询一下是否存在
                String messageId = mqMessageInfo.getMessageId();
                MQMessageInfo mqMessageInfoInRedis = MQHolder.getEasyMsMQRedisManager().getMQMessageInfoFromRedis(mqType.getName(), messageId);
                if (mqMessageInfoInRedis != null) {
                    // 如果已经存在并且消息内容相同，则表示已经消费过，因此直接抛弃
                    if (mqMessageInfoInRedis.getMessage().equals(mqMessageInfo.getMessage())) {
                        continue;
                    }

                    // 若不一样则表示有message_id重复，需要记录并重新生成message_id
                    String newMessageId = EasyMsMQIDUtil.getMessageId();
                    mqMessageInfo.setMessageId(newMessageId);
                    logger.warn("Receive repeat mq messageId, messageId is '{}', generator new messageId '{}'!",
                            messageId, newMessageId);
                }

                // 将MQ消息缓存在redis中一份数据，防止由于服务重启、宕机等情况引起的异步插入数据库时数据丢失，尽可能的保证MQ发送的可用性、稳定性
                mqMessageInfo.setLastLiveTime(System.currentTimeMillis());
                MQHolder.getEasyMsMQRedisManager().setMQMessageInfoToRedis(mqType.getName(), messageId, mqMessageInfo);

                AsyncReceiveMQMessageHandler.handle(mqMessageInfo);
            }
        }
    }
}
