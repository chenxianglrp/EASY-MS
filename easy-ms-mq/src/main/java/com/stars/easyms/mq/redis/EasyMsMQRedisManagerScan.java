package com.stars.easyms.mq.redis;

import com.stars.easyms.redis.scan.EasyMsRedisManagerScan;

/**
 * <p>className: EasyMsMQRedisManagerScan</p>
 * <p>description: EasyMsMQ的redis管理类的扫描实现类</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/4/14 8:23 下午
 */
public class EasyMsMQRedisManagerScan implements EasyMsRedisManagerScan {

    @Override
    public String[] scanPackages() {
        return new String[]{getClass().getPackage().getName()};
    }

}
