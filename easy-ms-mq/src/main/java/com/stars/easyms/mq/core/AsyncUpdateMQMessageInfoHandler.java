package com.stars.easyms.mq.core;

import com.stars.easyms.base.asynchronous.BaseBatchAsynchronousTask;
import com.stars.easyms.base.batch.BatchResult;
import com.stars.easyms.mq.bean.MQMessageInfo;
import com.stars.easyms.mq.service.MQMessageService;

import java.util.List;

/**
 * 异步修改mq消息
 *
 * @author guoguifang
 * @date 2021-04-23 13:54
 * @since 1.8.0
 */
final class AsyncUpdateMQMessageInfoHandler extends BaseBatchAsynchronousTask<MQMessageInfo> {

    private static final AsyncUpdateMQMessageInfoHandler INSTANCE = new AsyncUpdateMQMessageInfoHandler();

    @Override
    protected BatchResult execute(List<MQMessageInfo> mqMessageInfoList) {
        MQMessageService.getInstance()
                .batchUpdateMQMessageByPrimaryKey(mqMessageInfoList)
                .getSuccessDatas()
                .forEach(AsyncPendingConfirmedDataHandler::handle);
        return BatchResult.ofSuccess();
    }

    static void handle(MQMessageInfo mqMessageInfo) {
        INSTANCE.add(mqMessageInfo);
    }

    @Override
    protected int maxWorkerCount() {
        return 2;
    }

    @Override
    protected int singleWorkerThreshold() {
        return 6000;
    }

    private AsyncUpdateMQMessageInfoHandler() {
    }
}
