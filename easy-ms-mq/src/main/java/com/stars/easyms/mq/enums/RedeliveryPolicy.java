package com.stars.easyms.mq.enums;

/**
 * 消息重发机制
 *  1:NORMAL,重试时间间隔相同
 *  2:PROGRESSIVE,重试时间间隔递进的，每多失败一次就比之前多等待N秒再发送(如果重试间隔是30秒，则创建时间之后1:30,2:90,3:180,4:300,5:450,6:630...秒后再次发送)
 *  3:EXPONENTIAL,重试时间间隔指数的，每多失败一次就比之前多等待2N秒再发送(如果重试间隔是30秒，则创建时间之后1:30,2:60,3:120,4:240,5:480,6:960...秒后再次发送)
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
public enum RedeliveryPolicy {

    /**
     * 重试时间间隔相同
     */
    NORMAL("1"),

    /**
     * 重试时间间隔递进的，每多失败一次就比之前多等待N秒再发送(如果重试间隔是30秒，则创建时间之后1:30,2:90,3:180,4:300,5:450,6:630...秒后再次发送)
     */
    PROGRESSIVE("2"),

    /**
     * 重试时间间隔指数的，每多失败一次就比之前多等待2N秒再发送(如果重试间隔是30秒，则创建时间之后1:30,2:60,3:120,4:240,5:480,6:960...秒后再次发送)
     */
    EXPONENTIAL("3");

    private String code;

    RedeliveryPolicy(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}
