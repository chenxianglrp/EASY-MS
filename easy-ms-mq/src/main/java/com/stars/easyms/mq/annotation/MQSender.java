package com.stars.easyms.mq.annotation;

import com.stars.easyms.mq.enums.RedeliveryPolicy;
import com.stars.easyms.mq.enums.DestinationType;

import java.lang.annotation.*;

/**
 * 提供mq发送服务的方法注解
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MQSender {

    /**
     * MQ接收消息的key,如果增加
     */
    String key();

    /**
     * key所对应的中文名称
     */
    String name() default "";

    /**
     * QUEUE:点对点，不可重复消费
     * TOPIC:发布/订阅，可以重复消费
     */
    DestinationType destinationType() default DestinationType.TOPIC;

    /**
     * 发送通道数量，就是一个相同的KEY使用几个通道发送与接收，发送端和接收端的数值必须一致，不能超过5个，超过5个时默认5个
     */
    int channelCount() default 1;

    /**
     * 是否使用批量发送，默认是，当接收方为非easy-ms项目时需要设置为false
     */
    boolean batch() default true;

    /**
     * 单次单通道允许的最大发送数据量，不能超过2000条数据，当值超过2000时默认2000（只有批量发送的时候有效）
     */
    int channelSize() default 300;

    /**
     * 失败默认重试，该参数为最大重试次数，默认5
     */
    long maxRetryCount() default 5;

    /**
     * 消息重发机制：重试时间间隔相同、重试时间间隔递进、重试时间间隔2倍指数增加
     */
    RedeliveryPolicy redeliveryPolicy() default RedeliveryPolicy.EXPONENTIAL;

    /**
     * 消息重发时间间隔(单位:秒)
     */
    long redeliveryDelay() default 60;

    /**
     * 是否允许使用非持久化模式发送消息，使用非持久化模式会提升MQ性能，但会有丢失数据风险：该参数ActiveMQ有效,RocketMQ无效
     */
    boolean allowNonPersistent() default false;

    /**
     * key值是否随着环境变化(默认true，如果是true的话key值结尾会增加"_${spring.profiles.active}")
     * 该功能主要用于多个环境共用一个MQ环境的情况，若不想使用该功能，可以手动在配置里的easy-ms.mq.key中添加用来区分不同环境
     */
    boolean isDynamicKey() default true;
}
