package com.stars.easyms.mq.core;

import com.stars.easyms.base.util.ThreadUtil;
import com.stars.easyms.mq.constant.MQConstants;
import com.stars.easyms.mq.enums.MQStatus;
import com.stars.easyms.mq.service.MQMessageService;
import com.stars.easyms.redis.exception.DistributedLockTimeoutException;
import com.stars.easyms.redis.lock.DistributedLock;
import lombok.extern.slf4j.Slf4j;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * MQ消息调度者线程(发送、接收各一个单例线程)：
 * 1.生成发送/接收处理分类者线程的线程池
 * 2.抓取发送/接收处理失败的数据
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
@Slf4j
public final class MQMigratorScheduler implements Runnable {

    private static MQMigratorScheduler instance;

    private static final int PER_MAX_MOVE_SIZE = 1500;

    private final DistributedLock schedulerClearLock = new DistributedLock(MQConstants.MQ_SCHEDULER_LOCK, "C", 120);

    private final AtomicInteger loopExecuteExceptionCount = new AtomicInteger();

    @Override
    public void run() {
        // 循环执行mq调度任务,当失败次数过多时停止执行
        while (loopExecuteExceptionCount.get() < MQConstants.MAX_SCHEDULE_EXCEPTION_COUNT) {
            try {
                clearOldData();
                ThreadUtil.sleep(43200000 + ThreadLocalRandom.current().nextLong(43200000));
            } catch (Exception e) {
                log.error("MQ migrator scheduler execute failure!", e);
                loopExecuteExceptionCount.incrementAndGet();
                ThreadUtil.sleep(30, TimeUnit.SECONDS);
            }
        }
    }

    private void clearOldData() {
        for(;;) {
            // 为了防止分布式服务器同时读取同一份数据，因此需要加锁
            try {
                schedulerClearLock.blockLock(30000);
            } catch (DistributedLockTimeoutException e) {
                ThreadUtil.sleep(5, TimeUnit.SECONDS);
                continue;
            }

            try {
                // 清理7天以前的旧数据
                Map<String, Object> paramMap = new HashMap<>(4);
                paramMap.put("status", MQStatus.SUCCESS.getCode());
                paramMap.put("size", PER_MAX_MOVE_SIZE);
                paramMap.put("limitTime", new Timestamp(System.currentTimeMillis() - 604800000));

                // 查询7天以前的数据，分批次查询，每次查询1500条，如果没有需要归档的数据时中断任务
                List<String> allPendingArchivingMQMessageIdList =
                        MQMessageService.getInstance().getAllPendingArchivingMQMessageId(paramMap);
                if (allPendingArchivingMQMessageIdList.isEmpty()) {
                    break;
                }

                // 插入到历史表里并删除信息表中已插入的数据
                MQMessageService.getInstance().insertMQMessageOldData(allPendingArchivingMQMessageIdList);
                MQMessageService.getInstance().deleteMQMessageOldData(allPendingArchivingMQMessageIdList);
            } finally {
                schedulerClearLock.unlock();
            }
        }
    }

    public static synchronized MQMigratorScheduler createInstance() {
        if (instance == null) {
            instance = new MQMigratorScheduler();
        }
        return instance;
    }

    private MQMigratorScheduler() {}

}