package com.stars.easyms.mq.repository.dml.oracle;

import com.stars.easyms.mq.annotation.MQRepository;
import com.stars.easyms.mq.repository.dml.MQMessageDAO;

/**
 * MQ消息发送/接收信息OracleDAO
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
@MQRepository("oracleMQMessageDAO")
public interface OracleMQMessageDAO extends MQMessageDAO {

}
