package com.stars.easyms.mq.core;

import com.stars.easyms.base.bean.LazyLoadBean;
import com.stars.easyms.base.function.EasyMsSupplier;
import com.stars.easyms.base.util.ReflectUtil;
import com.stars.easyms.mq.annotation.MQReceiver;
import com.stars.easyms.mq.annotation.MQSender;
import com.stars.easyms.mq.bean.EasyMsMQInfo;
import com.stars.easyms.mq.bean.MQMessageInfo;
import com.stars.easyms.mq.bean.MQReceiveContext;
import com.stars.easyms.mq.constant.MQConstants;
import com.stars.easyms.mq.enums.MQStatus;
import com.stars.easyms.mq.enums.MQType;
import com.stars.easyms.mq.exception.EasyMsMQException;
import com.stars.easyms.mq.registrar.EasyMsMQHolder;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Method;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * <p>className: BaseEasyMsMQMethodInterceptor</p>
 * <p>description: EasyMsMQManager方法拦截器抽象类</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/4/14 3:46 下午
 */
public abstract class BaseEasyMsMQMethodInterceptor {

    private static final LazyLoadBean<EasyMsMQSender> EASY_MS_MQ_SENDER_LAZY_LOAD_BEAN = new LazyLoadBean<>(EasyMsMQSender.class);

    protected Object invoke(Method method, Object[] args, EasyMsSupplier<Object> proceedSupplier) throws Throwable {
        EasyMsMQInfo easyMsMQInfo = EasyMsMQHolder.getEasyMsMQInfo(method);
        if (easyMsMQInfo == null) {
            if (method.isAnnotationPresent(MQSender.class) || method.isAnnotationPresent(MQReceiver.class)) {
                throw new EasyMsMQException("Can't find the method '{}' mq info!", ReflectUtil.getMethodFullName(method));
            }
            return proceedSupplier.get();
        }

        if (easyMsMQInfo.getType() == MQType.SEND) {
            invokeSendMethod(easyMsMQInfo, args);
            if (Boolean.class.equals(easyMsMQInfo.getReturnType()) || boolean.class.equals(easyMsMQInfo.getReturnType())) {
                return Boolean.TRUE;
            }
            return null;
        }

        return invokeReceiveMethod(easyMsMQInfo, args, proceedSupplier);
    }

    private void invokeSendMethod(EasyMsMQInfo easyMsMQInfo, Object[] args) {
        // 获取key，如果不存在则抛出异常
        String key = easyMsMQInfo.getKey();
        if (StringUtils.isBlank(key)) {
            throw new EasyMsMQException("MQ has not been initialized!");
        }

        // 如果是多通道发送则获取随机的通道
        int channelCount = easyMsMQInfo.getChannelCount();
        if (channelCount > 1) {
            key = StringUtils.join(new Object[]{key,
                    StringUtils.leftPad(String.valueOf(
                            ThreadLocalRandom.current().nextInt(channelCount) + 1), 3, "0")}, "-");
        }

        // 发送消息
        EASY_MS_MQ_SENDER_LAZY_LOAD_BEAN.getNonNullBean()
                .send(key, easyMsMQInfo.getDestinationType(), easyMsMQInfo.getRedeliveryPolicy(),
                        easyMsMQInfo.getRedeliveryDelay(), args[0]);
    }

    /**
     * 为了防止消费端消费成功且事务提交成功后服务器宕机或重启等原因造成没有正常修改数据状态的问题，这里需要将修改状态的操作放在整体事务中
     * 执行到此处时已经开启事务
     */
    private Object invokeReceiveMethod(EasyMsMQInfo easyMsMQInfo, Object[] args, EasyMsSupplier<Object> proceedSupplier) throws Throwable {
        Object executeInstance = easyMsMQInfo.getExecuteInstance();
        Method executeMethod = easyMsMQInfo.getExecuteMethod();
        if (executeInstance != null && executeMethod != null) {
            return checkAndReturnReceiveResult(executeMethod.invoke(executeInstance, args[0]));
        }
        return checkAndReturnReceiveResult(proceedSupplier.get());
    }

    /**
     * 为了防止消费端重复消费，将修改状态的操作放入整体事务中，由于只为了防止重复消费所以当有异常时会回滚，只在执行成功时修改状态，执行失败放在外层修改状态
     */
    private Object checkAndReturnReceiveResult(Object result) {
        MQReceiveContext mqReceiveContext = MQHolder.MQ_RECEIVE_CONTENT_THREAD_LOCAL.get();
        if (mqReceiveContext != null) {
            MQMessageInfo mqMessageInfo = mqReceiveContext.getMqMessageInfo();
            List<MQMessageInfo> mqMessageInfoList = mqReceiveContext.getMqMessageInfoList();
            if (result instanceof Boolean && Boolean.FALSE.equals(result)) {
                if (mqMessageInfo != null) {
                    MQMessageStatusHandler.updateMQMessageStatus(mqMessageInfo, MQConstants.RECEIVE_HANDLE_FAIL, MQStatus.FAIL_RETRY, mqReceiveContext.getMaxRetryCount());
                } else {
                    MQMessageStatusHandler.batchUpdateMQMessageStatus(mqMessageInfoList, MQConstants.RECEIVE_HANDLE_FAIL, MQStatus.FAIL_RETRY, mqReceiveContext.getMaxRetryCount());
                }
                mqReceiveContext.setUpdateStatusFinish(true);
                return result;
            }
            if (mqMessageInfo != null) {
                MQMessageStatusHandler.updateMQMessageStatus(mqMessageInfo, MQConstants.RECEIVE_HANDLE_SUCCESS, MQStatus.SUCCESS, null);
            } else {
                MQMessageStatusHandler.batchUpdateMQMessageStatus(mqMessageInfoList, MQConstants.RECEIVE_HANDLE_SUCCESS, MQStatus.SUCCESS, null);
            }
            mqReceiveContext.setUpdateStatusFinish(true);
        }
        return result;
    }

}
