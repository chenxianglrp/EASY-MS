package com.stars.easyms.mq.core;

import com.stars.easyms.mq.core.activemq.ActiveMQListener;
import com.stars.easyms.mq.core.rocketmq.RocketMQListener;
import com.stars.easyms.mq.bean.EasyMsMQInfo;
import com.stars.easyms.mq.enums.MQ;
import com.stars.easyms.mq.properties.EasyMsMQProperties;
import org.springframework.context.ApplicationContext;

/**
 * MQ监听器工厂类
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
public class MQListenerFactory {

    private ApplicationContext applicationContext;
    private EasyMsMQProperties easyMsMQProperties;

    public MQListener getMQListener(EasyMsMQInfo easyMsMqInfo, String key) {
        if (easyMsMQProperties.getActive() == MQ.ACTIVEMQ) {
            return new ActiveMQListener(applicationContext, easyMsMQProperties, easyMsMqInfo, key);
        }
        return new RocketMQListener(easyMsMQProperties, easyMsMqInfo, key);
    }

    public MQListenerFactory(ApplicationContext applicationContext, EasyMsMQProperties easyMsMQProperties) {
        this.applicationContext = applicationContext;
        this.easyMsMQProperties = easyMsMQProperties;
    }

}
