package com.stars.easyms.mq.core;

import com.stars.easyms.mq.bean.MQMessageInfo;
import com.stars.easyms.mq.constant.MQConstants;
import com.stars.easyms.mq.enums.MQType;
import com.stars.easyms.mq.registrar.EasyMsMQHolder;
import com.stars.easyms.mq.service.MQMessageService;
import com.stars.easyms.mq.util.EasyMsMQIDUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 异步处理接收mq消息
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
@Slf4j
final class AsyncReceiveMQMessageHandler extends BaseAsyncMQMessageHandler {

    private static final AsyncReceiveMQMessageHandler INSTANCE = new AsyncReceiveMQMessageHandler();;

    static void handle(MQMessageInfo mqMessageInfo) {
        INSTANCE.add(mqMessageInfo);
        MQHolder.MQ_MESSAGE_INFO_PENDING_HEART_MAP.put(mqMessageInfo.getMessageId(), mqMessageInfo);
    }

    /**
     * 过滤消费端的重复消息：为了防止消费端重复消费，因此需要查询数据库中是否已经存在该messageId，
     * 如果存在则判断其消息内容是否一样，如果一样则表示重复消息，如果不一样则表示messageId重复则重新设置一个新的messageId
     */
    @Override
    protected List<MQMessageInfo> filterRepeatMQMessageInfo(List<MQMessageInfo> mqMessageInfoList) {
        // 如果是消费方，为了防止重复消费，需要过滤才可处理
        if (mqMessageInfoList.isEmpty()) {
            return mqMessageInfoList;
        }

        // 先需要判断是否已经存储在数据库中,批量查询,每次最多查询100条
        List<MQMessageInfo> nonRepeatMQMessageInfoList = new ArrayList<>();
        List<MQMessageInfo> pendingConfirmedList = new ArrayList<>();
        mqMessageInfoList.forEach(mqMessageInfo -> {
            // 如果该数据已经存储到数据库，则表示该数据为失败重试数据，为非重复处理数据
            if (mqMessageInfo.isPersistent()) {
                nonRepeatMQMessageInfoList.add(mqMessageInfo);
            } else {
                pendingConfirmedList.add(mqMessageInfo);
            }
        });
        int maxHandleCountPerTime = MQConstants.MAX_HANDLE_COUNT_PER_TIME;
        Stream.iterate(0, n -> n + 1)
                .limit((pendingConfirmedList.size() + maxHandleCountPerTime - 1) / maxHandleCountPerTime)
                .forEach(i -> {

                    // 防止单次查询数据量过大，采用分批次的方式查询
                    List<MQMessageInfo> pageMQMessageInfoList = pendingConfirmedList.stream()
                            .skip(i * maxHandleCountPerTime)
                            .limit(maxHandleCountPerTime)
                            .collect(Collectors.toList());

                    // 批量查询数据库并获取messageId以及对应消息内容
                    Map<String, String> existingMQMessageIdInDbMap = new HashMap<>(maxHandleCountPerTime);
                    Map<String, Object> paramMap = new HashMap<>(4);
                    paramMap.put("type", MQType.RECEIVE.getCode());
                    paramMap.put("destSys", EasyMsMQHolder.getApplicationId());
                    paramMap.put("messageIdList",
                            pageMQMessageInfoList.stream()
                                    .map(MQMessageInfo::getMessageId)
                                    .collect(Collectors.toList()));
                    MQMessageService.getInstance()
                            .getExistingMQMessageIdInDb(paramMap)
                            .forEach(m -> existingMQMessageIdInDbMap.put(m.getMessageId(), m.getMessage()));

                    pageMQMessageInfoList.forEach(mqMessageInfo -> {
                        // 获取messageId，并通过messageId获取数据库中的消息内容
                        String messageId = mqMessageInfo.getMessageId();
                        String mqMessageInDb = existingMQMessageIdInDbMap.get(messageId);
                        if (mqMessageInDb == null) {
                            nonRepeatMQMessageInfoList.add(mqMessageInfo);
                        } else {
                            MQHolder.MQ_MESSAGE_INFO_PENDING_HEART_MAP.remove(messageId);
                            // 如果数据库中存在，则判断其消息内容是否一样
                            if (!mqMessageInDb.equals(mqMessageInfo.getMessage())) {
                                // 如果不一样则表示messageId重复则重新设置一个新的messageId
                                String newMessageId = EasyMsMQIDUtil.getMessageId();
                                mqMessageInfo.setMessageId(newMessageId);
                                MQHolder.MQ_MESSAGE_INFO_PENDING_HEART_MAP.put(newMessageId, mqMessageInfo);
                                log.warn("Receive repeat mq messageId, messageId is '{}', generator new messageId '{}'!",
                                        messageId, newMessageId);
                                nonRepeatMQMessageInfoList.add(mqMessageInfo);
                            }
                        }
                    });
                });
        return nonRepeatMQMessageInfoList;
    }

    @Override
    protected MQType mqType() {
        return MQType.RECEIVE;
    }

    private AsyncReceiveMQMessageHandler() {
    }
}
