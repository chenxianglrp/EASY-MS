package com.stars.easyms.mq.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>className: MQStatus</p>
 * <p>description: MQ消息发送/接收状态</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/4/22 4:58 下午
 */
public enum MQStatus {

    /**
     * 初始化
     */
    INIT("0", "初始化状态"),

    /**
     * 发送中/处理中
     */
    PROCESSING("1", "处理中状态"),

    /**
     * 发送成功/处理成功
     */
    SUCCESS("2", "成功状态"),

    /**
     * 发送失败/处理失败并待重新执行
     */
    FAIL_RETRY("3", "失败待重试状态"),

    /**
     * 发送失败/处理失败不再重新执行
     */
    FAIL("4", "失败状态");

    private String code;

    private String name;

    MQStatus(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    private static Map<String, MQStatus> codeLookup;

    public static MQStatus forCode(String code) {
        return codeLookup.get(code);
    }

    static {
        codeLookup = new HashMap<>();
        for (MQStatus mqStatus : values()) {
            codeLookup.put(mqStatus.code, mqStatus);
        }
    }
}
