package com.stars.easyms.mq.annotation;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * EnhanceMQDAO注解
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
@Component
public @interface MQRepository {
    String value() default "";
}