package com.stars.easyms.mq.bean;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * <p>className: MQReceiveContext</p>
 * <p>description: MQ接收数据上下文</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/11 11:32 上午
 */
@Data
@Builder
public class MQReceiveContext {

    private MQMessageInfo mqMessageInfo;

    private List<MQMessageInfo> mqMessageInfoList;

    private Long maxRetryCount;

    private boolean updateStatusFinish;
}
