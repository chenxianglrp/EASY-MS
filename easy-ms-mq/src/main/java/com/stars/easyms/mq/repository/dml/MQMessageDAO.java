package com.stars.easyms.mq.repository.dml;

import com.stars.easyms.mq.bean.MQMessageInfo;

import java.util.List;
import java.util.Map;

/**
 * MQ消息发送/接收信息DAO
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
public interface MQMessageDAO {

    /**
     * 根据查询条件查询MQ消息
     *
     * @param paramMap 查询条件
     * @return
     */
    List<MQMessageInfo> getMQMessage(Map<String, Object> paramMap);

    /**
     * 根据传入的MQ主键列表查询所有存在的MQ messageId
     *
     * @param paramMap MQ消息查询列表
     * @return
     */
    List<MQMessageInfo> getExistingMQMessageId(Map<String, Object> paramMap);

    /**
     * 插入一条MQ封装消息到数据库
     *
     * @param mqMessageInfo MQ封装消息
     * @return
     */
    int insertMQMessage(MQMessageInfo mqMessageInfo);

    /**
     * 根据主键修改MQ封装消息
     *
     * @param mqMessageInfo MQ封装消息
     * @return
     */
    int updateMQMessageByPrimaryKey(MQMessageInfo mqMessageInfo);

    /**
     * 获取MQ封装消息总数量
     *
     * @param paramMap 查询条件
     * @return
     */
    Long getMQMessageCount(Map<String, Object> paramMap);

    /**
     * 获取MQ失败重试状态的符合重试条件的消息
     *
     * @param paramMap 查询条件
     * @return
     */
    List<MQMessageInfo> getMQMessageForFailRetry(Map<String, Object> paramMap);

    /**
     * 获取MQ初始化状态的符合重试条件的消息
     *
     * @param paramMap 查询条件
     * @return
     */
    List<MQMessageInfo> getMQMessageForInit(Map<String, Object> paramMap);

    /**
     * 获取待归档的MQ消息ID
     * @param paramMap 查询条件
     * @return
     */
    List<String> getAllPendingArchivingMQMessageId(Map<String, Object> paramMap);

    /**
     * 插入一条历史MQ封装消息
     *
     * @param idList 待插入的MQ消息ID列表
     * @return
     */
    int insertMQMessageOldData(List<String> idList);

    /**
     * 删除一条历史MQ封装消息
     *
     * @param idList 待插入的MQ消息ID列表
     * @return
     */
    int deleteMQMessageOldData(List<String> idList);

    /**
     * 对状态、类型分组获取其数量
     * @param paramMap 查询参数
     * @return
     */
    List<MQMessageInfo> getCountWithStatusAndType(Map<String, Object> paramMap);
}
