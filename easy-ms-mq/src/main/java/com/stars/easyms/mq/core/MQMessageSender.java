package com.stars.easyms.mq.core;

import com.stars.easyms.mq.bean.MQSendResult;

/**
 * MQ消息发送者接口
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
public interface MQMessageSender {

    /**
     * MQ消息转换并发送
     *
     * @param currentSendKey               发送目标地址
     * @param message                      消息
     * @param destinationType              发送类型：点对点、广播
     * @param currMessIsAllowNonPersistent 是否允许非持久化数据
     * @return mq发送结果
     */
    MQSendResult convertAndSend(String currentSendKey, String message, String destinationType, Boolean currMessIsAllowNonPersistent);

}
