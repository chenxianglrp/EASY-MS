package com.stars.easyms.mq.registrar;

import com.stars.easyms.base.util.ReflectUtil;
import com.stars.easyms.base.util.SpringBootUtil;
import com.stars.easyms.mq.annotation.MQManager;
import com.stars.easyms.mq.annotation.MQReceiver;
import com.stars.easyms.mq.annotation.MQSender;
import com.stars.easyms.mq.bean.EasyMsMQInfo;
import com.stars.easyms.mq.enums.MQType;
import com.stars.easyms.mq.exception.EasyMsMQException;
import com.stars.easyms.mq.scan.EasyMsMQManagerScan;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionReaderUtils;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.AnnotationBeanNameGenerator;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;

/**
 * <p>className: EasyMsMQRegistrar</p>
 * <p>description: EasyMsMQ注册类</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/4/12 7:54 下午
 */
@SuppressWarnings("unchecked")
public class EasyMsMQRegistrar implements ImportBeanDefinitionRegistrar {

    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        // 注册MQ的bean:1.本项目的使用SpringBootPackage; 2.依赖jar包里的使用ServiceLoad加载EasyMsMQManagerScan服务
        Set<String> easyMsMQScanPackageSet = new HashSet<>();
        easyMsMQScanPackageSet.add(SpringBootUtil.getSpringApplicationPackageName());
        ServiceLoader<EasyMsMQManagerScan> easyMsMQManagerScans = ServiceLoader.load(EasyMsMQManagerScan.class);
        for (EasyMsMQManagerScan easyMsMQManagerScan : easyMsMQManagerScans) {
            String[] scanPackages = easyMsMQManagerScan.scanPackages();
            if (scanPackages != null && scanPackages.length > 0) {
                easyMsMQScanPackageSet.addAll(Arrays.asList(scanPackages));
            }
        }

        // 扫描所有加了MQManager注解的类或接口
        Set<Class<?>> mqManagerClassSet = ReflectUtil.getAllClassByAnnotation(easyMsMQScanPackageSet, MQManager.class);
        if (mqManagerClassSet.isEmpty()) {
            return;
        }

        EasyMsMQManagerFactoryBean.setBeanFactory((AutowireCapableBeanFactory) registry);
        for (Class<?> mqManagerClass : mqManagerClassSet) {

            // 再次判断，确定只扫描加了MQManager注解的类或接口
            if (!mqManagerClass.isAnnotationPresent(MQManager.class)) {
                continue;
            }

            String mqManagerClassName = mqManagerClass.getName();
            String beanName = AnnotationBeanNameGenerator.INSTANCE.generateBeanName(
                    BeanDefinitionBuilder.genericBeanDefinition(mqManagerClass).getBeanDefinition(), registry);
            BeanDefinitionBuilder definitionBuilder = BeanDefinitionBuilder.genericBeanDefinition(EasyMsMQManagerFactoryBean.class);
            definitionBuilder.addPropertyValue("type", mqManagerClassName);
            definitionBuilder.addPropertyValue("beanName", beanName);
            definitionBuilder.setAutowireMode(AbstractBeanDefinition.AUTOWIRE_BY_TYPE);
            definitionBuilder.setPrimary(true);
            AbstractBeanDefinition beanDefinition = definitionBuilder.getBeanDefinition();
            beanDefinition.setAttribute(FactoryBean.OBJECT_TYPE_ATTRIBUTE, mqManagerClassName);
            BeanDefinitionHolder holder = new BeanDefinitionHolder(beanDefinition, beanName, new String[]{mqManagerClassName});

            // 由于MQManager注解包含了@Component注解(为了在IDE工具中方便抓取)，@Component会自动scan，所以可能会在此时提前注册，如果已经注册就删除掉以当前为准
            if (registry.containsBeanDefinition(beanName)) {
                registry.removeBeanDefinition(beanName);
            }
            BeanDefinitionReaderUtils.registerBeanDefinition(holder, registry);

            // 遍历该类或接口中的所有方法
            Method[] mqManagerMethods = mqManagerClass.getDeclaredMethods();
            for (Method mqManagerMethod : mqManagerMethods) {
                // 如果方法没有MQSender或MQReceiver注解直接跳过
                if (!mqManagerMethod.isAnnotationPresent(MQSender.class)
                        && !mqManagerMethod.isAnnotationPresent(MQReceiver.class)) {
                    continue;
                }

                // mq发送与接收的参数个数必须是一个
                Class<?>[] parameterClasses = mqManagerMethod.getParameterTypes();
                String mqManagerMethodName = mqManagerMethod.getName();
                Class<?> returnType = mqManagerMethod.getReturnType();
                mqManagerMethod.setAccessible(true);

                if (parameterClasses.length != 1) {
                    throw new EasyMsMQException("MQ manager({}) method({}) parameter count must be one!",
                            mqManagerClassName, mqManagerMethodName);
                }
                if (!Boolean.class.equals(returnType) && !boolean.class.equals(returnType)
                        && !Void.class.equals(returnType) && !void.class.equals(returnType)) {
                    throw new EasyMsMQException("MQ manager({}) method({}) return type must be " +
                            "one of (Boolean, boolean, Void, void)!", mqManagerClassName, mqManagerMethodName);
                }

                // 设置反射属性
                EasyMsMQInfo easyMsMqInfo = new EasyMsMQInfo();
                easyMsMqInfo.setMqManagerClass(mqManagerClass);
                easyMsMqInfo.setMethod(mqManagerMethod);
                easyMsMqInfo.setParameterType(mqManagerMethod.getGenericParameterTypes()[0]);
                easyMsMqInfo.setReturnType(returnType);

                // 判断该方法属于发送还是接收
                if (mqManagerMethod.isAnnotationPresent(MQSender.class)) {
                    registerMqSender(mqManagerMethod, easyMsMqInfo);
                    EasyMsMQHolder.MQ_METHOD_INFO_MAP.put(mqManagerMethod, easyMsMqInfo);
                    continue;
                }

                // 如果是批量接收那么接收的方法参数必须是List类型
                MQReceiver mqReceiver = mqManagerMethod.getAnnotation(MQReceiver.class);
                if (Boolean.TRUE.equals(mqReceiver.batch()) && !parameterClasses[0].isAssignableFrom(List.class)) {
                    throw new EasyMsMQException("MQ manager({}) method({}) annotation(MQReceiver) when batch receive, " +
                            "the parameter type must be List!", mqManagerClassName, mqManagerMethodName);
                }

                // 如果是接收类型，则需要判断是否设置了可执行类
                Class<?> executeClass = mqReceiver.executeClass();
                if (MQManager.class != executeClass && mqManagerClass != executeClass) {
                    String executeMethodName = mqReceiver.executeMethod();
                    if (StringUtils.isBlank(executeMethodName)) {
                        throw new EasyMsMQException("MQ manager({}) method({}) annotation(MQReceiver) attribute " +
                                "executeMethod is blank!", mqManagerClassName, mqManagerMethodName);
                    }
                    Method executeMethod = ReflectUtil.getMatchingMethod(executeClass, executeMethodName, parameterClasses[0]);
                    if (executeMethod == null) {
                        throw new EasyMsMQException("Method of the MQ manager({}) method({}) annotation(MQReceiver) " +
                                "attribute executeMethod is not exist or method parameter is not match!",
                                mqManagerClassName, mqManagerMethodName);
                    }
                    if (!executeMethod.getReturnType().equals(returnType)) {
                        throw new EasyMsMQException("The returnType of the MQ receive execute method({}) does not match " +
                                "the returnType of the MQ manager({}) method({})!",
                                ReflectUtil.getMethodFullName(executeMethod), mqManagerClassName, mqManagerMethodName);
                    }
                    easyMsMqInfo.setExecuteClass(executeClass);
                    easyMsMqInfo.setExecuteMethod(executeMethod);
                } else if ((mqManagerClass.isInterface() && !mqManagerMethod.isDefault())
                        || Modifier.isAbstract(mqManagerMethod.getModifiers())) {
                    throw new EasyMsMQException("MQ manager({}) method({}) is not an executable method!",
                            mqManagerClassName, mqManagerMethodName);
                }

                easyMsMqInfo.setType(MQType.RECEIVE);
                easyMsMqInfo.setAnnotationClass(MQReceiver.class);
                easyMsMqInfo.setSourceKey(mqReceiver.key());
                easyMsMqInfo.setName(mqReceiver.name());
                easyMsMqInfo.setDestinationType(mqReceiver.destinationType());
                easyMsMqInfo.setChannelCount(Math.max(Math.min(mqReceiver.channelCount(), 5), 1));
                easyMsMqInfo.setMaxRetryCount(Math.max(mqReceiver.maxRetryCount(), 0));
                easyMsMqInfo.setRedeliveryPolicy(mqReceiver.redeliveryPolicy());
                easyMsMqInfo.setRedeliveryDelay(Math.max(mqReceiver.redeliveryDelay(), 0));
                easyMsMqInfo.setBatch(mqReceiver.batch());
                easyMsMqInfo.setDynamicKey(mqReceiver.isDynamicKey());
                EasyMsMQHolder.MQ_METHOD_INFO_MAP.put(mqManagerMethod, easyMsMqInfo);
            }
        }
    }

    private void registerMqSender(Method mqManagerMethod, EasyMsMQInfo easyMsMqInfo) {
        MQSender mqSender = mqManagerMethod.getAnnotation(MQSender.class);
        easyMsMqInfo.setType(MQType.SEND);
        easyMsMqInfo.setAnnotationClass(MQSender.class);
        easyMsMqInfo.setName(mqSender.name());
        easyMsMqInfo.setSourceKey(mqSender.key());
        easyMsMqInfo.setChannelCount(Math.max(Math.min(mqSender.channelCount(), 5), 1));
        easyMsMqInfo.setChannelSize(Math.max(Math.min(mqSender.channelSize(), 2000), 1));
        easyMsMqInfo.setMaxRetryCount(Math.max(mqSender.maxRetryCount(), 0));
        easyMsMqInfo.setDestinationType(mqSender.destinationType());
        easyMsMqInfo.setRedeliveryPolicy(mqSender.redeliveryPolicy());
        easyMsMqInfo.setRedeliveryDelay(Math.max(mqSender.redeliveryDelay(), 0));
        easyMsMqInfo.setBatch(mqSender.batch());
        easyMsMqInfo.setDynamicKey(mqSender.isDynamicKey());
        easyMsMqInfo.setAllowNonPersistent(mqSender.allowNonPersistent());
    }

}
