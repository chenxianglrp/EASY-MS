package com.stars.easyms.mq.bean;

import com.stars.easyms.mq.core.MQListener;
import com.stars.easyms.mq.enums.DestinationType;
import com.stars.easyms.mq.enums.MQType;
import com.stars.easyms.mq.enums.RedeliveryPolicy;
import lombok.Data;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.List;

/**
 * MQ信息DTO
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
@Data
public class EasyMsMQInfo<T> {

    private MQType type;

    private Class<? extends Annotation> annotationClass;

    private String sourceKey;

    private String key;

    private String name;

    private DestinationType destinationType;

    private String sourceSys;

    private String destSys;

    private int channelCount;

    private Integer channelSize;

    private Long maxRetryCount;

    private RedeliveryPolicy redeliveryPolicy;

    private Long redeliveryDelay;

    private Boolean batch;

    private boolean dynamicKey;

    private String staticKey;

    private Boolean allowNonPersistent;

    private Class<T> mqManagerClass;

    private Method method;

    private T instance;

    private Type parameterType;

    private Class<?> returnType;

    private Class<?> executeClass;

    private Method executeMethod;

    private Object executeInstance;

    private List<MQListener> mqListenerList;

}
