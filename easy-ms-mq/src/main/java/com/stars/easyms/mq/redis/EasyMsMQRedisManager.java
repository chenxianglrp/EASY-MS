package com.stars.easyms.mq.redis;

import com.stars.easyms.mq.bean.MQMessageInfoScanContext;
import com.stars.easyms.mq.bean.MQMessageInfo;
import com.stars.easyms.redis.annotion.*;
import com.stars.easyms.redis.core.EasyMsCursor;

import java.util.Map;
import java.util.Set;

/**
 * <p>className: EasyMsMQRedisManager</p>
 * <p>description: EasyMsMQ的redis管理类</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/4/14 6:00 下午
 */
@RedisManager
public class EasyMsMQRedisManager {

    @RedisHashSet(group = "easy-ms-mq-message", resetExpire = false)
    public MQMessageInfo setMQMessageInfoToRedis(@RedisKey String myTypeName, @RedisHashKey String messageId, MQMessageInfo mqMessageInfo) {
        return mqMessageInfo;
    }

    @RedisHashSet(group = "easy-ms-mq-message", resetExpire = false)
    public Map<String, MQMessageInfo> setMQMessageInfoToRedis(@RedisKey String myTypeName, Map<String, MQMessageInfo> mqMessageInfoMap) {
        return mqMessageInfoMap;
    }

    @RedisHashGet(group = "easy-ms-mq-message")
    public MQMessageInfo getMQMessageInfoFromRedis(@RedisKey String myTypeName, @RedisHashKey String messageId) {
        return null;
    }

    @RedisHashScan(group = "easy-ms-mq-message")
    public EasyMsCursor<String, MQMessageInfo> getAllMQMessageInfoFromRedis(@RedisKey String myTypeName, @RedisScanCursorId long cursorId) {
        return null;
    }

    @RedisDelete(group = "easy-ms-mq-message")
    public void deleteMQMessageInfoFromRedis(@RedisKey String myTypeName, @RedisHashKey Set<String> messageIdSet) {
        // blank
    }

    @RedisStringSet(group = "easy-ms-mq-scanCursorContext", expire = "30m")
    public MQMessageInfoScanContext setScanCursorContextFromRedis(
            @RedisKey String mqTypeName, MQMessageInfoScanContext mqMessageInfoScanContext) {
        return mqMessageInfoScanContext;
    }

    @RedisStringGet(group = "easy-ms-mq-scanCursorContext")
    public MQMessageInfoScanContext getScanCursorContextFromRedis(@RedisKey String mqTypeName) {
        return null;
    }

    @RedisStringSet(group = "easy-ms-mq-monitor:time", expire = "1h")
    public Long setLastMQMonitorTime(Long lastMQMonitorTime) {
        return lastMQMonitorTime;
    }

    @RedisStringGet(group = "easy-ms-mq-monitor:time")
    public Long getLastMQMonitorTime() {
        return null;
    }

}
