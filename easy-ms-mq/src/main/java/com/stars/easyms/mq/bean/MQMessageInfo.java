package com.stars.easyms.mq.bean;

import com.stars.easyms.mq.dto.MQSendMessageDTO;
import com.stars.easyms.mq.enums.MQStatus;
import com.stars.easyms.mq.enums.MQType;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * MQ消息实体类
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
@Data
public class MQMessageInfo implements Serializable {

    /**
     * 主键
     */
    private String id;

    /**
     * 消息ID
     */
    private String messageId;

    /**
     * 类型：发送、接收
     */
    private String type;

    /**
     * 类型枚举
     */
    private transient MQType mqType;

    /**
     * 点对点、发布/订阅
     */
    private String destinationType;

    /**
     * mq的key，如果是rocketmq则表示topic
     */
    private String key;

    /**
     * 源系统
     */
    private String sourceSys;

    /**
     * 目标系统
     */
    private String destSys;

    /**
     * 消息体
     */
    private String message;

    /**
     * 发送状态：1：发送中，2：发送成功，3：发送失败待重试，4：发送失败不再重试
     */
    private String status;

    /**
     * 状态枚举
     */
    private transient MQStatus mqStatus;

    /**
     * 发送、处理traceId
     */
    private String traceId;

    /**
     * 发送、处理结果
     */
    private String result;

    /**
     * 失败次数,不能为空,默认0
     */
    private long failCount;

    /**
     * 重试策略,不能为空,默认"1"
     */
    private String redeliveryPolicy = "1";

    /**
     * 失败重试间隔(单位:秒),不能为空,默认60秒
     */
    private long redeliveryDelay = 60;

    /**
     * 消息创建时间
     */
    private Timestamp createTime;

    /**
     * 消息从MQ到应用端的接收时间
     */
    private Timestamp receiveTime;

    /**
     * 消息入库时间
     */
    private Timestamp storageTime;

    /**
     * 消息发送、接收执行时间
     */
    private Timestamp fireTime;

    /**
     * 消息发送、接收执行完成时间
     */
    private Timestamp finishTime;

    /**
     * 发送、接收执行用时(单位毫秒)
     */
    private Long executeTime;

    /**
     * 总用时(包含失败重试等待时间，单位毫秒)
     */
    private Long totalTime;

    /**
     * 全链路用时：接收端的finishTime - 发送端的createTime
     */
    private Long fullLinkTime;

    /**
     * 消息修改时间
     */
    private Timestamp updateTime;

    /**
     * 版本号：乐观锁,不能为空,默认0
     */
    private long version;

    /**
     * 最后存活时间
     */
    private long lastLiveTime;

    /**
     * 调度失败次数
     */
    private long scheduleFailTimes;

    /**
     * 当前心跳次数
     */
    private long heartCount;

    /**
     * 是否已经持久化到数据库
     */
    private boolean persistent;

    /**
     * 查询状态、类型的数量
     */
    private Long count;

    public void setType(String type) {
        this.type = type;
        this.mqType = MQType.forCode(type);
    }

    public void setMqType(MQType mqType) {
        this.mqType = mqType;
        this.type = mqType.getCode();
    }

    public void setStatus(String status) {
        this.status = status;
        this.mqStatus = MQStatus.forCode(status);
    }

    public void setMqStatus(MQStatus mqStatus) {
        this.mqStatus = mqStatus;
        this.status = mqStatus.getCode();
    }

    public static MQMessageInfo of(MQSendMessageDTO mqSendMessageDTO) {
        MQMessageInfo mqMessageInfo = new MQMessageInfo();
        mqMessageInfo.messageId = mqSendMessageDTO.getMessageId();
        mqMessageInfo.destinationType = mqSendMessageDTO.getDestinationType();
        mqMessageInfo.key = mqSendMessageDTO.getKey();
        mqMessageInfo.sourceSys = mqSendMessageDTO.getSourceSys();
        mqMessageInfo.createTime = mqSendMessageDTO.getCreateTime();
        mqMessageInfo.message = mqSendMessageDTO.getMessage();
        return mqMessageInfo;
    }
}
