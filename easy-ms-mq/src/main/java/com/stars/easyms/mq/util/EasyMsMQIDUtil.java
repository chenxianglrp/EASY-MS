package com.stars.easyms.mq.util;

import com.stars.easyms.base.util.DateTimeUtil;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicLong;

/**
 * <p>className: EasyMsMQIDUtil</p>
 * <p>description: EasyMsMQID工具类</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/4/28 5:03 下午
 */
public final class EasyMsMQIDUtil {

    private static final AtomicLong mqIdAutoIncrement = new AtomicLong(0);

    private static final String SERVER_RANDOM_VALUE = StringUtils.leftPad(
            String.valueOf(ThreadLocalRandom.current().nextInt(100000)), 5, "0");

    /**
     * 获取MQ主键：共32位
     * 规则：yyMMddHHmmssSSS的15位时间戳+5位当前服务器自增值+5位集群服务器随机值+7位当前服务器随机值
     */
    public static String getId() {
        // 先计算random值后计算自增值
        String randomValue = StringUtils.leftPad(String.valueOf(ThreadLocalRandom.current().nextInt(10000000)), 7, "0");
        return DateTimeUtil.getDatetimeShortYearStrWithMills(LocalDateTime.now())
                + getIncrementValue() + SERVER_RANDOM_VALUE + randomValue;
    }

    /**
     * 获取MQ消息ID：共32位
     * 规则：'M'+yyMMddHHmmssSSS的15位时间戳+5位当前服务器自增值+5位集群服务器随机值+6位当前服务器随机值
     */
    public static String getMessageId() {
        // 先计算random值后计算自增值
        String randomValue = StringUtils.leftPad(String.valueOf(ThreadLocalRandom.current().nextInt(1000000)), 6, "0");
        return "M" + DateTimeUtil.getDatetimeShortYearStrWithMills(LocalDateTime.now())
                + getIncrementValue() + SERVER_RANDOM_VALUE + randomValue;
    }

    private static String getIncrementValue() {
        long incrementValue = mqIdAutoIncrement.getAndIncrement();
        if (incrementValue < 0) {
            synchronized (EasyMsMQIDUtil.class) {
                incrementValue = mqIdAutoIncrement.getAndIncrement();
                if (incrementValue < 0) {
                    mqIdAutoIncrement.set(0);
                    incrementValue = mqIdAutoIncrement.getAndIncrement();
                }
            }
        }
        // 长度设置为5位，因为受服务器速度的限制，同一服务器同一毫秒内生成的MessageId不会超过100000条，即使极小概率超过也会有随机数降低重复风险
        String incrementValueStr = String.valueOf(incrementValue);
        int length = incrementValueStr.length();
        if (length == 5) {
            return incrementValueStr;
        }
        if (length > 5) {
            return incrementValueStr.substring(length - 5);
        }
        return StringUtils.leftPad(incrementValueStr, 5, "0");
    }

    private EasyMsMQIDUtil() {
    }
}
