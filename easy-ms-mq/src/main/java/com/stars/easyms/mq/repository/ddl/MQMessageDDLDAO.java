package com.stars.easyms.mq.repository.ddl;

import org.apache.ibatis.annotations.Param;

/**
 * <p>className: MQMessageDDLDAO</p>
 * <p>description: MQ数据库DDL语句的DAO</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/17 10:08 上午
 */
public interface MQMessageDDLDAO {

    long getCountByTableName(@Param(value = "tableName") String tableName);

    /**
     * 创建表MQ_MESSAGE_INFO、唯一索引
     */
    void createTable_mqMessageInfo();
    void alterTable_mqMessageInfo_uix_0();

    /**
     * 创建表MQ_MESSAGE_HIS
     */
    void createTable_mqMessageHis();

}
