package com.stars.easyms.mq.bean;

import lombok.Data;

/**
 * <p>className: MQSendResult</p>
 * <p>description: MQ发送结果DTO</p>
 *
 * @author guoguifang
 * @date 2019-09-16 17:31
 * @since 1.3.2
 */
@Data
public final class MQSendResult {

    /**
     * 是否发送成功
     */
    private boolean sendSuccess;

    /**
     * 发送结果
     */
    private String sendResult;

    public static MQSendResult from(boolean sendSuccess) {
        MQSendResult mqSendResult = new MQSendResult();
        mqSendResult.setSendSuccess(sendSuccess);
        return mqSendResult;
    }

    public static MQSendResult from(boolean sendSuccess, String sendResult) {
        MQSendResult mqSendResult = from(sendSuccess);
        mqSendResult.setSendResult(sendResult);
        return mqSendResult;
    }
}