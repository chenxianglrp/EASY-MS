package com.stars.easyms.mq.dto;

import com.stars.easyms.mq.bean.MQMessageInfo;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * <p>className: EasyMsMQSendMessageDTO</p>
 * <p>description: EasyMsMQ发送消息DTO</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/4/15 8:06 下午
 */
@Data
public class MQSendMessageDTO implements Serializable {

    private String messageId;

    private String destinationType;

    private String key;

    private String sourceSys;

    private String message;

    private Timestamp createTime;

    public static MQSendMessageDTO of(MQMessageInfo mqMessageInfo) {
        MQSendMessageDTO mqSendMessageDTO = new MQSendMessageDTO();
        mqSendMessageDTO.messageId = mqMessageInfo.getMessageId();
        mqSendMessageDTO.destinationType = mqMessageInfo.getDestinationType();
        mqSendMessageDTO.key = mqMessageInfo.getKey();
        mqSendMessageDTO.sourceSys = mqMessageInfo.getSourceSys();
        mqSendMessageDTO.message = mqMessageInfo.getMessage();
        mqSendMessageDTO.createTime = mqMessageInfo.getCreateTime();
        return mqSendMessageDTO;
    }

}
