package com.stars.easyms.mq.core.activemq.template;

import com.stars.easyms.mq.core.activemq.ActiveMQTemplate;
import org.springframework.jms.core.JmsMessagingTemplate;

import javax.jms.ConnectionFactory;

/**
 * 队列(点对点)模式消息发送模板
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
public class JmsQueueTemplate implements ActiveMQTemplate {

    private JmsMessagingTemplate jmsMessagingTemplate;

    @Override
    public void convertAndSend(String destinationName, final Object message) {
        jmsMessagingTemplate.convertAndSend(destinationName, message);
    }

    public JmsQueueTemplate(ConnectionFactory connectionFactory) {
        jmsMessagingTemplate = new JmsMessagingTemplate(connectionFactory);
    }
}
