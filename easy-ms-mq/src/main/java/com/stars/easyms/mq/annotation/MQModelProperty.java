package com.stars.easyms.mq.annotation;

import java.lang.annotation.*;

/**
 * MQ的Model属性注解
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MQModelProperty {

    String name() default "";

    boolean required() default false;

    String example() default "";
}
