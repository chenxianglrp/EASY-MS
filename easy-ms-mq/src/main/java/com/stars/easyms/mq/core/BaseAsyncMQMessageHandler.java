package com.stars.easyms.mq.core;

import com.stars.easyms.base.asynchronous.BaseBatchAsynchronousTask;
import com.stars.easyms.base.batch.BatchResult;
import com.stars.easyms.base.bean.LazyLoadBean;
import com.stars.easyms.mq.bean.MQMessageInfo;
import com.stars.easyms.mq.enums.MQType;
import com.stars.easyms.mq.properties.EasyMsMQProperties;

import java.util.List;

/**
 * <p>className: BaseMQMessageAsyncTask</p>
 * <p>description: MQ基础异步任务</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/4/21 1:49 下午
 */
abstract class BaseAsyncMQMessageHandler extends BaseBatchAsynchronousTask<MQMessageInfo> {

    private static final LazyLoadBean<EasyMsMQProperties> EASY_MS_MQ_PROPERTIES = new LazyLoadBean<>(EasyMsMQProperties.class);

    @Override
    protected BatchResult execute(List<MQMessageInfo> list) {
        MQScheduler.schedule(mqType(), filterRepeatMQMessageInfo(list));
        return BatchResult.ofSuccess();
    }

    protected abstract List<MQMessageInfo> filterRepeatMQMessageInfo(List<MQMessageInfo> list);

    @Override
    protected long delayTime() {
        return EASY_MS_MQ_PROPERTIES.getNonNullBean().getBatchTime();
    }

    /**
     * 失败重试次数：由于增加了redis补偿的方式不再使用异步
     */
    @Override
    protected int failRetryCount() {
        return 0;
    }

    protected abstract MQType mqType();

}
