package com.stars.easyms.mq.core;

import com.stars.easyms.base.bean.LazyLoadBean;
import com.stars.easyms.mq.bean.MQMessageInfo;
import com.stars.easyms.mq.bean.MQReceiveContext;
import com.stars.easyms.mq.redis.EasyMsMQRedisManager;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * <p>className: MQHolder</p>
 * <p>description: MQ相关的对象Holder类</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/6 1:57 下午
 */
final class MQHolder {

    private static final LazyLoadBean<EasyMsMQRedisManager> EASY_MS_MQ_REDIS_MANAGER = new LazyLoadBean<>(EasyMsMQRedisManager.class);

    /**
     * 待心跳数据
     */
    static final Map<String, MQMessageInfo> MQ_MESSAGE_INFO_PENDING_HEART_MAP = new ConcurrentHashMap<>(32);

    /**
     * 正在发送心跳中的数据
     */
    static final Map<String, MQMessageInfo> MQ_MESSAGE_INFO_HEARTING_MAP = new ConcurrentHashMap<>(32);

    /**
     * 待同步数据
     */
    static final Map<String, MQMessageInfo> MQ_MESSAGE_INFO_PENDING_SYNC_MAP = new ConcurrentHashMap<>(32);

    static final ThreadLocal<MQReceiveContext> MQ_RECEIVE_CONTENT_THREAD_LOCAL = new ThreadLocal<>();

    static EasyMsMQRedisManager getEasyMsMQRedisManager() {
        return EASY_MS_MQ_REDIS_MANAGER.getNonNullBean();
    }

    private MQHolder() {}
}
