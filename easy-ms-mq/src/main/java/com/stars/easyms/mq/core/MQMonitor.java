package com.stars.easyms.mq.core;

import com.stars.easyms.base.alarm.EasyMsAlarmAssistor;
import com.stars.easyms.base.util.DateTimeUtil;
import com.stars.easyms.mq.bean.MQMessageInfo;
import com.stars.easyms.mq.constant.MQConstants;
import com.stars.easyms.mq.enums.MQStatus;
import com.stars.easyms.mq.enums.MQType;
import com.stars.easyms.mq.registrar.EasyMsMQHolder;
import com.stars.easyms.mq.service.MQMessageService;
import com.stars.easyms.redis.lock.DistributedLock;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;

import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * <p>className: MQMonitor</p>
 * <p>description: MQ监控类</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/17 3:09 下午
 */
@Slf4j
public final class MQMonitor {

    private static final AtomicBoolean isInit = new AtomicBoolean();

    private final DistributedLock monitorLock;

    public static void init() {
        if (isInit.compareAndSet(false, true)) {
            MQMonitor mqMonitor = new MQMonitor();
            ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
            taskScheduler.setThreadNamePrefix("easy-ms-mq-monitor-");
            taskScheduler.setWaitForTasksToCompleteOnShutdown(true);
            taskScheduler.initialize();
            taskScheduler.schedule(mqMonitor::monitor, new CronTrigger("0 1 8,20 * * ? "));
        }
    }

    private void monitor() {

        boolean lockFlag = false;
        try {

            // 防止分布式环境下重复执行增加分布式锁
            lockFlag = monitorLock.lock();
            if (!lockFlag) {
                return;
            }

            // 为了避免分布式环境下重新执行增加缓存，半个小时之内只允许执行一次
            Long lastMQMonitorTime = MQHolder.getEasyMsMQRedisManager().getLastMQMonitorTime();
            if (lastMQMonitorTime != null && System.currentTimeMillis() - 1800000 < lastMQMonitorTime) {
                return;
            }

            // 如果lastMQMonitorTime为空或者超过半个小时则获取上一个小时的整点时间戳，并将其保存到redis中
            long currMQMonitorTime = getLastHourTime();
            MQHolder.getEasyMsMQRedisManager().setLastMQMonitorTime(currMQMonitorTime);

            // 查询所有的非处理成功的数据并告警
            Map<String, Object> paramMap = new HashMap<>(4);
            paramMap.put("successStatus", MQStatus.SUCCESS.getCode());
            paramMap.put("currSys", EasyMsMQHolder.getApplicationId());
            paramMap.put("limitTime", new Timestamp(currMQMonitorTime));
            List<MQMessageInfo> countWithStatusAndTypeList = MQMessageService.getInstance().getCountWithStatusAndType(paramMap);
            // 如果没有查询到则直接返回
            if (countWithStatusAndTypeList.isEmpty()) {
                return;
            }

            SortedMap<MQType, SortedMap<MQStatus, Long>> sortedMap = new TreeMap<>(Comparator.comparing(MQType::getCode));
            for (MQMessageInfo mqMessageInfo : countWithStatusAndTypeList) {
                sortedMap.computeIfAbsent(mqMessageInfo.getMqType(), k -> new TreeMap<>(Comparator.comparing(MQStatus::getCode)))
                        .computeIfAbsent(mqMessageInfo.getMqStatus(), k -> mqMessageInfo.getCount());
            }

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("截至").append(DateTimeUtil.getDatetimeNormalStr(currMQMonitorTime))
                    .append("(").append(DateTimeUtil.getDefaultTimeZone()).append("): ");
            sortedMap.forEach((mqType, m) -> {
                stringBuilder.append(mqType.getChineseName()).append("共有");
                m.forEach((mqStatus, count) ->
                        stringBuilder.append(mqStatus.getName()).append(" ").append(count).append(" 条、")
                );
                stringBuilder.deleteCharAt(stringBuilder.length() - 1);
                stringBuilder.append("消息需处理! ");
            });
            stringBuilder.append("请关注!");

            // 在日志里打印信息，然后告警
            String message = stringBuilder.toString();
            log.warn(message);
            EasyMsAlarmAssistor.sendNormalAlarmMessage(message);
        } catch (Exception e) {
            log.error("Easy-ms MQ task 'monitor' execute fail!", e);
        } finally {
            if (lockFlag) {
                try {
                    monitorLock.unlock();
                } catch (Exception e) {
                    // ignore
                }
            }
        }
    }

    private long getLastHourTime() {
        Calendar now = Calendar.getInstance();
        now.setTime(new Date());
        now.add(Calendar.HOUR, -1);
        now.set(Calendar.MINUTE, 0);
        now.set(Calendar.SECOND, 0);
        now.set(Calendar.MILLISECOND, 0);
        return now.getTime().getTime();
    }

    private MQMonitor() {
        this.monitorLock = new DistributedLock(MQConstants.MQ_SCHEDULER_LOCK, "monitorLock", 120);
    }
}
