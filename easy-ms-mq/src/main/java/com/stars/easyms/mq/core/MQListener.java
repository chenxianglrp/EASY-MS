package com.stars.easyms.mq.core;

/**
 * MQ监听器接口
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
public interface MQListener {

    /**
     * 启动监听器
     */
    void start();

    /**
     * 关闭监听器
     */
    void stop();

    /**
     * 监听器是否启动中
     * @return 启动中：true
     */
    boolean isRunning();
}