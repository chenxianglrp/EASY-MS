package com.stars.easyms.mq.core;

import com.stars.easyms.base.trace.EasyMsTraceSynchronizationManager;
import com.stars.easyms.base.util.ExceptionUtil;
import com.stars.easyms.mq.bean.MQMessageInfo;
import com.stars.easyms.mq.bean.MQSendResult;
import com.stars.easyms.mq.enums.MQStatus;
import com.stars.easyms.mq.enums.MQType;

import java.sql.Timestamp;
import java.util.List;

import static com.stars.easyms.mq.constant.MQConstants.MAX_FAIL_RESULT_LENGTH;

/**
 * <p>className: MQMessageStatusHandler</p>
 * <p>description: MQ消息状态处理器</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/6 1:48 下午
 */
final class MQMessageStatusHandler {

    static void updateMQMessageStatus(MQMessageInfo mqMessageInfo, MQSendResult mqSendResult, MQStatus status, Long maxRetryCount) {
        mqMessageInfo.setResult(mqSendResult.getSendResult());
        setMqMessageTraceId(mqMessageInfo);
        updateMQMessageStatus(mqMessageInfo, status, maxRetryCount);
    }

    static void batchUpdateMQMessageStatus(List<MQMessageInfo> mqMessageInfoList, MQSendResult mqSendResult, MQStatus status, Long maxRetryCount) {
        mqMessageInfoList.forEach(mqMessageInfo -> updateMQMessageStatus(mqMessageInfo, mqSendResult, status, maxRetryCount));
    }

    static void updateMQMessageStatus(MQMessageInfo mqMessageInfo, String result, MQStatus status, Long maxRetryCount) {
        mqMessageInfo.setResult(result);
        setMqMessageTraceId(mqMessageInfo);
        updateMQMessageStatus(mqMessageInfo, status, maxRetryCount);
    }

    static void batchUpdateMQMessageStatus(List<MQMessageInfo> mqMessageInfoList, String result, MQStatus status, Long maxRetryCount) {
        mqMessageInfoList.forEach(mqMessageInfo -> updateMQMessageStatus(mqMessageInfo, result, status, maxRetryCount));
    }

    static void updateMQMessageStatusWithException(MQMessageInfo mqMessageInfo, Exception exception, Long maxRetryCount) {
        mqMessageInfo.setResult(getThrowableMessage(ExceptionUtil.unwrapThrowable(exception)));
        setMqMessageTraceId(mqMessageInfo);
        updateMQMessageStatus(mqMessageInfo, MQStatus.FAIL_RETRY, maxRetryCount);
    }

    static void batchUpdateMQMessageStatusWithException(List<MQMessageInfo> mqMessageInfoList, Exception exception, Long maxRetryCount) {
        mqMessageInfoList.forEach(mqMessageInfo -> updateMQMessageStatusWithException(mqMessageInfo, exception, maxRetryCount));
    }

    static void updateMQMessageStatusToProcessing(MQMessageInfo mqMessageInfo) {
        updateMQMessageStatus(mqMessageInfo, MQStatus.PROCESSING, null);
    }

    static void batchUpdateMQMessageStatusToProcessing(List<MQMessageInfo> mqMessageInfoList) {
        mqMessageInfoList.forEach(MQMessageStatusHandler::updateMQMessageStatusToProcessing);
    }

    private static void updateMQMessageStatus(MQMessageInfo mqMessageInfo, MQStatus status, Long maxRetryCount) {
        String messageId = mqMessageInfo.getMessageId();
        if (MQStatus.PROCESSING == status) {
            mqMessageInfo.setFireTime(new Timestamp(System.currentTimeMillis()));
        } else if (MQStatus.SUCCESS == status) {
            if (MQType.RECEIVE == mqMessageInfo.getMqType()) {
                MQHolder.MQ_MESSAGE_INFO_PENDING_HEART_MAP.remove(messageId);
            }
            setMqMessageFinishTime(mqMessageInfo, status);
        } else if (MQStatus.FAIL_RETRY == status) {
            if (MQType.RECEIVE == mqMessageInfo.getMqType()) {
                MQHolder.MQ_MESSAGE_INFO_PENDING_HEART_MAP.remove(messageId);
            }
            long failCount = mqMessageInfo.getFailCount();
            mqMessageInfo.setFailCount(++failCount);
            if (maxRetryCount != null && failCount > maxRetryCount) {
                status = MQStatus.FAIL;
            }
            setMqMessageFinishTime(mqMessageInfo, status);
        }
        mqMessageInfo.setMqStatus(status);
        mqMessageInfo.setVersion(mqMessageInfo.getVersion() + 1);
        mqMessageInfo.setLastLiveTime(System.currentTimeMillis());

        // 如果当前数据正在心跳中，为了防止心跳数据覆盖掉当前修改状态的数据，所以将当前修改状态的数据放入待同步队列延后同步
        if (MQHolder.MQ_MESSAGE_INFO_HEARTING_MAP.containsKey(messageId)) {
            MQHolder.MQ_MESSAGE_INFO_PENDING_SYNC_MAP.put(messageId, mqMessageInfo);
        } else {
            MQHolder.getEasyMsMQRedisManager().setMQMessageInfoToRedis(mqMessageInfo.getMqType().getName(), messageId, mqMessageInfo);
        }

        // 如果不是处理中状态，则异步将数据持久化到数据库
        if (MQStatus.PROCESSING != status) {
            if (mqMessageInfo.isPersistent()) {
                AsyncUpdateMQMessageInfoHandler.handle(mqMessageInfo);
            } else {
                AsyncInsertMQMessageInfoHandler.handle(mqMessageInfo);
            }
        }
    }

    private static void setMqMessageFinishTime(MQMessageInfo mqMessageInfo, MQStatus status) {
        long currTime = System.currentTimeMillis();
        mqMessageInfo.setFinishTime(new Timestamp(currTime));
        Timestamp fireTime = mqMessageInfo.getFireTime();
        mqMessageInfo.setExecuteTime(fireTime != null ? currTime - fireTime.getTime() : 0);
        if (MQStatus.SUCCESS == status || MQStatus.FAIL == status) {
            if (MQType.SEND == mqMessageInfo.getMqType()) {
                mqMessageInfo.setTotalTime(currTime - mqMessageInfo.getCreateTime().getTime());
            } else {
                mqMessageInfo.setTotalTime(currTime - mqMessageInfo.getReceiveTime().getTime());
                mqMessageInfo.setFullLinkTime(currTime - mqMessageInfo.getCreateTime().getTime());
            }
        }
    }

    private static void setMqMessageTraceId(MQMessageInfo mqMessageInfo) {
        mqMessageInfo.setTraceId(EasyMsTraceSynchronizationManager.getTraceId());
    }

    private static String getThrowableMessage(Throwable throwable) {
        String throwableTrace = ExceptionUtil.getThrowableTrace(throwable);
        if (throwableTrace != null && throwableTrace.length() > MAX_FAIL_RESULT_LENGTH) {
            throwableTrace = throwableTrace.substring(0, MAX_FAIL_RESULT_LENGTH) + "...(详细信息请查看日志)";
        }
        return throwableTrace;
    }

}
