package com.stars.easyms.mq.service;

import com.stars.easyms.base.bean.LazyLoadBean;
import com.stars.easyms.base.util.ApplicationContextHolder;
import com.stars.easyms.datasource.enums.DatabaseType;
import com.stars.easyms.mq.constant.MQConstants;
import com.stars.easyms.mq.exception.EasyMsMQException;
import com.stars.easyms.mq.repository.ddl.MQMessageDDLDAO;
import com.stars.easyms.mq.util.EasyMsMQPackageUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.BadSqlGrammarException;

import java.sql.SQLSyntaxErrorException;

/**
 * <p>className: MQMessageDDLService</p>
 * <p>description: MQ数据库表ddl语句服务类</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/5/17 10:30 上午
 */
@Slf4j
public final class MQMessageDDLService {

    private static final MQMessageDDLService INSTANCE = new MQMessageDDLService();

    private final LazyLoadBean<MQMessageDDLDAO> mqMessageDDLDAO = new LazyLoadBean<>(this::getMQMessageDDLDAO);

    public void initTable() {
        // 判断表是否存在，若不存在则创建表
        initTable(MQConstants.TABLE_NAME_MQ_MESSAGE_INFO);
        initTable(MQConstants.TABLE_NAME_MQ_MESSAGE_HIS);
    }

    private void initTable(String tableName) {
        try {
            getMQMessageDDLDAO().getCountByTableName(tableName);
        } catch (BadSqlGrammarException badSqlGrammarException) {
            if (badSqlGrammarException.getSQLException() instanceof SQLSyntaxErrorException) {
                log.info("Table '{}' doesn't exist, ready to create...", tableName);
                try {
                    switch (tableName) {
                        case MQConstants.TABLE_NAME_MQ_MESSAGE_INFO:
                            mqMessageDDLDAO.accept(MQMessageDDLDAO::createTable_mqMessageInfo);
                            mqMessageDDLDAO.accept(MQMessageDDLDAO::alterTable_mqMessageInfo_uix_0);
                            break;
                        case MQConstants.TABLE_NAME_MQ_MESSAGE_HIS:
                            mqMessageDDLDAO.accept(MQMessageDDLDAO::createTable_mqMessageHis);
                            break;
                        default:
                            throw new EasyMsMQException("Unknown table name '{}'!", tableName);
                    }
                    log.info("Table '{}' create successfully!", tableName);
                } catch (Exception e) {
                    throw new EasyMsMQException("Create table '{}' fail, please grant user ddl permissions, or create table manually!", tableName, e);
                }
            } else {
                throw badSqlGrammarException;
            }
        }
    }

    private MQMessageDDLDAO getMQMessageDDLDAO() {
        if (EasyMsMQPackageUtil.getDefaultDbType() == DatabaseType.ORACLE) {
            return (MQMessageDDLDAO) ApplicationContextHolder.getApplicationContext().getBean("oracleMQMessageDDLDAO");
        }
        return (MQMessageDDLDAO) ApplicationContextHolder.getApplicationContext().getBean("mysqlMQMessageDDLDAO");
    }

    public static MQMessageDDLService getInstance() {
        return INSTANCE;
    }

    private MQMessageDDLService() {
    }
}
