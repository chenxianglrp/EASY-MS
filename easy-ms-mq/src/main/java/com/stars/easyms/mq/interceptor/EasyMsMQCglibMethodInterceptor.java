package com.stars.easyms.mq.interceptor;

import com.stars.easyms.base.interceptor.EasyMsCglibMethodInterceptor;
import com.stars.easyms.mq.core.BaseEasyMsMQMethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * <p>className: EasyMsMQCglibMethodInterceptor</p>
 * <p>description: EasyMsMQManager方法拦截器</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/4/14 10:30 上午
 */
public class EasyMsMQCglibMethodInterceptor extends BaseEasyMsMQMethodInterceptor implements EasyMsCglibMethodInterceptor {

    private Class<?> type;

    public EasyMsMQCglibMethodInterceptor(Class<?> type) {
        this.type = type;
    }

    @Override
    public Object intercept(MethodProxy methodProxy, Object proxy, Method method, Object[] args) throws Throwable {
        return invoke(method, args, () -> methodProxy.invokeSuper(proxy, args));
    }

    @Override
    public Class<?> getSuperType() {
        return type;
    }

}
