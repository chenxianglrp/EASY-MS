package com.stars.easyms.mq.annotation;

import java.lang.annotation.*;

/**
 * MQ的Model注解
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MQModel {
    String name();
}
