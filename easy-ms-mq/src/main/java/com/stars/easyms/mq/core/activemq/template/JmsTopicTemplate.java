package com.stars.easyms.mq.core.activemq.template;

import com.stars.easyms.mq.core.activemq.ActiveMQTemplate;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.jms.core.JmsTemplate;

import javax.jms.ConnectionFactory;

/**
 * 发布/订阅模式消息发送模板
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
public class JmsTopicTemplate implements ActiveMQTemplate {

    private JmsMessagingTemplate jmsMessagingTemplate;

    @Override
    public void convertAndSend(String destinationName, final Object message) {
        jmsMessagingTemplate.convertAndSend(destinationName, message);
    }

    public JmsTopicTemplate(ConnectionFactory connectionFactory) {
        JmsTemplate jmsTemplate = new JmsTemplate(connectionFactory);
        jmsTemplate.setPubSubDomain(true);
        jmsMessagingTemplate = new JmsMessagingTemplate(jmsTemplate);
    }
}
