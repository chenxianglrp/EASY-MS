package com.stars.easyms.mq.interceptor;

import com.stars.easyms.base.interceptor.EasyMsJdkMethodInterceptor;
import com.stars.easyms.base.util.ReflectUtil;
import com.stars.easyms.mq.bean.EasyMsMQInfo;
import com.stars.easyms.mq.core.BaseEasyMsMQMethodInterceptor;

import java.lang.reflect.Method;

/**
 * <p>className: EasyMsMQJdkMethodInterceptor</p>
 * <p>description: EasyMsMQManager方法拦截器</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/4/14 10:30 上午
 */
public class EasyMsMQJdkMethodInterceptor extends BaseEasyMsMQMethodInterceptor implements EasyMsJdkMethodInterceptor {

    private Class<?> type;

    public EasyMsMQJdkMethodInterceptor(Class<?> type) {
        this.type = type;
    }

    @Override
    public Object intercept(Object proxy, Method method, Object[] args) throws Throwable {
        return invoke(method, args, () -> ReflectUtil.invoke(proxy, method, args));
    }

    private Object interceptReceiver(Object proxy, Method method, Object[] args, EasyMsMQInfo easyMsMQInfo) throws Throwable {
        Object executeInstance = easyMsMQInfo.getExecuteInstance();
        Method executeMethod = easyMsMQInfo.getExecuteMethod();
        if (executeInstance != null && executeMethod != null) {
            return executeMethod.invoke(executeInstance, args);
        }
        return ReflectUtil.invoke(proxy, method, args);
    }

    @Override
    public Class<?> getInterfaceType() {
        return type;
    }

}
