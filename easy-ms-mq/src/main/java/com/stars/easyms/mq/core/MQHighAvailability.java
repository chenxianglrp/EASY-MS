package com.stars.easyms.mq.core;

import com.stars.easyms.base.batch.BatchResult;
import com.stars.easyms.base.util.DefaultThreadFactory;
import com.stars.easyms.base.util.ThreadUtil;
import com.stars.easyms.mq.bean.MQMessageInfoScanContext;
import com.stars.easyms.mq.constant.MQConstants;
import com.stars.easyms.mq.bean.MQMessageInfo;
import com.stars.easyms.mq.enums.MQStatus;
import com.stars.easyms.mq.enums.MQType;
import com.stars.easyms.mq.registrar.EasyMsMQHolder;
import com.stars.easyms.mq.service.MQMessageService;
import com.stars.easyms.redis.core.EasyMsCursor;
import com.stars.easyms.redis.exception.DistributedLockTimeoutException;
import com.stars.easyms.redis.lock.DistributedLock;
import lombok.extern.slf4j.Slf4j;

import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

/**
 * <p>className: MQHighAvailability</p>
 * <p>description: MQ高可用处理器</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/4/16 11:44 上午
 */
@Slf4j
public final class MQHighAvailability {

    private static final AtomicBoolean isInit = new AtomicBoolean();

    private final DistributedLock handleMissingDataForSendLock;

    private final DistributedLock handleMissingDataForReceiveLock;

    private final DistributedLock handleFailRetryMissingDataForSendLock;

    private final DistributedLock handleFailRetryMissingDataForReceiveLock;

    public static void init() {
        if (isInit.compareAndSet(false, true)) {
            MQHighAvailability mqHighAvailability = new MQHighAvailability();
            ScheduledExecutorService executorService = new ScheduledThreadPoolExecutor(4,
                    new DefaultThreadFactory().setNameFormat("easy-ms-mq-high-availability-%d").build());
            executorService.scheduleAtFixedRate(mqHighAvailability::heartProcessingDataForReceive,
                    10L, 10, TimeUnit.SECONDS);
            // 防止同一时间所有定时任务都启动，因此使用随机数作为间隔时间，从而错峰执行
            executorService.scheduleAtFixedRate(mqHighAvailability::handleMissingDataForSend,
                    20L, 20 + ThreadLocalRandom.current().nextInt(20), TimeUnit.SECONDS);
            executorService.scheduleAtFixedRate(mqHighAvailability::handleMissingDataForReceive,
                    25L, 20 + ThreadLocalRandom.current().nextInt(20), TimeUnit.SECONDS);
            executorService.scheduleWithFixedDelay(mqHighAvailability::handleFailRetryMissingData,
                    30L, 60, TimeUnit.SECONDS);
        }
    }

    /**
     * 发送处理中的数据心跳到redis，由于接收的数据可能处理时间过长，因此防止在处理中的数据由于超时重新执行因此需要发送心跳
     * 发送的数据由于需要时间较短，因此不发送心跳，此方法为单线程而且只在本进程中，因此无需加锁
     */
    private void heartProcessingDataForReceive() {
        try {
            // 由于高并发大数据量的情况下心跳会延缓造成部分数据无法正常心跳，因此进行批量心跳，已加入心跳队列的数据无法修改其状态待心跳完成后再修改
            int pendingHeartCount;
            do {
                pendingHeartCount = 0;
                long limitTime = System.currentTimeMillis() - 30000;
                for (MQMessageInfo mqMessageInfo : MQHolder.MQ_MESSAGE_INFO_PENDING_HEART_MAP.values()) {
                    if (mqMessageInfo.getLastLiveTime() < limitTime) {
                        MQHolder.MQ_MESSAGE_INFO_HEARTING_MAP.put(mqMessageInfo.getMessageId(), mqMessageInfo);
                        mqMessageInfo.setHeartCount(mqMessageInfo.getHeartCount() + 1);
                        mqMessageInfo.setLastLiveTime(System.currentTimeMillis());
                        if (++pendingHeartCount >= 100) {
                            break;
                        }
                    }
                }
                if (pendingHeartCount > 0) {
                    MQHolder.getEasyMsMQRedisManager()
                            .setMQMessageInfoToRedis(MQType.RECEIVE.getName(), MQHolder.MQ_MESSAGE_INFO_HEARTING_MAP);
                    MQHolder.MQ_MESSAGE_INFO_HEARTING_MAP.clear();
                }
                if (!MQHolder.MQ_MESSAGE_INFO_PENDING_SYNC_MAP.isEmpty()) {
                    MQHolder.getEasyMsMQRedisManager()
                            .setMQMessageInfoToRedis(MQType.RECEIVE.getName(), MQHolder.MQ_MESSAGE_INFO_PENDING_SYNC_MAP);
                    MQHolder.MQ_MESSAGE_INFO_PENDING_SYNC_MAP.clear();
                }
            } while (pendingHeartCount > 0);
        } catch (Throwable t) {
            // 防止异常引起调度线程池挂掉，这里需要整体catch住Throwable
            log.error("Easy-ms MQ task 'heartProcessingDataForReceive' execute fail!", t);
        } finally {
            MQHolder.MQ_MESSAGE_INFO_HEARTING_MAP.clear();
        }
    }

    private void handleMissingDataForSend() {
        try {
            handleMissingData(MQType.SEND);
        } catch (Throwable t) {
            // 防止异常引起调度线程池挂掉，这里需要整体catch住Throwable，分布式锁也可能出现异常因此也需要catch
            log.error("Easy-ms MQ task 'handleMissingDataForSend' execute fail!", t);
        }
    }

    private void handleMissingDataForReceive() {
        try {
            handleMissingData(MQType.RECEIVE);
        } catch (Throwable t) {
            // 防止异常引起调度线程池挂掉，这里需要整体catch住Throwable，分布式锁也可能出现异常因此也需要catch
            log.error("Easy-ms MQ task 'handleMissingDataForReceive' execute fail!", t);
        }
    }

    /**
     * 当失败重试时，会先批量修改失败重试状态为处理中状态，如果此时服务器宕机或重启等其他原因会造成未将数据同步到redis中造成数据丢失，因此需要对这些数据重新处理
     */
    private void handleFailRetryMissingData() {
        try {
            handleFailRetryMissingData(MQType.SEND);
            handleFailRetryMissingData(MQType.RECEIVE);
        } catch (Throwable t) {
            // 防止异常引起调度线程池挂掉，这里需要整体catch住Throwable，分布式锁也可能出现异常因此也需要catch
            log.error("Easy-ms MQ task 'handleFailRetryMissingData' execute fail!", t);
        }
    }

    /**
     * 处理丢失的数据：
     * 为了保证分布式环境下每个服务器都有机会处理，且不会重复，此处在redis中保存cursorId，每次获取cursorId时使用分布式锁
     * 获取redis中存在的mq消息，为了防止数据量太大，此处使用hscan的方式迭代扫描获取
     */
    private void handleMissingData(MQType mqType) {
        MQMessageInfoScanContext scanContext = null;
        do {
            // 如果超过最大执行数量则休眠5秒后再次执行
            if (AsyncPendingConfirmedDataHandler.isExceedMaxHandleCount()) {
                ThreadUtil.sleep(5, TimeUnit.SECONDS);
                continue;
            }

            // 先从redis中获取scanCursorId，如果为空则从0开始查询
            long cursorId = 0;
            final DistributedLock distributedLock = MQType.SEND == mqType ?
                    this.handleMissingDataForSendLock : this.handleMissingDataForReceiveLock;

            // 使用获取分布式锁防止重复获取到相同的值，使用block模式，如果获取超时执行下一循环，如果从redis中获取cursorId异常则
            try {
                distributedLock.blockLock(30000);
                scanContext = MQHolder.getEasyMsMQRedisManager().getScanCursorContextFromRedis(mqType.getName());
                if (scanContext != null) {
                    if (!scanContext.canScan()) {
                        distributedLock.unlock();
                        break;
                    }
                    cursorId = scanContext.getScanCursorId();
                }
            } catch (DistributedLockTimeoutException e) {
                ThreadUtil.sleep(5, TimeUnit.SECONDS);
                continue;
            } catch (Exception e) {
                log.error("Get easy-ms-mq-scanCursorId fail!", e);
                distributedLock.unlock();
                break;
            }

            // 获取ScanCursor，这里需要关闭
            try (EasyMsCursor<String, MQMessageInfo> mqMessageInfoInRedisCursor =
                         MQHolder.getEasyMsMQRedisManager()
                                 .getAllMQMessageInfoFromRedis(mqType.getName(), cursorId)) {

                // 如果查询结果为空，不需要删除原redis数据直接结束
                if (mqMessageInfoInRedisCursor == null) {
                    break;
                }

                // 如果查询到的结果显示下一个cursorId为0，则表示没有数据，保存扫描上下文到redis中
                long nextCursorId = mqMessageInfoInRedisCursor.getCursorId();
                if (scanContext == null) {
                    scanContext = new MQMessageInfoScanContext();
                }
                scanContext.setScanCursorId(nextCursorId);
                scanContext.setScanTime(System.currentTimeMillis());
                MQHolder.getEasyMsMQRedisManager().setScanCursorContextFromRedis(mqType.getName(), scanContext);

                List<MQMessageInfo> mqMessageInfoInRedisList = new ArrayList<>();
                while (mqMessageInfoInRedisCursor.hasNext()) {
                    Map.Entry<String, MQMessageInfo> next = mqMessageInfoInRedisCursor.next();
                    if (next != null) {
                        mqMessageInfoInRedisList.add(next.getValue());
                    }
                }

                // 处理redis中待确认的数据:选取redis中完成状态(成功、失败重试、失败)以及初始化、处理中状态但最后存活时间已经超过120秒的数据
                List<MQMessageInfo> pendingConfirmedDataList = mqMessageInfoInRedisList.stream()
                        .filter(mqMessageInfo -> mqMessageInfo.getLastLiveTime() < System.currentTimeMillis() - 120000)
                        .collect(Collectors.toList());

                // 重新设置LastLiveTime防止重复抓取
                pendingConfirmedDataList.forEach(mqMessageInfo -> {
                    MQStatus mqStatus = mqMessageInfo.getMqStatus();
                    if (MQStatus.INIT == mqStatus || MQStatus.PROCESSING == mqStatus) {
                        mqMessageInfo.setLastLiveTime(System.currentTimeMillis());
                        MQHolder.getEasyMsMQRedisManager()
                                .setMQMessageInfoToRedis(mqType.getName(), mqMessageInfo.getMessageId(), mqMessageInfo);
                    }
                });

                // 处理待确认数据
                AsyncPendingConfirmedDataHandler.handle(pendingConfirmedDataList, mqType);
            } catch (Exception e) {
                log.error("Get easy-ms-mq scan cursor fail!", e);
                break;
            } finally {
                distributedLock.unlock();
            }
        } while (scanContext == null || scanContext.canScan());
    }

    private void handleFailRetryMissingData(MQType mqType) {
        boolean isSend = MQType.SEND == mqType;
        try {
            final DistributedLock distributedLock = isSend ?
                    this.handleFailRetryMissingDataForSendLock : this.handleFailRetryMissingDataForReceiveLock;
            boolean lockFlag = distributedLock.lock();
            if (!lockFlag) {
                return;
            }
            try {
                Map<String, Object> paramMap = new HashMap<>(8);
                paramMap.put("initStatus", MQStatus.INIT.getCode());
                paramMap.put("type", mqType.getCode());
                paramMap.put(isSend ? "sourceSys" : "destSys", EasyMsMQHolder.getApplicationId());
                paramMap.put("perCatchSize", MQConstants.PER_CATCH_SIZE);

                List<MQMessageInfo> mqMessageInfoList;
                do {
                    paramMap.put("limitTime", new Timestamp(System.currentTimeMillis() - 120000));
                    mqMessageInfoList = MQMessageService.getInstance().getMQMessageForInit(paramMap);

                    // 修改这些数据的修改时间，防止被重复抓取
                    BatchResult<MQMessageInfo> batchResult =
                            MQMessageService.getInstance().batchUpdateMQMessageStatusToInit(mqMessageInfoList);

                    // 修改成功的数据分配任务给线程池，执行任务
                    MQFailRetryScheduler.handleFailRetryData(batchResult.getSuccessDatas(), mqType);

                } while (!mqMessageInfoList.isEmpty());
            } finally {
                distributedLock.unlock();
            }
        } catch (Throwable t) {
            // 防止异常引起调度线程池挂掉，这里需要整体catch住Throwable，分布式锁也可能出现异常因此也需要catch
            log.error("Easy-ms MQ task 'handleFailRetryMissingData({})' execute fail!", mqType.getName(), t);
        }
    }

    private MQHighAvailability() {
        this.handleMissingDataForSendLock = new DistributedLock(MQConstants.MQ_SCHEDULER_LOCK, "handleMissingDataForSendLock", 120);
        this.handleMissingDataForReceiveLock = new DistributedLock(MQConstants.MQ_SCHEDULER_LOCK, "handleMissingDataForReceiveLock", 120);
        this.handleFailRetryMissingDataForSendLock = new DistributedLock(MQConstants.MQ_SCHEDULER_LOCK, "handleFailRetryMissingDataForSendLock", 120);
        this.handleFailRetryMissingDataForReceiveLock = new DistributedLock(MQConstants.MQ_SCHEDULER_LOCK, "handleFailRetryMissingDataForReceiveLock", 120);
    }

}
