package com.stars.easyms.mq.annotation;

import com.stars.easyms.base.annotation.Manager;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * 提供mq服务的类注解
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Manager
public @interface MQManager {

    @AliasFor(annotation = Manager.class)
    String value() default "";
}
