package com.stars.easyms.mq.core.activemq;

/**
 * MQ消息发送模板接口
 *
 * @author guoguifang
 * @date 2018-04-23 13:54
 * @since 1.0.0
 */
public interface ActiveMQTemplate {

    /**
     * 转换消息类型并发送消息
     * @param destinationName 发送目标
     * @param message 消息
     */
    void convertAndSend(String destinationName, Object message);

}
