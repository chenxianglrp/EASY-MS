package com.stars.easyms.mq.monitor;

import com.stars.easyms.base.bean.LazyLoadBean;
import com.stars.easyms.monitor.MonitorRequestHandler;
import com.stars.easyms.mq.constant.MQConstants;
import com.stars.easyms.mq.properties.EasyMsMQProperties;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * EasyMs的MQ模块监控类.
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/6/29 8:16 下午
 */
public final class EasyMsMqMonitorRequestHandler implements MonitorRequestHandler {
    
    private final LazyLoadBean<EasyMsMQProperties> easyMsMQProperties = new LazyLoadBean<>(EasyMsMQProperties.class);
    
    @Override
    public Map<String, Object> getMonitorInfo() {
        Map<String, Object> returnMap = new LinkedHashMap<>();
        returnMap.put("enabled", easyMsMQProperties.getNonNullBean().isEnabled());
        return returnMap;
    }
    
    @Override
    public Map<String, Object> getBriefMonitorInfo() {
        return getMonitorInfo();
    }

    @Override
    public String getModuleName() {
        return MQConstants.MODULE_NAME;
    }

    @Override
    public int getOrder() {
        return 10;
    }
}
