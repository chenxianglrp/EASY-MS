CREATE TABLE MQ_SENDER_RECEIVER_INFO
(
    ID                VARCHAR2(32)            NOT NULL,
    MESSAGE_ID        VARCHAR2(32)            NOT NULL,
    TYPE              VARCHAR2(1)             NOT NULL,
    DESTINATION_TYPE  VARCHAR2(1) DEFAULT 'Q' NOT NULL,
    KEY               VARCHAR2(300)           NOT NULL,
    SOURCE_SYS        VARCHAR2(100),
    DEST_SYS          VARCHAR2(100),
    MESSAGE           CLOB                    NOT NULL,
    STATUS            VARCHAR2(1)             NOT NULL,
    SEND_RESULT       VARCHAR2(1000),
    FAIL_COUNT        INTEGER     DEFAULT 0   NOT NULL,
    REDELIVERY_POLICY VARCHAR2(1) DEFAULT '1' NOT NULL,
    REDELIVERY_DELAY  INTEGER     DEFAULT 60  NOT NULL,
    FIRE_TIME         TIMESTAMP(3)            NOT NULL,
    FINISH_TIME       TIMESTAMP(3),
    USED_TIME         INTEGER
);

comment on table MQ_SENDER_RECEIVER_INFO is 'MQ发送接收信息表';
comment on column MQ_SENDER_RECEIVER_INFO.ID is '物理主键';
comment on column MQ_SENDER_RECEIVER_INFO.MESSAGE_ID is 'MQ消息自定义标识';
comment on column MQ_SENDER_RECEIVER_INFO.TYPE is 'MQ消息类型--S:发送,R:接收';
comment on column MQ_SENDER_RECEIVER_INFO.DESTINATION_TYPE is 'MQ消息发送模式--Q:点对点,T:广播';
comment on column MQ_SENDER_RECEIVER_INFO.KEY is 'MQ消息发送/接收KEY,ActiveMQ对应destination,RocketMQ对应topic';
comment on column MQ_SENDER_RECEIVER_INFO.SOURCE_SYS is 'MQ消息来源系统编号';
comment on column MQ_SENDER_RECEIVER_INFO.DEST_SYS is 'MQ消息目的地系统编号';
comment on column MQ_SENDER_RECEIVER_INFO.MESSAGE is 'MQ消息内容';
comment on column MQ_SENDER_RECEIVER_INFO.STATUS is 'MQ消息发送/接收状态--1:发送中/处理中,2:发送成功/处理成功,3:发送失败/处理失败并待重新执行,4:发送失败/处理失败不再重新执行';
comment on column MQ_SENDER_RECEIVER_INFO.SEND_RESULT is 'MQ消息发送结果';
comment on column MQ_SENDER_RECEIVER_INFO.FAIL_COUNT is 'MQ消息发送/接收失败次数';
comment on column MQ_SENDER_RECEIVER_INFO.REDELIVERY_POLICY is '失败重试策略--1:重试间隔相同,2:重试间隔递进,3:重试间隔指数';
comment on column MQ_SENDER_RECEIVER_INFO.REDELIVERY_DELAY is '失败重试间隔(单位:秒)';
comment on column MQ_SENDER_RECEIVER_INFO.FIRE_TIME is '执行时间';
comment on column MQ_SENDER_RECEIVER_INFO.FINISH_TIME is '完成时间';
comment on column MQ_SENDER_RECEIVER_INFO.USED_TIME is '总用时(包含失败重试等待时间，单位毫秒)';

alter table MQ_SENDER_RECEIVER_INFO
    add constraint PK_MQ_SENDER_RECEIVER_INFO primary key (ID);
create unique index UK_MESSAGE_ID_TYPE_DEST ON mq_message_info (message_id, type, dest_sys);


CREATE TABLE MQ_SENDER_RECEIVER_HIS
(
    ID                VARCHAR2(32)            NOT NULL,
    MESSAGE_ID        VARCHAR2(32)            NOT NULL,
    TYPE              VARCHAR2(1)             NOT NULL,
    DESTINATION_TYPE  VARCHAR2(1) DEFAULT 'Q' NOT NULL,
    KEY               VARCHAR2(300)           NOT NULL,
    SOURCE_SYS        VARCHAR2(100),
    DEST_SYS          VARCHAR2(100),
    MESSAGE           CLOB                    NOT NULL,
    STATUS            VARCHAR2(1)             NOT NULL,
    SEND_RESULT       VARCHAR2(1000),
    FAIL_COUNT        INTEGER     DEFAULT 0   NOT NULL,
    REDELIVERY_POLICY VARCHAR2(1) DEFAULT '1' NOT NULL,
    REDELIVERY_DELAY  INTEGER     DEFAULT 60  NOT NULL,
    FIRE_TIME         TIMESTAMP(3)            NOT NULL,
    FINISH_TIME       TIMESTAMP(3),
    USED_TIME         INTEGER
);

comment on table MQ_SENDER_RECEIVER_HIS is 'MQ发送接收信息表';
comment on column MQ_SENDER_RECEIVER_HIS.ID is '物理主键';
comment on column MQ_SENDER_RECEIVER_HIS.MESSAGE_ID is 'MQ消息自定义标识';
comment on column MQ_SENDER_RECEIVER_HIS.TYPE is 'MQ消息类型--S:发送,R:接收';
comment on column MQ_SENDER_RECEIVER_HIS.DESTINATION_TYPE is 'MQ消息发送模式--Q:点对点,T:广播';
comment on column MQ_SENDER_RECEIVER_HIS.KEY is 'MQ消息发送/接收KEY';
comment on column MQ_SENDER_RECEIVER_HIS.SOURCE_SYS is 'MQ消息来源系统编号';
comment on column MQ_SENDER_RECEIVER_HIS.DEST_SYS is 'MQ消息目的地系统编号';
comment on column MQ_SENDER_RECEIVER_HIS.MESSAGE is 'MQ消息内容';
comment on column MQ_SENDER_RECEIVER_HIS.STATUS is 'MQ消息发送/接收状态--1:发送中/处理中,2:发送成功/处理成功,3:发送失败/处理失败并待重新执行,4:发送失败/处理失败不再重新执行';
comment on column MQ_SENDER_RECEIVER_HIS.SEND_RESULT is 'MQ消息发送结果';
comment on column MQ_SENDER_RECEIVER_HIS.FAIL_COUNT is 'MQ消息发送/接收失败次数';
comment on column MQ_SENDER_RECEIVER_HIS.REDELIVERY_POLICY is '失败重试策略--1:重试间隔相同,2:重试间隔递进,3:重试间隔指数';
comment on column MQ_SENDER_RECEIVER_HIS.REDELIVERY_DELAY is '失败重试间隔(单位:秒)';
comment on column MQ_SENDER_RECEIVER_HIS.FIRE_TIME is '执行时间';
comment on column MQ_SENDER_RECEIVER_HIS.FINISH_TIME is '完成时间';
comment on column MQ_SENDER_RECEIVER_HIS.USED_TIME is '总用时(包含失败重试等待时间，单位毫秒)';

alter table MQ_SENDER_RECEIVER_HIS
    add constraint PK_MQ_SENDER_RECEIVER_HIS primary key (ID);