DROP TABLE IF EXISTS `mq_message_info`;
CREATE TABLE `mq_message_info`
(
    `id`                VARCHAR(32)            NOT NULL comment '物理主键',
    `message_id`        VARCHAR(32)            NOT NULL comment 'MQ消息自定义标识',
    `type`              VARCHAR(1)             NOT NULL comment 'MQ消息类型--S:发送,R:接收',
    `destination_type`  VARCHAR(1) DEFAULT 'Q' NOT NULL comment 'MQ消息发送模式--Q:点对点,T:广播',
    `key`               VARCHAR(300)           NOT NULL comment 'MQ消息发送/接收KEY,ActiveMQ对应destination,RocketMQ对应topic',
    `source_sys`        VARCHAR(100)           NULL comment 'MQ消息来源系统编号',
    `dest_sys`          VARCHAR(100)           NULL comment 'MQ消息目的地系统编号',
    `message`           TEXT                   NOT NULL comment 'MQ消息内容',
    `status`            VARCHAR(1)             NOT NULL comment 'MQ消息发送/接收状态--1:发送中/处理中,2:发送成功/处理成功,3:发送失败/处理失败并待重新执行,4:发送失败/处理失败不再重新执行',
    `trace_id`          VARCHAR(50)            NULL comment 'MQ消息发送/处理traceId',
    `result`            VARCHAR(1000)          NULL comment 'MQ消息发送/处理结果',
    `fail_count`        INTEGER    DEFAULT 0   NOT NULL comment 'MQ消息发送/处理失败次数',
    `redelivery_policy` VARCHAR(1) DEFAULT '1' NOT NULL comment '失败重试策略--1:重试间隔相同,2:重试间隔递进,3:重试间隔指数',
    `redelivery_delay`  INTEGER    DEFAULT 60  NOT NULL comment '失败重试间隔(单位:秒)',
    `create_time`       DATETIME(3)            NOT NULL comment '消息创建时间',
    `receive_time`      DATETIME(3)            NULL comment '消息从MQ到应用端的接收时间(发送端为空)',
    `storage_time`      DATETIME(3)            NOT NULL comment '消息入库时间',
    `fire_time`         DATETIME(3)            NULL comment '消息发送、处理开始时间',
    `finish_time`       DATETIME(3)            NULL comment '消息发送、处理完成时间',
    `execute_time`      BIGINT                 NULL comment '发送、处理用时(单位毫秒)',
    `total_time`        BIGINT                 NULL comment '总用时(包含失败重试等待时间，单位毫秒)',
    `full_link_time`    BIGINT                 NULL comment '全链路总用时(接收端的finishTime-发送端的createTime，单位毫秒)',
    `update_time`       DATETIME(3)            NOT NULL comment '消息修改时间',
    `version`           BIGINT     DEFAULT 0   NOT NULL comment '乐观锁',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 comment ='MQ信息表';
create unique index `uk_message_id_type_dest` ON `mq_message_info` (`message_id`, `type`, `dest_sys`);


DROP TABLE IF EXISTS `mq_message_his`;
CREATE TABLE `mq_message_his`
(
    `id`                VARCHAR(32)            NOT NULL comment '物理主键',
    `message_id`        VARCHAR(32)            NOT NULL comment 'MQ消息自定义标识',
    `type`              VARCHAR(1)             NOT NULL comment 'MQ消息类型--S:发送,R:接收',
    `destination_type`  VARCHAR(1) DEFAULT 'Q' NOT NULL comment 'MQ消息发送模式--Q:点对点,T:广播',
    `key`               VARCHAR(300)           NOT NULL comment 'MQ消息发送/接收KEY,ActiveMQ对应destination,RocketMQ对应topic',
    `source_sys`        VARCHAR(100)           NULL comment 'MQ消息来源系统编号',
    `dest_sys`          VARCHAR(100)           NULL comment 'MQ消息目的地系统编号',
    `message`           TEXT                   NOT NULL comment 'MQ消息内容',
    `status`            VARCHAR(1)             NOT NULL comment 'MQ消息发送/接收状态--1:发送中/处理中,2:发送成功/处理成功,3:发送失败/处理失败并待重新执行,4:发送失败/处理失败不再重新执行',
    `trace_id`          VARCHAR(50)            NULL comment 'MQ消息发送/处理traceId',
    `result`            VARCHAR(1000)          NULL comment 'MQ消息发送/处理结果',
    `fail_count`        INTEGER    DEFAULT 0   NOT NULL comment 'MQ消息发送/处理失败次数',
    `redelivery_policy` VARCHAR(1) DEFAULT '1' NOT NULL comment '失败重试策略--1:重试间隔相同,2:重试间隔递进,3:重试间隔指数',
    `redelivery_delay`  INTEGER    DEFAULT 60  NOT NULL comment '失败重试间隔(单位:秒)',
    `create_time`       DATETIME(3)            NOT NULL comment '消息创建时间',
    `receive_time`      DATETIME(3)            NULL comment '消息从MQ到应用端的接收时间(发送端为空)',
    `storage_time`      DATETIME(3)            NOT NULL comment '消息入库时间',
    `fire_time`         DATETIME(3)            NULL comment '消息发送、处理开始时间',
    `finish_time`       DATETIME(3)            NULL comment '消息发送、处理完成时间',
    `execute_time`      BIGINT                 NULL comment '发送、处理用时(单位毫秒)',
    `total_time`        BIGINT                 NULL comment '总用时(包含失败重试等待时间，单位毫秒)',
    `full_link_time`    BIGINT                 NULL comment '全链路总用时(接收端的finishTime-发送端的createTime，单位毫秒)',
    `update_time`       DATETIME(3)            NOT NULL comment '消息修改时间',
    `version`           BIGINT     DEFAULT 0   NOT NULL comment '乐观锁',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 comment ='MQ信息历史表';