
 ${AnsiColor.GREEN}  ______                         ${AnsiColor.MAGENTA}  __  __
 ${AnsiColor.GREEN} |  ____|                        ${AnsiColor.MAGENTA} |  \/  |
 ${AnsiColor.GREEN} | |__      __ _   ___   _   _   ${AnsiColor.MAGENTA} | \  / |  ___
 ${AnsiColor.GREEN} |  __|    / _` | / __| | | | |  ${AnsiColor.MAGENTA} | |\/| | / __|
 ${AnsiColor.GREEN} | |____  | (_| | \__ \ | |_| |  ${AnsiColor.MAGENTA} | |  | | \__ \
 ${AnsiColor.GREEN} |______|  \__,_| |___/  \__, |  ${AnsiColor.MAGENTA} |_|  |_| |___/
 ${AnsiColor.GREEN}                          __/ |
 ${AnsiColor.GREEN}                         |___/

${AnsiColor.BRIGHT_BLUE}::  Easy Ms ${easy-ms.version}  ::  🤓  ::  Spring Boot ${spring-boot.version}  ::${AnsiColor.DEFAULT}
