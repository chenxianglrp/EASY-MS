package com.stars.easyms.base.util;

/**
 * <p>className: BeanPropertyUtil</p>
 * <p>description: BeanProperty工具类</p>
 *
 * @author guoguifang
 * @date 2019-12-23 13:48
 * @since 1.4.2
 */
public final class BeanPropertyUtil {

    public static String toDashedForm(String name) {
        StringBuilder result = new StringBuilder(name.length());
        boolean inIndex = false;
        for (int i = 0; i < name.length(); i++) {
            char ch = name.charAt(i);
            if (inIndex) {
                result.append(ch);
                if (ch == ']') {
                    inIndex = false;
                }
            } else {
                if (ch == '[') {
                    inIndex = true;
                    result.append(ch);
                } else {
                    ch = (ch != '_') ? ch : '-';
                    if (Character.isUpperCase(ch) && result.length() > 0 && result.charAt(result.length() - 1) != '-') {
                        result.append('-');
                    }
                    result.append(Character.toLowerCase(ch));
                }
            }
        }
        return result.toString();
    }

    private BeanPropertyUtil() {
    }
}