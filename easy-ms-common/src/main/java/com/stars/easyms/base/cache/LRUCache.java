package com.stars.easyms.base.cache;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * <p>className: LRUCache</p>
 * <p>description: 基于LRU算法的缓存</p>
 *
 * @author guoguifang
 * @date 2019-10-17 14:00
 * @since 1.3.3
 */
public class LRUCache<K, V> extends LinkedHashMap<K, V> {

    private final int maxSize;

    public LRUCache(int maxSize) {
        this(maxSize, 16, 0.75f, false);
    }

    public LRUCache(int maxSize, int initialCapacity, float loadFactor, boolean accessOrder) {
        super(initialCapacity, loadFactor, accessOrder);
        this.maxSize = maxSize;
    }

    @Override
    protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
        return this.size() > this.maxSize;
    }

}
