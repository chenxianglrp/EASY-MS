package com.stars.easyms.base.function;

/**
 * <p>className: EasyMsSupplier</p>
 * <p>description: 用来处理可抛出异常的Supplier场景</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/4/25 10:06 上午
 */
@FunctionalInterface
public interface EasyMsSupplier<T> extends EasyMsFunctionalInterface {

    T get() throws Throwable;

}
