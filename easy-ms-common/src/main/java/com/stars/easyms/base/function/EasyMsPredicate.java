package com.stars.easyms.base.function;

/**
 * <p>className: EasyMsPredicate</p>
 * <p>description: 用来处理可抛出异常的Predicate场景</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/4/23 11:19 上午
 */
@FunctionalInterface
public interface EasyMsPredicate<T> extends EasyMsFunctionalInterface {

    boolean test(T t) throws Throwable;

}
