package com.stars.easyms.base.util;

import com.plumelog.core.TraceId;

/**
 * <p>className: PlumeLogUtil</p>
 * <p>description: PlumeLog工具类</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/11/17 5:54 下午
 */
public final class PlumeLogUtil {

    public static void setTraceId(String traceId) {
        TraceId.logTraceID.set(traceId);
    }

    public static String getTraceId() {
        return TraceId.logTraceID.get();
    }

    public static void remove() {
        TraceId.logTraceID.remove();
    }

    private PlumeLogUtil() {}
}
