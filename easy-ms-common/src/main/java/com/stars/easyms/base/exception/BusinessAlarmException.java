package com.stars.easyms.base.exception;

/**
 * <p>className: BusinessAlarmException</p>
 * <p>description: 业务告警异常，区别于BusinessRestAlarmException可用于所有场景</p>
 *
 * @author guoguifang
 * @version 1.7.3
 * @date 2021/2/25 3:59 下午
 */
public class BusinessAlarmException extends BusinessException implements AlarmException {

    public BusinessAlarmException(String retMsg) {
        super(retMsg);
    }

    public BusinessAlarmException(String retMsg, Exception e) {
        super(retMsg, e);
    }

    public BusinessAlarmException(String retCode, String retMsg) {
        super(retCode, retMsg);
    }

    public BusinessAlarmException(String retCode, String retMsg, Exception e) {
        super(retCode, retMsg, e);
    }
    
    public BusinessAlarmException(String retCode, String retMsg, String errorDesc) {
        super(retCode, retMsg, errorDesc);
    }
}
