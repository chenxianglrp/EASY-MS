package com.stars.easyms.base.util;

import org.springframework.lang.Nullable;
import org.springframework.util.ClassUtils;

/**
 * <p>className: ClassUtil</p>
 * <p>description: Class类是否存在判断工具类</p>
 *
 * @author guoguifang
 * @date 2019-11-18 10:45
 * @since 1.4.0
 */
public final class ClassUtil {

    @Nullable
    public static Class<?> forName(String className) {
        try {
            return ClassUtils.forName(className, ClassUtils.getDefaultClassLoader());
        } catch (ClassNotFoundException e) {
            return null;
        }
    }

    public static boolean isExist(String className, ClassLoader classLoader) {
        return ClassUtils.isPresent(className, classLoader);
    }

    public static boolean isExist(String className) {
        return isExist(className, ClassUtils.getDefaultClassLoader());
    }

    public static boolean isExistApollo(ClassLoader classLoader) {
        return ClassUtils.isPresent("com.ctrip.framework.apollo.Config", classLoader);
    }

    public static boolean isExistApollo() {
        return isExistApollo(ClassUtils.getDefaultClassLoader());
    }

    private ClassUtil() {
    }

}