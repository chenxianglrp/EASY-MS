package com.stars.easyms.base.util;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * <p>className: EmptyLinkedBlockingQueue</p>
 * <p>description: 空阻塞队列，用于线程池使用</p>
 *
 * @author guoguifang
 * @date 2019-09-19 15:51
 * @since 1.3.2
 */
public final class EmptyLinkedBlockingQueue<E> extends LinkedBlockingQueue<E> {

    public EmptyLinkedBlockingQueue() {
        super(1);
    }

    @Override
    public boolean offer(E e) {
        return false;
    }

    @Override
    public boolean offer(E e, long timeout, TimeUnit unit) {
        return false;
    }

    @Override
    public void put(E e) {
        throw new UnsupportedOperationException();
    }
}