package com.stars.easyms.base.bridge;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>interfaceName: EasyMsUrlBlockHandler</p>
 * <p>description: 用于rest模块和sentinel模块的桥梁接口类</p>
 *
 * @author guoguifang
 * @version 1.2.2
 * @date 2019-07-26 20:04
 */
public interface EasyMsUrlBlockHandler {

    /**
     * 阻塞时处理请求
     *
     * @param request  Servlet request
     * @param response Servlet response
     * @param ex       the block exception.
     * @return 是否阻塞处理完成, true表示阻塞处理完成, false表示阻塞处理失败将默认返回"Blocked by Sentinel (flow limiting)"字符串
     */
    boolean blocked(HttpServletRequest request, HttpServletResponse response, Exception ex);
}
