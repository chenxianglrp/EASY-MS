package com.stars.easyms.base.trace.function;

/**
 * <p>className: Executor</p>
 * <p>description: 执行者，无参无返回类型</p>
 *
 * @author guoguifang
 * @version 1.7.1
 * @date 2020/12/19 3:58 下午
 */
@FunctionalInterface
public interface TraceExecutor {

    /**
     * 执行者，无参无返回类型
     *
     * @param traceId 链路id
     * @return 允许有返回值
     * @throws Throwable 允许抛出异常
     */
    Object execute(String traceId) throws Throwable;
}
