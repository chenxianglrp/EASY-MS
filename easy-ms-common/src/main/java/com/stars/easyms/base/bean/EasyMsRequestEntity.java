package com.stars.easyms.base.bean;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>className: EasyMsRestRequestContext</p>
 * <p>description: rest请求上下文</p>
 *
 * @author guoguifang
 * @date 2020/8/16 14:17
 * @version 1.6.1
 */
@Data
public class EasyMsRequestEntity implements Serializable {

    /**
     * 请求地址
     */
    private String requestPath;

    /**
     * 全局流水号，用于全链路追踪
     */
    private String traceId;

    /**
     * 调用系统
     */
    private String requestSys;

    /**
     * 请求ID
     */
    private String requestId;

    /**
     * 异步ID
     */
    private String asyncId;

    /**
     * 请求时间
     */
    private String requestTime;

    /**
     * 当前系统接收到请求的时间
     */
    private long receiveRequestTime;

    /**
     * 当前系统接收到请求的时间
     */
    private String receiveRequestTimeStr;

}
