package com.stars.easyms.base.function;

/**
 * <p>className: EasyMsFunctionalInterface</p>
 * <p>description: 用来区分是否是easy-ms归属的FunctionalInterface类型</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/4/23 11:40 上午
 */
public interface EasyMsFunctionalInterface {
}
