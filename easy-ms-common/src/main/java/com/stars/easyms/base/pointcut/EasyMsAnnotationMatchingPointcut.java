package com.stars.easyms.base.pointcut;

import org.springframework.aop.ClassFilter;
import org.springframework.aop.MethodMatcher;
import org.springframework.aop.Pointcut;
import org.springframework.lang.NonNull;

import java.lang.annotation.Annotation;
import java.util.Collections;
import java.util.List;

/**
 * <p>className: EasyMsAnnotationMatchingPointcut</p>
 * <p>description: EasyMs注解匹配切点类</p>
 *
 * @author guoguifang
 * @version 1.2.1
 * @date 2019/5/10 14:56
 */
public class EasyMsAnnotationMatchingPointcut implements Pointcut {

    private ClassFilter classFilter = ClassFilter.TRUE;

    private MethodMatcher methodMatcher = MethodMatcher.TRUE;

    public void setClassFilter(ClassFilter classFilter) {
        this.classFilter = classFilter;
    }

    public void setMethodMatcher(MethodMatcher methodMatcher) {
        this.methodMatcher = methodMatcher;
    }

    public void setClassFilter(Class<? extends Annotation> classAnnotationType, boolean checkInherited) {
        if (classAnnotationType != null) {
            this.classFilter = new EasyMsAnnotationClassFilter(Collections.singletonList(classAnnotationType), checkInherited);
        }
    }

    public void setClassFilter(List<Class<? extends Annotation>> classAnnotationTypeList, boolean checkInherited) {
        if (classAnnotationTypeList != null && !classAnnotationTypeList.isEmpty()) {
            this.classFilter = new EasyMsAnnotationClassFilter(classAnnotationTypeList, checkInherited);
        }
    }

    public void setMethodMatcher(Class<? extends Annotation> methodAnnotationType, boolean checkInherited) {
        if (methodAnnotationType != null) {
            this.methodMatcher = new EasyMsAnnotationMethodMatcher(methodAnnotationType, checkInherited);
        }
    }

    @NonNull
    @Override
    public ClassFilter getClassFilter() {
        return this.classFilter;
    }

    @NonNull
    @Override
    public MethodMatcher getMethodMatcher() {
        return this.methodMatcher;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        } else if (!(other instanceof EasyMsAnnotationMatchingPointcut)) {
            return false;
        } else {
            EasyMsAnnotationMatchingPointcut otherPointcut = (EasyMsAnnotationMatchingPointcut) other;
            return this.classFilter.equals(otherPointcut.classFilter) && this.methodMatcher.equals(otherPointcut.methodMatcher);
        }
    }

    @Override
    public int hashCode() {
        return this.classFilter.hashCode() * 37 + this.methodMatcher.hashCode();
    }

    @Override
    public String toString() {
        return this.getClass().getName() + ": " + this.classFilter + ", " + this.methodMatcher;
    }
}
