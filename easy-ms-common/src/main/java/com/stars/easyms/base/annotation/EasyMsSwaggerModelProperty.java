package com.stars.easyms.base.annotation;

import java.lang.annotation.*;

/**
 * <p>className: EasyMsSwaggerModel</p>
 * <p>description: 用来在swagger展示</p>
 *
 * @author guoguifang
 * @version 1.6.2
 * @date 2020/9/2 2:00 下午
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EasyMsSwaggerModelProperty {

    /**
     * The data type of the parameter.
     */
    String type() default "string";

    /**
     * The description of the parameter.
     */
    String description() default "";

    /**
     * Specifies if the parameter is required or not.
     */
    boolean required() default true;

    /**
     * A sample value for the property.
     */
    String example() default "";
}
