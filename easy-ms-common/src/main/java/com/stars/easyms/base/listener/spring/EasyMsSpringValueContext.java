package com.stars.easyms.base.listener.spring;

import org.springframework.core.MethodParameter;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * <p>className: EasyMsSpringValueContext</p>
 * <p>description: Spring的@Value注解上下文</p>
 *
 * @author guoguifang
 * @version 1.7.3
 * @date 2021/3/22 1:31 下午
 */
class EasyMsSpringValueContext {

    private MethodParameter methodParameter;

    private Field field;

    private Class<?> targetType;

    private Object bean;

    private String beanName;

    private String placeholder;

    EasyMsSpringValueContext(Method method, String placeholder, Object bean, String beanName) {
        this.methodParameter = new MethodParameter(method, 0);
        this.targetType = method.getParameterTypes()[0];
        this.placeholder = placeholder;
        this.bean = bean;
        this.beanName = beanName;
    }

    EasyMsSpringValueContext(Field field, String placeholder, Object bean, String beanName) {
        this.field = field;
        this.targetType = field.getType();
        this.placeholder = placeholder;
        this.bean = bean;
        this.beanName = beanName;
    }

    String getPlaceholder() {
        return placeholder;
    }

    Field getField() {
        return field;
    }

    Class<?> getTargetType() {
        return targetType;
    }

    MethodParameter getMethodParameter() {
        return methodParameter;
    }

    Object getBean() {
        return bean;
    }

    String getBeanName() {
        return beanName;
    }
}
