package com.stars.easyms.base.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * <p>className: EasyMsConfigChangeListenerRegister</p>
 * <p>description: EasyMs的nacos、apollo配置刷新监控注册类</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/11/25 10:20 上午
 */
public final class EasyMsConfigChangeListenerRegister {

    private static final Logger logger = LoggerFactory.getLogger(EasyMsConfigChangeListenerRegister.class);

    private static List<EasyMsConfigChangeListener> configRefreshListenerList = Collections.emptyList();

    static {
        try {
            // 获取所有的实现了EasyMsConfigRefreshListener接口的类
            List<EasyMsConfigChangeListener> localConfigRefreshListenerList = new ArrayList<>();
            ServiceLoader<EasyMsConfigChangeListener> configRefreshListenerServiceLoader = ServiceLoader.load(EasyMsConfigChangeListener.class);
            for (EasyMsConfigChangeListener configRefreshListener : configRefreshListenerServiceLoader) {
                logger.info("Found EasyMsConfigRefreshListener: {}!", configRefreshListener.getClass().getCanonicalName());
                localConfigRefreshListenerList.add(configRefreshListener);
            }
            configRefreshListenerList = Collections.unmodifiableList(localConfigRefreshListenerList);
        } catch (Exception ex) {
            logger.error("Initialization EasyMsConfigRefreshListener failed!", ex);
        }
    }

    public static List<EasyMsConfigChangeListener> getConfigRefreshListenerList() {
        return configRefreshListenerList;
    }
}
