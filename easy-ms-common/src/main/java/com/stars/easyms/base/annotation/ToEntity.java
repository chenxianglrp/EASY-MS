package com.stars.easyms.base.annotation;

import org.mapstruct.Mapping;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * <p>className: ToEntity</p>
 * <p>description: 使用mapstruct功能映射：从DTO映射到Entity时使用该注解</p>
 * <p>基于阿里巴巴的开发规范：表必备三字段:id,create_time,update_time，要求createTime和updateTime的类型为LocalDateTime</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/12/1 4:10 下午
 */
@Retention(RetentionPolicy.CLASS)
@Mapping(target = "id", ignore = true)
@Mapping(target = "createTime", expression = "java(java.time.LocalDateTime.now())")
@Mapping(target = "updateTime", expression = "java(java.time.LocalDateTime.now())")
public @interface ToEntity {

}
