package com.stars.easyms.base.alarm;

/**
 * <p>className: SendAlarmMessageService</p>
 * <p>description: 发送告警消息接口</p>
 *
 * @author guoguifang
 * @version 1.7.1
 * @date 2020/12/9 2:06 下午
 */
public interface SendAlarmMessageService {

    /**
     * 发送异常类型告警消息
     *
     * @param throwable 异常
     */
    void sendExceptionAlarmMessage(Throwable throwable);

    /**
     * 发送普通类型告警信息
     *
     * @param message 告警信息
     */
    void sendNormalAlarmMessage(String message);

    SendAlarmMessageService DEFAULT = new SendAlarmMessageService() {

        @Override
        public void sendExceptionAlarmMessage(Throwable throwable) {
            // blank
        }

        @Override
        public void sendNormalAlarmMessage(String message) {
            // blank
        }

    };
}
