package com.stars.easyms.base.annotation;

import java.lang.annotation.*;

/**
 * <p>className: EasyMsLogRequest</p>
 * <p>description: 是否打印请求注解</p>
 *
 * @author guoguifang
 * @version 1.7.1
 * @date 2020/12/21 11:39 上午
 */
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface EasyMsLogRequest {

    /**
     * 是否激活，默认激活
     */
    boolean enabled() default true;
}
