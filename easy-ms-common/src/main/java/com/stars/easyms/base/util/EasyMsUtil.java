package com.stars.easyms.base.util;

import com.stars.easyms.base.annotation.Transient;
import com.stars.easyms.base.constant.HttpHeaderConstants;

import java.lang.reflect.Field;

/**
 * <p>className: EasyMsUtil</p>
 * <p>description: EasyMs相关工具类</p>
 *
 * @author guoguifang
 * @version 1.6.3
 * @date 2020/9/11 11:13 上午
 */
public final class EasyMsUtil {

    /**
     * 判断是否是easy-ms相关url
     *
     * @param requestPath 请求路径
     * @return 是否是easy-ms相关url
     */
    public static boolean isEasyMsUrl(String requestPath) {
        for (String permitUrl : HttpHeaderConstants.EASY_MS_URL) {
            if (PatternMatcherUtil.doUrlPatternMatch(permitUrl, requestPath, true)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isTransientField(Field field) {
        return field.isAnnotationPresent(Transient.class);
    }

    private EasyMsUtil() {
    }
}
