package com.stars.easyms.base.trace;

/**
 * <p>className: EasyMsTraceBean</p>
 * <p>description: EasyMs自定义trace信息类</p>
 *
 * @author guoguifang
 * @version 1.7.2
 * @date 2021/1/13 4:09 下午
 */
public class EasyMsTraceBean {

    private String traceId;

    private String requestId;

    private String asyncId;

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public void setAsyncId(String asyncId) {
        this.asyncId = asyncId;
    }

    public String getTraceId() {
        return traceId;
    }

    public String getRequestId() {
        return requestId;
    }

    public String getAsyncId() {
        return asyncId;
    }
}
