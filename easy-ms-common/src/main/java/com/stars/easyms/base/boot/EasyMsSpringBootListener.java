package com.stars.easyms.base.boot;

import com.stars.easyms.base.alarm.EasyMsAlarmAssistor;
import com.stars.easyms.base.constant.EasyMsCommonConstants;
import com.stars.easyms.base.exception.ApplicationStartUpException;
import com.stars.easyms.base.util.EasyMsVersion;
import com.stars.easyms.base.util.MessageFormatUtil;
import com.stars.easyms.base.util.SpringBootUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringApplicationRunListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.Ordered;
import org.springframework.core.env.ConfigurableEnvironment;

import java.util.stream.Stream;

/**
 * <p>className: EasyMsSpringBootLoggerListener</p>
 * <p>description: SpringBoot版本的监听器</p>
 *
 * @author guoguifang
 * @date 2019-12-27 14:05
 * @since 1.5.0
 */
@Slf4j
public class EasyMsSpringBootListener implements SpringApplicationRunListener, Ordered {
    
    private static EasyMsSpringBootListener ROOT;
    
    private static final int START_MESSAGE_PREFIX_LENGTH = 8;
    
    private static final int START_MESSAGE_LINE_LENGTH = 120;
    
    private static final int START_MESSAGE_LINE_GAP_LENGTH = 10;
    
    private final String startMessagePrefix;
    
    private final String startMessageLine;
    
    private final String startMessageLineGap;
    
    public EasyMsSpringBootListener(SpringApplication application, String[] args) {
        startMessagePrefix = getLine(" ", START_MESSAGE_PREFIX_LENGTH);
        startMessageLine = getLine("=", START_MESSAGE_LINE_LENGTH);
        startMessageLineGap = getLine("=", START_MESSAGE_LINE_GAP_LENGTH);
        SpringBootUtil.setSpringApplication(application, args);
        System.setProperty(EasyMsCommonConstants.EASY_MS_PROPERTIES_PREFIX + "version", EasyMsVersion.getVersion());
        if (ROOT == null) {
            ROOT = this;
        }
    }
    
    @Override
    public void starting() {
        if (ROOT != this) {
            return;
        }
        log.info("\n\n{}{}\n{}{}{}{}\n{}{}\n", startMessagePrefix, startMessageLine, startMessagePrefix,
                startMessageLineGap, "Spring Boot Application begin to start...", startMessageLineGap,
                startMessagePrefix, startMessageLine);
    }
    
    @Override
    public void environmentPrepared(ConfigurableEnvironment environment) {
        // Intentionally blank
    }
    
    @Override
    public void contextPrepared(ConfigurableApplicationContext context) {
        // Intentionally blank
    }
    
    @Override
    public void contextLoaded(ConfigurableApplicationContext context) {
        // Intentionally blank
    }
    
    @Override
    public void started(ConfigurableApplicationContext context) {
        // Intentionally blank
    }
    
    @Override
    public void running(ConfigurableApplicationContext context) {
        if (ROOT != this) {
            return;
        }
        log.info("\n\n{}{}\n{}{}{}{}\n{}{}\n", startMessagePrefix, startMessageLine, startMessagePrefix,
                startMessageLineGap, getMarkedWords("Spring Boot Application '{}' startup success."),
                startMessageLineGap, startMessagePrefix, startMessageLine);
    }
    
    @Override
    public void failed(ConfigurableApplicationContext context, Throwable exception) {
        EasyMsAlarmAssistor
                .sendExceptionAlarmMessage(new ApplicationStartUpException("Application startup failure!", exception));
        if (ROOT != this) {
            return;
        }
        log.error("\n\n{}{}\n{}{}{}{}\n{}{}\n", startMessagePrefix, startMessageLine, startMessagePrefix,
                startMessageLineGap, getMarkedWords("Spring Boot Application '{}' startup failure!"),
                startMessageLineGap, startMessagePrefix, startMessageLine);
    }
    
    private String getLine(String str, int length) {
        StringBuilder stringBuilder = new StringBuilder();
        forAppend(stringBuilder, str, length);
        return stringBuilder.toString();
    }
    
    private String getMarkedWords(String str) {
        int length = START_MESSAGE_LINE_LENGTH - START_MESSAGE_LINE_GAP_LENGTH * 2;
        str = MessageFormatUtil.format(str, SpringBootUtil.getApplicationName());
        StringBuilder stringBuilder = new StringBuilder();
        int leftLength = (length - str.length()) / 2;
        forAppend(stringBuilder, " ", leftLength);
        stringBuilder.append(str);
        forAppend(stringBuilder, " ", length - str.length() - leftLength);
        return stringBuilder.toString();
    }
    
    private void forAppend(StringBuilder stringBuilder, String str, int length) {
        Stream.iterate(0, n -> n + 1).limit(length).forEach(i -> stringBuilder.append(str));
    }
    
    @Override
    public int getOrder() {
        return HIGHEST_PRECEDENCE;
    }
    
}