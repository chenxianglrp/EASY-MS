package com.stars.easyms.base.converter;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * <p>className: EasyMsMapStructConverter</p>
 * <p>description: EasyMs支持的mapstruct转换方法工具类</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/12/1 5:03 下午
 */
public final class EasyMsMapStructConverter {

    /**
     * 毫秒转换为LocalDateTime类型
     */
    public static LocalDateTime long2LocalDateTime(long time) {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(time), ZoneId.systemDefault());
    }

    /**
     * 布尔型转换为int类型，true为1,false为0
     */
    public static Integer boolean2Integer(Boolean b) {
        return b != null && b ? 1 : 0;
    }
}
