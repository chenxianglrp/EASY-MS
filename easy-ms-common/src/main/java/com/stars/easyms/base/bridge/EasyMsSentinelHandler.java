package com.stars.easyms.base.bridge;

/**
 * <p>interfaceName: EasyMsSentinelHandler</p>
 * <p>description: EasyMs处理sentinel的接口类</p>
 *
 * @author guoguifang
 * @version 1.2.2
 * @date 2019-07-26 13:10
 */
public interface EasyMsSentinelHandler {

    void handle();
}
