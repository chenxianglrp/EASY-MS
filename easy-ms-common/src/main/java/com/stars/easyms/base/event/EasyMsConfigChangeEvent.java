package com.stars.easyms.base.event;

import org.springframework.context.ApplicationEvent;

import java.util.Map;
import java.util.Set;

/**
 * <p>className: EasyMsConfigChangeEvent</p>
 * <p>description: 配置修改事件类</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/11/25 2:34 下午
 */
public class EasyMsConfigChangeEvent extends ApplicationEvent {

    private transient Map<String, EasyMsConfigChange> configChangeMap;

    public EasyMsConfigChangeEvent(Map<String, EasyMsConfigChange> configChangeMap) {
        super(configChangeMap);
        this.configChangeMap = configChangeMap;
    }

    public Set<String> changedKeys() {
        return configChangeMap.keySet();
    }

    public EasyMsConfigChange getChange(String key) {
        return configChangeMap.get(key);
    }

    public boolean isChanged(String key) {
        return configChangeMap.containsKey(key);
    }

    public Map<String, EasyMsConfigChange> getConfigChangeMap() {
        return configChangeMap;
    }
}
