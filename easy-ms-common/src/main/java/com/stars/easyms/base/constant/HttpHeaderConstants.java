package com.stars.easyms.base.constant;

import java.util.*;

/**
 * Request和response头相关常量类
 *
 * @author guoguifang
 * @date 2020-08-18 11:24
 * @since 1.6.1
 */
public final class HttpHeaderConstants {

    /**
     * 用于全链路跟踪，存放在请求头中
     */
    public static final String TRACE_KEY = "trace-id";

    /**
     * 请求头存放easy-ms的key，用于表明该响应体是easy-ms格式的响应体
     */
    public static final String HEADER_KEY_EASY_MS = "easy-ms-response";

    /**
     * 请求系统：存放在请求头中
     */
    public static final String HEADER_KEY_REQUEST_SYS = "request-sys";

    /**
     * 请求源，用来区分gateway的来源：存放在请求头中
     */
    public static final String HEADER_KEY_REQUEST_SOURCE = "request-source";

    /**
     * 响应系统：存放在请求头中
     */
    public static final String HEADER_KEY_RESPONSE_SYS = "response-sys";

    /**
     * 请求ID：存放在请求头中，同一链路中不同系统的requestId不同
     */
    public static final String HEADER_KEY_REQUEST_ID = "request-id";

    /**
     * 请求path: 存放在请求头中
     */
    public static final String HEADER_KEY_REQUEST_PATH = "request-path";

    /**
     * 请求时间：存放在请求头中
     */
    public static final String HEADER_KEY_REQUEST_TIME = "request-time";

    /**
     * 响应时间：存放在请求头中
     */
    public static final String HEADER_KEY_RESPONSE_TIME = "response-time";

    /**
     * 上游调用的请求ID：存放在exchange中
     */
    public static final String HEADER_KEY_UPSTREAM_REQUEST_ID = "upstream-request-id";

    /**
     * 请求ID：存放在请求头中，异步线程池链路ID
     */
    public static final String HEADER_KEY_ASYNC_ID = "async-id";

    /**
     * Dubbo请求信息
     */
    public static final String DUBBO_REQUEST_INFO = "easy-ms-dubbo-request-info";

    /**
     * 前端系统
     */
    public static final String FRONT_SYS = "web";

    /**
     * JWT前缀
     */
    public static final String HEADER_TOKEN_PREFIX = "Bearer ";

    /**
     * 用户信息：存放在请求头中
     */
    public static final String HEADER_KEY_USER_INFO = "user-info";

    /**
     * 是否记住我：存放在请求头中
     */
    public static final String HEADER_KEY_REMEMBER_ME = "remember-me";

    /**
     * Easy-Ms相关的url
     */
    public static final List<String> EASY_MS_URL = new ArrayList<>();

    static {
        EASY_MS_URL.add("/**/easy-ms-swagger");
        EASY_MS_URL.add("/**/swagger");
        EASY_MS_URL.add("/**/swagger-resources");
        EASY_MS_URL.add("/**/v2/api-docs");
        EASY_MS_URL.add("/**/webjars/**");
        EASY_MS_URL.add("/**/easy-ms/**");
        EASY_MS_URL.add("/**/datasource");
        EASY_MS_URL.add("/**/git");
        EASY_MS_URL.add("/**/redis");
        EASY_MS_URL.add("/**/mq");
        EASY_MS_URL.add("/**/schedule");
        EASY_MS_URL.add("/**/error");
    }

    private HttpHeaderConstants() {
    }

}
