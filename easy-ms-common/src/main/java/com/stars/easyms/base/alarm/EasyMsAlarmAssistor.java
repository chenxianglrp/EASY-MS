package com.stars.easyms.base.alarm;

import com.stars.easyms.base.bean.LazyLoadBean;

/**
 * <p>className: EasyMsAlarmAssistor</p>
 * <p>description: EasyMs告警辅助类：可以实现自定义的告警功能，如果不自定义可以使用easy-ms-alarm</p>
 *
 * @author guoguifang
 * @version 1.7.1
 * @date 2020/12/9 1:26 下午
 */
public final class EasyMsAlarmAssistor {
    
    private static SendAlarmMessageService sendAlarmMessageService;
    
    private static final LazyLoadBean<SendAlarmMessageService> SEND_ALARM_MESSAGE_SERVICE_LAZY_LOAD_BEAN = new LazyLoadBean<>(
            SendAlarmMessageService.class);
    
    /**
     * 提供一个自定义SendAlarmMessageService通道
     */
    public static void setSendAlarmMessageService(SendAlarmMessageService sendAlarmMessageService) {
        if (EasyMsAlarmAssistor.sendAlarmMessageService == null) {
            EasyMsAlarmAssistor.sendAlarmMessageService = sendAlarmMessageService;
        }
    }
    
    public static void sendExceptionAlarmMessage(Throwable throwable) {
        getSendAlarmMessageService().sendExceptionAlarmMessage(throwable);
    }
    
    public static void sendNormalAlarmMessage(String message) {
        getSendAlarmMessageService().sendNormalAlarmMessage(message);
    }
    
    private static SendAlarmMessageService getSendAlarmMessageService() {
        SendAlarmMessageService localSendAlarmMessageService = SEND_ALARM_MESSAGE_SERVICE_LAZY_LOAD_BEAN.getBean();
        if (localSendAlarmMessageService == null) {
            return sendAlarmMessageService != null ? sendAlarmMessageService : SendAlarmMessageService.DEFAULT;
        }
        return localSendAlarmMessageService;
    }
    
    private EasyMsAlarmAssistor() {
    }
}
