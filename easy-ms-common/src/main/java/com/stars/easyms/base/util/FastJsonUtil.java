package com.stars.easyms.base.util;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import org.springframework.http.MediaType;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>className: FastJsonUtil</p>
 * <p>description: fastJson消息转换器工具类</p>
 *
 * @author guoguifang
 * @version 1.6.4
 * @date 2020/10/22 4:33 下午
 */
public final class FastJsonUtil {

    private static FastJsonHttpMessageConverter fastJsonHttpMessageConverter;

    /**
     * 获取FastJsonHttpMessageConverter对象
     */
    public static synchronized FastJsonHttpMessageConverter getFastJsonHttpMessageConverter() {
        if (fastJsonHttpMessageConverter == null) {
            fastJsonHttpMessageConverter = new FastJsonHttpMessageConverter();
            FastJsonConfig fastJsonConfig = new FastJsonConfig();
            fastJsonConfig.setDateFormat("yyyy-MM-dd HH:mm:ss");
            fastJsonConfig.setSerializerFeatures(SerializerFeature.WriteMapNullValue, SerializerFeature.QuoteFieldNames);
            fastJsonHttpMessageConverter.setFastJsonConfig(fastJsonConfig);
            List<MediaType> supportedMediaTypes = new ArrayList<>();
            supportedMediaTypes.add(MediaType.APPLICATION_JSON);
            supportedMediaTypes.add(new MediaType("application", "*+json"));
            supportedMediaTypes.add(MediaType.TEXT_HTML);
            fastJsonHttpMessageConverter.setSupportedMediaTypes(supportedMediaTypes);
        }
        return fastJsonHttpMessageConverter;
    }
}
