package com.stars.easyms.base.exception;

import com.stars.easyms.base.util.ExceptionUtil;
import com.stars.easyms.base.util.MessageFormatUtil;

/**
 * <p>className: IllegalPropertiesException</p>
 * <p>description: 无效的属性异常</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/12/2 5:05 下午
 */
public final class IllegalPropertiesException extends RuntimeException {

    private static final long serialVersionUID = 1134894594345443632L;

    public IllegalPropertiesException(String message, Object... args) {
        super(MessageFormatUtil.format(message, ExceptionUtil.trimmedCopy(args)), ExceptionUtil.extractThrowable(args));
    }
}