package com.stars.easyms.base.event;

/**
 * <p>className: EasyMsConfigChangeType</p>
 * <p>description: 单个配置修改类型</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/11/25 2:49 下午
 */
public enum EasyMsConfigChangeType {

    /**
     * 新增配置
     */
    ADDED,

    /**
     * 修改配置
     */
    MODIFIED,

    /**
     * 删除配置
     */
    DELETED

}
