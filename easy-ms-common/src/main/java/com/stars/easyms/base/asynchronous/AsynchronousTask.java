package com.stars.easyms.base.asynchronous;

/**
 * 异步线程接口类
 *
 * @author guoguifang
 * @date 2018-10-19 10:07
 * @since 1.0.0
 */
public interface AsynchronousTask<T> {

    /**
     * 添加一个待执行的数据到队列中
     * @param t 待执行的数据
     */
    void add(T t);

}
