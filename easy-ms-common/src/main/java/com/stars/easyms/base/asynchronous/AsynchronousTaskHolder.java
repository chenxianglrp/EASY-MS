package com.stars.easyms.base.asynchronous;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>className: AsynchronousTaskHolder</p>
 * <p>description: 所有已创建的异步任务处理类的集合holder</p>
 *
 * @author guoguifang
 * @date 2020-08-21 13:54
 * @since 1.6.1
 */
public final class AsynchronousTaskHolder {

    private static List<AbstractAsynchronousTask> asynchronousTaskList = new ArrayList<>();

    static synchronized void addAsynchronousTask(AbstractAsynchronousTask asynchronousTask) {
        asynchronousTaskList.add(asynchronousTask);
    }

    public static List<AbstractAsynchronousTask> getDuplicateList() {
        List<AbstractAsynchronousTask> duplicateList;
        synchronized (AsynchronousTaskHolder.class) {
            duplicateList = new ArrayList<>(asynchronousTaskList);
        }
        return duplicateList;
    }
}
