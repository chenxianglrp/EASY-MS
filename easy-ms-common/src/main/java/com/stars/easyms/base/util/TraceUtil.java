package com.stars.easyms.base.util;

import com.stars.easyms.base.constant.EasyMsCommonConstants;
import org.apache.commons.lang3.StringUtils;

import java.util.UUID;

/**
 * <p>className: TraceUtil</p>
 * <p>description: Trace相关工具类</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/11/18 8:08 下午
 */
public final class TraceUtil {

    /**
     * 生成一个traceId
     */
    public static String getTraceId() {
        return getTraceId(null);
    }

    /**
     * 获取一个traceId，如果入参不为空则不再生成，否则生成一个新的traceId
     */
    public static String getTraceId(String traceId) {
        if (StringUtils.isBlank(traceId)) {
            String uuid = UUID.randomUUID().toString().replace("-", "");
            return uuid.substring(uuid.length() - 16);
        }
        return traceId;
    }

    public static String getAsyncIdTrace(String asyncId) {
        return asyncId != null ? "-【异步ID: " + asyncId + "】" : "";
    }

    public static String withUnknown(String str) {
        return str == null ? EasyMsCommonConstants.UNKNOWN : str;
    }

    private TraceUtil() {}
}
