package com.stars.easyms.base.function;

/**
 * 用来处理可抛出异常的Runnable场景.
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/7/29 6:32 下午
 */
@FunctionalInterface
public interface EasyMsRunnable extends EasyMsFunctionalInterface {
    
    void run() throws Throwable;

}
