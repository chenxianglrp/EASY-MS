package com.stars.easyms.base.interceptor;

import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Method;

/**
 * <p>className: EasyMsCglibMethodInterceptor</p>
 * <p>description: 方法拦截器接口</p>
 *
 * @author guoguifang
 * @version 1.7.3
 * @date 2021/3/25 7:45 下午
 */
public interface EasyMsCglibMethodInterceptor extends MethodInterceptor {

    @Override
    default Object intercept(Object proxy, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
        if (!isAllowEntry(proxy, method, args, methodProxy)) {
            return proceed(proxy, method, args, methodProxy);
        }

        if (ReflectionUtils.isEqualsMethod(method)) {
            return proxy == args[0];
        } else if (ReflectionUtils.isHashCodeMethod(method)) {
            return System.identityHashCode(proxy);
        } else if (ReflectionUtils.isToStringMethod(method)) {
            return (getSuperType() != null ? getSuperType().getName() : proxy.getClass().getName())
                    + "@" + Integer.toHexString(System.identityHashCode(proxy));
        } else if (Object.class.equals(method.getDeclaringClass())) {
            return proceed(proxy, method, args, methodProxy);
        }

        return intercept(methodProxy, proxy, method, args);
    }

    /**
     * 是否允许进入拦截器
     */
    default boolean isAllowEntry(Object proxy, Method method, Object[] args, MethodProxy methodProxy) {
        return true;
    }

    /**
     * 拦截不通过时的执行方法
     */
    default Object proceed(Object proxy, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
        return methodProxy.invokeSuper(proxy, args);
    }

    /**
     * 拦截成功后执行的方法
     */
    Object intercept(MethodProxy methodProxy, Object proxy, Method method, Object[] args) throws Throwable;

    Class<?> getSuperType();
}