package com.stars.easyms.base.util;

import com.stars.easyms.base.constant.EasyMsCommonConstants;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

/**
 * <p>className: IPUtils</p>
 * <p>description: Ip工具类</p>
 *
 * @author sungaoang
 * @version 1.6.2
 * @date 2020/9/4 8:59 上午
 */
@Slf4j
public final class IPUtils {
    
    /**
     * 获取IP地址
     * <p>
     * 使用Nginx等反向代理软件， 则不能通过request.getRemoteAddr()获取IP地址 如果使用了多级反向代理的话，X-Forwarded-For的值并不止一个，而是一串IP地址，X-Forwarded-For中第一个非unknown的有效IP字符串，则为真实IP地址
     */
    public static String getIpAddr(HttpServletRequest request) {
        String ip = null;
        try {
            Enumeration headerNames = request.getHeaderNames();
            while (ip == null && headerNames.hasMoreElements()) {
                String headerName = (String) headerNames.nextElement();
                if ("x-forwarded-for".equalsIgnoreCase(headerName)) {
                    ip = request.getHeader(headerName);
                } else if ("Proxy-Client-IP".equalsIgnoreCase(headerName)) {
                    ip = request.getHeader(headerName);
                } else if ("WL-Proxy-Client-IP".equalsIgnoreCase(headerName)) {
                    ip = request.getHeader(headerName);
                } else if ("HTTP_CLIENT_IP".equalsIgnoreCase(headerName)) {
                    ip = request.getHeader(headerName);
                } else if ("HTTP_X_FORWARDED_FOR".equalsIgnoreCase(headerName)) {
                    ip = request.getHeader(headerName);
                } else if ("X-Real-IP".equalsIgnoreCase(headerName)) {
                    ip = request.getHeader(headerName);
                }
                ip = checkAddress(ip);
            }
            if (ip == null) {
                ip = request.getRemoteAddr();
            }
            if (ip != null) {
                //对于通过多个代理的情况，最后IP为客户端真实IP,多个IP按照','分割
                int position = ip.indexOf(",");
                if (position > 0) {
                    ip = ip.substring(0, position);
                }
            }
        } catch (Exception e) {
            log.error("IPUtils ERROR ", e);
        }
        return "0:0:0:0:0:0:0:1".equals(ip) ? "127.0.0.1" : ip;
    }
    
    private static String checkAddress(String ip) {
        if (StringUtils.isEmpty(ip) || EasyMsCommonConstants.UNKNOWN.equalsIgnoreCase(ip) || !isValidAddress(ip)) {
            return null;
        }
        return ip;
    }
    
    private static boolean isValidAddress(String ip) {
        if (ip == null) {
            return false;
        }
        
        for (int i = 0; i < ip.length(); ++i) {
            char ch = ip.charAt(i);
            if (ch >= '0' && ch <= '9') {
            } else if (ch >= 'A' && ch <= 'F') {
            } else if (ch >= 'a' && ch <= 'f') {
            } else if (ch == '.' || ch == ':') {
                //
            } else {
                return false;
            }
        }
        
        return true;
    }
    
    private IPUtils() {
    }
    
}
