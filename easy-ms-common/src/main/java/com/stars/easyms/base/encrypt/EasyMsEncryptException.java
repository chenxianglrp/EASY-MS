package com.stars.easyms.base.encrypt;

import com.stars.easyms.base.util.ExceptionUtil;
import com.stars.easyms.base.util.MessageFormatUtil;

/**
 * <p>className: EasyMsEncryptException</p>
 * <p>description: 加解密异常</p>
 *
 * @author guoguifang
 * @version 1.6.1
 * @date 2020/8/28 10:05 上午
 */
public final class EasyMsEncryptException extends RuntimeException {

    public EasyMsEncryptException(String message, Object... args) {
        super(MessageFormatUtil.format(message, ExceptionUtil.trimmedCopy(args)), ExceptionUtil.extractThrowable(args));
    }

}
