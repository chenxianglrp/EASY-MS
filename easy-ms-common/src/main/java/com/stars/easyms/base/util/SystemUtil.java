package com.stars.easyms.base.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Date;
import java.util.Enumeration;

/**
 * 系统信息工具类
 *
 * @author guoguifang
 * @date 2018-10-19 10:07
 * @since 1.0.0
 */
public final class SystemUtil {

    private static final Logger logger = LoggerFactory.getLogger(SystemUtil.class);

    private static volatile String hostIp;

    private static Date startTime;

    /**
     * 获取本机ip地址，并自动区分Windows还是linux操作系统
     *
     * @return String
     */
    public static String getHostIp() {
        if (StringUtils.isBlank(hostIp)) {
            synchronized (SystemUtil.class) {
                if (StringUtils.isBlank(hostIp)) {
                    InetAddress ip = null;
                    try {
                        //如果是Windows操作系统
                        if (isWindows()) {
                            ip = InetAddress.getLocalHost();
                        }
                        //如果是Linux操作系统
                        else {
                            boolean bFindIp = false;
                            Enumeration<NetworkInterface> netInterfaces = NetworkInterface.getNetworkInterfaces();
                            while (netInterfaces.hasMoreElements()) {
                                if (bFindIp) {
                                    break;
                                }
                                NetworkInterface ni = netInterfaces.nextElement();
                                //----------特定情况，可以考虑用ni.getName判断
                                //遍历所有ip
                                Enumeration<InetAddress> ips = ni.getInetAddresses();
                                while (ips.hasMoreElements()) {
                                    ip = ips.nextElement();
                                    if (ip.isSiteLocalAddress()
                                            && !ip.isLoopbackAddress()   //127.开头的都是lookback地址
                                            && !ip.getHostAddress().contains(":")) {
                                        bFindIp = true;
                                        break;
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        logger.warn("读取系统ip地址失败", e);
                    }

                    if (null != ip) {
                        hostIp = ip.getHostAddress();
                    }
                }
            }
        }
        return hostIp;
    }

    public static synchronized Date getStartTime() {
        if (startTime == null) {
            startTime = new Date(ManagementFactory.getRuntimeMXBean().getStartTime());
        }
        return startTime;
    }

    private static boolean isWindows() {
        return System.getProperty("os.name").toLowerCase().contains("windows");
    }


    private SystemUtil() {
    }
}
