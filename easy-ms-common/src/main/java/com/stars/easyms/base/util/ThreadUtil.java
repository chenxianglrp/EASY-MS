package com.stars.easyms.base.util;

import java.util.concurrent.TimeUnit;

/**
 * <p>className: ThreadUtil</p>
 * <p>description: 线程工具类</p>
 *
 * @author guoguifang
 * @date 2019-10-12 16:58
 * @since 1.3.3
 */
public final class ThreadUtil {

    public static void sleep(long time) {
        sleep(time, TimeUnit.MILLISECONDS);
    }

    public static void sleep(long time, TimeUnit timeUnit) {
        long millis = timeUnit.toMillis(time);
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    private ThreadUtil() {
    }
}