package com.stars.easyms.base.util;

import lombok.extern.slf4j.Slf4j;

import java.security.MessageDigest;

/**
 * <p>className: IPUtils</p>
 * <p>description: Ip工具类</p>
 *
 * @author sungaoang
 * @version 1.6.2
 * @date 2020/9/4 8:59 上午
 */
@Slf4j
public final class MD5Utils {

    /**
     * md5 加密
     */
    public static String md5Encode(String value) {
        String reMd5 = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(value.getBytes());
            byte[] b = md.digest();
            int i;
            StringBuilder sb = new StringBuilder("");
            for (byte item : b) {
                i = item;
                if (i < 0) {
                    i += 256;
                }
                if (i < 16) {
                    sb.append("0");
                }
                sb.append(Integer.toHexString(i));
            }
            reMd5 = sb.toString();
        } catch (Exception e) {
            log.error("md5加密异常", e);
        }
        return reMd5;
    }

    private MD5Utils() {}

}
