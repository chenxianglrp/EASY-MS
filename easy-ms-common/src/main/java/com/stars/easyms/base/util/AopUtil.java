package com.stars.easyms.base.util;

import org.springframework.aop.framework.AopContext;

/**
 * <p>className: AopUtil</p>
 * <p>description: Aop工具类</p>
 *
 * @author guoguifang
 * @date 2019-12-12 10:02
 * @since 1.4.2
 */
@SuppressWarnings("unchecked")
public final class AopUtil {

    public static <T> T getAopProxy(Class<T> clazz) {
        try {
            return (T) AopContext.currentProxy();
        } catch (IllegalStateException e) {
            return ApplicationContextHolder.getApplicationContext().getBean(clazz);
        }
    }

    public static <T> T getAopProxy(T t) {
        try {
            return (T) AopContext.currentProxy();
        } catch (IllegalStateException e) {
            return (T) ApplicationContextHolder.getApplicationContext().getBean(t.getClass());
        }
    }

    private AopUtil() {
    }
}