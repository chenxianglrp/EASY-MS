package com.stars.easyms.base.util;

import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * <p>className: StopWatch</p>
 * <p>description: 计时器:支持异步多个任务同时执行</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/4/28 10:42 上午
 */
public class StopWatch {

    private final String id;
    private final Map<String, Task> taskMap = new ConcurrentHashMap<>(8);
    private final AtomicInteger runningTaskCount = new AtomicInteger(0);
    private volatile long startTimeNanos;
    private volatile long stopTimeNanos;

    public StopWatch(String id) {
        this.id = id;
    }

    public void start(String taskName) throws IllegalStateException {
        if (this.startTimeNanos == 0) {
            this.startTimeNanos = System.nanoTime();
        }
        this.taskMap.computeIfAbsent(taskName, n -> {
            runningTaskCount.incrementAndGet();
            this.stopTimeNanos = 0;
            return new Task(n);
        });
    }

    public void stop(String taskName) throws IllegalStateException {
        taskMap.computeIfPresent(taskName, (n, t) -> {
            t.stop();
            if (runningTaskCount.decrementAndGet() == 0) {
                this.stopTimeNanos = System.nanoTime();
            }
            return t;
        });
    }

    public String getId() {
        return id;
    }

    public boolean isRunning() {
        return this.runningTaskCount.get() > 0;
    }

    public long getStartTimeNanos() {
        return startTimeNanos;
    }

    public long getStopTimeNanos() {
        return stopTimeNanos;
    }

    public long getTotalTimeNanos() {
        if (this.stopTimeNanos != 0 && this.startTimeNanos != 0) {
            return this.stopTimeNanos - this.startTimeNanos;
        }
        return 0;
    }

    public int getTaskCount() {
        return this.taskMap.size();
    }

    public String prettyPrint() {
        String taskNameTitle = "Task name";
        AtomicInteger taskNameMaxLength = new AtomicInteger(taskNameTitle.length());
        Map<Long, Task> sortedMap = new TreeMap<>();
        this.taskMap.values().forEach(t -> {
            sortedMap.put(t.startTimeNanos, t);
            int taskNameLength = t.taskName.length();
            if (taskNameLength > taskNameMaxLength.get()) {
                taskNameMaxLength.set(taskNameLength);
            }
        });

        StringBuilder sb = new StringBuilder(shortSummary());
        sb.append('\n');
        sb.append("---------------------------------------------\n");
        sb.append(StringUtils.rightPad(taskNameTitle, taskNameMaxLength.get(), " "));
        sb.append("  ms         \n");
        sb.append("---------------------------------------------\n");
        for (Task task : sortedMap.values()) {
            sb.append(StringUtils.rightPad(task.getTaskName(), taskNameMaxLength.get(), " "))
                    .append("  ")
                    .append(StringUtils.rightPad(task.usedTimeMillis + "", 9, "")).append("  \n");
        }
        return sb.toString();
    }

    public String shortSummary() {
        return "StopWatch '" + getId() + "': running time = " + getTotalTimeNanos() + " ns";
    }

    public static final class Task {
        private final String taskName;
        private long startTimeNanos;
        private long stopTimeNanos;
        private long usedTimeNanos;
        private long usedTimeMillis;

        private Task(String taskName) {
            this.startTimeNanos = System.nanoTime();
            this.taskName = taskName;
        }

        private void stop() {
            if (this.stopTimeNanos != 0) {
                throw new IllegalStateException("Can't stop StopWatch Task(" + taskName + "): it's not running");
            }
            this.stopTimeNanos = System.nanoTime();
            this.usedTimeNanos = this.stopTimeNanos - this.startTimeNanos;
            this.usedTimeMillis = this.usedTimeNanos / 1000000;
        }

        public String getTaskName() {
            return this.taskName;
        }

        public long getStartTimeNanos() {
            return startTimeNanos;
        }

        public long getStopTimeNanos() {
            return stopTimeNanos;
        }

        public long getUsedTimeNanos() {
            return usedTimeNanos;
        }

        public long getUsedTimeMillis() {
            return usedTimeMillis;
        }
    }

}
