package com.stars.easyms.base.annotation;

import java.lang.annotation.*;

/**
 * <p>className: EasyMsAlarm</p>
 * <p>description: 是否告警注解:1.可以用于类、方法上，2.方法优先级高于类，3.可以用于异常类上但只有enabled属性有效</p>
 *
 * @author guoguifang
 * @version 1.7.2
 * @date 2021/1/18 6:03 下午
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface EasyMsAlarm {

    /**
     * 是否激活，默认激活，当注解在异常类上时其他属性无效
     */
    boolean enabled() default true;

    /**
     * 是否对异常进行告警，默认为true
     */
    boolean exception() default true;

    /**
     * 如果exception为true时有效：只有设置的包含的异常才告警，其他不告警(优先级高于excludeException)
     */
    Class<? extends Throwable>[] includeException() default {};

    /**
     * 如果exception为true时有效：只有设置的不包含的异常才不告警，其他告警
     */
    Class<? extends Throwable>[] excludeException() default {};

    /**
     * 是否对普通类型的告警信息告警，默认为true
     */
    boolean normal() default true;

}
