package com.stars.easyms.base.annotation;

import java.lang.annotation.*;

/**
 * <p>className: Transient</p>
 * <p>description: Transient字段</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/11/26 6:10 下午
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Transient {
}
