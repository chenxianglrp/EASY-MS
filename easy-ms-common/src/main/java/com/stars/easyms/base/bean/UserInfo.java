package com.stars.easyms.base.bean;

import com.stars.easyms.base.annotation.EasyMsSwaggerModelProperty;
import lombok.Data;

/**
 * <p>interfaceName: UserInfo</p>
 * <p>description: jwt保存的用户信息</p>
 *
 * @author guoguifang
 * @version 1.6.1
 * @date 2020-08-18 09:33
 */
@Data
public class UserInfo {

    @EasyMsSwaggerModelProperty(type = "integer", description = "用户ID", example = "12345")
    private Long userId;

    @EasyMsSwaggerModelProperty(description = "用户编号", example = "CT000001")
    private String userNo;

    @EasyMsSwaggerModelProperty(description = "用户手机号", example = "123456789012")
    private String mobile;

}
