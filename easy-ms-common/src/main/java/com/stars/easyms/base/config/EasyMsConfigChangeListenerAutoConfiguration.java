package com.stars.easyms.base.config;

import com.stars.easyms.base.boot.EasyMsConfigurationPropertiesBeansRegisterRunner;
import com.stars.easyms.base.listener.EasyMsConfigChangeApplicationListener;
import com.stars.easyms.base.listener.spring.EasyMsSpringValueRegister;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <p>className: EasyMsConfigChangeListenerAutoConfiguration</p>
 * <p>description: EasyMs的nacos、apollo等配置刷新类的自动配置类</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/11/25 10:31 上午
 */
@Configuration
public class EasyMsConfigChangeListenerAutoConfiguration {

    @Configuration
    @ConditionalOnClass(name = "org.springframework.cloud.context.properties.ConfigurationPropertiesBeans")
    public static class SpringCloudEnvironmentAutoConfiguration {

        @Bean
        public EasyMsConfigChangeApplicationListener easyMsConfigChangeApplicationListener(
                ApplicationContext applicationContext) {
            return new EasyMsConfigChangeApplicationListener(applicationContext);
        }

        @Bean
        public EasyMsConfigurationPropertiesBeansRegisterRunner easyMsConfigurationPropertiesBeansRegisterRunner() {
            return new EasyMsConfigurationPropertiesBeansRegisterRunner();
        }

    }

    @Bean
    public EasyMsSpringValueRegister easyMsSpringValueRegister() {
        return new EasyMsSpringValueRegister();
    }

}
