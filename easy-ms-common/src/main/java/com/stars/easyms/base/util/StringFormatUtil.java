package com.stars.easyms.base.util;

import org.apache.commons.lang3.StringUtils;

/**
 * 字符串格式化工具类
 *
 * @author guoguifang
 * @date 2018-10-19 10:07
 * @since 1.0.0
 */
public final class StringFormatUtil {

    /**
     * RequestMapping的path格式化方法
     *
     * @param path 路径字符
     * @return 格式后的字符
     */
    public static String formatRequestMappingPath(String path) {
        if (StringUtils.isBlank(path)) {
            return "/";
        }
        // 去掉首尾的"/"
        int l = path.length();
        int start = 0;
        for (int i = 0; i < l; i++) {
            if (path.charAt(i) != '/') {
                start = i;
                break;
            }
        }
        int end = 0;
        for (int i = l - 1; i >= 0; i--) {
            if (path.charAt(i) != '/') {
                end = i;
                break;
            }
        }
        String result = "/" + path.substring(start, end + 1);
        while (result.contains("//")) {
            result = result.replace("//", "/");
        }
        return result;
    }

    public static String formatBlank(String str) {
        return str.replaceAll("\\s+", " ");
    }

    private StringFormatUtil() {
    }

}
