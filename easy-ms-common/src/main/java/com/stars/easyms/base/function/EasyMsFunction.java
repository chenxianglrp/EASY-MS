package com.stars.easyms.base.function;

/**
 * <p>className: EasyMsFunction</p>
 * <p>description: 用来处理可抛出异常的Function场景</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/4/23 11:19 上午
 */
@FunctionalInterface
public interface EasyMsFunction<T, R> extends EasyMsFunctionalInterface {

    R apply(T t) throws Throwable;

}
