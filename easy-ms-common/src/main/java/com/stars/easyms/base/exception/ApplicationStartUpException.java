package com.stars.easyms.base.exception;

import com.stars.easyms.base.util.ExceptionUtil;
import com.stars.easyms.base.util.MessageFormatUtil;

/**
 * <p>className: ApplicationStartUpException</p>
 * <p>description: EasyMs启动时异常</p>
 *
 * @author guoguifang
 * @version 1.6.1
 * @date 2020/8/24 11:53 上午
 */
public final class ApplicationStartUpException extends RuntimeException {

    public ApplicationStartUpException(String message, Object... args) {
        super(MessageFormatUtil.format(message, ExceptionUtil.trimmedCopy(args)), ExceptionUtil.extractThrowable(args));
    }
}
