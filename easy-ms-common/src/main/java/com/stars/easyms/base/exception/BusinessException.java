package com.stars.easyms.base.exception;

import com.stars.easyms.base.constant.RestCodeConstants;
import com.stars.easyms.base.util.ExceptionUtil;

/**
 * <p>className: BusinessException</p>
 * <p>description: 业务异常，区别于BusinessRestException可用于所有场景</p>
 *
 * @author guoguifang
 * @version 1.7.3
 * @date 2021/2/25 3:59 下午
 */
public class BusinessException extends RuntimeException {
    
    /**
     * 返回错误编码
     */
    private String retCode;
    
    /**
     * 返回的错误显示信息
     */
    private String retMsg;
    
    /**
     * 返回的错误详细信息
     */
    private String errorDesc;
    
    public BusinessException(String retMsg) {
        super(retMsg);
        this.retCode = RestCodeConstants.DEFAULT_ERROR_CODE;
        this.retMsg = retMsg;
    }
    
    public BusinessException(String retMsg, Exception e) {
        super(ExceptionUtil.getNestedThrowable(e));
        this.retCode = RestCodeConstants.DEFAULT_ERROR_CODE;
        this.retMsg = retMsg;
        this.errorDesc = ExceptionUtil.getExceptionDesc(e);
    }
    
    public BusinessException(String retCode, String retMsg) {
        super(retMsg);
        this.retCode = retCode;
        this.retMsg = retMsg;
    }
    
    public BusinessException(String retCode, String retMsg, Exception e) {
        super(ExceptionUtil.getNestedThrowable(e));
        this.retCode = retCode;
        this.retMsg = retMsg;
        this.errorDesc = ExceptionUtil.getExceptionDesc(e);
    }
    
    public BusinessException(String retCode, String retMsg, String errorDesc) {
        super(errorDesc);
        this.retCode = retCode;
        this.retMsg = retMsg;
        this.errorDesc = errorDesc;
    }
    
    public String getRetCode() {
        return retCode;
    }
    
    public String getRetMsg() {
        return retMsg;
    }
    
    public String getErrorDesc() {
        return errorDesc;
    }
    
    @Override
    public String toString() {
        return "[retCode: " + retCode + "],[retMsg: " + retMsg + "],[errorDesc: " + errorDesc + "]";
    }
}
