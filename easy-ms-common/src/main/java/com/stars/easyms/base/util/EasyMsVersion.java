package com.stars.easyms.base.util;

import com.stars.easyms.base.constant.EasyMsCommonConstants;

/**
 * <p>className: EasyMsVersion</p>
 * <p>description: EasyMs版本号工具类</p>
 *
 * @author guoguifang
 * @date 2019-09-12 16:43
 * @since 1.3.1
 */
public final class EasyMsVersion {

    /**
     * Return the full version string of the present Spring codebase,
     * or {@code null} if it cannot be determined.
     *
     * @see Package#getImplementationVersion()
     */
    public static String getVersion() {
        String version = null;
        Package pkg = EasyMsVersion.class.getPackage();
        if (pkg != null) {
            version = pkg.getImplementationVersion();
        }
        return version != null ? version : EasyMsCommonConstants.UNKNOWN;
    }

    private EasyMsVersion() {
    }
}