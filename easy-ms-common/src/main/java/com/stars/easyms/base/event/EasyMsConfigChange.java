package com.stars.easyms.base.event;

import lombok.Getter;
import lombok.ToString;

/**
 * <p>className: EasyMsConfigChange</p>
 * <p>description: 单个配置的修改参数类</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/11/25 2:40 下午
 */
@Getter
@ToString
public class EasyMsConfigChange {

    private String propertyName;

    private Object oldValue;

    private Object newValue;

    private EasyMsConfigChangeType changeType;

    public EasyMsConfigChange(String propertyName, Object oldValue, Object newValue, EasyMsConfigChangeType changeType) {
        this.propertyName = propertyName;
        this.oldValue = oldValue;
        this.newValue = newValue;
        this.changeType = changeType;
    }
}
