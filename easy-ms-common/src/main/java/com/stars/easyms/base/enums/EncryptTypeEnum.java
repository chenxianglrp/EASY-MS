package com.stars.easyms.base.enums;

/**
 * <p>className: EncryptTypeEnum</p>
 * <p>description: 加密类型：对称加密、非对称加密</p>
 *
 * @author guoguifang
 * @version 1.7.3
 * @date 2021/3/5 6:06 下午
 */
public enum EncryptTypeEnum {

    /**
     * 对称加密
     */
    SYMMETRIC,

    /**
     * 非对称加密
     */
    ASYMMETRIC;

}
