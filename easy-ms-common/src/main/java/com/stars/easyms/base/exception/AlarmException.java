package com.stars.easyms.base.exception;

/**
 * <p>className: AlarmException</p>
 * <p>description: 告警异常接口</p>
 *
 * @author guoguifang
 * @version 1.7.3
 * @date 2021/2/25 3:57 下午
 */
public interface AlarmException {
}
