package com.stars.easyms.base.annotation;

import org.springframework.core.annotation.AliasFor;
import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * <p>annotationName: Manager</p>
 * <p>description: 通用业务处理层注解</p>
 *
 * <p>通用业务处理层，它有如下特征：
 * 1） 对第三方平台封装的层，预处理返回结果及转化异常信息；
 * 2） 对Service层通用能力的下沉，如缓存方案、中间件通用处理；
 * 3） 与DAO层交互，对DAO的业务通用能力的封装。
 *
 * @author guoguifang
 * @date 2019-11-06 15:27
 * @since 1.3.3
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface Manager {

    /**
     * 该值可以指示逻辑组件名称的建议，以便在自动检测组件时将其转换为Spring bean。
     *
     * @return 建议的组件名称(如果有)(否则为空字符串)
     */
    @AliasFor(annotation = Component.class)
    String value() default "";

}
