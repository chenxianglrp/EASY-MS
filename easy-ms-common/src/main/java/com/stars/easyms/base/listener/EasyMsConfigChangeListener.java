package com.stars.easyms.base.listener;

import com.stars.easyms.base.event.EasyMsConfigChangeEvent;
import com.stars.easyms.base.util.ApplicationContextHolder;

/**
 * <p>className: EasyMsConfigChangeListener</p>
 * <p>description: nacos、apllo等配置刷新监听接口</p>
 *
 * @author guoguifang
 * @version 1.7.0
 * @date 2020/11/25 10:19 上午
 */
public interface EasyMsConfigChangeListener {

    /**
     * 监听配置修改事件
     *
     * @param configChangeEvent 配置修改事件
     */
    void listen(EasyMsConfigChangeEvent configChangeEvent);

    /**
     * 监听的配置的前缀，只有校验通过的才监听，默认监听所有
     *
     * @return 正则表达式
     */
    default String listenConfigPrefix() {
        return null;
    }

    /**
     * 获取属性类对象
     *
     * @param clazz 属性类
     * @return 属性类对象
     */
    default <T> T getProperties(Class<T> clazz) {
        return ApplicationContextHolder.getApplicationContext().getBean(clazz);
    }

}
