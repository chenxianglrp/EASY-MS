package com.stars.easyms.base.bean;

import com.stars.easyms.base.function.EasyMsConsumer;
import com.stars.easyms.base.function.EasyMsFunction;
import com.stars.easyms.base.util.ApplicationContextHolder;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * <p>className: LazyLoadBean</p>
 * <p>description: 懒加载bean</p>
 *
 * @author guoguifang
 * @version 1.7.1
 * @date 2020/12/22 10:02 上午
 */
public class LazyLoadBean<T> {
    
    private Class<T> beanClass;
    
    private Supplier<T> defaultBeanSupplier;
    
    private T bean;
    
    private boolean loaded;
    
    private final ReentrantLock lock = new ReentrantLock();
    
    public LazyLoadBean(@NonNull Class<T> beanClass) {
        this.beanClass = beanClass;
    }
    
    public LazyLoadBean(@NonNull Supplier<T> defaultBeanSupplier) {
        this.defaultBeanSupplier = defaultBeanSupplier;
    }
    
    public LazyLoadBean(@NonNull Class<T> beanClass, @NonNull Supplier<T> defaultBeanSupplier) {
        this.beanClass = beanClass;
        this.defaultBeanSupplier = defaultBeanSupplier;
    }
    
    public static <T> LazyLoadBean<T> of(@NonNull Class<T> beanClass) {
        return new LazyLoadBean<>(beanClass);
    }
    
    public static <T> LazyLoadBean<T> of(@NonNull Supplier<T> defaultBeanSupplier) {
        return new LazyLoadBean<>(defaultBeanSupplier);
    }
    
    public static <T> LazyLoadBean<T> of(@NonNull Class<T> beanClass, @NonNull Supplier<T> defaultBeanSupplier) {
        return new LazyLoadBean<>(beanClass, defaultBeanSupplier);
    }
    
    public void accept(@NonNull Consumer<T> consumer) {
        T localBean = getBean();
        notNull(localBean);
        consumer.accept(localBean);
    }
    
    public void acceptWithThrowable(@NonNull EasyMsConsumer<T> consumer) throws Throwable {
        T localBean = getBean();
        notNull(localBean);
        consumer.accept(localBean);
    }
    
    public void acceptIfBeanExist(@NonNull Consumer<T> consumer) {
        T localBean = getBean();
        if (localBean != null) {
            consumer.accept(localBean);
        }
    }
    
    public void acceptWithThrowableIfBeanExist(@NonNull EasyMsConsumer<T> consumer) throws Throwable {
        T localBean = getBean();
        if (localBean != null) {
            consumer.accept(localBean);
        }
    }
    
    public <R> R apply(@NonNull Function<T, R> function) {
        T localBean = getBean();
        notNull(localBean);
        return function.apply(localBean);
    }
    
    public <R> R applyWithThrowable(@NonNull EasyMsFunction<T, R> function) throws Throwable {
        T localBean = getBean();
        notNull(localBean);
        return function.apply(localBean);
    }
    
    @NonNull
    public T getNonNullBean() {
        T localBean = getBean();
        notNull(localBean);
        return localBean;
    }
    
    @Nullable
    public T getBean() {
        if (!loaded) {
            lock.lock();
            try {
                if (!loaded) {
                    if (beanClass != null) {
                        bean = ApplicationContextHolder.getBean(beanClass);
                    }
                    if (bean == null && defaultBeanSupplier != null) {
                        bean = defaultBeanSupplier.get();
                    }
                    loaded = true;
                }
            } finally {
                lock.unlock();
            }
        }
        return bean;
    }
    
    private void notNull(T localBean) {
        if (localBean == null) {
            if (beanClass != null) {
                throw new IllegalStateException("Cant't find bean for class '" + beanClass.getName() + "'!");
            }
            throw new IllegalStateException("bean is null!");
        }
    }
}
