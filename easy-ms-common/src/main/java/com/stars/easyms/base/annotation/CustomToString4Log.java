package com.stars.easyms.base.annotation;

import java.lang.annotation.*;

/**
 * <p>className: CustomToString</p>
 * <p>description: 使用自定义toString方法输出日志</p>
 *
 * @author guoguifang
 * @version 1.6.3
 * @date 2020/9/19 6:50 下午
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CustomToString4Log {
}
