package com.stars.easyms.base.util;

import com.alibaba.fastjson.JSON;
import com.stars.easyms.base.bean.UserInfo;
import com.stars.easyms.base.trace.EasyMsTraceSynchronizationManager;
import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.Nullable;

/**
 * <p>className: UserUtil</p>
 * <p>description: 用户工具类</p>
 *
 * @author guoguifang
 * @version 1.6.2
 * @date 2020/9/5 3:35 下午
 */
public final class UserUtil {

    @Nullable
    public static UserInfo getUserInfo() {
        return getUserInfo(UserInfo.class);
    }

    @Nullable
    public static <T> T getUserInfo(Class<T> userInfoClass) {
        String userInfoStr = EasyMsTraceSynchronizationManager.getDecodedCurrentUserInfo();
        if (StringUtils.isNotBlank(userInfoStr)) {
            return JSON.parseObject(userInfoStr, userInfoClass);
        }
        return null;
    }

    private UserUtil() {}

}
