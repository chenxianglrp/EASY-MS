package com.stars.easyms.base.function;

/**
 * <p>className: EasyMsConsumer</p>
 * <p>description: 用来处理可抛出异常的Consumer场景</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/4/23 11:19 上午
 */
@FunctionalInterface
public interface EasyMsConsumer<T> extends EasyMsFunctionalInterface {

    void accept(T t) throws Throwable;

}
