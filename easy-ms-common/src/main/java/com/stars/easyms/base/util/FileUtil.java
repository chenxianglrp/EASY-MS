package com.stars.easyms.base.util;

import org.springframework.core.io.ClassPathResource;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * <p>className: FileUtil</p>
 * <p>description: 文件工具类</p>
 *
 * @author guoguifang
 * @version 1.2.2
 * @date 2019-08-01 18:21
 */
public final class FileUtil {

    public static final String CLASSPATH_URL_PREFIX = "classpath:";

    private static final String URL_PROTOCOL_FILE = "file";

    /**
     * 根据资源路径获取file文件，该方法不可获取jar包中的file资源，若想获取jar包中file资源请使用{@link FileUtil#getClassPathResource}
     *
     * @param resourceLocation 资源路径，若在classpath目录下需要以classpath:开头
     * @return file文件
     * @throws FileNotFoundException 找不到相应文件时抛出，若在jar包中即使存在也会抛出该异常
     */
    public static File getFile(String resourceLocation) throws FileNotFoundException {
        Assert.notNull(resourceLocation, "Resource location must not be null");
        if (resourceLocation.startsWith(CLASSPATH_URL_PREFIX)) {
            String path = resourceLocation.substring(CLASSPATH_URL_PREFIX.length());
            ClassPathResource cpr = new ClassPathResource(path);
            try {
                return cpr.getFile();
            } catch (IOException e) {
                throw new FileNotFoundException("File [" + resourceLocation + "] not found!");
            }
        }
        URL resourceUrl;
        try {
            resourceUrl = new URL(resourceLocation);
            if (!URL_PROTOCOL_FILE.equals(resourceUrl.getProtocol())) {
                throw new FileNotFoundException("URL cannot be resolved to absolute file path because it does not reside in the file system: " + resourceUrl);
            }
        } catch (MalformedURLException ex) {
            return new File(resourceLocation);
        }
        try {
            return new File(new URI(StringUtils.replace(resourceUrl.toString(), " ", "%20")).getSchemeSpecificPart());
        } catch (URISyntaxException ex) {
            return new File(resourceUrl.getFile());
        }
    }

    /**
     * 获取classpath目录下的文件资源，可以获取到jar包中的资源，必须以classpath:开头的才可使用该方法，否则返回null，非jar包资源可使用{@link FileUtil#getFile}
     *
     * @param classpathResourceLocation classpath资源路径
     * @return classpath资源
     */
    @Nullable
    public static ClassPathResource getClassPathResource(String classpathResourceLocation) {
        Assert.notNull(classpathResourceLocation, "Class Path Resource location must not be null");
        if (classpathResourceLocation.startsWith(CLASSPATH_URL_PREFIX)) {
            classpathResourceLocation = classpathResourceLocation.substring(CLASSPATH_URL_PREFIX.length());
            return new ClassPathResource(classpathResourceLocation);
        }
        return null;
    }

    private FileUtil() {
    }
}