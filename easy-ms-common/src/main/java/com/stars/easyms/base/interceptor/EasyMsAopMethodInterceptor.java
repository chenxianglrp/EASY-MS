package com.stars.easyms.base.interceptor;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

/**
 * <p>className: EasyMsAopMethodInterceptor</p>
 * <p>description: 方法拦截器抽象类</p>
 *
 * @author guoguifang
 * @version 1.2.1
 * @date 2019-05-10 15:46
 */
public interface EasyMsAopMethodInterceptor extends MethodInterceptor {

    @Override
    default Object invoke(MethodInvocation methodInvocation) throws Throwable {
        if (!isAllowEntry(methodInvocation)) {
            return proceed(methodInvocation);
        }

        // 如果是底层Object对象的方法则跳过拦截
        if (ignoreObjectMethod() && Object.class.equals(methodInvocation.getMethod().getDeclaringClass())) {
            return proceed(methodInvocation);
        }

        return intercept(methodInvocation);
    }

    /**
     * 是否需要忽略Object类的方法
     */
    default boolean ignoreObjectMethod() {
        return true;
    }

    /**
     * 是否允许进入拦截器
     */
    default boolean isAllowEntry(MethodInvocation methodInvocation) {
        return true;
    }

    /**
     * 拦截不通过时的执行方法
     *
     * @param methodInvocation 方法拦截对象
     */
    default Object proceed(MethodInvocation methodInvocation) throws Throwable {
        return methodInvocation.proceed();
    }

    /**
     * 拦截成功后执行的方法
     *
     * @param methodInvocation 方法拦截对象
     */
    Object intercept(MethodInvocation methodInvocation) throws Throwable;

}