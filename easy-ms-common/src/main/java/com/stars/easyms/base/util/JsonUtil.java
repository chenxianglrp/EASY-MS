package com.stars.easyms.base.util;

import com.alibaba.fastjson.JSON;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * <p>className: JsonUtil</p>
 * <p>description: Json工具类</p>
 *
 * @author guoguifang
 * @version 1.6.2
 * @date 2020/9/6 11:34 上午
 */
public final class JsonUtil {

    public static boolean isJson(String text) {
        if (StringUtils.isBlank(text)) {
            return false;
        }
        text = text.trim();
        return text.length() >= 2 && text.startsWith("{") && text.endsWith("}");
    }

    /**
     * 检查是否是有效的json字符串并且返回是否是空json
     *
     * @param jsonStr json字符串
     * @return 是否是空json
     */
    public static boolean checkAndReturnIsBlankJson(String jsonStr) {
        if (!isJson(jsonStr)) {
            throw new IllegalArgumentException("请求数据不是有效的json数据!");
        }
        String middleStr = jsonStr.substring(1, jsonStr.length() - 1);
        if (StringUtils.isBlank(middleStr)) {
            return true;
        }
        if (!middleStr.contains(":")) {
            throw new IllegalArgumentException("请求数据不是有效的json数据!");
        }
        return false;
    }

    public static String toJSONString(Object obj) {
        if (obj instanceof String) {
            return (String) obj;
        }
        return JSON.toJSONString(obj);
    }

    public static Object parseObject(String text, Type type) {
        if (type == null || String.class.equals(type)) {
            return text;
        }
        if (!(type instanceof ParameterizedType)) {
            return JSON.parseObject(text, type);
        }
        ParameterizedType parameterizedType = (ParameterizedType) type;
        return GenericTypeUtil.parseObject(text, parameterizedType, parameterizedType.getRawType());
    }

    public static Object parseObject(String text, Type type, Class<?> clazz) {
        if ((type == null && clazz == null) || String.class.equals(type)) {
            return text;
        }
        if (type != null && !(type instanceof ParameterizedType)) {
            return JSON.parseObject(text, type);
        }
        return GenericTypeUtil.parseObject(text, (ParameterizedType) type, clazz);
    }

    private JsonUtil() {
    }
}
