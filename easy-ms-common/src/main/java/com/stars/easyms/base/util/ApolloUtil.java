package com.stars.easyms.base.util;

import com.ctrip.framework.apollo.build.ApolloInjector;
import com.ctrip.framework.apollo.util.ConfigUtil;
import com.stars.easyms.base.constant.EasyMsCommonConstants;

/**
 * <p>className: ApolloUtil</p>
 * <p>description: Apollo工具类</p>
 *
 * @author guoguifang
 * @date 2019-08-30 14:42
 * @since 1.3.0
 */
public final class ApolloUtil {

    private static final ConfigUtil CONFIG_UTIL = ApolloInjector.getInstance(ConfigUtil.class);

    public static String getAppId() {
        return System.getProperty(EasyMsCommonConstants.APP_ID);
    }

    public static String getEnv() {
        return CONFIG_UTIL.getApolloEnv().name();
    }

    public static String getNamespace() {
        // namespace default application
        String namespaces = PropertyPlaceholderUtil.replace(EasyMsCommonConstants.APOLLO_BOOTSTRAP_NAMESPACES,
                EasyMsCommonConstants.NAMESPACE_APPLICATION);
        return namespaces.split(",")[0];
    }

    public static String getCluster() {
        return CONFIG_UTIL.getCluster();
    }

    private ApolloUtil() {}
}