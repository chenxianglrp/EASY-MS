package com.stars.easyms.base.util;

import java.lang.reflect.Method;

/**
 * <p>className: SkywalkingUtil</p>
 * <p>description: Skywalking工具类</p>
 *
 * @author guoguifang
 * @version 1.8.0
 * @date 2021/4/9 11:17 上午
 */
public final class SkywalkingUtil {

    private static final String SKYWALKING_CONTEXT_MANAGER_CLASS_NAME = "org.apache.skywalking.apm.agent.core.context.ContextManager";

    private static final String GET_GLOBAL_TRACE_ID_METHOD_NAME = "getGlobalTraceId";

    private static final String GET_SEGMENT_ID_METHOD_NAME = "getSegmentId";

    private static final String GET_SPAN_ID_METHOD_NAME = "getSpanId";

    private static final String EMPTY_TRACE_CONTEXT_ID = "N/A";

    private static Class<?> SKYWALKING_CONTEXT_MANAGER_CLASS;

    private static Method GET_GLOBAL_TRACE_ID_METHOD;

    private static Method GET_SEGMENT_ID_METHOD;

    private static Method GET_SPAN_ID_METHOD;

    static {
        SKYWALKING_CONTEXT_MANAGER_CLASS = ClassUtil.forName(SKYWALKING_CONTEXT_MANAGER_CLASS_NAME);
        if (SKYWALKING_CONTEXT_MANAGER_CLASS != null) {
            GET_GLOBAL_TRACE_ID_METHOD = ReflectUtil.getMatchingMethod(SKYWALKING_CONTEXT_MANAGER_CLASS, GET_GLOBAL_TRACE_ID_METHOD_NAME);
            GET_SEGMENT_ID_METHOD = ReflectUtil.getMatchingMethod(SKYWALKING_CONTEXT_MANAGER_CLASS, GET_SEGMENT_ID_METHOD_NAME);
            GET_SPAN_ID_METHOD = ReflectUtil.getMatchingMethod(SKYWALKING_CONTEXT_MANAGER_CLASS, GET_SPAN_ID_METHOD_NAME);
        }
    }

    public static String getTraceId() {
        if (GET_GLOBAL_TRACE_ID_METHOD != null) {
            try {
                String str = (String) GET_GLOBAL_TRACE_ID_METHOD.invoke(SKYWALKING_CONTEXT_MANAGER_CLASS);
                return EMPTY_TRACE_CONTEXT_ID.equals(str) ? null : str;
            } catch (Exception e) {
                // ignore
            }
        }
        return null;
    }

    public static String getSegmentId() {
        if (GET_SEGMENT_ID_METHOD != null) {
            try {
                String str = (String) GET_SEGMENT_ID_METHOD.invoke(SKYWALKING_CONTEXT_MANAGER_CLASS);
                return EMPTY_TRACE_CONTEXT_ID.equals(str) ? null : str;
            } catch (Exception e) {
                // ignore
            }
        }
        return null;
    }

    public static int getSpanId() {
        if (GET_SPAN_ID_METHOD != null) {
            try {
                return (int) GET_SPAN_ID_METHOD.invoke(SKYWALKING_CONTEXT_MANAGER_CLASS);
            } catch (Exception e) {
                // ignore
            }
        }
        return -1;
    }

    private SkywalkingUtil() {
    }

}
