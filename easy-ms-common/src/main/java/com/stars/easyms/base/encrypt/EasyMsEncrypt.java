package com.stars.easyms.base.encrypt;

import com.alibaba.fastjson.JSON;
import com.stars.easyms.base.util.AESUtil;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>className: EasyMsEncrypt</p>
 * <p>description: EasyMs的rest服务默认的加解密类</p>
 *
 * @author guoguifang
 * @version 1.2.1
 * @date 2019-07-05 18:08
 */
public final class EasyMsEncrypt {

    /**
     * 解密Rest请求参数
     *
     * @param encryptedMsg 待解密的信息
     * @param encryptSecret   秘钥
     * @param encryptIv    秘钥偏移量
     * @return 解密后的信息
     */
    public static String decode(String encryptedMsg, String encryptSecret, String encryptIv) {
        return decode(encryptedMsg, encryptSecret, encryptIv, null);
    }

    /**
     * 解密Rest请求参数
     *
     * @param encryptedMsg 待解密的信息
     * @param encryptSecret   秘钥
     * @param encryptIv    秘钥偏移量
     * @param key          待解密的信息中需转换的属性key
     * @return 解密后的信息
     */
    public static String decode(String encryptedMsg, String encryptSecret, String encryptIv, String key) {
        if (StringUtils.isBlank(encryptedMsg)) {
            throw new EasyMsEncryptException("待解密信息不能为空");
        }
        if (StringUtils.isBlank(encryptSecret)) {
            throw new EasyMsEncryptException("秘钥不能为空");
        }
        String requestStr = encryptedMsg;
        if (StringUtils.isNotBlank(key)) {
            Map<String, Object> reqParams;
            try {
                reqParams = JSON.parseObject(encryptedMsg);
            } catch (Exception e) {
                throw new EasyMsEncryptException("待解密信息无法转换为有效数据！");
            }
            if (reqParams == null || reqParams.isEmpty()) {
                throw new EasyMsEncryptException("待解密信息不能为空");
            }
            requestStr = (String) reqParams.get(key);
            if (requestStr == null) {
                throw new EasyMsEncryptException("待解密数据中参数[{}]不能为空", key);
            }
        }
        try {
            String decodeStr = AESUtil.aesDecodeStr(requestStr, encryptSecret, encryptIv);
            if (StringUtils.isBlank(decodeStr)) {
                throw new EasyMsEncryptException("解密异常！");
            }
            return decodeStr;
        } catch (Exception e) {
            throw new EasyMsEncryptException("解密异常！", e);
        }
    }

    /**
     * 加密Rest返回参数
     *
     * @param responseMsg 待加密的信息
     * @param encryptSecret  秘钥
     * @param encryptIv   秘钥偏移量
     * @return 加密后的信息，若key为空返回字符串，若key不为空则返回map
     */
    public static String encrypt(Object responseMsg, String encryptSecret, String encryptIv) {
        return (String) encrypt(responseMsg, encryptSecret, encryptIv, null);
    }

    /**
     * 加密Rest返回参数
     *
     * @param responseMsg 待加密的信息
     * @param encryptSecret  秘钥
     * @param encryptIv   秘钥偏移量
     * @param key         加密后的信息需要放到的属性
     * @return 加密后的信息，若key为空返回字符串，若key不为空则返回map
     */
    public static Object encrypt(Object responseMsg, String encryptSecret, String encryptIv, String key) {
        if (StringUtils.isBlank(encryptSecret)) {
            throw new EasyMsEncryptException("秘钥不能为空");
        }
        String responseStr;
        if (responseMsg instanceof String) {
            responseStr = (String) responseMsg;
        } else {
            responseStr = JSON.toJSONString(responseMsg);
        }
        String aesResponse;
        try {
            aesResponse = AESUtil.aesEncryptStr(responseStr, encryptSecret, encryptIv);
        } catch (Exception e) {
            throw new EasyMsEncryptException("加密异常！", e);
        }
        if (StringUtils.isBlank(aesResponse)) {
            throw new EasyMsEncryptException("加密异常！");
        }
        if (StringUtils.isNotBlank(key)) {
            Map<String, Object> responseMap = new HashMap<>(8);
            responseMap.put(key, aesResponse);
            return responseMap;
        }
        return aesResponse;
    }

    private EasyMsEncrypt() {}
}