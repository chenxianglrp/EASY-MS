package com.stars.easyms.base.constant;

/**
 * <p>className: EasyMsCommonConstants</p>
 * <p>description: EasyMs普通常量类</p>
 *
 * @author guoguifang
 * @date 2019-08-30 14:59
 * @since 1.3.0
 */
public final class EasyMsCommonConstants {

    public static final String APP_ID = "app.id";

    public static final String APOLLO_BOOTSTRAP_NAMESPACES = "apollo.bootstrap.namespaces";

    public static final String NAMESPACE_APPLICATION = "application";

    public static final String YES = "Y";

    public static final String NO = "N";

    public static final String COMMA = ",";

    public static final String PLUME_LOG_TRACE_ID_CLASS_NAME = "com.plumelog.core.TraceId";

    public static final String ASYNC_ID_CONNECTION_SYMBOL = ".";

    public static final String REDIS_KEY_SEPARATOR = ":";

    public static final String UNKNOWN = "unknown";

    public static final String DEFAULT_ENCRYPT_KEY = "seccode";

    public static final String DEFAULT = "default";

    public static final String EASY_MS_PROPERTIES_PREFIX = "easy-ms.";

    private EasyMsCommonConstants() {
    }
}