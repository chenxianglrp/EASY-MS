# EASY-MS

　　`Easy-Ms`是一个`基于Spring Cloud、Spring Boot、Mybatis`的`微服务功能增强`框架，具有`组件化`、`高性能`、`功能丰富`的特点。代码简洁，架构清晰，组件可自由搭配，遵循阿里巴巴开发标准规范，可以作为后端服务的开发基础框架。

　　核心目的是让开发人员写少量的代码即可快速搭建起一套功能强大的微服务应用，利用业务代码与技术代码相隔离的思想，使项目看起来更简洁、结构更清晰、开发更简单，让敏捷开发变得更敏捷。

## 组件

`easy-ms`  :  组件整合

`easy-ms-common` : 基础依赖组件

`easy-ms-datasource`  :   数据源增强组件

`easy-ms-logging` : 日志输出增强组件

`easy-ms-rest ` : Rest接口增强组件

`easy-ms-mq` : MQ增强组件

`easy-ms-redis` :  Redis增强组件

　　其他组件例如前端、权限、部分公共代码等组件由于涉及到公司内部私密代码，暂不开源，后续可根据实际情况独立出来或者直接依赖使用其他前端框架

## 组件介绍

### 基础依赖组件

#### 　功能特点介绍

1. 

#### 　**可[点击这里](/easy-ms-redis/README.md)查看详细文档**

### 数据源增强组件

#### 　**功能特点介绍**

1. 支持**单、多数据源**，同时**每个数据源又支持多主多从结构**；
2. **配置简单**，大大**简化了数据源的配置**，同时**提供了全局参数配置**；
3. 支持**负载均衡**，包含主流负载均衡策略，满足绝大多数场景，可以最大限度地提高数据库使用率；
4. 支持**高可用**，数据库宕机等原因引起的数据库连接超时或失败，负载均衡时可以自动剔除该数据库；
5. 支持**故障转移**，在执行SQL期间遇到数据库连接问题还可以自动切换其他数据库重试；
6. 支持应用运行期间**动态增加、修改、删除数据源**；
7. 支持**多数据源分布式事务**，需配合Spring的**事务注解@Transactional**或使用**编程式事务**使用；
8. 支持**慢SQL记录**，也支持开关（自定义是否开启），支持配置慢SQL的时间，可在页面进行实时展示；
9. 支持**所有数据源的监控**，可详细显示当前所有数据源正在执行的SQL，包括已用时、已完成数量，正在处理数量等等非常详细的监控数据；
10. 集成**PageHelper分页**，并使用了**异步查询总数**的方式，性能提升较大；
11. 支持以**注解声明式**或者**手动编程式**两种方式指定数据源名称以及指定是否强制选择主或者从；
12. 支持**批量操作**，可以以在mybatis的xml写SQL的方式批量提交，SQL管理更简单，代码更简洁，使用更简单，效率更高，同时支持批量事务；
13. 严格控制增删改操作必须走主数据源，降低开发人员由于代码错误造成的BUG风险；
14. 不需要引入mycat等中间件，但拥有mycat的所有功能，而且比mycat使用更灵活，使用场景更丰富，比使用mycat架构更简单，却更好用，也无需部署，节省服务器成本，也不需要运维人员维护；
15. 支持在非Easy-Ms、已存在的spring boot或spring mvc项目中引入使用。

#### 　**可[点击这里](/easy-ms-datasource/README.md)查看详细文档**

### 日志输出增强组件

#### 　功能特点介绍

1. 采用**双层异步机制**，在大幅提升性能的同时还能保证占用较少内存；
2. 采用**自研无锁队列**，**高并发下不仅无阻塞还能保证线程安全及队列顺序一致**；
3. **日志对象复用**，高并发下可大大降低内存的占用；
4. 优化日志打印逻辑，**高并发下减少IO次数**，提升日志输出性能；
5. 重写部分日志转化器（如日期转化器等），高并发下日志转化速度大大提升；
6. 支持**Logback（已兼容所有版本）、commons-logging**，可支持其他框架的扩展；
7. 支持**实时动态开关切换**（需使用配置中心），可在应用运行期间自由切换是否开启异步日志框架；
8. 支持**在线实时监控**，可实时查看开关、异步日志框架的使用情况等信息；
9. 支持在非Easy-Ms、已存在的spring boot或spring mvc项目中引入使用。

#### 　**可[点击这里](/easy-ms-logging/README.md)查看详细文档**

### Rest接口增强组件

#### 　功能特点介绍

1. 标准化rest接口，开发更简单，代码更整洁；
2. 支持接口全局是否加密、单接口是否加密两种加密方式；
3. 支持阿里巴巴sentinel的接口限流，可返回接口
4. 支持自定义处理返回类；
5. 支持接口分布式锁（需依赖easy-ms-redis）；
6. 优化request和response处理链路，拥有更好的性能；
7. 集成swagger，重写swagger-ui，具有比swagger-ui更丰富的功能；
8. 支持在线调试，调试结果更方便、更快捷；

#### 　**可[点击这里](/easy-ms-rest/README.md)查看详细文档**

### MQ增强组件

#### 　功能特点介绍

1. 支持单条与批量两种发送方式（生产者把多条拼成一条数据发送，消费者把多条拆分成单条处理或多条批量处理）；
2. 支持MQ消息缓存，可实时查询生产与消费信息，5天后自动把成功消息归档；
3. 支持MQ消息发送、消费失败后重新发送、消费，有失败告警；
4. 支持ActiveMQ和RocketMQ

#### 　**可[点击这里](/easy-ms-mq/README.md)查看详细文档**

### Redis增强组件

#### 　功能特点介绍

1. 支持添加前缀（当多个系统共用一个redis时，增加前缀可避免key值重复）
2. 使用注解可增加开发效率，使开发标准化，可防止开发不规范引起的BUG
3. 统一管理，可实时在页面查询所有使用中的redis的key值，防止使用keys查询所有key值时引起系统崩溃
4. 支持分布式锁（使用lua脚本编写，可以防止死锁）

#### 　**可[点击这里](/easy-ms-redis/README.md)查看详细文档**

